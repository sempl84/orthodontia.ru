<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'cliniccourse']">
		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />
		
		<!--Content -->
	    <div class="container-fluid cliniccourse-page">
	        <div class="page-header">
	            <div class="row">
	                <div class="col-xs-12">
	                    <i class="fa fa-users" aria-hidden="true"></i>
	                    <h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
	                </div>
	            </div>
	        </div>
	        <div class="row">
	            <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
	                <xsl:apply-templates select="document('udata://cliniccourse/listElements/?extProps=photo,short_desrc,full_desrc')/udata" />
	            </div>
	            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
	                <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
	                
	                <xsl:apply-templates select="." mode="right_col" />
	                
	            </div>
	        </div>
	    </div>
	</xsl:template>
	
	<xsl:template match="udata[@module='cliniccourse' and @method='listElements']">
		Ничего не найдено
	</xsl:template>
	<xsl:template match="udata[@module='cliniccourse' and @method='listElements' and items/item]">
		<ul class="items">
            <xsl:apply-templates select="items/item" />
        </ul>
        <xsl:apply-templates select="total" />
	</xsl:template>
	
	<xsl:template match="udata[@module='cliniccourse' and @method='listElements']/items/item" >	
		<li class="item">
            <div class="white-pnl">
                <div class="left">
                    <a href="{@link}">
                    	<xsl:choose>
							<xsl:when test=".//property[@name='photo']/value">
								<img src="{.//property[@name='photo']/value}" class="img-responsive" alt="{text()}" />
							</xsl:when>
							<xsl:otherwise>
								<img src="&empty-photo-speaker;" class="img-responsive" alt="{text()}" />
							</xsl:otherwise>
						</xsl:choose>
                    	
                    </a>
                </div>
                <div class="right">
                    <h4><a href="{@link}"><xsl:value-of select="text()" disable-output-escaping="yes" /></a></h4>
                    <p umi:element-id="{@id}" umi:field-name="short_desrc" umi:empty="&empty-page-content;">
						<xsl:value-of select=".//property[@name = 'short_desrc']/value" disable-output-escaping="yes" />
					</p>
                </div>
                <div class="about">
                	<div umi:element-id="{@id}" umi:field-name="full_desrc" umi:empty="&empty-page-content;">
						<xsl:value-of select=".//property[@name = 'full_desrc']/value" disable-output-escaping="yes" />
                	</div>
                	
                    <a class="link-right-arrow" href="&all_event_url;?filter[speaker][2]={text()}">Мероприятия спикера <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </li>
	</xsl:template>

</xsl:stylesheet>