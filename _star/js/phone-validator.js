/**
 * Phone validator
 */

var phoneValidator = {
  // Urls
  IS_PHONE_VALID_URL: 'json/is_valid_phone.json',
  SEND_CODE_URL: 'json/send_code.json',
  APPLY_CODE_URL: 'json/apply_code.json',
  CHANGE_PHONE_URL: 'json/change_phone.json',

  // Defaults
  $phoneWrapper: undefined,
  $phoneInput: undefined,
  $phoneInputValue: '',

  init: function (element) {
    var $this = this;

    // Defaults
    $this.$phoneWrapper = $(element);
    $this.$phoneInput = $this.$phoneWrapper.find('.js-phone--input');

    // Not validated check
    var isNotValidated = $this.$phoneWrapper.hasClass('js-phone--not-validated');
    if (isNotValidated) {
      $this.removeStartUpContent();
      $this.removeCheckCodeContent();
      $this.removeMessageContent();
      $this.addStartUpContent();
    }

    // Input changes
    $this.$phoneInput.on('change keyup paste', function (e) {
      var value = e.target.value;
      $this.removeStartUpContent();
      $this.removeCheckCodeContent();
      $this.removeMessageContent();

      $this.updateInputValue(value);
      var phoneCode = value.substring(0, 2);
      if (phoneCode === '+7' && value.length === 12) {
        $this.addStartUpContent();
      }
    });

    // Clicks
    $this.$phoneWrapper
      .on('click', '.js-phone--later', function (e) {
        e.preventDefault();

        $this.$phoneInput.attr('readonly', false);
        $this.removeStartUpContent();
        $this.removeCheckCodeContent();
      })
      .on('click', '.js-phone--send-code', function (e) {
        e.preventDefault();

        var phoneValue = $this.$phoneInputValue;
        $this.sendCode(phoneValue);
      })
      .on('click', '.js-phone--validate', function (e) {
        e.preventDefault();

        var code = $this.$phoneWrapper
          .find('.js-phone--code-input')
          .val();

        if (code) {
          $this.applyCode(code);
        }
      })
      .on('click', '.js-phone--repeat', function (e) {
        e.preventDefault();

        var phoneValue = $this.$phoneInputValue;
        $this.sendCode(phoneValue);
      })
      .on('click', '.js-phone--change-phone', function (e) {
        e.preventDefault();

        $('#phoneValidateModal').modal('hide');
        $('#phoneChangeModal').modal('show');
      });
  },

  /**
   * API methods
   */

  updateInputValue: function(value) {
    this.$phoneInputValue = value;
  },

  checkPhone: function (phone) {
    var $this = this;

    $.ajax({
      type: "POST",
      url: $this.IS_PHONE_VALID_URL,
      dataType: "json",
      data: {phone: phone},
      contentType: "application/json; charset=utf-8",
      success: function (data) {
        if (data.status === 'successful') {
          alert('checkPhone: successful');
        } else if (data.status === 'error') {
          alert('checkPhone: error');
        }
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus);
        alert("Error: " + errorThrown);
      }
    })
  },

  sendCode: function (phone) {
    var $this = this;

    $this.removeMessageContent();
    $.ajax({
      type: "POST",
      url: $this.SEND_CODE_URL,
      dataType: "json",
      data: {phone: phone},
      contentType: "application/json; charset=utf-8",
      success: function (data) {
        if (data.status === 'successful') {
          $this.removeStartUpContent();
          $this.removeCheckCodeContent();

          $this.addCheckCodeContent();
        } else if (data.status === 'error') {
          alert('checkPhone: error');
        }
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus);
        alert("Error: " + errorThrown);
      }
    })
  },

  applyCode: function (code) {
    var $this = this;
    var $phoneInput = $this.$phoneInput;

    $this.clearCodeInputErrors();
    $.ajax({
      type: "POST",
      url: $this.APPLY_CODE_URL,
      dataType: "json",
      data: {code: code},
      contentType: "application/json; charset=utf-8",
      success: function (data) {
        if (data.status === 'successful') {
          $phoneInput.attr('readonly', false);
          $this.removeCheckCodeContent();
          $this.addMessageContent('Ваш телефон успешно подтверждён!');
        } else if (data.status === 'error') {
          $this.showCodeInputErrors(data.error.message);
        }
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus);
        alert("Error: " + errorThrown);
      }
    })
  },

  changePhone: function (phone) {
    var $this = this;
    var $phoneInput = $this.$phoneInput;

    $this.clearCodeInputErrors();
    $.ajax({
      type: "POST",
      url: $this.CHANGE_PHONE_URL,
      dataType: "json",
      data: {phone: phone},
      contentType: "application/json; charset=utf-8",
      success: function (data) {
        if (data.status === 'successful') {
          $phoneInput.attr('readonly', false);
          $this.removeCheckCodeContent();
          $this.addMessageContent('Ваш телефон успешно изменён!');
        } else if (data.status === 'error') {
          $this.showCodeInputErrors(data.error.message);
        }
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert("Status: " + textStatus);
        alert("Error: " + errorThrown);
      }
    })
  },


  /**
   * Content methods
   */

  // Start Up Content
  addStartUpContent: function () {
    var $this = this;
    var $wrapper = $this.$phoneWrapper;

    // Content
    var $content = $('<div class="phone-validator js-phone--StartUpContent"></div>').appendTo($wrapper);
    $('<h5></h5>').appendTo($content).text('Подтвердите ваш номер мобильного телефона');
    $('<a class="btn btn-primary js-phone--send-code"></a>')
      .appendTo($content)
      .attr('href', '#')
      .text('Отправить код');
    $('<a class="btn btn-link js-phone--later"></a>')
      .appendTo($content)
      .attr('href', '#')
      .text('Подтвердить позже');
  },

  removeStartUpContent: function () {
    var $wrapper = this.$phoneWrapper;

    $wrapper
      .find('.js-phone--StartUpContent')
      .remove();
  },


  // Check Code Content
  addCheckCodeContent: function () {
    var $this = this;
    var $phoneInput = this.$phoneInput;
    var $wrapper = this.$phoneWrapper;

    // Readonly
    $phoneInput.attr('readonly', true);

    // Content
    var $content = $('<div class="phone-validator js-phone--CheckCodeContent"></div>').appendTo($wrapper);
    $('<input class="form-control js-phone--code-input" />')
      .appendTo($content)
      .attr('type', 'text')
      .attr('placeholder', 'Введите код')
      .attr('data-val', true)
      .attr('data-val-required', true);

    $('<a class="btn btn-primary js-phone--validate"></a>')
      .appendTo($content)
      .attr('href', '#')
      .text('Подтвердить номер');

    $('<a class="btn btn-link js-phone--later"></a>')
      .appendTo($content)
      .attr('href', '#')
      .text('Подтвердить позже');

    setTimeout(function () {
      $phoneInput.attr('readonly', false);
      $('<a class="btn btn-link js-phone--repeat"></a>')
        .appendTo($content)
        .attr('href', '#')
        .text('Отправить код повторно');
    }, 90000);
  },

  removeCheckCodeContent: function () {
    var $wrapper = this.$phoneWrapper;

    $wrapper
      .find('.js-phone--CheckCodeContent')
      .remove();
  },

  // Errors
  showCodeInputErrors: function (message) {
    var $this = this;
    var $codeInput = $this.$phoneWrapper.find('.js-phone--code-input');
    $codeInput.addClass('input-validation-error');

    if (message) {
      $codeInput.after('<div class="phone-validator__error">' + message + '</div>');
    }
  },

  clearCodeInputErrors: function () {
    var $this = this;
    $this.$phoneWrapper
      .find('.js-phone--code-input')
      .removeClass('input-validation-error');

    $this.$phoneWrapper
      .find('.phone-validator__error')
      .remove();
  },

  // Messages
  addMessageContent: function (message, isError) {
    var type = isError ? 'error' : 'success';

    var $this = this;
    var $wrapper = $this.$phoneWrapper;

    // Content
    var $content = $('<div class="phone-validator js-phone--MessageContent"></div>').appendTo($wrapper);
    $('<div class="phone-validator__message phone-validator__message_' + type + '"></div>')
      .appendTo($content)
      .text(message);
  },

  removeMessageContent: function () {
    var $this = this;
    var $wrapper = $this.$phoneWrapper;

    $wrapper
      .find('.js-phone--MessageContent')
      .remove();
  }
};

// Init phone magic
$('.js-phone--wrapper').each(function () {
  var phoneValidatorObject = Object.create(phoneValidator);
  phoneValidatorObject.init(this);
});

// Open validation modal (visibility by default)
$('#phoneValidateModal').modal('show');
