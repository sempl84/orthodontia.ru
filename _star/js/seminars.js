﻿jQuery(function ($) {
    $.datepicker.regional['ru'] = {
        closeText: 'Закрыть',
        prevText: '&#x3c;Пред',
        nextText: 'След&#x3e;',
        currentText: 'Сегодня',
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
		'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
		'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
        dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
        dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        dateFormat: 'dd.mm.yy', firstDay: 1,
        isRTL: false
    };
    $.datepicker.setDefaults($.datepicker.regional['ru']);
});

var seminars = {
    $wrapCalendar: null,
    $calendar: null,
    arrayDays: [],
    types: [],
    init: function (dates, types) {
        var cthis = this;
        this.$wrapCalendar = $('.js-calendar');
        if (this.$wrapCalendar.length != 0) {
            this.arrayDays = dates;
            this.types = types;
            this.$calendar = $('<div/>').appendTo(this.$wrapCalendar).attr('class', 'calendar');
            this.$calendar.datepicker({
                showOtherMonths: true,
                beforeShowDay: function (date) {
                    var cday = cthis.getDay(date);
                    if (cday != null) {
                        return [true, cthis.types[cday.type].className, cday.description];
                    }
                    return [true];
                },
                onSelect: function (dateText, inst) {
                    var paths = dateText.split('.')
                    var cday = cthis.getDay(new Date(parseInt(paths[2]), parseInt(paths[1]) - 1, parseInt(paths[0]), 0, 0, 0, 0));
                    if (cday != null) {
                        window.location = cday.link;
                    }
                }
            });
            this.setTypes();
        }
    },
    setTypes: function () {
        var $wraper = $('<div/>').appendTo(this.$wrapCalendar).attr('class', 'seminar-types');
        for (var i = 0; i < this.types.length; i++) {
            $('<a/>').appendTo($wraper).attr('class', 'seminar-type-link ' + this.types[i].className).
            attr('href', this.types[i].link).text(this.types[i].description);
        }
    },
    getDay: function (date) {
        var cday = null;
        var m = date.getMonth(),
        d = date.getDate(),
        y = date.getFullYear();

        for (var i = 0; i < this.arrayDays.length; i++) {
            if (this.arrayDays[i].date == d + '.' + (m + 1) + '.' + y) {
                cday = this.arrayDays[i];
                break;
            }
        }
        return cday;
    }
}

$(document).ready(function () {
    if (typeof settings != typeof undefined)
        seminars.init(settings.dates, settings.types);
});