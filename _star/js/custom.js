﻿var menuService = {
    init: function () {
        $(window).resize(function () {
            var className = '.js-main-menu';
            if ($(window).width() < 854) {
                className = '.js-top-menu';
                $('.js-academy').insertAfter(className + ' > li:last');
                $('.js-cabinet').insertAfter(className + ' > li:last');
                $('.js-top-menu > li.js-corporation').addClass('dropdown');
                $('.js-top-menu > li.js-corporation > ul').addClass('dropdown-menu');
            }
            else {
                $('.js-academy').insertAfter(className + ' > li:last');
                $('.js-cabinet').insertAfter(className + ' > li:last');
                $('.js-top-menu > li.js-corporation').removeClass('dropdown');
                $('.js-top-menu > li.js-corporation > ul').removeClass('dropdown-menu');
            }
        }).resize();

        $(window).scroll(function () {
            if ($(document).scrollTop() > 50) {
                $('nav').addClass('shrink');
            } else {
                $('nav').removeClass('shrink');
            }
        });
    }
}

var userProfileService = {
    init: function () {
        var $mainPanel = $('.js-user-profile-form');
        if ($mainPanel.length != 0) {
            $mainPanel.find('.js-phone').mask('+7 (000) 0000-0000', { placeholder: "+7 (___) ____-____" });
        }
    }
}

var photoSlider = {
    init: function () {
        var $pnl = $('.js-photo-slider');
        if ($pnl.length != 0) {
            var totalItems = $pnl.find('.item').length;
            var currentIndex = $pnl.find('div.active').index() + 1;

            $pnl.find('.js-numbers').html(currentIndex + ' / ' + totalItems);
            $pnl.bind('slid.bs.carousel', function () {
                var currentIndex = $pnl.find('div.active').index() + 1;
                $pnl.find('.js-numbers').html(currentIndex + ' / ' + totalItems);
            });
        }
    }
}


var initPhotoSwipeFromDOM = function (gallerySelector) {

    //add figure html tag from img alt tag
    $(gallerySelector).find('a').each(function () {
        var $lnk = $(this);
        var $img = $lnk.find('img:first');
        if ($lnk.find('figure').length == 0 && $img.length != 0) {
            var $figure = $('<figure/>').prependTo($lnk);
            if ($img.attr('alt'))
                $figure.text($img.attr('alt'));
        }
        else {
            var $figure = $('<figure/>').appendTo($lnk);
            if ($lnk.attr('title'))
                $figure.text($lnk.attr('title'));
        }
    });

    var parseThumbnailElements = function (el) {
        var thumbElements = $(el).find('a.js-photo').toArray(),
            numNodes = thumbElements.length,
            items = [],
            el,
            childElements,
            thumbnailEl,
            size,
            item;

        for (var i = 0; i < numNodes; i++) {
            el = thumbElements[i];

            // include only element nodes
            if (el.nodeType !== 1) {
                continue;
            }

            childElements = el.children;

            size = el.getAttribute('data-size').split('x');

            // create slide object
            item = {
                src: el.getAttribute('href'),
                w: parseInt(size[0], 10),
                h: parseInt(size[1], 10),
                author: '',
            };

            item.el = el; // save link to element for getThumbBoundsFn

            if (childElements.length > 0) {
                item.msrc = childElements[0].getAttribute('src'); // thumbnail url
                item.title = childElements[0].innerHTML;

            }


            var mediumSrc = el.getAttribute('data-med');
            if (mediumSrc) {
                size = el.getAttribute('data-med-size').split('x');
                // "medium-sized" image
                item.m = {
                    src: mediumSrc,
                    w: parseInt(size[0], 10),
                    h: parseInt(size[1], 10)
                };
            }
            // original image
            item.o = {
                src: item.src,
                w: item.w,
                h: item.h
            };

            items.push(item);
        }

        return items;
    };

    var closest = function closest(el, fn) {
        return el && (fn(el) ? el : closest(el.parentNode, fn));
    };

    var onThumbnailsClick = function (e) {
        e = e || window.event;
        e.preventDefault ? e.preventDefault() : e.returnValue = false;

        var eTarget = e.target || e.srcElement;

        var clickedListItem = closest(eTarget, function (el) {
            return el.tagName === 'A';
        });

        if (!clickedListItem) {
            return;
        }

        var clickedGallery = $(clickedListItem).parents('.js-photos:first')[0];

        var childNodes = $(clickedGallery).find('a').toArray(),
            numChildNodes = childNodes.length,
            nodeIndex = 0,
            index;

        for (var i = 0; i < numChildNodes; i++) {
            if (childNodes[i].nodeType !== 1) {
                continue;
            }

            if (childNodes[i] === clickedListItem) {
                index = nodeIndex;
                break;
            }
            nodeIndex++;
        }

        if (index >= 0) {
            openPhotoSwipe(index, clickedGallery);
        }
        return false;
    };

    var photoswipeParseHash = function () {
        var hash = window.location.hash.substring(1),
        params = {};

        if (hash.length < 5) { // pid=1
            return params;
        }

        var vars = hash.split('&');
        for (var i = 0; i < vars.length; i++) {
            if (!vars[i]) {
                continue;
            }
            var pair = vars[i].split('=');
            if (pair.length < 2) {
                continue;
            }
            params[pair[0]] = pair[1];
        }

        if (params.gid) {
            params.gid = parseInt(params.gid, 10);
        }

        return params;
    };

    var openPhotoSwipe = function (index, galleryElement, disableAnimation, fromURL) {
        var pswpElement = document.querySelectorAll('.pswp')[0],
            gallery,
            options,
            items;

        items = parseThumbnailElements(galleryElement);

        options = {

            galleryUID: galleryElement.getAttribute('data-pswp-uid'),

            getThumbBoundsFn: function (index) {
                // See Options->getThumbBoundsFn section of docs for more info
                var thumbnail = items[index].el.children[0],
                    pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                    rect = thumbnail.getBoundingClientRect();

                return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
            },

            addCaptionHTMLFn: function (item, captionEl, isFake) {
                if (!item.title) {
                    captionEl.children[0].innerText = '';
                    return false;
                }
                captionEl.children[0].innerHTML = item.title + '<br/><small>Photo: ' + item.author + '</small>';
                return true;
            }

        };


        if (fromURL) {
            if (options.galleryPIDs) {
                for (var j = 0; j < items.length; j++) {
                    if (items[j].pid == index) {
                        options.index = j;
                        break;
                    }
                }
            } else {
                options.index = parseInt(index, 10) - 1;
            }
        } else {
            options.index = parseInt(index, 10);
        }

        // exit if index not found
        if (isNaN(options.index)) {
            return;
        }

        var radios = document.getElementsByName('gallery-style');
        for (var i = 0, length = radios.length; i < length; i++) {
            if (radios[i].checked) {
                if (radios[i].id == 'radio-all-controls') {

                } else if (radios[i].id == 'radio-minimal-black') {
                    options.mainClass = 'pswp--minimal--dark';
                    options.barsSize = { top: 0, bottom: 0 };
                    options.captionEl = false;
                    options.fullscreenEl = false;
                    options.shareEl = false;
                    options.bgOpacity = 0.85;
                    options.tapToClose = true;
                    options.tapToToggleControls = false;
                }
                break;
            }
        }

        if (disableAnimation) {
            options.showAnimationDuration = 0;
        }

        gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);

        var realViewportWidth,
            firstResize = true;

        gallery.listen('beforeResize', function () {

            var dpiRatio = window.devicePixelRatio ? window.devicePixelRatio : 1;
            dpiRatio = Math.min(dpiRatio, 2.5);
            realViewportWidth = gallery.viewportSize.x * dpiRatio;

            if (!firstResize) {
                gallery.invalidateCurrItems();
            }

            if (firstResize) {
                firstResize = false;
            }
        });

        gallery.listen('gettingData', function (index, item) {
            item.src = item.o.src;
            item.w = item.o.w;
            item.h = item.o.h;
        });

        gallery.init();
    };

    var galleryElements = document.querySelectorAll(gallerySelector);
    for (var i = 0, l = galleryElements.length; i < l; i++) {
        galleryElements[i].setAttribute('data-pswp-uid', i + 1);
        galleryElements[i].onclick = onThumbnailsClick;
    }

    var hashData = photoswipeParseHash();
    if (hashData.pid && hashData.gid) {
        openPhotoSwipe(hashData.pid, galleryElements[hashData.gid - 1], true, true);
    }
};

var topArrow = {
    init: function () {
        var $topLink = $('<a/>').attr('href', 'javascript:void(0)').addClass('top-arrow').appendTo($(document.body));
        $topLink.click(function () {
            $('body,html').stop().animate({ scrollTop: 0 }, 500);
            return false;
        });

        $(window).scroll(function () {
            if ($(document).scrollTop() < 50)
                $topLink.hide();
            else
                $topLink.css('display', 'block');
        }).scroll();
    }
}

var photosService = {
    init: function () {
        var $filterByDate = $('#filterByDate');
        if ($filterByDate.length != 0) {
            $filterByDate.removeClass('hidden').hide().datepicker({
                beforeShow: function (input, inst) {
                    $('#ui-datepicker-div').addClass('blue-skin');
                }
            });
            $('.js-show-calendar').click(function () { $filterByDate.show().focus().hide(); });
        }
    }
}

var collapseService = {
    init: function () {
        //alert(Collapse.TRANSITION_DURATION);
        $('.js-items div.collapse').on('hide.bs.collapse', function () {
            $('html,body').animate({
                scrollTop: $(this).parents('.white-pnl').offset().top - 100
            }, 600);
        });
    }
}


var dealers = {
    $delears_tables: null,
    $ddl_cities: null,
    $ddl_coutries: null,
    dealers_data: [],
    all_cities_value: "",
    init_controls: function () {
        this.$delears_tables = $('.js-dealers-table tbody');
        if (this.$delears_tables.length != 0) {
            this.$ddl_cities = $('.js-cities').change(function () {
                dealers.set_dealers_data();
            });
            this.$ddl_coutries = $('.js-coutries').change(function () {
                dealers.set_cities_ddl();
                dealers.set_dealers_data();
            });
            this.all_cities_value = this.$ddl_cities.val();
            this.load_dealers_data();
        }
    },
    load_dealers_data: function () {
        $.ajax({
            type: "GET",
            url: "dealers.json",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            processData: false,
            success: function (data) {
                dealers.dealers_data = data;
                dealers.set_coutries_ddl();
                dealers.set_dealers_data();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("Status: " + textStatus); alert("Error: " + errorThrown);
            }
        })
    },
    set_coutries_ddl: function () {
        var coutries = {};
        for (var i = 0; i < this.dealers_data.length; i++) {
            var item = this.dealers_data[i];
            if (!coutries.hasOwnProperty(item.country)) {
                coutries[item.country] = 1;
                $("<option></option>").appendTo(this.$ddl_coutries).text(item.country).val(item.country);
            }
        }
    },
    set_cities_ddl: function () {
        var coutries = {};
        var selected_country = this.$ddl_coutries.val();
        this.$ddl_cities.empty();
        $("<option></option>").appendTo(this.$ddl_cities).text(this.all_cities_value).val(this.all_cities_value);
        var cities = [];
        for (var i = 0; i < this.dealers_data.length; i++) {
            var item = this.dealers_data[i];
            if (!coutries.hasOwnProperty(item.city) && selected_country == item.country) {
                coutries[item.city] = 1;
                cities.push(item.city);
            }
        }
        cities.sort();
        for (var i = 0; i < cities.length; i++) {
            $("<option></option>").appendTo(this.$ddl_cities).text(cities[i]).val(cities[i]);
        }
        this.$ddl_cities.selectpicker('refresh');
    },
    set_dealers_data: function () {
        var last_location = null;
        this.$delears_tables.empty();
        var selected_country = this.$ddl_coutries.val();
        var selected_city = this.$ddl_cities.val();
        var selected_country_index = this.$ddl_coutries.children().index(this.$ddl_coutries.find("option:selected"));
        var selected_city_index = this.$ddl_cities.children().index(this.$ddl_cities.find("option:selected"));
        for (var i = 0; i < this.dealers_data.length; i++) {
            var item = this.dealers_data[i];
            if ((item.city == selected_city || selected_city_index == 0) &&
				(item.country == selected_country || selected_country_index == 0)) {
                var $row = $("<tr></tr>").appendTo(this.$delears_tables);
                $("<td></td>").appendTo($row).text(item.country);
                $("<a></a>").appendTo($("<td></td>").appendTo($row)).attr("href", item.webaddress).attr("target", "_blank").attr("rel", "nofollow").text(item.name);
                $("<td></td>").appendTo($row).text(item.address);
                $("<td></td>").appendTo($row).html(item.phone.split(',').join('<br/>'));
                $("<a></a>").appendTo($("<td></td>").appendTo($row)).attr("href", item.email).text(item.email);
            }
        }
    }
}

var tilesResize = {
    init: function () {
        var $items = $('.js-tiles-resize > li > div');
        var $wrapperItems = $('.js-tiles-resize > li > div > .topic-wrapper');
        if ($items.length != 0) {
            $(window).resize(function () {
                if ($(window).width() > 580) {
                    var maxHeight = 0;
                    $items.css('min-height', '');
                    $wrapperItems.css('min-height', '');
                    $items.each(function () {
                        maxHeight = Math.max(maxHeight, $(this).height());
                    });
                    maxHeight += 20;

                    $wrapperItems.each(function () {
                        $(this).css('min-height', $(this).height() + (maxHeight - $(this).parent().height()));
                    });
                    $items.css('min-height', maxHeight + 'px');
                }
                else {
                    $wrapperItems.each(function () {
                        $(this).css('min-height', '');
                    });
                    $items.css('min-height', '');
                }
            }).resize();

        }
    }
}

var modalService = {
    init: function () {
        var nb = $('nav.navbar-fixed-top');
        $('.modal').on('show.bs.modal', function () {
            nb.width(nb.width());
        }).on('hidden.bs.modal', function () {
            nb.width(nb.width('auto'));
        });
    }
}

$('[data-ride="carousel"]').carousel({
    swipe: 50
});

var uploadFile = {
    init: function () {
        $('.js-file-upload').each(function () {
            var $control = $(this);
            $control.find('input').change(function () {
                $control.find('span').text($(this).val());
            });
        });
    }
}

modalService.init();
tilesResize.init();
dealers.init_controls();
initPhotoSwipeFromDOM('.js-photos');
photoSlider.init();
menuService.init();
userProfileService.init();
topArrow.init();
photosService.init();
collapseService.init();
uploadFile.init();

$(function () {
    $('[data-toggle="tooltip"]').tooltip();
});
