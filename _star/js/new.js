$(document).ready(function () {
  /**
   * Download list
   */

  var downloadList = {
    $list: $('#dealers-list'),

    init: function () {
      var $this = this;

      // Resize
      $(window).resize(function() {
        if ($(window).width() <= 991) {
          $('.download-list').removeClass('download-list_sticky');
        }
      });

      // Scroll
      $(window).scroll(function() {
        if ($(document).scrollTop() > 320 && $(window).width() > 991) {
          $('.download-list').addClass('download-list_sticky');
        } else {
          $('.download-list').removeClass('download-list_sticky');
        }
      });

      // Click on "checkbox"
      $this.$list.on('click', '.js--pdf-download-item', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var $target = $(e.currentTarget ? e.currentTarget : e.target);
        var isActive = $target.hasClass('card-item__checkbox_checked');

        if (!isActive) {
          $target.addClass('card-item__checkbox_checked');
        } else {
          $target.removeClass('card-item__checkbox_checked');
        }
      });

      // Click on "checkbox"
      $this.$list.on('click', '.card-item', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var $target = $(e.currentTarget ? e.currentTarget : e.target);
        var url = $target.data('href');

        if (url) {
          window.location.href = url;
        }
      });

      // Click on download link
      $('.download-list').on('click', '.js--pdf-download-link', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var pdfIdsArray = [];

        $('.js--pdf-download-item')
          .each(function () {
            var $item = $(this);
            if ($item.hasClass('card-item__checkbox_checked')) {
              var pdfId = $item.data('pdf-id');
              pdfIdsArray.push(pdfId);
            }
          });

        if (pdfIdsArray.length > 0) {
          window.open('/catalog/genpdf/(' + pdfIdsArray.join(',') + ')', '_blank');
        }
      });
    }
  };


  /**
   * Filters
   */

  var filterContent = {
    SUBMIT_URL: 'catalog/getSmartFilter',
    $listWrapper: $('#dealers-list'),

    init: function () {
      var $this = this;

      // Add buttons for multiple select
      $('.multiple-custom').on('loaded.bs.select', function (e) {
        var $target = $(e.currentTarget ? e.currentTarget : e.target);

        var $dropdownMenu = $target.parent().find('div.dropdown-menu');
        var $dropdownMenuButtonContainer = $('<div class="dropdown-menu__button-container"></div>').appendTo($dropdownMenu);

        $('<a class="btn btn-primary btn_small js--filters-submit"></a>')
          .appendTo($dropdownMenuButtonContainer)
          .attr('href', '#')
          .text('Применить');
      });

      // On change
      $('.js--filters select').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
        var select = e.currentTarget ? e.currentTarget : e.target;
        var isMultipleSelect = select.multiple;

        if (!isMultipleSelect) {
          var params = $this.getParams();
          $this.submitData(params);
        }
      });

      // On click
      $(document).on('click', '.js--filters-submit', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var params = $this.getParams();
        $this.submitData(params);
      });

      // On click
      $('.right-filters').on('click', '.js--right-filters', function(e) {
        e.preventDefault();
        e.stopPropagation();
        var $target = $(e.currentTarget ? e.currentTarget : e.target);

        if ($target.hasClass('right-filters__item-link_active')) {
          $target.removeClass('right-filters__item-link_active');
          $target
            .find('.right-filters__item-checkbox')
            .removeClass('right-filters__item-checkbox_checked');
        } else {
          $target.addClass('right-filters__item-link_active');
          $target
            .find('.right-filters__item-checkbox')
            .addClass('right-filters__item-checkbox_checked');
        }
      });
    },

    getParams: function () {
      var filters = {};

      $('.js--filters select')
        .each(function () {
            var item = this;

            if ($(item).val()) {
              var filter = {};
              filter[item.name] = $(item).val();
              filters = Object.assign({}, filters, filter);
            }
          });

      return filters;
    },

    submitData: function (params) {
      var $this = this;

      $.ajax({
        type: 'GET',
        url: $this.SUBMIT_URL,
        data: params,
        success: function (data) {
          $this.$listWrapper.html(data);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          console.error(XMLHttpRequest, textStatus, errorThrown);
        }
      });
    }
  };


  /**
   * Custom file input
   */

  var customFileInput = {
    init: function () {
      $('.input-file').before(function() {
        var $this = $(this);

        if (!$this.prev().hasClass('input-ghost')) {
          var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
          element.attr('name', $this.data('name'));
          element.change(function(){
            element.next(element).find('input').val((element.val()).split('\\').pop());
          });

          $this.click(function(){
            element.click();
          });

          // $this.find('button.btn-reset').click(function(){
          //   element.val(null);
          //   $this.parents('.input-file').find('input').val('');
          // });

          $this.find('input').css('cursor', 'pointer');
          $this.find('input').mousedown(function() {
            $this.parents('.input-file').prev().click();

            return false;
          });

          return element;
        }
      });
    }
  };


  /**
   * Submenu
   */

  var subMenu = {
    init: function () {
      $('.submenu').on('click', '.submenu__top', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var $subMenu = $('.submenu');
        var $subMenuContent = $('.submenu__content');
        var isOpen = $subMenu.hasClass('submenu_is-open');

        if (!isOpen) {
          $subMenu.addClass('submenu_is-open');
        } else {
          $subMenu.removeClass('submenu_is-open');
        }
        $subMenuContent.slideToggle();
      });
    }
  };


  // Init all
  downloadList.init();
  filterContent.init();
  customFileInput.init();
  subMenu.init();
});
