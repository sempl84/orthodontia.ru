<?php
	ob_start();
	require CURRENT_WORKING_DIR . "/libs/root-src/standalone.php";
	ob_end_clean();

	session::getInstance();
	$drawer = umiCaptcha::getDrawer();

	$code = $drawer->getRandomCode();
	$id = getRequest('id');

	if (!is_null($id)) {
		$captchaId = (string) $id;

		if (is_string($_SESSION['umi_captcha'])) {
			$_SESSION['umi_captcha'] = array();
		}

		$_SESSION['umi_captcha'][$captchaId] = md5($code);
	} else {
		$_SESSION['umi_captcha'] = md5($code);
	}

	$drawer->draw($code);
?>