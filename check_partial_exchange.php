<?php
require_once 'standalone.php';

/**
 * Скрипт нужен для организации порционной загрузки заказов в 1С.
 */
$order_export_max_count = 100; // максимальное разрешенное количество заказов

echo "<h1>Все заказы для выгрузки</h1>";
$orders = new selector('objects');
$orders->types('object-type')->name('emarket', 'order');
$orders->where('need_export')->equals(1);
$orders->order('order_create_date')->asc();
//$orders->limit(0,10);

$orders_late = new selector('objects');
$orders_late->types('object-type')->name('emarket', 'order');
$orders_late->where('need_export_later')->equals(1);
$orders_late->order('order_create_date')->desc();
//$orders_late->limit(0,10);

/**
 * Может быть так, что заказов для выгрузки больше, чем допустимо - тогда делаеи им отложенную выгрузку
 */
if($orders->length() > $order_export_max_count){
    $late_exchange_order_count = $orders->length() - $order_export_max_count;
    echo "<h2>Всего: {$orders->length()}, отложенных: {$orders_late->length()}, допустимо: {$order_export_max_count}. Отложим загрузку самых старых заказов в количестве: {$late_exchange_order_count}</h2>";
    $count = 0;
    foreach($orders as $order){
        echo "{$order->id} - {$order->name}<br />\n";
        $order->setValue('need_export_later', 1);
        $order->setValue('need_export', 0);
        $order->commit();
        $count++;
        if($count == $late_exchange_order_count){
            break;
        }
    }
/**
 * Может быть так, что заказов для выгрузки меньше, чем допустимо - тогда добавляем из отложенных в текущий экспорт
 */
}elseif($orders->length() < $order_export_max_count){
    $late_exchange_order_count = $order_export_max_count - $orders->length();
    $late_exchange_order_count = $late_exchange_order_count > $orders_late->length() ? $orders_late->length() : $late_exchange_order_count;
    echo "<h2>Всего: {$orders->length()}, отложенных: {$orders_late->length()}, допустимо: {$order_export_max_count}. Добавим в загрузку самых новых заказов в количестве: {$late_exchange_order_count}</h2>";
    $count = 0;
    foreach($orders_late as $order){
        echo "{$order->id} - {$order->name}<br />\n";
        $order->setValue('need_export_later', 0);
        $order->setValue('need_export', 1);
        $order->commit();
        $count++;
        if($count == $late_exchange_order_count){
            break;
        }
    }
}else{
    echo "<h2>Всего: {$orders->length()}, отложенных: {$orders_late->length()}, допустимо: {$order_export_max_count}. Можно выполнять обмен заказов с 1С</h2>";
}

//Отложенная выгрузка в 1С[need_export_later](Кнопка-флажок)