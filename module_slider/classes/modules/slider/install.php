<?php

$INFO = Array();

$INFO['name'] = "slider";
$INFO['filename'] = "modules/slider/class.php";
$INFO['config'] = "1";
$INFO['default_method'] = "listElements";
$INFO['default_method_admin'] = "lists";
$INFO['is_indexed'] = "1";
$INFO['per_page'] = "10";

$INFO['func_perms'] = "";

$COMPONENTS = array();

$COMPONENTS[0] = "./classes/modules/slider/__admin.php";
$COMPONENTS[1] = "./classes/modules/slider/class.php";
$COMPONENTS[2] = "./classes/modules/slider/i18n.php";
$COMPONENTS[3] = "./classes/modules/slider/lang.php";
$COMPONENTS[4] = "./classes/modules/slider/permissions.php";
?>