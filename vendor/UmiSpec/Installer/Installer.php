<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2020, Evgenii Ioffe
 */

require_once dirname(__FILE__) . '/Field.php';
require_once dirname(__FILE__) . '/Group.php';
require_once dirname(__FILE__) . '/ObjectType.php';
require_once dirname(__FILE__) . '/HierarchyType.php';
require_once dirname(__FILE__) . '/WebformsTemplate.php';
require_once dirname(__FILE__) . '/UmiNotifications/Notification.php';
require_once dirname(__FILE__) . '/UmiNotifications/Template.php';

use UmiCms\Service;

class UmiSpecInstaller
{
    const guide_value_guid = 'guid';
    const guide_value_name = 'name';

    /**
     * @var umiHierarchyTypesCollection
     */
    protected $umiHierarchyTypesCollection;

    /**
     * @var umiObjectTypesCollection
     */
    protected $umiObjectTypesCollection;

    /**
     * @var umiFieldsCollection
     */
    protected $umiFieldsCollection;

    /**
     * @var umiFieldTypesCollection
     */
    protected $umiFieldTypesCollection;

    /**
     * @var umiObjectsCollection
     */
    protected $umiObjectsCollection;

    public function __construct()
    {
        $this->umiHierarchyTypesCollection = umiHierarchyTypesCollection::getInstance();
        $this->umiObjectTypesCollection = umiObjectTypesCollection::getInstance();
        $this->umiFieldsCollection = umiFieldsCollection::getInstance();
        $this->umiFieldTypesCollection = umiFieldTypesCollection::getInstance();
        $this->umiObjectsCollection = umiObjectsCollection::getInstance();
    }

    public function getRootGuidesObjectTypeId()
    {
        return $this->umiObjectTypesCollection->getTypeIdByGUID('root-guides-type');
    }

    public function getRootPagesObjectTypeId()
    {
        return $this->umiObjectTypesCollection->getTypeIdByGUID('root-pages-type');
    }

    public function getContentPageObjectTypeId()
    {
        return $this->umiObjectTypesCollection->getTypeIdByGUID('content-page');
    }

    /**
     * @param $name
     * @param string $method
     * @return bool|umiObjectType
     */
    public function getObjectTypeByHierarchyTypeName($name, $method = '')
    {
        $typeId = method_exists($this->umiObjectTypesCollection, 'getTypeIdByHierarchyTypeName') ? $this->umiObjectTypesCollection->getTypeIdByHierarchyTypeName($name, $method) : $this->umiObjectTypesCollection->getBaseType($name, $method);

        if (!$typeId) {
            return false;
        }

        $type = $this->umiObjectTypesCollection->getType($typeId);

        return $type instanceof umiObjectType ? $type : false;
    }

    public function getObjectTypeByGUID($guid)
    {
        $type = $this->umiObjectTypesCollection->getTypeByGUID($guid);

        return $type instanceof umiObjectType ? $type : false;
    }

    public function getObjectTypeIdByGUID($guid)
    {
        return $this->umiObjectTypesCollection->getTypeIdByGUID($guid);
    }

    public function createObjectTypeByGUID(UmiSpecInstallerObjectType $objectType, $bUpdate = true)
    {
        $guid = $objectType->getGuid();

        if (!$guid) {
            throw new publicException('У класса ' . $objectType->getName() . ' не указан guid');
        }

        $type = $this->getObjectTypeByGUID($guid);

        if (!$type instanceof umiObjectType) {
            $typeId = $this->umiObjectTypesCollection->addType($objectType->getParentId(), $objectType->getName());
            if (!$typeId) {
                throw new publicException('Ошибка при создании класса ' . $objectType->getName());
            }

            $type = $this->umiObjectTypesCollection->getType($typeId);

            if (!$type instanceof umiObjectType) {
                throw new publicException('Ошибка при получении класса ' . $typeId);
            }

            $type->setGUID($objectType->getGuid());
            $type->setIsGuidable($objectType->isIsGuidable());
            $type->commit();

            $this->umiObjectTypesCollection->clearCache();
        } else {
            if ($bUpdate) {
                $type->setName($objectType->getName());
                $type->setIsGuidable($objectType->isIsGuidable());
                $type->commit();
            }
        }

        return $type instanceof umiObjectType ? $type : false;
    }

    public function createObjectTypeByHierarchyTypeId(UmiSpecInstallerObjectType $objectType, $bUpdate = true)
    {
        $hierarchyTypeId = $objectType->getHierarchyTypeId();
        if (!$hierarchyTypeId) {
            throw new publicException('У класса ' . $objectType->getName() . ' не указан иерархический тип данных');
        }

        $type = false;

        $typeId = $this->umiObjectTypesCollection->getTypeIdByHierarchyTypeId($hierarchyTypeId, true);
        if ($typeId) {
            $type = $this->umiObjectTypesCollection->getType($typeId);
        }

        if (!$type instanceof umiObjectType) {
            $typeId = $this->umiObjectTypesCollection->addType($objectType->getParentId(), $objectType->getName());
            if (!$typeId) {
                throw new publicException('Ошибка при создании класса ' . $objectType->getName());
            }

            $type = $this->umiObjectTypesCollection->getType($typeId);

            if (!$type instanceof umiObjectType) {
                throw new publicException('Ошибка при получении класса ' . $typeId);
            }

            $type->setHierarchyTypeId($hierarchyTypeId);
            $type->setIsGuidable($objectType->isIsGuidable());
            $type->setIsPublic($objectType->isIsPublic());
            $type->commit();

            $this->umiObjectTypesCollection->clearCache();
            $this->umiHierarchyTypesCollection->clearCache();
        } else {
            if ($bUpdate) {
                $type->setName($objectType->getName());
                $type->setIsGuidable($objectType->isIsGuidable());
                $type->setIsPublic($objectType->isIsPublic());
                $type->commit();
            }
        }

        return $type instanceof umiObjectType ? $type : false;
    }

    public function createObjectTypeGroups($arGroups, umiObjectType $type, umiObjectType $baseType = null)
    {
        foreach ($arGroups as $arGroup) {
            $this->createObjectTypeGroup($arGroup, $type, $baseType);
        }

        $subTypesList = $this->umiObjectTypesCollection->getSubTypesList($type->getId());

        if ($subTypesList) {
            foreach ($subTypesList as $subTypeId) {
                $subType = $this->umiObjectTypesCollection->getType($subTypeId);

                if (!$subType instanceof umiObjectType) {
                    continue;
                }

                $this->createObjectTypeGroups($arGroups, $subType, $type);
            }
        }
    }

    public function createObjectTypeGroup(UmiSpecInstallerGroup $oGroup, umiObjectType $type, umiObjectType $baseType = null)
    {
        $name = $oGroup->getName();

        if (!$name) {
            throw new publicException('Не указан идентификатор группы');
        }

        $title = $oGroup->getTitle();
        $active = $oGroup->isActive();
        $visible = $oGroup->isVisible();
        $tip = $oGroup->getTip();

        $group = $type->getFieldsGroupByName($name, true);

        if (!$group instanceof umiFieldsGroup) {
            $groupId = $type->addFieldsGroup($name, $title, $active, $visible, $tip);

            if (!$groupId) {
                throw new publicException('Не удалось создать группу полей ' . $title . ' (' . $name . ') в типе данных ' . $type->getId());
            }

            $group = $type->getFieldsGroup($groupId);

            if (!$group instanceof umiFieldsGroup) {
                throw new publicException('Не удалось создать группу полей ' . $title . ' (' . $name . ') в типе данных ' . $type->getId());
            }
        }

        if ($title !== null && $group->getTitle() != $title) {
            $group->setTitle($title);
        }

        if ($group->getIsActive() != $active) {
            $group->setIsActive($active);
        }

        if ($group->getIsVisible() != $visible) {
            $group->setIsVisible($visible);
        }

        if ($group->getIsLocked()) {
            $group->setIsLocked(false);
        }

        if ($group->getIsUpdated()) {
            $group->commit();
        }

        $fields = $oGroup->getFields();

        if ($fields) {
            foreach ($fields as $arField) {
                $this->createObjectTypeField($arField, $type, $group, $baseType);
            }
        }
    }

    public function createObjectTypeField(UmiSpecInstallerField $oField, umiObjectType $type, umiFieldsGroup $group, umiObjectType $baseType = null)
    {
        $name = $oField->getName();

        if (!$name) {
            throw new publicException('Не указан идентификатор поля');
        }

        $title = $oField->getTitle();
        $typeId = $oField->getTypeId();
        $visible = $oField->isVisible();
        $locked = $oField->isLocked();

        $bAttach = true;
        $bCreate = false;

        $fieldId = $type->getFieldId($name);

        if (!$fieldId) {
            if (!$baseType) {
                $fieldId = $this->umiFieldsCollection->addField($name, $title, $typeId, $visible, $locked);

                if (!$fieldId) {
                    throw new publicException('Ошибка при создании поля ' . $title);
                }

                $bCreate = true;
            } else {
                $fieldId = $baseType->getFieldId($name);
            }
        } else {
            $bAttach = false;
        }

        if (!$fieldId) {
            throw new publicException('Не найдено поле ' . $name);
        }

        $field = $this->umiFieldsCollection->getField($fieldId);

        if (!$field instanceof umiField) {
            throw new publicException('Не найдено поле ' . $fieldId);
        }

        if ($field->getTitle() != $title) {
            $field->setTitle($title);
        }

        if ($field->getFieldTypeId() != $typeId) {
            $field->setFieldTypeId($typeId);
        }

        if ($field->getIsVisible() != $visible) {
            $field->setIsVisible($visible);
        }

        $field->setImportanceStatus($visible);

        if ($field->getIsLocked() != $locked) {
            $field->setIsLocked($locked);
        }

        $required = $oField->isRequired();

        if ($field->getIsRequired() != $required) {
            $field->setIsRequired($required);
        }

        $tip = $oField->getTip();

        if ($field->getTip() != $tip) {
            $field->setTip($tip);
        }

        $guideId = $oField->getGuideId();

        if ($field->getGuideId() != $guideId) {
            $field->setGuideId($guideId);
        }

        $inFilter = $oField->isInFilter();
        if ($inFilter !== null) {
            $field->setIsInFilter($inFilter);
        } elseif ($bCreate) {
            $field->setIsInFilter(false);
        }

        $inSearch = $oField->isInSearch();
        if ($inSearch !== null) {
            $field->setIsInSearch($inSearch);
        }

        if (!$field->getIsInheritable()) {
            $field->setIsInheritable(true);
        }

        if ($field->getIsUpdated()) {
            $field->commit();
        }

        if ($bAttach) {
            $group->attachField($fieldId);
        }
    }

    public function getFieldTypeId($type, $isMultiple = false)
    {
        static $types = array();

        $hash = $type;

        if ($isMultiple) {
            $hash .= '_multiple';
        }

        if (!isset($types[$hash])) {
            $fieldType = $this->umiFieldTypesCollection->getFieldTypeByDataType($type, $isMultiple);

            if (!$fieldType instanceof umiFieldType) {
                throw new publicException('Не найдено поле с типом ' . $type);
            }

            $types[$hash] = $fieldType->getId();
        }

        return $types[$hash];
    }

    public function moveFieldToGroup(umiObjectType $objectType, $groupName, $fieldName)
    {
        $group = $objectType->getFieldsGroupByName($groupName);
        if (!$group instanceof umiFieldsGroup) {
            throw new publicException('Не найдена группа полей ' . $groupName);
        }

        $fieldId = $objectType->getFieldId($fieldName);
        if (!$fieldId) {
            throw new publicException('Не найдено поле ' . $fieldName);
        }

        foreach ($objectType->getFieldsGroupsList(true) as $fieldsGroup) {
            if (!$fieldsGroup instanceof umiFieldsGroup) {
                continue;
            }

            if (!$fieldsGroup->hasFieldWithName($fieldName)) {
                continue;
            }

            $fieldsGroup->detachField($fieldId);
            break;
        }

        $group->attachField($fieldId);

        return true;
    }

    public function createGuideValues($typeId, $values = array())
    {
        if (!$values) {
            return false;
        }

        $type = $this->umiObjectTypesCollection->getType($typeId);

        if (!$type instanceof umiObjectType) {
            return false;
        }

        $arValues = array();

        foreach ($values as $value) {
            $guid = getArrayKey($value, self::guide_value_guid);

            if (!$guid) {
                continue;
            }

            $arValues[$guid] = getArrayKey($value, self::guide_value_name);
        }

        $sel = new selector('objects');
        $sel->types('object-type')->id($typeId);
        $sel->where('guid')->equals(array_keys($arValues));

        $guideValues = array();

        foreach ($sel as $object) {
            if (!$object instanceof umiObject) {
                continue;
            }

            $guid = $object->getGUID();

            if (!$guid) {
                continue;
            }

            $guideValues[$guid] = $object->getId();
        }

        foreach ($arValues as $guid => $name) {
            if (isset($guideValues[$guid])) {
                $objectId = $guideValues[$guid];
            } else {
                $objectId = $this->umiObjectsCollection->addObject($name, $typeId);
            }

            $object = $this->umiObjectsCollection->getObject($objectId);

            if (!$object instanceof umiObject) {
                continue;
            }

            $object->setGUID($guid);
            $object->setName($name);
            $object->commit();
        }

        return true;
    }

    public function createGuideValueArray($guid, $name)
    {
        return array(
            self::guide_value_guid => $guid,
            self::guide_value_name => $name
        );
    }

    /**
     * @param UmiSpecInstallerHierarchyType $hierarchyType
     * @return umiHierarchyType
     * @throws publicException
     */
    public function createHierarchyType(UmiSpecInstallerHierarchyType $hierarchyType)
    {
        $type = $this->umiHierarchyTypesCollection->getTypeByName($hierarchyType->getModule(), $hierarchyType->getMethod());
        if (!$type instanceof umiHierarchyType) {
            $typeId = $this->umiHierarchyTypesCollection->addType($hierarchyType->getModule(), $hierarchyType->getTitle(), $hierarchyType->getMethod());
            if (!$typeId) {
                throw new publicException('Ошибка при создании иерархического типа данных ' . $hierarchyType->getModule() . ':' . $hierarchyType->getMethod());
            }

            $type = $this->umiHierarchyTypesCollection->getType($typeId);
            if (!$type instanceof umiHierarchyType) {
                throw new publicException('Не найден иерархический тип данных ' . $typeId);
            }

            $this->umiHierarchyTypesCollection->clearCache();
        } else {
            $type->setTitle($hierarchyType->getTitle());
            $type->commit();
        }

        return $type;
    }

    public function createGuideObjectType($name, $guid)
    {
        $objectType = $this->createObjectTypeByGUID(new UmiSpecInstallerObjectType($name, $guid, $this->getRootGuidesObjectTypeId(), true));
        if (!$objectType instanceof umiObjectType) {
            throw new publicException('Ошибка при создании справочника ' . $name);
        }
        $objectType->setIsGuidable(true);
        $objectType->setIsPublic(true);
        $objectType->commit();

        return $objectType;
    }

    public function attachFieldToGroup($fieldName, UmiSpecInstallerGroup $installerGroup, umiObjectType $objectType)
    {
        $fieldId = $objectType->getFieldId($fieldName);
        if (!$fieldId) {
            throw new publicException('В типе данных ' . $objectType->getId() . ' не найдено поле ' . $fieldName);
        }

        $newGroup = $objectType->getFieldsGroupByName($installerGroup->getName());
        if (!$newGroup instanceof umiFieldsGroup) {
            throw new publicException('В типе данных ' . $objectType->getId() . ' не найдено группа ' . $installerGroup->getName());
        }

        foreach ($objectType->getFieldsGroupsList() as $group) {
            if (!$group instanceof umiFieldsGroup) {
                continue;
            }

            $field = $group->getFieldByName($fieldName);
            if (!$field instanceof umiField) {
                continue;
            }

            $group->detachField($fieldId);
            $group->commit();
        }

        $newGroup->attachField($fieldId);
        $newGroup->commit();
    }

    public function createWebformsAddressObject($formId, $name, $description, $list, $bUpdate = false)
    {
        $objectTypeId = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName('webforms', 'address');
        if (!$objectTypeId) {
            throw new publicException('Не найден тип данных webforms:address');
        }

        $sel = new selector('objects');
        $sel->types('object-type')->id($objectTypeId);
        $sel->where('form_id')->equals($formId);
        $sel->limit(0, 1);
        $sel->order('id')->asc();
        $result = $sel->result();

        if ($result && !$bUpdate) {
            return true;
        }

        $object = false;
        if ($result) {
            $object = getArrayKey($result, 0);
        }

        if (!$object instanceof umiObject) {
            $objectId = umiObjectsCollection::getInstance()->addObject($name, $objectTypeId);
            if (!$objectId) {
                throw new publicException('Ошибка при создании объекта ' . $name);
            }

            $object = umiObjectsCollection::getInstance()->getObject($objectId);
            if (!$object instanceof umiObject) {
                throw new publicException('Ошибка при получении объекта ' . $objectId);
            }
        }

        $object->setName($name);
        $object->setValue('form_id', $formId);
        $object->setValue('address_description', $description);
        $object->setValue('address_list', $list);
        $object->commit();

        return true;
    }

    public function createWebformsTemplate(UmiSpecInstallerWebformsTemplate $webformsTemplate, $bUpdate = false)
    {
        $objectTypeId = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName('webforms', 'template');

        $sel = new selector('objects');
        $sel->types('object-type')->id($objectTypeId);
        $sel->where('form_id')->equals($webformsTemplate->getFormId());
        $sel->order('id')->asc();

        $object = getArrayKey($sel->result(), 0);
        if ($object instanceof umiObject && !$bUpdate) {
            return true;
        }

        if (!$object instanceof umiObject) {
            $objectId = umiObjectsCollection::getInstance()->addObject($webformsTemplate->getName(), $objectTypeId);
            if (!$objectId) {
                throw new publicException('Ошибка при создании объекта ' . $webformsTemplate->getName());
            }

            $object = umiObjectsCollection::getInstance()->getObject($objectId);
            if (!$object instanceof umiObject) {
                throw new publicException('Ошибка при получении объекта ' . $objectId);
            }
        }

        $oldMode = umiObjectProperty::$IGNORE_FILTER_INPUT_STRING;
        umiObjectProperty::$IGNORE_FILTER_INPUT_STRING = true;

        $object->setName($webformsTemplate->getName());
        $object->setValue('form_id', $webformsTemplate->getFormId());
        $object->setValue('from_email_template', $webformsTemplate->getFromEmail());
        $object->setValue('from_template', $webformsTemplate->getFrom());
        $object->setValue('subject_template', $webformsTemplate->getSubject());
        $object->setValue('master_template', $webformsTemplate->getMaster());
        $object->setValue('posted_message', $webformsTemplate->getPostedMessage());
        $object->commit();

        umiObjectProperty::$IGNORE_FILTER_INPUT_STRING = $oldMode;

        return true;
    }

    public function setTypeIdRelation($sourceName, $oldTypeId, $newTypeId)
    {
        $umiImportRelations = umiImportRelations::getInstance();
        if (!$umiImportRelations instanceof umiImportRelations) {
            throw new publicException('Не найден класс umiImportRelations');
        }

        $sourceId = $umiImportRelations->getSourceId($sourceName);
        if (!$sourceId) {
            throw new publicException('Не найден sourceId ' . $sourceName);
        }

        $umiImportRelations->setTypeIdRelation($sourceId, $oldTypeId, $newTypeId);
    }

    public function createDeliveryObject(umiObjectType $objectType, $className, $name = false)
    {
        if (!$name) {
            $name = $objectType->getName();
        }

        $baseDeliveryTypeObjectTypeId = $this->getObjectTypeIdByGUID('emarket-deliverytype');

        $sel = new selector('objects');
        $sel->types('object-type')->id($baseDeliveryTypeObjectTypeId);
        $sel->where('delivery_type_guid')->equals($objectType->getGUID());
        $sel->order('id')->asc();
        $sel->limit(0, 1);

        $object = getArrayKey($sel->result(), 0);

        if (!$object instanceof umiObject) {
            $umiObjectsCollection = umiObjectsCollection::getInstance();

            $objectId = $umiObjectsCollection->addObject($name, $baseDeliveryTypeObjectTypeId);
            if (!$objectId) {
                throw new publicException('Ошибка при создании объекта для доставки ' . $className);
            }

            $object = $umiObjectsCollection->getObject($objectId);
            if (!$object instanceof umiObject) {
                throw new publicException('Не найден объект ' . $objectId);
            }
        }

        $object->setName($name);
        $object->setValue('class_name', $className);
        $object->setValue('delivery_type_id', $objectType->getId());
        $object->setValue('delivery_type_guid', $objectType->getGUID());
        $object->commit();

        return $objectType;
    }

    public function createNotification(UmiSpecInstallerUmiNotificationsNotification $notification, $bUpdate = false)
    {
        $mailNotifications = Service::MailNotifications();
        if (!$mailNotifications instanceof MailNotificationsCollection) {
            throw new publicException('Не найден класс MailNotificationsCollection');
        }
        $mailNotificationsMap = $mailNotifications->getMap();

        $mailNotification = $mailNotifications->getByName($notification->getName());
        if (!$mailNotification instanceof MailNotification) {
            $mailNotification = $mailNotifications->create([
                $mailNotificationsMap->get('LANG_ID_FIELD_NAME') => $notification->getLangId(),
                $mailNotificationsMap->get('DOMAIN_ID_FIELD_NAME') => $notification->getDomainId(),
                $mailNotificationsMap->get('NAME_FIELD_NAME') => $notification->getName(),
                $mailNotificationsMap->get('MODULE_FIELD_NAME') => $notification->getModule(),
            ]);
        }

        $mailTemplates = Service::MailTemplates();
        if (!$mailTemplates instanceof MailTemplatesCollection) {
            throw new publicException('Не найден класс MailTemplatesCollection');
        }
        $mailTemplatesMap = $mailTemplates->getMap();
        foreach ($notification->getTemplates() as $template) {
            $mailTemplate = $mailTemplates->getByName($template->getName());
            if (!$mailTemplate instanceof MailTemplate) {
                $mailTemplates->create([
                    $mailTemplatesMap->get('NOTIFICATION_ID_FIELD_NAME') => $mailNotification->getId(),
                    $mailTemplatesMap->get('NAME_FIELD_NAME') => $template->getName(),
                    $mailTemplatesMap->get('TYPE_FIELD_NAME') => $template->getType(),
                    $mailTemplatesMap->get('CONTENT_FIELD_NAME') => $template->getContent(),
                ]);
            } else {
                if (!is_null($template->getType())) {
                    $mailTemplate->setType($template->getType());
                }
                if ($bUpdate) {
                    $mailTemplate->setContent($template->getContent());
                }

                $mailTemplate->commit();
            }
        }

        return true;
    }
}