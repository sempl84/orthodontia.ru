<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2019, Evgenii Ioffe
 */
abstract class UmiSpecInstallerMenuItem
{
    protected $isActive;
    protected $link;
    protected $name;
    protected $rel;
    protected $children = array();
    protected $additionalData = array();

    public function addChildren(UmiSpecInstallerMenuItem $children)
    {
        $this->children[] = $children;
    }

    public function prepareArray()
    {
        $return = array(
            'isactive' => $this->isActive ? 1 : 0,
            'name' => $this->name,
            'link' => $this->link,
            'rel' => $this->rel,
        );

        if (is_array($this->additionalData)) {
            $return = array_merge($return, $this->additionalData);
        }

        if ($this->children) {
            $arChildren = array();
            foreach ($this->children as $children) {
                if (!$children instanceof UmiSpecInstallerMenuItem) {
                    continue;
                }
                $arChildren[] = $children->prepareArray();
            }
            if(count($arChildren)) {
                $return['children'] = $arChildren;
            }
        }

        return $return;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }
}