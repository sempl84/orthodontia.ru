<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class UmiSpecInstallerGroup
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $title;
    /**
     * @var bool
     */
    private $active = true;
    /**
     * @var bool
     */
    private $visible = true;
    /**
     * @var string
     */
    private $tip = '';
    /**
     * @var array
     */
    private $fields = array();
    /**
     * @var bool
     */
    private $fieldInheritName = false;
    private $fieldDefaultVisible = null;

    public function __construct($name, $title = null)
    {
        $this->name = $name;
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return boolean
     */
    public function isVisible()
    {
        return $this->visible;
    }

    /**
     * @param boolean $visible
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    }

    /**
     * @return string
     */
    public function getTip()
    {
        return $this->tip;
    }

    /**
     * @param string $tip
     */
    public function setTip($tip)
    {
        $this->tip = $tip;
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    public function addField(UmiSpecInstallerField $field)
    {
        if ($this->fieldInheritName) {
            if (strpos($field->getName(), $this->getName()) === false) {
                $field->setName($this->getName() . '_' . $field->getName());
            }
        }

        if(!is_null($this->fieldDefaultVisible)) {
            $field->setVisible($this->fieldDefaultVisible);
        }

        $this->fields[] = $field;
    }

    /**
     * @param boolean $fieldInheritName
     */
    public function setFieldInheritName($fieldInheritName)
    {
        $this->fieldInheritName = $fieldInheritName;
    }

    public function setFieldDefaultVisible($fieldDefaultVisible)
    {
        $this->fieldDefaultVisible = boolval($fieldDefaultVisible);
    }
}