<?php
session_start();

header("HTTP/1.1 200 OK");
header('Content-Type: text/html; charset=UTF-8');

// ini_set("display_errors", "1");
// ini_set("display_startup_errors", "1");
// ini_set("error_reporting", E_ALL);
ini_set("display_errors", "0");
ini_set("error_reporting", 0);

$gal_sizes = "800*400";
$event_id = 2540;
$event_name = "Семинар Стюарта Фроста";
$event_date = "25 ноября 2019";
// $event_type = "Практический семинар";
$event_short = "Искусство ортодонтии.<br /><span>Пациент – на первом месте";
$event_title = "Семинар «Искусство ортодонтии. Пациент – на первом месте» Стюарта Фроста 25 ноября 2019 года";
$uploaddir = "https://orthodontia.ru/frost2019/";
$mktimes = array(1 => "10:00 - 11:30", 2 => "12:00 - 13:30", 3 => "14:30 - 16:00", 4 => "16:30 - 18:00");
$places_count = 300;
$event_city = "Москва";
$event_place = "Москва,<br />Отель «Ренессанс Монарх»";
$place_address = "Ленинградский пр-т, д. 31А, стр. 1";
$mkcounts = explode("/", file_get_contents("mkcounts.txt"));
$programm = array(
0 => array(
"time" => "9.00 - 10.00",
"type" => 3,
"theme" => "Регистрация, welcome coffee"
),
1 => array(
"time" => "10.00 - 11.30",
"type" => 1,
"theme" => "Переход на новый уровень завершения клинических случаев
<small>Стюарт Фрост раскроет 5 ключевых факторов, влияющих на завершение случаев с наилучшими для пациента клиническими и эстетическими результатами</small>"
),
2 => array(
"time" => "11.30 – 12.00",
"type" => 2,
"theme" => "Кофе-брейк"
),
3 => array(
"time" => "12.00 – 13.30",
"type" => 1,
"theme" => "Взгляд в будущее: как определить наилучший достижимый результат лечения
еще до установки брекетов
<small>Индивидуальный подход: использование 5 ключевых факторов для визуализации завершенного клинического случая еще до установки брекетов. Учимся определять, что возможно для пациента, и что станет лучшим результатом.</small>"
),
4 => array(
"time" => "13.30 – 14.30",
"type" => 3,
"theme" => "Обед"
),
5 => array(
"time" => "14.30 – 16.00",
"type" => 1,
"theme" => "Красивое завершение. Как сделать так, чтобы пациент был счастлив, а врач –
удовлетворен результатом
<small>Фокус на опыте пациента от первого посещения клиники до того момента, как он увидел в зеркале свою улыбку в конце лечения. Применение моделирования зубного ряда, контурирования мягких тканей и других практических техник, чтобы завершать клинические случаи на новом уровне.</small>"
),
6 => array(
"time" => "16.00 – 16.30",
"type" => 2,
"theme" => "Кофе-брейк"
),
7 => array(
"time" => "16.30 – 17.30",
"type" => 1,
"theme" => "Репутация и имидж врача-ортодонта
<small>Влияние опыта и мнения пациентов на репутацию врача. Использование ваших эстетически красивых завершенных случаев для формирования имиджа и роста популярности врача в социальных сетях и других каналах коммуникаций.</small>"
),
8 => array(
"time" => "17.30 – 18.00",
"type" => 3,
"theme" => "Сессия вопросов и ответов"
)
);
$prices = array(
	1 => array(	"name" => "при регистрации и оплате <span>до 7 октября</span>",
				"price" => 17000,
				"date_before" => "2019-10-07",
				"date_after" => ""
	),
	2 => array(	"name" => "при регистрации и оплате после 7 октября",
				"price" => 19000,
				"date_before" => "",
				"date_after" => "2019-10-07"
	)
);
$speakers = array(
	0 => array(	"name" => "Стюарт Фрост",
				"about" => "Его можно назвать поистине легендарным и всемирно известным.
				Д-р Фрост стоял у истоков пассивного самолигирования: его опыт использования Damon System насчитывает более 20 лет.",
				"image" => "frost.png",
				"more" => "Доктор Фрост получил образование врача-стоматолога в Школе Стоматологии Тихоокеанского университета в 1992 году. 5 лет работал стоматологом общей практики – 2 года вместе со своим отцом, и 3 года – в своем собственном кабинете. После завершения аспирантуры по специализации «Дисфункция височно-нижнечелюстного сустава» в Университете Рочестера, штат Нью-Йорк, он продолжил свое образование, завершив двухлетний курс по ортодонтии и зубочелюстной ортопедии. Защитил диссертацию на тему: «Аномалии роста и развития зубочелюстной системы: соединительной ткани, швов и ВНЧС»
В настоящее время д-р Фрост ведет практику в городе Меса штата Аризона. По совместительству работает клиническим профессором в Тихоокеанском Университете на кафедре ортодонтии, регулярно выступает на различных ортодонтических Форумах, проводит авторские семинары и офис-курсы в своей клинике.
Доктор Стюарт Фрост - один из лучших спикеров Ormco.
Его можно назвать поистине легендарным и всемирно известным. Д-р Фрост стоял у истоков пассивного самолигирования: его опыт использования Damon System насчитывает более 20 лет. Д-р Фрост работал вместе с такими именитыми ортодонтами, как Дуайт Дэймон и Том Питтс, став одним из первых врачей, начавших применять Damon System в своей практике. Мало кто знает, что практически все люди, которых вы видите в промо-материалах Ormco - не фото-модели, а реальные пациенты, которых лечил д-р Фрост.",
	)
);
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?=$event_title;?></title>
		<meta http–equiv="Content–Type" content="text/html; charset=UTF–8">
		<link rel="stylesheet" media="screen" type="text/css" href="main.css">
		<meta name="keywords" content="">
		<meta name="description" content="">
		<meta name="HandheldFriendly" content="true">
		<meta name="MobileOptimized" content="width">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="/favicon.gif" type="image/x-icon">
		<link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<!--<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800&amp;subset=cyrillic" rel="stylesheet">-->
		<script src="js/jquery.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="js/ajaxupload.3.5.js" type="text/javascript"></script>
		<script src="js/swiper.min.js" type="text/javascript"></script>
		<script src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyBoDhMbGeceqxv4_7SROd3YL1-0C-SyBGo" type="text/javascript"></script>
		<script>
			var userid = "";
			function noplaces() {
				$('.mask').fadeIn(300);
				$('#noplaces').fadeIn(300);
			}
			function remember_pass() {
				var e_email = $("#e_email").val();
				if (isEmail(e_email)) {
					$.ajax({
						url:"https://orthodontia.ru/udata/emarket/forget_pas/" + e_email + "/.json",
						xhrFields: {
						  withCredentials: true
						},
						dataType: "json",
						success:function(data) {
							if (data.status == "success") {
								$("#rp_ok").fadeIn(300);
								setTimeout(function() {$("#rp_ok").fadeOut(300)}, 15000);
							}
							else {
								$("#rp_fail").text("Пользователь не найден в базе, попробуйте еще раз").fadeIn(300);
								setTimeout(function() {$("#rp_fail").fadeOut(300)}, 15000);
							}
						}
					});
				}
				else {
					$("#rp_fail").text("Укажите свой email!").fadeIn(300);
					setTimeout(function() {$("#rp_fail").fadeOut(300)}, 15000);
				}
			}
			function countdown() {
				var mcl1 = $("#mcl1").val();
				var mcl2 = $("#mcl2").val();
				$.ajax({
					type: "POST",
					url: "xp_countdown.php",
					timeout: 5000,
					data: "mcl1=" + mcl1 + "&mcl2=" + mcl2,
					success: function(data) {
					},
					error: function(data) {
					}
				});
			}
			function writedata() {
				var alldata = "";
				$('form input').each(function() {
					if ($(this).attr("id") != undefined)
						alldata += "&" + $(this).attr("id") + "=" + $(this).val();
				});
				$('form select').each(function() {
					if ($(this).attr("id") != undefined)
						alldata += "&" + $(this).attr("id") + "=" + $(this).val();
				});
				$.ajax({
					type: "POST",
					url: "xp_write.php",
					timeout: 5000,
					data: "alldata=" + alldata,
					success: function(data) {
					},
					error: function(data) {
					}
				});
			}
			function setdiscount(discount, element, chk) {
				$(".dscnt").not("#" + element).prop("checked", false);
				var pr = $("#regularprice").val();
				var eventid = $("#events").val();
				if (chk) {
					$("#discount").text(discount);
					$(".discount").fadeIn(300);
					pr = pr - parseInt(pr*(discount/100));
					// указываем кол-во звезд, которые можно получить
					$.ajax({					
						url:"https://orthodontia.ru/udata/custom/outputEventDiscount/" + eventid + "/.json",
						dataType: "json",
						success:function(data) {
							var bonuses = 0;
							var freez = 0;
							if (typeof data.item !== "undefined") {
								bonuses = data.item[0].loyalty_bonus;
								freez = data.item[0].freez_ormco_star;
								for (var key in data.item) {
									if (data.item[key].id == "discount_" + element) {
										bonuses = data.item[key].loyalty_bonus;
										freez = data.item[key].freez_ormco_star;
									}
								}
							}
							$("#bonuses").text(bonuses);
							$("#potential_ormco_star").val(bonuses);
							$("#freez_ormco_star").val(freez);
						}
					});
				}
				else {
					$(".discount").fadeOut(300);
					$(".newform_upload").fadeOut(300);
					$("#uploaded").fadeOut(300);
					// указываем кол-во звезд, которые можно получить
					$.ajax({					
						url:"https://orthodontia.ru/udata/custom/outputEventDiscount/" + eventid + "/.json",
						dataType: "json",
						success:function(data) {
							var bonuses = 0;
							var freez = 0;
							if (typeof data.item !== "undefined") {
								bonuses = data.item[0].loyalty_bonus;
								freez = data.item[0].freez_ormco_star;
							}
							$("#bonuses").text(bonuses);
							$("#potential_ormco_star").val(bonuses);
							$("#freez_ormco_star").val(freez);
						}
					});
				}
				$("#finalprice").val(pr);
				$("#finalprice_txt").text(pr + " р.");
			}
			function addnewyur() {
				$(".yurs").fadeOut(300);
				$(".newform_reg").fadeOut(300);
				$(".addyur").fadeOut(300, function() {
					$(".addyurform1").fadeIn(300);
				});
			}
			function addyurajax() {
				var tosend = "id=" + userid;
				$('.addyurform input').each(function(index) {
					var did = $(this).attr("id");
					if (did == "yurname")
						did = "name";
					if (did == "yuremail")
						did = "email";
					var dval = $(this).val();
					tosend += "&data[new][" + did + "]=" + dval;
				});
				$('.addyurform textarea').each(function(index) {
					var did = $(this).attr("id");
					var dval = $(this).val();
					tosend += "&data[new][" + did + "]=" + dval;
				});
				$.ajax({
					url:"https://orthodontia.ru/udata/emarket/add_legal_item/.json?" + tosend,
					xhrFields: {
					  withCredentials: true
					},
					dataType: "json",
					success:function(data) {
						if (data.result != 0) {
							$(".addyurform").fadeOut(300).promise().done(function() {
								// добавляем новое юр. лицо в select
								$('#yur').append($('<option>', { 
										value: data.result,
										text : $("#yurname").val()
								}));
								$(".newform_reg").fadeIn(300);
								$(".addyur").fadeIn(300);
								$(".yurs").fadeIn(300);
							});
						}
						else {
							$(".addyurform").fadeOut(300, function() {
								$(".newform_reg").fadeIn(300);
								$(".addyur").fadeIn(300);
								$(".yurs").fadeIn(300);
							});
							alert("Новое юр. лицо не добавлено! Проверьте данные!");
						}
					}
				});
			}
			function showyur(n) {
				var foo = 0;
				if (n == 1) {
					$(".addyurform1 .ness").each(function() {
						if ($(this).val() == '') {
							if (foo == 0) {
								$('html,body').animate({scrollTop: $(this).offset().top - 100}, 500);
							}
							$(this).addClass('redfld');
							foo = 1;
						}
						else {
							$(this).removeClass('redfld');
						}
					});
				}
				if (foo == 0) {
					$(".addyurform" + (3 - n)).fadeOut(300, function() {
						$(".addyurform" + n).fadeIn(300);
					});
				}
			}
			function showtab(tab) {
				if (tab == 1)
					$("#yur").attr("disabled", "disabled");
				else
					$("#yur").removeAttr("disabled");
				$(".tab" + (3 - tab)).fadeOut(300);
				$(".tab" + tab).fadeIn(300);
				$(".tabbut" + (3 - tab)).removeClass("active");
				$(".tabbut" + tab).addClass("active");
			}
			function showpart(part) {
				if (chkform(2)) {
					if (userid == "") {
						$.ajax({
							url:"https://orthodontia.ru/udata/emarket/logout_user/.json",
							xhrFields: {
							  withCredentials: true
							},
							dataType: "json",
							success:function(data) {
								// авторизация на сайте
								var tosend = "id=" + userid;
								$('.part1 input').each(function(index) {
									var did = $(this).attr("id");
									var dval = $(this).val();
									tosend += "&data[new][" + did + "]=" + dval;
								});
								$('.part1 select').each(function(index) {
									var did = $(this).attr("id");
									var dval = $(this).val();
									tosend += "&data[new][" + did + "]=" + dval;
								});
								$.ajax({
									url:"https://orthodontia.ru/udata/users/lpreg/.json?" + tosend,
									dataType: "json",
									success:function(data) {
										$(".f_anim").css("opacity", 0);
										if (data.status == "successful") {
											// пользователь зарегистрирован
											user_id = data.user_id;
											// отправляем данные в GA
											var utype = $("#who").val();
											var usertype = 'not a doctor';
											if (utype == "Ортодонт")
												usertype = "orthodontist";
											if (utype == "Хирург")
												usertype = "surgeon";
											if (utype == "Другая специализация")
												usertype = "other";
											console.log("usertype: " + usertype);
											ga('send', 'event', 'form', 'registration', usertype);
											yaCounter23063434.reachGoal('UserRegistration');
											$.ajax({
												url:"https://orthodontia.ru/udata/emarket/logout_user/.json",
												xhrFields: {
												  withCredentials: true
												},
												dataType: "json",
												success:function(data) {
													var newemail = $("#email").val();
													var newpassword = $("#password").val();
													$.ajax({
														xhrFields: {
														  withCredentials: true
														},
														dataType: "json",
														url:"https://orthodontia.ru/udata/emarket/get_user_info/" + newemail + "/" + newpassword + "/.json",
														success:function(data) {
															$(".part" + (3 - part)).fadeOut(300);
															$(".part" + part).fadeIn(300);
															$(".partbut" + (3 - part)).addClass("hidden");
															$(".partbut" + part).removeClass("hidden");
															$(".notfound").hide(0);
														}
													});
												}
											});
										}
										else {
											// неудачно, рисуем ошибку
											$("#form_error").html(data.result);
											$(".mask1").fadeIn(300);
											$(".form_error").fadeIn(300);
										}
									}
								});
							}
						});
					}
					else {
						$(".part" + (3 - part)).fadeOut(300);
						$(".part" + part).fadeIn(300);
						$(".partbut" + (3 - part)).addClass("hidden");
						$(".partbut" + part).removeClass("hidden");
					}
				}
			}
			function chk_entry() {
				$("#entry_but").text("Проверяем...").attr("disabled", "disabled");
				$(".wrong").fadeOut(300);
				var e_email = $("#e_email").val();
				var e_pass = $("#e_pass").val();
				var eventid = $("#events").val();
				$.ajax({
					url:"https://orthodontia.ru/udata/emarket/logout_user/.json",
					xhrFields: {
					  withCredentials: true
					},
					dataType: "json",
					success:function(data) {
						$.ajax({
							xhrFields: {
							  withCredentials: true
							},
							dataType: "json",
							url:"https://orthodontia.ru/udata/emarket/get_user_info/" + e_email + "/" + e_pass + "/.json",
							success:function(data) {
								$(".f_anim").css("opacity", 0);
								if (typeof data.extended === 'object') {
									// пользователь ввел правильные данные
									console.log("user auth");
									// отправляем данные в GA
									ga('send', 'event', 'form', 'authorization');
									yaCounter23063434.reachGoal('UserAuthorization');
									var user = data;
									$(".wrong").fadeOut(300);
									$.ajax({
										url:"https://orthodontia.ru/udata/emarket/is_first_purchase/" + e_email + "/" + eventid + "/.json",
										xhrFields: {
										  withCredentials: true
										},
										dataType: "json",
										success:function(data) {
											$(".f_anim").css("opacity", 0);
											if (data.result == 1) {
												// пользователь уже зарегистрирован на мероприятие, выводим надпись
												$(".exists").css("opacity", 1);
											}
											else {
												userid = user.id;
												// alert(userid);
												// пользователь еще не зарегистрирован, все ОК, рисуем форму регистрации на мероприятие
												$(".openform").fadeOut(300, function() {
													$(".formbox2").hide(0);
													// console.table(user);
													// вставляем поля
													arruser = new Array();
													var allprops = user.extended.groups.group[0].property;
													$.each(allprops, function(index, value) {
														// console.log(this.name);
														arruser[this.name] = this.value;
													});
													if (typeof user.extended.groups.group[1] == "object") {
														var allprops = user.extended.groups.group[1].property;
														$.each(allprops, function(index, value) {
															// console.log(this.name);
															arruser[this.name] = this.value;
														});
													}
													// console.table(arruser);
													$("#fname").val(arruser["fname"].value);
													$("#lname").val(arruser["lname"].value);
													$("#father_name").val(arruser["father_name"].value);
													$("#email").val(arruser["e-mail"].value);
													$("#phone").val(arruser["phone"].value);
													$("#city").val(arruser["city"].value);
													$("#company").val(arruser["company"].value);
													$("#bd").val(arruser["bd"].value);
													$("#country").val(arruser["country"].item.name);
													$("#region").val(arruser["region"].item.id);
													if (typeof arruser["prof_status"] == "object")
														$("#who").val(arruser["prof_status"].item.name);
													// убираем пароли из формы
													$(".phide").hide();
													// рисуем форму
													$(".formbox3").show(0);
													$(".openform").fadeIn(300);
												});
												// подгружаем юр. лиц
												$.ajax({
													url:"https://orthodontia.ru/udata/emarket/legalList/.json",
													xhrFields: {
													  withCredentials: true
													},
													dataType: "json",
													success:function(data1) {
														// alert(data1.items.length);
														// добавляем юр. лица в select
														$.each(data1.items.item, function(index, value) {
															$('#yur').append($('<option>', { 
																	value: value.id,
																	text : value.name 
															}));
														});
														// console.table(data1.items);
													}
												});
											}
										},
										error:function(data) {
										}
									});
								}
								else {
									// пользователь ввел неверные данные
									$("#e_email").addClass("invalid");
									$("#e_pass").addClass("invalid");
									$(".wrong").fadeIn(300, function() {
										$("#entry_but").text("Войти").removeAttr("disabled");
									});
								}
							}
						});
					}
				});
			}
			function fixbody() {
				$("body").addClass("hold");
			}
			function unfixbody() {
				$("body").removeClass("hold");
			}
			function chg_passstate() {
				if ($("#e_pass").attr("type") == "password") {
					$("#e_pass").attr("type", "text");
				}
				else {
					$("#e_pass").attr("type", "password");
				}
			}
			function chk_f_email() {
				var f_email = $("#f_email").val();
				var eventid = $("#events").val();
				if (f_email != "" && isEmail(f_email)) {
					$(".exists").fadeOut(300);
					$(".f_anim").css("opacity", 1);
					$("#f_email").removeClass("invalid");
					// проверяем email
					$.ajax({
						url:"https://orthodontia.ru/udata/emarket/is_user_exist/" + f_email + "/.json",
						dataType: "json",
						success:function(data) {
							$(".f_anim").css("opacity", 0);
							if (data.result == 1) {
								// пользователь найден, проверяем, есть ли регистрация на мероприятие
								$.ajax({
									url:"https://orthodontia.ru/udata/emarket/is_first_purchase/" + f_email + "/" + eventid + "/.json",
									dataType: "json",
									success:function(data) {
										$(".f_anim").css("opacity", 0);
										if (data.result == 1) {
											// пользователь уже зарегистрирован на мероприятие, выводим надпись
											$(".exists").fadeIn(300);
										}
										else {
											// еще не зарегистрирован, рисуем форму входа
											$(".openform").fadeOut(300, function() {
												$("#email").val($("#f_email").val());
												$(".formbox1").hide(0);
												$("#e_email").val(f_email);
												$(".formbox2").show(0);
												$(".openform").fadeIn(300);
											});
										}
									}
								});
							}
							else {
								// пользователь не найден, отображаем форму регистрации пользователя
								$(".openform").fadeOut(300, function() {
									$("#email").val($("#f_email").val());
									$(".notfound").show(0);
									$(".formbox1").hide(0);
									$(".formbox3").show(0);
									$(".openform").fadeIn(300);
								});
							}
						},
						error:function(data) {
						}
					});
				}
				else {
					$("#f_email").addClass("invalid");
				}
			}
			jQuery(function($){
				$.datepicker.regional['ru'] = {
					closeText: 'Закрыть',
					prevText: '&#x3C;Пред',
					nextText: 'След&#x3E;',
					currentText: 'Сегодня',
					monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
					'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
					monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
					'Июл','Авг','Сен','Окт','Ноя','Дек'],
					dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
					dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
					dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
					weekHeader: 'Нед',
					dateFormat: 'yy-mm-dd',
					firstDay: 1,
					isRTL: false,
					showMonthAfterYear: false,
					yearSuffix: ''};
				$.datepicker.setDefaults($.datepicker.regional['ru']);
			});
			$(function() {
				$(".date1").datepicker({
					changeYear: true,
					yearRange: '1920:2020',
					defaultDate: '1980-01-01'
				});
			});
			var igos = 0;
			var totigos = 0;
			var prods = 0;
			var filedone = 0;
			function pset(ord, val, state) {
				if (state) {
					prods++;
					$("#p" + ord).val(val);
					$(".prodsel span").text("Выбрано " + prods);
				}
				else {
					prods--;
					$("#p" + ord).val("");
					if (prods == 0)
						$(".prodsel span").text("-------");
					else
						$(".prodsel span").text("Выбрано " + prods);
				}
			}
			function igo(group, ord, theme, state, similar) {
				if (state) {
					igos++;
					var arr_s = similar.split(",");
					$(".prog[data-group=" + group + "]").addClass("faded").find(".igo").fadeOut(200);
					$(".prog[data-mk=" + ord + "]").removeClass("faded").find(".igo").fadeIn(200);
					$(".igoset[data-group=" + group + "]").addClass("faded").find("input").attr("disabled", "disabled");
					$(".igoset[data-mk=" + ord + "]").removeClass("faded").find("input").removeAttr("disabled").attr("checked", "checked");;
					$.each(arr_s, function(key, val) {
						$(".prog[data-mk=" + val + "]").addClass("faded").find(".igo").fadeOut(200);
						$(".igoset[data-mk=" + val + "]").addClass("faded").find("input").attr("disabled", "disabled");
					})
					$("#mk" + ord).parent().addClass("sel");
					$("#lf" + ord).text("Пойду!");
					$("#mk_" + ord).parent().addClass("sel");
					$("#lf_" + ord).text("Пойду!");
					var html = "<div id=\"chosen" + ord + "\" class=\"chitem group\"><div class=\"igonum\">" + igos + "</div><div class=\"igodesc\" id=\"igod" + ord + "\">" + theme + "</div><!--<div class=\"igodel\" onClick=\"igo(" + group + ", " + ord + ", '', false);\"><span>X</span> Удалить</div>--></div>";
					$(".chosen").append(html);
					$("#mclasses" + group).val(theme);
					$("#mcl" + group).val(ord);
					// для прокрутки вниз при выборе первого мастер-класса
					if (igos == 1)
						$('html,body').animate({scrollTop: $(".prog[data-group=" + (3 - group) + "]").offset().top - 200}, 500);
					
				}
				else {
					igos--;
					var arr_s = similar.split(",");
					$(".prog[data-group=" + group + "]").removeClass("faded").find(".igo").fadeIn(200);
					$(".igoset[data-group=" + group + "]").not(".cancelled").removeClass("faded").find("input").removeAttr("disabled");
					$.each(arr_s, function(key, val) {
						$(".prog[data-mk=" + val + "]").removeClass("faded").find(".igo").fadeIn(200);
						$(".igoset[data-mk=" + val + "]").not(".cancelled").removeClass("faded").find("input").removeAttr("disabled");
					})
					$(".igoset[data-mk=" + ord + "]").find("input").removeAttr("checked");
					$("#mk" + ord).parent().removeClass("sel");
					$("#lf" + ord).text("Хочу пойти");
					$("#mk_" + ord).parent().removeClass("sel");
					$("#lf_" + ord).text("Хочу пойти");
					$("#chosen" + ord).remove();
					$('.chitem').each(function(index) {
						$(this).find(".igonum").text(index + 1);
					});
					$("#mclasses" + group).val("");
					$("#mcl" + group).val("");
				}
			}
			function openmob(n) {
				var wdth = $(window).width();
				if (wdth < 960) {
					$("#event" + n).slideToggle(300);
					$("#prog" + n).toggleClass("open");
				}
			}
			function isEmail(email) {
				var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				return regex.test(email);
			}
			function chkigos() {
				if (igos < totigos) {
					$('.prog1').fadeOut(200);
					$('.progtab.sel').removeClass('sel');
					$('.progtab2').addClass('sel');
					$('div[data-date=8]').fadeOut(300);
					$('h2[data-date=8]').fadeOut(300);
					$('.progspace').fadeIn(300);
					$('.mktime').css('display', 'table');
					$('div[data-date=9]').fadeIn(300);
					$('h2[data-date=9]').fadeIn(300);
					$('.prog2').fadeIn(200).promise().done(function() {
						$('html,body').animate({scrollTop: $(".prog2").offset().top - 100}, 500);
					});
					return false;
				}
				else
					return true;
			}
			function chkform(chkfile) {
				var foo = 0;
				if (chkfile == 1) {
					$("#register").attr("disabled", "disabled");
					// отправляем данные в GA
					ga('send', 'event', 'ET', 'event_click', 'LP');
					yaCounter23063434.reachGoal('EventClick');
				}
				if (chkfile != 0 && filedone == 0 && $('.upl:checked').length > 0)
					{
					if (foo == 0) {
						$('html,body').animate({scrollTop: $('.upl').offset().top - 100}, 500);
					}
					$('#upload').addClass('redfld');
					foo = 1;
				}
				$('.ness:visible').each(function() {
					if ($(this).val() == '') {
						if (foo == 0) {
							$('html,body').animate({scrollTop: $(this).offset().top - 100}, 500);
						}
						$(this).addClass('redfld');
						foo = 1;
					}
					else {
						$(this).removeClass('redfld');
					}
				}); 
				$('#email').each(function() {
					if (!isEmail($(this).val())) {
						if (foo == 0) {
							// alert($(this).offset().top);
							$('html,body').animate({scrollTop: $(this).offset().top - 100}, 500);
						}
						$(this).addClass('redfld');
						foo = 1;
					}
					else {
						$(this).removeClass('redfld');
					}
				});
				/*
				if (foo == 0) {
					if (($("#events").val() == "Лекции и мастер-классы" || $("#events").val() == "Только лекция 8 декабря") && igos < 3) {
						$('html,body').animate({scrollTop: $(".igosel").offset().top - 100}, 500);
						$(".igosel").addClass('redfld');
						foo = 1;
					}
					else {
						$(".igosel").removeClass('redfld');
					}
				}
				*/
				if (foo == 0) {
					if (userid != "" && chkfile == 1) {
						countdown();
						writedata();
						ga('send', 'event', 'ET', 'event_registration', 'LP');
						yaCounter23063434.reachGoal('EventRegistration');
					}
					return true;
				}
				else {
					$("#register").removeAttr("disabled");
					return false;
				}
			}
			$(window).resize(function() {
				var wdth = $(window).width();
				if (wdth < 960) {
					swidth = $(".speakersdiv").width();
					$(".speaker").css("width", swidth);
				}
				else {
					swidth = $(".speakersdiv").width()/5;
					$(".speaker").css("width", swidth);
				}
			})
			var spmargin = 0;
			var swidth = 320;
			var curspeaker = 1;
 			function speaker_left() {
				if (spmargin < 0) {
					spmargin = spmargin + swidth;
					$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 500);
					curspeaker--;
					$(".spbullets").find(".active").removeClass("active");
					$("#spbul_" + curspeaker).addClass("active");
				}
				else {
					$(".speakers").animate({margin: '0 0 0 20'}, 200, function() {
						$(".speakers").animate({margin: '0 0 0 0'}, 200);
					});
				}
			}
 			function speaker_right() {
				var slide_count = $(".speakers").find(".speaker:visible").length;
				var limit = (slide_count - 1) * swidth;
				if (spmargin > -limit) {
					spmargin = spmargin - swidth;
					$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 500);
					$(".spbullets").find(".active").removeClass("active");
					curspeaker++;
					$(".spbullets").find(".active").removeClass("active");
					$("#spbul_" + curspeaker).addClass("active");
				}
				else {
					$(".speakers").animate({margin: '0 0 0 ' + (spmargin - 20)}, 200, function() {
						$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 200);
					});
				}
			}
			function speaker_go(sp) {
				spmargin = - swidth * (sp - 1);
				$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 500);
				$(".spbullets").find(".active").removeClass("active");
				$("#spbul_" + sp).addClass("active");
				curspeaker = sp;
			}
			$(document).ready(function() {
				// определяем кол-во
				var eventid = $("#events").val();
				$.ajax({
					url:"https://orthodontia.ru/udata/emarket/avaliable_reserv/" + eventid + "/.json",
					dataType: "json",
					success:function(data) {
						var places_left = data.amount_free;
						$(".places_left").text(places_left);
						$.ajax({
							type: "POST",
							url: "xp_places_left.php",
							timeout: 5000,
							data: "num=" + places_left,
							success: function(data) {
							},
							error: function(data) {
							}
						});
					}
				});
				// определяем возможность скидки и кол-во звезд, которые можно заработать
				$.ajax({
					url:"https://orthodontia.ru/udata/custom/outputEventLoyalty/" + eventid + ".json",
					dataType: "json",
					success:function(data) {
						// неправильно
						// var bonuses = data.add_loyalty.potential_bonus;
						// $("#bonuses").text(bonuses);
						var isallow50 = data.discount_loyalty.isallow;
						if (isallow50 == 1) {
							$(".allow50").addClass("is-active");
							$("#loyalty").removeAttr("disabled");
						}
					}
				});
				$.ajax({					
					url:"https://orthodontia.ru/udata/custom/outputEventDiscount/" + eventid + "/.json",
					dataType: "json",
					success:function(data) {
						var bonuses = 0;
						var freez = 0;
						if (typeof data.item !== "undefined") {
							bonuses = data.item[0].loyalty_bonus;
							freez = data.item[0].freez_ormco_star;
						}
						$("#bonuses").text(bonuses);
						$("#potential_ormco_star").val(bonuses);
						$("#freez_ormco_star").val(freez);
					}
				});
				var galSwiper = new Swiper('.gallery1 .swiper-container', {
					slidesPerView: 1,
					speed: 800,
					spaceBetween: 0,
					navigation: {
						prevEl: '.gallery1 .swiper-button-prev',
						nextEl: '.gallery1 .swiper-button-next'
					},
					centeredSlides: true,
				});
				if ($(window).width() < 760) {
					$(".howitwas").addClass("fp-auto-height");
				}
				hghts = new Array();
				lnks = new Array("form", "place", "price", "speakers", "programm", "about");
				for (var key in lnks) {
					if ($("[name=" + lnks[key] +"]").length) {
						hghts[key] = $("[name=" + lnks[key] +"]").offset().top;
					}
				}
				initialize_map();
				$("body").bind("click", function(e) {
					if ($(e.target).closest(".igosel").length > 0 || $(e.target).closest(".prodsel").length > 0 || $(e.target).closest(".igoset").length > 0 || $(e.target).closest(".prodset").length > 0) {
						return;
					}
					$(".igodd").fadeOut(500);
					$(".igosel").removeClass('sel');
					$(".proddd").fadeOut(500);
					$(".prodsel").removeClass('sel');
				});
				var wdth = $(window).width();
				if (wdth < 960) {
					swidth = $(".speakersdiv").width();
					$(".speaker").css("width", swidth);
				}
				// для навигации по хешам, если нет fullpage
				/*
				var i = document.location.hash.replace("#", "");
				$("a[href*='#']:not([href='#'])").click(function() {
					if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
						|| location.hostname == this.hostname) {
						var target = $(this.hash);
						target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
						if (target.length) {
							var i = target.attr("name");
							scrolling = 1;
							// $("#mobmenu").slideUp(300);
							$('html,body').animate({
								 scrollTop: target.offset().top - 60
							}, 1000, function() {
								scrolling = 0;
								if ($(window).width() < 760) {
									$('.menu').fadeOut(300);
									$('.burger').removeClass('on');
								}
								alert(target.offset().top);
							});
							// $(".ch").removeClass("ch");
							// $(this).parent().addClass("ch");
							// history.pushState(null, i, i);
							return false;
						}
					}
				});
				*/
			});
			var scrolling = 0;
			var scrolled = window.pageYOffset || document.documentElement.scrollTop;;
			window.onscroll = function() {
				var hght = $('body').height();
				// alert(hght);
				var prevscrolled = scrolled;
				scrolled = window.pageYOffset || document.documentElement.scrollTop;
				if (scrolling == 0)
					{
					for (var key in hghts)
						{
						if (scrolled >= hghts[key] - 300) {
							$(".ch").removeClass("ch");
							$("#mi" + lnks[key]).addClass("ch");
							$("#mi" + lnks[key] + "1").addClass("ch");
							break;
						}
					}
				}
			}
			function reload() {
				var src = document.captcha.src;
				document.captcha.src = '/images/loading.gif';
				document.captcha.src = src + '?rand='+Math.random();
			}
			var x1 = 55.7838804;
			var y1 = 37.5604173;
			var myLatlng1;
			var map;
			var marker1;
			markers = new Array();
			function initialize_map() {
				if ($(window).width() > 760)
					myLatlng1 = new google.maps.LatLng(x1, y1 - 0.000007*$(window).width());
				else {
					if ($(window).width() == 320)
						myLatlng1 = new google.maps.LatLng(x1 + 0.002, y1);
					else
						myLatlng1 = new google.maps.LatLng(x1 + 0.003, y1);
				}
				var mapOptions = {
					center: myLatlng1,
					mapTypeControl:!1,
					streetViewControl:!1,
					scrollwheel:!1,
					panControl:!1,
					zoomControlOptions:{position:google.maps.ControlPosition.RIGHT_BOTTOM},
					zoom: 16,
					mapTypeId: google.maps.MapTypeId.ROADMAP,
					styles: [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]
				};
				map = new google.maps.Map(document.getElementById("map"),
					mapOptions);
				myLatlng1 = new google.maps.LatLng(x1, y1);
				var infowindow1 = new google.maps.InfoWindow({
					content: "<div class=\"onmap\"><h4><?=$event_place;?></h4><?=$place_address;?></div>"
				});
				marker1 = new google.maps.Marker({
					position: myLatlng1,
					map: map,
					title: "<?=$address_text;?>",
					icon: "images/marker.svg"
				});
				// infowindow1.open(map, marker1);
				marker1.addListener('click', function() {
					infowindow1.open(map, marker1);
				});
				$.each(markers, function(index, value) {
					var contentString = "<div class=\"onmap\"><h4>" + value.name + "</h4>" + value.address + "<br />Тел.: <a href=\"tel:" + value.phone + "\"><b>" + value.phone + "</b></a><br /><a href=\"" + value.www + "\">" + value.www + "</a><br />" + value.descr + "</div>";
					var infowindow = new google.maps.InfoWindow({
						content: contentString
					});
					var marker = new google.maps.Marker({
						position: new google.maps.LatLng(value.x, value.y),
						map: map,
						icon: 'images/rest.png',
						title: value.name
					});
					marker.addListener('click', function() {
						infowindow.open(map, marker);
					});
				})
			}
			// загрузка изображения
			$(function(){
				var btnUploads = $("#upload");
				$(btnUploads).each(function() {
					new AjaxUpload(this, {
						action: "xp_upload.php",
						name: "uploadfile",
						onComplete: function(file, response){
							if (response.replace("Error", "") == response) {
								$("#upload").addClass("done").text("Файл загружен");
								$("#uploaded").html("<img width=\"100%\" src=\"<?=$uploaddir;?>uploads/" + response + "\" alt=\"\" />");
								$("#file").val('<?=$uploaddir;?>uploads/' + response);
								filedone = 1;
							}
							else {
								alert("Файл " + file + " не загружен! Ошибка: " + response.replace("Error", ""));
							}
						}
					});
				})
			});
		</script>
	</head>
	<body>
		<div class="video_div">
			<video muted autoplay loop playsinline preload="false" controls="false" class="ourvideo">
				<source src="video.mp4" type="video/mp4"></source>
			</video>
		</div>
		<button class="reg_button" onClick="<?if ($mkcounts[0] > 0) {?>if (chkigos()) {fixbody(); $('.mask').fadeIn(300); $('.openform').fadeIn(300, function() {/* $('#f_email').focus() */;});}<?}else{?>noplaces();<?}?>">Зарегистрироваться</button>
		<div class="fixed_layer">
			<div class="logo"><a href="#top"><svg 
				width="111" height="45" viewBox="0 0 111 45" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M0 15.5C0 24.0678 4.53107 31 12.4995 31C20.5461 31 24.999 24.0678 24.999 15.5C25.0771 6.93216 20.5461 0 12.4995 0C4.53107 0 0 6.93216 0 15.5ZM6.64036 15.5C6.64036 11.0603 8.28092 6.69849 12.5776 6.69849C16.8743 6.69849 18.5149 11.1382 18.5149 15.5C18.5149 19.9397 16.8743 24.3015 12.5776 24.3015C8.2028 24.3015 6.64036 19.9397 6.64036 15.5Z" fill="#1E4674"/>
				<path d="M27.0723 9.38732H32.494V13.1831H32.5663C33.3614 10.4718 35.3133 9 37.6988 9C38.1325 9 38.5663 9.07746 39 9.15493V15.5845C38.2771 15.3521 37.6265 15.1972 36.8313 15.1972C34.0843 15.1972 32.6386 17.5986 32.6386 20.3099V31H27V9.38732H27.0723Z" fill="#1E4674"/>
				<path d="M42 9.62191H47.7221V12.576H47.7984C48.8665 10.3993 50.7738 9 52.9101 9C55.1226 9 57.1826 9.85512 58.0981 12.4982C59.4714 10.2438 61.2262 9 63.6676 9C69.3896 9 70 14.2085 70 18.2509V31H64.1253V18.4841C64.1253 16.1519 63.2098 14.8304 61.6839 14.8304C59.1662 14.8304 58.9373 17.1625 58.9373 20.583V31H53.0627V18.8728C53.0627 16.3852 52.4523 14.8304 50.8501 14.8304C48.7902 14.8304 47.8747 16.2297 47.8747 20.583V31H42V9.62191Z" fill="#1E4674"/>
				<path d="M84.2833 17.2206C84.2833 15.4982 82.8927 14.2456 81.5021 14.2456C78.2575 14.2456 77.794 17.4555 77.794 20.3523C77.794 23.0142 78.7983 25.7544 81.1931 25.7544C83.2017 25.7544 84.206 24.4235 84.4378 22.2313H90C89.4592 27.79 85.8283 31 81.1931 31C75.9399 31 72 26.694 72 20.3523C72 13.7758 75.5536 9 81.1931 9C85.5193 9 89.4592 11.7402 89.8455 17.2206H84.2833Z" fill="#1E4674"/>
				<path d="M91 20.0391C91 26.3025 94.675 31 100 31C105.325 31 109 26.2242 109 20.0391C109 13.7758 105.325 9 100 9C94.675 9 91 13.7758 91 20.0391ZM96.55 20.0391C96.55 17.3772 97.375 14.2456 99.925 14.2456C102.55 14.2456 103.375 17.3772 103.375 20.0391C103.375 22.7011 102.55 25.7544 99.925 25.7544C97.375 25.7544 96.55 22.6228 96.55 20.0391Z" fill="#1E4674"/>
				<path d="M108.238 9.21429V11H107.857V9.21429H107V9H109V9.21429H108.238Z" fill="#1E4674"/>
				<path d="M111 9V11H110.704L110.778 9.28571L110.111 11H109.889L109.222 9.35714V11H109V9H109.296L109.963 10.6429L110.63 9H111Z" fill="#1E4674"/>
				<path d="M5.14286 40.0698L3 36H3.97403L5.53247 39.093L7.09091 36H8L5.85714 40.0698V43H5.14286V40.0698Z" fill="#1E4674"/>
				<path d="M9.5 42.25C10.4677 42.25 11.0323 41.5833 11.0323 40.5833C11.0323 39.5833 10.4677 38.9167 9.5 38.9167C8.53226 38.9167 7.96774 39.5833 7.96774 40.5833C7.96774 41.5 8.53226 42.25 9.5 42.25ZM9.5 38C10.871 38 12 39.1667 12 40.5C12 41.9167 10.871 43 9.5 43C8.12903 43 7 41.8333 7 40.5C7 39.1667 8.12903 38 9.5 38Z" fill="#1E4674"/>
				<path d="M17 42.9153H16.1373V42.1525C15.902 42.661 15.3529 43 14.6471 43C13.7843 43 13 42.4068 13 41.1356V38H13.8627V40.8814C13.8627 41.8136 14.3333 42.1525 14.8824 42.1525C15.5882 42.1525 16.1373 41.6441 16.1373 40.5424V38H17V42.9153Z" fill="#1E4674"/>
				<path d="M18 38.0833H18.6667V38.8333C18.8485 38.3333 19.2727 38 19.697 38C19.8182 38 19.8788 38 20 38.0833V39.0833C19.8788 39.0833 19.7576 39 19.697 39C19.0303 39 18.6667 39.5 18.6667 40.5833V43H18V38.0833Z" fill="#1E4674"/>
				<path d="M25.017 39.093H26.3729C27.1356 39.093 27.9831 38.9302 27.9831 37.9535C27.9831 36.9767 27.1356 36.814 26.3729 36.814H25.1017V39.093H25.017ZM24.0847 36H26.6271C28.4915 36 29 37.0581 29 37.9535C29 38.8488 28.4915 39.907 26.6271 39.907H25.017V43H24V36H24.0847Z" fill="#1E4674"/>
				<path d="M29 38.0833H30V38.8333C30.2727 38.3333 30.9091 38 31.5455 38C31.7273 38 31.8182 38 32 38.0833V39.0833C31.8182 39.0833 31.6364 39 31.5455 39C30.5455 39 30 39.5 30 40.5833V43H29V38.0833Z" fill="#1E4674"/>
				<path d="M34.72 40.6667C33.92 40.6667 32.88 40.8333 32.88 41.5833C32.88 42.1667 33.28 42.3333 33.84 42.3333C34.8 42.3333 35.12 41.5833 35.12 40.9167V40.5833H34.72V40.6667ZM32.24 38.75C32.72 38.25 33.44 38 34.08 38C35.44 38 36 38.75 36 39.5833V42.0833C36 42.4167 36 42.6667 36 42.9167H35.2C35.2 42.6667 35.2 42.4167 35.2 42.1667C34.8 42.75 34.32 43 33.6 43C32.72 43 32 42.5 32 41.5833C32 40.3333 33.12 39.9167 34.56 39.9167H35.2V39.75C35.2 39.25 34.88 38.75 34.08 38.75C33.44 38.75 33.12 39.0833 32.8 39.3333L32.24 38.75Z" fill="#1E4674"/>
				<path d="M40.3077 39.2295C40 38.9016 39.6923 38.7377 39.2308 38.7377C38.3077 38.7377 37.8462 39.5574 37.8462 40.459C37.8462 41.3607 38.3846 42.0984 39.3077 42.0984C39.7692 42.0984 40.0769 41.9344 40.3846 41.6066L41 42.2623C40.5385 42.7541 39.9231 43 39.3077 43C37.9231 43 37 42.0164 37 40.541C37 39.0656 37.9231 38 39.3077 38C39.9231 38 40.5385 38.2459 41 38.7377L40.3077 39.2295Z" fill="#1E4674"/>
				<path d="M44 39.1316H42.8V41.1842C42.8 41.6579 42.8 42.2105 43.4 42.2105C43.625 42.2105 43.85 42.2105 44 42.0526V42.8421C43.775 42.9211 43.4 43 43.25 43C41.975 43 41.975 42.2105 41.975 41.4211V39.0526H41V38.2632H41.975V37H42.8V38.2632H44V39.1316Z" fill="#1E4674"/>
				<path d="M45.125 38.3333H45.8125V43H45.125V38.3333ZM45.5 36C45.75 36 46 36.3218 46 36.6437C46 36.9655 45.75 37.2874 45.5 37.2874C45.25 37.2874 45 36.9655 45 36.6437C45 36.2414 45.1875 36 45.5 36Z" fill="#1E4674"/>
				<path d="M50.3077 39.2295C50 38.9016 49.6923 38.7377 49.2308 38.7377C48.3077 38.7377 47.8462 39.5574 47.8462 40.459C47.8462 41.3607 48.3846 42.0984 49.3077 42.0984C49.7692 42.0984 50.0769 41.9344 50.3846 41.6066L51 42.2623C50.5385 42.7541 49.9231 43 49.3077 43C47.9231 43 47 42.0164 47 40.541C47 39.0656 47.9231 38 49.3077 38C49.9231 38 50.5385 38.2459 51 38.7377L50.3077 39.2295Z" fill="#1E4674"/>
				<path d="M55.0517 40.1667C55.0517 39.3333 54.5345 38.75 53.5862 38.75C52.6379 38.75 52.0345 39.5 52.0345 40.1667H55.0517ZM52.0345 40.8333C52.0345 41.6667 52.8103 42.25 53.7586 42.25C54.3621 42.25 54.7931 42 55.1379 41.5L55.8276 42C55.3103 42.6667 54.5345 43 53.5862 43C52.0345 43 51 41.9167 51 40.5C51 39.0833 52.1207 38 53.5862 38C55.3103 38 56 39.3333 56 40.5833V40.9167H52.0345V40.8333Z" fill="#1E4674"/>
				<path d="M56 42.5C56 42.25 56.5 42 57 42C57.5 42 58 42.25 58 42.5C58 42.75 57.5 43 57 43C56.5 43 56 42.75 56 42.5Z" fill="#1E4674"/>
				<path d="M67.0667 39.4986C67.0667 38.0214 66.0556 36.8552 64.5 36.8552C62.9444 36.8552 61.9333 38.0992 61.9333 39.4986C61.9333 40.9758 62.9444 42.142 64.5 42.142C66.0556 42.142 67.0667 40.9758 67.0667 39.4986ZM61 39.4986C61 37.4772 62.4778 36 64.5 36C66.5222 36 68 37.5549 68 39.4986C68 41.52 66.5222 42.9972 64.5 42.9972C62.4778 43.075 61 41.52 61 39.4986Z" fill="#1E4674"/>
				<path d="M73 42.9153H72.1373V42.1525C71.902 42.661 71.3529 43 70.6471 43C69.7843 43 69 42.4068 69 41.1356V38H69.8627V40.8814C69.8627 41.8136 70.3333 42.1525 70.8824 42.1525C71.5882 42.1525 72.1373 41.6441 72.1373 40.5424V38H73V42.9153Z" fill="#1E4674"/>
				<path d="M74 38.0833H74.6667V38.8333C74.8485 38.3333 75.2727 38 75.697 38C75.8182 38 75.8788 38 76 38.0833V39.0833C75.8788 39.0833 75.7576 39 75.697 39C75.0303 39 74.6667 39.5 74.6667 40.5833V43H74V38.0833Z" fill="#1E4674"/>
				<path d="M80.8136 39.093H81.8305C82.4407 39.093 83.1186 38.9302 83.1186 37.9535C83.1186 36.9767 82.5085 36.814 81.8305 36.814H80.8136V39.093ZM80.0678 36H82.1017C83.5932 36 84 37.0581 84 37.9535C84 38.8488 83.5932 39.907 82.1017 39.907H80.8136V43H80V36H80.0678Z" fill="#1E4674"/>
				<path d="M85 38.0833H86V38.8333C86.2727 38.3333 86.9091 38 87.5455 38C87.7273 38 87.8182 38 88 38.0833V39.0833C87.8182 39.0833 87.6364 39 87.5455 39C86.5455 39 86 39.5 86 40.5833V43H85V38.0833Z" fill="#1E4674"/>
				<path d="M88.375 38.3333H89.75V43H88.375V38.3333ZM89 36C89.5 36 90 36.3218 90 36.6437C90 36.9655 89.5 37.2874 89 37.2874C88.5 37.2874 88 36.9655 88 36.6437C88 36.2414 88.5 36 89 36Z" fill="#1E4674"/>
				<path d="M92.5 42.25C93.4677 42.25 94.0323 41.5833 94.0323 40.5833C94.0323 39.5833 93.4677 38.9167 92.5 38.9167C91.5323 38.9167 90.9677 39.5833 90.9677 40.5833C90.9677 41.5 91.5323 42.25 92.5 42.25ZM92.5 38C93.871 38 95 39.1667 95 40.5C95 41.9167 93.871 43 92.5 43C91.129 43 90 41.8333 90 40.5C90 39.1667 91.0484 38 92.5 38Z" fill="#1E4674"/>
				<path d="M96 38.0833H96.6667V38.8333C96.8485 38.3333 97.2727 38 97.697 38C97.8182 38 97.8788 38 98 38.0833V39.0833C97.8788 39.0833 97.7576 39 97.697 39C97.0303 39 96.6667 39.5 96.6667 40.5833V43H96V38.0833Z" fill="#1E4674"/>
				<path d="M99.1875 38.3333H99.875V43H99.1875V38.3333ZM99.5 36C99.75 36 100 36.3218 100 36.6437C100 36.9655 99.75 37.2874 99.5 37.2874C99.25 37.2874 99 36.9655 99 36.6437C99 36.2414 99.25 36 99.5 36Z" fill="#1E4674"/>
				<path d="M104 39.1316H102.8V41.1842C102.8 41.6579 102.8 42.2105 103.4 42.2105C103.625 42.2105 103.85 42.2105 104 42.0526V42.8421C103.775 42.9211 103.4 43 103.25 43C101.975 43 101.975 42.2105 101.975 41.4211V39.0526H101V38.2632H101.975V37H102.8V38.2632H104V39.1316Z" fill="#1E4674"/>
				<path d="M104 38H105.121L106.586 41.6207L107.966 38H109L106.672 43.6322C106.328 44.4368 105.983 45 104.948 45C104.69 45 104.345 45 104.086 44.8391L104.172 44.0345C104.345 44.1149 104.517 44.1149 104.776 44.1149C105.379 44.1149 105.552 43.7931 105.724 43.3103L106.069 42.5862L104 38Z" fill="#1E4674"/>
				<path d="M109 42.5C109 42.25 109.25 42 109.5 42C109.75 42 110 42.25 110 42.5C110 42.75 109.75 43 109.5 43C109.25 43 109 42.75 109 42.5Z" fill="#1E4674"/>
			  </svg>
			</a></div>
			<div class="fixed_top">Тема семинара: <span><?=$event_short;?></span></div>
			<div class="burger" onClick="$('.menu').fadeToggle(300); $(this).toggleClass('on');"><img class="ib" src="images/burger.svg" /><img class="ic" src="images/close.svg" /></div>
			<div class="fixed_bottom">
				<div class="menu">
					<ul class="mainmenu">
						<li><a href="#speakers">Спикер<?if (count($speakers) > 1) {?>ы<?}?></a></li>
						<li><a href="#programm">Программа</a></li>
						<li><a href="#place">Место проведения</a></li>
						<li><a href="#price">Стоимость</a></li>
						<li><a href="#form">Регистрация</a></li>
					</ul>
				</div>
				<div class="hide_down">
					<hr>
					<h3><time><?=$event_date;?></time></h3>
					<h4><?=$event_place;?></h4>
					<!--<a href="#form"><button type="button">Регистрация</button></a>-->
					<span class="reg_span" onClick="<?if ($mkcounts[0] > 0) {?>if (chkigos()) {fixbody(); $('.mask').fadeIn(300); $('.openform').fadeIn(300, function() {/* $('#f_email').focus() */;});}<?}else{?>noplaces();<?}?>">Зарегистрироваться</span>
				</div>
			</div>
		</div>
		<div class="screen-page">
			<div class="section top">
				<!--<h5>Практический семинар</h5>-->
				<h1><?=$event_short;?></h1>
				<h4><?=$speakers[0]["name"];?></h4>
				<h5><?=$event_date;?>, <?=$event_city;?></h5>
			</div>
			<div class="section speakers">
				<div class="flex">
					<div class="sp_text">
						<h2>Спикер</h2>
						<p><?=nl2br($speakers[0]["about"]);?></p>
						<button class="small-button speakers_more" onClick="$('.mask').fadeIn(300); $('.speakers_data').fadeIn(300);">Подробнее</button>
					</div>
					<!--<div class="sp_image"><a href=""><img src="images/<?=$speakers[0]["image"];?>" /><div class="on"><img src="images/video.png" /></div></a></div>-->
				</div>
			</div>
			<div class="section programm fp-auto-height-responsive">
				<h2>Программа</h2>
				<div class="prog_items_container flex flex-wrap">
					<div class="prog_items">
						<?
						$cnt = 0;
						foreach ($programm as $prog)
							{
							?>
							<div class="prog_item flex flex-wrap">
								<div class="prog_time"><?=$prog["time"];?></div>
								<div class="prog_theme"><?=nl2br($prog["theme"]);?></div>
							</div>
							<?
						}
						?>
					</div>
				</div>
			</div>
			<div class="section place">
				<div class="map">
					<div id="map"></div>
				</div>
				<div class="beforemap">
					<h2>Место проведения</h2>
					<h4><?=$event_place;?></h4>
					<h5><?=$place_address;?></h5>
				</div>
			</div>
			<div class="section price_div fp-auto-height-responsive">
				<div class="prices">
					<div class="prices_ flex">
						<div class="prices_left">
							<h2>Стоимость<br />участия</h2>
							<h4>Осталось мест: <em class="places_left"><?=$mkcounts[0];?></em> из <?=$places_count;?></h4>
							<span>Количество мест ограничено! Просим заблаговременно пройти регистрацию и подтвердить участие!
							<br />
							<br />
							<h6>Действуют скидки!</h6>
							Для ординаторов – скидка 40%<br />
							Для действующих участников Школы ортодонтии – скидка 50%<br />
							Для действующих членов Профессионального общества ортодонтов – скидка 15%<br />
							Для участников программы <span>Ormco Stars</span> - скидка 50%
							</span>
						</div>
						<div class="prices_right">
							<?
							foreach ($prices as $price)
								{
								?>
								<div class="one_price <?if ($price["date_before"] != "" && date("Y-m-d H:s") >= $price["date_before"]." 23:59" || $price["date_after"] != "" && date("Y-m-d H:s") < $price["date_after"]." 23:59") {?> opa<?}?>">
									<div class="price"><?=number_format($price["price"], 0, ".", " ");?> р.</div>
									<div class="pr_title"><?=$price["name"];?></div>
								</div>
								<?
							}
							?>
						</div>
					</div>
				</div>
			</div>
			<div class="section howitwas">
				<h2>Фрост в 2017 году</h2>
				<div class="gallery1">
					<div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide" style="background-image: url(images/gal/1.jpg);"></div>
							<div class="swiper-slide" style="background-image: url(images/gal/2.jpg);"></div>
							<div class="swiper-slide" style="background-image: url(images/gal/3.jpg);"></div>
							<div class="swiper-slide" style="background-image: url(images/gal/4.jpg);"></div>
							<div class="swiper-slide" style="background-image: url(images/gal/5.jpg);"></div>
						</div>
						<div class="swiper-button-prev"></div>
						<div class="swiper-button-next"></div>
					</div>
				</div>
			</div>
			<div class="section form_">
				<h2>Регистрация</h2>
				<div class="formtable">
					<div class="flex">
						<img src="images/face.jpg" />						
						<div class="formtxt">
							В случае появления вопросов или сложностей при регистрации Вы можете связаться по телефону с нашим менеджером Григорием:
						</div>
					</div>
					<div class="formphone">
						<a href="tel:+79266108002">+7&nbsp;(926)&nbsp;610-80-02</a>
					</div>
					<?
					if ($mkcounts[0] < 1)
						{
						?>
						<h4 class="red">Регистрация окончена. Мест нет.</h4>
						<?
					}
					else
						{
						?>
						<button type="button" onClick="fixbody(); $('.mask').fadeIn(300); $('.openform').fadeIn(300, function() {/* $('#f_email').focus() */;});">Зарегистрироваться</button>
						<?
					}
					?>
				</div>
			</div>
			<div class="section footer fp-auto-height">
<script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
<script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
<div class="ya-share2" data-services="vkontakte,facebook,gplus,twitter" data-counter=""></div>
				Ormco Corporation &copy; 2019<br />
				<a href="http://ormco.ru">ormco.ru</a>
			</div>
		</div>
		<div class="openform">
			<div class="openform_">
				<div class="newform formbox1">
					<div class="formbox_close" onClick="unfixbody(); $('.mask').fadeOut(300); $('.openform').fadeOut(300);"><span class="shape1"></span><span class="shape2"></span></div>
					<h2>Регистрация</h2>
					<h4>Введите Ваш email. Если он уже есть в нашей базе, регистрация пройдет быстрее</h4>
					<input id="f_email" name="f_email" value="<?=$_SESSION["f_email"];?>" />
					<button type="button" onClick="chk_f_email();">Далее</button>
					<div class="f_anim">
						<div class="at">@</div>
						<div class="arr arr1">←</div>
						<div class="dash arr2"></div>
						<div class="dash arr3"></div>
						<div class="dash arr4"></div>
						<div class="arr arr5">→</div>
						<div class="db">
							<div class="db1"></div>
							<div class="db2"></div>
							<div class="db3"></div>
							<div class="db4"></div>
							<div class="db5"></div>
							<div class="db6"></div>
							<div class="db7"></div>
							<div class="db8"></div>
							<div class="db9"></div>
						</div>
						<div class="f_desc">Проверяем email на наличие в базе данных</div>
					</div>
					<div class="exists">Человек с данным Email уже зарегистрировался на данное мероприятие</div>
				</div>
				<div class="newform formbox2">
					<div class="formbox_close" onClick="unfixbody(); $('.mask').fadeOut(300); $('.openform').fadeOut(300);"><span class="shape1"></span><span class="shape2"></span></div>
					<h2>Вход в систему</h2>
					<h4>Email найден! Пожалуйста, введите пароль от вашей учетной записи Ormco (сайты ormco.ru или orthodontia.ru), чтобы зарегистрироваться на семинар</h4>
					<div class="newform_field group">
						<div class="newform_ttl">Логин (Email):</div>
						<div class="newform_fld"><input id="e_email" name="e_email" value="<?=$_SESSION["f_email"];?>" /></div>
					</div>
					<div class="newform_field group">
						<div class="newform_ttl">Пароль:</div>
						<div class="newform_fld"><input type="password" id="e_pass" name="e_pass" value="" /><img onClick="chg_passstate();" src="images/eye.svg" /></div>
					</div>
					<div class="newform_link"><span onClick="remember_pass();">Вспомнить пароль</span></div>
					<div id="rp_ok" class="form_msg">Письмо с дальнейшими инструкциями по восстановлению пароля отправлены на указанный email</div>
					<div id="rp_fail" class="form_msg"></div>
					<div class="newform_field group">
						<div class="newform_ttl"></div>
						<div class="newform_fld"><button type="button" id="entry_but" onClick="chk_entry();">Войти</button></div>
					</div>
					<div class="exists">Человек с данным Email уже зарегистрировался на данное мероприятие</div>
					<div class="wrong">Пароль или логин указаны неверно</div>
				</div>
				<div class="newform formbox3">
					<div class="formbox_close" onClick="unfixbody(); $('.mask').fadeOut(300); $('.openform').fadeOut(300);"><span class="shape1"></span><span class="shape2"></span></div>
					<h2>Регистрация на мероприятие</h2>
					<div class="notfound red hidden">Пользователь не найден в базе данных. Вы можете зарегистрироваться на мероприятие, заполнив форму ниже:</div>
					<form action="https://orthodontia.ru/emarket/lporderSmart/" method="post" onSubmit="return chkform(1);" enctype="multipart/form-data">
					<input type="hidden" name="regularprice" id="regularprice" value="<?if (date("Y-m-d H:s") < "2019-10-07 23:59") {?>17000<?}else{?>19000<?}?>" />
					<input type="hidden" name="finalprice" id="finalprice" value="<?if (date("Y-m-d H:s") < "2019-10-07 23:59") {?>17000<?}else{?>19000<?}?>" />
					<input type="hidden" name="file" id="file" value="" />
					<input type="hidden" name="events" id="events" value="<?=$event_id;?>" />
					<input type="hidden" name="potential_ormco_star" id="potential_ormco_star" value="" />
					<input type="hidden" name="freez_ormco_star" id="freez_ormco_star" value="" />
					<div class="partbuts">
						<!--<div class="partbut active partbut1" onClick="showpart(2);">Личные данные</div> → <div class="partbut partbut2" onClick="showpart(2);">Способ оплаты</div>-->
						<h4 class="partbut1">Личные данные</h4>
						<h4 class="partbut2 hidden">Регистрация</h4>
					</div>
					<hr class="hr">
					<div class="part1">
						<div class="newform_field group">
							<div class="newform_ttl">Email <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="email" id="email" /></div>
						</div>
						<div class="newform_field group phide">
							<div class="newform_ttl"></div>
							<div class="newform_fld small">На этот email будет отправлено подтверждение регистрации и ссылка для активации вашей учетной записи Ormco</div>
						</div>
						<div class="newform_field group phide">
							<div class="newform_ttl">Пароль <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="password" size="30" name="password" id="password" /></div>
						</div>
						<div class="newform_field group phide">
							<div class="newform_ttl">Пароль еще раз <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="password" size="30" name="password2" id="password2" /></div>
						</div>
						<br />
						<div class="newform_field group">
							<div class="newform_ttl">Фамилия <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="lname" id="lname" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Имя <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="fname" id="fname" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Отчество <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="father_name" id="father_name" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Телефон <sup>*</sup></div>
							<div class="newform_fld"><input placeholder="+7 (987) 1234567" class="short ness" type="text" size="25" name="phone" id="phone" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Страна<sup>*</sup></div>
							<div class="newform_fld"><select name="country" id="country" class="ness" onChange="if (this.value == 'Россия') {$('.region_field').fadeIn(300);}else{$('.region_field').fadeOut(300);}">
									<option value="Россия">Россия</option>
									<option value="Азербайджан">Азербайджан</option>
									<option value="Армения">Армения</option>
									<option value="Белоруссия">Белоруссия</option>
									<option value="Грузия">Грузия</option>
									<option value="Украина">Украина</option>
									<option value="Другая">Другая</option>
								</select>
							</div>
						</div>
							<div class="newform_field group region_field">
								<div class="newform_ttl">Регион <sup>*</sup></div>
								<div class="newform_fld"><select name="region" id="region" class="ness">
									<option value="">-= выберите регион =-</option>
									<option value="9870">Москва и МО</option>
									<option value="9871">Санкт-Петербург и ЛО</option>
									<option value="9872">Северо-Западный</option>
									<option value="9873">Центральный</option>
									<option value="9874">Сибирский</option>
									<option value="9875">Приволжский</option>
									<option value="9876">Южный</option>
									<option value="9877">Северо-Кавказский</option>
									<option value="9878">Уральский</option>
									<option value="9879">Дальневосточный</option>
									<option value="16761">Не Россия</option>
								</select></div>
							</div>
						<div class="newform_field group">
							<div class="newform_ttl">Город<sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="city" id="city" /></div>
						</div>
						<div class="newform_field group" id="speciality">
							<div class="newform_ttl">Кто Вы? <sup>*</sup></div>
							<div class="newform_fld"><select name="who" id="who" class="ness">
									<option value="">Кто Вы?</option>
									<option value="Ортодонт">Ортодонт</option>
									<option value="Хирург">Хирург</option>
									<option value="Другая специализация">Другая специализация</option>
									<option value="Не врач">Не врач</option>
								</select>
							</div>
						</div>
						<!--<div class="newform_field group">
							<div class="newform_ttl">Клиника/ВУЗ<sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="company" id="company" /></div>
						</div>-->
						<div class="newform_field group">
							<div class="newform_ttl"></div>
							<div class="newform_fld"><button type="button" id="entry_but" onClick="showpart(2);">Далее</button></div>
							<!--<div class="newform_fld"><button id="register">Зарегистрироваться</button></div>-->
						</div>
					</div>
					<div class="part2 hidden">
						<h3>Выберите скидку. Обратите внимание: скидка будет направлена менеджеру Ormco для подтверждения</h3>
						<div class="newform_field group">
							<div class="newform_flda"><input type="checkbox" class="dscnt upl" name="poo" id="poo" value="Член ПОО" onClick="$('.newform_upload').fadeIn(300); $('#uploaded').fadeIn(300); setdiscount(15, 'poo', this.checked);" /> <label for="poo">Являюсь членом Профессионального общества ортодонтов</label></div>
						</div>
						<div class="newform_field group">
							<div class="newform_flda"><input type="checkbox" class="dscnt" name="school" id="shoo" value="Действующий участник Школы ортодонтии" onClick="$('.newform_upload').fadeOut(300); $('#uploaded').fadeOut(300); setdiscount(50, 'shoo', this.checked);" /> <label for="shoo">Действующий участник Школы ортодонтии</label></div>
						</div>
						<div class="newform_field group">
							<div class="newform_flda"><input type="checkbox" class="dscnt upl" name="ordinator" id="ordinator" value="Ординатор" onClick="$('.newform_upload').fadeIn(300); $('#uploaded').fadeIn(300); setdiscount(40, 'ordinator', this.checked);" /> <label for="ordinator">Я ординатор</label></div>
						</div>
						<div class="allow50 newform_field group">
							<div class="newform_flda"><input type="checkbox" class="dscnt" name="loyalty" id="loyalty" value="Скидка 50% по программе Ormco Stars" onClick="$('.newform_upload').fadeOut(300); $('#uploaded').fadeOut(300); setdiscount(50, 'loyalty', this.checked);" disabled /> <label for="loyalty">Скидка 50% по программе <span>Ormco Stars</span></label></div>
						</div>
						<div class="newform_field newform_upload hidden group">
							<div class="newform_fldu">Загрузить удостоверение <button type="button" class="upload" name="upload" id="upload">Выберите файл</button></div>
						</div>
						<div class="uploaded hidden" id="uploaded">(форматы jpg, gif, png или pdf)</div>
						<div class="discount hidden">
							Ваша скидка <span id="discount">0</span> процентов
						</div>
						<div class="finalprice">
							Стоимость участия <span id="finalprice_txt"><?if (date("Y-m-d H:s") < "2019-10-07 23:59") {?>17000<?}else{?>19000<?}?> р.</span>
						</div>
						<div class="newform_field">
							<h4>За участие в этом мероприятии Вы получите <span id="bonuses">0</span> звезд по программе <span>Ormco Stars</span></h4>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Оплата:</div>
							<div class="newform_fld"><div class="tabbut active tabbut1" onClick="showtab(1);">Как физ. лицо</div><div class="tabbut tabbut2" onClick="showtab(2);">Как юр. лицо</div></div>
						</div>
						<div class="tab1">
							<div class="newform_field group">
								<div class="newform_ttl">Способ оплаты:</div>
								<div class="newform_fld"><select name="payment_id" id="payment_id" class="ness" onChange="if (this.value == 4666) {showtab(2);}">
										<option value="">выберите способ оплаты</option>
										<option value="4663">Квитанция в банк</option>
										<option value="4664" comment="Не позднее, чем за 10 дней до мероприятия!">Наличными в офисе Ormco в Москве</option>
										<option value="17688" comment="Приготовьтесь сразу произвести оплату">Онлайн оплата</option>
										<option value="4666">Счет для юр. лиц</option>
									</select>
								</div>
							</div>
						</div>
						<div class="tab2 hidden">
							<div class="newform_field group yurs">
								<div class="newform_ttl">Юр. лицо:</div>
								<div class="newform_fld"><select name="yur" id="yur" class="ness" disabled>
									</select>
								</div>
							</div>
							<div class="addyur" onClick="addnewyur();">
								<span>+</span> Добавить новое юр. лицо
								<small>Приготовьтесь ввести реквизиты юр. лица</small>
							</div>
							<div class="addyurform addyurform1 hidden">
								<div class="addyur_close" onClick="$('.addyurform').fadeOut(300, function(){$('.newform_reg').fadeIn(300); $('.addyur').fadeIn(300); $('.yurs').fadeIn(300);})"><span class="shape1"></span><span class="shape2"></span></div>
								<div class="newform_field group">
									<div class="newform_ttl">Новое юр. лицо:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="yurname" id="yurname" /><small>Введите юридическое название организации</small></div>
								</div>
								<h4>Контактная информация</h4>
								<div class="newform_field group">
									<div class="newform_ttl">ФИО:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="fio_rukovoditelya" id="fio_rukovoditelya" /><small>ФИО руководителя организации</small></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Должность:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="dolzhnost" id="dolzhnost" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Контактное лицо:</div>
									<div class="newform_fld"><input type="text" size="30" name="contact_person" id="contact_person" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Email<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="yuremail" id="yuremail" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Телефон</div>
									<div class="newform_fld"><input class="short" type="text" size="30" name="phone_number" id="phone_number" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Факс</div>
									<div class="newform_fld"><input class="short" type="text" size="30" name="fax" id="fax" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl"></div>
									<div class="newform_fld"><button type="button" onClick="showyur(2);">Далее</button></div>
								</div>
							</div>
							<div class="addyurform addyurform2 hidden">
								<div class="addyur_back" onClick="showyur(1);"></div>
								<div class="addyur_close" onClick="$('.addyurform').fadeOut(300, function(){$('.newform_reg').fadeIn(300); $('.addyur').fadeIn(300); $('.yurs').fadeIn(300);})"><span class="shape1"></span><span class="shape2"></span></div>
								<div class="newform_field group">
									<div class="newform_ttl">ИНН:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="inn" id="inn" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">КПП:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="kpp" id="kpp" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Рассчетный счет:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="account" id="account" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">БИК:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness short" type="text" size="30" name="bik" id="bik" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Корр. счет:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="bank_account" id="bank_account" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">ОГРН:</div>
									<div class="newform_fld"><input type="text" size="30" name="ogrn" id="ogrn" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Юридический адрес:<sup>*</sup></div>
									<div class="newform_fld"><textarea class="ness" name="legal_address" id="legal_address"></textarea></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Фактический адрес:<sup>*</sup></div>
									<div class="newform_fld"><textarea class="ness" name="defacto_address" id="defacto_address"></textarea></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Почтовый индекс:</div>
									<div class="newform_fld"><input type="text" size="30" name="postal_address" id="postal_address" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl"></div>
									<div class="newform_fld"><button type="button" onClick="addyurajax();">Добавить юр. лицо</button></div>
								</div>
							</div>
						</div>
						<div id="igosel">
							<h4><?=$event_name;?> (осталось <em class="places_left"><?=$mkcounts[0];?></em> мест)</h4>
							<!--<h3>Выбранные мастер-классы</h3>
							<div class="chosen"></div>-->
						</div>
						<div class="newform_field newform_reg group">
							<div class="newform_ttl"></div>
							<div class="newform_fld"><button id="register">Зарегистрироваться</button></div>
						</div>
						<!--<div class="newform_field group">
							<div class="newform_ttla double">Планирую покупку продукции Ormco в ближайшее время (1-2 месяца)</div>
							<div class="newform_flda"><input type="checkbox" size="30" name="want_to_buy" id="want_to_buy" value="1" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttla double">Принимал(а) участие в образовательных мероприятиях Ormco в течение последнего года</div>
							<div class="flda"><input type="checkbox" size="30" name="how_you_been_edu" id="how_you_been_edu" value="1" /></div>
						</div>
						<div id="igosel">
							<h3>Выбранные мастер-классы</h3>
							<div class="igosel" onClick="$('.igodd').fadeToggle(200); $(this).toggleClass('sel');"><span>Выберите три мастер-класса</span></div>
							<div class="igodd">
								<?
								foreach ($mks as $n => $nks)
									{
									?>
									<div class="igotime igo<?=$n;?>"><?=$mktimes[$n];?></div>
									<?
									foreach ($nks as $m => $mk)
										{
										?>
										<div class="igoset igo<?=$n;?><?if ($mkcounts[$m - 1] <= 0) {?> faded cancelled<?}?>" data-group="<?=$n;?>" data-mk="<?=$m;?>" id="igo_<?=$m;?>"><input type="checkbox" <?if ($mkcounts[$m - 1] <= 0) {?>disabled <?}?>name="iset<?=$m;?>" id="iset<?=$m;?>" onClick="igo(<?=$n;?>, <?=$m;?>, '<strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?>', this.checked);" /><label for="iset<?=$m;?>"><strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?></label></div>
										<?
									}
								}
								?>
							</div>
							<div class="chosen"></div>
						</div>-->
						<div class="formagree">Нажимая «Отправить», вы соглашаетесь с <span class="lnk" onClick="$('#pol').fadeIn(300);">политикой конфиденциальности</span> и <a target="_blank" href="http://ormco.ru/files/sys/oferta_Ormco_Seminary.docx">условиями договора-оферты</a></div>
					</div>
					</form>
				</div>
			</div>
		</div>
		<div class="mask1"></div>
		<div class="form_error">
			<div class="close" onClick="$('.form_error').fadeOut(300); $('.mask1').fadeOut(300);"><img src="images/close.png" /></div>
			<h3 id="form_error"></h3>
		</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter23063434 = new Ya.Metrika({
                    id:23063434,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    ecommerce:"dataLayer"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/23063434" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67881160-2', 'auto');
  ga('require', 'displayfeatures');
  ga('require', 'linkid');
  ga('require', 'ec'); 
  ga('send', 'pageview');

</script>
<div id="pol">
<div class="close" onClick="$('#pol').fadeOut(300);"><img src="images/close.png" /></div>
<div id="inpol">
<h2>Политика конфиденциальности</h2>
<h4>1. Общие положения</h4>
Корпорация Ormco в лице ООО «Ормко» (далее «Ормко») и её аффилированные лица с уважением относятся к конфиденциальной информации любого лица, ставшего посетителем данного сайта. Мы хотели бы проинформировать Вас о том, какие именно данные мы собираем и каким образом используем собранные данные. Вы также узнаете о том, как Вы можете проверить точность собранной информации и дать нам указание об удалении подобной информации. Данные собираются, обрабатываются и используются строго в соответствии с требованиями действующего законодательства того государства, в котором расположено соответствующее аффилированное лицо компании "Ормко", отвечающее за защиту персональных данных. Мы делаем все возможное для обеспечения соответствия требованиям действующего законодательства.
Данное заявление не распространяется на сайты, на которые сайт компании "Ормко" содержит гиперссылки.
<h4>2. Сбор, использование и переработка персональных данных</h4>
Мы осуществляем сбор информации, относящейся к определенным лицам, лишь в целях обработки и использования информации и только в том случае, если Вы добровольно предоставили информацию или явно выразили свое согласие на ее использование.
Когда Вы посещаете наш сайт, определенные данные автоматически записываются на серверы компании "Ормко" и/или её аффилированных лиц для целей системного администрирования или для статистических или резервных целей. Записываемая информация содержит название Вашего интернет-провайдера, в некоторых случаях Ваш IP-адрес, данные о версии программного обеспечения Вашего браузера, об операционной системе компьютера, с которого Вы посетили наш сайт, адреса сайтов, после посещения которых Вы по ссылке зашли на наш сайт, заданные Вами параметры поиска, приведшие Вас на наш сайт.
В зависимости от обстоятельств, подобная информация позволяет сделать выводы о том, какая аудитория посещает наш сайт. В данном контексте, однако, не используются никакие персональные данные. Использоваться может лишь анонимная информация. Если информация передается компанией "Ормко" и/или её аффилированными лицами внешнему провайдеру, принимаются все возможные технические и организационные меры, гарантирующие передачу данных в соответствии с требованиями действующего законодательства.
Если Вы добровольно предоставляете нам персональную информацию, мы обязуемся не использовать, не обрабатывать и не передавать такую информацию способом, выходящим за рамки требований действующего законодательства. Использование и распространение Ваших персональных данных без Вашего согласия возможно только на основании судебного решения или в ином порядке, предусмотренном законодательством РФ.
Любые изменения, которые будут внесены в правила по соблюдению конфиденциальности, будут размещены на данной странице. Это позволяет Вам в любое время получить информацию о том, какие данные у нас хранятся и о том, каким образом мы собираем и храним такие данные.
<h4>3. Безопасность данных</h4>
Компания "Ормко" и её аффилированные лица обязуется бережно хранить Ваши персональные данные и принимать все меры предосторожности для защиты Ваших персональных данных от утраты, неправильного использования или внесения в персональные данные изменений. Партнеры компании "Ормко" и её аффилированных лиц, которые имеют доступ к Вашим данным, необходимым им для предоставления Вам услуг от имени компании "Ормко" и её аффилированных лиц, несут перед компанией "Ормко" и её аффилированными лицами закрепленные в контрактах обязательства соблюдать конфиденциальность данной информации и не имеют права использовать предоставляемые данные для каких-либо иных целей.
<h4>4. Персональные данные несовершеннолетних потребителей</h4>
Компания "Ормко" и её аффилированные лица не ведет сбор информации в отношении потребителей, не достигших 14 лет. При необходимости, мы можем специально попросить ребенка не присылать в наш адрес никакой личной информации. Если родители или иные законные представители ребенка обнаружат, что дети сделали какую-либо информацию доступной для компании "Ормко" и её аффилированных лиц, и сочтут, что предоставленные ребенком данные должны быть уничтожены, таким родителям или иным законным представителям необходимо связаться с нами по нижеуказанному (см. п. 6) адресу. В этом случае мы немедленно удалим личную информацию о ребенке.
<h4>5. Файлы Cookie</h4>
Мы используем файлы cookie для того, чтобы предоставить Вам больше возможностей при использовании сайта. Файлы cookie представляют собой небольшие блоки данных, помещаемые Вашим браузером на временное хранение на жестком диске Вашего компьютера, необходимые для навигации по нашему сайту. Файлы cookie не содержат никакой личной информации о Вас и не могут использоваться для идентификации отдельного пользователя. Файл cookie часто содержит уникальный идентификатор, представляющий собой анонимный номер (генерируемый случайным образом), сохраняемый на Вашем устройстве. Некоторые файлы удаляются по окончании Вашего сеанса работы на сайте; другие остаются на Вашем компьютере дольше.
Приступая к использованию данного веб-сайта, вы соглашаетесь на использование файлов cookie, Вы также признаете, что в подобном контенте могут использоваться свои файлы cookie.
Компания Ормко не контролирует и не несет ответственность за файлы cookie сторонних разработчиков. Дополнительную информацию Вы можете найти на сайте разработчика.
<h4>6. Отслеживание через интернет</h4>
На данном сайте осуществляется сбор и хранение данных для маркетинга и оптимизации с использованием технологии Яндекс.Метрика и Google Analitycs. Эти данные могут использоваться для создания профилей пользователей под псевдонимами. Сайт может устанавливать файлы cookie.
Без ясно выраженного согласия наших пользователей данные, собираемые с помощью технологий Яндекс.Метрика и Google Analitycs, не используются для идентификации личности посетителя и не связываются с какими-либо другими личными данными носителя псевдонима.
Дополнительную информацию об отслеживании через интернет Вы можете найти на сайтах провайдеров сервисов Яндекс.Метрика и Google Analitycs.
<h4>7. Ваши пожелания и вопросы</h4>
Хранящиеся данные будут стерты компанией "Ормко" и/или её аффилированными лицами по истечении периода хранения, установленного законодательством или договором либо в случае если сама компания "Ормко" и/или её аффилированные лица отменит хранение тех или иных данных. Вы вправе в любое время потребовать удаления из базы данных компании "Ормко" и/или её аффилированных лиц информации о Вас. Вы также вправе в любое время отозвать Ваше согласие на использование или переработку Ваших персональных данных. В таких случаях, а также, если у Вас есть какие-либо иные пожелания, связанные с Вашими персональными данными, просим Вас выслать письмо по почте в адрес «Ормко» в России по адресу: 195112, Санкт-Петербург, Малоохтинский пр-т, д. 64, корп. 3. или по электронной почте dc-sales@ormco.com. Просим Вас также связаться с нами в случае, если Вам хотелось бы узнать, собираем ли мы данные о Вас и если да, то какие именно данные. Мы постараемся выполнить Ваши пожелания в возможно короткие сроки.
<h4>8. Законодательство по обработке персональных данных</h4>
Все действия с персональными данными, собираемыми на данном сайте, производятся в соответствии с Федеральным законом Российской Федерации №152-ФЗ от 27 июля 2006 года «О персональных данных».
<h4>(1) Заявленная цель сбора, обработки или использования данных:</h4>
•	Предметом деятельности «Ормко» и её аффилированных лиц является производство и распространение стоматологических продуктов всех типов, главным образом брекет-систем, ортодонтических инструментов, микроимплантов, адгезивов;
<h4>(2) Описание групп вовлеченных лиц и соответствующих данных или категорий данных:</h4>
Данные, касающиеся заказчиков, сотрудников, пенсионеров, сотрудников сторонних компаний (субподрядчиков), персонала, работающего по лизингу, претендентов на рабочие места, авторов изобретений, не являющихся сотрудниками компаний, или наследников, соответственно, поставщиков товаров и услуг, сторонних заказчиков, потребителей, добровольцев, участвующих в потребительских испытаниях, посетителей производственных объектов корпорации, инвесторов – насколько это необходимо для выполнения целей, определенных в пункте 4.
<h4>(3) Получатели или категории получателей, которым могут быть разглашены данные:</h4>
Органы власти, фонды страхования здоровья, ассоциация по страхованию ответственности работодателей при наличии соответствующего правового регулирования, сторонние подрядчики в соответствии сторонние поставщики услуг, ассоциация пенсионеров «Ормко», аффилированные лица и внутренние подразделения для выполнения целей, указанных в пункте 4.
<h4>(4) Периодичность регулярного удаления данных:</h4>
Юристами подготовлено множество инструкций, касающихся обязанностей по хранению данных и периодов хранения. Данные удаляются в установленном порядке по истечении указанных периодов. Данные, не подпадающие под действие данных условий, удаляются, если цели, указанные в пункте 4, перестают существовать.
<h4>(5) Запланированная передача данных другим странам:</h4>
В рамках всемирной системы информации о трудовых ресурсах, данные по персоналу должны быть доступны определенным руководящим работникам в других странах. Соответствующие соглашения о защите данных должны быть заключены с соответствующими компаниями в соответствии со стандартами ЕС.
<h4>9. Использование встраиваемых модулей для социальных сетей</h4>
На наших интернет-страницах предусмотрена возможность встраивания модулей для социальных сетей ВКонтакте, Twitter, Instagram, Youtube (далее – «провайдеры»). 
Только если Вы активируете модуль, тем самым разрешая передачу данных, браузер создаст прямое соединение с сервером провайдера. Содержимое различных модулей впоследствии передается соответствующим провайдером непосредственно в Ваш браузер и выводится на экран Вашего компьютера.
Модуль сообщает провайдеру, на какую из страниц нашего сайта Вы вошли. Если во время просмотра нашего сайта Вы вошли на ВКонтакте, Instagram, Youtube или Twitter под своей учетной записью, провайдер может подобрать информацию, в соответствии с Вашими интересами, т.е. информацию, которую Вы просматриваете с помощью Вашей учетной записи. При использовании какой-либо функции встроенного модуля (например, кнопки “Мне нравится”, размещения комментария), эта информация также будет передана браузером непосредственно провайдеру для сохранения.
Дополнительную информацию по сбору и использованию данных социальными сетями, а также по правам и возможностям защиты Вашей конфиденциальности в данных обстоятельствах, можно найти в рекомендациях провайдеров по защите данных /конфиденциальности:
Для того, чтобы не подключаться к учетным записям провайдеров при посещении нашего сайта, Вам необходимо отключиться от соответствующей учетной записи перед посещением наших интернет-страниц.
</div>
</div>
	<div class="mask" onClick="$('.popup').fadeOut(300);$('.mask').fadeOut(300);"></div>
	<script src="js/fullpage.js" type="text/javascript"></script>
	<script src="js/jquery.nicescroll.min.js" type="text/javascript"></script>
	<script>
	$(document).ready(function () {
	  if ($(window).height() < 700 && $(window).width() > 760)
		{
		$(".prog_items_container").niceScroll({cursorborder:"",cursorcolor:"#3a97cc"});
	  }
	  $('.screen-page').fullpage({
		anchors: ['top', 'speakers', 'programm', 'place', 'price', 'howitwas', 'form', 'footer'],
		verticalCentered: false,
		licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
		menu: '.mainmenu',
		responsive: 1000,
		navigation: true,
		navigationPosition: 'right',
		afterLoad: function afterLoad() {
		  $('body').removeClass('is-single-show');
		},
		onLeave: function(index, nextIndex, direction){
			if ($(window).width() > 760) {
				if (nextIndex == 1) {
					$(".fixed_top").css("opacity", 0);
					$(".up").css("display", "none");
				}
				else {
					$(".fixed_top").css("opacity", 1);
					$(".up").css("display", "block");
				}
				if (nextIndex == 8) {
					if ($(window).height() < 1200)
						$(".hide_down").slideUp(500);
					$(".fixed_layer").css("height", "calc(100vh - 10rem)");
				}
				else {
					$(".hide_down").slideDown(500);
					$(".fixed_layer").css("height", "calc(100vh - 2rem)");
				}
			}
			else {
				$('.menu').fadeOut(300);
				$('.burger').removeClass('on');				
			}
		}
	  });
	  $('html').addClass('is-ready');

	  // $('[name="phone"]').mask('+7 (999) 999-99-99');
	  /*
	  if ($(window).width() < 1000) {
	    $(".menu ul li a").click(function() {
		  $(".menu").fadeOut(300);
		  $(".ic").fadeOut(300).promise().done(function() {
			  $(".ib").fadeIn(300)
		  })
	    })
	  }
	  */
	});
	(function (doc, win) {
	  var docEl = doc.documentElement,
		  resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
		  recalc = function recalc() {
		var clientWidth = docEl.clientWidth;
		if (!clientWidth) {
		  return;
		}
		if ($(window).width() > 960)
			docEl.style.fontSize = 3.7647 * (clientWidth / 320) + 'px';
	  };

	  if (!doc.addEventListener) return;
	  win.addEventListener(resizeEvt, recalc, false);
	  doc.addEventListener('DOMContentLoaded', recalc, false);
	})(document, window);
	</script>
		<div class="popup quote">...
			<button type="button" onClick="$('.popup').fadeOut(300);$('.mask').fadeOut(300);">Закрыть</button>
		</div>
		<div class="popup speakers_data"><button class="small-button close-button hidden-desktop" type="button" onClick="$('.popup').fadeOut(300);$('.mask').fadeOut(300);">Закрыть</button>
		<?=nl2br($speakers[0]["more"]);?>
		<button class="small-button close-button" type="button" onClick="$('.popup').fadeOut(300);$('.mask').fadeOut(300);">Закрыть</button></div>
		<a href="#top"><div class="up">вверх</div></a>
	</body>
</html>
<?
function places($num) {
	$rest = $num % 10;
	if ($num > 4 && $num < 21)
		return "мест";
	else {
		if ($num > 1 && $num < 5)
			return "места";
		else {
			if ($num == 1 || $rest == 1)
				return "место";
			else {
				if ($num > 20 && $rest > 1 && $rest < 5)
					return "места";
				else
					return "мест";
			}
		}
	}
}
?>