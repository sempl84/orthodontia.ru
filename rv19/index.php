<?php
session_start();
header("HTTP/1.1 200 OK");
header('Content-Type: text/html; charset=UTF-8');

// ini_set("display_errors", "1");
// ini_set("display_startup_errors", "1");
// ini_set("error_reporting", E_ALL);
ini_set("display_errors", "0");
ini_set("error_reporting", 0);

$event_id = array(2641,2643,2642);
$event_name = "Рождественские встречи 2019";
$uploaddir = "https://orthodontia.ru/rv19/";
function places($num) {
	$rest = $num % 10;
	if ($num > 4 && $num < 21)
		return "мест";
	else {
		if ($num > 1 && $num < 5)
			return "места";
		else {
			if ($num == 1 || $rest == 1)
				return "место";
			else {
				if ($num > 20 && $rest > 1 && $rest < 5)
					return "места";
				else
					return "мест";
			}
		}
	}
}

$speakers = array(
	1 => array(	"name" => "Яна Дьячкова",
				"about" => "Врач-ортодонт, к.м.н. В 2009 году защитила кандидатскую диссертацию на тему «Совершенствование диагностики аномалий зубных рядов посредством компьютерных технологий». За время работы на кафедре ортодонтии и детского протезирования МГМСУ вела научную, преподавательскую деятельность, участвовала в многочисленных конференциях и съездах, была переводчиком-референтом журнала «Ортодонтия», принимала участие в подготовке кафедральных реферативных конференций.
Является переводчиком с английского двух зарубежных монографий, впервые изданных в России:
- М. Б. Экермен, «Ортодонтическое лечение. Теория и практика»;
- К. Г. Исааксон с соавторами, «Съемные ортодонтические аппараты».
Неоднократно участвовала в конференциях и съездах врачей-ортодонтов, посещала семинары и лекции, посвященные работе брекет-системами разных производителей, особенностям лечения ортодонтических аномалий лингвальными брекетами, ортодонтическими имплантами, элайнерами, связи ортодонтических аномалий с нарушениями осанки, коррекции дисфункций височно-нижнечелюстных суставов  и многому другому.
Предпочтение в клинической практике отдает системе Damon.

Тематики семинаров спикера:

Диагностика в ортодонтии
Damon System
ВНЧС
",
				"image" => "Dyachkova_2017.jpg",
	),
	2 => array(	"name" => "Александр Спесивцев",
				"about" => "Закончил стоматологический факультет Тверской Государственной медицинской академии. Проходил целевую интернатуру по общей стоматологии. С 2005-2007 года проходил клиническую ординатуру по ортодонтии. Имеет большой опыт в ортодонтической практике с использованием Damon System и другой аппаратуры, опытный спикер.

Тематики семинаров спикера:

Введение в систему Damon
",
				"image" => "Spesivtsev_2017.jpg",
	),
	3 => array(	"name" => "Михаил Есипович",
				"about" => "Врач-ортодонт, главный врач и руководитель клиники \"Стоматология Михаила Есиповича\". Автор метода непрямой фиксации \"Капа Есиповича\". Разработал методическое пособие по телерентгенограмме. Клинический опыт работы с системой Damon - более 9 лет, эксперт в области продаж ортодонтического лечения.

Тематики семинаров спикера:

Курс \"Я – ортодонт\"
Мастер – класс \"Точная фиксация брекетов\"
Маркетинг в ортодонтии
Эффективная ортодонтическая консультация 
Ортоконтроша
",
				"image" => "Esipovich_2017.jpg",
	),
);
$mkcounts = explode("/", file_get_contents("mkcounts.txt"));
$mktimes = array(1 => "10:00 - 11:30", 2 => "12:00 - 13:30", 3 => "14:30 - 16:00");
$mks = array(
	1 => array(
		1 => array(
			"speaker" => 1,
			"event" => 2641,
			"similar" => "2, 3, 4, 5, 6, 7, 8, 9",
			"theme" => "Первичная консультация пациента с признаками дисфункции ВНЧС",
			"themedesc" => "1. Подробная беседа с пациентом - важнейшая часть консультации! О чем надо обязательно спросить каждого пациента?

2. Анализ движения нижней челюсти и оценка степени открывания рта. Чем отличается дефлексия от девиации и о чем говорит их наличие? Суставные шумы, как разобраться, что они означают?

3. Пальпация ВНЧС, мышц челюстно-лицевой области в покое и при функции. Почему это необходимо проводить каждому ортодонтическому пациенту?

4. Повседневный анализ окклюзии зубных рядов в клинике. Нарушения структуры зубной эмали и слизистой оболочки полости рта, указывающие на дисфункцию жевательного аппарата. Основные понятия биомеханики зубочелюстной системы.

5. В каких случаях ортодонту нужна помощь смежных специалистов?

БОНУС: 

Неврологический осмотр, который может провести любой врач-стоматолог на первичной консультации, чтобы понять, нужна ли пациенту консультация невролога, или будет достаточно стоматологических методов  лечения."
		),
		2 => array(
			"speaker" => 2,
			"event" => 2643,
			"similar" => "1, 3, 4, 5, 6, 7, 8, 9",
			"theme" => "Движение вперед или назад. Современный взгляд на лечение подростков с дистальным прикусом",
			"themedesc" => "Коротко о причинах и путях развития второго класса.
Диагностика: лицо, челюсти и зубы - выделяем главное.
Damon System - как инструмент расширения и создания условий для лечения дистальной окклюзии (archdevelopment, ротация моляров).
Изготовление дистализатора. Применение мини-винтов (практика)."
		),
		3 => array(
			"speaker" => 3,
			"event" => 2642,
			"similar" => "1, 2, 4, 5, 6, 7, 8, 9",
			"theme" => "Мой идеальный план лечения",
			"themedesc" => "- разбор диагностических данных необходимых для точного планирования 
- презентация плана лечения
- работа в Power Pointe и Keynote
- визуальная карта плана лечения
- работа с шаблонами визуальной карты, планами лечения и ортодонтической картой

Вы научитесь:
-структурировать диагностические данные 
-работать с визуальным планом лечения
-составлять наглядный и понятный план лечения для врача и пациента

Вы получите:
- шаблон презентации дигностических данных для пациента
- шаблон визуальной карты с зубами и с необходимым набором символов и значков 
- шаблон ортодонтической карты для удобной повседневной работы на приеме
"
		),
	),
	2 => array(
		1 => array(
			"speaker" => 1,
			"event" => 2641,
			"similar" => "1, 2, 3, 5, 6, 7, 8, 9",
			"theme" => "Первичная консультация пациента с признаками дисфункции ВНЧС",
			"themedesc" => "1. Подробная беседа с пациентом - важнейшая часть консультации! О чем надо обязательно спросить каждого пациента?

2. Анализ движения нижней челюсти и оценка степени открывания рта. Чем отличается дефлексия от девиации и о чем говорит их наличие? Суставные шумы, как разобраться, что они означают?

3. Пальпация ВНЧС, мышц челюстно-лицевой области в покое и при функции. Почему это необходимо проводить каждому ортодонтическому пациенту?

4. Повседневный анализ окклюзии зубных рядов в клинике. Нарушения структуры зубной эмали и слизистой оболочки полости рта, указывающие на дисфункцию жевательного аппарата. Основные понятия биомеханики зубочелюстной системы.

5. В каких случаях ортодонту нужна помощь смежных специалистов?

БОНУС: 

Неврологический осмотр, который может провести любой врач-стоматолог на первичной консультации, чтобы понять, нужна ли пациенту консультация невролога, или будет достаточно стоматологических методов  лечения."
		),
		2 => array(
			"speaker" => 2,
			"event" => 2643,
			"similar" => "1, 2, 3, 4, 6, 7, 8, 9",
			"theme" => "Движение вперед или назад. Современный взгляд на лечение подростков с дистальным прикусом",
			"themedesc" => "Коротко о причинах и путях развития второго класса.
Диагностика: лицо, челюсти и зубы - выделяем главное.
Damon System - как инструмент расширения и создания условий для лечения дистальной окклюзии (archdevelopment, ротация моляров).
Изготовление дистализатора. Применение мини-винтов (практика)."
		),
		3 => array(
			"speaker" => 3,
			"event" => 2642,
			"similar" => "1, 2, 3, 4, 5, 7, 8, 9",
			"theme" => "Мой идеальный план лечения",
			"themedesc" => "- разбор диагностических данных необходимых для точного планирования 
- презентация плана лечения
- работа в Power Pointe и Keynote
- визуальная карта плана лечения
- работа с шаблонами визуальной карты, планами лечения и ортодонтической картой

Вы научитесь:
-структурировать диагностические данные 
-работать с визуальным планом лечения
-составлять наглядный и понятный план лечения для врача и пациента

Вы получите:
- шаблон презентации дигностических данных для пациента
- шаблон визуальной карты с зубами и с необходимым набором символов и значков 
- шаблон ортодонтической карты для удобной повседневной работы на приеме
"
		),
	),
	3 => array(
		1 => array(
			"speaker" => 1,
			"event" => 2641,
			"similar" => "1, 2, 3, 4, 5, 6, 8, 9",
			"theme" => "Первичная консультация пациента с признаками дисфункции ВНЧС",
			"themedesc" => "1. Подробная беседа с пациентом - важнейшая часть консультации! О чем надо обязательно спросить каждого пациента?

2. Анализ движения нижней челюсти и оценка степени открывания рта. Чем отличается дефлексия от девиации и о чем говорит их наличие? Суставные шумы, как разобраться, что они означают?

3. Пальпация ВНЧС, мышц челюстно-лицевой области в покое и при функции. Почему это необходимо проводить каждому ортодонтическому пациенту?

4. Повседневный анализ окклюзии зубных рядов в клинике. Нарушения структуры зубной эмали и слизистой оболочки полости рта, указывающие на дисфункцию жевательного аппарата. Основные понятия биомеханики зубочелюстной системы.

5. В каких случаях ортодонту нужна помощь смежных специалистов?

БОНУС: 

Неврологический осмотр, который может провести любой врач-стоматолог на первичной консультации, чтобы понять, нужна ли пациенту консультация невролога, или будет достаточно стоматологических методов  лечения."
		),
		2 => array(
			"speaker" => 2,
			"event" => 2643,
			"similar" => "1, 2, 3, 4, 5, 6, 7, 9",
			"theme" => "Движение вперед или назад. Современный взгляд на лечение подростков с дистальным прикусом",
			"themedesc" => "Коротко о причинах и путях развития второго класса.
Диагностика: лицо, челюсти и зубы - выделяем главное.
Damon System - как инструмент расширения и создания условий для лечения дистальной окклюзии (archdevelopment, ротация моляров).
Изготовление дистализатора. Применение мини-винтов (практика)."
		),
		3 => array(
			"speaker" => 3,
			"event" => 2642,
			"similar" => "1, 2, 3, 4, 5, 6, 7, 8",
			"theme" => "Мой идеальный план лечения",
			"themedesc" => "- разбор диагностических данных необходимых для точного планирования 
- презентация плана лечения
- работа в Power Pointe и Keynote
- визуальная карта плана лечения
- работа с шаблонами визуальной карты, планами лечения и ортодонтической картой

Вы научитесь:
-структурировать диагностические данные 
-работать с визуальным планом лечения
-составлять наглядный и понятный план лечения для врача и пациента

Вы получите:
- шаблон презентации дигностических данных для пациента
- шаблон визуальной карты с зубами и с необходимым набором символов и значков 
- шаблон ортодонтической карты для удобной повседневной работы на приеме
"
		),
	),
);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Рождественские встречи с ORMCO 2019</title>
		<meta http–equiv="Content–Type" content="text/html; charset=UTF–8">
		<link rel="stylesheet" media="screen" type="text/css" href="main.css">
		<meta name="keywords" content="">
		<meta name="description" content="">
		<meta name="HandheldFriendly" content="true">
		<meta name="MobileOptimized" content="width">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="/favicon.gif" type="image/x-icon">
		<link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="js/jquery.touchSwipe.min.js" type="text/javascript"></script>
		<script src="js/ajaxupload.3.5.js" type="text/javascript"></script>
		<script src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyBoDhMbGeceqxv4_7SROd3YL1-0C-SyBGo" type="text/javascript"></script>
		<script>
			var pop1 = 0;
			var userid = "";
			/*
			function parallaxScroll(){
				var scrolled = $(window).scrollTop();
				$('#parallax-bg1').css('top',(0-(scrolled*.25))+'px');
				$('#parallax-bg2').css('top',(0-(scrolled*.5))+'px');
				$('#parallax-bg3').css('top',(0-(scrolled*.75))+'px');
				// $('body').css('background-position', '50% ' + (0-(scrolled*.75))+'px');
			}
			*/
			function places(num) {
				rest = num % 10;
				if (num > 4 && num < 21)
					return "мест";
				else {
					if (num > 1 && num < 5)
						return "места";
					else {
						if (num == 1 || rest == 1)
							return "место";
						else {
							if (num > 20 && rest > 1 && rest < 5)
								return "места";
							else
								return "мест";
						}
					}
				}
			}
			function remember_pass() {
				var e_email = $("#e_email").val();
				if (isEmail(e_email)) {
					$.ajax({
						url:"https://orthodontia.ru/udata/emarket/forget_pas/" + e_email + "/.json",
						xhrFields: {
						  withCredentials: true
						},
						dataType: "json",
						success:function(data) {
							if (data.status == "success") {
								$("#rp_ok").fadeIn(300);
								setTimeout(function() {$("#rp_ok").fadeOut(300)}, 15000);
							}
							else {
								$("#rp_fail").text("Пользователь не найден в базе, попробуйте еще раз").fadeIn(300);
								setTimeout(function() {$("#rp_fail").fadeOut(300)}, 15000);
							}
						}
					});
				}
				else {
					$("#rp_fail").text("Укажите свой email!").fadeIn(300);
					setTimeout(function() {$("#rp_fail").fadeOut(300)}, 15000);
				}
			}
			function countdown() {
				var mcl1 = $("#mcl1").val();
				var mcl2 = $("#mcl2").val();
				$.ajax({
					type: "POST",
					url: "xp_countdown.php",
					timeout: 5000,
					data: "mcl1=" + mcl1 + "&mcl2=" + mcl2,
					success: function(data) {
					},
					error: function(data) {
					}
				});
			}
			function writedata() {
				var alldata = "";
				$('form input').each(function() {
					if ($(this).attr("id") != undefined)
						alldata += "&" + $(this).attr("id") + "=" + $(this).val();
				});
				$('form select').each(function() {
					if ($(this).attr("id") != undefined)
						alldata += "&" + $(this).attr("id") + "=" + $(this).val();
				});
				$.ajax({
					type: "POST",
					url: "xp_write.php",
					timeout: 5000,
					data: "alldata=" + alldata,
					success: function(data) {
					},
					error: function(data) {
					}
				});
			}
			function setdiscount(discount, element, chk) {
				$(".dscnt").not("#" + element).removeAttr("checked");
				var pr = $("#regularprice").val();
				var eventid = $("#events").val();
				if (chk) {
					$("#discount").text(discount);
					$(".discount").fadeIn(300);
					pr = pr - parseInt(pr*(discount/100));
					// указываем кол-во звезд, которые можно получить
					$.ajax({					
						url:"https://orthodontia.ru/udata/custom/outputEventDiscount/" + eventid + "/.json",
						dataType: "json",
						success:function(data) {
							var bonuses = 0;
							var freez = 0;
							if (typeof data.item !== "undefined") {
								bonuses = data.item[0].loyalty_bonus;
								freez = data.item[0].freez_ormco_star;
								for (var key in data.item) {
									if (data.item[key].id == "discount_" + element) {
										bonuses = data.item[key].loyalty_bonus;
										freez = data.item[key].freez_ormco_star;
									}
								}
							}
							$("#bonuses").text(bonuses);
							$("#potential_ormco_star").val(bonuses);
							$("#freez_ormco_star").val(freez);
						}
					});
				}
				else {
					$(".discount").fadeOut(300);
					$(".newform_upload").fadeOut(300);
					$("#uploaded").fadeOut(300);
					// указываем кол-во звезд, которые можно получить
					$.ajax({					
						url:"https://orthodontia.ru/udata/custom/outputEventDiscount/" + eventid + "/.json",
						dataType: "json",
						success:function(data) {
							var bonuses = 0;
							var freez = 0;
							if (typeof data.item !== "undefined") {
								bonuses = data.item[0].loyalty_bonus;
								freez = data.item[0].freez_ormco_star;
							}
							$("#bonuses").text(bonuses);
							$("#potential_ormco_star").val(bonuses);
							$("#freez_ormco_star").val(freez);
						}
					});
				}
				$("#finalprice").text(pr + " р.");
			}
			function addnewyur() {
				$(".yurs").fadeOut(300);
				$(".newform_reg").fadeOut(300);
				$(".addyur").fadeOut(300, function() {
					$(".addyurform1").fadeIn(300);
				});
			}
			function addyurajax() {
				var tosend = "id=" + userid;
				$('.addyurform input').each(function(index) {
					var did = $(this).attr("id");
					if (did == "yurname")
						did = "name";
					if (did == "yuremail")
						did = "email";
					var dval = $(this).val();
					tosend += "&data[new][" + did + "]=" + dval;
				});
				$('.addyurform textarea').each(function(index) {
					var did = $(this).attr("id");
					var dval = $(this).val();
					tosend += "&data[new][" + did + "]=" + dval;
				});
				$.ajax({
					url:"https://orthodontia.ru/udata/emarket/add_legal_item/.json?" + tosend,
					xhrFields: {
					  withCredentials: true
					},
					dataType: "json",
					success:function(data) {
						if (data.result != 0) {
							$(".addyurform").fadeOut(300).promise().done(function() {
								// добавляем новое юр. лицо в select
								$('#yur').append($('<option>', { 
										value: data.result,
										text : $("#yurname").val()
								}));
								$(".newform_reg").fadeIn(300);
								$(".addyur").fadeIn(300);
								$(".yurs").fadeIn(300);
							});
						}
						else {
							$(".addyurform").fadeOut(300, function() {
								$(".newform_reg").fadeIn(300);
								$(".addyur").fadeIn(300);
								$(".yurs").fadeIn(300);
							});
							alert("Новое юр. лицо не добавлено! Проверьте данные!");
						}
					}
				});
			}
			function showyur(n) {
				var foo = 0;
				if (n == 1) {
					$(".addyurform1 .ness").each(function() {
						if ($(this).val() == '') {
							if (foo == 0) {
								$('html,body').animate({scrollTop: $(this).offset().top - 100}, 500);
							}
							$(this).addClass('redfld');
							foo = 1;
						}
						else {
							$(this).removeClass('redfld');
						}
					});
				}
				if (foo == 0) {
					$(".addyurform" + (3 - n)).fadeOut(300, function() {
						$(".addyurform" + n).fadeIn(300);
					});
				}
			}
			function showtab(tab) {
				if (tab == 1)
					$("#yur").attr("disabled", "disabled");
				else
					$("#yur").removeAttr("disabled");
				$(".tab" + (3 - tab)).fadeOut(300);
				$(".tab" + tab).fadeIn(300);
				$(".tabbut" + (3 - tab)).removeClass("active");
				$(".tabbut" + tab).addClass("active");
			}
			function showpart(part) {
				if (chkform(2)) {
					if (userid == "") {
						$.ajax({
							url:"https://orthodontia.ru/udata/emarket/logout_user/.json",
							xhrFields: {
							  withCredentials: true
							},
							dataType: "json",
							success:function(data) {
								// авторизация на сайте
								var tosend = "id=" + userid;
								$('.part1 input').each(function(index) {
									var did = $(this).attr("id");
									var dval = $(this).val();
									tosend += "&data[new][" + did + "]=" + dval;
								});
								$('.part1 select').each(function(index) {
									var did = $(this).attr("id");
									var dval = $(this).val();
									tosend += "&data[new][" + did + "]=" + dval;
								});
								$.ajax({
									url:"https://orthodontia.ru/udata/users/lpreg/.json?" + tosend,
									dataType: "json",
									success:function(data) {
										$(".f_anim").css("opacity", 0);
										if (data.status == "successful") {
											// пользователь зарегистрирован
											user_id = data.user_id;
											// отправляем данные в GA
											var utype = $("#who").val();
											var usertype = 'not a doctor';
											if (utype == "Ортодонт")
												usertype = "orthodontist";
											if (utype == "Хирург")
												usertype = "surgeon";
											if (utype == "Другая специализация")
												usertype = "other";
											console.log("usertype: " + usertype);
											ga('send', 'event', 'form', 'registration', usertype);
											yaCounter23063434.reachGoal('UserRegistration');
											$.ajax({
												url:"https://orthodontia.ru/udata/emarket/logout_user/.json",
												xhrFields: {
												  withCredentials: true
												},
												dataType: "json",
												success:function(data) {
													var newemail = $("#email").val();
													var newpassword = $("#password").val();
													$.ajax({
														xhrFields: {
														  withCredentials: true
														},
														dataType: "json",
														url:"https://orthodontia.ru/udata/emarket/get_user_info/" + newemail + "/" + newpassword + "/.json",
														success:function(data) {
															$(".part" + (3 - part)).fadeOut(300);
															$(".part" + part).fadeIn(300);
															$(".partbut" + (3 - part)).addClass("hidden");
															$(".partbut" + part).removeClass("hidden");
															$(".notfound").hide(0);
														}
													});
												}
											});
										}
										else {
											// неудачно, рисуем ошибку
											$("#form_error").html(data.result);
											$(".mask1").fadeIn(300);
											$(".form_error").fadeIn(300);
										}
									}
								});
							}
						});
					}
					else {
						$(".part" + (3 - part)).fadeOut(300);
						$(".part" + part).fadeIn(300);
						$(".partbut" + (3 - part)).addClass("hidden");
						$(".partbut" + part).removeClass("hidden");
					}
				}
			}
			function chk_entry() {
				$("#entry_but").text("Проверяем...").attr("disabled", "disabled");
				$(".wrong").fadeOut(300);
				var e_email = $("#e_email").val();
				var e_pass = $("#e_pass").val();
				var eventid = $("#events").val();
				$.ajax({
					url:"https://orthodontia.ru/udata/emarket/logout_user/.json",
					xhrFields: {
					  withCredentials: true
					},
					dataType: "json",
					success:function(data) {
						$.ajax({
							xhrFields: {
							  withCredentials: true
							},
							dataType: "json",
							url:"https://orthodontia.ru/udata/emarket/get_user_info/" + e_email + "/" + e_pass + "/.json",
							success:function(data) {
								$(".f_anim").css("opacity", 0);
								if (typeof data.extended === 'object') {
									// пользователь ввел правильные данные
									console.log("user auth");
									// отправляем данные в GA
									ga('send', 'event', 'form', 'authorization');
									yaCounter23063434.reachGoal('UserAuthorization');
									var user = data;
									$(".wrong").fadeOut(300);
									$.ajax({
										url:"https://orthodontia.ru/udata/emarket/is_first_purchase/" + e_email + "/" + eventid + "/.json",
										xhrFields: {
										  withCredentials: true
										},
										dataType: "json",
										success:function(data) {
											$(".f_anim").css("opacity", 0);
											if (data.result == 1) {
												// пользователь уже зарегистрирован на мероприятие, выводим надпись
												$(".exists").css("opacity", 1);
											}
											else {
												userid = user.id;
												// alert(userid);
												// пользователь еще не зарегистрирован, все ОК, рисуем форму регистрации на мероприятие
												$(".openform").fadeOut(300, function() {
													$(".formbox2").hide(0);
													// console.table(user);
													// вставляем поля
													arruser = new Array();
													var allprops = user.extended.groups.group[0].property;
													$.each(allprops, function(index, value) {
														// console.log(this.name);
														arruser[this.name] = this.value;
													});
													if (typeof user.extended.groups.group[1] == "object") {
														var allprops = user.extended.groups.group[1].property;
														$.each(allprops, function(index, value) {
															// console.log(this.name);
															arruser[this.name] = this.value;
														});
													}
													// console.table(arruser);
													$("#fname").val(arruser["fname"].value);
													$("#lname").val(arruser["lname"].value);
													$("#father_name").val(arruser["father_name"].value);
													$("#email").val(arruser["e-mail"].value);
													$("#phone").val(arruser["phone"].value);
													$("#city").val(arruser["city"].value);
													$("#company").val(arruser["company"].value);
													$("#bd").val(arruser["bd"].value);
													$("#country").val(arruser["country"].item.name);
													$("#region").val(arruser["region"].item.id);
													if (typeof arruser["prof_status"] == "object")
														$("#who").val(arruser["prof_status"].item.name);
													// убираем пароли из формы
													$(".phide").hide();
													// рисуем форму
													$(".formbox3").show(0);
													$(".openform").fadeIn(300);
												});
												// подгружаем юр. лиц
												$.ajax({
													url:"https://orthodontia.ru/udata/emarket/legalList/.json",
													xhrFields: {
													  withCredentials: true
													},
													dataType: "json",
													success:function(data1) {
														// alert(data1.items.length);
														// добавляем юр. лица в select
														$.each(data1.items.item, function(index, value) {
															$('#yur').append($('<option>', { 
																	value: value.id,
																	text : value.name 
															}));
														});
														// console.table(data1.items);
													}
												});
											}
										},
										error:function(data) {
										}
									});
								}
								else {
									// пользователь ввел неверные данные
									$("#e_email").addClass("invalid");
									$("#e_pass").addClass("invalid");
									$(".wrong").fadeIn(300, function() {
										$("#entry_but").text("Войти").removeAttr("disabled");
									});
								}
							}
						});
					}
				});
			}
			function fixbody() {
				$("body").addClass("hold");
			}
			function unfixbody() {
				$("body").removeClass("hold");
			}
			function chg_passstate() {
				if ($("#e_pass").attr("type") == "password") {
					$("#e_pass").attr("type", "text");
				}
				else {
					$("#e_pass").attr("type", "password");
				}
			}
			function chk_f_email() {
				var f_email = $("#f_email").val();
				var eventid = $("#events").val();
				if (f_email != "" && isEmail(f_email)) {
					$(".exists").fadeOut(300);
					$(".f_anim").css("opacity", 1);
					$("#f_email").removeClass("invalid");
					// проверяем email
					$.ajax({
						url:"https://orthodontia.ru/udata/emarket/is_user_exist/" + f_email + "/.json",
						dataType: "json",
						success:function(data) {
							$(".f_anim").css("opacity", 0);
							if (data.result == 1) {
								// пользователь найден, проверяем, есть ли регистрация на мероприятие
								$.ajax({
									url:"https://orthodontia.ru/udata/emarket/is_first_purchase/" + f_email + "/" + eventid + "/.json",
									dataType: "json",
									success:function(data) {
										$(".f_anim").css("opacity", 0);
										if (data.result == 1) {
											// пользователь уже зарегистрирован на мероприятие, выводим надпись
											$(".exists").fadeIn(300);
										}
										else {
											// еще не зарегистрирован, рисуем форму входа
											$(".openform").fadeOut(300, function() {
												$("#email").val($("#f_email").val());
												$(".formbox1").hide(0);
												$("#e_email").val(f_email);
												$(".formbox2").show(0);
												$(".openform").fadeIn(300);
											});
										}
									}
								});
							}
							else {
								// пользователь не найден, отображаем форму регистрации пользователя
								$(".openform").fadeOut(300, function() {
									$("#email").val($("#f_email").val());
									$(".notfound").show(0);
									$(".formbox1").hide(0);
									$(".formbox3").show(0);
									$(".openform").fadeIn(300);
								});
							}
						},
						error:function(data) {
						}
					});
				}
				else {
					$("#f_email").addClass("invalid");
				}
			}
			jQuery(function($){
				$.datepicker.regional['ru'] = {
					closeText: 'Закрыть',
					prevText: '&#x3C;Пред',
					nextText: 'След&#x3E;',
					currentText: 'Сегодня',
					monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
					'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
					monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
					'Июл','Авг','Сен','Окт','Ноя','Дек'],
					dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
					dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
					dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
					weekHeader: 'Нед',
					dateFormat: 'yy-mm-dd',
					firstDay: 1,
					isRTL: false,
					showMonthAfterYear: false,
					yearSuffix: ''};
				$.datepicker.setDefaults($.datepicker.regional['ru']);
			});
			$(function() {
				$(".date1").datepicker({
					changeYear: true,
					yearRange: '1920:2020',
					defaultDate: '1980-01-01'
				});
			});
			lnks = new Array("price", "place", "speakers", "programm", "about");
			var gmargin = 797;
			var curgal = 1;
			var maxx = 6;
			var gmargin1 = 797;
			var curgal1 = 1;
			var maxx1 = 12;
			var igos = 0;
			var totigos = 1;
			var prods = 0;
			var filedone = 0;
			function pset(ord, val, state) {
				if (state) {
					prods++;
					$("#p" + ord).val(val);
					$(".prodsel span").text("Выбрано " + prods);
				}
				else {
					prods--;
					$("#p" + ord).val("");
					if (prods == 0)
						$(".prodsel span").text("-------");
					else
						$(".prodsel span").text("Выбрано " + prods);
				}
			}
			function igo(group, ord, theme, state, eventid = <?=$event_id[0];?>, similar = "") {
				if (state) {
					igos++;
					var arr_s = similar.split(",");
					$("#events").val(eventid);
					$(".prog[data-group=" + group + "]").addClass("faded").find(".igo").fadeOut(200);
					$(".prog[data-mk=" + ord + "]").removeClass("faded").find(".igo").fadeIn(200);
					$(".igoset[data-group=" + group + "]").addClass("faded").find("input").attr("disabled", "disabled");
					$(".igoset[data-mk=" + ord + "]").removeClass("faded").find("input").removeAttr("disabled").attr("checked", "checked");;
					// $.each(arr_s, function(key, val) {
						// $(".prog[data-mk=" + val + "]").addClass("faded").find(".igo").fadeOut(200);
						// $(".igoset[data-mk=" + val + "]").removeClass("faded").find("input").removeAttr("disabled").attr("checked", "checked");
					// })
					$("#mk" + ord).parent().addClass("sel");
					$("#lf" + ord).text("Пойду!");
					$(".mk_" + ord).attr("checked", "checked");
					$(".mk_" + ord).parent().addClass("sel");
					$("#lf_" + ord).text("Пойду!");
					var html = "<div id=\"chosen" + ord + "\" class=\"chitem group\"><div class=\"igonum\">" + igos + "</div><div class=\"igodesc\" id=\"igod" + ord + "\">" + theme + "</div><!--<div class=\"igodel\" onClick=\"igo(" + group + ", " + ord + ", '', false);\"><span>X</span> Удалить</div>--></div>";
					$(".chosen").append(html);
					$("#mclasses" + group).val(theme);
					$("#mcl" + group).val(ord);
					// для прокрутки вниз при выборе первого мастер-класса
					// if (igos == 1)
						// $('html,body').animate({scrollTop: $(".prog[data-group=" + (3 - group) + "]").offset().top - 200}, 500);
					if (igos >= totigos) {
						fixbody();
						$('.mask').fadeIn(300);
						$('.openform').fadeIn(300);
					}
				}
				else {
					igos--;
					var arr_s = similar.split(",");
					$(".prog[data-group=" + group + "]").removeClass("faded").find(".igo").fadeIn(200);
					$(".igoset[data-group=" + group + "]").not(".cancelled").removeClass("faded").find("input").removeAttr("disabled");
					// $.each(arr_s, function(key, val) {
						// $(".prog[data-mk=" + val + "]").removeClass("faded").find(".igo").fadeIn(200);
						// $(".igoset[data-mk=" + val + "]").not(".cancelled").removeClass("faded").find("input").removeAttr("disabled");
					// })
					$(".igoset[data-mk=" + ord + "]").find("input").removeAttr("checked");
					$("#mk" + ord).parent().removeClass("sel");
					$("#lf" + ord).text("Зарегистрироваться");
					$(".mk_" + ord).removeAttr("checked");
					$(".mk_" + ord).parent().removeClass("sel");
					$("#lf_" + ord).text("Зарегистрироваться");
					$("#chosen" + ord).remove();
					$('.chitem').each(function(index) {
						$(this).find(".igonum").text(index + 1);
					});
					$("#mclasses" + group).val("");
					$("#mcl" + group).val("");
				}
			}
			function openmob(n) {
				var wdth = $(window).width();
				if (wdth < 960) {
					$("#event" + n).slideToggle(300);
					$("#prog" + n).toggleClass("open");
				}
			}
			function isEmail(email) {
				var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				return regex.test(email);
			}
			function noplaces() {
				$('.mask').fadeIn(300);
				$('#noplaces').fadeIn(300);
			}
			function chkigos() {
				if (igos < totigos) {						
					$('.prog1').fadeOut(200);
					$('.progtab.sel').removeClass('sel');
					$('.progtab2').addClass('sel');
					$('div[data-date=15]').fadeOut(300);
					$('h2[data-date=15]').fadeOut(300);
					$('.progspace').fadeIn(300);
					$('.mktime').css('display', 'table');
					$('div[data-date=16]').fadeIn(300);
					$('h2[data-date=16]').fadeIn(300);
					$('.prog2').fadeIn(200).promise().done(function() {
						$('html,body').animate({scrollTop: $(".prog2").offset().top - 100}, 500);
					});
					if (pop1 == 0) {
						$('.mask').fadeIn(300);
						$('#popup').fadeIn(300);
					}
					return false;
				}
				else
					return true;
			}
			function chkform(chkfile) {
				var foo = 0;
				if (chkfile == 1) {
					$("#register").attr("disabled", "disabled");
					var pr = $("#finalprice").val();
					pr = pr.replace(' p.', '');
					// отправляем данные в GA
					ga('send', 'event', 'ET', 'event_click', 'LP');
					yaCounter23063434.reachGoal('EventClick');
					dataLayer.push({
						"ecommerce": {
							"add": {
								"products": [
									{
									"name" : "<?=$event_name;?>",
									"price": pr,
									"category": "Платное",
									"quantity": 1
									}
								]
							}
						}
					});
				}
				if (chkfile != 0 && filedone == 0 && $('.upl:checked').length > 0)
					{
					if (foo == 0) {
						$('html,body').animate({scrollTop: $('.upl').offset().top - 100}, 500);
					}
					$('#upload').addClass('redfld');
					foo = 1;
				}
				$('.ness:visible').each(function() {
					if ($(this).val() == '') {
						if (foo == 0) {
							$('html,body').animate({scrollTop: $(this).offset().top - 100}, 500);
						}
						$(this).addClass('redfld');
						foo = 1;
					}
					else {
						$(this).removeClass('redfld');
					}
				}); 
				$('#email').each(function() {
					if (!isEmail($(this).val())) {
						if (foo == 0) {
							// alert($(this).offset().top);
							$('html,body').animate({scrollTop: $(this).offset().top - 100}, 500);
						}
						$(this).addClass('redfld');
						foo = 1;
					}
					else {
						$(this).removeClass('redfld');
					}
				});
				/*
				if (foo == 0) {
					if (($("#events").val() == "Лекции и мастер-классы" || $("#events").val() == "Только лекция 15 декабря") && igos < 3) {
						$('html,body').animate({scrollTop: $(".igosel").offset().top - 100}, 500);
						$(".igosel").addClass('redfld');
						foo = 1;
					}
					else {
						$(".igosel").removeClass('redfld');
					}
				}
				*/
				if (foo == 0) {
					if (userid != "" && chkfile == 1) {
						countdown();
						writedata();
						var eventid = $("#events").val();
						ga('send', 'event', 'ET', 'event_registration', 'LP');
						ga('ec:addProduct', {
						'name': '<?=$event_name;?>',
						'category': 'Платное',
						'price': pr,
						'quantity': 1});
						ga('ec:setAction', 'purchase', {
						'id': eventid,
						'revenue': pr,
						});
						yaCounter23063434.reachGoal('EventRegistration');
						dataLayer.push({
							"ecommerce": {
								"purchase": {
									"actionField": {
										"id" : eventid
									},
									"products": [{
										"name" : "<?=$event_name;?>",
										"price": pr
									}]
								}
							}
						});
					}
					return true;
				}
				else {
					$("#register").removeAttr("disabled");
					return false;
				}
			}
			$(window).resize(function() {
				var wdth = $(window).width();
				if (wdth < 960) {
					gmargin = wdth - 20;
					$(".gal_slide").css("width", gmargin);
					var ghght = gmargin/780*330;
					$(".gal_slide").css("height", ghght);
					$(".gallery").css("height", ghght);
					$(".gal_navs").css("top", ghght - 80);
				}
				else
					document.location.reload();
				/*
				var gwdth = $(window).width();
				if (gwdth > 1250)
					gwdth = 1250;
				gmargin = gwdth;
				var ghght = gwdth/780*330;
				if (gwdth < 480)
					$(".prog").css("width", gwdth - 40);
				$(".gal_slide").css("width", gwdth);
				$(".gal_slide").css("height", ghght);
				$(".gallery").css("height", ghght);
				$(".gal_navs").css("top", ghght - 80);
				*/
			})
			// $(window).load(function() {
				// $("#left").css("margin-left", "-1046px");
				// $("#right").css("margin-left", "250px");
			// });
			var spmargin = 0;
			var swidth = 320;
			var curspeaker = 1;
			var prevscrolled = 0;
 			function speaker_left() {
				if (spmargin < 0) {
					spmargin = spmargin + swidth;
					$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 500);
					curspeaker--;
					$(".spbullets").find(".active").removeClass("active");
					$("#spbul_" + curspeaker).addClass("active");
				}
				else {
					$(".speakers").animate({margin: '0 0 0 20'}, 200, function() {
						$(".speakers").animate({margin: '0 0 0 0'}, 200);
					});
				}
			}
 			function speaker_right() {
				var slide_count = $(".speakers").find(".speaker:visible").length;
				var limit = (slide_count - 1) * swidth;
				if (spmargin > -limit) {
					spmargin = spmargin - swidth;
					$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 500);
					$(".spbullets").find(".active").removeClass("active");
					curspeaker++;
					$(".spbullets").find(".active").removeClass("active");
					$("#spbul_" + curspeaker).addClass("active");
				}
				else {
					$(".speakers").animate({margin: '0 0 0 ' + (spmargin - 20)}, 200, function() {
						$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 200);
					});
				}
			}
			function speaker_go(sp) {
				spmargin = - swidth * (sp - 1);
				$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 500);
				$(".spbullets").find(".active").removeClass("active");
				$("#spbul_" + sp).addClass("active");
				curspeaker = sp;
			}
			hghts = new Array();
			$(document).ready(function() {
				// определяем кол-во
				var places_left = "";
				<?
				$ecnt = 0;
				foreach ($event_id as $eid)
					{
					$ecnt++;
					?>
					var eventid = <?=$eid;?>;
					$.ajax({
						url:"https://orthodontia.ru/udata/emarket/avaliable_reserv/" + eventid + "/.json",
						dataType: "json",
						success:function(data) {
							if (places_left != "")
								places_left += "/";
							places_left += data.amount_free;
							var eid = <?=$eid;?>;
							$(".places_left" + eid).text("Осталось " + data.amount_free + " " + places(data.amount_free));
							if (data.amount_free == 0) {
								$(".igoyes" + eid).fadeOut(300).promise().done(function() {
									$(".igonot" + eid).fadeIn(300)
								});
							}
							else {
								$(".igonot" + eid).fadeOut(300).promise().done(function() {
									$(".igoyes" + eid).fadeIn(300)
								});
							}
							<?
							if ($ecnt == 3)
								{
								?>
								setTimeout(function() {
									$.ajax({
										type: "POST",
										url: "xp_places_left.php",
										timeout: 5000,
										data: "num=" + places_left,
										success: function(data) {
											// alert(places_left);
										},
										error: function(data) {
										}
									});
								}, 2000);
								<?
							}
							?>
						}
					});
					<?
				}
				?>
				// определяем возможность скидки и кол-во звезд, которые можно заработать
				$.ajax({
					url:"https://orthodontia.ru/udata/custom/outputEventLoyalty/" + eventid + ".json",
					dataType: "json",
					success:function(data) {
						// неправильно
						// var bonuses = data.add_loyalty.potential_bonus;
						// $("#bonuses").text(bonuses);
						var isallow50 = 0;
						if (typeof data.discount_loyalty !== "undefined")
							isallow50 = data.discount_loyalty.isallow;
						if (isallow50 == 1) {
							$(".allow50").addClass("is-active");
							$("#loyalty").removeAttr("disabled");
						}
					}
				});
				$.ajax({					
					url:"https://orthodontia.ru/udata/custom/outputEventDiscount/" + eventid + "/.json",
					dataType: "json",
					success:function(data) {
						var bonuses = 0;
						var freez = 0;
						if (typeof data.item !== "undefined") {
							bonuses = data.item[0].loyalty_bonus;
							freez = data.item[0].freez_ormco_star;
						}
						$("#bonuses").text(bonuses);
						$("#potential_ormco_star").val(bonuses);
						$("#freez_ormco_star").val(freez);
					}
				});
				if ($(window).width() > 960)
					$(".speaker_desc").niceScroll({cursorborder:"",cursorcolor:"#3a97cc"});
				initialize_map();
				// $(window).bind('scroll',function(e){
					// parallaxScroll();
				// });
				$("body").bind("click", function(e) {
					if ($(e.target).closest(".igosel").length > 0 || $(e.target).closest(".prodsel").length > 0 || $(e.target).closest(".igoset").length > 0 || $(e.target).closest(".prodset").length > 0) {
						return;
					}
					$(".igodd").fadeOut(500);
					$(".igosel").removeClass('sel');
					$(".proddd").fadeOut(500);
					$(".prodsel").removeClass('sel');
				});
				var wdth = $(window).width();
				if (wdth < 960) {
					gmargin = wdth - 20;
					$(".gal_slide").css("width", gmargin);
					var ghght = gmargin/780*330;
					$(".gal_slide").css("height", ghght);
					$(".gallery").css("height", ghght);
					$(".gal_navs").css("top", ghght - 80);
					/*
					$(".speakers").swipe( {
						swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
							speaker_right();
						},
						swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
							speaker_left();
						},
					   threshold: 75
					});
					swidth = wdth;
					$(".speaker").css("width", wdth);
					*/
					$(".gallery").swipe( {
						swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
							gallery_right();
						},
						swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
							gallery_left();
						},
					   threshold: 75
					});
				}
				/*
				maxx = $(".gal_slide").length;
				var gwdth = $(window).width();
				if (gwdth > 1250)
					gwdth = 1250;
				gmargin = gwdth;
				var ghght = gwdth/780*330;
				if (gwdth < 480)
					$(".prog").css("width", gwdth - 40);
				$(".gal_slide").css("width", gwdth);
				$(".gal_slide").css("height", ghght);
				$(".gallery").css("height", ghght);
				$(".gal_navs").css("top", ghght - 80);
				*/
				for (var key in lnks) {
					if ($("[name=" + lnks[key] +"]").length) {
						hghts[key] = $("[name=" + lnks[key] +"]").offset().top;
					}
				}
				var i = document.location.hash.replace("#", "");
				$("a[href*='#']:not([href='#'])").click(function() {
					if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
						|| location.hostname == this.hostname) {
						var target = $(this.hash);
						target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
						   if (target.length) {
							var i = target.attr("name");
							scrolling = 1;
							$("#mobmenu").slideUp(300);
							$('html,body').animate({
								 scrollTop: target.offset().top - 120
							}, 1000, function() {
								scrolling = 0;
								// alert(target.offset().top);
							});
							$(".ch").removeClass("ch");
							$(this).parent().addClass("ch");
							// history.pushState(null, i, i);
							return false;
						}
					}
				});
				prevscrolled = window.pageYOffset || document.documentElement.scrollTop;
			});
			function gallery_go(n) {
				curgmargin = -gmargin * (n - 1);
				$(".gal_slider").animate({marginLeft: curgmargin});
				curgal = n;
			}
			function gallery_left() {
				if (curgal > 1) {
					gallery_go(curgal - 1);
				}
				else {
					gallery_go(maxx);
				}
			}
			function gallery_right() {
				if (curgal < maxx) {
					gallery_go(curgal + 1);
				}
				else {
					gallery_go(1);
				}
			}
			function gallery_go1(n) {
				curgmargin1 = -gmargin1 * (n - 1);
				$(".gal_slider1").animate({marginLeft: curgmargin1});
				curgal1 = n;
			}
			function gallery_left1() {
				if (curgal1 > 1) {
					gallery_go1(curgal1 - 1);
				}
				else {
					gallery_go1(maxx1);
				}
			}
			function gallery_right1() {
				if (curgal1 < maxx1) {
					gallery_go1(curgal1 + 1);
				}
				else {
					gallery_go1(1);
				}
			}
			var scrolling = 0;
			window.onscroll = function() {
				var hght = $('body').height();
				// alert(hght);
				var scrolled = window.pageYOffset || document.documentElement.scrollTop;
				if (scrolling == 0)
					{
					for (var key in hghts)
						{
						if (scrolled >= hghts[key] - 300) {
							$(".ch").removeClass("ch");
							$("#mi" + lnks[key]).addClass("ch");
							$("#mi" + lnks[key] + "1").addClass("ch");
							break;
						}
					}
				}
				var wdth = $(window).width();
				var hght = $(window).height();
				/*
				if (scrolled < 320 && wdth > 960) {
					var leftmarg = scrolled - 1146;
					var rightmarg = 450 - scrolled;
					$("#left").css("margin-left", leftmarg);
					$("#right").css("margin-left", rightmarg);
				}
				*/
				// if (wdth > 960) {
					// console.log(prevscrolled + "|" + scrolled);
					if (scrolled > 90)
						$(".header").addClass("out");
					else
						$(".header").removeClass("out");
					if (scrolled > hght)
						$(".baseinfo").addClass("fixed");
					else
						$(".baseinfo").removeClass("fixed");
				// }
				if (wdth > 960) {
					$(".owl img").css("bottom", -scrolled/8 + "px");
					$(".h1").css("margin-top", scrolled/8 + "px");
					$(".h3").css("margin-top", scrolled/8 + "px");
				}
				prevscrolled = scrolled;
			}
			function reload() {
				var src = document.captcha.src;
				document.captcha.src = '/images/loading.gif';
				document.captcha.src = src + '?rand='+Math.random();
			}
			var x1 = 55.787932;
			var y1 = 37.680616;
			var x2 = 25.217603;
			var y2 = 55.2828107;
			var myLatlng1;
			var myLatlng2;
			var map;
			var marker1;
			var marker2;
			function initialize_map() {
				myLatlng1 = new google.maps.LatLng(x1, y1);
				myLatlng2 = new google.maps.LatLng(x2, y2);
				var mapOptions = {
					center: myLatlng1,
					mapTypeControl:!1,
					streetViewControl:!1,
					scrollwheel:!1,
					panControl:!1,
					draggable:!1,
					zoomControlOptions:{position:google.maps.ControlPosition.LEFT_TOP},
					zoom: 17,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				map = new google.maps.Map(document.getElementById("map"),
					mapOptions);
				marker1 = new google.maps.Marker({
					position: myLatlng1,
					map: map,
					icon: "images/marker.svg",
					title: "отель «Холидей ИНН», Москва"
				});
				var infowindow1 = new google.maps.InfoWindow({
					content: "<div class=\"onmap\"><h3>Холидей ИНН, г. Москва</h3><br />ул. Русаковская, д. 24</div>"
				});
				// marker1.addListener('click', function() {
					infowindow1.open(map, marker1);
				// });
				/*
				marker2 = new google.maps.Marker({
					position: myLatlng2,
					map: map,
					title: "Москва"
				});
				*/
			}
			function map1() {
				map.setCenter(myLatlng1);
			}
			function map2() {
				map.setCenter(myLatlng2);
			}
			// загрузка изображения
			$(function(){
				var btnUploads = $("#upload");
				$(btnUploads).each(function() {
					new AjaxUpload(this, {
						action: "xp_upload.php",
						name: "uploadfile",
						onComplete: function(file, response){
							if (response.replace("Error", "") == response) {
								$("#upload").addClass("done").text("Файл загружен");
								$("#uploaded").html("<img width=\"100%\" src=\"<?=$uploaddir;?>uploads/" + response + "\" alt=\"\" />");
								$("#file").val('<?=$uploaddir;?>uploads/' + response);
								filedone = 1;
							}
							else {
								alert("Файл " + file + " не загружен! Ошибка: " + response.replace("Error", ""));
							}
						}
					});
				})
			});
		</script>
		<script src="js/jquery.nicescroll.min.js" type="text/javascript"></script>
	</head>
	<body>
<script type="text/javascript">
if ($(window).width() > 960) {
	var imgsrc="images/snow.gif";
	var ie4=(document.all)?1:0;
	var ns6=(document.getElementById&&!document.all)?1:0;
	var height=(document.layers)?window.innerHeight:window.document.body.scrollHeight;
	if (height<window.document.body.clientHeight) height=window.document.body.clientHeight;
	var width=(document.layers)?window.innerWidth:window.document.body.clientWidth;
	width=width-50;
	var col=Math.round(height/45);    //количество снежинок

	amp=new Array();
	x_pos=new Array();
	y_pos=new Array();
	stx=new Array();
	sty=new Array();
	deltax=new Array();
	obj=new Array();

	for (i=0; i<col; ++i) {
	 amp[i]=Math.random()*19;
	 x_pos[i]=Math.random()*(width-amp[i]-29);
	 y_pos[i]=Math.random()*height;
	 stx[i]=0.03+Math.random()*0.25;
	 sty[i]=2+Math.random();
	 deltax[i]=0;
	 if (ie4||ns6) {
		document.write("<img id=\"sn"+ i +"\" style=\"position:absolute;" +
		"z-index: "+ i +"; visibility:visible; top:-50px; left:-50px;\"" +
		"src='"+imgsrc+"' border=0>");
	 }
	 obj[i] = document.getElementById("sn"+i);
	}

	function flake() {
	 for (i=0; i<col; ++i) {
		y_pos[i]+=sty[i];
		if (y_pos[i]>height-49) {
			x_pos[i]=Math.random()*(width-amp[i]-29);
			y_pos[i]=0;
		}
		deltax[i]+=stx[i];
		obj[i].style.top=y_pos[i]+"px";
		obj[i].style.left=x_pos[i]+amp[i]*Math.sin(deltax[i])+"px";
	 }
	setTimeout("flake()", 40);
	}
	flake();
}
</script>
		<div class="section header flex flex-vcenter flex-hcenter">
			<div class="logo"><a href="#about"><img src="images/logo.svg" /></a></div>
			<div class="menu">
				<ul>
					<li id="miprogramm"><a href="#programm">Программа</a></li>
					<li id="mispeakers"><a href="#speakers">Спикеры</a></li>
					<li id="miplace"><a href="#place">Место проведения</a></li>
					<li id="miprice"><a href="#price">Стоимость</a></li>
				</ul>
			</div>
			<button type="button" onClick="<?if ($mkcounts[0] > 0 || $mkcounts[1] > 0 || $mkcounts[2] > 0) {?>if (chkigos()) {fixbody(); $('.mask').fadeIn(300); $('.openform').fadeIn(300);}<?}else{?>noplaces();<?}?>">РЕГИСТРАЦИЯ</button>
		</div>
		<div class="container">
			<a name="about"></a>
			<div class="section top">
				<div class="seminar flex">
					<div class="owl"><img src="images/owl.png" /></div>
					<div class="h1">ЧТО?<h1>Рождественские встречи с ORMCO<span>.</span></h1></div>
					<div class="h3">
						<div>
							ГДЕ?
							<h3>Москва, Холидей Инн Сокольники</h3>
						</div>
						<div>
							КОГДА?
							<h3>14 декабря 2019 г.</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="section baseinfo">
				<a name="programm"></a>
				<div class="programm flex flex-hcenter">
					<!-- Программа -->
					<div class="programm_h2">
						<h2>Программа<span>.</span></h2>
					</div>
					<div class="programm_self">
					<div class="prog2"><h3>Обратите внимание!</h3>
<br />
- 3 мастер-класса проходят одновременно в разных аудиториях
<br />
- каждый мастер-класс проходит с 10 до 16 часов;
<br />
- мастер-класс прерывается на кофе-брейки и обед;
<br />
- можно выбрать только 1 мастер-класс;
<br />
- количество мест ограничено.
<br /><br /><h3>Пожалуйста, выберите 1 мастер-класс</h3>
</div>
						<div class="progs_cont">
							<div class="progs">
								<!-- Событие 11 -->
								<div class="prog group" data-date="16" id="prog11" onClick="openmob(11);">
									<div class="event1">
										<time>09:00 – 10:00</time>
										<div class="eshort">Регистрация</div>
									</div>
									<div class="pspeaker group">
										<div class="psimg"><img align="left" src="images/coffee.png" /></div>
										<div class="psd"><h4><time>09:00 – 10:00</time></h4></div>
									</div>
									<div class="event">
										<div class="eshort">Регистрация</div>
									</div>
									<div class="event2" id="event11">
										<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									</div>
								</div>
								<div class="progspace"></div>
								<div class="mktime"><?=$mktimes[1];?> - мастер-классы</div>
								<!-- Мастер-классы 1 -->
								<?
								$mcnt = 0;
								foreach ($mks[1] as $m => $mk)
									{
									$mcnt++;
									?>
									<div class="prog prog_mk group" data-date="16" data-group="1" data-similar="<?=$mk["similar"];?>" data-sp="<?=$mk["speaker"];?>" data-mk="<?=$m;?>" id="prog<?=$m;?>" onClick="openmob(<?=$m + 20;?>);">
										<div class="event1">
											<time><?=$mktimes[1];?></time>
											<div class="psimg"><img align="left" src="images/<?=$speakers[$mk["speaker"]]["image"];?>" /></div>
											<div class="eshort2"><h4><?=$speakers[$mk["speaker"]]["name"];?></h4></div>
											<div class="eshort"><?=$mk["theme"];?><div><h5>Мастер-класс</h5></div></div>
											<div class="psd">
												<div class="mkcount"><span class="places_left<?=$mk["event"];?>"><?if ($mkcounts[$m - 1] > 0) {?>Осталось <?=$mkcounts[$m - 1];?> <?=places($mkcounts[$m - 1]);?><?}else{?>Мест больше нет<?}?></span></div>
											</div>
											<div class="igo">
												<div class="igoyes igoyes<?=$mk["event"];?>"<?if (($mkcounts[$m - 1]) <= 0) {?> style="display: none;"<?}?>><input type="checkbox" class="mk_<?=$m;?>" name="mk_<?=$m;?>" id="mk_<?=$m;?>" onClick="igo(1, <?=$m;?>, '<strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?>', this.checked, <?=$mk["event"];?>, '<?=$mk["similar"];?>');" /><label id="lf_<?=$m;?>" for="mk_<?=$m;?>">Зарегистрироваться</label></div>
												<div class="igonot igonot<?=$mk["event"];?>"<?if (($mkcounts[$m - 1]) > 0) {?> style="display: none;"<?}?>>Мест больше нет</div>
											</div>
										</div>
										<div class="pspeaker group">
											<div class="psimg"><img align="left" src="images/<?=$speakers[$mk["speaker"]]["image"];?>" /></div>
											<div class="psd"><h4><?=str_replace(" ", "<br />", $speakers[$mk["speaker"]]["name"]);?></h4>
												<!--<h5><?=$halls[$mk["hall"]];?></h5>-->
											</div>
										</div>
										<div class="event">
											<div class="mkcount"><span class="places_left<?=$mk["event"];?>"><?if ($mkcounts[$m - 1] > 0) {?>Осталось <?=$mkcounts[$m - 1];?> <?=places($mkcounts[$m - 1]);?><?}else{?>Мест больше нет<?}?></span></div>
											<img id="imgopen<?=$mcnt;?>" src="images/arrow.svg" onClick="$(this).fadeOut(200); $('#imgclose<?=$mcnt;?>').fadeIn(200); $(this).parent().find('.elong').slideDown(300);" />
											<img id="imgclose<?=$mcnt;?>" src="images/arrow2.svg" style="display: none;" onClick="$(this).fadeOut(200); $('#imgopen<?=$mcnt;?>').fadeIn(200); $(this).parent().find('.elong').slideUp(300);" />
											<div class="eshort"><?=$mk["theme"];?><div><h5>Мастер-класс</h5></div></div>
											<div class="elong"><?=nl2br($mk["themedesc"]);?></div>
											<div class="igo">
												<div class="igoyes igoyes<?=$mk["event"];?>"<?if (($mkcounts[$m - 1]) <= 0) {?> style="display: none;"<?}?>><input type="checkbox" class="mk_<?=$m;?>" name="mk_<?=$m;?>" id="mk_<?=$m;?>" onClick="igo(1, <?=$m;?>, '<strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?>', this.checked, <?=$mk["event"];?>, '<?=$mk["similar"];?>');" /><label id="lf_<?=$m;?>" for="mk_<?=$m;?>">Зарегистрироваться</label></div>
												<div class="igonot igonot<?=$mk["event"];?>"<?if (($mkcounts[$m - 1]) > 0) {?> style="display: none;"<?}?>>Мест больше нет</div>
											</div>
										</div>
										<div class="event2" id="event<?=$m + 20;?>">
											<hr class="blue" />
											<div class="elong2"><?=nl2br($mk["themedesc"]);?></div>
										</div>
									</div>
									<?
								}
								?>
								<div class="progspace"></div>
								<!-- Событие 14 -->
								<div class="prog group" data-date="16" id="prog14" onClick="openmob(14);">
									<div class="event1">
										<time>11:30 - 12:00</time>
										<div class="eshort">Кофе-брейк</div>
									</div>
									<div class="pspeaker group">
										<div class="psimg"><img align="left" src="images/coffee.png" /></div>
										<div class="psd"><h4><time>11:30 - 12:00</time></h4></div>
									</div>
									<div class="event">
										<div class="eshort">Кофе-брейк</div>
									</div>
									<div class="event2" id="event14">
										<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									</div>
								</div>
								<div class="progspace"></div>
								<div class="mktime"><?=$mktimes[2];?> - мастер-классы</div>
								<?
								foreach ($mks[2] as $m => $mk)
									{
									$mcnt++;
									?>
									<div class="prog prog_mk group" data-date="16" data-group="1" data-similar="<?=$mk["similar"];?>" data-sp="<?=$mk["speaker"];?>" data-mk="<?=$m;?>" id="prog<?=$m;?>" onClick="openmob(<?=$m + 20;?>);">
										<div class="event1">
											<!--<time><?=$mktimes[2];?></time>-->
											<div class="psimg"><img align="left" src="images/<?=$speakers[$mk["speaker"]]["image"];?>" /></div>
											<div class="eshort2"><h4><?=$speakers[$mk["speaker"]]["name"];?></h4></div>
											<div class="eshort"><?=$mk["theme"];?><div><h5>Мастер-класс</h5></div></div>
											<div class="psd">
												<div class="mkcount"><span class="places_left<?=$mk["event"];?>"><?if ($mkcounts[$m - 1] > 0) {?>Осталось <?=$mkcounts[$m - 1];?> <?=places($mkcounts[$m - 1]);?><?}else{?>Мест больше нет<?}?></span></div>
											</div>
											<div class="igo">
												<div class="igoyes igoyes<?=$mk["event"];?>"<?if (($mkcounts[$m - 1]) <= 0) {?> style="display: none;"<?}?>><input type="checkbox" class="mk_<?=$m;?>" name="mk_<?=$m;?>" id="mk_<?=$m;?>" onClick="igo(1, <?=$m;?>, '<strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?>', this.checked, <?=$mk["event"];?>, '<?=$mk["similar"];?>');" /><label id="lf_<?=$m;?>" for="mk_<?=$m;?>">Зарегистрироваться</label></div>
												<div class="igonot igonot<?=$mk["event"];?>"<?if (($mkcounts[$m - 1]) > 0) {?> style="display: none;"<?}?>>Мест больше нет</div>
											</div>
										</div>
										<div class="pspeaker group">
											<div class="psimg"><img align="left" src="images/<?=$speakers[$mk["speaker"]]["image"];?>" /></div>
											<div class="psd"><h4><?=str_replace(" ", "<br />", $speakers[$mk["speaker"]]["name"]);?></h4>
												<!--<h5><?=$halls[$mk["hall"]];?></h5>-->
											</div>
										</div>
										<div class="event">
											<div class="mkcount"><span class="places_left<?=$mk["event"];?>"><?if ($mkcounts[$m - 1] > 0) {?>Осталось <?=$mkcounts[$m - 1];?> <?=places($mkcounts[$m - 1]);?><?}else{?>Мест больше нет<?}?></span></div>
											<img id="imgopen<?=$mcnt;?>" src="images/arrow.svg" onClick="$(this).fadeOut(200); $('#imgclose<?=$mcnt;?>').fadeIn(200); $(this).parent().find('.elong').slideDown(300);" />
											<img id="imgclose<?=$mcnt;?>" src="images/arrow2.svg" style="display: none;" onClick="$(this).fadeOut(200); $('#imgopen<?=$mcnt;?>').fadeIn(200); $(this).parent().find('.elong').slideUp(300);" />
											<div class="eshort"><?=$mk["theme"];?><div><h5>Мастер-класс</h5></div></div>
											<div class="elong"><?=nl2br($mk["themedesc"]);?></div>
											<div class="igo">
												<div class="igoyes igoyes<?=$mk["event"];?>"<?if (($mkcounts[$m - 1]) <= 0) {?> style="display: none;"<?}?>><input type="checkbox" class="mk_<?=$m;?>" name="mk_<?=$m;?>" id="mk_<?=$m;?>" onClick="igo(1, <?=$m;?>, '<strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?>', this.checked, <?=$mk["event"];?>, '<?=$mk["similar"];?>');" /><label id="lf_<?=$m;?>" for="mk_<?=$m;?>">Зарегистрироваться</label></div>
												<div class="igonot igonot<?=$mk["event"];?>"<?if (($mkcounts[$m - 1]) > 0) {?> style="display: none;"<?}?>>Мест больше нет</div>
											</div>
										</div>
										<div class="event2" id="event<?=$m + 20;?>">
											<hr class="blue" />
											<div class="elong2"><?=nl2br($mk["themedesc"]);?></div>
										</div>
									</div>
									<?
								}
								?>
								<div class="progspace"></div>
								<!-- Событие 16 -->
								<div class="prog group" data-date="16" id="prog16" onClick="openmob(16);">
									<div class="event1">
										<time>13:30 - 14:30</time>
										<div class="eshort">Обед</div>
									</div>
									<div class="pspeaker group">
										<div class="psimg"><img align="left" src="images/lunch.png" /></div>
										<div class="psd"><h4><time>13:30 - 14:30</time></h4></div>
									</div>
									<div class="event">
										<div class="eshort">Обед</div>
									</div>
									<div class="event2" id="event16">
										<div class="psimg"><img align="left" src="images/lunch.png" /></div>
									</div>
								</div>
								<div class="progspace"></div>
								<div class="mktime"><?=$mktimes[3];?> - мастер-классы</div>
								<?
								foreach ($mks[3] as $m => $mk)
									{
									$mcnt++;
									?>
									<div class="prog prog_mk group" data-date="16" data-group="1" data-similar="<?=$mk["similar"];?>" data-sp="<?=$mk["speaker"];?>" data-mk="<?=$m;?>" id="prog<?=$m;?>" onClick="openmob(<?=$m + 20;?>);">
										<div class="event1">
											<!--<time><?=$mktimes[2];?></time>-->
											<div class="psimg"><img align="left" src="images/<?=$speakers[$mk["speaker"]]["image"];?>" /></div>
											<div class="eshort2"><h4><?=$speakers[$mk["speaker"]]["name"];?></h4></div>
											<div class="eshort"><?=$mk["theme"];?><div><h5>Мастер-класс</h5></div></div>
											<div class="psd">
												<div class="mkcount"><span class="places_left<?=$mk["event"];?>"><?if ($mkcounts[$m - 1] > 0) {?>Осталось <?=$mkcounts[$m - 1];?> <?=places($mkcounts[$m - 1]);?><?}else{?>Мест больше нет<?}?></span></div>
											</div>
											<div class="igo">
												<div class="igoyes igoyes<?=$mk["event"];?>"<?if (($mkcounts[$m - 1]) <= 0) {?> style="display: none;"<?}?>><input type="checkbox" class="mk_<?=$m;?>" name="mk_<?=$m;?>" id="mk_<?=$m;?>" onClick="igo(1, <?=$m;?>, '<strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?>', this.checked, <?=$mk["event"];?>, '<?=$mk["similar"];?>');" /><label id="lf_<?=$m;?>" for="mk_<?=$m;?>">Зарегистрироваться</label></div>
												<div class="igonot igonot<?=$mk["event"];?>"<?if (($mkcounts[$m - 1]) > 0) {?> style="display: none;"<?}?>>Мест больше нет</div>
											</div>
										</div>
										<div class="pspeaker group">
											<div class="psimg"><img align="left" src="images/<?=$speakers[$mk["speaker"]]["image"];?>" /></div>
											<div class="psd"><h4><?=str_replace(" ", "<br />", $speakers[$mk["speaker"]]["name"]);?></h4>
												<!--<h5><?=$halls[$mk["hall"]];?></h5>-->
											</div>
										</div>
										<div class="event">
											<div class="mkcount"><span class="places_left<?=$mk["event"];?>"><?if ($mkcounts[$m - 1] > 0) {?>Осталось <?=$mkcounts[$m - 1];?> <?=places($mkcounts[$m - 1]);?><?}else{?>Мест больше нет<?}?></span></div>
											<img id="imgopen<?=$mcnt;?>" src="images/arrow.svg" onClick="$(this).fadeOut(200); $('#imgclose<?=$mcnt;?>').fadeIn(200); $(this).parent().find('.elong').slideDown(300);" />
											<img id="imgclose<?=$mcnt;?>" src="images/arrow2.svg" style="display: none;" onClick="$(this).fadeOut(200); $('#imgopen<?=$mcnt;?>').fadeIn(200); $(this).parent().find('.elong').slideUp(300);" />
											<div class="eshort"><?=$mk["theme"];?><div><h5>Мастер-класс</h5></div></div>
											<div class="elong"><?=nl2br($mk["themedesc"]);?></div>
											<div class="igo">
												<div class="igoyes igoyes<?=$mk["event"];?>"<?if (($mkcounts[$m - 1]) <= 0) {?> style="display: none;"<?}?>><input type="checkbox" class="mk_<?=$m;?>" name="mk_<?=$m;?>" id="mk_<?=$m;?>" onClick="igo(1, <?=$m;?>, '<strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?>', this.checked, <?=$mk["event"];?>, '<?=$mk["similar"];?>');" /><label id="lf_<?=$m;?>" for="mk_<?=$m;?>">Зарегистрироваться</label></div>
												<div class="igonot igonot<?=$mk["event"];?>"<?if (($mkcounts[$m - 1]) > 0) {?> style="display: none;"<?}?>>Мест больше нет</div>
											</div>
										</div>
										<div class="event2" id="event<?=$m + 20;?>">
											<hr class="blue" />
											<div class="elong2"><?=nl2br($mk["themedesc"]);?></div>
										</div>
									</div>
									<?
								}
								?>
								<div class="progspace"></div>
								<!-- Событие 17 -->
								<div class="prog group" data-date="16" id="prog17" onClick="openmob(17);">
									<div class="event1">
										<time>16:00 - 18:00</time>
										<div class="eshort">Орто-игра «Что? Где? Когда?». Фуршет.</div>
									</div>
									<div class="pspeaker group">
										<div class="psimg"><img align="left" src="images/coffee.png" /></div>
										<div class="psd"><h4><time>16:00 - 18:00</time></h4></div>
									</div>
									<div class="event">
										<div class="eshort">Орто-игра «Что? Где? Когда?». Фуршет.</div>
									</div>
									<div class="event2" id="event17">
										<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="section main">
				<!-- Спикеры -->
				<a name="speakers"></a>
				<div class="speakers flex">
					<div class="speakers_h2">
						<h2>Спикеры<span>.</span></h2>
					</div>
					<div class="speakers_self">
						<div class="speakersdiv">
							<div class="flex flex-wrap">
								<?
								$scnt = 0;
								foreach ($speakers as $speaker)
									{
									$scnt++;
									$arr_name = explode(" ", $speaker["name"]);
									?>
									<div class="speaker" onClick="$('.mask').fadeIn(300); $('#spdesc<?=$scnt;?>').fadeIn(300);">
										<img src="images/<?=$speaker["image"];?>" />
										<div class="speakerdata">
											<h4><?=$arr_name[0];?><br /><?=$arr_name[1];?> <?=$arr_name[2];?></h4>
										</div>
									</div>
									<?
								}
								$scnt = 0;
								foreach ($speakers as $speaker)
									{
									$scnt++;
									?>
									<div class="speaker_desc" id="spdesc<?=$scnt;?>">
										<div class="close" onClick="$('.mask').fadeOut(300); $('.speaker_desc').fadeOut(300);"><img src="images/close.png" /></div>
										<h4><?=$speaker["name"];?></h4>
										<?=nl2br($speaker["about"]);?>
									</div>
									<?
								}
								?>
							</div>
						</div>
					</div>
				</div>
				<!-- Место -->
				<a name="place"></a>
				<div class="section place flex">
					<div class="place_h2">
						<hr class="hrtop">
						<h2>Место проведения<span>.</span></h2>
						<h4>Отель Холидей ИНН<br />г. Москва, ул. Русаковская, д. 24</h4>
						<hr class="hrbottom">
					</div>
					<div class="place_self">
						<!--<div class="gallery">
							<div class="gal_slider group">
								<div class="gal_slide"><img src="images/building.jpg" /></div>
								<div class="gal_slide"><img src="images/building1.jpg" /></div>
								<div class="gal_slide"><img src="images/building2.jpg" /></div>
								<div class="gal_slide"><img src="images/building3.jpg" /></div>
								<div class="gal_slide"><img src="images/building4.jpg" /></div>
								<div class="gal_slide"><img src="images/building5.jpg" /></div>
								<div class="gal_slide"><img src="images/building6.jpg" /></div>
							</div>
							<div class="gal_navs">
								<div class="gal_nav" onClick="gallery_left();"><img src="images/arr_left.png" /></div>
								<div class="gal_nav" onClick="gallery_right();"><img src="images/arr_right.png" /></div>
							</div>
						</div>-->
						<div class="map"><div id="map"></div></div>
					</div>
				</div>
				<!-- Стоимость -->
				<a name="price"></a>
				<div class="section prices">
					<div class="prices_ flex">
						<div class="prices_h2">
							<h2>Стоимость и регистрация<span>.</span></h2>
						</div>
						<div class="prices_self">
							<div class="txt">Стоимость участия может измениться в зависимости от даты оплаты. Просим вас не затягивать и позаботиться об оплате мероприятия заблаговременно</div>
							<div class="prices_split flex">
								<div class="price<?if (date("Y-m-d") >= "2019-12-01") {?> opa<?}?>">
									<div class="pr_price">11 000 р.</div>
									<div class="pr_title">при оплате до 1 декабря</div>
								</div>
								<div class="price<?if (date("Y-m-d") < "2019-12-01") {?> opa<?}?>">
									<div class="pr_price">14 000 р.</div>
									<div class="pr_title">при оплате после 1 декабря</div>
								</div>
							</div>
							<div class="prices_discount">
								<h5>-40% ординаторам при предъявлении подтверждающего документа</h5>
								<h5>-50% участникам Школы ортодонтии</h5>
								<h5>-15% членам Профессионального общества ортодонтов при предъявлении подтверждающего документа</h5>
								<h5>-50% по программе лояльности Ormco Stars</h5>
							</div>
						</div>
					</div>
					<div id="igosel">
						<h3>Выберите 1 мастер-класс</h3>
						<div class="igos">
							<?
							foreach ($mks[1] as $m => $mk)
								{
								?>
								<div class="flex flex-vcenter igoset<?if ($mkcounts[$m - 1] <= 0) {?> faded cancelled<?}?>" data-group="1" data-mk="<?=$m;?>" id="igo_<?=$m;?>">
									<div>
										<label for="iset<?=$m;?>"><strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?></label>
									</div>
									<div>
										<input type="checkbox" <?if ($mkcounts[$m - 1] <= 0) {?>disabled <?}?>name="iset<?=$m;?>" id="iset<?=$m;?>" onClick="igo(1, <?=$m;?>, '<strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?>', this.checked, <?=$mk["event"];?>);" />
									</div>
								</div>
								<?
							}
							?>
						</div>
						<button class="wide" type="button" onClick="<?if ($mkcounts[0] > 0 || $mkcounts[1] > 0 || $mkcounts[2] > 0) {?>if (chkigos()) {fixbody(); $('.mask').fadeIn(300); $('.openform').fadeIn(300);}<?}else{?>noplaces();<?}?>">ЗАРЕГИСТРИРОВАТЬСЯ</button>
					</div>
				</div>
			</div>
			<a name="form"></a>
			<div class="section regform flex">
				<div class="regform_h2">
					<h2>Справки<span>.</span></h2>
				</div>
				<div class="regform_self flex">
					<div class="formmiddle">
						По всем вопросам вы можете связаться с нашим менеджером Григорием:
						<div class="regform_phone"><a href="tel:+79266108002">+7&nbsp;(926)&nbsp;610-80-02</a></div>
						<div class="regform_email"><a href="mailto:Grigory.Bugaev@ormco.com">Grigory.Bugaev@ormco.com</a></div>
						<button type="button" onClick="<?if ($mkcounts[0] > 0 || $mkcounts[1] > 0 || $mkcounts[2] > 0) {?>if (chkigos()) {fixbody(); $('.mask').fadeIn(300); $('.openform').fadeIn(300);}<?}else{?>noplaces();<?}?>">ЗАРЕГИСТРИРОВАТЬСЯ</button>
					</div>
					<img src="images/face.jpg" />
				</div>
			</div>
		</div>
		<div class="openform">
			<div class="openform_">
				<div class="newform formbox1">
					<div class="formbox_close" onClick="unfixbody(); $('.mask').fadeOut(300); $('.openform').fadeOut(300);"><span class="shape1"></span><span class="shape2"></span></div>
					<h2>Регистрация</h2>
					<h4>Введите Ваш email. Если он уже есть в нашей базе, регистрация пройдет быстрее</h4>
					<input id="f_email" name="f_email" value="<?=$_SESSION["f_email"];?>" />
					<button type="button" onClick="chk_f_email();">Далее</button>
					<div class="f_anim">
						<div class="at">@</div>
						<div class="arr arr1">←</div>
						<div class="dash arr2"></div>
						<div class="dash arr3"></div>
						<div class="dash arr4"></div>
						<div class="arr arr5">→</div>
						<div class="db">
							<div class="db1"></div>
							<div class="db2"></div>
							<div class="db3"></div>
							<div class="db4"></div>
							<div class="db5"></div>
							<div class="db6"></div>
							<div class="db7"></div>
							<div class="db8"></div>
							<div class="db9"></div>
						</div>
						<div class="f_desc">Проверяем email на наличие в базе данных</div>
					</div>
					<div class="exists">Человек с данным Email уже зарегистрировался на данное мероприятие</div>
				</div>
				<div class="newform formbox2">
					<div class="formbox_close" onClick="unfixbody(); $('.mask').fadeOut(300); $('.openform').fadeOut(300);"><span class="shape1"></span><span class="shape2"></span></div>
					<h2>Вход в систему</h2>
					<h4>Email найден! Пожалуйста, введите пароль от вашей учетной записи Ormco (сайты ormco.ru или orthodontia.ru), чтобы зарегистрироваться на семинар</h4>
					<div class="newform_field group">
						<div class="newform_ttl">Логин (Email):</div>
						<div class="newform_fld"><input id="e_email" name="e_email" value="<?=$_SESSION["f_email"];?>" /></div>
					</div>
					<div class="newform_field group">
						<div class="newform_ttl">Пароль:</div>
						<div class="newform_fld"><input type="password" id="e_pass" name="e_pass" value="" /><img onClick="chg_passstate();" src="images/eye.svg" /></div>
					</div>
					<div class="newform_link"><span onClick="remember_pass();">Вспомнить пароль</span></div>
					<div id="rp_ok" class="form_msg">Письмо с дальнейшими инструкциями по восстановлению пароля отправлены на указанный email</div>
					<div id="rp_fail" class="form_msg"></div>
					<div class="newform_field group">
						<div class="newform_ttl"></div>
						<div class="newform_fld"><button type="button" id="entry_but" onClick="chk_entry();">Войти</button></div>
					</div>
					<div class="exists">Человек с данным Email уже зарегистрировался на данное мероприятие</div>
					<div class="wrong">Пароль или логин указаны неверно</div>
				</div>
				<div class="newform formbox3">
					<div class="formbox_close" onClick="unfixbody(); $('.mask').fadeOut(300); $('.openform').fadeOut(300);"><span class="shape1"></span><span class="shape2"></span></div>
					<h2>Регистрация на мероприятие</h2>
					<div class="notfound red hidden">Пользователь не найден в базе данных. Вы можете зарегистрироваться на мероприятие, заполнив форму ниже:</div>
					<form action="https://orthodontia.ru/emarket/lporderSmart/" method="post" onSubmit="return chkform(1);" enctype="multipart/form-data">
					<input type="hidden" name="regularprice" id="regularprice" value="<?if (date("Y-m-d") < "2019-12-01") {?>11000<?}else{?>14000<?}?>" />
					<input type="hidden" name="mclasses1" id="mclasses1" value="" />
					<input type="hidden" name="mcl1" id="mcl1" value="" />
					<input type="hidden" name="file" id="file" value="" />
					<input type="hidden" name="events" id="events" value="<?=$event_id[0];?>" />
					<input type="hidden" name="potential_ormco_star" id="potential_ormco_star" value="" />
					<input type="hidden" name="freez_ormco_star" id="freez_ormco_star" value="" />
					<div class="partbuts">
						<!--<div class="partbut active partbut1" onClick="showpart(2);">Личные данные</div> → <div class="partbut partbut2" onClick="showpart(2);">Способ оплаты</div>-->
						<h4 class="partbut1">Личные данные</h4>
						<h4 class="partbut2 hidden">Способ оплаты</h4>
					</div>
					<hr class="hr">
					<div class="part1">
						<div class="newform_field group">
							<div class="newform_ttl">Email <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="email" id="email" /></div>
						</div>
						<div class="newform_field group phide">
							<div class="newform_ttl"></div>
							<div class="newform_fld small">На этот email будет отправлено подтверждение регистрации и ссылка для активации вашей учетной записи Ormco</div>
						</div>
						<div class="newform_field group phide">
							<div class="newform_ttl">Пароль <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="password" size="30" name="password" id="password" /></div>
						</div>
						<div class="newform_field group phide">
							<div class="newform_ttl">Пароль еще раз <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="password" size="30" name="password2" id="password2" /></div>
						</div>
						<br />
						<div class="newform_field group">
							<div class="newform_ttl">Фамилия <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="lname" id="lname" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Имя <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="fname" id="fname" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Отчество <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="father_name" id="father_name" /></div>
						</div>
						<div class="newform_field group" id="birthday">
							<div class="newform_ttl">Дата рождения<sup>*</sup></div>
							<div class="newform_fld"><input placeholder="" class="date1 ness" type="text" size="20" name="bd" id="bd" readonly /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Телефон <sup>*</sup></div>
							<div class="newform_fld"><input placeholder="+7 (987) 1234567" class="short ness" type="text" size="25" name="phone" id="phone" /></div>
						</div>
						<!--<div class="newform_field group">
							<div class="newform_ttl big">Специальность для получения баллов по НМО<sup>*</sup></div>
							<div class="newform_fld"><select name="spec" id="spec" class="ness">
									<option value="">-= выберите Вашу специальность =-</option>
									<option value="Ортодонтия">Ортодонтия</option>
									<option value="Стоматология терапевтическая">Стоматология терапевтическая</option>
									<option value="Стоматология общей практики">Стоматология общей практики</option>
								</select>
							</div>
						</div>-->
						<div class="newform_field group">
							<div class="newform_ttl">Страна<sup>*</sup></div>
							<div class="newform_fld"><select name="country" id="country" class="ness" onChange="if (this.value == 'Россия') {$('.region_field').fadeIn(300);}else{$('.region_field').fadeOut(300);}">
									<option value="Россия">Россия</option>
									<option value="Азербайджан">Азербайджан</option>
									<option value="Армения">Армения</option>
									<option value="Белоруссия">Белоруссия</option>
									<option value="Грузия">Грузия</option>
									<option value="Украина">Украина</option>
									<option value="Другая">Другая</option>
								</select>
							</div>
						</div>
							<div class="newform_field group region_field">
								<div class="newform_ttl">Регион <sup>*</sup></div>
								<div class="newform_fld"><select name="region" id="region" class="ness">
									<option value="">-= выберите регион =-</option>
									<option value="9870">Москва и МО</option>
									<option value="9871">Санкт-Петербург и ЛО</option>
									<option value="9872">Северо-Западный</option>
									<option value="9873">Центральный</option>
									<option value="9874">Сибирский</option>
									<option value="9875">Приволжский</option>
									<option value="9876">Южный</option>
									<option value="9877">Северо-Кавказский</option>
									<option value="9878">Уральский</option>
									<option value="9879">Дальневосточный</option>
									<option value="16761">Не Россия</option>
								</select></div>
							</div>
						<div class="newform_field group">
							<div class="newform_ttl">Город<sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="city" id="city" /></div>
						</div>
						<div class="newform_field group" id="speciality">
							<div class="newform_ttl">Кто Вы? <sup>*</sup></div>
							<div class="newform_fld"><select name="who" id="who" class="ness">
									<option value="">Кто Вы?</option>
									<option value="Ортодонт">Ортодонт</option>
									<option value="Хирург">Хирург</option>
									<option value="Другая специализация">Другая специализация</option>
									<option value="Не врач">Не врач</option>
								</select>
							</div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Клиника/ВУЗ<sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="company" id="company" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl"></div>
							<div class="newform_fld"><button type="button" id="entry_but" onClick="showpart(2);">Далее</button></div>
						</div>
					</div>
					<div class="part2 hidden">
						<h3>Выберите скидку. Обратите внимание: скидка будет направлена менеджеру Ormco для подтверждения</h3>
						<div class="newform_field group">
							<div class="newform_flda"><input type="checkbox" class="dscnt upl" name="poo" id="poo" value="Член ПОО" onClick="$('.newform_upload').fadeIn(300); $('#uploaded').fadeIn(300); setdiscount(15, 'poo', this.checked);" /> <label for="poo">Являюсь членом Профессионального общества ортодонтов</label></div>
						</div>
						<div class="newform_field group">
							<div class="newform_flda"><input type="checkbox" class="dscnt" name="school" id="school" value="Действующий участник Школы ортодонтии" onClick="$('.newform_upload').fadeOut(300); $('#uploaded').fadeOut(300); setdiscount(50, 'school', this.checked);" /> <label for="school">Действующий участник Школы ортодонтии</label></div>
						</div>
						<div class="newform_field group">
							<div class="newform_flda"><input type="checkbox" class="dscnt upl" name="ordinator" id="ordinator" value="Ординатор" onClick="$('.newform_upload').fadeIn(300); $('#uploaded').fadeIn(300); setdiscount(40, 'ordinator', this.checked);" /> <label for="ordinator">Я ординатор</label></div>
						</div>
						<div class="allow50 newform_field group">
							<div class="newform_flda"><input type="checkbox" class="dscnt" name="loyalty" id="loyalty" value="Скидка 50% по программе Ormco Stars" onClick="$('.newform_upload').fadeOut(300); $('#uploaded').fadeOut(300); setdiscount(50, 'loyalty', this.checked);" disabled /> <label for="loyalty">Скидка 50% по программе <span>Ormco Stars</span></label></div>
						</div>
						<div class="newform_field newform_upload hidden group">
							<div class="newform_fldu">Загрузить удостоверение <button type="button" class="upload" name="upload" id="upload">Выберите файл</button></div>
						</div>
						<div class="uploaded hidden" id="uploaded">(форматы jpg, gif, png или pdf)</div>
						<div class="discount hidden">
							Ваша скидка <span id="discount">0</span> процентов
						</div>
						<div class="finalprice">
							Стоимость участия <span id="finalprice"><?if (date("Y-m-d") < "2019-12-01") {?>11 000<?}else{?>14 000<?}?> р.</span>
						</div>
						<div class="newform_field">
							<h4>За участие в этом мероприятии Вы получите <span id="bonuses">0</span> звезд по программе <span>Ormco Stars</span></h4>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Оплата:</div>
							<div class="newform_fld"><div class="tabbut active tabbut1" onClick="showtab(1);">Как физ. лицо</div><div class="tabbut tabbut2" onClick="showtab(2);">Как юр. лицо</div></div>
						</div>
						<div class="tab1">
							<div class="newform_field group">
								<div class="newform_ttl">Способ оплаты:</div>
								<div class="newform_fld"><select name="payment_id" id="payment_id" class="ness" onChange="if (this.value == 4666) {showtab(2);}">
										<option value="">выберите способ оплаты</option>
										<option value="4663">Квитанция в банк</option>
										<option value="4664" comment="Не позднее, чем за 10 дней до мероприятия!">Наличными в офисе Ormco</option>
										<option value="17688" comment="Приготовьтесь сразу произвести оплату">Онлайн оплата</option>
										<option value="4666">Счет для юр. лиц</option>
									</select>
								</div>
							</div>
						</div>
						<div class="tab2 hidden">
							<div class="newform_field group yurs">
								<div class="newform_ttl">Юр. лицо:</div>
								<div class="newform_fld"><select name="yur" id="yur" class="ness" disabled>
									</select>
								</div>
							</div>
							<div class="addyur" onClick="addnewyur();">
								<span>+</span> Добавить новое юр. лицо
								<small>Приготовьтесь ввести реквизиты юр. лица</small>
							</div>
							<div class="addyurform addyurform1 hidden">
								<div class="addyur_close" onClick="$('.addyurform').fadeOut(300, function(){$('.newform_reg').fadeIn(300); $('.addyur').fadeIn(300); $('.yurs').fadeIn(300);})"><span class="shape1"></span><span class="shape2"></span></div>
								<div class="newform_field group">
									<div class="newform_ttl">Новое юр. лицо:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="yurname" id="yurname" /><small>Введите юридическое название организации</small></div>
								</div>
								<h4>Контактная информация</h4>
								<div class="newform_field group">
									<div class="newform_ttl">ФИО:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="fio_rukovoditelya" id="fio_rukovoditelya" /><small>ФИО руководителя организации</small></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Должность:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="dolzhnost" id="dolzhnost" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Контактное лицо:</div>
									<div class="newform_fld"><input type="text" size="30" name="contact_person" id="contact_person" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Email<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="yuremail" id="yuremail" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Телефон</div>
									<div class="newform_fld"><input class="short" type="text" size="30" name="phone_number" id="phone_number" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Факс</div>
									<div class="newform_fld"><input class="short" type="text" size="30" name="fax" id="fax" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl"></div>
									<div class="newform_fld"><button type="button" onClick="showyur(2);">Далее</button></div>
								</div>
							</div>
							<div class="addyurform addyurform2 hidden">
								<div class="addyur_back" onClick="showyur(1);"></div>
								<div class="addyur_close" onClick="$('.addyurform').fadeOut(300, function(){$('.newform_reg').fadeIn(300); $('.addyur').fadeIn(300); $('.yurs').fadeIn(300);})"><span class="shape1"></span><span class="shape2"></span></div>
								<div class="newform_field group">
									<div class="newform_ttl">ИНН:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="inn" id="inn" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">КПП:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="kpp" id="kpp" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Рассчетный счет:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="account" id="account" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">БИК:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness short" type="text" size="30" name="bik" id="bik" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Корр. счет:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="bank_account" id="bank_account" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">ОГРН:</div>
									<div class="newform_fld"><input type="text" size="30" name="ogrn" id="ogrn" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Юридический адрес:<sup>*</sup></div>
									<div class="newform_fld"><textarea class="ness" name="legal_address" id="legal_address"></textarea></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Фактический адрес:<sup>*</sup></div>
									<div class="newform_fld"><textarea class="ness" name="defacto_address" id="defacto_address"></textarea></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Почтовый индекс:</div>
									<div class="newform_fld"><input type="text" size="30" name="postal_address" id="postal_address" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl"></div>
									<div class="newform_fld"><button type="button" onClick="addyurajax();">Добавить юр. лицо</button></div>
								</div>
							</div>
						</div>
						<div id="igosel">
							<h3>Выбранные мастер-классы</h3>
							<div class="chosen"></div>
						</div>
						<div class="newform_field newform_reg group">
							<div class="newform_ttl"></div>
							<div class="newform_fld"><button id="register">Зарегистрироваться</button></div>
						</div>
						<!--<div class="newform_field group">
							<div class="newform_ttla double">Планирую покупку продукции Ormco в ближайшее время (1-2 месяца)</div>
							<div class="newform_flda"><input type="checkbox" size="30" name="want_to_buy" id="want_to_buy" value="1" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttla double">Принимал(а) участие в образовательных мероприятиях Ormco в течение последнего года</div>
							<div class="flda"><input type="checkbox" size="30" name="how_you_been_edu" id="how_you_been_edu" value="1" /></div>
						</div>
						<div id="igosel">
							<h3>Выбранные мастер-классы</h3>
							<div class="igosel" onClick="$('.igodd').fadeToggle(200); $(this).toggleClass('sel');"><span>Выберите три мастер-класса</span></div>
							<div class="igodd">
								<?
								foreach ($mks as $n => $nks)
									{
									?>
									<div class="igotime igo<?=$n;?>"><?=$mktimes[$n];?></div>
									<?
									foreach ($nks as $m => $mk)
										{
										?>
										<div class="igoset igo<?=$n;?><?if ($mkcounts[$m - 1] <= 0) {?> faded cancelled<?}?>" data-group="<?=$n;?>" data-mk="<?=$m;?>" id="igo_<?=$m;?>"><input type="checkbox" <?if ($mkcounts[$m - 1] <= 0) {?>disabled <?}?>name="iset<?=$m;?>" id="iset<?=$m;?>" onClick="igo(<?=$n;?>, <?=$m;?>, '<strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?>', this.checked);" /><label for="iset<?=$m;?>"><strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?></label></div>
										<?
									}
								}
								?>
							</div>
							<div class="chosen"></div>
						</div>-->
						<div class="formagree">Нажимая «Отправить», вы соглашаетесь с <span class="lnk" onClick="$('#pol').fadeIn(300);">политикой конфиденциальности</span> и <a target="_blank" href="http://ormco.ru/files/sys/oferta_Ormco_Seminary.docx">условиями договора-оферты</a></div>
					</div>
					</form>
				</div>
			</div>
		</div>
		<div class="sugrob" style="background-image: url('images/sugrob.png'); background-size: cover;"></div>
		<div class="footer">
			Ormco Corporation &copy; 2019<br />
			<a href="http://ormco.ru">ormco.ru</a>
		</div>
		<div class="mask"></div>
		<div class="mask1"></div>
		<div class="burger" onClick="$('#mobmenu').slideDown(300);"><img src="images/burger.png" /></div>
		<div id="mobmenu" class="mobmenu">
			<div class="close2" onClick="$('#mobmenu').slideUp(300);"><img src="images/close2.png" /></div>
			<ul>
				<li id="miprogramm1"><a href="#programm">Программа</a></li>
				<li id="mispeakers1"><a href="#speakers">Спикеры</a></li>
				<li id="miprice1"><a href="#price">Стоимость</a></li>
				<li id="miplace1"><a href="#place">Место проведения</a></li>
			</ul>
			<button type="button" onClick="<?if ($mkcounts[0] > 0 || $mkcounts[1] > 0 || $mkcounts[2] > 0) {?>$('#mobmenu').slideUp(300); if (chkigos()) {fixbody(); $('.mask').fadeIn(300); $('.openform').fadeIn(300);}<?}else{?>noplaces();<?}?>">РЕГИСТРАЦИЯ</button>
		</div>
		<div class="form_error">
			<div class="close" onClick="$('.form_error').fadeOut(300); $('.mask1').fadeOut(300);"><img src="images/close.png" /></div>
			<h3 id="form_error"></h3>
		</div>
		<div class="popup" id="noplaces">
			<div class="close" onClick="$('.popup').fadeOut(300); $('.mask').fadeOut(300);"><img src="images/close.png" /></div>
			<h3>Мест больше нет, регистрация окончена!</h3>
			<br />
			Вы можете записаться в лист ожидания. Для этого свяжитесь с нашим менеджером Григорием<br />по телефону <a href="tel:+79266108002">+7&nbsp;(926)&nbsp;610-80-02</a>
			<br />
			<br />
			<h4>Обратите внимание!</h4>
			Участники из листа ожидания смогут принять участие в тех мастер-классах, на которых появятся свободные места. Мы не можем гарантировать наличие мест именно на тех мастер-классах, на которые вы больше всего хотите попасть.
		</div>
		<div class="popup" id="popup">
			<div class="close" onClick="$('.popup').fadeOut(300); $('.mask').fadeOut(300);"><img src="images/close.png" /></div>
			<h3>Обратите внимание!</h3>
<br />
- 3 мастер-класса проходят одновременно в разных аудиториях
<br />
- каждый мастер-класс проходит с 10 до 16 часов;
<br />
- мастер-класс прерывается на кофе-брейки и обед;
<br />
- можно выбрать только 1 мастер-класс;
<br />
- количество мест ограничено.
<br /><br /><h3>Пожалуйста, выберите 1 мастер-класс</h3>
			<button type="button" onClick="/* pop1 = 1; */ $('.popup').fadeOut(300); $('.mask').fadeOut(300);">Понятно!</button>
		</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter23063434 = new Ya.Metrika({
                    id:23063434,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    ecommerce:"dataLayer"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/23063434" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67881160-2', 'auto');
  ga('require', 'displayfeatures');
  ga('require', 'linkid');
  ga('require', 'ec');

  ga('ec:addProduct', {'name': '<?=$event_name;?>', 'category': 'Платное', 'price': '<?if (date("Y-m-d") < "2019-12-01") {?>11 000<?}else{?>14 000<?}?>', 'quantity': 1});
  ga('ec:setAction','checkout');

  ga('send', 'pageview');

</script>
<div id="pol">
<div class="close" onClick="$('#pol').fadeOut(300);"><img src="images/close.png" /></div>
<div id="inpol">
<h2>Политика конфиденциальности</h2>
<h4>1. Общие положения</h4>
Корпорация Ormco в лице ООО «Ормко» (далее «Ормко») и её аффилированные лица с уважением относятся к конфиденциальной информации любого лица, ставшего посетителем данного сайта. Мы хотели бы проинформировать Вас о том, какие именно данные мы собираем и каким образом используем собранные данные. Вы также узнаете о том, как Вы можете проверить точность собранной информации и дать нам указание об удалении подобной информации. Данные собираются, обрабатываются и используются строго в соответствии с требованиями действующего законодательства того государства, в котором расположено соответствующее аффилированное лицо компании "Ормко", отвечающее за защиту персональных данных. Мы делаем все возможное для обеспечения соответствия требованиям действующего законодательства.
Данное заявление не распространяется на сайты, на которые сайт компании "Ормко" содержит гиперссылки.
<h4>2. Сбор, использование и переработка персональных данных</h4>
Мы осуществляем сбор информации, относящейся к определенным лицам, лишь в целях обработки и использования информации и только в том случае, если Вы добровольно предоставили информацию или явно выразили свое согласие на ее использование.
Когда Вы посещаете наш сайт, определенные данные автоматически записываются на серверы компании "Ормко" и/или её аффилированных лиц для целей системного администрирования или для статистических или резервных целей. Записываемая информация содержит название Вашего интернет-провайдера, в некоторых случаях Ваш IP-адрес, данные о версии программного обеспечения Вашего браузера, об операционной системе компьютера, с которого Вы посетили наш сайт, адреса сайтов, после посещения которых Вы по ссылке зашли на наш сайт, заданные Вами параметры поиска, приведшие Вас на наш сайт.
В зависимости от обстоятельств, подобная информация позволяет сделать выводы о том, какая аудитория посещает наш сайт. В данном контексте, однако, не используются никакие персональные данные. Использоваться может лишь анонимная информация. Если информация передается компанией "Ормко" и/или её аффилированными лицами внешнему провайдеру, принимаются все возможные технические и организационные меры, гарантирующие передачу данных в соответствии с требованиями действующего законодательства.
Если Вы добровольно предоставляете нам персональную информацию, мы обязуемся не использовать, не обрабатывать и не передавать такую информацию способом, выходящим за рамки требований действующего законодательства. Использование и распространение Ваших персональных данных без Вашего согласия возможно только на основании судебного решения или в ином порядке, предусмотренном законодательством РФ.
Любые изменения, которые будут внесены в правила по соблюдению конфиденциальности, будут размещены на данной странице. Это позволяет Вам в любое время получить информацию о том, какие данные у нас хранятся и о том, каким образом мы собираем и храним такие данные.
<h4>3. Безопасность данных</h4>
Компания "Ормко" и её аффилированные лица обязуется бережно хранить Ваши персональные данные и принимать все меры предосторожности для защиты Ваших персональных данных от утраты, неправильного использования или внесения в персональные данные изменений. Партнеры компании "Ормко" и её аффилированных лиц, которые имеют доступ к Вашим данным, необходимым им для предоставления Вам услуг от имени компании "Ормко" и её аффилированных лиц, несут перед компанией "Ормко" и её аффилированными лицами закрепленные в контрактах обязательства соблюдать конфиденциальность данной информации и не имеют права использовать предоставляемые данные для каких-либо иных целей.
<h4>4. Персональные данные несовершеннолетних потребителей</h4>
Компания "Ормко" и её аффилированные лица не ведет сбор информации в отношении потребителей, не достигших 14 лет. При необходимости, мы можем специально попросить ребенка не присылать в наш адрес никакой личной информации. Если родители или иные законные представители ребенка обнаружат, что дети сделали какую-либо информацию доступной для компании "Ормко" и её аффилированных лиц, и сочтут, что предоставленные ребенком данные должны быть уничтожены, таким родителям или иным законным представителям необходимо связаться с нами по нижеуказанному (см. п. 6) адресу. В этом случае мы немедленно удалим личную информацию о ребенке.
<h4>5. Файлы Cookie</h4>
Мы используем файлы cookie для того, чтобы предоставить Вам больше возможностей при использовании сайта. Файлы cookie представляют собой небольшие блоки данных, помещаемые Вашим браузером на временное хранение на жестком диске Вашего компьютера, необходимые для навигации по нашему сайту. Файлы cookie не содержат никакой личной информации о Вас и не могут использоваться для идентификации отдельного пользователя. Файл cookie часто содержит уникальный идентификатор, представляющий собой анонимный номер (генерируемый случайным образом), сохраняемый на Вашем устройстве. Некоторые файлы удаляются по окончании Вашего сеанса работы на сайте; другие остаются на Вашем компьютере дольше.
Приступая к использованию данного веб-сайта, вы соглашаетесь на использование файлов cookie, Вы также признаете, что в подобном контенте могут использоваться свои файлы cookie.
Компания Ормко не контролирует и не несет ответственность за файлы cookie сторонних разработчиков. Дополнительную информацию Вы можете найти на сайте разработчика.
<h4>6. Отслеживание через интернет</h4>
На данном сайте осуществляется сбор и хранение данных для маркетинга и оптимизации с использованием технологии Яндекс.Метрика и Google Analitycs. Эти данные могут использоваться для создания профилей пользователей под псевдонимами. Сайт может устанавливать файлы cookie.
Без ясно выраженного согласия наших пользователей данные, собираемые с помощью технологий Яндекс.Метрика и Google Analitycs, не используются для идентификации личности посетителя и не связываются с какими-либо другими личными данными носителя псевдонима.
Дополнительную информацию об отслеживании через интернет Вы можете найти на сайтах провайдеров сервисов Яндекс.Метрика и Google Analitycs.
<h4>7. Ваши пожелания и вопросы</h4>
Хранящиеся данные будут стерты компанией "Ормко" и/или её аффилированными лицами по истечении периода хранения, установленного законодательством или договором либо в случае если сама компания "Ормко" и/или её аффилированные лица отменит хранение тех или иных данных. Вы вправе в любое время потребовать удаления из базы данных компании "Ормко" и/или её аффилированных лиц информации о Вас. Вы также вправе в любое время отозвать Ваше согласие на использование или переработку Ваших персональных данных. В таких случаях, а также, если у Вас есть какие-либо иные пожелания, связанные с Вашими персональными данными, просим Вас выслать письмо по почте в адрес «Ормко» в России по адресу: 195112, Санкт-Петербург, Малоохтинский пр-т, д. 64, корп. 3. или по электронной почте dc-sales@ormco.com. Просим Вас также связаться с нами в случае, если Вам хотелось бы узнать, собираем ли мы данные о Вас и если да, то какие именно данные. Мы постараемся выполнить Ваши пожелания в возможно короткие сроки.
<h4>8. Законодательство по обработке персональных данных</h4>
Все действия с персональными данными, собираемыми на данном сайте, производятся в соответствии с Федеральным законом Российской Федерации №152-ФЗ от 27 июля 2006 года «О персональных данных».
<h4>(1) Заявленная цель сбора, обработки или использования данных:</h4>
•	Предметом деятельности «Ормко» и её аффилированных лиц является производство и распространение стоматологических продуктов всех типов, главным образом брекет-систем, ортодонтических инструментов, микроимплантов, адгезивов;
<h4>(2) Описание групп вовлеченных лиц и соответствующих данных или категорий данных:</h4>
Данные, касающиеся заказчиков, сотрудников, пенсионеров, сотрудников сторонних компаний (субподрядчиков), персонала, работающего по лизингу, претендентов на рабочие места, авторов изобретений, не являющихся сотрудниками компаний, или наследников, соответственно, поставщиков товаров и услуг, сторонних заказчиков, потребителей, добровольцев, участвующих в потребительских испытаниях, посетителей производственных объектов корпорации, инвесторов – насколько это необходимо для выполнения целей, определенных в пункте 4.
<h4>(3) Получатели или категории получателей, которым могут быть разглашены данные:</h4>
Органы власти, фонды страхования здоровья, ассоциация по страхованию ответственности работодателей при наличии соответствующего правового регулирования, сторонние подрядчики в соответствии сторонние поставщики услуг, ассоциация пенсионеров «Ормко», аффилированные лица и внутренние подразделения для выполнения целей, указанных в пункте 4.
<h4>(4) Периодичность регулярного удаления данных:</h4>
Юристами подготовлено множество инструкций, касающихся обязанностей по хранению данных и периодов хранения. Данные удаляются в установленном порядке по истечении указанных периодов. Данные, не подпадающие под действие данных условий, удаляются, если цели, указанные в пункте 4, перестают существовать.
<h4>(5) Запланированная передача данных другим странам:</h4>
В рамках всемирной системы информации о трудовых ресурсах, данные по персоналу должны быть доступны определенным руководящим работникам в других странах. Соответствующие соглашения о защите данных должны быть заключены с соответствующими компаниями в соответствии со стандартами ЕС.
<h4>9. Использование встраиваемых модулей для социальных сетей</h4>
На наших интернет-страницах предусмотрена возможность встраивания модулей для социальных сетей ВКонтакте, Twitter, Instagram, Youtube (далее – «провайдеры»). 
Только если Вы активируете модуль, тем самым разрешая передачу данных, браузер создаст прямое соединение с сервером провайдера. Содержимое различных модулей впоследствии передается соответствующим провайдером непосредственно в Ваш браузер и выводится на экран Вашего компьютера.
Модуль сообщает провайдеру, на какую из страниц нашего сайта Вы вошли. Если во время просмотра нашего сайта Вы вошли на ВКонтакте, Instagram, Youtube или Twitter под своей учетной записью, провайдер может подобрать информацию, в соответствии с Вашими интересами, т.е. информацию, которую Вы просматриваете с помощью Вашей учетной записи. При использовании какой-либо функции встроенного модуля (например, кнопки “Мне нравится”, размещения комментария), эта информация также будет передана браузером непосредственно провайдеру для сохранения.
Дополнительную информацию по сбору и использованию данных социальными сетями, а также по правам и возможностям защиты Вашей конфиденциальности в данных обстоятельствах, можно найти в рекомендациях провайдеров по защите данных /конфиденциальности:
Для того, чтобы не подключаться к учетным записям провайдеров при посещении нашего сайта, Вам необходимо отключиться от соответствующей учетной записи перед посещением наших интернет-страниц.
</div>
</div>
	</body>
</html>
