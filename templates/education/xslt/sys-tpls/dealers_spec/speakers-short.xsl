<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">


<xsl:stylesheet	version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:php="http://php.net/xsl"
		xsl:extension-element-prefixes="php"
		exclude-result-prefixes="php">

	<xsl:output encoding="utf-8" method="html" indent="yes"/>

	<xsl:variable name="domain" select="'https://orthodontia.ru'" />
	<xsl:variable name="body_left_margin" select="'55px'" />
	<xsl:variable name="body_right_margin" select="'55px'" />
	<xsl:variable name="def_letter_spacing" select="'letter-spacing:0.2px;'" />

	<xsl:template match="/">

		<html>
			<head>
			<!-- <style type="text/css">
				table td td{
					border: 1px solid black;
				}
				.table_caption {
					text-align: center;
					margin: 0;
					font-size: 13px;
				}
			</style> -->
			<style type="text/css">
				body {
	        font-family: 'Open Sans';
	        font-size: 10pt;
	    }


				.header{
					background:#07477D;
					margin-bottom:0px;
					padding:16px 55px 21px;

				}
				.footer{
					background:#387BB5;
					text-align:center;
					color:#ffffff;
					font-size:10px;
					padding:6px 55px;
				}

				.spec_dealers_speakers{
					padding:0 55px 20px;
				}

				.spec_dealers_speakers_item{
					margin: 0px 0 30px
				}
				.spec_dealers_speakers_item h2{
					font-size:23px;
					font-weight:normal;
					line-height:23px;
					color: #003D66;
					padding:0;
				}
				.spec_dealers_speakers_img_wrap{
					float:left;
					width:139px;
					height:139px;
					margin:0 20px 0 0;
					background-size0:cover;
					position:relative;
					border:none;
				}
				.spec_dealers_speakers_img{
					width:135px;
					height:135px;
					margin-top:1px;
					margin-left:1px;
				}
				.spec_dealers_speakers_img_ramka{
				width:139px;
				height:139px;
					position:relative;
					margin-top:-136px;
					margin-left:-1px;
					border:none;
				}

				.spec_dealers_speakers_item_short_info{
					width:100%;
				}
				.spec_dealers_speakers_params{
					padding:0 0 0 155px;
				}
				.spec_dealers_speakers_params_regalii{
					color:#787878;
					font-size: 11.5;
					line-height: 18px;
				}
				.spec_dealers_speakers_params_city{
					color:#3A7DB7;
					vertical-align:baseline;
					line-height:15px;
					margin:10px 0 8px;
					font-size:10.5px;
				}
				.spec_dealers_speakers_params_city_icon{
					vertical-align:middle;
					margin-right:6px;
					width:15px;
				}

				.program_title{
					font-size: 15px;
					line-height: 21px;
					margin-bottom:13px;
					color:#1D6CA7
				}

				.program_params{
					color:#3A7DB7;
					vertical-align:baseline;
					line-height:15px;
					margin-top: 7px;
					font-size:10.5px;
				}
				.program_params_img{
					vertical-align:middle;
					margin-right:7px;
					width:15px;
				}
				.program_tematika_provodimyh_meropriyatij{
					margin:11px 0 0px;
				}

				.hr_line{
					color:#EBEBEB;
					height:1px;
				}
				.program_hr{
					color:#EBEBEB;
					height:1px;
					margin:2px 0 0;
				}

				.tematika_provodimyh_meropriyatij{
					margin:5px 0 0;
					vertical-align:middle;
					line-height:20px;
				}
				.tematika_provodimyh_meropriyatij_img{
					max-height0:20px;
					margin:0 10px 10px 0;
					vertical-align:middle;
					line-height:20px;
				}
				.program_tematika_provodimyh_meropriyatij .tematika_provodimyh_meropriyatij_img{
					margin:0 10px 0 0;
				}

				.opisanie_spikera{
					width:100%;

					font-size: 11.5;
					line-height: 18px;
					color:#505050;
					margin: 10px 0 0;
				}
				.opisanie_spikera p{
					margin: 0 0 10px;
				}




				.spec_dealers_speakers_item_program{
					margin: 18px 0 15px;
				}




				.header_td{
					color:#fff;
					font-size:11.5px;
					line-height:16px;
				}
				.first_site_link{
					margin:0 0 0 20px;
				}
				.site_link{
					color:#ffffff;
					text-decoration:none;
					font-size: 11.5px;
				}

				.opisanie_spikera h2{
					font-size: 13px;
					line-height: 21px;
					margin-bottom:7px;
					padding-bottom:0;
					color:#1D6CA7;
					font-weight:normal;
				}

				.opisanie_spikera h3{
					font-size: 11px;
					line-height: 21px;
					margin-bottom:3px;

					padding-bottom:0;
					color:#1D6CA7;
					font-weight:normal;
				}

				.ib{
				 page-break-inside:avoid !important;
				}

			</style>

			</head>
			<body>


				<div class="spec_dealers_speakers">
					<xsl:apply-templates select="udata/items/item" mode="spec_dealers_speakers"/>
				</div>

			</body>
		</html>
	</xsl:template>

	<xsl:template match="item" mode="spec_dealers_speakers">
		<xsl:if test="not(position() = 1)">
			<pagebreak />
		</xsl:if>
		<div class="spec_dealers_speakers_item">
			<xsl:if test="position() = 1">
				<xsl:attribute name="class">spec_dealers_speakers_item first</xsl:attribute>
			</xsl:if>
            <div class="ib2">
    			<h2>
    				<xsl:value-of select="text()" />
    			</h2>
    			<div class="spec_dealers_speakers_item_short_info">
    				<!-- width="135px"  -->
    				<div class="spec_dealers_speakers_img_wrap" >
    					<img class="spec_dealers_speakers_img" src="{$domain}{extended/properties/property[@name='pdf_photo']/value}" >
                		<xsl:if test="not(extended/properties/property[@name='pdf_photo']/value) or extended/properties/property[@name='pdf_photo']/value=''"><xsl:attribute name="src"><xsl:value-of select="extended/properties/property[@name='foto_spikera']/value" /></xsl:attribute></xsl:if>
                		<xsl:if test="(not(extended/properties/property[@name='foto_spikera']/value) or extended/properties/property[@name='foto_spikera']/value='') and not(extended/properties/property[@name='pdf_photo']/value) or extended/properties/property[@name='pdf_photo']/value=''"><xsl:attribute name="src"><xsl:value-of select="concat($domain,'/tcpdf/docs/dealers/pdf_def_img.png')" /></xsl:attribute></xsl:if>
                	</img>
    					<!-- <img class="spec_dealers_speakers_img" src="{$domain}{.//property[@name='foto_spikera']/value}" >
                    		<xsl:if test="not(.//property[@name='foto_spikera']/value) or .//property[@name='foto_spikera']/value=''"><xsl:attribute name="src"><xsl:value-of select="concat($domain,'/tcpdf/docs/dealers/pdf_def_img.png')" /></xsl:attribute></xsl:if>
                    	</img>
    					<img class="spec_dealers_speakers_img_ramka" src="{$domain}/tcpdf/docs/dealers/speaker_r.png" /> -->

    				</div>
    				<div class="spec_dealers_speakers_params">
    					<div class="spec_dealers_speakers_params_regalii">
    						<xsl:value-of select="extended/properties/property[@name='regalii']/value" disable-output-escaping="yes" />
    					</div>
    					<div class="spec_dealers_speakers_params_city">
    						<img class="spec_dealers_speakers_params_city_icon" src="{$domain}/tcpdf/docs/dealers/city.png"/>
    						<xsl:apply-templates select="extended/properties/property[@name='gorod_prozhivaniya']" mode="gorod_prozhivaniya" />
    					</div>

    					<hr class="hr_line" />
    					<xsl:apply-templates select="extended/properties/property[@name='tematika_provodimyh_meropriyatij']" mode="tematika_provodimyh_meropriyatij" />
    				</div>
    			</div>
			</div>
			<div class="opisanie_spikera">
				<xsl:value-of select=".//property[@name='opisanie_spikera']/value" disable-output-escaping="yes" />
			</div>

				<xsl:apply-templates select="programs/program" mode="program" />

		</div>


	</xsl:template>

	<!-- gorod_prozhivaniya -->
	<xsl:template match="property" mode="gorod_prozhivaniya">Город проживания: <xsl:value-of select="value/item/@name" /></xsl:template>



	<!-- tematika_provodimyh_meropriyatij -->
	<xsl:template match="property" mode="tematika_provodimyh_meropriyatij">
		<div class="tematika_provodimyh_meropriyatij">
			<xsl:apply-templates select="value/item" mode="tematika_provodimyh_meropriyatij" />
		</div>
	</xsl:template>
	<xsl:template match="item" mode="tematika_provodimyh_meropriyatij">
        <xsl:variable name="logo" select="document(concat('uobject://',@id,'.logo'))//value" />

				<img src="{$domain}{$logo}"  class="tematika_provodimyh_meropriyatij_img" >
					<xsl:if test="$logo/@height &gt; 26">
						<xsl:attribute name="height">25</xsl:attribute>
					</xsl:if>

					<!-- Insignia -->
					<xsl:if test="@id = 1315">
						<xsl:attribute name="height">10</xsl:attribute>
					</xsl:if>
					<!-- Damon system -->
					<xsl:if test="@id = 1284">
						<xsl:attribute name="height">16</xsl:attribute>
					</xsl:if>
					<!-- Абсолютный анкораж (Vector TAS) -->
					<xsl:if test="@id = 1321">
						<xsl:attribute name="height">11</xsl:attribute>
					</xsl:if>
				</img>
	</xsl:template>

	<!-- program -->
	<xsl:template match="program" mode="program">
		<div class="spec_dealers_speakers_item_program ib">

			<div class="program_title">
				 <xsl:value-of select="text()" disable-output-escaping="yes" />
			</div>

			<xsl:apply-templates select="extended/properties/property[@name='uroven_meropriyatiya']" mode="program_params" />
			<xsl:apply-templates select="extended/properties/property[@name='tip_meropriyatiya']" mode="program_params" />
			<xsl:apply-templates select="extended/properties/property[@name='kolvo_dnej']" mode="program_params" />
			<div class="program_tematika_provodimyh_meropriyatij">
				<xsl:apply-templates select="extended/properties/property[@name='tematika_provodimyh_meropriyatij']" mode="tematika_provodimyh_meropriyatij" />
			</div>

			<xsl:if test="not(position() = last())">
				<hr class="hr_line program_hr" />
			</xsl:if>
		</div>
	</xsl:template>

	<!-- program property-->
	<xsl:template match="property" mode="program_params">
		<div class="program_params">
			<xsl:choose>
			  <xsl:when test="@name='uroven_meropriyatiya'" >
			    <img class="program_params_img" src="{$domain}/tcpdf/docs/dealers/level.png" />
			  </xsl:when>
				<xsl:when test="@name='tip_meropriyatiya'">
			    <img class="program_params_img" src="{$domain}/tcpdf/docs/dealers/type.png" />
			  </xsl:when>
				<xsl:when test="@name='kolvo_dnej'">
			    <img class="program_params_img" src="{$domain}/tcpdf/docs/dealers/day.png"/>
			  </xsl:when>
			</xsl:choose>

			<xsl:if test="@name = 'kolvo_dnej'">Кол-во дней: </xsl:if>
			<xsl:apply-templates select="value/item" mode="program_uroven_meropriyatiya_values"/>
		</div>
	</xsl:template>

	<!-- program property values-->
	<xsl:template match="item" mode="program_uroven_meropriyatiya_values">
		<xsl:if test="not(position() = 1)">, </xsl:if><xsl:value-of select="@name" />
	</xsl:template>



</xsl:stylesheet>
