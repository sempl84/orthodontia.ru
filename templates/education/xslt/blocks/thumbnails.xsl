<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi"
	exclude-result-prefixes="umi">

	<xsl:template name="makeThumbnailFull_ByPath">
		<xsl:param name="element-id" />
		<xsl:param name="field-name" />
		<xsl:param name="source" />
		<xsl:param name="empty" select="'&empty-photo;'" />
		<xsl:param name="width">auto</xsl:param>
		<xsl:param name="height">auto</xsl:param>
		<xsl:param name="item">1</xsl:param>
		<xsl:param name="alt" />
		<xsl:param name="class" />

		<xsl:variable name="src">
			<xsl:choose>
				<xsl:when test="$source">
					<xsl:value-of select="$source" />
				</xsl:when>
				<xsl:otherwise><xsl:value-of select="$empty" /></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="thumb" select="document(concat('udata://system/makeThumbnailFull/(.', $src, ')/', $width, '/', $height, '/void/0/1///100'))/udata" />

		<img class="{$class}" src="{$thumb/src}">
			<xsl:if test="$element-id and $field-name">
				<xsl:attribute name="umi:element-id">
					<xsl:value-of select="$element-id" />
				</xsl:attribute>

				<xsl:attribute name="umi:field-name">
					<xsl:value-of select="$field-name" />
				</xsl:attribute>
			</xsl:if>

			<xsl:if test="$alt">
				<xsl:attribute name="alt">
					<xsl:value-of select="$alt" />
				</xsl:attribute>
			</xsl:if>

			<xsl:if test="$empty">
				<xsl:attribute name="umi:empty">
					<xsl:value-of select="$empty" />
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="$item = 1">
				<xsl:attribute name="itemprop">image</xsl:attribute>
			</xsl:if>
		</img>
	</xsl:template>

	<xsl:template name="makeThumbnail_ByPath">
		<xsl:param name="element-id" />
		<xsl:param name="field-name" />
		<xsl:param name="source" />
		<xsl:param name="empty" select="'&empty-photo;'" />
		<xsl:param name="width">auto</xsl:param>
		<xsl:param name="height">auto</xsl:param>
		<xsl:param name="item">1</xsl:param>
		<xsl:param name="alt" />
		<xsl:param name="class" />

		<xsl:variable name="src">
			<xsl:choose>
				<xsl:when test="$source">
					<xsl:value-of select="$source" />
				</xsl:when>
				<xsl:otherwise><xsl:value-of select="$empty" /></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="thumb" select="document(concat('udata://system/makeThumbnail/(.', $src, ')/', $width, '/', $height, '/void/0/1/100'))/udata" />

		<img class="{$class}" src="{$thumb/src}">
			<xsl:if test="$element-id and $field-name">
				<xsl:attribute name="umi:element-id">
					<xsl:value-of select="$element-id" />
				</xsl:attribute>

				<xsl:attribute name="umi:field-name">
					<xsl:value-of select="$field-name" />
				</xsl:attribute>
			</xsl:if>

			<xsl:if test="$alt">
				<xsl:attribute name="alt">
					<xsl:value-of select="$alt" />
				</xsl:attribute>
			</xsl:if>

			<xsl:if test="$empty">
				<xsl:attribute name="umi:empty">
					<xsl:value-of select="$empty" />
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="$item = 1">
				<xsl:attribute name="itemprop">image</xsl:attribute>
			</xsl:if>
		</img>
	</xsl:template>
</xsl:stylesheet>