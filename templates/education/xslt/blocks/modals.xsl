<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:xlink="http://www.w3.org/TR/xlink">

    <xsl:template match="result" mode="modals">
        <!-- форма обратной связи -->
        <div class="modal fade register-modal wide-modal" id="feedbackModal" tabindex="-1" role="dialog" aria-labelledby="gridRegister">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <xsl:apply-templates select="document(concat('udata://webforms/add/', &feedback_form_oid;))/udata" mode="feedback_form">
                        <xsl:with-param name="submit_text" select="'Отправить'"/>
                    </xsl:apply-templates>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- форма записи на курсы -->
        <div class="modal fade register-modal wide-modal" id="courseFormModal" tabindex="-1" role="dialog" aria-labelledby="gridRegister">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <xsl:apply-templates select="document(concat('udata://webforms/add/', &course_form_oid;))/udata" mode="feedback_form">
                        <xsl:with-param name="submit_text" select="'Отправить'"/>
                    </xsl:apply-templates>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- Результат отправки формы -->
        <div class="modal fade register-modal" id="formResultModal" tabindex="-1" role="dialog" aria-labelledby="gridRegister">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <h4 class="modal-title" id="gridRegister"> </h4>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer"></div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- подгрузка блоков для гостей: авторизация, регистрация
        и блоков для зарегистрированных: форма регистрации на мероприятие -->
        <xsl:apply-templates select="user" mode="header_auth" />

        <!-- блок с описание помощи -->
        <div class="modal fade register-modal wide-modal" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="gridRegister">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <xsl:value-of select="$settings//property[@name='filter_help_text']/value" disable-output-escaping="yes" />
                    </div>
                    <div class="modal-footer"></div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- модальные окна для валидации телефона -->
        <div class="modal fade phone-validator-modal" id="phoneValidateModal" tabindex="-1" role="dialog" aria-labelledby="phoneValidateModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <div class="header4 modal-title">Подтверждение телефона</div>
                    </div>
                    <div class="modal-body">
                        <p>Ваш номер мобильного телефона <b><xsl:value-of select="$user-info//property[@name='phone']/value" /></b> не подтвержден</p>
                        <div class="form-group js-phone--wrapper ">
                            <input type="hidden" class="form-control js-phone js-phone--input" value="{$user-info//property[@name='phone']/value}" name="phone" />
                            <a class="btn btn-primary js-phone--send-code" href="#">Отправить код</a>
                            <a class="btn btn-primary js-phone--change-phone" href="#">Изменить номер</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade phone-validator-modal" id="phoneChangeModal" tabindex="-1" role="dialog" aria-labelledby="phoneChangeModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <div class="header4 modal-title">Изменение телефона</div>
                    </div>
                    <div class="modal-body">
                        <p class="phoneChangeText">Укажите актуальный номер мобильного телефона:</p>
                        <div class="form-group js-phone--wrapper">
                            <input type="text" class="form-control js-phone js-phone--input" value="{$user-info//property[@name='phone']/value}" name="phone" />
                            <br/>
                            <a class="btn btn-primary js-phone--change-phone-apply" href="#">Применить</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END. модальные окна для валидации телефона -->

        <!-- оценка удобства заказ для партнерских мероприятий -->
        <!-- <xsl:if test="$document-page-id = &partner_event_apply_successful_pid;">
            <xsl:call-template name="modal_interview" />
        </xsl:if> -->
        <div class="modal fade phone-validator-modal" id="helpModalDownloadForDealers" tabindex="-1" role="dialog" aria-labelledby="helpModalDownloadForDealers">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <div class="header4 modal-title">Скачать pdf</div>
                    </div>
                    <div class="modal-body">
                        <p>Инструкция для скачивания pdf</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade phone-validator-modal" id="helpModalDealerssFilter" tabindex="-1" role="dialog" aria-labelledby="helpModalDealerssFilter">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <div class="header4 modal-title">Фильтрация</div>
                    </div>
                    <div class="modal-body">
                        <p>Инструкция для работы с фильтром</p>
                    </div>
                </div>
            </div>
        </div>

        <xsl:if test="$module = 'catalog' and $method='object' and $parents/page/@id = &spec_dealers_speakers_pid;">
            <div class="modal fade phone-validator-modal" id="speakerBigImage" tabindex="-1" role="dialog" aria-labelledby="speakerBigImage">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <img  src="{.//property[@name='big_photo']/value}" alt="" id="speakerBigImageSrc" />
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>

        <div class="modal fade phone-validator-modal" id="stub_vds_modal" tabindex="-1" role="dialog" aria-labelledby="stub_vds_modalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <div class="header4 modal-title">Технические работы</div>
                    </div>
                    <div class="modal-body">
                        <p>Здравствуйте!</p>
                        <p>На сайте orthodontia.ru сейчас идут технические работы. Создание учетных записей и регистрация на мероприятия недоступны с 21:00 седьмого октября до 04:00 восьмого октября (МСК).<br/>Заходите к нам чуть позже и регистрируйтесь на лучшие образовательные мероприятия!</p>
                        <p>Приносим извинения за доставленные неудобства!</p>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>