<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:xlink="http://www.w3.org/TR/xlink">

    <xsl:template match="result" mode="footer">
        <!-- Footer -->
        <footer class="content-block dark-blue">
            <div class="container-fluid">
                <div class="row hidden-xs menu">
                    <div class="col-md-3 col-sm-6">
                        <span>Мероприятия</span>
                        <ul>
                            <xsl:apply-templates select="document('udata://menu/draw/right_menu_events')/udata/item" mode="right_menu" />

                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <span>Школа ортодонтии</span>
                        <ul>
                            <li>
                                <a href="https://orthodontexpert.ru/doctors">Регистрация</a>
                            </li>
                            <li>
                                <a href="https://orthodontexpert.ru/doctors/faq">Вопросы и ответы</a>
                            </li>
                            <li>
                                <a href="https://orthodontexpert.ru/doctors/seminars">Семинары</a>
                            </li>
                            <li>
                                <a href="https://orthodontexpert.ru/doctors/online">Онлайн-обучение</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <span>Записи</span>
                        <ul>
                            <li>
                                <a href="{document('upage://&webinars_pid;')/udata/page/@link}">Записи вебинаров</a>
                            </li>
                            <li>
                                <a href="{document('upage://&library_pid;')/udata/page/@link}">Библиотека</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <span>Корпорация Ormco</span>
                        <ul>
                            <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata/item" mode="right_menu" />
                        </ul>
                    </div>
                </div>
                <div class="row hidden-xs">
                    <div class="col-lg-12">
                        <hr />
                    </div>
                </div>
                <div class="row bottom-info">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 left-pnl">
                        <a href="{$settings//property[@name = 'dogovoroferta']/value}" target="_blank">Договор-оферта</a>
                        <a href="&conf_politics_url;" target="_blank">Политика конфиденциальности</a>
                        <span>© ООО «Ормко», <xsl:value-of select="document('udata://system/convertDate/now/(Y)')/udata" /></span>
                    </div>
                    <div class="col-lg-5 col-md-4 col-sm-4 col-xs-12">
                        <ul class="social-icons">
                            <!--<li><a class="circled-icon" href="https://facebook.com/ormcorussia" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>-->
                            <li><a class="circled-icon" href="https://vk.com/ormcorussia" target="_blank"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
                            <li><a class="circled-icon" href="https://www.youtube.com/channel/UCXlGHVez9C5F9vOneqHwmAg" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                            <li><a class="circled-icon" href="https://twitter.com/ormcorussia" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <!--<li><a class="circled-icon" href="https://www.instagram.com/ormcorussia/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>-->
                            <li><a class="circled-icon" href="https://t.me/ormcorussia" target="_blank"><i class="fa fa-telegram" aria-hidden="true"></i></a></li>
                        </ul>
                        <ul class="payment-icons">
                            <li><img src="{$template-resources}img/mc.png" alt="mc" /></li>
                            <li><img src="{$template-resources}img/visa.png" alt="visa" /></li>
                            <li><img style="height:19px;" src="{$template-resources}img/payanyway.png" alt="payanyway" /></li>
                        </ul>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <ul class="social-icons">
                            <li><a target="_blank" class="ga_applestore" href="https://itunes.apple.com/us/app/%D0%BE%D0%B1%D1%83%D1%87%D0%B5%D0%BD%D0%B8%D0%B5-%D1%81-ormco/id1212861947?mt=8"><img style="width:150px;" src="{$template-resources}img/1306.png" alt="" /></a></li>
                            <li><a target="_blank" class="ga_googleplay" href="https://play.google.com/store/apps/details?id=dpromo.app.ormco"><img style="width:150px;" alt="" src="{$template-resources}img/GooglePlay.png" /></a></li>

                        </ul>
                    </div>
                    <div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
                        <!-- <a class="old-site-lnk" href="http://orthodontia.ru/" target="_blank">Сайт ШОО</a> -->
                        <span class="site-creator">Сайт разработан в студии <a href="http://www.realred.ru/" target="_blank">RED</a></span>
                    </div>
                </div>
            </div>
        </footer>
    </xsl:template>
</xsl:stylesheet>