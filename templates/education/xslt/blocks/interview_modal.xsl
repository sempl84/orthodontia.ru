<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:umi="http://www.umi-cms.ru/TR/umi"
				xmlns:xlink="http://www.w3.org/TR/xlink">

	<xsl:template name="modal_interview">  
	    <!-- модальное окно для отзыва о сервисе заказа мероприятия -->
	    <div class="modal fade in interview-modal" id="interviewModal" tabindex="-1" role="dialog" aria-labelledby="interviewModal">
	        <div class="modal-dialog" role="document">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
	                    <div class="header4 modal-title">Опрос</div>
	                </div>
	
	                <div class="modal-body">
	                    <div class="form-group js-interview--wrapper">
	                        <div class="interview">
	                            <input type="hidden" class="form-control js-interview--orderId" value="{document('udata://emarket/eventId/')/udata}" data-usertype="{$user-type}" name="orderId" />
	                            <input type="hidden" class="form-control js-interview--starNumber" value="0" name="starNumber" />
	
	                            <div class="interview__errors js-interview--errors"></div>
	
	                            <p class="interview__description">
	                            	Спасибо, что обучаетесь вместе с Ormco!<br/>
	                            	Оцените, пожалуйста, насколько вам удобно пользоваться нашим сайтом orthodontia.ru
	                            </p>
	
	                            <div class="interview__stars">
	                                <i class="fa fa-star interview__stars-item js-interview--star-item" data-starNumber="1"></i>
	                                <i class="fa fa-star interview__stars-item js-interview--star-item" data-starNumber="2"></i>
	                                <i class="fa fa-star interview__stars-item js-interview--star-item" data-starNumber="3"></i>
	                                <i class="fa fa-star interview__stars-item js-interview--star-item" data-starNumber="4"></i>
	                                <i class="fa fa-star interview__stars-item js-interview--star-item" data-starNumber="5"></i>
	                            </div>
	
	                            <div class="interview__comment js-interview--comment-block">
	                                <textarea rows="3" placeholder="Опишите, пожалуйста, что нам можно улучшить" class="form-control interview__comment-textarea js-interview--comment" name="comment"></textarea>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	
	                <div class="modal-footer">
	                    <button type="submit" class="btn btn-default js-interview--later">Отменить</button>
	                    <button type="submit" class="btn btn-primary pull-right js-interview--send">Отправить</button>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- END. модальное окно для отзыва о сервисе заказа мероприятия -->
	    
	    <!-- результат -->
	    <div class="modal fade in interview-modal" id="interviewModalResult" tabindex="-1" role="dialog" aria-labelledby="interviewModalResult">
	        <div class="modal-dialog" role="document">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
	                    <div class="header4 modal-title">Спасибо, за оценку</div>
	                </div>
	
	                <div class="modal-body">
	                    <div class="form-group js-interview--wrapper">
	                        <div class="interview">
	                            <input type="hidden" class="form-control js-interview--orderId" value="{document('udata://emarket/eventId/')/udata}" name="orderId" />
	                            <input type="hidden" class="form-control js-interview--starNumber" value="0" name="starNumber" />
	
	                            <div class="interview__errors js-interview--errors"></div>
	
	                            <p class="interview__description">
	                            	Ваше мнение помогает нам становиться лучше!
	                            </p>
	                        </div>
	                    </div>
	                </div>
	
	                <div class="modal-footer">
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- END. модальное окно для отзыва о сервисе заказа мероприятия -->
	</xsl:template>
	
</xsl:stylesheet>