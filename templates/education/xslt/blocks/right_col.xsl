<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:umi="http://www.umi-cms.ru/TR/umi"
				xmlns:xlink="http://www.w3.org/TR/xlink">

	<xsl:template match="result" mode="right_col">  
		<xsl:call-template name='social_links'/>
	   	<xsl:apply-templates select="document('udata://banners/fastInsert/(right_block)')/udata" mode="right" />
	   	<xsl:apply-templates select="document('udata://banners/fastInsert/(right_block_2)')/udata" mode="right" />
	   	<xsl:apply-templates select="document('udata://banners/fastInsertCatalogItem/(catalog_item_right_col)/?extProps=price,price_origin,event_url_redirect,gorod_in,speaker,photo,publish_date,finish_date,mesto_provedeniya,event_type,level,organizer,price_string,level_shoo,max_reserv,curr_reserv,hide_reg_button')/udata" mode="right" />
	</xsl:template>
	
</xsl:stylesheet>