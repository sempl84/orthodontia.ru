<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:umi="http://www.umi-cms.ru/TR/umi"
				xmlns:xlink="http://www.w3.org/TR/xlink">

	<xsl:template match="result" mode="scripts">
		<!-- Bootstrap Core JavaScript -->
		<script src="/min/index.php?f={$template-resources}js/bootstrap.min.js,{$template-resources}js/jquery-ui.min.js,{$template-resources}js/photoswipe.min.js,{$template-resources}js/photoswipe-ui-default.min.js,{$template-resources}js/filters.js,{$template-resources}js/bootstrap-select.min.js,{$template-resources}js/jquery.validate.js,{$template-resources}js/jquery.validate.unobtrusive.js,{$template-resources}js/jquery.mask.min.js,{$template-resources}js/seminars.js,{$template-resources}js/carousel-swipe.js,{$template-resources}js/jquery.cookie.js,{$template-resources}js/interview.js,{$template-resources}js/custom.js,{$template-resources}js/new.js,{$template-resources}js/ga.js"></script>

		<!--<script src="{$template-resources}js/bootstrap.min.js?v={$build_version}"></script>-->
		<!--<script src="{$template-resources}js/jquery-ui.min.js?v={$build_version}"></script>-->

		<!--<xsl:if test="@module = 'photoalbum'">-->
			<!--<script src="{$template-resources}js/photoswipe.min.js?v={$build_version}"></script>-->
			<!--<script src="{$template-resources}js/photoswipe-ui-default.min.js?v={$build_version}"></script>-->
		<!--</xsl:if>-->

<!--		<script src="{$template-resources}js/retina.min.js?v={$build_version}"></script>-->

		<!--<script src="{$template-resources}js/filters.js?v={$build_version}"></script>-->
		<!--<script src="{$template-resources}js/bootstrap-select.min.js?v={$build_version}"></script>-->

		<!--<script src="{$template-resources}js/jquery.validate.js"></script>-->

		<!--<script src="{$template-resources}js/jquery.validate.unobtrusive.js?v={$build_version}"></script>-->
		<!--<script src="{$template-resources}js/jquery.mask.min.js?v={$build_version}"></script>-->
		<!--<script src="{$template-resources}js/seminars.js?v={$build_version}"></script>-->

		<!--<script src="{$template-resources}js/carousel-swipe.js"></script>-->
		<!--<script src="{$template-resources}js/jquery.cookie.js"></script>-->


		<!--Временно убрано - возможно не используется-->
<!--		<xsl:if test="user/@status='auth'">
			<! - - валидация телефона, только для админа - - >
			<script src="{$template-resources}js/phone-validator.js?v={$build_version}"></script>
		</xsl:if>-->

		<!-- отзыв о сервисе заказа -->
		<!--<script src="{$template-resources}js/interview.js?v={$build_version}"></script>-->

		<!--<script src="{$template-resources}js/custom.js?v={$build_version}"></script>-->

		<!--<script src="{$template-resources}js/new.js?v={$build_version}"></script>-->

		<xsl:if test="document('udata://orthoSiteData/getSiteDataDadataToken/')/udata/token">
			<link href="https://cdn.jsdelivr.net/npm/suggestions-jquery@21.12.0/dist/css/suggestions.min.css" rel="stylesheet" />
			<script src="https://cdn.jsdelivr.net/npm/suggestions-jquery@21.12.0/dist/js/jquery.suggestions.min.js"></script>
			<script src="{$template-resources}js/dadata.js"></script>

			<xsl:if test="user/@type != 'guest'">
				<xsl:if test="/result/@method != 'address' and document('udata://orthoSiteData/getSiteDataIsUserDadataEmpty/')/udata/@is-empty = '1'">
					<script type="text/javascript">
						jQuery(document).ready(function() {
							jQuery('#dadataModal').modal('show');
							jQuery('#dadataModal').on('hide.bs.modal', function (e) {
								e.preventDefault();
								document.location.href = '/users/address/';
							});
						});
					</script>
				</xsl:if>
			</xsl:if>
		</xsl:if>

		<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" async="async"></script>
		<script src="//yastatic.net/share2/share.js" async="async"></script>
	</xsl:template>
</xsl:stylesheet>