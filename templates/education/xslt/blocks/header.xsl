<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:xlink="http://www.w3.org/TR/xlink">

    <xsl:template match="result" mode="header">
        <nav class="main-nav navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
		            <xsl:apply-templates select="$user" mode="header_auth_mobile" />
					<a href="#" class="top_search js-mobile-search-btn"></a>
                    <a class="navbar-brand" href="/">
                        <img src="{$template-resources}img/logo.png" alt="Ormco" />
                        <img src="{$template-resources}img/logo-min.png" alt="Ormco" />
                        <span class="txt-portal">Образовательный портал для ортодонтов</span>
                    </a>
                </div>
				<div class="mobile-search-pnl js-mobile-search-pnl">
	                <form action="/search/search_do/" method="get">
						<input type="text" name="search_string" placeholder="Поиск по сайту" />
						<span></span>
					</form>
	            </div>
                <div id="navbar" class="navbar-collapse collapse" aria-expanded="false" role="navigation">
                    <xsl:apply-templates select="document('udata://menu/draw/main_menu')/udata" mode="main_menu" />

                    <div class="top-nav">
                        <div class="search" >
                            <form action="/search/search_do/" method="get" class="ga_search">
                                <input type="search" class="text-field" name="search_string" placeholder="Поиск по сайту" />
                                <button class="search-btn">
                                    <i class="fa fa-search"></i>
                                </button>
                            </form>
                        </div>
                        <div class="top">
                            <a class="email circled-icon" href="#feedbackModal" data-toggle="modal" data-target="#feedbackModal">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            </a>

                            <ul class="social-icons">
<!--                                <li>
                                    <a class="circled-icon" href="https://facebook.com/ormcorussia" target="_blank">
                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                    </a>
                                </li>-->
                                <li>
                                    <a class="circled-icon" href="https://vk.com/ormcorussia" target="_blank">
                                        <i class="fa fa-vk" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="circled-icon" href="https://www.youtube.com/channel/UCXlGHVez9C5F9vOneqHwmAg" target="_blank">
                                        <i class="fa fa-youtube-play" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="circled-icon" href="https://twitter.com/ormcorussia" target="_blank">
                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                    </a>
                                </li>
<!--                                <li>
                                    <a class="circled-icon" href="https://www.instagram.com/ormcorussia/" target="_blank">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </a>
                                </li>-->
                                <li>
                                    <a class="circled-icon" href="https://t.me/ormcorussia" target="_blank">
                                        <i class="fa fa-telegram" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul>
                            <!-- верхнее меню -->
                            <xsl:apply-templates select="document('udata://menu/draw/top_menu')/udata" mode="top_menu" />
                        </div>
                    </div>
                </div><!--/.nav-collapse -->
            </div>
        </nav>
    </xsl:template>
</xsl:stylesheet>