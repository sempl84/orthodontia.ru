<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:umi="http://www.umi-cms.ru/TR/umi"
				xmlns:xlink="http://www.w3.org/TR/xlink">

	<xsl:template name="social_links">
	    <xsl:param name="class" select="''" />

	    <div class="socials-pnl-right {$class}">
            <div class="ya-share2" data-services="vkontakte,odnoklassniki,twitter" data-size="s" data-counter=""></div>
        </div>
	</xsl:template>

</xsl:stylesheet>