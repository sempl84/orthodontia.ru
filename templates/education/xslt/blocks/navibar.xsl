<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="udata[@method='navibar']">
        <xsl:param name="header" />

        <xsl:variable name="menu_events" select="document('udata://menu/draw/right_menu_events')/udata" />
        <xsl:variable name="menu_common" select="document('udata://menu/draw/right_menu')/udata" />

        <div class="breadcrumbs-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="/{$lang-prefix}">Главная</a>
                            </li>
                            <xsl:if test="not($document-page-id)">
                                <xsl:choose>
                                    <xsl:when test="$header">
                                        <li class="breadcrumb-item active">
                                            <xsl:value-of select="$header" />
                                        </li>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <li class="breadcrumb-item active">
                                            <xsl:value-of select="$document-header" />
                                        </li>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:if>
                            <xsl:choose>
                                <xsl:when test="$menu_events/item/@id = $document-page-id or $menu_events/item/@id = $parents/page/@id">
                                    <li class="breadcrumb-item">
                                        <a href="&all_event_url;">Мероприятия</a>
                                    </li>
                                </xsl:when>
                                <xsl:when test="$menu_common/item/@id = $document-page-id or $menu_common/item/@id = $parents/page/@id">
                                    <xsl:variable name="about_pid" select="document(concat('upage://',&about_pid;))/udata" />
                                    <li class="breadcrumb-item">
                                        <a href="{$about_pid/page/@link}">Корпорация Ormco</a>
                                    </li>
                                </xsl:when>
                            </xsl:choose>
                            <xsl:apply-templates select="items/item" />
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="udata[@method='navibar']/items/item">
        <li class="breadcrumb-item">
            <a href="{@link}">
                <xsl:value-of select="node()" />
            </a>
        </li>
    </xsl:template>

    <xsl:template match="udata[@method='navibar']/items/item[last()]">
        <li class="breadcrumb-item active">
            <xsl:value-of select="node()" />
        </li>
    </xsl:template>
</xsl:stylesheet>