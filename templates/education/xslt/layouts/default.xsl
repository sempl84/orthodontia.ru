<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:xlink="http://www.w3.org/TR/xlink"
                exclude-result-prefixes="xsl umi xlink">

    <xsl:template match="/" mode="layout">
        <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;</xsl:text>
        <html lang="ru">
            <head>
                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />

                <title>
                    <xsl:choose>
                        <xsl:when test="$metas/@title">
                            <xsl:value-of select="$metas/@title" />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$document-title" />
                        </xsl:otherwise>
                    </xsl:choose>
                </title>
                <meta name="description" content="{//meta/description}" >
                    <xsl:if test="$metas/@description">
                        <xsl:attribute name="content">
                            <xsl:value-of select="$metas/@description" />
                        </xsl:attribute>
                    </xsl:if>
                </meta>
                <meta name="keywords" content="{//meta/keywords}" >
                    <xsl:if test="$metas/@keywords">
                        <xsl:attribute name="content">
                            <xsl:value-of select="$metas/@keywords" />
                        </xsl:attribute>
                    </xsl:if>
                </meta>
                <meta name="apple-itunes-app" content="app-id=1212861947" />
                <meta name="google-play-app" content="app-id=dpromo.app.ormco" />

                <link href="/min/index.php?f={$template-resources}css/jquery-ui.min.css,{$template-resources}css/bootstrap.min.css,{$template-resources}css/font-awesome.min.css,{$template-resources}css/photoswipe.css,{$template-resources}css/default-skin.css,{$template-resources}css/bootstrap-select.min.css,{$template-resources}css/phone-validator.css,{$template-resources}css/interview.css,{$template-resources}css/styles.css,{$template-resources}css/new.css,{$template-resources}css/new_custom.css,{$template-resources}css/event-points.css,{$template-resources}css/custom.css" rel="stylesheet" />

                <!-- Bootstrap Core CSS -->
                <!--<link href="{$template-resources}css/jquery-ui.min.css?v={$build_version}" rel="stylesheet" />-->
                <!--<link href="{$template-resources}css/bootstrap.min.css?v={$build_version}" rel="stylesheet" />-->
                <!--<link href="{$template-resources}css/font-awesome.min.css?v={$build_version}" rel="stylesheet" />-->

<!--                <xsl:if test="result[@module = 'photoalbum']">
                    <link href="{$template-resources}css/photoswipe.css?v={$build_version}" rel="stylesheet" />
                    <link href="{$template-resources}css/default-skin.css?v={$build_version}" rel="stylesheet" />
                </xsl:if>-->

                <!--<link href="{$template-resources}css/bootstrap-select.min.css?v={$build_version}" rel="stylesheet" />-->

                <!-- валидация телефона, только для админа -->
                <!--<link href="{$template-resources}css/phone-validator.css?v={$build_version}" rel="stylesheet" />-->

                <!-- отзыв о сервисе заказа -->
                <!--<link href="{$template-resources}css/interview.css?v={$build_version}" rel="stylesheet" />-->

                <!-- Custom CSS -->
                <!--<link href="{$template-resources}css/styles.css?v={$build_version}" rel="stylesheet" />-->

                <!--<xsl:if test="result[@module = 'catalog' and (page/@id = &spec_dealers_pid; or parents/page/@id = &spec_dealers_pid;)]">-->
                    <!--<link href="{$template-resources}css/new.css?v={$build_version}" rel="stylesheet" />-->
                    <!--<link href="{$template-resources}css/new_custom.css?v={$build_version}" rel="stylesheet" />-->
                <!--</xsl:if>-->

                <!--<link href="{$template-resources}css/event-points.css?v={$build_version}" rel="stylesheet" />-->

                <!--<link href="{$template-resources}css/custom.css?v={$build_version}" rel="stylesheet" />-->
                <!--<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,400italic&subset=latin,cyrillic,cyrillic-ext' rel='stylesheet' type='text/css'>-->
                <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
                <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
                <!--[if lt IE 9]>
                    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
                <![endif]-->

                <!--<link rel="icon" href="/favicon.gif" type="image/x-icon" />-->
                <link rel="shortcut icon" href="/favicon.gif" type="image/x-icon" />

				<link rel="preload" href="/templates/education/fonts/opensans-semibold.woff2" as="font" crossorigin="" />
				<link rel="preload" href="/templates/education/fonts/opensans-regular.woff2" as="font" crossorigin="" />
				<link rel="preload" href="/templates/education/fonts/fontawesome-webfont.woff2" as="font" crossorigin="" />

                <!--
                <link rel="stylesheet" href="/templates/demodizzy/css/jquery-ui.css" />
                <script src="/templates/demodizzy/js/jquery-1.10.2.js"></script>
                <script src="/templates/demodizzy/js/jquery-ui.js"></script>
                <script>
                        var lastJQ = $.noConflict();
                </script>

                <xsl:value-of select="document('udata://system/includeQuickEditJs')/udata" disable-output-escaping="yes" />
                <script type="text/javascript" src="/templates/demodizzy/js/i18n.{result/@lang}.js"></script>
                <script type="text/javascript" charset="utf-8" src="/templates/demodizzy/js/__common.js"></script>

                <link type="text/css" rel="stylesheet" href="/templates/demodizzy/css/common.css" />

                <xsl:if test="$method = 'compare'">
                        <link type="text/css" rel="stylesheet" href="/templates/demodizzy/css/two_left.css" />
                </xsl:if>
                <link rel="canonical" href="http://{concat(result/@domain, result/@request-uri)}" />
                <script type="text/javascript" src="http://apis.google.com/js/plusone.js">
                        {lang: 'ru'}
                </script>

                <script type="text/javascript" src="https://vk.com/js/api/openapi.js"></script>
                <script src="http://vkontakte.ru/js/api/openapi.js" type="text/javascript" charset="windows-1251"></script>

                <link media="print" type="text/css" rel="stylesheet" href="/templates/demodizzy/css/print.css" />
                -->

                <!-- jQuery -->
                <script src="{$template-resources}js/jquery-2.2.4.min.js?v={$build_version}"></script>

                <xsl:value-of select="$settings//property[@name='scripts_head']/value" disable-output-escaping="yes" />
                <script>window.dataLayer = window.dataLayer || [];</script>

                <!-- Добавление разметки google event -->
                <xsl:if test="result/@module = 'catalog' and result/@method='object' and ($parents/page/@id = &ormco_club_pid; or $parents/page/@id = &all_event_pid;)">
                    <script type="application/ld+json">
                        <xsl:value-of select="document('udata://catalog/google_event/')/udata" disable-output-escaping="yes" />
                    </script>
                </xsl:if>
            </head>
            <body>
				<xsl:if test="$top_information_string_close != 1 and ($settings//property[@name = 'pokazyvat_informacionnuyu_stroku']/value = 1)">
					<xsl:attribute name="class">with_top_info_string</xsl:attribute>
				</xsl:if>

                <xsl:apply-templates select="result" mode="header" />
                <xsl:apply-templates select="result" />
                <xsl:apply-templates select="result" mode="representations"/>
                <xsl:apply-templates select="result" mode="footer" />
                <xsl:apply-templates select="result" mode="modals" />
                <xsl:apply-templates select="result" mode="scripts" />

                <xsl:value-of select="$settings//property[@name='counters']/value" disable-output-escaping="yes" />
                <div class="waiting" style="display:none;"></div>

				<xsl:if test="$top_information_string_close != 1 and ($settings//property[@name = 'pokazyvat_informacionnuyu_stroku']/value = 1)">
					<div class="top_information_string" style="color:{$settings//property[@name = 'cvet_teksta_informacionnoj_stroki']/value};background-color:{$settings//property[@name = 'cvet_fona_informacionnoj_stroki']/value}">
						<div class="container">
							<noindex>
								<div class="top_info_string_content">
									<xsl:if test="$settings//property[@name = 'ikonka_informacionnoj_stroki']/value">
										<div class="top_info_string_icon" style="color:{$settings//property[@name = 'cvet_ikonki_informacionnoj_stroki']/value};">
											<i class="fa {$settings//property[@name = 'ikonka_informacionnoj_stroki']/value}"></i>
										</div>
									</xsl:if>
									<a class="top_info_string_close" href="#" title="Закрыть" style="color:{$settings//property[@name = 'cvet_teksta_informacionnoj_stroki']/value};">
										<i class="fa fa-close"></i>
									</a>
									<div class="top_info_string_data">
										<div class="top_info_string_data_long">
											<xsl:value-of select="$settings//property[@name = 'tekst_informacionnoj_stroki']/value" disable-output-escaping="yes" />
										</div>
										<div class="top_info_string_data_middle">
											<xsl:value-of select="$settings//property[@name = 'tekst_informacionnoj_stroki_srednij']/value" disable-output-escaping="yes" />
										</div>
										<div class="top_info_string_data_small">
											<xsl:value-of select="$settings//property[@name = 'tekst_informacionnoj_stroki_korotkij']/value" disable-output-escaping="yes" />
										</div>
									</div>
								</div>
							</noindex>
						</div>
					</div>
				</xsl:if>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>