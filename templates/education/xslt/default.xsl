<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xlink="http://www.w3.org/TR/xlink">

    <xsl:output encoding="utf-8" method="html" indent="yes"/>

    <xsl:decimal-format name="price" decimal-separator="," grouping-separator="&#160;"/>

    <xsl:variable name="errors"	select="document('udata://system/listErrorMessages')/udata"/>

    <xsl:variable name="lang-prefix" select="/result/@pre-lang" />
    <xsl:variable name="document-page-id" select="/result/@pageId" />
    <xsl:variable name="document-title" select="/result/@title" />
    <xsl:variable name="document-header" select="/result/@header" />
    <xsl:variable name="user" select="/result/user" />
    <xsl:variable name="user-type" select="/result/user/@type" />
    <xsl:variable name="is-admin" select="$user-type = 'sv' or $user-type = 'admin'" />
    <xsl:variable name="request-uri" select="/result/@request-uri" />
    <xsl:variable name="domain" select="/result/@domain" />

    <xsl:variable name="site-info-id" select="document('upage://contacts')/udata/page/@id" />
    <xsl:variable name="site-info" select="document('upage://contacts')//group[@name = 'site_info']/property" />

    <xsl:variable name="user-id" select="/result/user/@id" />
    <xsl:variable name="user-info" select="document(concat('uobject://', $user-id))" />

    <xsl:variable name="module" select="/result/@module" />
    <xsl:variable name="method" select="/result/@method" />

    <xsl:variable name="cart" select="document('udata://emarket/cart/')/udata" />
    <xsl:variable name="currency-prefix" select="$cart/summary/price/@prefix" />
    <xsl:variable name="currency-suffix" select="$cart/summary/price/@suffix" />

    <xsl:variable name="purchase-method" select="document('udata://emarket/getPurchaseLink')/udata" />

    <!-- custom variables and params -->
    <xsl:variable name="build_version" select="043" />
    <xsl:variable name="curr_url" select="result/page/@link" />
    <xsl:variable name="parents" select="result/parents" />
    <xsl:variable name="is_default_page" select="result/page/@is-default" />
    <xsl:variable name="right_menu_lk" select="document('udata://menu/draw/right_menu_lk')/udata" />
    <xsl:variable name="settings" select="document('upage://&settings_pid;')/udata" />
    <xsl:variable name="metas" select="document('udata://data/systemMeta/')/udata" />
    <xsl:param name="template-resources" />
    <xsl:param name="e" />
    <xsl:param name="mr" /> <!-- mobile reg event id -->
    <xsl:param name="stub_vds" /> <!-- stub for vds -->
    <xsl:param name="o" />
    <!-- /custom variables and params -->

	<!--Верхняя информационная строка-->
    <xsl:param name="top_information_string_close">0</xsl:param>

    <xsl:param name="p">0</xsl:param>
    <xsl:param name="catalog" />
    <xsl:param name="sort_field" />
    <xsl:param name="sort_direction" />
    <xsl:param name="search_string" />

    <xsl:include href="layouts/default.xsl" />
    <xsl:include href="library/common.xsl" />
    <xsl:include href="blocks/common.xsl" />

    <xsl:include href="modules/content/common.xsl" />
    <xsl:include href="modules/users/common.xsl" />
    <xsl:include href="modules/catalog/common.xsl" />
    <xsl:include href="modules/data/common.xsl" />
    <xsl:include href="modules/emarket/common.xsl" />
    <xsl:include href="modules/search/common.xsl" />
    <xsl:include href="modules/news/common.xsl" />
    <xsl:include href="modules/comments/common.xsl" />
    <xsl:include href="modules/webforms/common.xsl" />
    <xsl:include href="modules/banners/common.xsl" />
    <xsl:include href="modules/dispatches/common.xsl" />
    <xsl:include href="modules/faq/common.xsl" />
    <xsl:include href="modules/filemanager/common.xsl" />
    <xsl:include href="modules/photoalbum/common.xsl" />
    <xsl:include href="modules/menu/common.xsl" />

    <xsl:include href="modules/speakers/common.xsl" />
    <xsl:include href="modules/feedbacks/common.xsl" />
    <xsl:include href="modules/library/common.xsl" />
    <xsl:include href="modules/partners/common.xsl" />
    <xsl:include href="modules/webinars/common.xsl" />
    <xsl:include href="modules/cliniccourse/common.xsl" />
    <xsl:include href="modules/slider/common.xsl" />
    <xsl:include href="modules/doclocator/common.xsl" />
</xsl:stylesheet>