<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'filemanager' and @method = 'shared_file']">
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
		                <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

		<!--Content -->
	    <div class="container-fluid speakers-page">
	    	<div class="page-header">
	            <div class="row">
	                <div class="col-xs-12">
	                    <i class="fa fa-file-o" aria-hidden="true"></i>
	                    <h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
	                </div>
	            </div>
	        </div>
	        <div class="row">

	            <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
	                <div class="white-pnl">

	                	<div umi:element-id="{@id}" umi:field-name="content" umi:empty="&empty-page-content;">
							<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
	                	</div>

		                <xsl:apply-templates select="document(concat('udata://filemanager/shared_file//', $document-page-id))/udata">
	                		<xsl:with-param name="content" select=".//property[@name = 'content']/value"/>
						</xsl:apply-templates>
	                </div>
	            </div>

	            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
	                <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
	                <xsl:apply-templates select="." mode="right_col" />
	            </div>
	        </div>
	    </div>
	</xsl:template>

	<xsl:template match="udata[@module = 'filemanager'][@method = 'shared_file']" />
	<xsl:template match="udata[@module = 'filemanager' and @method = 'shared_file' and file_size &gt;0]">
		<xsl:param name="content" />
		<xsl:if test="$content">
			<hr/>
		</xsl:if>
		<div class="gray_block">
			<p>
				<xsl:text>&download-file-name;: </xsl:text>
				<xsl:value-of select="file_name" />
			</p>
			<p>
				<xsl:text>&download-file-size;: </xsl:text>
				<xsl:value-of select="file_size" />
				<xsl:text> Kb</xsl:text>
			</p>
			<p>
				&download-part-one; <a href="{download_link}">&download-part-two;</a>
			</p>
			<script languge="text/javascript">
				<xsl:text>window.setTimeout('document.location.href="</xsl:text>
				<xsl:value-of select="download_link" />
				<xsl:text>";', 10000);</xsl:text>
			</script>
		</div>
	</xsl:template>



</xsl:stylesheet>