<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

    <xsl:include href="blocks/payment_online.xsl" />

    <xsl:include href="pages/default.xsl" />
    <xsl:include href="pages/404.xsl" />
    <xsl:include href="pages/sitemap.xsl" />
    <xsl:include href="pages/main.xsl" />
    <xsl:include href="pages/contacts.xsl" />
    <xsl:include href="pages/mobile_reg.xsl" />
    <xsl:include href="pages/loyalty.xsl" />
    <xsl:include href="pages/sale_office.xsl" />
</xsl:stylesheet>