<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<!-- Контакты отдела продаж - конкретный менеджер -->
	<xsl:template match="result[@module = 'content' and parents/page/@id = '1364']">
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
		                <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

		<!--Content -->
	    <div class="container-fluid contacts">
	        <div class="page-header">
	            <div class="row">
	                <div class="col-xs-12">
	                    <i class="fa fa-building" aria-hidden="true">
	                    	<xsl:if test=".//property[@name='icon_style']/value">
								<xsl:attribute name="class"><xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" /></xsl:attribute>
							</xsl:if>
	                    </i><h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
	                </div>
	            </div>
	        </div>
	        <div class="row">
	            <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
	                <div class="white-pnl">
						<div class="row">
							<div class="col-xs-12">
								<h3><xsl:value-of select="page/name" disable-output-escaping="yes" /></h3>
								<h3><xsl:value-of select=".//property[@name = 'opisanie11']/value" disable-output-escaping="yes" /></h3>
								<p><i class="fa fa-phone" aria-hidden="true"></i> Тел.: <xsl:value-of select=".//property[@name = 'telefon11']/value" disable-output-escaping="yes" /></p>
								<p><i class="fa fa-envelope-o" aria-hidden="true"></i> E-mail.: <a href="mailto:{.//property[@name = 'email11']/value}"><xsl:value-of select=".//property[@name = 'email11']/value" disable-output-escaping="yes" /></a></p>
							</div>
						</div>
					</div>
	            </div>

	            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
	                <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
	                <xsl:apply-templates select="." mode="right_col" />
	            </div>
	        </div>
	    </div>
	</xsl:template>
</xsl:stylesheet>