<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'content' and page/@alt-name = 'contacts']">
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
		                <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

		<!--Content -->
	    <div class="container-fluid contacts">
	        <div class="page-header">
	            <div class="row">
	                <div class="col-xs-12">
	                    <i class="fa fa-building" aria-hidden="true">
	                    	<xsl:if test=".//property[@name='icon_style']/value">
								<xsl:attribute name="class"><xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" /></xsl:attribute>
							</xsl:if>
	                    </i><h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
	                </div>
	            </div>
	        </div>
	        <div class="row">
	            <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
	                <!-- <div class="white-pnl">
	                    <article umi:element-id="{$document-page-id}" umi:field-name="content" umi:empty="&empty-page-content;">
							<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
	                    </article>
	                </div> -->
	                <div class="white-pnl">
						<div class="row">
							<div class="col-xs-8">
								<h3>Офис в Санкт-Петербурге</h3>
								<p><i class="fa fa-map-marker" aria-hidden="true"></i> <xsl:value-of select=".//property[@name='adres1']/value" /></p>
								<p><i class="fa fa-phone" aria-hidden="true"></i> Тел.: <xsl:value-of select=".//property[@name='tel1']/value" /></p>
								<p><i class="fa fa-phone" aria-hidden="true"></i> Факс.: <xsl:value-of select=".//property[@name='faks1']/value" /> </p>
								<!-- <p><i class="fa fa-envelope-o" aria-hidden="true"></i> Эл. почта: <a href="mailto:sales.stp@ormco.com">sales.stp@ormco.com</a></p>
								 -->
								 <p><i class="fa fa-clock-o" aria-hidden="true"></i> Режим работы: <xsl:value-of select=".//property[@name='jobt1']/value" /></p>
								<!-- <h4>Обучение</h4> -->
								<!-- <xsl:value-of select=".//property[@name='email1']/value" disable-output-escaping="yes" /> -->
								<xsl:apply-templates select=".//group[contains(@name, 'manager_spb')]" mode="manager" />

								<!-- <p><i class="fa fa-envelope-o" aria-hidden="true"></i> Эл. почта: <a href="mailto:dentalseminar@ormco.com">dentalseminar@ormco.com</a></p>
								<p><i class="fa fa-phone" aria-hidden="true"></i> Тел.: +7 (921) 969-77-68</p> -->
							</div>
							<div class="col-xs-4 map">
								<xsl:value-of select=".//property[@name='map1']/value" disable-output-escaping="yes" />
								<!-- <script type="text/javascript" charset="utf-8" async="async" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=ZpNAY7mqVQgQH-DrTcm5maMEj6x_Xtsq&amp;width=100%25&amp;height=240&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script>
							--> </div>
						</div>
					</div>
					<div class="white-pnl">
						<div class="row">
							<div class="col-xs-8">
								<h3>Офис в Москве</h3>
								<p><i class="fa fa-map-marker" aria-hidden="true"></i> <xsl:value-of select=".//property[@name='adres2']/value" /></p>
								<p><i class="fa fa-phone" aria-hidden="true"></i> Тел.: <xsl:value-of select=".//property[@name='tel2']/value" /></p>
								<p><i class="fa fa-phone" aria-hidden="true"></i> Факс.: <xsl:value-of select=".//property[@name='faks2']/value" /> </p>
								<!-- <p><i class="fa fa-envelope-o" aria-hidden="true"></i> Эл. почта: <a href="mailto:sales.stp@ormco.com">sales.stp@ormco.com</a></p>
								 -->
								 <p><i class="fa fa-clock-o" aria-hidden="true"></i> Режим работы: <xsl:value-of select=".//property[@name='jobt2']/value" /></p>
								<!-- <h4>Обучение</h4> -->
								<xsl:apply-templates select=".//group[contains(@name, 'manager_msk')]" mode="manager" />
								<!-- <xsl:value-of select=".//property[@name='email2']/value" disable-output-escaping="yes" /> -->

								<!-- <p><i class="fa fa-map-marker" aria-hidden="true"></i> 125167, Москва, Ленинградский проспект, д. 37, корп. 9</p>
								<p><i class="fa fa-phone" aria-hidden="true"></i> Тел.: (495) 664-75-55</p>
								<p><i class="fa fa-phone" aria-hidden="true"></i> Факс: (495) 664-75-56</p>
								<p><i class="fa fa-envelope-o" aria-hidden="true"></i> Эл. почта: <a href="mailto:sales.stp@ormco.com">sales.stp@ormco.com</a></p>
								<p><i class="fa fa-clock-o" aria-hidden="true"></i> Режим работы: пн–пт с 9:00 до 17:30, сб–вс выходные дни</p>
							 --></div>
							<div class="col-xs-4 map">
								<xsl:value-of select=".//property[@name='map2']/value" disable-output-escaping="yes" />
								<!-- <script type="text/javascript" charset="utf-8" async="async" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=tYIQoAy7Mf2Wv4OFZwYHXL9OUN_XZuK2&amp;width=100%25&amp;height=240&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script>
							 --></div>
						</div>
					</div>
					<div class="white-pnl">
						<div class="row">
							<div class="col-xs-12">
								<h3>Социальные сети</h3>

								<!--<p><i class="fa fa-facebook" aria-hidden="true"></i> Читайте нас на <a href="https://facebook.com/ormcorussia" target="_blank">Facebook</a></p>-->
								<p><i class="fa fa-vk" aria-hidden="true"></i> Присоединяйтесь к нашей <a href="https://vk.com/ormcorussia" target="_blank">группе ВКонтакте</a></p>
								<p><i class="fa fa-youtube-play" aria-hidden="true"></i> Смотрите наш канал на <a href="https://www.youtube.com/channel/UCXlGHVez9C5F9vOneqHwmAg" target="_blank">YouTube</a></p>
								<p><i class="fa fa-twitter" aria-hidden="true"></i> Читайте нас в <a href="https://twitter.com/ormcorussia" target="_blank">Twitter</a></p>
								<p><i class="fa fa-telegram" aria-hidden="true"></i> Следите за нами в <a href="https://t.me/ormcorussia" target="_blank">Telegram</a></p>
								<!--<p><i class="fa fa-instagram" aria-hidden="true"></i> Смотрите нас в <a href="https://www.instagram.com/ormcorussia/" target="_blank">Instagram</a></p>-->
							</div>
						</div>
					</div>
					<div class="white-pnl">
						<div class="row">
							<div class="col-xs-12">
								<h3>Обратная связь</h3>

								<xsl:apply-templates select="document(concat('udata://webforms/add/', &feedback_form_oid;))/udata" mode="feedback_form_contacts">
									<xsl:with-param name="submit_text" select="'Отправить'"/>
								</xsl:apply-templates>
							</div>
						</div>
					</div>
	            </div>

	            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
	                <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
	                <xsl:apply-templates select="." mode="right_col" />
	            </div>
	        </div>
	    </div>
	</xsl:template>

	<xsl:template match="group" mode="manager">
		<xsl:value-of select=".//property[contains(@name, 'opisanie')]/value" disable-output-escaping="yes" />
		<xsl:apply-templates select=".//property[contains(@name, 'email')]" mode="manager_email" />
		<xsl:apply-templates select=".//property[contains(@name, 'telefon')]" mode="manager_telefon" />
	</xsl:template>

	<xsl:template match="property" mode="manager_telefon">
		<p><i class="fa fa-phone" aria-hidden="true"></i> Тел.: <xsl:value-of select="value" /></p>
	</xsl:template>

	<xsl:template match="property" mode="manager_email">
		<p><i class="fa fa-envelope-o" aria-hidden="true"></i> Эл. почта: <a href="mailto:{value}"><xsl:value-of select="value" /></a></p>
	</xsl:template>
</xsl:stylesheet>