<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

    <xsl:template match="result[@module = 'content' and @pageId = &loyalty_pid;]">
        <xsl:variable name="del_loyalty_notify" select="document('udata://users/del_loyalty_notify/')/udata" />
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
	                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

        <!--Breadcrumbs -->
        <xsl:apply-templates select="document('udata://core/navibar/')/udata" />

        <!--Content -->
        <div class="container-fluid loyalty-page">
            <div class="page-header">
                <div class="row">
                    <div class="col-xs-12">
                        <i class="fa fa-building" aria-hidden="true">
                            <xsl:if test=".//property[@name='icon_style']/value">
                                <xsl:attribute name="class"><xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" /></xsl:attribute>
                            </xsl:if>
                        </i><h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <div class="white-pnl">
                        <article class="pc-{$document-page-id} content_wrap" umi:element-id="{$document-page-id}" umi:field-name="content" umi:empty="&empty-page-content;">
                            <xsl:apply-templates select="document('udata://users/loyalty/')/udata" mode="loyalty">
                                <xsl:with-param name="content" select=".//property[@name = 'content']/value" />
                            </xsl:apply-templates>
                        </article>
                        <div class="content_soc_wrap">
                            <xsl:call-template name='social_links'>
                                <xsl:with-param name="class" select="'pull-right'"/>
                            </xsl:call-template>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />

                    <xsl:apply-templates select="." mode="right_col" />
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="udata" mode="loyalty">
        <xsl:param name="content" />

        <div class="event-points-summary">
            <xsl:if test="start_ormco_star">
                <p class="event-points-summary__text"><b>Дата начала участия:</b><xsl:text>&#160;</xsl:text><xsl:value-of select="start_ormco_star" /> г.</p>
                <p class="event-points-summary__text"><b>Через <span class="event-points-summary__text"><xsl:value-of select="day_left" /></span> (<span><xsl:value-of select="finish_ormco_star" />&#160;г.</span>) у вас сгорят звезд: <span><xsl:value-of select="kolichestvo_sgorayuwih_ballov" /></span>.</b></p>
                <br />
            </xsl:if>
            <xsl:value-of select="$content" disable-output-escaping="yes" />
        </div>

        <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="event-points-table event-points-table_existing">
                    <div class="event-points-table__header">
                        Существующие
                    </div>
                    <div class="event-points-table__count">
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <xsl:value-of select="yet_ormco_star" />
                    </div>
                    <div class="event-points-table__text">
                        звезд
                        <p>Этими звездами вы можете оплачивать 50% стоимости участия в мероприятиях</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="event-points-table event-points-table_potential">
                    <div class="event-points-table__header">
                        Потенциальные
                    </div>
                    <div class="event-points-table__count">
                        <i class="fa fa-bolt" aria-hidden="true"></i>
                        <xsl:value-of select="potential_ormco_star" />
                    </div>
                    <div class="event-points-table__text">
                        звезд
                        <p>Эти звезды будут начисленны после посещения мероприятий, на которые вы зарегистрировались</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="event-points-table event-points-table_frozen">
                    <div class="event-points-table__header">
                        Замороженные
                    </div>
                    <div class="event-points-table__count">
                        <i class="fa fa-asterisk" aria-hidden="true"></i>
                        <xsl:value-of select="freez_ormco_star" />
                    </div>
                    <div class="event-points-table__text">
                        звезд
                        <p>Эти звезды будут списаны после посещения мероприятий при оплате которых вы использовали скидку Ormco Stars</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="event-points-buttons">
            <div class="event-points-buttons__item">
                <a href="&link_ormco_star_info;" class="btn btn-points-discount">Условия программы</a>
            </div>
            <div class="event-points-buttons__item">
                <a href="&all_event_url;" class="btn btn-green no_ajax_link_waiting_show">Потратить звезды</a>
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>