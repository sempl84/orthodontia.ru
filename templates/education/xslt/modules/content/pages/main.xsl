<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'content' and page/@is-default = 1]">
		<xsl:variable name="main_bottom1" select="document('udata://banners/fastInsert/(main_bottom1)')/udata" />
		<xsl:variable name="main_bottom2" select="document('udata://banners/fastInsert/(main_bottom2)')/udata" />

		<div class="light-gray content-block _mhz-dn">
	        <div class="container-fluid">
	            <div class="row">
					<!-- Slider. Calendar -->
					<xsl:apply-templates select="document('udata://slider/listElements/751?extProps=link_for_slide,analytics_flag,right_img,content')/udata" mode="slider_block"/>

	            	<div class="col-lg-3 col-md-4 hidden-sm hidden-xs">

	                    <!-- calendar -->
						<xsl:call-template name="calendar" >
		                	<xsl:with-param name="parent_id" select="&all_event_pid;" />
		                </xsl:call-template>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- Events -->
	    <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalogPro//', &all_event_pid;, '/8/1/2/publish_date/1/?extProps=event_url_redirect,gorod_in,speaker,photo,publish_date,finish_date,mesto_provedeniya,event_type,level,organizer,price_string,level_shoo,max_reserv,curr_reserv,price,price_origin,hide_reg_button,akkreditaciya_star'))/udata" mode="short_event_block"/>


	    <!-- News -->
	    <xsl:apply-templates select="document('udata://news/lastlist/&news_pid;//4/1?extProps=publish_time,anons_pic,anons')/udata" mode="short_news_block"/>


	    <!-- Videos -->
	    <xsl:apply-templates select="document('udata://webinars/listElements/&webinars_pid;//4/1/')/udata" mode="short_webinars_block"/>


	    <!-- Discount -->
	    <!-- <div class="discount container-fluid">
	        <div class="row">
	            <div class="col-sm-6">
	                <a href="http://ormco.ru/market/vestibular/kit/aesthetic/inspire-ice/" target="_blank"><img class="img-responsive" src="{$template-resources}img/discount/1.jpg" alt="discount" /></a>
	            </div>
	            <div class="col-sm-6">
	                <a href="http://ormco.ru/market/vestibular/kit/metal/roth/" target="_blank"><img class="img-responsive" src="{$template-resources}img/discount/2.jpg" alt="discount" /></a>
	            </div>
	        </div>
	    </div> -->

	    <xsl:if test="$main_bottom1/banner or $main_bottom2/banner">
		    <div class="discount container-fluid">
		        <div class="row">
		            <div class="col-sm-6">
		            	<xsl:apply-templates select="$main_bottom1" mode="main_bottom" />
		            </div>
		            <div class="col-sm-6">
		                <xsl:apply-templates select="$main_bottom2" mode="main_bottom" />
		            </div>
		        </div>
		    </div>
	    </xsl:if>

	    <!-- Reviews -->
	    <xsl:apply-templates select="document('udata://feedbacks/listElements/&feedbacks_pid;//2/1?extProps=event_type,event_name,feedback_for_event,content,author_info')/udata" mode="short_feedbacks_block"/>


	    <!-- Representations -->
	    <!-- <div class="representations content-block blue hidden-xs">
	        <div class="container-fluid">
	            <div class="row">
	                <div class="col-md-2 hidden-sm">
	                    <span class="info-icon">i</span>
	                </div>
	                <div class="col-md-4 col-sm-6">
	                    <h4>Представительство в Москве</h4>
	                    <p>
	                        Ленинградский проспект, д. 37, корп. 9<br />
	                        Тел. (495) 664-75-55; Факс (495) 664-75-56
	                    </p>
	                </div>
	                <div class="col-md-4 col-sm-6">
	                    <h4>Представительство в Петербурге</h4>
	                    <p>
	                        Малоохтинский пр-т, д. 64, корп. 3<br />
	                        Тел. (812) 324-74-14; Факс (812) 320-20-52
	                    </p>
	                </div>
	                <div class="col-md-2 col-sm-12 right">
	                    <a class="btn btn-white" href="contacts.html">Написать нам</a>
	                </div>
	            </div>
	        </div>
	    </div> -->
	</xsl:template>

</xsl:stylesheet>