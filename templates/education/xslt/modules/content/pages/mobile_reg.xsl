<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

    <xsl:template match="result[@module = 'content' and @pageId = &event_mobile_reg_pid;]">
        <xsl:variable name="isFirstPurchase" select="document(concat('udata://emarket/checkSecondPurchase/',$mr,'/',$user-info//property[@name='e-mail']/value))/udata" />

		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
	                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

        <!--Breadcrumbs -->
        <xsl:apply-templates select="document('udata://core/navibar/')/udata" />

        <!--Content -->
        <div class="container-fluid news-page">
            <div class="page-header">
                <div class="row">
                    <div class="col-xs-12">
                        <i class="fa fa-building" aria-hidden="true">
                            <xsl:if test=".//property[@name='icon_style']/value">
                                <xsl:attribute name="class">
                                    <xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" />
                                </xsl:attribute>
                            </xsl:if>
                        </i>
                        <h1>
                            <xsl:value-of select="@header" disable-output-escaping="yes" />
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <div class="white-pnl">
                        <article class="content_wrap" umi:element-id="{$document-page-id}" umi:field-name="content" umi:empty="&empty-page-content;">
                            <xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
                        </article>
                        <xsl:choose>
                            <xsl:when test="user[@type = 'guest']">
                                <form action="/users/login_do_pre" method="post">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="gridAuth">Авторизация</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="email2" name="e-mail" placeholder="E-mail" data-val="true" data-val-required="true" data-val-email="true" />
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" name="password" placeholder="Пароль" />
                                        </div>
										<p class="popup_auth_error_message"></p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary pull-left">Вход</button>
                                        <a href="{$lang-prefix}/users/forget/" class="forget_pas ga_btn">Забыли пароль?</a>
                                    </div>
                                </form>
                            </xsl:when>
                            <xsl:otherwise>
                                <div id="buyModal" class="register-modal in">
                                    <xsl:choose>
                                        <xsl:when test="$stub_vds=1">
                                            <p>Здравствуйте!</p>
                                            <p>На сайте orthodontia.ru сейчас идут технические работы. Создание учетных записей и регистрация на мероприятия недоступны с 21:00 седьмого октября до 04:00 восьмого октября (МСК).<br/>
                                            Заходите к нам чуть позже и регистрируйтесь на лучшие образовательные мероприятия!</p>
                                            <p>Приносим извинения за доставленные неудобства!</p>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <form class="buyOneClick" action="/emarket/registerEventOneClick/" method="post" enctype="multipart/form-data">
                                                <div class="modal-body">
                                                    <input type="hidden" id="page_id_input" name="page_id" value="{$mr}" />
                                                    <!-- <input type="hidden" name="event_discount_name" value="" />
                                                    <input type="hidden" name="event_discount_value" value="" /> -->
                                                    <input type="hidden" name="data[new][from_mobile]" value="1" />

                                                    <input type="hidden" id="event_price" value="0" />
                                                    <input type="hidden" name="event_discount_name" value="" />
                                                    <input type="hidden" name="event_discount_value" value="" />
                                                    <input type="hidden" name="event_discount_id" value="" />
                                                    <input type="hidden" name="potential_ormco_star" value="" />
                                                    <input type="hidden" name="freez_ormco_star" value="" />

                                                    <xsl:apply-templates select="document('udata://data/getCreateForm/&order-tid;//(form_params)')" mode="simple_form_order"/>
                                                </div>
                                                <div class="modal-footer">
                                                    <xsl:choose>
                                                        <xsl:when test="$isFirstPurchase = 0">
                                                            <a href="#buyModalSecondPurchase" data-price="-" data-page_id="{$document-page-id}" data-toggle="modal" data-target="#buyModalSecondPurchase" class="btn btn-green buy-button">Вы уже зарегистрированы</a>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <button type="submit" class="btn btn-green pull-left buyOneClickButton">Зарегистрироваться</button>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </div>
                                                <script>
                                                    $(document).ready(function() {
                                                        var page_id = '<xsl:value-of select="$mr" />';
                                                        jQuery('#page_id_input').val(page_id);
                                                        jQuery.ajax({
                                                            url : "/udata/custom/outputEventDiscount/" + page_id + "/?transform=modules/catalog/pages/buymodal.xsl",
                                                            dataType : "html",
                                                            success : function(data) {
                                                                $( ".event_discount_wrap" ).remove();
                                                                $(data).insertBefore( "#fblock_order_agree" );
                                                                $('#fblock_order_upload').insertAfter('.event_discount_wrap');
                                                                jQuery('.event_discount_wrap input[type="radio"]').change(function(){
                                                                    var is_checked = jQuery(this).prop('checked'),
                                                                    id = jQuery(this).attr('id');
                                                                    if (is_checked &amp;&amp; (id=='discount_ordinator' || id=='discount_ordinator_30' || id=='discount_doctor' || id=='discount_teacher' || id=='discount_poo')) {
                                                                        $('#fblock_order_upload').fadeIn(200);
                                                                    }else{
                                                                        $('#fblock_order_upload').fadeOut(200);
                                                                    }
                                                                    $('#fblock_order_upload input[type=file]').removeClass('input-validation-error');
                                                                });
                                                            }
                                                        });
                                                        <!-- $(".buyOneClickButton").click(function() {
                                                          var ceil = $("#buyModal #fblock_order_agree #order_agree").val();

                                                          if (ceil == false) {
                                                            alert('Необходимо принять условия Политики Конфиденциальности');
                                                          }
                                                        }); -->
                                                    });
                                                </script>
                                            </form>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </div>
                            </xsl:otherwise>
                        </xsl:choose>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
                    <xsl:apply-templates select="." mode="right_col" />
                </div>
            </div>
        </div>
    </xsl:template>


    <xsl:template match="result[@module = 'content' and @pageId = &consultation_mobile_reg_pid;]">
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
	                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

        <!--Breadcrumbs -->
        <xsl:apply-templates select="document('udata://core/navibar/')/udata" />

        <!--Content -->
        <div class="container-fluid news-page">
            <div class="page-header">
                <div class="row">
                    <div class="col-xs-12">
                        <i class="fa fa-building" aria-hidden="true">
                            <xsl:if test=".//property[@name='icon_style']/value">
                                <xsl:attribute name="class">
                                    <xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" />
                                </xsl:attribute>
                            </xsl:if>
                        </i>
                        <h1>
                            <xsl:value-of select="@header" disable-output-escaping="yes" />
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <div class="white-pnl">
                        <article class="content_wrap" umi:element-id="{$document-page-id}" umi:field-name="content" umi:empty="&empty-page-content;">
                            <xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
                        </article>
                        <xsl:choose>
                            <xsl:when test="user[@type = 'guest']">
                                <form action="/users/login_do_pre" method="post">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="gridAuth">Авторизация</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="email2" name="e-mail" placeholder="E-mail" data-val="true" data-val-required="true" data-val-email="true" />
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" name="password" placeholder="Пароль" />
                                        </div>
										<p class="popup_auth_error_message"></p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary pull-left">Вход</button>
                                        <a href="{$lang-prefix}/users/forget/" class="forget_pas ga_btn">Забыли пароль?</a>
                                    </div>
                                </form>
                            </xsl:when>
                            <xsl:otherwise>
                                <div id="buyModal" class="register-modal in">
                                    <xsl:choose>
                                        <xsl:when test="$stub_vds=1">
                                            <p>Здравствуйте!</p>
                                            <p>На сайте orthodontia.ru сейчас идут технические работы. Создание учетных записей и регистрация на мероприятия недоступны с 21:00 седьмого октября до 04:00 восьмого октября (МСК).<br/>
                                            Заходите к нам чуть позже и регистрируйтесь на лучшие образовательные мероприятия!</p>
                                            <p>Приносим извинения за доставленные неудобства!</p>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <form class="buyOneClick" action="/emarket/submit_consultation_order_form/" method="post" enctype="multipart/form-data">
                                                <div class="modal-body">
                                                    <input type="hidden" id="page_id_input" name="page_id" value="{$mr}" />
                                                    <input type="hidden" name="data[new][from_mobile]" value="1" />
                                                    <input type="hidden" id="event_price" value="0" />
                                                    <xsl:apply-templates select="document('udata://data/getCreateForm/&order-tid;//(form_params)(dannye_dlya_konsultacii)')" mode="simple_form_order"/>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-green pull-left buyOneClickButton">Зарегистрироваться</button>
                                                </div>
                                            </form>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </div>
                            </xsl:otherwise>
                        </xsl:choose>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
                    <xsl:apply-templates select="." mode="right_col" />
                </div>
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>
