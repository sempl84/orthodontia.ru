<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<!-- <xsl:include href="pages/default-groupelements.xsl" /> -->
	<xsl:include href="blocks/right_col_auth.xsl" />
	<xsl:include href="blocks/header_auth.xsl" />
	<xsl:include href="blocks/header_auth_link.xsl" />
	<xsl:include href="blocks/form_element.xsl" />
	<xsl:include href="blocks/legal_list_field.xsl" />

	<xsl:include href="pages/address.xsl" />
	<xsl:include href="pages/authorization.xsl" />
	<xsl:include href="pages/forget.xsl" />
	<xsl:include href="pages/registration.xsl" />
	<xsl:include href="pages/restore.xsl" />
	<xsl:include href="pages/change_email.xsl" />

</xsl:stylesheet>
