<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xlink="http://www.w3.org/TR/xlink">

    <!-- для зарегистрированного пользователя
    modal - регистрация на мероприятие -->
    <xsl:template match="user" mode="header_auth">
        <xsl:choose>
            <xsl:when test="$stub_vds=1">
                <div class="modal fade phone-validator-modal" id="buyModal" tabindex="-1" role="dialog" aria-labelledby="buyModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </button>
                                <div class="header4 modal-title">Технические работы</div>
                            </div>
                            <div class="modal-body">
                                <p>Здравствуйте!</p>
                                <p>На сайте orthodontia.ru сейчас идут технические работы. Создание учетных записей и регистрация на мероприятия недоступны с 21:00 седьмого октября до 04:00 восьмого октября (МСК).<br/>Заходите к нам чуть позже и регистрируйтесь на лучшие образовательные мероприятия!</p>
                                <p>Приносим извинения за доставленные неудобства!</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade phone-validator-modal" id="buyModalSecondPurchase" tabindex="-1" role="dialog" aria-labelledby="buyModalSecondPurchaseLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </button>
                                <div class="header4 modal-title">Технические работы</div>
                            </div>
                            <div class="modal-body">
                                <p>Здравствуйте!</p>
                                <p>На сайте orthodontia.ru сейчас идут технические работы. Создание учетных записей и регистрация на мероприятия недоступны с 21:00 седьмого октября до 04:00 восьмого октября (МСК).<br/>Заходите к нам чуть позже и регистрируйтесь на лучшие образовательные мероприятия!</p>
                                <p>Приносим извинения за доставленные неудобства!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="/result/@method != 'address' and document('udata://orthoSiteData/getSiteDataIsUserDadataEmpty/')/udata/@is-empty = '1'">
                    <div class="modal fade buy-modal" id="dadataModal" tabindex="-1" role="dialog" aria-labelledby="gridAuth">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </button>
                                    <div class="modal-title header4" id="gridAuth">Обновите адрес</div>
                                </div>
                                <div class="modal-body">
                                    <p>Пожалуйста, обновите адрес</p>
                                </div>
                                <div class="modal-footer">
                                    <a href="/users/address/" class="btn btn-green pull-left">Обновить</a>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                </xsl:if>

                <div class="modal fade register-modal wide-modal buy-modal" id="buyModal" tabindex="-1" role="dialog" aria-labelledby="gridAuth">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form class="buyOneClick" action="/emarket/registerEventOneClick/" method="post" enctype="multipart/form-data">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </button>
                                    <h4 class="modal-title" id="gridAuth">Регистрация на мероприятие</h4>
                                </div>
                                <div class="modal-body">
                                    <input type="hidden" id="page_id_input" name="page_id" value="{$document-page-id}" />
                                    <input type="hidden" id="event_price" value="0" />
                                    <input type="hidden" name="event_discount_name" value="" />
                                    <input type="hidden" name="event_discount_value" value="" />
                                    <input type="hidden" name="event_discount_id" value="" />
                                    <input type="hidden" name="potential_ormco_star" value="" />
                                    <input type="hidden" name="freez_ormco_star" value="" />

                                    <xsl:apply-templates select="document('udata://data/getCreateForm/&order-tid;//(form_params)')" mode="simple_form_order"/>

                                    <xsl:if test="$document-page-id != '4486'">
                                        <div class="event-points-amount">
                                            <span class="event-points-amount__text">
                                                Звезды можно обменять на скидку 50%
                                                <!-- Заработайте <b>200 баллов</b> на покупке мероприятия по программе Ormco Stars -->
                                            </span>
                                            <span class="event-points-amount__help" data-toggle="tooltip" data-placement="top" title="" data-original-title="Получайте баллы за участие в платных мероприятиях">
                                                <i class="fa fa-question-circle " aria-hidden="true"></i>
                                            </span>
                                        </div>
                                    </xsl:if>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-green pull-left buyOneClickButton">Зарегистрироваться</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <!-- вывод сообщения о том что человек уже регистрировался на данное мероприятие -->
                <div class="modal fade register-modal wide-modal buy-modal" id="buyModalSecondPurchase" tabindex="-1" role="dialog" aria-labelledby="gridAuth">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </button>
                                <h4 class="modal-title" id="gridAuth">Регистрация на мероприятие</h4>
                            </div>
                            <div class="modal-body">Вы уже зарегистрированы на данное мероприятие</div>
                            <div class="modal-footer"></div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!--
        для гостя
        modal - регистрация на мероприятие с формой авторизации и пояснительным текстом
        modal - регистрация пользователя
        modal - авторизация пользователя
    -->
    <xsl:template match="user[@type = 'guest']" mode="header_auth">
        <xsl:variable name="registrate_type_id" select="document('udata://users/registrate')" />
        <xsl:variable name="reg_fields" select="document($registrate_type_id/udata/@xlink:href)" />

        <xsl:choose>
            <xsl:when test="$stub_vds=1">
                <div class="modal fade phone-validator-modal" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </button>
                                <div class="header4 modal-title">Технические работы</div>
                            </div>
                            <div class="modal-body">
                                <p>Здравствуйте!</p>
                                <p>На сайте orthodontia.ru сейчас идут технические работы. Создание учетных записей и регистрация на мероприятия недоступны с 21:00 седьмого октября до 04:00 восьмого октября (МСК).<br/>Заходите к нам чуть позже и регистрируйтесь на лучшие образовательные мероприятия!</p>
                                <p>Приносим извинения за доставленные неудобства!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="dadataToken" select="document('udata://orthoSiteData/getSiteDataDadataToken/')/udata" />

                <div class="modal fade register-modal wide-modal" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="gridRegister">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form class="js-user-profile-form"  action="/users/registrate_do_pre/" method="post">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </button>
                                    <h4 class="modal-title" id="gridRegister">Регистрация</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="info_block">
                                        <xsl:value-of select="$settings//property[@name='site_reg_info']/value" disable-output-escaping="yes" />
                                    </div>

                                    <h5>Представьтесь, пожалуйста</h5>
									<div class="form-group fn_block_lname hid"><input type="text" class="form-control" id="phone1" name="data[new][phone1]" placeholder="Телефон*" /></div>
                                    <xsl:apply-templates select="$reg_fields//field[@name='lname']" mode="simple_form"/>
                                    <xsl:apply-templates select="$reg_fields//field[@name='fname']" mode="simple_form"/>
                                    <xsl:apply-templates select="$reg_fields//field[@name='father_name']" mode="simple_form"/>
                                    <xsl:apply-templates select="$reg_fields//field[@name='prof_status']" mode="simple_form"/>
                                    <div class="ord_dop_fields collapse ord_dop_fields3">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="city567" placeholder="Другая специализация*" name="data[new][other_specialization]" data-val="true" data-val-required="true"/>
                                        </div>
                                    </div>

									<div class="student_dop_field ord_dop_fields collapse">
										<xsl:apply-templates select="$reg_fields//field[@name='vuz']" mode="simple_form"/>
									</div>

									<hr />
                                    <h5>Контакты для связи</h5>
                                    <xsl:apply-templates select="$reg_fields//field[@name='e-mail']" mode="simple_form"/>

                                    <div class="form-group">
                                        <input type="password" class="form-control" id="password" name="password" value="" placeholder="Пароль" data-val="true" data-val-required="true"/>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="password_confirm" name="password_confirm" value="" placeholder="Повторить Пароль" data-val="true" data-val-required="true"/>
                                    </div>

                                    <xsl:apply-templates select="$reg_fields//field[@name = 'country']" mode="simple_form"/>

                                    <xsl:choose>
                                        <xsl:when test="$dadataToken/token">
                                            <div data-address="dadata-container" style="display: none;">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="registerModalAddress" data-dadata="address" data-token="{$dadataToken/token}" placeholder="Адрес*" value="" name="address" data-val="true" data-val-required="true" />
                                                </div>
                                                <textarea name="dadata" data-dadata="input" style="display:none;"></textarea>
                                            </div>
                                            <div data-address="address-container" style="display: none;">
                                                <xsl:apply-templates select="$reg_fields//field[@name='city']" mode="simple_form"/>
                                            </div>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:apply-templates select="$reg_fields//field[@name='region']" mode="simple_form"/>
                                            <xsl:apply-templates select="$reg_fields//field[@name = 'city']" mode="simple_form"/>
                                        </xsl:otherwise>
                                    </xsl:choose>

                                    <xsl:apply-templates select="$reg_fields//field[@name='phone']" mode="simple_form"/>
<!--                                    <div class="form-group">
                                        <button type="button" class="btn" data-toggle="collapse" data-target=".dop_tel">Добавить доп. телефон</button>
                                    </div>

                                    <div class="form-group collapse dop_tel">
                                        <input type="text" class="form-control" id="phone_office566" placeholder="Доп. телефон" name="data[new][phone_office]" data-val="true"/>
                                    </div>-->
                                    <xsl:apply-templates select="$reg_fields//field[@name='agree']" mode="simple_form"/>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary pull-left">Зарегистрироваться</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </xsl:otherwise>
        </xsl:choose>

        <div class="modal fade register-modal wide-modal" id="authModal" tabindex="-1" role="dialog" aria-labelledby="gridAuth">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="/users/login_do_pre" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </button>
                            <h4 class="modal-title" id="gridAuth">Авторизация</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="text" class="form-control" id="email2" name="e-mail" placeholder="E-mail" data-val="true" data-val-required="true" data-val-email="true" />
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Пароль" />
                            </div>
							<p class="popup_auth_error_message"></p>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary pull-left">Вход</button>
                            <a href="{$lang-prefix}/users/forget/" class="forget_pas ga_btn">Забыли пароль?</a>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <xsl:choose>
            <xsl:when test="$stub_vds=1">
                <div class="modal fade phone-validator-modal" id="buyModal" tabindex="-1" role="dialog" aria-labelledby="buyModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </button>
                                <div class="header4 modal-title">Технические работы</div>
                            </div>
                            <div class="modal-body">
                                <p>Здравствуйте!</p>
                                <p>На сайте orthodontia.ru сейчас идут технические работы. Создание учетных записей и регистрация на мероприятия недоступны с 21:00 седьмого октября до 04:00 восьмого октября (МСК).<br/>Заходите к нам чуть позже и регистрируйтесь на лучшие образовательные мероприятия!</p>
                                <p>Приносим извинения за доставленные неудобства!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </xsl:when>
            <xsl:otherwise>
                <div class="modal fade register-modal wide-modal buy-modal" id="buyModal" tabindex="-1" role="dialog" aria-labelledby="gridAuth">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form action="/users/login_do_pre" method="post">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </button>
                                    <h4 class="modal-title" id="gridAuth">Авторизация</h4>
                                </div>
                                <div class="modal-body">
                                    Для регистрации на мероприятие вам необходимо<br/>
                                    <a href="#registerModal" data-toggle="modal" data-target="#registerModal" data-dismiss="modal">зарегистрироваться</a> или авторизоваться
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="email3" name="e-mail" placeholder="E-mail" data-val="true" data-val-required="true" data-val-email="true" />
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" name="password" placeholder="Пароль" />
                                    </div>
									<p class="popup_auth_error_message"></p>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary pull-left">Вход</button>
                                    <a href="{$lang-prefix}/users/forget/" class="forget_pas ga_btn">Забыли пароль?</a>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
