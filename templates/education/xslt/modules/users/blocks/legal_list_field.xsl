<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="field" mode="legal_list_field">
		<div class="form-group">
			<label for="{@name}{@id}" class="col-xs-12 control-label">
				<xsl:value-of select="@title" />
				<xsl:apply-templates select="." mode="webforms_required_ast" />
			</label>
			<div class="col-xs-12">
				<xsl:apply-templates select="." mode="form_field_type" />
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>