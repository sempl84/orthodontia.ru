<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="user" mode="header_auth_link">
		<li class="dropdown cabinet js-cabinet">
			<xsl:if test="$right_menu_lk/item/@id = $document-page-id or $right_menu_lk/item/@id = $parents/page/@id or (concat('/',$module,'/',$method,'/') = $right_menu_lk/item/@link)">
				<xsl:attribute name="class">dropdown cabinet js-cabinet active</xsl:attribute>
			</xsl:if>
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i>Личный кабинет<span class="caret"></span></a>

            <ul class="dropdown-menu">
                <xsl:apply-templates select="$right_menu_lk/item" mode="right_menu" />
            </ul>
        </li>
	</xsl:template>

	<xsl:template match="user[@type = 'guest']" mode="header_auth_link">
		<li class="cabinet js-cabinet auth_links"><a href="#authModal" data-toggle="modal" data-target="#authModal"><i class="fa fa-lock" aria-hidden="true"></i>Вход</a>&#160;/&#160;<a href="#registerModal" data-toggle="modal" data-target="#registerModal">Регистрация</a></li>
	</xsl:template>


	<xsl:template match="user" mode="header_auth_mobile">
		<a href="/users/settings/" class="login cabinet"></a>
	</xsl:template>
	<xsl:template match="user[@type = 'guest']" mode="header_auth_mobile">
		<a href="#authModal" data-toggle="modal" data-target="#authModal" class="login"></a>
	</xsl:template>
</xsl:stylesheet>