<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="user" mode="right_col_auth" />
	<xsl:template match="user[@type = 'guest']" mode="right_col_auth">
		<div class="banner blue">
            <img src="{$template-resources}img/people-icon.png" alt="library" />
            <p>Как получить расширенный доступ к материалам?</p>
            <a class="btn btn-white" href="#registerModal" data-toggle="modal" data-target="#registerModal">Зарегистрироваться</a>
            <p class="or-txt">или</p>
            <a class="btn btn-white" href="#authModal" data-toggle="modal" data-target="#authModal">Войти</a>
        </div>
	</xsl:template>
</xsl:stylesheet>