<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="result[@module = 'users'][@method = 'restore']">
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
		                <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

		<!--Content -->
	    <div class="container-fluid user-profile">
	        <div class="page-header">
	            <div class="row">
	                <div class="col-xs-12">
	                    <i class="fa fa-user" aria-hidden="true">
	                    	<xsl:if test=".//property[@name='icon_style']/value">
								<xsl:attribute name="class"><xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" /></xsl:attribute>
							</xsl:if>
	                    </i><h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
	                </div>
	            </div>
	        </div>
	        <div class="row">
	            <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
	                <div class="white-pnl">
	                    <div class="row">
                        	<div class="col-xs-12">
								<xsl:apply-templates select="document(concat('udata://users/restore/', $param0 ,'/'))/udata" />

		                    </div>
		                </div>
	                </div>
	            </div>

	            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
	                <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
	                <xsl:apply-templates select="." mode="right_col" />
	            </div>
	        </div>
	    </div>
	</xsl:template>

	<xsl:template match="udata[@module = 'users'][@method = 'restore'][@status = 'success']">
		<p>
			<xsl:text>&forget-message;</xsl:text>
		</p>
	</xsl:template>

	<xsl:template match="udata[@module = 'users'][@method = 'restore'][@status = 'fail']">
		<xsl:text>&activation-error;</xsl:text>
	</xsl:template>
</xsl:stylesheet>