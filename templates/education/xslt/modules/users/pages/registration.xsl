<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
				   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				   xmlns:xlink="http://www.w3.org/TR/xlink">

    <xsl:param name="address" />

    <xsl:template match="result[@method = 'registrate']">
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
	                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

        <!--Breadcrumbs -->
        <xsl:apply-templates select="document('udata://core/navibar/')/udata" />

        <!--Content -->
        <div class="container-fluid user-profile">
            <div class="page-header">
                <div class="row">
                    <div class="col-xs-12">
                        <i class="fa fa-user" aria-hidden="true">
                            <xsl:if test=".//property[@name='icon_style']/value">
                                <xsl:attribute name="class"><xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" /></xsl:attribute>
                            </xsl:if>
                        </i>
                        <h1>
                            <xsl:value-of select="@header" disable-output-escaping="yes" />
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <div class="white-pnl">
                        <div class="row">
                            <div class="col-xs-12">
                                <xsl:apply-templates select="$errors" />
                                <div class="info_block">
                                    <xsl:value-of select="$settings//property[@name='site_reg_info']/value" disable-output-escaping="yes" />
                                </div>
                                <xsl:choose>
                                    <xsl:when test="$mr='test'">
                                        <xsl:apply-templates select="document('udata://users/registrate')" mode="test"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:apply-templates select="document('udata://users/registrate')" />
                                    </xsl:otherwise>
                                </xsl:choose>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
                    <xsl:apply-templates select="." mode="right_col" />
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="udata[@method = 'registrate']">
        <xsl:variable name="dadataToken" select="document('udata://orthoSiteData/getSiteDataDadataToken/')/udata" />

        <xsl:variable name="reg_fields" select="document(@xlink:href)" />

        <form class="form-horizontal js-user-profile-form" id="registrate" enctype="multipart/form-data" method="post" action="{$lang-prefix}/users/registrate_do_pre/">
            <h4>Представьтесь, пожалуйста</h4>
			<div class="form-group fn_block_lname hid"><input type="text" class="form-control" id="phone1" name="data[new][phone1]" placeholder="Телефон*" /></div>
            <xsl:apply-templates select="$reg_fields//field[@name='lname']" mode="form"/>
            <xsl:apply-templates select="$reg_fields//field[@name='fname']" mode="form"/>
            <xsl:apply-templates select="$reg_fields//field[@name='father_name']" mode="form"/>
            <xsl:apply-templates select="$reg_fields//field[@name='prof_status']" mode="form"/>
            <div class="ord_dop_fields collapse ord_dop_fields3">
                <div class="form-group">
                    <label for="prof_status716" class="col-xs-3 control-label">Другая специализация*</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="city567" placeholder="Другая специализация*" name="data[new][other_specialization]" data-val="true" data-val-required="true"/>
                    </div>
                </div>
            </div>

            <div class="student_dop_field ord_dop_fields collapse">
				<xsl:apply-templates select="$reg_fields//field[@name='vuz']" mode="form"/>
            </div>

            <hr />
            <h4>Контакты для связи</h4>
            <xsl:apply-templates select="$reg_fields//field[@name='e-mail']" mode="form"/>

            <div class="form-group">
                <label for="inputOldPassword" class="col-xs-3 control-label">Пароль</label>
                <div class="col-xs-9">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Новый пароль" data-val="true" data-val-length-min="3" data-val-required="true"/>
                </div>
            </div>
            <div class="form-group">
                <label for="inputNewPassword" class="col-xs-3 control-label">Пароль ещё раз</label>
                <div class="col-xs-9">
                    <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Новый пароль ещё раз" data-val="true" data-val-equalto="true" data-val-equalto-other="password" data-val-required="true"/>
                    <span>Придумайте пароль не короче 3 символов.</span>
                </div>
            </div>

            <xsl:apply-templates select="$reg_fields//field[@name='country']" mode="form"/>
            <xsl:choose>
                <xsl:when test="$dadataToken/token">
                    <div data-address="dadata-container" style="display: none;">
                        <div class="form-group">
                            <label for="registerAddress" class="col-xs-3 control-label">Адрес*</label>
                            <div class="col-xs-9">
                                <input type="text" class="form-control" id="registerAddress" data-dadata="address" data-token="{$dadataToken/token}" placeholder="Адрес*" value="" name="address" data-val="true" data-val-required="true" />
                            </div>
                        </div>
                        <textarea name="dadata" data-dadata="input" style="display:none;"></textarea>
                    </div>
                    <div data-address="address-container" style="display: none;">
                        <xsl:apply-templates select="$reg_fields//field[@name='city']" mode="form"/>
                    </div>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="$reg_fields//field[@name='region']" mode="form"/>
                    <xsl:apply-templates select="$reg_fields//field[@name='city']" mode="form"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates select="$reg_fields//field[@name='phone']" mode="form"/>
            <xsl:apply-templates select="$reg_fields//field[@name='agree']" mode="form"/>

            <xsl:apply-templates select="document('udata://system/captcha')" />

            <div class="form-group">
                <div class="col-xs-offset-3 col-xs-9">
                    <button type="submit" class="btn btn-primary">Зарегистрироваться</button>
                </div>
            </div>
        </form>
    </xsl:template>

    <xsl:template match="result[@method = 'registrate_done']">
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
	                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

        <!--Breadcrumbs -->
        <xsl:apply-templates select="document('udata://core/navibar/')/udata" />

        <!--Content -->
        <div class="container-fluid user-profile">
            <div class="page-header">
                <div class="row">
                    <div class="col-xs-12">
                        <i class="fa fa-user" aria-hidden="true">
                            <xsl:if test=".//property[@name='icon_style']/value">
                                <xsl:attribute name="class">
                                    <xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" />
                                </xsl:attribute>
                            </xsl:if>
                        </i>
                        <h1>
                            <xsl:value-of select="@header" disable-output-escaping="yes" />
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <div class="white-pnl">
                        <div class="row">
                            <div class="col-xs-12">
                                <xsl:apply-templates select="document('udata://users/registrate_done')/udata"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
                    <xsl:apply-templates select="." mode="right_col" />
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="udata[@method = 'registrate_done']">
        <xsl:choose>
            <xsl:when test="result = 'without_activation'">
                <h4>
                    <xsl:text>&registration-done;</xsl:text>
                </h4>
            </xsl:when>
            <xsl:when test="result = 'error'">
                <h4>
                    <xsl:text>&registration-error;</xsl:text>
                </h4>
            </xsl:when>
            <xsl:when test="result = 'error_user_exists'">
                <h4>
                    <xsl:text>&registration-error-user-exists;</xsl:text>
                </h4>
            </xsl:when>
            <xsl:otherwise>
                <h4>
                    <xsl:text>&registration-done;</xsl:text>
                </h4>
                <p>
                    <xsl:text>&registration-activation-note;</xsl:text>
                </p>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <xsl:template match="result[@method = 'activate']">
        <xsl:variable name="activation-errors" select="document('udata://users/activate')/udata/error" />

		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
	                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

        <!--Breadcrumbs -->
        <xsl:apply-templates select="document('udata://core/navibar/')/udata" />

        <!--Content -->
        <div class="container-fluid user-profile">
            <div class="page-header">
                <div class="row">
                    <div class="col-xs-12">
                        <i class="fa fa-user" aria-hidden="true">
                            <xsl:if test=".//property[@name='icon_style']/value">
                                <xsl:attribute name="class">
                                    <xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" />
                                </xsl:attribute>
                            </xsl:if>
                        </i>
                        <h1>
                            <xsl:value-of select="@header" disable-output-escaping="yes" />
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <div class="white-pnl">
                        <div class="row">
                            <div class="col-xs-12">

                                <xsl:choose>
                                    <xsl:when test="count($activation-errors)">
                                        <xsl:apply-templates select="$activation-errors" />
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <p>
                                            <xsl:text>&account-activated;</xsl:text>
                                        </p>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
                    <xsl:apply-templates select="." mode="right_col" />
                </div>
            </div>
        </div>
    </xsl:template>

    <!-- User settings -->
    <xsl:template match="result[@method = 'settings']">
        <xsl:if test="$user-type = 'guest'">
            <xsl:value-of select="document('udata://content/redirect/(/users/auth/)')" />
        </xsl:if>

		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
	                    <xsl:apply-templates select="document('udata://menu/draw/right_menu_lk')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

        <!--Breadcrumbs -->
        <div class="breadcrumbs-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Главная</a></li>
                            <li class="breadcrumb-item"><a href="/users/settings/">Личный кабинет</a></li>
                            <li class="breadcrumb-item active"><xsl:value-of select="@header" disable-output-escaping="yes" /></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <!--Content -->
        <div class="container-fluid news-page">
            <div class="page-header">
                <div class="row">
                    <div class="col-xs-12">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        <h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <div class="white-pnl">
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- форма с данными -->
                                <xsl:apply-templates select="$errors" />

                                <xsl:if test="$address = 'ok'">
                                    <div class="text-success"><p>Данные успешно сохранены</p></div>
                                </xsl:if>


                                <xsl:apply-templates select="document('udata://users/settings')" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
                    <xsl:apply-templates select="document('udata://menu/draw/right_menu_lk')/udata" mode="right_menu" />
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="udata[@method = 'settings']">
        <xsl:variable name="legalList" select="document('udata://emarket/legalList')" />
        <xsl:variable name="setting_fields" select="document(concat('udata://data/getEditForm/', $user-id))" />

        <form enctype="multipart/form-data" method="post" action="{$lang-prefix}/users/settings_do_pre/" class="form-horizontal js-user-profile-form" id="personal_information_form">
            <h4>Персональная информация</h4>
            <xsl:apply-templates select="$setting_fields//field[@name='lname']" mode="form"/>
            <xsl:apply-templates select="$setting_fields//field[@name='fname']" mode="form"/>
            <xsl:apply-templates select="$setting_fields//field[@name='father_name']" mode="form"/>
            <xsl:apply-templates select="$setting_fields//field[@name='prof_status']" mode="form"/>
            <div  class="ord_dop_fields collapse ord_dop_fields3">
                <xsl:if test="not($user-type  = 'guest') and ($user-info//property[@name='other_specialization']/value or $user-info//property[@name='prof_status']/value/item/@id = 12137)">
                    <xsl:attribute name="class">ord_dop_fields collapse in ord_dop_fields3</xsl:attribute>
                </xsl:if>
                <div class="form-group">
                    <label for="prof_status716" class="col-xs-3 control-label">Другая специализация*</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control"  placeholder="Другая специализация*" name="data[{$user-id}][other_specialization]" data-val="true" data-val-required="true">
                            <xsl:if test="not($user-type  = 'guest') and $user-info//property[@name='other_specialization']/value">
                                <xsl:attribute name="value"><xsl:value-of select="$user-info//property[@name='other_specialization']/value" /></xsl:attribute>
                            </xsl:if>
                        </input>
                    </div>
                </div>
            </div>

            <hr />
            <h4>Контакты для связи</h4>
            <xsl:apply-templates select="$setting_fields//field[@name='e-mail']" mode="form"/>

			<xsl:if test="$setting_fields//field[@name='email_dlya_izmeneniya']/text() != ''">
				<div class="col-xs-offset-3 col-xs-9">
					<p>Вы запросии изменение e-mail адреса. Новый e-mail <a href="mailto:{$setting_fields//field[@name='email_dlya_izmeneniya']/text()}"><xsl:value-of select="$setting_fields//field[@name='email_dlya_izmeneniya']/text()" /></a>.</p>
					<p>Чтобы подтвердить изменение - перейдите по ссылке в письме, отправленном вам на новый e-mail. Если вы не получили ссылку - вы можете повторно отправить письмо на новый e-mail со ссылкой для подтвердения.</p>
					<p>
						<a href="/udata/users/send_confirm_email_letter/" class="btn btn-primary">Отправить повторное письмо</a>
						<xsl:text> </xsl:text>
						<a href="/udata/users/cancel_change_email/" class="btn btn-primary">Отменить изменение e-mail</a>
					</p>
				</div>
			</xsl:if>

            <div class="form-group">
                <label class="col-xs-3 control-label">Адрес</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" id="address" placeholder="Адрес*" value="{document('udata://users/getUserAddress/')/udata}" data-val="true" data-val-required="true" disabled="" style="color:#999" />
                    <p><a href="/users/address/">Изменить адрес</a></p>
                </div>
            </div>

<!--            <xsl:apply-templates select="$setting_fields//field[@name='country']" mode="form"/>-->
<!--            <xsl:apply-templates select="$setting_fields//field[@name='region']" mode="form"/>-->
<!--            <xsl:apply-templates select="$setting_fields//field[@name='city']" mode="form"/>-->
            <xsl:apply-templates select="$setting_fields//field[@name='phone']" mode="form"/>
            <xsl:apply-templates select="$setting_fields//field[@name='agree']" mode="form"/>

            <h4>Сменить пароль</h4>
            <div class="form-group">
                <label for="inputOldPassword" class="col-xs-3 control-label">Новый пароль</label>
                <div class="col-xs-9">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Новый пароль" data-val="true" data-val-length-min="3" />
                </div>
            </div>

            <div class="form-group">
                <label for="inputNewPassword" class="col-xs-3 control-label">Новый пароль ещё раз</label>
                <div class="col-xs-9">
                    <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Новый пароль ещё раз" data-val="true" data-val-equalto="true" data-val-equalto-other="password" />
                    <span>Придумайте пароль не короче 3 символов.</span>
                </div>
            </div>
            <hr />

            <div class="form-group">
				<div class="checkbox add-more col-xs-3">
					<label>
						<input type="checkbox" data-toggle="collapse" data-target="#collapseLegalEntity1">
							<xsl:if test="$legalList/udata/items/item">
								<xsl:attribute name="checked">checked</xsl:attribute>
							</xsl:if>
						</input>
						 Оплачивать от юр. лица
					</label>
				</div>

                <div class="col-xs-9">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </div>
        </form>

        <div class="collapse" id="collapseLegalEntity1">
            <xsl:if test="$legalList/udata/items/item">
                <xsl:attribute name="class">collapse in</xsl:attribute>
            </xsl:if>
            <xsl:apply-templates select="$legalList" />
            <hr />
        </div>
    </xsl:template>

    <!-- legalList -->
    <xsl:template match="udata[@method = 'legalList']">
        <xsl:choose>
            <xsl:when test="@xlink:href">
	            <hr />

				<h4>Мои юридические лица</h4>
				<xsl:apply-templates select="items/item" mode="legal_list"/>

				<a href="#" onclick="jQuery('#addLegal').show();return false" class="add_legal_person">
					<i class="glyphicon glyphicon-plus" aria-hidden="true"></i>
					Добавить еще юр. лицо
				</a>

				<div id="addLegal" style="display:none">
					<form enctype="multipart/form-data" method="post" action="{$lang-prefix}/emarket/legalAdd/" class="form-horizontal js-user-profile-form">
						<div class="row">
							<div class="col-md-6">
								<h5 class="fw600">Реквизиты</h5>
								<xsl:apply-templates select="document(@xlink:href)//field[@name = 'name']" mode="legal_list_field" />
								<xsl:apply-templates select="document(@xlink:href)//field[@name = 'inn']" mode="legal_list_field" />
								<xsl:apply-templates select="document(@xlink:href)//field[@name = 'account']" mode="legal_list_field" />
								<xsl:apply-templates select="document(@xlink:href)//field[@name = 'bik']" mode="legal_list_field" />
								<div>
									<button type="submit" class="btn btn-primary">Добавить</button>
								</div>
							</div>
							<div class="col-md-6">
								<h5 class="fw600">Контактные данные</h5>
								<xsl:apply-templates select="document(@xlink:href)//field[@name = 'phone_number']" mode="legal_list_field" />
							</div>
						</div>
					</form>
				</div>
            </xsl:when>
            <xsl:otherwise>
                Вам необходимо <a href="{$lang-prefix}/users/login">авторизоваться</a> или <a href="{$lang-prefix}/users/registrate">зарегистрироваться</a>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <xsl:template match="item" mode="legal_list">
        <xsl:variable name="item_info" select="document(concat('uobject://',@id))/udata" />

		<div class="one_legal_person">
			<div class="one_legal_person_head">
				<div class="one_legal_person_head_item"><xsl:value-of select="$item_info//property[@name = 'name']/value" disable-output-escaping="yes" /></div>
				<div class="one_legal_person_head_item">ИНН: <xsl:value-of select="$item_info//property[@name = 'inn']/value" disable-output-escaping="yes" /></div>
			</div>

			<div class="one_legal_person_form">
				<form enctype="multipart/form-data" method="post" action="{$lang-prefix}/emarket/legalAdd/{@id}" class="form-horizontal js-user-profile-form">
					<div class="row">
						<div class="col-md-6">
							<h5 class="fw600">Реквизиты</h5>
							<xsl:apply-templates select="document(concat('udata://data/getEditForm/',@id))//field[@name = 'name']" mode="legal_list_field" />
							<xsl:apply-templates select="document(concat('udata://data/getEditForm/',@id))//field[@name = 'inn']" mode="legal_list_field" />
							<xsl:apply-templates select="document(concat('udata://data/getEditForm/',@id))//field[@name = 'account']" mode="legal_list_field" />
							<xsl:apply-templates select="document(concat('udata://data/getEditForm/',@id))//field[@name = 'bik']" mode="legal_list_field" />

							<div>
								<a href="/udata/emarket/legal_delete/?legal_person={@id}" class="btn link_delete_legal" title="Удалить" onclick="return confirm('Уверены, что хотите удалить юридическое лицо? Удаление нельзя будет отменить.');">Удалить</a>
								<button type="submit" class="btn btn-primary">Сохранить</button>
							</div>
						</div>
						<div class="col-md-6">
							<h5 class="fw600">Контактные данные</h5>
							<xsl:apply-templates select="document(concat('udata://data/getEditForm/',@id))//field[@name = 'phone_number']" mode="legal_list_field" />
						</div>
					</div>
				</form>
			</div>
		</div>
    </xsl:template>
    <!-- END legalList -->

    <xsl:template match="udata[@method = 'registrate']" mode="test">
        <form class="form-horizontal js-user-profile-form" id="registrate" enctype="multipart/form-data" method="post" action="/users/registrate_do_pre/">
            <h4>Представьтесь, пожалуйста</h4>
			<div class="form-group fn_block_lname hid">
				<label for="phone1" class="col-xs-3 control-label">Телефон*</label>
                <div class="col-xs-9">
					<input type="text" class="form-control" id="phone1" name="data[new][phone1]" placeholder="Телефон*" />
				</div>
			</div>
            <div class="form-group">
                <label for="lname187" class="col-xs-3 control-label">Фамилия*</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" id="lname187" placeholder="Фамилия*" name="data[new][lname]" data-val="true" data-val-required="true"/>
                </div>
            </div>
            <div class="form-group">
                <label for="fname188" class="col-xs-3 control-label">Имя*</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" id="fname188" placeholder="Имя*" name="data[new][fname]" data-val="true" data-val-required="true"/>
                </div>
            </div>
            <div class="form-group">
                <label for="father_name189" class="col-xs-3 control-label">Отчество*</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" id="father_name189" placeholder="Отчество*" name="data[new][father_name]" data-val="true" data-val-required="true"/>
                </div>
            </div>
            <div class="form-group">
                <label for="prof_status716" class="col-xs-3 control-label">Укажите, кто вы*</label>
                <div class="col-xs-9">
                    <select id="prof_status" name="data[new][prof_status]" class="form-control" data-val="true">
                        <option value="" selected="selected" disabled="disabled">Укажите, кто вы</option>
                        <option value="12135">Врач-ортодонт</option>
                        <option value="12135">Хирург</option>
<!--                        <option value="12136" target="ord_dop_fields1">Ординатор</option>-->
<!--                        <option value="121362" target="ord_dop_fields2">Студент</option>-->
                        <option value="12137" target="ord_dop_fields3">Другая специализация</option>
                        <option value="12138">Не врач</option>
<!--                        <option value="121382">Пациент</option>-->
                    </select>
                </div>
            </div>
            <div id="ord_dop_fields1" class="ord_dop_fields collapse">
                <div class="form-group">
                    <label for="prof_status716" class="col-xs-3 control-label">Год поступления в ординатуру*</label>
                    <div class="col-xs-9">
                        <select id="prof_status" name="data[new][prof_status]" class="form-control" data-val="true">
                            <option value="" selected="selected" disabled="disabled">Год поступления в ординатуру*</option>
                            <option>2020</option>
                            <option>2019</option>
                            <option value="12136">2018</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="prof_status716" class="col-xs-3 control-label">Подтверждающий документ*</label>
                    <div class="col-xs-9">
                        <div class="input-group input-file" id="{@name}{/udata/@form_id}" data-name="{@input_name}">
                            <input type="text" class="form-control" placeholder="Загрузить"/>
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="button">Загрузить</button>
                            </span>
                        </div>
                        <span>Удостоверение, справка и т.п. Можно фото с телефона. Максимальный размер файла - 100Mb</span>
                    </div>
                </div>
            </div>
            <div id="ord_dop_fields2" class="ord_dop_fields collapse">
                <div class="form-group">
                    <label for="prof_status716" class="col-xs-3 control-label">Год поступления в ВУЗ*</label>
                    <div class="col-xs-9">
                        <select id="prof_status" name="data[new][prof_status]" class="form-control" data-val="true">
                            <option value="" selected="selected" disabled="disabled">Год поступления в ВУЗ*</option>
                            <option value="12135">2020</option>
                            <option value="12135">2019</option>
                            <option value="12136">2018</option>
                            <option value="12136">2017</option>
                            <option value="12137">2016</option>
                            <option value="12138">2015</option>
                            <option value="12138">2014</option>
                        </select>
                    </div>
                </div>
            </div>
            <div id="ord_dop_fields3" class="ord_dop_fields collapse">
                <div class="form-group">
                    <label for="prof_status716" class="col-xs-3 control-label">Другая специализация*</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="city567" placeholder="Другая специализация*" name="data[new][city]" data-val="true" data-val-required="true"/>
                    </div>
                </div>
            </div>

            <script>
                $('#prof_status').on('change', function() {
                    var option_val = this.value;
                    var targ = $('#prof_status option[value="'+option_val+'"]').attr('target');

                    $('.ord_dop_fields').collapse('hide');
                    $('#'+targ).collapse('show');
                });
            </script>

            <h4>Контакты для связи</h4>

            <div class="form-group">
                <label for="e-mail165" class="col-xs-3 control-label">E-mail*</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" id="e-mail165" placeholder="E-mail*" name="data[new][e-mail]" data-val="true" data-val-required="true"/>
                    <span>Это будет ваш логин.</span>
                </div>
            </div>

            <div class="form-group">
                <label for="inputOldPassword" class="col-xs-3 control-label">Пароль*</label>
                <div class="col-xs-9">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Новый пароль" data-val="true" data-val-length-min="3" data-val-required="true"/>
                </div>
            </div>
            <div class="form-group">
                <label for="inputNewPassword" class="col-xs-3 control-label">Пароль ещё раз*</label>
                <div class="col-xs-9">
                    <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Новый пароль ещё раз" data-val="true" data-val-equalto="true" data-val-equalto-other="password" data-val-required="true"/>
                    <span>Придумайте пароль не короче 3 символов.</span>
                </div>
            </div>

            <div class="form-group">
                <label for="country712" class="col-xs-3 control-label">Страна*</label>
                <div class="col-xs-9">
                    <select id="country" name="data[new][country]" class="form-control country_field" data-val="true" data-val-required="true">
                        <option value="" selected="selected" disabled="disabled">Страна*</option>
                        <option value="12115" data-mask="+7 (000) 0000000" data-mask-placeholder="+7 (___) ___-____">
                            Россия
                        </option>
                        <option value="12116" data-mask="+rr4 (00) 0000000" data-mask-placeholder="+994 (__) ___-____">
                            Азербайджан
                        </option>
                        <option value="12117" data-mask="maskArmenia" data-mask-placeholder="+374 (___) ___-____">
                            Армения
                        </option>
                        <option value="12118" data-mask="+375 (00) 0000000" data-mask-placeholder="+375 (__) ___-____">
                            Белоруссия
                        </option>
                        <option value="12119" data-mask="+rr5 (000) 0000000"
                                                    data-mask-placeholder="+995 (___) ___-____">Грузия
                        </option>
                        <option value="12120" data-mask="+38y (00) 0000000" data-mask-placeholder="+380 (__) ___-____">
                            Украина
                        </option>
                        <option value="12121" data-mask="+7 (000) 0000000" data-mask-placeholder="+7 (___) ___-____">
                            Казахстан
                        </option>
                        <option value="12122" data-mask="+9 (999) 999-9999" data-mask-placeholder="+___ (___) ___-____">
                            Другие
                        </option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="region665" class="col-xs-3 control-label">Регион*</label>
                <div class="col-xs-9">
                    <select id="region" name="data[new][region]" class="form-control region_field" data-val="true" data-val-required="true">
                        <option value="" selected="selected" disabled="disabled">Регион*</option>
                        <option value="9870">Москва и МО</option>
                        <option value="9871">Санкт-Петербург и ЛО</option>
                        <option value="9872">Северо-Западный</option>
                        <option value="9873">Центральный</option>
                        <option value="9874">Сибирский</option>
                        <option value="9875">Приволжский</option>
                        <option value="9876">Южный</option>
                        <option value="9877">Северо-Кавказский</option>
                        <option value="9878">Уральский</option>
                        <option value="9879">Дальневосточный</option>
                        <option value="16761" disabled="disabled">Не Россия</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="city567" class="col-xs-3 control-label">Город*</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" id="city567" placeholder="Город*" name="data[new][city]" data-val="true" data-val-required="true"/>
                </div>
            </div>
            <div class="form-group">
                <label for="phone565" class="col-xs-3 control-label">Мобильный телефон*</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control js-phone" id="phone565" placeholder="Мобильный телефон*" name="data[new][phone]" data-val="true" data-val-required="true"/>
                </div>
            </div>

            <script>
                $('.country_field').on('change', function() {
                    console.log( 'country ch' );
                    console.log( this.value );
                    var option_val = this.value,
                        this_form = $(this).parents('form'),
                        this_region_select = $('.region_field',this_form),
                        russia_val = 12115,
                        not_russia_region_val = 16761;

                    if(option_val != russia_val &amp;&amp; option_val != ''){
                        this_region_select.val(not_russia_region_val);
                        $('option',this_region_select).prop('disabled', true);
                        $('option:last',this_region_select).prop('disabled', false);
                    }else if(option_val == russia_val &amp;&amp; this_region_select.val() == not_russia_region_val){
                        console.log($('.region_field option:first'));
                        $('option',this_region_select).prop('disabled', false);
                        $('option:first',this_region_select).prop('selected', 'selected');
                        $('option:last',this_region_select).prop('disabled', true);
                    }
                });
            </script>

            <div class="form-group agree">
                <div class="col-xs-offset-3 col-xs-9">
                    <div class="checkbox">
                        <label class="show">
                            <input type="hidden" id="agree" name="data[new][agree]"/>
                            <input type="checkbox" class="form-control checkbox" value="0" name="agree_fake" id="agree_fake" onclick="javascript:document.getElementById('agree').value = this.checked;" data-val-required="отметка обязательна" data-val="true"/>
                            <span>Согласие на обработку данных и с <a href="/politika-konfidencial-nosti/" target="_blank">политикой конфиденциальности</a>.*</span>
                            <div class="required_text" data-valmsg-for="agree_fake" data-valmsg-replace="true"></div>
                        </label>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="form-group">
                <div class="col-xs-offset-3 col-xs-9">
                    <button type="submit" class="btn btn-primary">Зарегистрироваться</button>
                </div>
            </div>
        </form>
    </xsl:template>
</xsl:stylesheet>