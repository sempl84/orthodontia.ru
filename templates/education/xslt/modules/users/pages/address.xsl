<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xlink="http://www.w3.org/TR/xlink">

    <xsl:template match="result[@method = 'address']">
        <xsl:variable name="user" select="document(concat('uobject://', user/@id))/udata/object" />
        <xsl:variable name="form" select="document(concat('udata://data/getEditForm/', user/@id, '/'))/udata" />
        <xsl:variable name="dadataToken" select="document('udata://orthoSiteData/getSiteDataDadataToken/')/udata" />

        <nav class="submenu">
            <div class="submenu__inner">
                <div class="submenu__top">
                    Меню раздела
                </div>
                <div class="submenu__content">
                    <div class="submenu__content-inner">
                        <xsl:apply-templates select="document('udata://menu/draw/right_menu_lk')/udata" mode="right_menu" />
                    </div>
                </div>
            </div>
        </nav>

        <div class="breadcrumbs-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Главная</a></li>
                            <li class="breadcrumb-item"><a href="/users/settings/">Личный кабинет</a></li>
                            <li class="breadcrumb-item active"><xsl:value-of select="@header" disable-output-escaping="yes" /></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid news-page">
            <div class="page-header">
                <div class="row">
                    <div class="col-xs-12">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        <h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <div class="white-pnl">
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- форма с данными -->
                                <xsl:apply-templates select="$errors" />

                                <form enctype="multipart/form-data" method="post" action="/users/address/do/" id="address_form" class="form-horizontal" novalidate="novalidate">
                                    <h4>Редактирование адреса</h4>

                                    <xsl:apply-templates select="$form//field[@name='country']" mode="form">
                                        <xsl:with-param name="inputName">country</xsl:with-param>
                                    </xsl:apply-templates>

                                    <div data-address="dadata-container">
                                        <xsl:if test="$user//property[@name = 'country']/value/item[1]/@id != '12115'">
                                            <xsl:attribute name="style">display: none;</xsl:attribute>
                                        </xsl:if>
                                        <xsl:if test="$user//property[@name = 'user_address_raw']/value">
                                            <div class="form-group">
                                                <label class="col-xs-3 control-label">Текущий адрес</label>
                                                <div class="col-sm-9 col-xs-12">
                                                    <input type="text" class="form-control" disabled="" value="{$user//property[@name = 'user_address_raw']/value}" />
                                                </div>
                                            </div>
                                        </xsl:if>
                                        <div class="form-group">
                                            <label for="settingsAddress" class="col-xs-3 control-label">Адрес*</label>
                                            <div class="col-xs-9">
                                                <input type="text" class="form-control" id="settingsAddress" data-dadata="address" data-token="{$dadataToken/token}" placeholder="Адрес*" value="" name="address" data-val="true" data-val-required="true" />
                                            </div>
                                        </div>
                                        <textarea name="dadata" data-dadata="input" style="display:none;"></textarea>
                                    </div>
                                    <div data-address="address-container">
                                        <xsl:if test="$user//property[@name = 'country']/value/item[1]/@id = '12115'">
                                            <xsl:attribute name="style">display: none;</xsl:attribute>
                                        </xsl:if>
                                        <xsl:apply-templates select="$form//field[@name='city']" mode="form">
                                            <xsl:with-param name="inputName">city</xsl:with-param>
                                        </xsl:apply-templates>
                                    </div>

                                    <div class="form-group"><div class="col-xs-offset-3 col-xs-9"><button type="submit" class="btn btn-primary" data-gtm-event="ButtonClicks" data-gtm-event-value="Сохранить">Сохранить</button></div></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
                    <xsl:apply-templates select="document('udata://menu/draw/right_menu_lk')/udata" mode="right_menu" />
                </div>
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>