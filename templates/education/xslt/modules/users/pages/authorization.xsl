<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="udata[@method='getLoginzaProvider']">
		<div class="loginza_block">
			<script src="http://loginza.ru/js/widget.js" type="text/javascript"></script>
			<a href="{widget_url}" class="loginza">
				<img src="http://loginza.ru/img/sign_in_button_gray.gif" alt="Loginza"/>
			</a>
		</div>
	</xsl:template>

	<xsl:template match="result[@method = 'login' or @method = 'login_do' or @method = 'login_do_pre' or @method = 'loginza' or @method = 'auth']">
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
						<xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

		<!--Content -->
		<div class="container-fluid user-profile">
			<div class="page-header">
				<div class="row">
					<div class="col-xs-12">
						<i class="fa fa-user" aria-hidden="true">
							<xsl:if test=".//property[@name='icon_style']/value">
								<xsl:attribute name="class">
									<xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" />
								</xsl:attribute>
							</xsl:if>
						</i>
						<h1>
							<xsl:value-of select="@header" disable-output-escaping="yes" />
						</h1>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
					<div class="white-pnl">
						<div class="row">
							<div class="col-xs-12">
								<xsl:if test="@not-permitted = 1">
									<p>
										Для доступа к странице "<b><xsl:value-of select="document(concat('udata://users/get_page_name_for_not_permited_page/(', /result/@request-uri ,')'))" disable-output-escaping="yes" /></b>" необходимо войти в личный кабинет. Пожалуйста, авторизуйтесь или зарегистрируйтесь на сайте.
									</p>
								</xsl:if>
								<xsl:if test="user[@type = 'guest'] and (@method = 'login_do' or @method = 'login_do_pre' or @method = 'loginza')">
									<p>
										<xsl:text>&user-reauth;</xsl:text>
									</p>
								</xsl:if>
								<xsl:apply-templates select="document('udata://users/auth/')/udata" />
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
					<xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
					<xsl:apply-templates select="." mode="right_col" />
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="udata[@module = 'users'][@method = 'auth']">
		<form class="form-horizontal js-user-profile-form" id="auth" method="post" action="/users/login_do_pre/">
			<input type="hidden" name="from_page" value="{from_page}" />

			<div class="form-group">
				<label for="e-mail" class="col-xs-3 control-label">
					<xsl:text>&e-mail;:</xsl:text>
				</label>
				<div class="col-xs-9">
					<input type="text" class="form-control" id="e-mail" placeholder="&e-mail;*" name="e-mail" data-val="true" data-val-required="true" />
				</div>
			</div>

			<div class="form-group">
				<label for="password" class="col-xs-3 control-label">
					<xsl:text>&password;:</xsl:text>
				</label>
				<div class="col-xs-9">
					<input type="password" class="form-control" id="password" placeholder="&password;*" name="password" data-val="true" data-val-required="true" />
				</div>
				<p class="popup_auth_error_message"></p>
			</div>

			<div class="form-group">
				<div class="col-xs-offset-3 col-xs-9">
					<div class="pull-right">
						<a href="{$lang-prefix}/users/registrate/">
							<xsl:text>&registration;</xsl:text>
						</a>
						<a href="/users/forget/" style="margin:0 15px;">
							<xsl:text>&forget-password;</xsl:text>
						</a>
					</div>
					<button type="submit" class="btn btn-primary">Вход</button>
				</div>
			</div>
		</form>
	</xsl:template>

	<xsl:template match="udata[@module = 'users'][@method = 'auth'][user_id]">
		<div>
			<xsl:text>&welcome; </xsl:text>
			<xsl:choose>
				<xsl:when test="translate(user_name, ' ', '') = ''">
					<xsl:value-of select="user_login" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="user_name" /> (<xsl:value-of select="user_login" />)
				</xsl:otherwise>
			</xsl:choose>
		</div>
		<div>
			<a href="{$lang-prefix}/users/logout/">
				<xsl:text>&log-out;</xsl:text>
			</a>
			<xsl:text> | </xsl:text>
			<a href="{$lang-prefix}/users/settings/">
				<xsl:text>&office;</xsl:text>
			</a>
		</div>
	</xsl:template>
</xsl:stylesheet>