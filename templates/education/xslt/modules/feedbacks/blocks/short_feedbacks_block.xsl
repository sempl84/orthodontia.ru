<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<!-- новости на главной -->
	<xsl:template match="udata[@module='feedbacks']" mode="short_feedbacks_block" />	
	<xsl:template match="udata[@module='feedbacks' and items/item]" mode="short_feedbacks_block">	
		<div class="reviews content-block gray">
	        <div class="container-fluid">
	            <div class="row">
	                <div class="col-sm-12"><div class="header2">Отзывы</div></div>
	                <ul>
	                	<xsl:apply-templates select="items/item" mode="short_feedbacks_block"/>
	                </ul>
	                <div class="col-sm-12 text-center">
	                    <a class="btn btn-primary" href="{document('upage://&feedbacks_pid;')/udata/page/@link}">Все отзывы</a>
	                </div>
	            </div>
	        </div>
	    </div>

	</xsl:template>
	
	<xsl:template match="item" mode="short_feedbacks_block">
		<li class="col-sm-6">
            <xsl:value-of select=".//property[@name='content']/value" disable-output-escaping="yes" />
            <span><xsl:value-of select=".//property[@name='author_info']/value" disable-output-escaping="yes" /></span>
        </li>
	</xsl:template>

</xsl:stylesheet>