<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<!-- отзывы в виде слайдов на странице события -->
	<xsl:template match="property" mode="reviews_slider" />
	<xsl:template match="property[value/page]" mode="reviews_slider">
		<div class="white-pnl clearfix reviews_slider">
			<h3 class="reviews_head">
				Отзывы
				<span class="reviews_count">
					<xsl:value-of select="count(value/page)" />
				</span>
			</h3>

			<div id="reviews_slider" class="carousel slide reviews_slider" data-ride="carousel">
				<div class="carousel-inner" role="listbox">
					<xsl:apply-templates select="value/page" mode="reviews_slider" />
				</div>

				<a class="left carousel-control" href="#reviews_slider" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
				</a>
				<a class="right carousel-control" href="#reviews_slider" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
				</a>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="page" mode="reviews_slider">
		<div class="item">
			<xsl:if test="position() = 1">
				<xsl:attribute name="class">item active</xsl:attribute>
			</xsl:if>

			<div class="one_slider_review_item">
				<div class="one_slider_review_item_text"><xsl:value-of select="document(concat('upage://', @id, '.content'))//value" disable-output-escaping="yes" /></div>
				<div class="one_slider_review_item_author"><xsl:value-of select="document(concat('upage://', @id, '.author_info'))//value" disable-output-escaping="yes" /></div>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>