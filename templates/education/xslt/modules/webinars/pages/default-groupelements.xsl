<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'webinars']">
		<xsl:variable name="all_webinars" select="document('udata://webinars/listElements///10000/1/1')/udata" />
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
		                <xsl:apply-templates select="user" mode="right_col_auth" />
						<xsl:apply-templates select="document('udata://menu/draw/right_menu_events')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

		<!--Content -->
	    <div class="container-fluid webinars">
	        <div class="page-header">
	            <div class="row">
	                <div class="col-xs-12">
	                    <i class="fa fa-play-circle" aria-hidden="true"></i>
	                    <h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
	                </div>
	            </div>
	        </div>
	        <div class="row">
	            <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
	                <xsl:apply-templates select="document('udata://catalog/search/&webinars_pid;')/udata" >
	                	<xsl:with-param name="all_webinars" select="$all_webinars" />
	                </xsl:apply-templates>

	                <xsl:apply-templates select="document('udata://webinars/listElements/')/udata" />
	                <xsl:apply-templates select="$all_webinars" mode="js_date"/>
	            </div>
	            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
					<xsl:apply-templates select="document('udata://menu/draw/right_menu_events')/udata" mode="right_menu" />
	                <xsl:apply-templates select="user" mode="right_col_auth" />

	                <xsl:apply-templates select="." mode="right_col" />
	            </div>
	        </div>
	    </div>
	</xsl:template>

	<xsl:template match="udata[@module='webinars' and @method='listElements']">
		Ничего не найдено
	</xsl:template>
	<xsl:template match="udata[@module='webinars' and @method='listElements' and items/item]">
		<ul class="items videos">
            <xsl:apply-templates select="items/item" />
        </ul>
        <xsl:apply-templates select="total" />
	</xsl:template>

	<xsl:template match="udata[@module='webinars' and @method='listElements']/items/item" >
		<li class="item">
			<xsl:choose>
				<xsl:when test=".//property[@name='library_allow']/value/item/@id = &dealers-oid;">
					<xsl:attribute name="class">item dealers</xsl:attribute>
				</xsl:when>
				<xsl:when test=".//property[@name='library_allow']/value/item/@id = &dealers-oid; and .//property[@name='library_allow']/value/item/@id = &disallow-oid;">
					<xsl:attribute name="class">item locked dealers</xsl:attribute>
				</xsl:when>
				<xsl:when test=".//property[@name='library_allow']/value/item/@id = &disallow-oid; and $user-type='guest'">
					<xsl:attribute name="class">item locked</xsl:attribute>
				</xsl:when>
			</xsl:choose>
			<!-- <xsl:if test=".//property[@name='library_allow']/value/item/@id = &disallow-oid;">
				<xsl:attribute name="class">item locked</xsl:attribute>
			</xsl:if> -->
            <div class="white-pnl clearfix">
                <div class="left">
                    <a class="video-link" href="{@link}">
                        <div class="preview">
                        	<xsl:choose>
								<xsl:when test=".//property[@name='video_thumb']/value">
									<img src="{.//property[@name='video_thumb']/value}" class="img-responsive" alt="{text()}" />
								</xsl:when>
								<xsl:otherwise>
									<img src="&empty-photo-webinars;" class="img-responsive" alt="{text()}" />
								</xsl:otherwise>
							</xsl:choose>

                    		<!-- TODO права доступа -->
							<xsl:if test=".//property[@name='library_allow']/value/item/@id = &disallow-oid; and $user-type='guest'">
								<img class="lock" src="{$template-resources}img/lock-icon.png" alt="" />
							</xsl:if>

                        </div>
                    </a>
                </div>
                <div class="right">
                    <h4><a href="{@link}"><xsl:value-of select="text()" /></a></h4>

                    <xsl:choose>
                        <xsl:when test=".//property[@name='ssylka_na_spikera']/value/item[1]/@id">
                        	<div class="speaker">
                        		<a href="{document(concat('udata://catalog/getLinkByObject/', .//property[@name='ssylka_na_spikera']/value/item[1]/@id))/udata}">
			                        <!--<img src="{document(concat('uobject://', .//property[@name='ssylka_na_spikera']/value/item[1]/@id,'.photo'))//value}" alt="{.//property[@name='ssylka_na_spikera']/value/item[1]/@name}"/>-->
									<xsl:call-template name="makeThumbnailFull_ByPath">
										<xsl:with-param name="source" select="document(concat('uobject://', .//property[@name='ssylka_na_spikera']/value/item[1]/@id,'.photo'))//value" />
										<xsl:with-param name="width" select="35" />
										<xsl:with-param name="height" select="35" />
										<xsl:with-param name="alt" select=".//property[@name='ssylka_na_spikera']/value/item[1]/@name" />
									</xsl:call-template>
			                        <span>
			                        	<xsl:value-of select=".//property[@name='ssylka_na_spikera']/value/item[1]/@name" />
			                        </span>
			                    </a>
		                    </div>
                        </xsl:when>
                        <xsl:when test=".//property[@name='author_info']/value">
                        	<div class="speaker">
		                        <xsl:if test=".//property[@name='author_photo']/value">
									<!--<img src="{.//property[@name='author_photo']/value}" alt="{.//property[@name='author_info']/value}"/>-->
									<xsl:call-template name="makeThumbnailFull_ByPath">
										<xsl:with-param name="source" select=".//property[@name='author_photo']/value" />
										<xsl:with-param name="width" select="35" />
										<xsl:with-param name="height" select="35" />
										<xsl:with-param name="alt" select=".//property[@name='author_info']/value" />
									</xsl:call-template>
								</xsl:if>
		                        <span><xsl:value-of select=".//property[@name='author_info']/value" /></span>
		                    </div>
                        </xsl:when>
                    </xsl:choose>

                    <!-- <xsl:if test=".//property[@name='author_info']/value">
                    	<div class="speaker">
	                        <xsl:if test=".//property[@name='author_photo']/value">
								<img src="{.//property[@name='author_photo']/value}" alt="{.//property[@name='author_info']/value}"/>
							</xsl:if>

	                        <span><xsl:value-of select=".//property[@name='author_info']/value" /></span>
	                    </div>
	                    </xsl:if> -->
                    <xsl:if test=".//property[@name='publish_time']/value/@unix-timestamp">
                    	<span class="date"><i class="fa fa-calendar" aria-hidden="true"></i>Добавлен: <xsl:value-of select="document(concat('udata://catalog/dateru/',.//property[@name='publish_time']/value/@unix-timestamp))/udata"/></span>
                    </xsl:if>
                    <div class="content_wrap">
                    	<xsl:value-of select=".//property[@name='anons']/value" disable-output-escaping="yes" />
                    </div>
                </div>
            </div>
        </li>
	</xsl:template>


	<xsl:template match="udata" mode="js_date">
        <script>
	        var settings = {
	            types: [{ className: 'css-yellow', description: "" }],
	            dates: [],
				page_id: <xsl:value-of select="$document-page-id" />
	        }
	    </script>
	</xsl:template>

<!--	<xsl:template match="udata[items/item]" mode="js_date">
        <script>
	        var settings = {
	            types: [{ className: 'css-yellow', description: "" }],
	            dates: [<xsl:apply-templates select="items/item" mode="js_date"/>]
	        }
	    </script>
	</xsl:template>

	<xsl:template match="udata/items/item" mode="js_date">
		{type: 0, date: '<xsl:value-of select="document(concat('udata://system/convertDate/', .//property[@name='publish_time']/value/@unix-timestamp, '/(j.n.Y)'))/udata" />', link: '<xsl:value-of select="document(concat('udata://data/dateFilter/',.//property[@name='publish_time']/value/@unix-timestamp))/udata" />', description: '<xsl:value-of select="text()" />' },
	</xsl:template>-->
</xsl:stylesheet>
