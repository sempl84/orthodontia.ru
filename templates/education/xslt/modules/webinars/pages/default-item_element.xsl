<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'webinars' and @method='item_element']">
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
		                <xsl:apply-templates select="user" mode="right_col_auth" />
						<xsl:apply-templates select="document('udata://menu/draw/right_menu_events')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

		<!--Content -->
	    <div class="container-fluid webinars-page">
	        <div class="page-header">
	            <div class="row">
	                <div class="col-xs-12">
	                    <i class="fa fa-play-circle" aria-hidden="true"></i>
	                    <h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
	                </div>
	            </div>
	        </div>
	        <div class="row">
	            <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
	                <div class="white-pnl">
	                	<article class="content_wrap" umi:element-id="{$document-page-id}" umi:field-name="content" umi:empty="&empty-page-content;">
							<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
	                    </article>
	                    <xsl:if test=".//property[@name = 'video']/value">
							<div class="webinar_video">
								<xsl:value-of select=".//property[@name = 'video']/value" disable-output-escaping="yes" />
							</div>
						</xsl:if>
						<hr/>

						<xsl:call-template name='social_links'>
	                    	<xsl:with-param name="class" select="'pull-right'"/>
	                    </xsl:call-template>
						<a class="link-right-arrow" href="../">Вернуться к списку вебинаров <i class="fa fa-angle-right" aria-hidden="true"></i></a>
	                </div>
	            </div>

	            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
					<xsl:apply-templates select="document('udata://menu/draw/right_menu_events')/udata" mode="right_menu" />
	                <xsl:apply-templates select="user" mode="right_col_auth" />
	                <xsl:apply-templates select="." mode="right_col" />
	            </div>
	        </div>
	    </div>
	</xsl:template>
</xsl:stylesheet>
