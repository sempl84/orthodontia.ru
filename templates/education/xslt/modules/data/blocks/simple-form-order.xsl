<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:xlink="http://www.w3.org/TR/xlink"
                exclude-result-prefixes="xsl umi xlink">

    <xsl:template match="udata[@method = 'getCreateForm' or @method = 'getEditForm']" mode="simple_form_order">
        <div class="registration_on_event_form_area">
            <xsl:apply-templates select="group" mode="simple_form_order" />
        </div>
    </xsl:template>

    <xsl:template match="group" mode="simple_form_order">
        <div>
            <xsl:if test="@name = 'form_params'">
                <h4 class="personal_data_head">Нам не хватает кое-каких данных, заполните их, пожалуйста:</h4>
            </xsl:if>
            <xsl:apply-templates select="field" mode="simple_form_order" />
        </div>
    </xsl:template>

    <xsl:template match="field" mode="simple_form_order">
        <div class="form-group" id="fblock_{@name}{/udata/@form_id}">
            <xsl:apply-templates select="." mode="simple_form_order_type" />
        </div>
    </xsl:template>


    <xsl:template match="field" mode="simple_form_order_type">
        <xsl:variable name="curr_field_name" select="@name" />
        <xsl:choose>
            <xsl:when test="document(concat('udata://users/is_empty_field/', $curr_field_name))/udata = 'true'">
                <input type="text" class="form-control" id="{@name}{/udata/@form_id}" name="{@input_name}" placeholder="{@title}" data-val="true" >
                    <xsl:attribute name="placeholder">
                        <xsl:value-of select="@title" />
                        <xsl:apply-templates select="." mode="simple_form_order_ast" />
                    </xsl:attribute>
                    <xsl:attribute name="data-val-required">true</xsl:attribute>
                    <xsl:if test="@name = 'phone'">
                        <xsl:attribute name="class">form-control js-phone</xsl:attribute>
                    </xsl:if>
<!--                    <xsl:if test="$user-info//property[contains($curr_field_name,@name)]/value">
                        <xsl:attribute name="readonly">readonly</xsl:attribute>
                    </xsl:if>-->
                </input>
            </xsl:when>
            <xsl:otherwise>
                <xsl:attribute name="style">
                    <xsl:text>display:none;</xsl:text>
                </xsl:attribute>
                <xsl:attribute name="class">
                    <xsl:text>form-group form-group-hidden</xsl:text>
                </xsl:attribute>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <xsl:template match="field[@type = 'text' or @type='wysiwyg']" mode="simple_form_order_type">
        <textarea class="form-control" id="{@name}{/udata/@form_id}" name="{@input_name}" placeholder="{@title}" data-val="true">
            <xsl:attribute name="placeholder">
                <xsl:value-of select="@title" />
                <xsl:apply-templates select="." mode="simple_form_order_ast" />
            </xsl:attribute>
            <xsl:attribute name="data-val-required">true</xsl:attribute>
        </textarea>
    </xsl:template>
    <xsl:template match="field[@type = 'text' or @type='wysiwyg'][@name = 'kommentarij']" mode="simple_form_order_type">
        <p>Ваши вопросы для консультации</p>
        <textarea class="form-control" id="{@name}{/udata/@form_id}" name="{@input_name}" placeholder="Не более 1000 символов" data-val="true" data-val-required="true" maxlength="1000"></textarea>
    </xsl:template>


    <xsl:template match="field[@type = 'boolean']" mode="simple_form_order_type">
        <xsl:attribute name="class">form-group agree</xsl:attribute>

        <input type="hidden" id="{@name}{/udata/@form_id}" name="{@input_name}" value="" />
        <input type="checkbox" class="form-control checkbox" value="0" name="{@name}{/udata/@form_id}_fake" id="{@name}{/udata/@form_id}_fake" onclick="javascript:document.getElementById('{@name}{/udata/@form_id}').value = this.checked;">
            <xsl:if test="@required = 'required' or @name='order_agree'">
                <xsl:attribute name="data-val-required">отметка обязательна</xsl:attribute>
                <xsl:attribute name="data-val">true</xsl:attribute>
            </xsl:if>
        </input>
        <span>
            <xsl:choose>
                <xsl:when test="@name='order_agree'">
					Согласен с условиями <a href='&conf_politics_url;' target='_blank'>Политики Конфиденциальности</a>, на обработку предоставленных данных ООО «Ормко», передачу данных аффилированным лицам, получение информационных рассылок и принимаю условия <a href="{$settings//property[@name = 'dogovoroferta']/value}"  target='_blank'>договора-оферты</a>.
                    <xsl:apply-templates select="." mode="simple_form_order_ast" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@title" />
                    <xsl:apply-templates select="." mode="webforms_required_ast" />
                </xsl:otherwise>
            </xsl:choose>
        </span>
        <div class="required_text" data-valmsg-for="{@name}{/udata/@form_id}_fake" data-valmsg-replace="true"></div>
    </xsl:template>


    <xsl:template match="field[@type = 'relation']" mode="simple_form_order_type">
        <xsl:variable name="curr_field_name" select="@name" />

        <xsl:choose>
            <xsl:when test="document(concat('udata://users/is_empty_field/', $curr_field_name))/udata = 'true'">
                <select id="{@name}{/udata/@form_id}" name="{@input_name}" class="form-control" data-val="true">
                    <xsl:attribute name="data-val-required">true</xsl:attribute>
                    <xsl:if test="@multiple">
                        <xsl:attribute name="multiple">
                            <xsl:text>multiple</xsl:text>
                        </xsl:attribute>
                    </xsl:if>
                    <option value="" selected="selected">
                        <xsl:value-of select="@title" />
                        <xsl:apply-templates select="." mode="simple_form_order_ast" />
                    </option>
                    <xsl:apply-templates select="values/item" mode="webforms_input_type_item" />
                </select>
            </xsl:when>
            <xsl:otherwise>
                <xsl:attribute name="style">
                    <xsl:text>display:none;</xsl:text>
                </xsl:attribute>
                <xsl:attribute name="class">
                    <xsl:text>form-group form-group-hidden</xsl:text>
                </xsl:attribute>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <xsl:template match="item" mode="webforms_input_type_item">
        <xsl:variable name="curr_field_name" select="../../@name" />

        <option value="{@id}">
            <xsl:if test="$user-info//property[contains($curr_field_name,@name)]/value/item/@id = @id">
                <xsl:attribute name="selected">selected</xsl:attribute>
            </xsl:if>
            <xsl:apply-templates />
        </option>
    </xsl:template>


    <xsl:template match="field[@type = 'file' or @type = 'img_file' or @type = 'swf_file' or @type = 'video_file']" mode="simple_form_order_type">
        <input type="file" tp='simple-form-order' class="form-control" id="{@name}{/udata/@form_id}" name="{@input_name}" placeholder="{@title}" data-val="true" >
            <xsl:attribute name="placeholder">
                <xsl:value-of select="@title" />
                <xsl:apply-templates select="." mode="simple_form_order_ast" />
            </xsl:attribute>
            <xsl:attribute name="data-val-required">true</xsl:attribute>
        </input>
        <span>
            <xsl:text> &max-file-size; </xsl:text>
            <xsl:value-of select="@maxsize" />Mb</span>
    </xsl:template>


    <!-- галочка "я ординатор" -->
    <xsl:template match="field[@type = 'boolean' and @name='ordinator']" mode="simple_form_order_type">
        <xsl:attribute name="class">form-group agree</xsl:attribute>
        <input type="hidden" id="{@name}{/udata/@form_id}" name="{@input_name}" value="" />
        <input type="checkbox" id="{@name}{/udata/@form_id}_fake" value="1" >
            <xsl:attribute name="onclick"><![CDATA[
                jQuery('#ordinator').val((jQuery(this).prop('checked')));
                if (jQuery(this).prop('checked')) {
                    $('#fblock_order_upload').fadeIn(200);
                }else{
                    $('#fblock_order_upload').fadeOut(200);
                }
            ]]></xsl:attribute>
            <xsl:if test="@required = 'required'">
                <xsl:attribute name="data-val-required">true</xsl:attribute>
            </xsl:if>
        </input>
        <span>
            <xsl:value-of select="@title" /><xsl:apply-templates select="." mode="webforms_required_ast" />
        </span>
    </xsl:template>


    <!-- галочка "я ординатор 30" -->
    <xsl:template match="field[@type = 'boolean' and @name='ordinator_30']" mode="simple_form_order_type">
        <xsl:attribute name="class">form-group agree</xsl:attribute>
        <input type="hidden" id="{@name}{/udata/@form_id}" name="{@input_name}" value="" />
        <input type="checkbox" id="{@name}{/udata/@form_id}_fake" value="1" >
            <xsl:attribute name="onclick"><![CDATA[
                jQuery('#ordinator').val((jQuery(this).prop('checked')));
                if (jQuery(this).prop('checked')) {
                    $('#fblock_order_upload').fadeIn(200);
                }else{
                    $('#fblock_order_upload').fadeOut(200);
                }
            ]]></xsl:attribute>
            <xsl:if test="@required = 'required'">
                <xsl:attribute name="data-val-required">true</xsl:attribute>
            </xsl:if>
        </input>
        <span>
            <xsl:value-of select="@title" /><xsl:apply-templates select="." mode="webforms_required_ast" />
        </span>
    </xsl:template>


    <!-- галочка "я врач 1-2 года" -->
    <xsl:template match="field[@type = 'boolean' and @name='doctor']" mode="simple_form_order_type">
        <xsl:attribute name="class">form-group agree</xsl:attribute>
        <input type="hidden" id="{@name}{/udata/@form_id}" name="{@input_name}" value="" />
        <input type="checkbox" id="{@name}{/udata/@form_id}_fake" value="1" >
            <xsl:attribute name="onclick"><![CDATA[
                jQuery('#doctor').val((jQuery(this).prop('checked')));
                if (jQuery(this).prop('checked')) {
                    $('#fblock_order_upload').fadeIn(200);
                }else{
                    $('#fblock_order_upload').fadeOut(200);
                }
            ]]></xsl:attribute>
            <xsl:if test="@required = 'required'">
                <xsl:attribute name="data-val-required">true</xsl:attribute>
            </xsl:if>
        </input>
        <span>
            <xsl:value-of select="@title" /><xsl:apply-templates select="." mode="webforms_required_ast" />
        </span>
    </xsl:template>


    <!-- галочка "я преподаватель кафедры" -->
    <xsl:template match="field[@type = 'boolean' and @name='teacher']" mode="simple_form_order_type">
        <xsl:attribute name="class">form-group agree</xsl:attribute>
        <input type="hidden" id="{@name}{/udata/@form_id}" name="{@input_name}" value="" />
        <input type="checkbox" id="{@name}{/udata/@form_id}_fake" value="1" >
            <xsl:attribute name="onclick"><![CDATA[
                jQuery('#teacher').val((jQuery(this).prop('checked')));
                if (jQuery(this).prop('checked')) {
                    $('#fblock_order_upload').fadeIn(200);
                }else{
                    $('#fblock_order_upload').fadeOut(200);
                }
            ]]></xsl:attribute>
            <xsl:if test="@required = 'required'">
                <xsl:attribute name="data-val-required">true</xsl:attribute>
            </xsl:if>
        </input>
        <span>
            <xsl:value-of select="@title" /><xsl:apply-templates select="." mode="webforms_required_ast" />
        </span>
    </xsl:template>


    <!-- поле для загрузки фотки ординатора -->
    <xsl:template match="field[(@type = 'file' or @type = 'img_file') and @name='order_upload']" mode="simple_form_order_type">
        <!-- new type of field -->
        <div class="input-group input-file" id="{@name}{/udata/@form_id}" data-name="{@input_name}">
            <input type="text" class="form-control file_load"  placeholder="{@title}" >
                <xsl:if test="@required = 'required'">
                    <xsl:attribute name="placeholder">
                        <xsl:value-of select="@title" />
                        <xsl:apply-templates select="." mode="simple_form_order_ast" />
                    </xsl:attribute>
                    <xsl:attribute name="data-val-required">true</xsl:attribute>
                </xsl:if>
            </input>
            <button class="btn btn-primary" type="button">Выбрать файл</button>
        </div>
        <span>Загрузите скан либо фото подтверждающего документа <br/><xsl:text> &max-file-size; </xsl:text><xsl:value-of select="@maxsize" />Mb</span>
    </xsl:template>

    <xsl:template match="field" mode="simple_form_order_ast">*</xsl:template>
</xsl:stylesheet>