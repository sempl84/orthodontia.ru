<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:xlink="http://www.w3.org/TR/xlink"
                exclude-result-prefixes="xsl umi xlink">

    <xsl:template match="udata[@method = 'getCreateForm' or @method = 'getEditForm']">
        <xsl:apply-templates select="group" mode="form" />
    </xsl:template>

    <xsl:template match="group" mode="form">
        <h4>
            <xsl:value-of select="@title" />
        </h4>
        <xsl:apply-templates select="field" mode="form" />
        <hr />
    </xsl:template>


    <xsl:template match="field" mode="form">
        <xsl:param name="inputName" select="current()/@input_name" />
        <div class="form-group">
            <label for="{@name}{@id}" class="col-xs-3 control-label">
                <xsl:value-of select="@title" />
                <xsl:apply-templates select="." mode="webforms_required_ast" />
            </label>
            <div class="col-xs-9">
                <xsl:apply-templates select="." mode="form_field_type">
                    <xsl:with-param name="inputName" select="$inputName" />
                </xsl:apply-templates>
            </div>
        </div>
    </xsl:template>


    <xsl:template match="field" mode="form_field_type">
        <xsl:param name="inputName" />

        <input type="text" class="form-control" id="{@name}{@id}" placeholder="{@title}" value="{.}" name="{$inputName}" data-val="true">
            <xsl:if test="@required = 'required'">
                <xsl:attribute name="placeholder">
                    <xsl:value-of select="@title" />
                    <xsl:apply-templates select="." mode="webforms_required_ast" />
                </xsl:attribute>
                <xsl:attribute name="data-val-required">true</xsl:attribute>
            </xsl:if>
            <xsl:if test="@name = 'phone'">
                <xsl:attribute name="class">form-control js-phone</xsl:attribute>
            </xsl:if>
        </input>
        <xsl:if test="@name = 'e-mail'  and /udata/@method = 'getCreateForm'">
            <span>Это будет ваш логин</span>
        </xsl:if>
    </xsl:template>

    <xsl:template match="field[@type = 'boolean']" mode="form">
        <div class="form-group agree">
            <div class="col-xs-offset-3 col-xs-9">
                <div class="checkbox">
                    <label class="show">
                        <input type="hidden" id="{@name}{/udata/@form_id}" name="{@input_name}" value="" >
                            <xsl:if test="@name='agree' and text() = '1'">
                                <xsl:attribute name="value">1</xsl:attribute>
                            </xsl:if>
                        </input>
                        <input type="checkbox" class="form-control checkbox" value="0" name="{@name}{/udata/@form_id}_fake" id="{@name}{/udata/@form_id}_fake" onclick="javascript:document.getElementById('{@name}{/udata/@form_id}').value = this.checked;">
                            <xsl:if test="@required = 'required' or @name='agree'">
                                <xsl:attribute name="data-val-required">отметка обязательна</xsl:attribute>
                                <xsl:attribute name="data-val">true</xsl:attribute>
                            </xsl:if>
                            <xsl:if test="@name='agree' and text() = '1'">
                                <xsl:attribute name="checked">checked</xsl:attribute>
                            </xsl:if>
                        </input>
                        <span>
                            <xsl:choose>
                                <xsl:when test="@name='agree'">&personal_data_agree_text;
                                    <xsl:apply-templates select="." mode="webforms_required_ast" />
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="@title" />
                                    <xsl:apply-templates select="." mode="webforms_required_ast" />
                                </xsl:otherwise>
                            </xsl:choose>
                        </span>
                        <div class="required_text" data-valmsg-for="{@name}{/udata/@form_id}_fake" data-valmsg-replace="true"></div>
                    </label>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="field[@type = 'relation']" mode="form_field_type">
        <xsl:param name="inputName" />

        <xsl:apply-templates select="." mode="simple_form_type">
            <xsl:with-param name="inputName" select="$inputName" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="field[@type = 'file' or @type = 'img_file']" mode="form_field_type">
        <input type="file" tp='common-edit-form' class="form-control" id="{@name}{/udata/@form_id}" name="{@input_name}" placeholder="{@title}" data-val="true" >
            <xsl:if test="@required = 'required'">
                <xsl:attribute name="placeholder">
                    <xsl:value-of select="@title" />
                    <xsl:apply-templates select="." mode="simple_form_order_ast" />
                </xsl:attribute>
                <xsl:attribute name="data-val-required">true</xsl:attribute>
            </xsl:if>
        </input>
        <span>
            <xsl:text> &max-file-size; </xsl:text>
            <xsl:value-of select="@maxsize" />Mb
        </span>
    </xsl:template>
</xsl:stylesheet>
