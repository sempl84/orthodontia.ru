<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"

				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:umi="http://www.umi-cms.ru/TR/umi"
				xmlns:xlink="http://www.w3.org/TR/xlink"
	exclude-result-prefixes="xsl umi xlink">

	<xsl:template match="udata[@method = 'getCreateForm' or @method = 'getEditForm']" mode="simple_form">
		<xsl:apply-templates select="group" mode="simple_form" />
	</xsl:template>

	<xsl:template match="group" mode="simple_form">
		<xsl:apply-templates select="field" mode="simple_form" />
	</xsl:template>


	<xsl:template match="field" mode="simple_form">
		<div class="form-group fn_block_{@name}">
			<xsl:apply-templates select="." mode="simple_form_type" />
        </div>
	</xsl:template>

	<xsl:template match="field" mode="simple_form_type">
		<input type="text" class="form-control" id="{@name}{/udata/@form_id}" name="{@input_name}" placeholder="{@title}" data-val="true" >
			<xsl:if test="@required = 'required'">
				<xsl:attribute name="placeholder">
					<xsl:value-of select="@title" /><xsl:apply-templates select="." mode="webforms_required_ast" />
				</xsl:attribute>
				<xsl:attribute name="data-val-required">true</xsl:attribute>
			</xsl:if>
			<xsl:if test="@name = 'phone'">
				<xsl:attribute name="class">form-control js-phone</xsl:attribute>
				<xsl:attribute name="data-val-length">true</xsl:attribute>
				<xsl:attribute name="data-val-length-min">16</xsl:attribute>
			</xsl:if>
			<xsl:if test="@name = 'email' or @name='e-mail'">
				<xsl:attribute name="data-val-email">true</xsl:attribute>
			</xsl:if>
		</input>
		<xsl:if test="@name = 'e-mail'  and /udata/@method = 'getCreateForm'">
			<span>Это будет ваш логин</span>
		</xsl:if>
	</xsl:template>

	<xsl:template match="field[@type = 'text' or @type='wysiwyg']" mode="simple_form_type">
		<textarea class="form-control" id="{@name}{/udata/@form_id}" name="{@input_name}" placeholder="{@title}" data-val="true">
			<xsl:if test="@required = 'required'">
				<xsl:attribute name="placeholder">
					<xsl:value-of select="@title" /><xsl:apply-templates select="." mode="webforms_required_ast" />
				</xsl:attribute>
				<xsl:attribute name="data-val-required">true</xsl:attribute>
			</xsl:if>
		</textarea>
	</xsl:template>

	<xsl:template match="field[@type = 'boolean']" mode="simple_form_type">
		<xsl:attribute name="class">form-group agree</xsl:attribute>
		<input type="hidden" id="{@name}{/udata/@form_id}" name="{@input_name}" value="0" >
			<xsl:if test="@name='agree' and text() = '1'">
				<xsl:attribute name="value">1</xsl:attribute>
			</xsl:if>
		</input>
        <input type="checkbox" id="{@name}{/udata/@form_id}_fake" value="0" name="{@name}{/udata/@form_id}_fake" onclick="javascript:document.getElementById('{@name}{/udata/@form_id}').value = this.checked;">
        	<xsl:if test="@required = 'required' or @name='agree'">
				<xsl:attribute name="data-val-required">отметка обязательна</xsl:attribute>
				<xsl:attribute name="data-val">true</xsl:attribute>
			</xsl:if>
			<xsl:if test="@name='agree' and text() = '1'">
				<xsl:attribute name="checked">checked</xsl:attribute>
			</xsl:if>
        </input>
        <span>
        	<xsl:choose>
				<xsl:when test="@name='agree'">&personal_data_agree_text;</xsl:when>
				<xsl:otherwise><xsl:value-of select="@title" /></xsl:otherwise>
			</xsl:choose>
        	<xsl:apply-templates select="." mode="webforms_required_ast" />
        </span>
        <div class="required_text" data-valmsg-for="{@name}{/udata/@form_id}_fake" data-valmsg-replace="true"></div>
	</xsl:template>


	<xsl:template match="field[@type = 'relation']" mode="simple_form_type">
		<xsl:param name="inputName" select="current()/@input_name" />

		<select id="{@name}{/udata/@form_id}" name="{$inputName}" class="form-control {@name}_field" data-val="true">
            <xsl:if test="@required = 'required'">
				<xsl:attribute name="data-val-required">true</xsl:attribute>
			</xsl:if>
            <xsl:if test="@multiple">
				<xsl:attribute name="multiple">
					<xsl:text>multiple</xsl:text>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@name = 'vuz'">
				<xsl:attribute name="data-size">8</xsl:attribute>
			</xsl:if>
			<option value="" selected="selected"  disabled="disabled">
				<xsl:value-of select="@title" /><xsl:apply-templates select="." mode="webforms_required_ast" />
			</option>
			<!-- выключаем регион с id 9990 где в строчку перечисленны все регионы -->
			<!-- <xsl:apply-templates select="values/item[not(@id = 9990 or @id = 9988 or @id = 9989)]" mode="webforms_input_type" /> -->
			<xsl:choose>
				<xsl:when test="@name = 'country'">
					<xsl:apply-templates select="values/item[@id != 12122]" mode="webforms_input_type_country" />
					<xsl:apply-templates select="values/item[@id = 12122]" mode="webforms_input_type_country" /> <!-- Другие - в конце -->
				</xsl:when>
				<xsl:when test="@name = 'vuz'">
					<xsl:apply-templates select="values" mode="webforms_input_type_vuz" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="values/item" mode="webforms_input_type" />
				</xsl:otherwise>
			</xsl:choose>
        </select>
	</xsl:template>

	<xsl:template match="item" mode="webforms_input_type_country">
		<xsl:variable name="item_params" select="document(concat('uobject://',@id))/udata" />
		<xsl:variable name="mask" select="$item_params//property[@name='mask']/value" />
		<xsl:variable name="placeholder" select="$item_params//property[@name='placeholder']/value" />

		<option value="{@id}">
			<xsl:if test="@selected">
        		<xsl:attribute name="selected">selected</xsl:attribute>
        	</xsl:if>
			<!-- <xsl:if test="position()=1">
				<xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if> -->
			<xsl:if test="$mask">
				<xsl:attribute name="data-mask"><xsl:value-of select="$mask" disable-output-escaping="yes" /></xsl:attribute>
			</xsl:if>
			<xsl:if test="$placeholder">
				<xsl:attribute name="data-mask-placeholder"><xsl:value-of select="$placeholder" disable-output-escaping="yes" /></xsl:attribute>
			</xsl:if>
			<xsl:apply-templates />
		</option>
	</xsl:template>


	<xsl:template match="values" mode="webforms_input_type_vuz">
		<xsl:variable name="values" select="document('udata://data/get_all_vuz_by_region')" />
		<xsl:apply-templates select="$values/udata/item" mode="webforms_input_type_vuz_region" />
	</xsl:template>

	<xsl:template match="item" mode="webforms_input_type_vuz_region">
		<xsl:apply-templates select="." mode="webforms_input_type_vuz_one" />
	</xsl:template>
	<xsl:template match="item[item]" mode="webforms_input_type_vuz_region">
		<optgroup label="Регион: {@name}">
			<xsl:apply-templates select="./item" mode="webforms_input_type_vuz_one" />
		</optgroup>
	</xsl:template>

	<xsl:template match="item" mode="webforms_input_type_vuz_one">
		<option value="{@id}">
			<xsl:value-of select="text()" />
		</option>
	</xsl:template>


	<xsl:template match="field[@type = 'file' or @type = 'img_file' or @type = 'swf_file' or @type = 'video_file']" mode="simple_form_type">
		<div class="file-upload js-file-upload">
			<input type="button" class="btn btn-primary" value="Выбрать" />
            <span></span>
			<input type="file" tp='simple_form_type' class="form-control" id="{@name}{/udata/@form_id}" name="{@input_name}" placeholder="{@title}" data-val="true" >
				<xsl:if test="@required = 'required'">
					<xsl:attribute name="placeholder">
						<xsl:value-of select="@title" /><xsl:apply-templates select="." mode="simple_form_order_ast" />
					</xsl:attribute>
					<xsl:attribute name="data-val-required">true</xsl:attribute>
				</xsl:if>
			</input>
		</div>
		<span><xsl:value-of select="@title" /><xsl:apply-templates select="." mode="simple_form_order_ast" />
		<!-- <br/>
		<xsl:text> &max-file-size; </xsl:text><xsl:value-of select="@maxsize" />Mb -->
		</span>
	</xsl:template>

	<xsl:template match="field[@type = 'file' or @type = 'img_file' or @type = 'swf_file' or @type = 'video_file']" mode="simple_form_typeTODEL">
		<input type="file" tp='simple_form_type' class="form-control" id="{@name}{/udata/@form_id}" name="{@input_name}" placeholder="{@title}" data-val="true" >
			<xsl:if test="@required = 'required'">
				<xsl:attribute name="placeholder">
					<xsl:value-of select="@title" /><xsl:apply-templates select="." mode="simple_form_order_ast" />
				</xsl:attribute>
				<xsl:attribute name="data-val-required">true</xsl:attribute>
			</xsl:if>
		</input>
		<span><xsl:value-of select="@title" />
		<!-- <br/>
		<xsl:text> &max-file-size; </xsl:text><xsl:value-of select="@maxsize" />Mb -->
		</span>
	</xsl:template>
</xsl:stylesheet>