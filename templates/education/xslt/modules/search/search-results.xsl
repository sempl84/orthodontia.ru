<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">
	<xsl:template match="/result[@method = 'search_do']">
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
		                <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

		<!--Content -->
	    <div class="container-fluid news-page">
	        <div class="row">
	            <div class="col-xs-12">

	                <h2><i class="fa fa-search" aria-hidden="true"></i><xsl:value-of select="@header" disable-output-escaping="yes" /></h2>
	            </div>
	            <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
	                <div class="white-pnl">
	                    <xsl:apply-templates select="document('udata://search/search_do')" />

	                </div>
	            </div>

	            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
	                <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
	                <xsl:apply-templates select="." mode="right_col" />
	            </div>
	        </div>
	    </div>
	</xsl:template>

	<xsl:template match="udata[@method = 'search_do']">
		<p>
			<strong>
				<xsl:text>&search-founded-left; "</xsl:text>
				<xsl:value-of select="$search_string" />
				<xsl:text>" &search-founded-nothing;.</xsl:text>
			</strong>
		</p>
	</xsl:template>

	<xsl:template match="udata[@method = 'search_do' and count(items/item)]">
		<p>
			<strong>
				<xsl:text>&search-founded-left; "</xsl:text>
				<xsl:value-of select="$search_string" />
				<xsl:text>" &search-founded-right;: </xsl:text>
				<xsl:value-of select="total" />
				<xsl:text>.</xsl:text>
			</strong>
		</p>

		<dl class="search">
			<xsl:apply-templates select="items/item" mode="search-result" />
		</dl>
		<xsl:apply-templates select="total" />
	</xsl:template>

	<xsl:template match="item" mode="search-result">
		<div>
			<span>
				<xsl:value-of select="$p*10 + position()" />.&#160;
			</span>
			<a href="{@link}" umi:element-id="{@id}" umi:field-name="name">
				<xsl:value-of select="@name" />
			</a>
		</div>
		<div class="content_wrap">
			<xsl:value-of select="." disable-output-escaping="yes" />
		</div>
	</xsl:template>
</xsl:stylesheet>