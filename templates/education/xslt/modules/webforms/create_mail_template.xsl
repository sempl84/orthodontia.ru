<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/" >
     <xsl:apply-templates select="udata/groups/group" />
</xsl:template>

<xsl:template match="group" >
     <div><xsl:value-of select="@title" /></div>
     <div>
          <xsl:apply-templates select="field" />
     </div>
     <br/>
</xsl:template>

<xsl:template match="field" >
      <xsl:value-of select="@title" /><xsl:text>:</xsl:text><strong>%<xsl:value-of select="@name" />%</strong><br/>
</xsl:template>

</xsl:stylesheet>