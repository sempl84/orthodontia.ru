<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:date="http://exslt.org/dates-and-times"
                xmlns:udt="http://umi-cms.ru/2007/UData/templates"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                exclude-result-prefixes="xsl date udt xlink">

    <xsl:template match="udata[@module = 'webforms' and @method = 'add']" mode="feedback_form">
        <xsl:param name="submit_text" select="'Отправить'" />

		<xsl:variable name="title" select="document(concat('utype://',/udata/@form_id))/udata/type/@title" />

        <form method="post" action="{$lang-prefix}/webforms/send_form/" class="ajaxSend" enctype="multipart/form-data" data-gtm-event-form="{$title}">
            <xsl:apply-templates select="items" mode="address" />
            <input type="hidden" name="system_form_id" value="{/udata/@form_id}" />
            <input type="hidden" name="ref_onsuccess" value="{$lang-prefix}/webforms/posted/{/udata/@form_id}/" />

            <xsl:if test="/udata/@form_id = &course_form_oid;">
				<input type="hidden" id="from_course" name="data[new][from_course]" value="не найдено" />
				<input type="hidden" id="from_course_name" name="data[new][from_course_name]" value="не найдено" />
            </xsl:if>

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
                <h4 class="modal-title" id="gridRegister">
                    <xsl:value-of select="$title" />
                </h4>
            </div>
            <div class="modal-body">
				<!--Поле для защиты от ботов-->
				<div class="form-group hid">
					<input type="text" class="form-control" name="data[new][phone1]" placeholder="Телефон 1"  />
				</div>
                <!-- fields -->
                <xsl:apply-templates select=".//field" mode="feedback_form" />

                <!-- captcha -->
                <xsl:apply-templates select="document('udata://system/captcha/')/udata" />
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary pull-left">
                    <xsl:value-of select="$submit_text" />
                </button>
            </div>
        </form>
    </xsl:template>

	<xsl:template match="field" mode="feedback_form">
		<div class="form-group">
			<xsl:apply-templates select="." mode="webforms_input_type" />
		</div>
	</xsl:template>
	<xsl:template match="field[@name = 'nazvanie_kliniki' or @name = 'adres_kliniki']" mode="feedback_form">
		<div class="form-group doctor_only">
			<xsl:apply-templates select="." mode="webforms_input_type" />
		</div>
	</xsl:template>
	<xsl:template match="field[@name = 'stranica_otpravki_formy']" mode="feedback_form">
		<input type="hidden" name="{@input_name}" value="{$request-uri}" />
	</xsl:template>



    <xsl:template match="udata[@module = 'webforms' and @method = 'add']" mode="feedback_form_contacts">
        <xsl:param name="submit_text" select="'Отправить'" />

        <form method="post" action="{$lang-prefix}/webforms/send_form/" class="contactsForm ajaxSend form-horizontal" enctype="multipart/form-data" data-gtm-event-form="Обратная связь">
            <xsl:apply-templates select="items" mode="address" />
            <input type="hidden" name="system_form_id" value="{/udata/@form_id}" />
            <input type="hidden" name="ref_onsuccess" value="{$lang-prefix}/webforms/posted/{/udata/@form_id}/" />

			<!--Поле для защиты от ботов-->
			<div class="form-group hid">
				<input type="text" class="form-control" name="data[new][phone1]" placeholder="Телефон 1"  />
			</div>

			<!-- fields -->
			<xsl:apply-templates select=".//field" mode="feedback_form_contacts" />

			<!-- captcha -->
			<xsl:apply-templates select="document('udata://system/captcha/')/udata" />

			<div class="form-group">
				<div class="col-xs-offset-3 col-xs-9">
					<button type="submit" class="btn btn-primary">Отправить</button>
				</div>
			</div>
        </form>
    </xsl:template>

	<xsl:template match="field" mode="feedback_form_contacts">
		<div class="form-group">
			<label for="inputName" class="col-xs-3 control-label">
				<xsl:value-of select="@title" />
				<xsl:if test="@required = 'required'">*</xsl:if>
			</label>
			<div class="col-xs-9">
				<xsl:apply-templates select="." mode="webforms_input_type">
					<xsl:with-param name="page" select="'_contacts_'" />
				</xsl:apply-templates>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="field[@type = 'boolean']" mode="feedback_form_contacts">
		<div class="col-xs-offset-3 col-xs-9">
			<div class="form-group">
				<xsl:apply-templates select="." mode="webforms_input_type">
					<xsl:with-param name="page" select="'_contacts_'" />
				</xsl:apply-templates>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="field[@name = 'nazvanie_kliniki' or @name = 'adres_kliniki']" mode="feedback_form_contacts">
		<div class="form-group doctor_only">
			<label for="inputName" class="col-xs-3 control-label">
				<xsl:value-of select="@title" />
				<xsl:if test="@required = 'required'">*</xsl:if>
			</label>
			<div class="col-xs-9">
				<xsl:apply-templates select="." mode="webforms_input_type">
					<xsl:with-param name="page" select="'_contacts_'" />
				</xsl:apply-templates>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="field[@name = 'stranica_otpravki_formy']" mode="feedback_form_contacts">
		<input type="hidden" name="{@input_name}" value="{$request-uri}" />
	</xsl:template>
</xsl:stylesheet>