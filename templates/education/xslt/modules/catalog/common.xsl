<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">
	<xsl:include href="pages/events-list.xsl" />
	<xsl:include href="pages/event.xsl" />
	<xsl:include href="pages/club-ormco.xsl" />
	<xsl:include href="pages/my_events.xsl" />
	<xsl:include href="pages/my_archive_events.xsl" />
	<xsl:include href="pages/events_more.xsl" />
	<xsl:include href="pages/consultation.xsl" />

	<xsl:include href="pages/spec_dealers.xsl" />
	<xsl:include href="pages/spec_dealers_speakers.xsl" />
	<xsl:include href="pages/spec_dealers_programs.xsl" />
	<xsl:include href="pages/spec_dealers_speaker_item.xsl" />
	<xsl:include href="pages/spec_dealers_program_item.xsl" />

	<xsl:include href="blocks/smart-filters.xsl" />
	<xsl:include href="blocks/search-filter.xsl" />
	<xsl:include href="blocks/calendar.xsl" />
	<xsl:include href="blocks/calendar_club_ormco.xsl" />
	<xsl:include href="blocks/short_event_block.xsl" />

	<!-- шаблон, который по id типа мероприятия возвращает css класс -->
	<xsl:template name="add_event_css_class" >
		<xsl:param name="old_class" select="''" />
		<xsl:param name="event_type_id" />

		<xsl:variable name="event_css_class">
			<xsl:choose>
				<xsl:when test="$event_type_id = 1278"> <!-- Семинар Ormco -->
					<xsl:text>css-aqua</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1279"> <!-- Вебинар -->
					<xsl:text>css-yellow</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1280"> <!-- Семинар Партнера -->
					<xsl:text>css-blue</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1281"> <!-- Семинар ШОО -->
					<xsl:text>css-green</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1282"> <!-- Конференция -->
					<xsl:text>css-pink</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1971"> <!-- Клуб Ormco -->
					<xsl:text>css-aqua</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1901823"> <!-- Онлайн курс -->
					<xsl:text>css-yellowbr</xsl:text>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:attribute name="class"><xsl:value-of select="concat($old_class,' ',$event_css_class)" /></xsl:attribute>
	</xsl:template>

	<!-- шаблон, который по id типа мероприятия возвращает id js type (для формирования календаря) -->
	<xsl:template name="event_calendar_type" >
		<xsl:param name="event_type_id" />

		<xsl:variable name="event_calendar_type">
			<xsl:choose>
				<xsl:when test="$event_type_id = 1278"> <!-- Семинар Ormco -->
					<xsl:text>0</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1279"> <!-- Вебинар -->
					<xsl:text>1</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1280"> <!-- Семинар Партнера -->
					<xsl:text>2</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1281"> <!-- Семинар ШОО -->
					<xsl:text>3</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1282"> <!-- Конференция -->
					<xsl:text>4</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1971"> <!-- Клуб Ormco -->
					<xsl:text>5</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1901823"> <!-- Онлайн курс -->
					<xsl:text>6</xsl:text>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:value-of select="$event_calendar_type" />
	</xsl:template>

	<xsl:template name="feedback_event_class" >
		<xsl:param name="old_class" select="''" />
		<xsl:param name="event_type_id" />

		<xsl:variable name="event_css_class">
			<xsl:choose>
				<xsl:when test="$event_type_id = 1278"> <!-- Семинар Ormco -->
					<xsl:text>seminar-type</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1279"> <!-- Вебинар -->
					<xsl:text>webinar-type</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1280"> <!-- Семинар Партнера -->
					<xsl:text>seminar-partner-type</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1281"> <!-- Семинар ШОО -->
					<xsl:text>seminar-woo-type</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1282"> <!-- Конференция -->
					<xsl:text>conference-type</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1971"> <!-- Клуб Ormco -->
					<xsl:text>seminar-type</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1901823"> <!-- Онлайн курс -->
					<xsl:text>online-type</xsl:text>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:attribute name="class"><xsl:value-of select="concat($old_class,' ',$event_css_class)" /></xsl:attribute>
	</xsl:template>

	<!-- шаблон, который по id типа мероприятия возвращает css класс для кратких карточек мероприятия (как на главной) -->
	<xsl:template name="add_event_css_class_short" >
		<xsl:param name="old_class" select="''" />
		<xsl:param name="event_type_id" />

		<xsl:variable name="event_css_class">
			<xsl:choose>
				<xsl:when test="$event_type_id = 1278"> <!-- Семинар Ormco -->
					<xsl:text>aqua</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1279"> <!-- Вебинар -->
					<xsl:text>yellow</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1280"> <!-- Семинар Партнера -->
					<xsl:text>blue</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1281"> <!-- Семинар ШОО -->
					<xsl:text>green</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1282"> <!-- Конференция -->
					<xsl:text>pink</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1971"> <!-- Клуб Ormco -->
					<xsl:text>aqua</xsl:text>
				</xsl:when>
				<xsl:when test="$event_type_id = 1901823"> <!-- Онлайн курс -->
					<xsl:text>yellowbr</xsl:text>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:attribute name="class"><xsl:value-of select="concat($old_class,' ',$event_css_class)" /></xsl:attribute>
	</xsl:template>
</xsl:stylesheet>