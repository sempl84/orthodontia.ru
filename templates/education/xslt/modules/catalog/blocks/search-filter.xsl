<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="udata[@method = 'search'][group]">
		<xsl:param name="all_webinars" />
        <hr />
        <div class="top-selectors">
        	<form class="clearfix">
        		<xsl:apply-templates select=".//field[@data-type = 'relation' or @data-type = 'date']" mode="search">
        			<xsl:with-param name="all_webinars" select="$all_webinars" />
        		</xsl:apply-templates>
        		<a href="#" class="reset_filter" onclick="document.location.href = '?'"><i class="fa fa-times" aria-hidden="true"></i>&reset;</a>
            </form>
        </div>
	</xsl:template>

	<xsl:template match="field[@data-type = 'date']" mode="search">
		<!-- <input type="text" id="filterByDate" name="FilterByDate" class="hidden filter-by-date" /> -->
		<input type="text" id="filterByDateWithDate" name="FilterByDate" class="hidden filter-by-date" />
        <div class="btn-group bootstrap-select show-tick">
            <button type="button" class="btn dropdown-toggle btn-default js-show-calendar" data-toggle="dropdown" title="Дата добавления">
                <span class="filter-option pull-left"><xsl:value-of select="@title" /></span>&nbsp;<span class="bs-caret"><span class="caret"></span></span>
            </button>
        </div>
	</xsl:template>

	<xsl:template match="field[@data-type = 'relation' or @data-type = 'symlink']" mode="search">
		<select id="{@name}" name="fields_filter[{@name}]" title="{@title}" class="selectpicker show-tick ">
            <option value="">Все</option>
			<xsl:apply-templates select="values/item" mode="search" />
        </select>
	</xsl:template>

	<xsl:template match="field[@data-type = 'relation' and @name='ssylka_na_spikera']" mode="search">
		<xsl:param name="all_webinars" />
		<select id="{@name}" name="fields_filter[{@name}]" title="{@title}" class="selectpicker show-tick ">
            <option value="">Все</option>
			<xsl:apply-templates select="values/item[text() = $all_webinars//property[@name='ssylka_na_spikera']/value/item/@name]" mode="search">
				 <xsl:sort select="text()"/>
			</xsl:apply-templates>
        </select>
	</xsl:template>

	<xsl:template match="field/values/item" mode="search">
		<option value="{@id}">
			<xsl:copy-of select="@selected" />
			<xsl:value-of select="text()" />
		</option>
	</xsl:template>
	<xsl:template match="field/values/item[@id = '7695']" mode="search">
		<!--Показ только дилерам-->
		<xsl:if test="$user-info//property[@name = 'groups']//value/item/@id = '7694'">
			<option value="{@id}">
				<xsl:copy-of select="@selected" />
				<xsl:value-of select="text()" />
			</option>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>