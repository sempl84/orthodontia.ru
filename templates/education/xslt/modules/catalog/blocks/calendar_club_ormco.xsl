<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template name="calendar_club_ormco">
		<xsl:param name="parent_id" select="$document-page-id" />

		<div class="events-calendar blue-skin js-calendar"></div>


        <!-- вывод js данных для календаря -->
	    <!-- TODO выводить актуальные или архивные мероприятия -->
	    <xsl:apply-templates select="document(concat('udata://catalog/getObjectsList//', $parent_id, '/1000/1/20/?extProps=publish_date,event_type'))/udata" mode="calendar_club_ormco_js_data"/>

	</xsl:template>

	<!-- вывод js данных для календаря -->
	<xsl:template match="udata[@module='catalog' and @method='getObjectsList']" mode="calendar_club_ormco_js_data">
		<script>
	        var all_event_url = '&all_event_url;';
	        <![CDATA[
	        var settings = {
	            types: [{ className: 'css-aqua', link: all_event_url, description: "Клуб Ormco" }],
	            dates: []
	        }
	        ]]>
	    </script>
	</xsl:template>
	<xsl:template match="udata[@module='catalog' and @method='getObjectsList' and lines/item]" mode="calendar_club_ormco_js_data">
		<script>
	        var all_event_url = '&ormco_club_url;';
	        <![CDATA[
	        var settings = {
	            types: [{ className: 'css-aqua', link: all_event_url, description: "Клуб Ormco" }],
	            dates: [
	        ]]>
	        	<xsl:apply-templates select="lines/item" mode="calendar_club_ormco_js_data"/>
	        <![CDATA[
	            ]
	        }
	        ]]>
	    </script>
	</xsl:template>

		<xsl:template match="udata[@module='catalog' and @method='getObjectsList']/lines/item" mode="calendar_club_ormco_js_data">
			{ 	type: 0,
				date: '<xsl:value-of select="document(concat('udata://system/convertDate/', .//property[@name='publish_date']/value/@unix-timestamp ,'/(d.n.Y)'))/udata" />',
				link: '<xsl:value-of select="@link" />',
				description: '<xsl:value-of select="text()" />' },
		</xsl:template>


</xsl:stylesheet>