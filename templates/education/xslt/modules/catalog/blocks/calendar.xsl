<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template name="calendar">
        <!--<xsl:param name="parent_id" select="$document-page-id" />-->
		<!-- Все мероприятия вместо прежнего какого-то родителя -->
		<xsl:variable name="parent_id" select="&all_event_pid;" />

        <div class="events-calendar blue-skin js-calendar"></div>
        <!-- вывод js данных для календаря -->
        <!-- TODO выводить актуальные или архивные мероприятия -->
<!--        <xsl:apply-templates select="document(concat('udata://catalog/getObjectsListWithCache//', $parent_id, '/1000/1/20/?t=1&amp;extProps=publish_date,finish_date,event_type'))/udata" mode="calendar_js_data">
            <xsl:with-param name="parent_link" select="document(concat('upage://', $parent_id))/udata/page/@link" />
        </xsl:apply-templates>-->

		<script>
            var all_event_url = '&all_event_url;';
	        <![CDATA[
	        var settings = {
	            types: [{ className: 'css-aqua', link: all_event_url + '?filter[event_type]=Семинар+Ormco', description: "Семинар Ormco" },
	                { className: 'css-yellow', link: all_event_url + '?filter[event_type]=Вебинар', description: "Вебинар" },
	                { className: 'css-blue', link: all_event_url + '?filter[event_type]=Семинар+партнера', description: "Семинар партнера" },
	                { className: 'css-green', link: all_event_url + '?filter[event_type]=Damon+клуб', description: "Damon клуб" },
	                { className: 'css-pink', link: all_event_url + '?filter[event_type]=Конференция', description: "Конференция" },
	                { className: 'css-aqua', link: all_event_url + '?filter[event_type]=Клуб Ormco', description: "Клуб Ormco" },
	                { className: 'css-yellowbr', link: all_event_url + '?filter[event_type]=Онлайн курс', description: "Онлайн курс" }],
	            dates: []
	        }
	        ]]>
        </script>
    </xsl:template>

    <!-- вывод js данных для календаря -->
<!--    <xsl:template match="udata[@module='catalog' and (@method='getObjectsList' or @method='getObjectsListWithCache')]" mode="calendar_js_data">
        <script>
            var all_event_url = '&all_event_url;';
	        <![CDATA[
	        var settings = {
	            types: [{ className: 'css-aqua', link: all_event_url + '?filter[event_type]=Семинар+Ormco', description: "Семинар Ormco" },
	                { className: 'css-yellow', link: all_event_url + '?filter[event_type]=Вебинар', description: "Вебинар" },
	                { className: 'css-blue', link: all_event_url + '?filter[event_type]=Семинар+партнера', description: "Семинар партнера" },
	                { className: 'css-green', link: all_event_url + '?filter[event_type]=Damon+клуб', description: "Damon клуб" },
	                { className: 'css-pink', link: all_event_url + '?filter[event_type]=Конференция', description: "Конференция" },
	                { className: 'css-aqua', link: all_event_url + '?filter[event_type]=Клуб Ormco', description: "Клуб Ormco" },
	                { className: 'css-yellowbr', link: all_event_url + '?filter[event_type]=Онлайн курс', description: "Онлайн курс" }],
	            dates: []
	        }
	        ]]>
        </script>
    </xsl:template>-->

<!--    <xsl:template match="udata[@module='catalog' and (@method='getObjectsList' or @method='getObjectsListWithCache') and lines/item]" mode="calendar_js_data">
        <xsl:param name="parent_link" select="'/events/'" />

        <script>
            var all_event_url = '&all_event_url;';
	        <![CDATA[
	        var settings = {
	            types: [{ className: 'css-aqua', link: all_event_url + '?filter[event_type]=Семинар+Ormco', description: "Семинар Ormco" },
	                { className: 'css-yellow', link: all_event_url + '?filter[event_type]=Вебинар', description: "Вебинар" },
	                { className: 'css-blue', link: all_event_url + '?filter[event_type]=Семинар+партнера', description: "Семинар партнера" },
	                { className: 'css-green', link: all_event_url + '?filter[event_type]=Damon+клуб', description: "Damon клуб" },
	                { className: 'css-pink', link: all_event_url + '?filter[event_type]=Конференция', description: "Конференция" },
	                { className: 'css-aqua', link: all_event_url + '?filter[event_type]=Клуб Ormco', description: "Клуб Ormco" },
	                { className: 'css-yellowbr', link: all_event_url + '?filter[event_type]=Онлайн курс', description: "Онлайн курс" }],
	            dates: [
	        ]]>
            <xsl:apply-templates select="lines/item" mode="calendar_js_data">
                <xsl:with-param name="parent_link" select="$parent_link" />
            </xsl:apply-templates>

            <xsl:apply-templates select="document(concat('udata://catalog/getObjectsListWithCache//', &all_event_ya_ortodont_pid;, '/1000/1/20/?t=1&amp;extProps=publish_date,finish_date,event_type'))/udata/lines/item" mode="calendar_js_data">
                <xsl:with-param name="parent_link" select="document(concat('upage://',&all_event_ya_ortodont_pid;))/udata/page/@link" />
            </xsl:apply-templates>

            <xsl:apply-templates select="document(concat('udata://catalog/getObjectsListWithCache//', &all_event_archive_pid;, '/1000/1/20/?t=1&amp;extProps=publish_date,finish_date,event_type'))/udata/lines/item" mode="calendar_js_data">
                <xsl:with-param name="parent_link" select="document(concat('upage://',&all_event_archive_pid;))/udata/page/@link" />
            </xsl:apply-templates>
	        <![CDATA[
	            ]
	        }
	        ]]>
        </script>
    </xsl:template>-->

<!--    <xsl:template match="udata[@module='catalog' and (@method='getObjectsList' or @method='getObjectsListWithCache')]/lines/item" mode="calendar_js_data">
        <xsl:param name="parent_link"  />
        <xsl:variable name="event_calendar_type" >
            <xsl:call-template name="event_calendar_type">
                <xsl:with-param name="event_type_id" select=".//property[@name='event_type']/value/item/@id"/>
            </xsl:call-template>
        </xsl:variable>
        {
            type: <xsl:value-of select="$event_calendar_type" />,
            date: '<xsl:value-of select="document(concat('udata://system/convertDate/', .//property[@name='publish_date']/value/@unix-timestamp ,'/(j.n.Y)'))/udata" />',
            link: '<xsl:value-of select="$parent_link" /><xsl:value-of select="document(concat('udata://data/dateFilterSmart/',.//property[@name='publish_date']/value/@unix-timestamp))/udata" disable-output-escaping="yes" />',
            description: '<xsl:value-of select="text()" />',
            <xsl:if test=".//property[@name='finish_date']/value/@unix-timestamp &gt; .//property[@name='publish_date']/value/@unix-timestamp">periodcss: 'calendar_start_date'</xsl:if>
        },
        <xsl:call-template name="more_days">
            <xsl:with-param name="start_time" select=".//property[@name='publish_date']/value/@unix-timestamp" />
            <xsl:with-param name="finish_time" select=".//property[@name='finish_date']/value/@unix-timestamp" />
            <xsl:with-param name="event_calendar_type" select="$event_calendar_type" />
            <xsl:with-param name="this" select="." />
            <xsl:with-param name="last" select="'yes'" />
            <xsl:with-param name="parent_link" select="$parent_link" />
        </xsl:call-template>
    </xsl:template>-->

<!--    <xsl:template name="more_days">
        <xsl:param name="parent_link" select="'/events/'" />
        <xsl:param name="start_time" />
        <xsl:param name="finish_time" />
        <xsl:param name="event_calendar_type" />
        <xsl:param name="this" />
        <xsl:param name="last" select="'no'" />

        <xsl:if test="$finish_time &gt; $start_time">
            {
                type: <xsl:value-of select="$event_calendar_type" />,
                date: '<xsl:value-of select="document(concat('udata://system/convertDate/', $finish_time ,'/(j.n.Y)'))/udata" />',
                link: '<xsl:value-of select="$parent_link" /><xsl:value-of select="document(concat('udata://data/dateFilterSmart/',$finish_time))/udata" disable-output-escaping="yes" />',
                 link: '<xsl:value-of select="$this/@link" />',
                description: '<xsl:value-of select="$this/text()" />',
                <xsl:if test="$last = 'yes'">periodcss: 'calendar_finish_date'</xsl:if>
                <xsl:if test="$last = 'no'">periodcss: 'calendar_middle_date'</xsl:if>
            },
            <xsl:call-template name="more_days">
                <xsl:with-param name="start_time" select="$start_time" />
                <xsl:with-param name="finish_time" select="$finish_time - 86400" />
                <xsl:with-param name="event_calendar_type" select="$event_calendar_type" />
                <xsl:with-param name="this" select="$this" />
	            <xsl:with-param name="parent_link" select="$parent_link" />
            </xsl:call-template>
        </xsl:if>
    </xsl:template>-->
</xsl:stylesheet>
