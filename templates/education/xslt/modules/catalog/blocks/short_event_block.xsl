<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi">

    <!-- пока нигде не используется -->
    <!-- <xsl:template match="item" mode="short_event_block_3x">

        <li class="col-md-4 col-sm-6 col-xs-6">
            <div class="thumbnail yellow">
                <span class="level">продвинутый уровень</span>
                <ul class="stars">
                    <li><i class="fa fa-graduation-cap" aria-hidden="true"></i></li>
                    <li><i class="fa fa-graduation-cap" aria-hidden="true"></i></li>
                    <li><i class="fa fa-graduation-cap" aria-hidden="true"></i></li>
                </ul>
                <span class="date">21 июня – 25 сентября</span>
                <span class="location">Семинар Ormco, Санкт-Петербург</span>
                <div class="topic-wrapper">
                    <div class="topic">
                        <a href="event.html">Современные брекет-системы: материалы и стоимость</a>
                    </div>
                </div>
                <div class="author">
                    <img src="{$template-resources}img/videos/author-1.png" alt="author" />
                    <span>Констанинский Дмитрий</span>
                </div>
                <div class="price-border">
                    <span class="price">15 000 р.</span>
                </div>
                <a href="javascript:void(0)" class="btn btn-green register">Зарегистрироватся</a>
                <a href="event.html" class="link"></a>
            </div>
        </li>
    </xsl:template> -->

    <!-- краткая карточка на главной -->
    <xsl:template match="udata[@module='catalog' and (@method='getSmartCatalog' or @method='getSmartCatalogPro')]" mode="short_event_block" />
    <xsl:template match="udata[@module='catalog' and (@method='getSmartCatalog' or @method='getSmartCatalogPro') and lines/item]" mode="short_event_block">
        <div class="events content-block gray">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12"><div class="header2">Ближайшие мероприятия</div></div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <ul class="list js-tiles-resize ga_items_list row mhz-list" data-name="Ближайшие мероприятия">
                          <xsl:apply-templates select="lines/item" mode="short_event_block"/>
                      </ul>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                        <a class="btn btn-primary" href="&all_event_url;">Все мероприятия</a>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="item" mode="short_event_block">
        <xsl:variable name="date_begin" select=".//property[@name = 'publish_date']/value/@unix-timestamp" />
        <xsl:variable name="date_end" select=".//property[@name = 'finish_date']/value/@unix-timestamp" />
        <xsl:variable name="dateru" select="document(concat('udata://data/dateruPeriod/', $date_begin, '/', $date_end))/udata" />
        <xsl:variable name="max_reserv" select="number(.//property[@name = 'max_reserv']/value)" />
        <xsl:variable name="curr_reserv" select="number(.//property[@name = 'curr_reserv']/value)" />
        <xsl:variable name="reserv" select="$max_reserv - $curr_reserv" />
        <xsl:variable name="showDiscountByDate" select="document(concat('udata://emarket/showDiscountByDate/',@id))/udata" />
        <xsl:variable name="isFirstPurchase" select="document(concat('udata://emarket/checkSecondPurchase/',@id,'/',$user-info//property[@name='e-mail']/value))/udata" />
        <xsl:variable name="loyalty" select="document(concat('udata://custom/outputEventLoyalty/',@id))/udata" />

        <xsl:variable name="speaker_photo" select="document(concat('uobject://', .//property[@name='speaker']/value/item/@id,'.photo'))//value" />

        <!-- ga params -->
        <xsl:variable name="ga_price_category">
            <xsl:choose>
                <xsl:when test="contains(.//property[@name='event_url_redirect']/value, 'orthodontia.ru')">Лендинг</xsl:when>
                <xsl:when test="($showDiscountByDate &gt; 0 and $showDiscountByDate &gt; 0) or .//property[@name='price']/value &gt; 0">Платное</xsl:when>
                <xsl:otherwise>Бесплатное</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="ga_price">
            <xsl:choose>
                <xsl:when test=".//property[@name='price_origin']/value &gt; 0">
                    <xsl:value-of select=".//property[@name='price']/value" />
                </xsl:when>
                <xsl:when test="$showDiscountByDate &gt; 0">
                    <xsl:value-of select="$showDiscountByDate" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="document(concat('udata://emarket/price/', @id,'//0'))/udata//actual" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="ga_list">
            <xsl:choose>
                <xsl:when test="contains(.//property[@name='event_url_redirect']/value, 'orthodontia.ru')">Лендинг</xsl:when>
                <xsl:when test="$parents/page/@id = &all_event_pid; or $document-page-id = &all_event_pid;">Все мероприятия</xsl:when>
                <xsl:when test="$parents/page/@id = &ormco_club_pid; or $document-page-id = &ormco_club_pid;">Клуб Ormco</xsl:when>
                <xsl:when test="$is_default_page = 1">Главная страница</xsl:when>
            </xsl:choose>
        </xsl:variable>

        <li class="col-md-3 col-sm-4 col-xs-6 ga_addImpression ga_items_list_item" data-name="{text()}" data-category="{$ga_price_category}" data-list="{$ga_list}" data-position="{position()}" data-id="{@id}" data-price="{$ga_price}" data-event_category="{.//property[@name='event_type']/value/item/@name}">
            <div class="thumbnail yellow">
                <!-- вызов шаблона, который по id типа мероприятия возвращает css класс -->
                <xsl:call-template name="add_event_css_class_short">
                    <xsl:with-param name="old_class" select="'thumbnail'"/>
                    <xsl:with-param name="event_type_id" select=".//property[@name='event_type']/value/item/@id"/>
                </xsl:call-template>

                <xsl:apply-templates select=".//property[@name='level']/value" mode="short_event_block_level" />
                <span class="date">
                    <xsl:value-of select="$dateru/first_line" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$dateru/second_line" />
                    <xsl:text> (</xsl:text>
                    <xsl:value-of select="$dateru/days" />
                    <xsl:text>)</xsl:text>
                </span>
                <span class="location">
                    <xsl:value-of select=".//property[@name='event_type']/value/item/@name" />
                    <xsl:if test=".//property[@name='gorod_in']/value/item/@name">
                        <xsl:text>, </xsl:text>
                        <xsl:value-of select=".//property[@name='gorod_in']/value/item/@name" />
                    </xsl:if>
                </span>
                <div class="topic-wrapper">
                    <div class="topic">
                        <a href="{@link}">
                            <xsl:value-of select="text()" />
                        </a>
                    </div>
                </div>
                <div class="author">
                    <img src="{$speaker_photo}" alt="{.//property[@name='speaker']/value/item/@name}">
                        <xsl:if test="not($speaker_photo)">
                            <xsl:attribute name="src">&empty-photo-speaker;</xsl:attribute>
                        </xsl:if>
                    </img>
                    <span>
                        <xsl:value-of select=".//property[@name='speaker']/value/item/@name" />
                    </span>
                </div>
                <div class="price-border">
                    <xsl:choose>
                        <xsl:when test=".//property[@name='price_origin']/value &gt; 0">
                            <span class="price">
                                <span class="old-price">
                                    <xsl:value-of select="concat(format-number(.//property[@name='price_origin']/value, '#&#160;###','price'), ' ', $currency-suffix)" />
                                </span>
                                <span class="new-price">
                                    <xsl:value-of select="concat(format-number(.//property[@name='price']/value, '#&#160;###','price'), ' ', $currency-suffix)" />
                                </span>

                                <xsl:if test="$loyalty/discount_loyalty/@isallow &gt; 0">
                                    <xsl:call-template name="discount_loyalty_short" />
                                </xsl:if>
                            </span>
                        </xsl:when>
                        <xsl:when test="$showDiscountByDate &gt; 0">
                            <span class="price">
                                <span class="old-price">
                                    <xsl:value-of select="concat(format-number(.//property[@name='price']/value, '#&#160;###','price'), ' ', $currency-suffix)" />
                                </span>
                                <span class="new-price">
                                    <xsl:value-of select="concat(format-number($showDiscountByDate, '#&#160;###','price'), ' ', $currency-suffix)" />
                                </span>

                                <xsl:if test="$loyalty/discount_loyalty/@isallow &gt; 0">
                                    <xsl:call-template name="discount_loyalty_short" />
                                </xsl:if>
                            </span>
                            <!-- <span class="btn old-price"><xsl:value-of select="concat(format-number(.//property[@name='price']/value, '#&#160;###','price'), ' ', $currency-suffix)" /></span>
                            <span class="btn btn-blue"><xsl:value-of select="concat(format-number($showDiscountByDate, '#&#160;###','price'), ' ', $currency-suffix)" /></span> -->
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="document(concat('udata://emarket/price/', @id,'//0'))/udata" mode="short_event_price" >
                                <xsl:with-param name="loyalty" select="$loyalty"/>
                            </xsl:apply-templates>
                        </xsl:otherwise>
                    </xsl:choose>
                    <!-- <span class="price">15 000 р.</span> -->
                </div>
                <xsl:choose>
                    <xsl:when test="$stub_vds=1">
                        <a href="#stub_vds_modal" data-toggle="modal" data-target="#stub_vds_modal"  class="btn btn-green buy-button register ga_item_add_basket" data-name="{text()}" data-id="{@id}" data-price="{$ga_price}" data-event_category="{.//property[@name='event_type']/value/item/@name}">Зарегистрироваться</a>
                    </xsl:when>
                    <!-- есть галочка "прячем кнопку"" -->
                    <xsl:when test=".//property[@name='hide_reg_button']/value"></xsl:when>
                    <!-- нет мест прячем кнопку" -->
                    <xsl:when test="$max_reserv &gt; 0 and  $curr_reserv &gt; 0 and not($reserv &gt; 0)">
                        <span class="fake_btn">Мест нет</span>
                    </xsl:when>
                    <!-- есть ссылка то делаем кнопку редиректа" -->
                    <!-- <xsl:when test=".//property[@name='event_type']/value/item/@id = &event_type_WOO-oid; and .//property[@name='event_url_redirect']/value"> -->
                    <xsl:when test=".//property[@name='event_url_redirect']/value">
                        <a href="{.//property[@name='event_url_redirect']/value}" class="btn btn-green buy-button register ga_item_add_basket" data-name="{text()}" data-id="{@id}" data-price="{$ga_price}" data-event_category="{.//property[@name='event_type']/value/item/@name}">Перейти к регистрации</a>
                    </xsl:when>
                    <!-- ШОО  прячем кнопку" -->
<!--                    <xsl:when test=".//property[@name='event_type']/value/item/@id = &event_type_WOO-oid;">
                        <span class="fake_btn"></span>
                    </xsl:when>-->
                    <!-- отображать сообщение, если человек уже регистрировался на данное мероприятие -->
                    <xsl:when test="$isFirstPurchase = 0">
                        <a href="#buyModalSecondPurchase" data-page_id="{@id}" data-toggle="modal" data-target="#buyModalSecondPurchase" class="btn btn-green buy-button register ga_item_add_basket" data-name="{text()}" data-id="{@id}" data-price="{$ga_price}" data-event_category="{.//property[@name='event_type']/value/item/@name}">Зарегистрироваться</a>
                    </xsl:when>
                    <xsl:otherwise>
                        <a href="#buyModal" data-page_id="{@id}" data-toggle="modal" data-target="#buyModal" class="btn btn-green buy-button register ga_item_add_basket" data-name="{text()}" data-id="{@id}" data-price="{$ga_price}" data-event_category="{.//property[@name='event_type']/value/item/@name}">Зарегистрироваться</a>
                    </xsl:otherwise>
                    <!-- есть ссылка то делаем кнопку редиректа" -->
                    <!-- <xsl:when test=".//property[@name='event_url_redirect']/value">
                        <a href="{.//property[@name='event_url_redirect']/value}" class="btn btn-green buy-button register">Перейти к регистрации</a>
                    </xsl:when> -->

                    <!-- ШОО  прячем кнопку" -->
                    <!-- <xsl:when test=".//property[@name='event_type']/value/item/@id = &event_type_WOO-oid;"><a href="#" class="btn btn-green buy-button register empty_button">&#160;</a></xsl:when>
                    <xsl:otherwise>
                        <a href="#buyModal" data-page_id="{@id}" data-toggle="modal" data-target="#buyModal" class="btn btn-green buy-button register">Зарегистрироваться</a>
                    </xsl:otherwise> -->
                </xsl:choose>
                <!-- <a href="#buyModal" data-toggle="modal" data-target="#buyModal" class="btn btn-green buy-button register">Зарегистрироватся</a> -->

                <xsl:if test=".//property[@name='akkreditaciya_star']/value">
                    <span class="akkr"></span>
                </xsl:if>

                <a href="{@link}" class="ga_eventclick link"></a>
                <xsl:if test="$loyalty/add_loyalty/@potential_bonus">
                    <a  href="&link_potential_bonus_info;"
                        class="event-points-info"
                        data-toggle="tooltip"
                        data-placement="top"
                        target="_blank"
                        title="Эти звезды вы можете получить за участие в мероприятии">
                        <i class="fa fa-star event-points-info__icon" aria-hidden="true"></i>
                        <span class="event-points-info__count">
                            <xsl:value-of select="$loyalty/add_loyalty/@potential_bonus" />
                        </span>
                    </a>
                </xsl:if>
            </div>
        </li>
    </xsl:template>

    <!-- Вывод типа мероприятия -->
    <xsl:template match="value" mode="short_event_block_level">
        <span class="level">
            <xsl:value-of select="item/@name" />
        </span>
        <ul class="stars">
            <xsl:choose>
                <xsl:when test="item/@id = 1277">
                    <li>
                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                    </li>
                </xsl:when>
                <xsl:when test="item/@id = 1313">
                    <li>
                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                    </li>
                    <li>
                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                    </li>
                </xsl:when>
                <xsl:when test="item/@id = 1314">
                    <li>
                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                    </li>
                    <li>
                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                    </li>
                    <li>
                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                    </li>
                </xsl:when>
            </xsl:choose>
        </ul>
    </xsl:template>
    <!-- TODEL Вывод место проведения мероприятия -->
    <!-- <xsl:template match="value" mode="short_event_block_mesto_provedeniya">
                    <span class="location"><xsl:value-of select="text()" /></span>
    </xsl:template> -->

    <xsl:template match="item" mode="short_event_block_right_col">
        <xsl:variable name="date_begin" select=".//property[@name = 'publish_date']/value/@unix-timestamp" />
        <xsl:variable name="date_end" select=".//property[@name = 'finish_date']/value/@unix-timestamp" />
        <xsl:variable name="dateru" select="document(concat('udata://data/dateruPeriod/', $date_begin, '/', $date_end))/udata" />
        <xsl:variable name="max_reserv" select="number(.//property[@name = 'max_reserv']/value)" />
        <xsl:variable name="curr_reserv" select="number(.//property[@name = 'curr_reserv']/value)" />
        <xsl:variable name="reserv" select="$max_reserv - $curr_reserv" />
        <xsl:variable name="showDiscountByDate" select="document(concat('udata://emarket/showDiscountByDate/',@id))/udata" />
        <xsl:variable name="loyalty" select="document(concat('udata://custom/outputEventLoyalty/',@id))/udata" />

        <xsl:variable name="speaker_photo" select="document(concat('uobject://', .//property[@name='speaker']/value/item/@id,'.photo'))//value" />

        <!-- ga params -->
        <xsl:variable name="ga_price">
            <xsl:choose>
                <xsl:when test="contains(.//property[@name='event_url_redirect']/value, 'orthodontia.ru') ">Лендинг</xsl:when>
                <xsl:when test="($showDiscountByDate &gt; 0 and $showDiscountByDate &gt; 0) or .//property[@name='price']/value &gt; 0">Платное</xsl:when>
                <xsl:otherwise>Бесплатное</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="ga_price_float">
            <xsl:choose>
                <xsl:when test="$showDiscountByDate &gt; 0 and $showDiscountByDate &gt; 0">
                    <xsl:value-of select="$showDiscountByDate" />
                </xsl:when>
                <xsl:when test=".//property[@name='price']/value &gt; 0">
                    <xsl:value-of select=".//property[@name='price']/value" />
                </xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="ga_list">
            <xsl:choose>
                <xsl:when test="contains(.//property[@name='event_url_redirect']/value, 'orthodontia.ru')">Лендинг</xsl:when>
                <xsl:when test="$parents/page/@id = &all_event_pid; or $document-page-id = &all_event_pid;">Все мероприятия</xsl:when>
                <xsl:when test="$parents/page/@id = &ormco_club_pid; or $document-page-id = &ormco_club_pid;">Клуб Ormco</xsl:when>
                <xsl:when test="$is_default_page = 1">Главная страница</xsl:when>
            </xsl:choose>
        </xsl:variable>

        <li class="ga_addImpression" data-name="{text()}" data-category="{$ga_price}" data-list="{$ga_list}" data-position="{position()}">
            <div class="thumbnail yellow">
                <!-- вызов шаблона, который по id типа мероприятия возвращает css класс -->
                <xsl:call-template name="add_event_css_class_short">
                    <xsl:with-param name="old_class" select="'thumbnail'"/>
                    <xsl:with-param name="event_type_id" select=".//property[@name='event_type']/value/item/@id"/>
                </xsl:call-template>

                <xsl:apply-templates select=".//property[@name='level']/value" mode="short_event_block_level" />
                <span class="date">
                    <xsl:value-of select="$dateru/first_line" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$dateru/second_line" />
                    <xsl:text> (</xsl:text>
                    <xsl:value-of select="$dateru/days" />
                    <xsl:text>)</xsl:text>
                </span>

                <!-- <xsl:apply-templates select=".//property[@name='mesto_provedeniya']/value" mode="short_event_block_mesto_provedeniya" />  -->
                <span class="location">
                    <xsl:value-of select=".//property[@name='event_type']/value/item/@name" />
                    <xsl:if test=".//property[@name='gorod_in']/value/item/@name">
                        <xsl:text>, </xsl:text>
                        <xsl:value-of select=".//property[@name='gorod_in']/value/item/@name" />
                    </xsl:if>
                </span>

                <div class="topic-wrapper">
                    <div class="topic">
                        <a href="{@link}">
                            <xsl:value-of select="text()" />
                        </a>
                    </div>
                </div>
                <div class="author">
                    <img src="{$speaker_photo}" alt="{.//property[@name='speaker']/value/item/@name}">
                        <xsl:if test="not($speaker_photo)">
                            <xsl:attribute name="src">&empty-photo-speaker;</xsl:attribute>
                        </xsl:if>
                    </img>
                    <span>
                        <xsl:value-of select=".//property[@name='speaker']/value/item/@name" />
                    </span>
                </div>
                <div class="price-border">
                    <xsl:choose>
                        <xsl:when test=".//property[@name='price_origin']/value &gt; 0">
                            <span class="price">
                                <span class="old-price">
                                    <xsl:value-of select="concat(format-number(.//property[@name='price_origin']/value, '#&#160;###','price'), ' ', $currency-suffix)" />
                                </span>
                                <span class="new-price">
                                    <xsl:value-of select="concat(format-number(.//property[@name='price']/value, '#&#160;###','price'), ' ', $currency-suffix)" />
                                </span>

                                <xsl:if test="$loyalty/discount_loyalty/@isallow &gt; 0">
                                    <xsl:call-template name="discount_loyalty_short" />
                                </xsl:if>
                            </span>
                        </xsl:when>
                        <xsl:when test="$showDiscountByDate &gt; 0">
                            <span class="price">
                                <span class="old-price">
                                    <xsl:value-of select="concat(format-number(.//property[@name='price']/value, '#&#160;###','price'), ' ', $currency-suffix)" />
                                </span>
                                <span class="new-price">
                                    <xsl:value-of select="concat(format-number($showDiscountByDate, '#&#160;###','price'), ' ', $currency-suffix)" />
                                </span>

                                <xsl:if test="$loyalty/discount_loyalty/@isallow &gt; 0">
                                    <xsl:call-template name="discount_loyalty_short" />
                                </xsl:if>
                            </span>
                            <!-- <span class="btn old-price"><xsl:value-of select="concat(format-number(.//property[@name='price']/value, '#&#160;###','price'), ' ', $currency-suffix)" /></span>
                            <span class="btn btn-blue"><xsl:value-of select="concat(format-number($showDiscountByDate, '#&#160;###','price'), ' ', $currency-suffix)" /></span> -->
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="document(concat('udata://emarket/price/', @id,'//0'))/udata" mode="short_event_price"  >
                                <xsl:with-param name="loyalty" select="$loyalty"/>
                            </xsl:apply-templates>
                        </xsl:otherwise>
                    </xsl:choose>
                    <!-- <span class="price">15 000 р.</span> -->
                </div>
                <xsl:choose>
                    <xsl:when test="$stub_vds=1">
                        <a href="#stub_vds_modal" data-toggle="modal" data-target="#stub_vds_modal"  class="btn btn-green buy-button register">Зарегистрироваться</a>
                    </xsl:when>
                    <!-- есть галочка "прячем кнопку"" -->
                    <xsl:when test=".//property[@name='hide_reg_button']/value"></xsl:when>
                    <!-- нет мест прячем кнопку" -->
                    <xsl:when test="$max_reserv &gt; 0 and  $curr_reserv &gt; 0 and not($reserv &gt; 0)">
                        <span class="fake_btn">Мест нет</span>
                    </xsl:when>
                    <!-- есть ссылка то делаем кнопку редиректа" -->
                    <!-- <xsl:when test=".//property[@name='event_type']/value/item/@id = &event_type_WOO-oid; and .//property[@name='event_url_redirect']/value"> -->
                    <xsl:when test=".//property[@name='event_url_redirect']/value">
                        <a href="{.//property[@name='event_url_redirect']/value}" data-price="{$ga_price_float}"  class="btn btn-green buy-button register">Перейти к регистрации</a>
                    </xsl:when>
                    <!-- ШОО  прячем кнопку" -->
<!--                    <xsl:when test=".//property[@name='event_type']/value/item/@id = &event_type_WOO-oid;">
                        <span class="fake_btn"></span>
                    </xsl:when>-->

                    <xsl:otherwise>
                        <a href="#buyModal" data-price="{$ga_price_float}"  data-page_id="{@id}" data-toggle="modal" data-target="#buyModal" class="btn btn-green buy-button register">Зарегистрироваться</a>
                    </xsl:otherwise>
                    <!-- есть ссылка то делаем кнопку редиректа" -->
                    <!-- <xsl:when test=".//property[@name='event_url_redirect']/value">
                            <a href="{.//property[@name='event_url_redirect']/value}" class="btn btn-green buy-button register">Перейти к регистрации</a>
                    </xsl:when> -->
                    <!-- ШОО  прячем кнопку" -->
                    <!-- <xsl:when test=".//property[@name='event_type']/value/item/@id = &event_type_WOO-oid;"><a href="#" class="btn btn-green buy-button register empty_button">&#160;</a></xsl:when>
                    <xsl:otherwise>
                            <a href="#buyModal" data-page_id="{@id}" data-toggle="modal" data-target="#buyModal" class="btn btn-green buy-button register">Зарегистрироваться</a>
                    </xsl:otherwise> -->
                </xsl:choose>
                <!-- <a href="#buyModal" data-toggle="modal" data-target="#buyModal" class="btn btn-green buy-button register">Зарегистрироватся</a> -->
                <a href="{@link}" class="ga_eventclick link"></a>
                <xsl:if test="$loyalty/add_loyalty/@potential_bonus">
                    <a  href="&link_potential_bonus_info;"
                        class="event-points-info"
                        data-toggle="tooltip"
                        data-placement="top"
                        target="_blank"
                        title="Эти звезды вы можете получить за участие в мероприятии">
                        <i class="fa fa-star event-points-info__icon" aria-hidden="true"></i>
                        <span class="event-points-info__count">
                            <xsl:value-of select="$loyalty/add_loyalty/@potential_bonus" />
                        </span>
                    </a>
                </xsl:if>
            </div>
        </li>
    </xsl:template>
</xsl:stylesheet>