<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="filter" select="'filter'" />
	<xsl:variable name="from" select="'from'" />
	<xsl:variable name="to" select="'to'" />

	<xsl:template match="udata[@method = 'getSmartFilters']" mode="search"/>
	<xsl:template match="udata[@method = 'getSmartFilters'][group]" mode="search">
		<xsl:param name="action" select="''" />
		<xsl:variable name="fields" select="//field" />

		<hr />
		<div class="top-selectors">
			<form  class="clearfix" action="{$action}" data-category="{@category-id}">
				<xsl:choose>
					<xsl:when test="$document-page-id = &consultation_pid; or $parents/page/@id = &consultation_pid;">
						<xsl:apply-templates select="//field[@name = 'gorod_in' or @name = 'tematika_konsultacii']"  mode="filter" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="//field[not(@name = 'publish_date' or @name = 'finish_date')]"  mode="filter" />
					</xsl:otherwise>
				</xsl:choose>

            	<a class="help_ico"  href="#helpModal" data-toggle="modal" data-target="#helpModal"><i class="fa fa-question-circle " aria-hidden="true"></i></a>
            	<a href="#" class="reset_filter" onclick="document.location.href = '?'"><i class="fa fa-times" aria-hidden="true"></i>&reset;</a>
            </form>
        </div>
	</xsl:template>

	<xsl:template match="field" mode="filter">
		<select name="{$filter}[{@name}][{position() - 1}]" title="{@title}" id="basic_{@name}" class="selectpicker show-tick">
            <option value="">Все</option>
            <xsl:apply-templates select="." mode="filter_field"/>
        </select>
	</xsl:template>

	<xsl:template match="field" mode="filter_field">
		<xsl:apply-templates select="item" mode="filter_field" />
	</xsl:template>

	<xsl:template match="field" mode="filter_field_uroven_meropriyatiya">
		<xsl:apply-templates select="item[1]" mode="filter_field" />
		<xsl:apply-templates select="item[3]" mode="filter_field" />
		<xsl:apply-templates select="item[2]" mode="filter_field" />
	</xsl:template>

	<xsl:template match="field/item" mode="filter_field">
		<option value="{text()}">
			<xsl:if test="@is-selected = '1'">
				<xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="text()" />
		</option>
	</xsl:template>
</xsl:stylesheet>