<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

    <xsl:template match="result[@module = 'catalog' and @method='category']">
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
	                    <xsl:apply-templates select="document('udata://menu/draw/right_menu_events')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

        <!--Breadcrumbs -->
        <xsl:apply-templates select="document('udata://core/navibar/')/udata" />

        <!--Content -->
        <div class="container-fluid events-page">
            <div class="page-header">
                <div class="row">
                    <div class="col-xs-12">
                        <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                        <h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <xsl:apply-templates select="document(concat('udata://catalog/getSmartFilters//',$document-page-id, '/0/100'))" mode="search" />

                    <xsl:choose>
                        <xsl:when test="&all_event_pid; = $document-page-id">
                            <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalogPro//', $document-page-id, '///2/publish_date/1/?extProps=event_url_redirect,gorod_in,speaker,photo,publish_date,finish_date,mesto_provedeniya,event_type,level,organizer,price_string,level_shoo,max_reserv,curr_reserv,price,price_origin,hide_reg_button,akkreditaciya_star,uchebnyj_centr'))/udata" />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalogPro//', $document-page-id, '///2/?extProps=event_url_redirect,gorod_in,speaker,photo,publish_date,finish_date,mesto_provedeniya,event_type,level,organizer,price_string,level_shoo,max_reserv,curr_reserv,price,price_origin,hide_reg_button,akkreditaciya_star,uchebnyj_centr'))/udata" />
                        </xsl:otherwise>
                    </xsl:choose>
                </div>
                <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
                    <xsl:apply-templates select="document('udata://menu/draw/right_menu_events')/udata" mode="right_menu" />

                    <!-- calendar -->
                    <xsl:call-template name="calendar" />
                    <xsl:apply-templates select="." mode="right_col" />
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="udata[@module='catalog' and (@method='getSmartCatalog' or @method='getSmartCatalogPro')]">
        <div class="white-pnl clearfix">
            Актуальных мероприятий не найдено.<br/>
            Вы можете проверить другие разделы с мероприятиями.<br/>
            Мероприятия располагаются в разделах <a href="&all_event_url;">"Все мероприятия"</a>, <a href="{document('upage://&all_event_archive_pid;')/udata/page/@link}">"Архив мероприятий"</a> или <a href="{document('upage://&ormco_club_pid;')/udata/page/@link}">"Клуб Ormco"</a>.
        </div>
    </xsl:template>

    <xsl:template match="udata[@module='catalog' and (@method='getSmartCatalog' or @method='getSmartCatalogPro') and lines/item]">
        <ul class="items ga_items_list" data-name="{@header}">
            <xsl:apply-templates select="lines/item" />
        </ul>
        <xsl:apply-templates select="total" />
    </xsl:template>

    <xsl:template match="udata[@module='catalog' and (@method='getSmartCatalog' or @method='getSmartCatalogPro')]/lines/item">
        <xsl:variable name="date_begin" select=".//property[@name = 'publish_date']/value/@unix-timestamp" />
        <xsl:variable name="date_end" select=".//property[@name = 'finish_date']/value/@unix-timestamp" />
        <xsl:variable name="dateru" select="document(concat('udata://data/dateruPeriod/', $date_begin, '/', $date_end))/udata" />
        <xsl:variable name="max_reserv" select="number(.//property[@name = 'max_reserv']/value)" />
        <xsl:variable name="curr_reserv" select="number(.//property[@name = 'curr_reserv']/value)" />
        <xsl:variable name="reserv" select="$max_reserv - $curr_reserv" />
        <xsl:variable name="showDiscountByDate" select="document(concat('udata://emarket/showDiscountByDate/',@id))/udata" />
        <xsl:variable name="isFirstPurchase" select="document(concat('udata://emarket/checkSecondPurchase/',@id,'/',$user-info//property[@name='e-mail']/value))/udata" />
        <xsl:variable name="loyalty" select="document(concat('udata://custom/outputEventLoyalty/',@id))/udata" />

        <xsl:variable name="currency-suffix">
            <xsl:choose>
                <xsl:when test="document(concat('upage://', @id, '.vybrana_drugaya_valyuta'))//value"><xsl:value-of select="document(concat('uobject://', document(concat('upage://', @id, '.vybrana_drugaya_valyuta'))//value/item/@id, '.postfiks_valyuty'))//value" /></xsl:when>
                <xsl:otherwise><xsl:value-of select="$cart/summary/price/@suffix" /></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="speaker_photo">
            <xsl:choose>
                <xsl:when test=".//property[@name='photo']/value">
                    <xsl:value-of select=".//property[@name='photo']/value" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="document(concat('uobject://', .//property[@name='speaker']/value/item/@id,'.photo'))//value" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <!-- ga params -->
        <xsl:variable name="ga_price">
            <xsl:choose>
                <xsl:when test="contains(.//property[@name='event_url_redirect']/value, 'orthodontia.ru') ">Лендинг</xsl:when>
                <xsl:when test="($showDiscountByDate &gt; 0 and $showDiscountByDate &gt; 0) or .//property[@name='price']/value &gt; 0">Платное</xsl:when>
                <xsl:otherwise>Бесплатное</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="ga_price_float">
            <xsl:choose>
                <xsl:when test="$showDiscountByDate &gt; 0 and $showDiscountByDate &gt; 0"><xsl:value-of select="$showDiscountByDate" /></xsl:when>
                <xsl:when test=".//property[@name='price']/value &gt; 0"><xsl:value-of select=".//property[@name='price']/value" /></xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="ga_list">
            <xsl:choose>
                <xsl:when test="contains(.//property[@name='event_url_redirect']/value, 'orthodontia.ru')">Лендинг</xsl:when>
                <xsl:when test="$parents/page/@id = &all_event_pid; or $document-page-id = &all_event_pid;">Все мероприятия</xsl:when>
                <xsl:when test="$parents/page/@id = &ormco_club_pid; or $document-page-id = &ormco_club_pid;">Клуб Ormco</xsl:when>
                <xsl:when test="$is_default_page = 1">Главная страница</xsl:when>
            </xsl:choose>
        </xsl:variable>

        <li class="item ga_addImpression ga_items_list_item" data-name="{text()}" data-category="{$ga_price}" data-list="{$ga_list}" data-position="{position()}" data-id="{@id}" data-price="{$ga_price_float}" data-event_category="{.//property[@name='event_type']/value/item/@name}">
            <div class="white-pnl clearfix">
                <xsl:if test="$loyalty/add_loyalty/@potential_bonus">
                    <a href="&link_potential_bonus_info;"
                      class="event-points-info"
                      data-toggle="tooltip"
                      data-placement="top"
                      target="_blank">
                        <xsl:choose>
                            <xsl:when test="$user-type='svtest'">
                                <xsl:attribute name="data-toggle" ></xsl:attribute>
                                <xsl:attribute name="class" >event-points-info stars_popover_link</xsl:attribute>
                                <xsl:attribute name="data-content" >Эти звезды вы можете получить за участие в мероприятии &lt;br/&gt;&lt;a href='&link_potential_bonus_info;' target='_blank' &gt;Подробнее&lt;/a&gt;</xsl:attribute>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:attribute name="title">Эти звезды вы можете получить за участие в мероприятии</xsl:attribute>
                            </xsl:otherwise>
                        </xsl:choose>

                        <i class="fa fa-star event-points-info__icon" aria-hidden="true"></i>
                        <span class="event-points-info__count"><xsl:value-of select="$loyalty/add_loyalty/@potential_bonus" /> звезд</span>
                    </a>
                </xsl:if>

                <div class="left">
                    <a href="{@link}" class="ga_eventclick">
                    	<img src="{$speaker_photo}" class="speaker" alt="{.//property[@name='speaker']/value/item/@name}">
                            <xsl:if test="not($speaker_photo) or $speaker_photo=''"><xsl:attribute name="src">&empty-photo-speaker;</xsl:attribute></xsl:if>
                    	</img>
                    </a>
                </div>
                <div class="right">
                    <a href="{@link}" class="ga_eventclick">
                    	<h4><xsl:value-of select="text()" />
                            <xsl:if test=".//property[@name='level_shoo']/value/item/@name">
                                , уровень <xsl:value-of select=".//property[@name='level_shoo']/value/item/@name" />
                                <xsl:if test=".//property[@name='gorod_in']/value/item/@name">
                                    , <xsl:value-of select=".//property[@name='gorod_in']/value/item/@name" />
                                </xsl:if>
                            </xsl:if>
                    	</h4>
                    </a>
                    <xsl:apply-templates select=".//property[@name='speaker']/value/item" mode="speaker" />

                    <!-- <xsl:apply-templates select=".//property[@name='publish_date']/value" mode="publish_date" /> -->
                    <span class="date">
                        <i class="fa fa-calendar" aria-hidden="true"></i><xsl:text>Дата: </xsl:text>
                        <xsl:value-of select="$dateru/first_line" />
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="$dateru/second_line" />
                        <xsl:text> (</xsl:text>
                        <xsl:value-of select="$dateru/days" />
                        <xsl:text>)</xsl:text>
                    </span>

                    <xsl:apply-templates select=".//property[@name='mesto_provedeniya']/value" mode="mesto_provedeniya" />
                    <xsl:apply-templates select=".//property[@name='event_type']/value" mode="event_type" />
                    <xsl:apply-templates select=".//property[@name='level']/value" mode="level" />
                    <xsl:apply-templates select=".//property[@name='organizer']/value" mode="organizer" />
                    <xsl:apply-templates select=".//property[@name='uchebnyj_centr']" mode="uchebnyj_centr" />
                </div>

                <div class="row">
                    <div class="col-xs-12 text-right buttons">
                        <hr />

                        <xsl:if test=".//property[@name='akkreditaciya_star']/value">
                            <span class="akkr_list"></span>
                        </xsl:if>

                        <xsl:choose>
                            <xsl:when test="$document-page-id = 269"></xsl:when> <!-- Скрываем места в архиве -->
                            <xsl:when test="$max_reserv &gt; 0 and  not($curr_reserv)">
                                <span class="reserv hidden-xs0">Места еще есть</span>
                            </xsl:when>
                            <xsl:when test="$max_reserv &gt; 0 and  $curr_reserv &gt; 0 and $reserv &gt; 0">
                                <span class="reserv hidden-xs0">Места еще есть</span>
                            </xsl:when>
                            <xsl:when test="$max_reserv &gt; 0 and  $curr_reserv &gt; 0 and not($reserv &gt; 0)">
                                <span class="reserv hidden-xs0">Мест нет</span>
                            </xsl:when>
                        </xsl:choose>

                        <xsl:choose>
                            <xsl:when test=".//property[@name='price_origin']/value &gt; 0">
                                <span class="btn old-price"><xsl:value-of select="concat(format-number(.//property[@name='price_origin']/value, '#&#160;###','price'), ' ', $currency-suffix)" /></span>
                                <!-- нужно ли выводить скидку лояльности -->
                                <xsl:choose>
                                    <xsl:when test="$loyalty/discount_loyalty/@isallow &gt; 0">
                                        <div class="btn-group">
                                            <span class="btn btn-blue"><xsl:value-of select="concat(format-number(.//property[@name='price']/value, '#&#160;###','price'), ' ', $currency-suffix)" /></span>
                                            <xsl:call-template name="discount_loyalty" />
                                        </div>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <span class="btn btn-blue"><xsl:value-of select="concat(format-number(.//property[@name='price']/value, '#&#160;###','price'), ' ', $currency-suffix)" /></span>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:when test="$showDiscountByDate &gt; 0">
                                <span class="btn old-price"><xsl:value-of select="concat(format-number(.//property[@name='price']/value, '#&#160;###','price'), ' ', $currency-suffix)" /></span>
                                <!-- нужно ли выводить скидку лояльности -->
                                <xsl:choose>
                                    <xsl:when test="$loyalty/discount_loyalty/@isallow &gt; 0">
                                        <div class="btn-group">
                                            <span class="btn btn-blue"><xsl:value-of select="concat(format-number($showDiscountByDate, '#&#160;###','price'), ' ', $currency-suffix)" /></span>
                                            <xsl:call-template name="discount_loyalty" />
                                        </div>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <span class="btn btn-blue"><xsl:value-of select="concat(format-number($showDiscountByDate, '#&#160;###','price'), ' ', $currency-suffix)" /></span>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <!-- нужно ли выводить скидку лояльности -->
                                <xsl:choose>
                                    <xsl:when test="$loyalty/discount_loyalty/@isallow &gt; 0">
                                        <div class="btn-group">
                                            <xsl:apply-templates select="document(concat('udata://emarket/price/', @id,'//0'))/udata">
                                                <xsl:with-param name="currency-suffix" select="$currency-suffix" />
                                            </xsl:apply-templates>
                                            <xsl:call-template name="discount_loyalty" />
                                        </div>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:apply-templates select="document(concat('udata://emarket/price/', @id,'//0'))/udata">
                                            <xsl:with-param name="currency-suffix" select="$currency-suffix" />
                                        </xsl:apply-templates>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>

                        <!-- Показываем цену и кнопку регистрации только если мы в разделе все мероприятия или в разделе клуб ормко -->
                        <!-- <xsl:if test="not($parents/page/@id = &all_event_archive_pid; or $document-page-id = &all_event_archive_pid;)"> -->
                        <xsl:choose>
                            <xsl:when test="$stub_vds=1">
                                    <a href="#stub_vds_modal" data-toggle="modal" data-target="#stub_vds_modal"  class="btn btn-green buy-button">Зарегистрироваться</a>
                            </xsl:when>
                            <xsl:when test="$e=7">
                                    <a href="#buyModal" data-price="{$ga_price_float}" data-page_id="{@id}" data-toggle="modal" data-target="#buyModal" class="btn btn-green buy-button">Зарегистрироваться</a>
                            </xsl:when>
                            <!-- есть галочка "прячем кнопку"" -->
                            <xsl:when test=".//property[@name='hide_reg_button']/value"></xsl:when>
                            <!-- нет мест прячем кнопку" -->
                            <xsl:when test="$max_reserv &gt; 0 and  $curr_reserv &gt; 0 and not($reserv &gt; 0)"></xsl:when>
                            <!-- есть ссылка то делаем кнопку редиректа" -->
                            <!-- <xsl:when test=".//property[@name='event_type']/value/item/@id = &event_type_WOO-oid; and .//property[@name='event_url_redirect']/value"> -->
                            <xsl:when test=".//property[@name='event_url_redirect']/value">
                                <a href="{.//property[@name='event_url_redirect']/value}" class="btn btn-green buy-button">Перейти к регистрации</a>
                            </xsl:when>

                            <!-- ШОО  прячем кнопку" -->
                            <!--<xsl:when test=".//property[@name='event_type']/value/item/@id = &event_type_WOO-oid;"></xsl:when>-->

                            <!-- отображать сообщение, если человек уже регистрировался на данное мероприятие -->
                            <xsl:when test="$isFirstPurchase = 0 and ($parents/page/@id = &ormco_club_pid; or $document-page-id = &ormco_club_pid; or $parents/page/@id = &all_event_pid; or $document-page-id = &all_event_pid;)">
                                <a href="#buyModalSecondPurchase" data-price="{$ga_price_float}"  data-page_id="{@id}" data-toggle="modal" data-target="#buyModalSecondPurchase" class="btn btn-green buy-button">Зарегистрироваться</a>
                            </xsl:when>

                            <xsl:when test="$parents/page/@id = &ormco_club_pid; or $document-page-id = &ormco_club_pid; or $parents/page/@id = &all_event_pid; or $document-page-id = &all_event_pid;">
                                <a href="#buyModal" data-price="{$ga_price_float}"  data-page_id="{@id}" data-toggle="modal" data-target="#buyModal" class="btn btn-green buy-button">Зарегистрироваться</a>
                            </xsl:when>
                        </xsl:choose>

                        <!-- <xsl:if test="$parents/page/@id = &ormco_club_pid; or $document-page-id = &ormco_club_pid; or $parents/page/@id = &all_event_pid; or $document-page-id = &all_event_pid;">
                            <a href="#buyModal" data-page_id="{@id}" data-toggle="modal" data-target="#buyModal" class="btn btn-green buy-button">Зарегистрироваться</a>
                        </xsl:if> -->
                    </div>
                </div>
            </div>
        </li>
    </xsl:template>

    <!-- Вывод спикеров -->
    <xsl:template match="item" mode="speaker">
        <xsl:variable name="speaker_link" select="document(concat('udata://catalog/getLinkByObject/', @id))/udata" />

        <a href="{$speaker_link}" class="speaker">
            <xsl:if test="position()=last()"><xsl:attribute name="class">speaker last</xsl:attribute></xsl:if>
            <xsl:value-of select="@name" />
        </a>
    </xsl:template>

    <!-- Вывод даты мероприятия -->
    <xsl:template match="value" mode="publish_date">
        <span class="date"><i class="fa fa-calendar" aria-hidden="true"></i>Дата: <xsl:value-of select="document(concat('udata://catalog/dateru/',@unix-timestamp))/udata"/></span>
    </xsl:template>

    <!-- Вывод место проведения мероприятия -->
    <xsl:template match="value" mode="mesto_provedeniya">
        <p class="location"><i class="fa fa-map-pin" aria-hidden="true"></i>
            <xsl:if test="../..//property[@name='gorod_in']/value/item/@name">
                <xsl:text>г. </xsl:text><xsl:value-of select="../..//property[@name='gorod_in']/value/item/@name" /><xsl:text>, </xsl:text>
            </xsl:if>
            <xsl:value-of select="text()" />
        </p>
    </xsl:template>

    <!-- Вывод типа мероприятия -->
    <xsl:template match="value" mode="event_type">
        <span class="seminar-type-link css-aqua">
            <!-- вызов шаблона, который по id типа мероприятия возвращает css класс -->
            <xsl:call-template name="add_event_css_class">
                <xsl:with-param name="old_class" select="'seminar-type-link'"/>
                <xsl:with-param name="event_type_id" select="item/@id"/>
            </xsl:call-template>
            <a href="{$curr_url}?filter[event_type][4]={item/@name}" class="ga_btn"><xsl:value-of select="item/@name" /></a>
        </span>
    </xsl:template>

    <!-- Вывод уровня мероприятия -->
    <xsl:template match="value" mode="level">
        <div class="level">
            <ul class="stars">
                <xsl:choose>
                    <xsl:when test="item/@id = 1277">
                        <li><i class="fa fa-graduation-cap" aria-hidden="true"></i></li>
                    </xsl:when>
                    <xsl:when test="item/@id = 1313">
                        <li><i class="fa fa-graduation-cap" aria-hidden="true"></i></li>
                        <li><i class="fa fa-graduation-cap" aria-hidden="true"></i></li>
                    </xsl:when>
                    <xsl:when test="item/@id = 1314">
                        <li><i class="fa fa-graduation-cap" aria-hidden="true"></i></li>
                        <li><i class="fa fa-graduation-cap" aria-hidden="true"></i></li>
                        <li><i class="fa fa-graduation-cap" aria-hidden="true"></i></li>
                    </xsl:when>
                </xsl:choose>
            </ul>
            <span><a href="{$curr_url}?filter[level][3]={item/@name}" class="ga_btn"><xsl:value-of select="item/@name" /></a></span>
        </div>
    </xsl:template>

    <!-- Вывод тематики мероприятия -->
    <xsl:template match="value" mode="organizer">
        <xsl:variable name="logo" select="document(concat('uobject://',item/@id,'logo'))//value" />
        <span class="theme">
            <a href="{$curr_url}?filter[organizer][1]={item/@name}" class="ga_btn">
                <img src="{$logo}" alt="{item/@name}" />
                <xsl:if test="not(item/@id = 1284 or item/@id = 1315 or item/@id = 1321)">
                    <xsl:value-of select="item/@name" />
                </xsl:if>
            </a>
        </span>
    </xsl:template>

    <xsl:template match="property" mode="uchebnyj_centr" />
    <xsl:template match="property[value]" mode="uchebnyj_centr">
        <xsl:apply-templates select="value/page" mode="uchebnyj_centr" />
    </xsl:template>
    <xsl:template match="page" mode="uchebnyj_centr">
        <a href="{@link}" class="education_center">
            <xsl:value-of select="name" />
        </a>
    </xsl:template>

    <xsl:template name="discount_loyalty">
        <a href="&link_discount_ormco_star_info;"
            class="btn btn-points-discount event-points-button"
            data-toggle="tooltip"
            data-placement="top"
            target="_blank"
            title="Вы можете зарегистрироваться на данное мероприятие со скидкой за звезды Ormco Stars">
            Получить скидку 50%
            <span class="event-points-button__help">
                <i class="fa fa-question-circle " aria-hidden="true"></i>
            </span>
        </a>
    </xsl:template>

    <xsl:template name="discount_loyalty_short">
        <a  href="&link_discount_ormco_star_info;"
            class="btn-points-discount event-points-button"
            data-toggle="tooltip"
            data-placement="top"
            target="_blank"
            title="Вы можете зарегистрироваться на данное мероприятие со скидкой за звезды Ormco Stars">
            <span class="event-points-button__help">
                <i class="fa fa-star event-points-info__icon" aria-hidden="true"></i>
            </span>
        </a>
    </xsl:template>
</xsl:stylesheet>