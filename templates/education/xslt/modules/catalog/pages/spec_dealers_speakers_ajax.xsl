<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:date="http://exslt.org/dates-and-times"
	xmlns:udt="http://umi-cms.ru/2007/UData/templates"
	xmlns:xlink="http://www.w3.org/TR/xlink"
	exclude-result-prefixes="xsl date udt xlink">
	
	<xsl:output encoding="utf-8" method="html" indent="yes" />
 
	<xsl:template match="/">
		<div id="filter_result">
			<xsl:choose>
				<xsl:when test="udata/lines/item">
					<xsl:apply-templates select="udata/lines/item" mode="spec_dealers_speakers_row"/>
				</xsl:when>
				<xsl:otherwise>
					<div class="white-pnl clearfix dealer-event"><article>Результатов не найдено! Попробуйте изменить параметры фильтра</article></div>
				</xsl:otherwise>
			</xsl:choose>
			
	  </div>              
	</xsl:template>
	
	<xsl:template match="item" mode="spec_dealers_speakers_row" />
	<xsl:template match="item[position() mod 2 = 1]" mode="spec_dealers_speakers_row">
		<xsl:variable name="cur_pos" select="position() + 1" />
		<div class="row" >
			<xsl:apply-templates select="." mode="spec_dealers_speakers"/>
			<xsl:apply-templates select="../item[position() = $cur_pos]" mode="spec_dealers_speakers"/>
		</div>
	</xsl:template>

	<xsl:template match="item" mode="spec_dealers_speakers">
		<div class="col-sm-6 col-xs-12">
        <div class="card-item" data-href="{@link}">
            <div class="card-item__checkbox js--pdf-download-item" data-pdf-id="{@id}"></div>
            <div class="card-item__left">
                <a href="{@link}" >
                	<img src="{.//property[@name='foto_spikera']/value}" class="img-responsive" alt="{text()}">
                		<!-- <xsl:if test="not(.//property[@name='foto_spikera']/value) or .//property[@name='foto_spikera']/value=''"><xsl:attribute name="src">/templates/education/img/speakers/2.png</xsl:attribute></xsl:if>
                	 --></img>
                </a>
            </div>
            <div class="card-item__right">
                <h4 class="card-item__username">
                    <a href="{@link}"><xsl:value-of select="text()" /></a>
                </h4>

                <ul class="metadata-list">
                	<xsl:apply-templates select=".//property[@name='gorod_prozhivaniya']" mode="gorod_prozhivaniya" />
                    <xsl:apply-templates select=".//property[@name='tip_meropriyatiya']" mode="tip_meropriyatiya" />
                </ul>
            </div>
            <hr class="card-item__hr" />
            <xsl:apply-templates select=".//property[@name='tematika_provodimyh_meropriyatij']" mode="tematika_provodimyh_meropriyatij" />


        </div>
    </div>
	</xsl:template>
	
	<!-- gorod_prozhivaniya -->
	<xsl:template match="property" mode="gorod_prozhivaniya">
		<li class="metadata-list__item">
            <i class="new-icon new-icon_home metadata-list__item-icon"></i>
            <span class="metadata-list__item-text">
                Город проживания: <xsl:value-of select="value/item/@name" />
            </span>
        </li>
	</xsl:template>

	<!-- tip_meropriyatiya -->
	<xsl:template match="property" mode="tip_meropriyatiya">
		<li class="metadata-list__item">
            <i class="new-icon new-icon_user metadata-list__item-icon"></i>
            <span class="metadata-list__item-text">
                <xsl:apply-templates select="value/item" mode="tip_meropriyatiya" />
            </span>
        </li>
	</xsl:template>
	<xsl:template match="item" mode="tip_meropriyatiya">
		<xsl:if test="not(position()=1)">, </xsl:if><xsl:value-of select="@name" />
	</xsl:template>

	<!-- tematika_provodimyh_meropriyatij -->
	<xsl:template match="property" mode="tematika_provodimyh_meropriyatij">
		<ul class="types-list">
			<xsl:apply-templates select="value/item" mode="tematika_provodimyh_meropriyatij" />
        </ul>
	</xsl:template>
	<xsl:template match="item" mode="tematika_provodimyh_meropriyatij">
        <xsl:variable name="logo" select="document(concat('uobject://',@id,'.logo'))//value" />


        <li class="types-list__item">
            <img src="{$logo}" alt="{@name}" class="types-list__item-img" data-toggle="tooltip" data-placement="top" data-html="true">
                <xsl:attribute name="title"><xsl:value-of select="@name" /></xsl:attribute>
            </img>
        </li>
	</xsl:template>

	

</xsl:stylesheet>
