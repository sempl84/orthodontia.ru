<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'catalog' and @method='category' and @pageId=&my_archive_events_pid;]">
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
		                <xsl:apply-templates select="document('udata://menu/draw/right_menu_lk')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

		<!--Content -->
	    <div class="container-fluid events-page">
	        <div class="page-header">
	            <div class="row">
	                <div class="col-xs-12">
	                    <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
	                    <h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
	                </div>
	            </div>
	        </div>
	        <div class="row">
	            <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
	                 <xsl:apply-templates select="document('udata://emarket/eventsList///1?extProps=speaker,publish_date,finish_date,mesto_provedeniya,event_type,level,organizer,price_string')/udata" mode="my_archive_events"/>
	            </div>
	            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
	                <xsl:apply-templates select="document('udata://menu/draw/right_menu_lk')/udata" mode="right_menu" />

	                <!-- calendar -->
	                <xsl:call-template name="calendar" />
	                <xsl:apply-templates select="." mode="right_col" />
	            </div>
	        </div>
	    </div>
	</xsl:template>

	<xsl:template match="udata" mode="my_archive_events">
		<div class="white-pnl clearfix">
        	Мероприятий нет.
        </div>
	</xsl:template>
	<xsl:template match="udata[items/item]" mode="my_archive_events">
		<ul class="items">
            <xsl:apply-templates select="items/item[lines/item]" mode="my_events">
				<xsl:sort select="lines/item/@publish_date" order="descending"/>
				<xsl:with-param name="with_event_participation" select="1" />
            </xsl:apply-templates>
        </ul>
	</xsl:template>
</xsl:stylesheet>