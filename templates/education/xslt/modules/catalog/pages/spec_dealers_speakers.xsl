<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'catalog' and @method='category' and page/@id = &spec_dealers_speakers_pid;]">
		<xsl:variable name="filters" select="document(concat('udata://catalog/getSmartFilters//',$document-page-id, '/0/100'))" />
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
						<xsl:apply-templates select="document('udata://menu/draw/spec_dealers')/udata" mode="right_menu" />
						<xsl:apply-templates select="$filters" mode="right_filters_spikers" />
					</div>
				</div>
			</div>
		</nav>

		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

		<!--Content -->
		<div class="container-fluid dealers-speakers-page">
			<div class="row">
				<div class="col-xs-12">
					<div class="top-header">
						<i class="new-icon new-icon_large new-icon_speaker top-header__icon"></i>
						<h1 class="top-header__title">
							<xsl:value-of select="@header" disable-output-escaping="yes" />
						</h1>
					</div>
				</div>

				<div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
					<xsl:if test="$filters[.//field[@data-type = 'relation' and @name='tematika_provodimyh_meropriyatij']]">
						<hr />
						<xsl:apply-templates select="$filters" mode="filters_spikers" >
							<!-- <xsl:with-param name="action" select="concat('/udata/catalog/getSmartCatalogPro//', $document-page-id, '/1000/1/2///?extProps=foto_spikera,uroven_meropriyatiya,tip_meropriyatiya,gorod_prozhivaniya,tematika_provodimyh_meropriyatij')" /> -->
							<xsl:with-param name="action" select="'/templates/education/speakers.html'" />
						</xsl:apply-templates>
						<div style="position:absolute; left:-100000px;">
							<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate" ></span>
						</div>
					</xsl:if>

					<div class="download-list">
						<div class="download-list__inner">
							<h4 class="download-list__header">
								Скачайте список спикеров и их семинаров к себе на устройство в формате .pdf
							</h4>

							<div class="download-list__list">
								<div class="download-list__item">
									<a href="#" title="Краткая версия" class="pdf_speakers_short btn btn-primary btn_icon-download js--pdf-download-link">Все кратко</a>
								</div>
								<div class="download-list__item">
									<a href="#" title="Полная версия" class="pdf_speakers_full btn btn-primary btn_icon-download js--pdf-download-link">Все подробно</a>
								</div>
								<div class="download-list__item">
									<a href="#" title="Скачать выбранное (кратко)" class="pdf_speakers_selected btn btn-primary btn_icon-download js--pdf-download-link">Выбранное кратко</a>
								</div>
								<div class="download-list__item">
									<a href="#" title="Скачать выбранное (подробно)" class="pdf_speakers_selected_full btn btn-primary btn_icon-download js--pdf-download-link">Выбранное подробно</a>
								</div>
								<a class="help_ico" href="#helpModalDownloadForDealers" data-toggle="tooltip" data-placement="top" title="Скачайте на свое устройство в формате pdf краткую или подробную информацию о всех спикерах, либо отмечайте галочкой нужных спикеров и скачивайте краткую информацию только о них.">
									<i class="fa fa-question-circle " aria-hidden="true"></i>
								</a>
							</div>
						</div>
					</div>

					<div class="rows-equals-height" id="dealers-list">
						<xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalogPro//', $document-page-id, '/1000/1/2///?extProps=foto_spikera,uroven_meropriyatiya,tip_meropriyatiya,gorod_prozhivaniya,tematika_provodimyh_meropriyatij'))/udata/lines/item" mode="spec_dealers_speakers_row"/>
					</div>
				</div>

				<div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
					<xsl:apply-templates select="document('udata://menu/draw/spec_dealers')/udata" mode="right_menu" />
					<xsl:apply-templates select="$filters" mode="right_filters_spikers" />
				</div>
			</div>
		</div>

	</xsl:template>

	<xsl:template match="item" mode="spec_dealers_speakers_row" />
	<xsl:template match="item[position() mod 2 = 1]" mode="spec_dealers_speakers_row">
		<xsl:variable name="cur_pos" select="position() + 1" />
		<div class="row" >
			<xsl:apply-templates select="." mode="spec_dealers_speakers"/>
			<xsl:apply-templates select="../item[position() = $cur_pos]" mode="spec_dealers_speakers"/>
		</div>
	</xsl:template>

	<xsl:template match="item" mode="spec_dealers_speakers">
		<div class="col-sm-6 col-xs-12">
			<div class="card-item" data-href="{@link}">
				<div class="card-item__checkbox js--pdf-download-item" data-pdf-id="{@id}"></div>
				<div class="card-item__left">
					<a href="{@link}" >
						<img src="{.//property[@name='foto_spikera']/value}" class="img-responsive" alt="{text()}">
							<xsl:if test="not(.//property[@name='foto_spikera']/value) or .//property[@name='foto_spikera']/value=''">
								<xsl:attribute name="src">&empty-photo-speaker;</xsl:attribute>
							</xsl:if>
						</img>
					</a>
				</div>
				<div class="card-item__right">
					<h4 class="card-item__username">
						<a href="{@link}">
							<xsl:value-of select="text()" />
						</a>
					</h4>

					<ul class="metadata-list">
						<xsl:apply-templates select=".//property[@name='gorod_prozhivaniya']" mode="gorod_prozhivaniya" />
						<xsl:apply-templates select=".//property[@name='tip_meropriyatiya']" mode="tip_meropriyatiya" />
					</ul>
				</div>
				<hr class="card-item__hr" />
				<xsl:apply-templates select=".//property[@name='tematika_provodimyh_meropriyatij']" mode="tematika_provodimyh_meropriyatij" />
			</div>
		</div>
	</xsl:template>

	<!-- gorod_prozhivaniya -->
	<xsl:template match="property" mode="gorod_prozhivaniya">
		<li class="metadata-list__item">
			<i class="new-icon new-icon_home metadata-list__item-icon"></i>
			<span class="metadata-list__item-text">
				<!-- Город проживания:  -->
				<xsl:value-of select="value/item/@name" />
			</span>
		</li>
	</xsl:template>

	<!-- tip_meropriyatiya -->
	<xsl:template match="property" mode="tip_meropriyatiya">
		<li class="metadata-list__item">
			<i class="new-icon new-icon_user metadata-list__item-icon"></i>
			<span class="metadata-list__item-text">
				<xsl:apply-templates select="value/item" mode="tip_meropriyatiya" />
			</span>
		</li>
	</xsl:template>
	<xsl:template match="item" mode="tip_meropriyatiya">
		<xsl:if test="not(position()=1)">, </xsl:if>
		<xsl:value-of select="@name" />
	</xsl:template>

	<!-- stazh_raboty -->
	<xsl:template match="property" mode="stazh_raboty">
		<li class="metadata-list__item">
			<i class="new-icon new-icon_stazh metadata-list__item-icon"></i>
			<span class="metadata-list__item-text">
				Сотрудничество с Ormco: <xsl:value-of select="value" />
			</span>
		</li>
	</xsl:template>

	<!-- tematika_provodimyh_meropriyatij -->
	<xsl:template match="property" mode="tematika_provodimyh_meropriyatij">
		<ul class="types-list">
			<xsl:apply-templates select="value/item" mode="tematika_provodimyh_meropriyatij" />
		</ul>
	</xsl:template>
	<xsl:template match="item" mode="tematika_provodimyh_meropriyatij">
		<xsl:variable name="logo" select="document(concat('uobject://',@id,'.logo'))//value" />

		<li class="types-list__item">
			<img src="{$logo}" alt="{@name}" class="types-list__item-img" href="#helpModalDealerssFilter" data-toggle="tooltip" data-placement="top" data-html="true">
				<xsl:attribute name="title">
					<xsl:value-of select="@name" />
				</xsl:attribute>
			</img>
		</li>
	</xsl:template>

	<!-- uroven_meropriyatiya -->
	<xsl:template match="property" mode="uroven_meropriyatiya">
		<xsl:apply-templates select="value/item" mode="uroven_meropriyatiya" />

	</xsl:template>
	<xsl:template match="item" mode="uroven_meropriyatiya">
		<xsl:if test="not(position()=1)">, </xsl:if>
		<xsl:value-of select="substring-before(@name,' уровень')" />
	</xsl:template>

	<!-- filters_spikers -->
	<xsl:template match="udata" mode="filters_spikers">
		<xsl:param name="action"  />
		<div class="top-selectors js--filters" data-action="{$action}" data-type="speakers" data-category="{$document-page-id}">
			<xsl:apply-templates select=".//field[@data-type = 'relation' and @name='uroven_meropriyatiya']" mode="filters_spikers" />
			<xsl:apply-templates select=".//field[@data-type = 'relation' and @name='tematika_provodimyh_meropriyatij']" mode="filters_spikers" />
			<xsl:apply-templates select=".//field[@data-type = 'relation' and @name='gorod_prozhivaniya']" mode="filters_spikers" />

			<a href="#" class="reset_filter" onclick="document.location.href = '?'" data-toggle="tooltip" data-placement="top" title="Сбросить фильтры">
				<i class="fa fa-times-circle-o"  aria-hidden="true"></i>
			</a>
			<a class="help_ico" href="#helpModalDealerssFilter" data-toggle="tooltip" data-placement="top" data-html="true">
				<xsl:attribute name="title">Воспользуйтесь фильтрами&lt;br/&gt; для быстрого доступа к информации о спикерах Ormco.</xsl:attribute>

				<i class="fa fa-question-circle " aria-hidden="true"></i>
			</a>
		</div>
	</xsl:template>

	<xsl:template match="field[@data-type = 'relation']" mode="filters_spikers">
		<!-- <select id="{@name}" name="fields_filter[{@name}]" title="{@title}" class="selectpicker show-tick "> -->
		<select id="{@name}" name="filter[{@name}][]" title="{@title}" multiple="multiple" class="selectpicker multiple-custom">
			<xsl:choose>
				<xsl:when test="@name='uroven_meropriyatiya'">
					<xsl:attribute name="title">Уровень</xsl:attribute>
					<xsl:apply-templates select="." mode="filter_field_uroven_meropriyatiya" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="." mode="filter_field"/>
				</xsl:otherwise>
			</xsl:choose>
		</select>
	</xsl:template>


	<!-- right_filters_spikers -->
	<xsl:template match="udata" mode="right_filters_spikers" />
	<xsl:template match="udata[.//field[@name='tematika_provodimyh_meropriyatij']/item]" mode="right_filters_spikers">
		<div class="right-filters">
			<div class="right-filters__inner">
				<ul class="right-filters__list">
					<xsl:apply-templates select=".//field[@name='tematika_provodimyh_meropriyatij']/item" mode="right_filters_spikers" >
						<xsl:with-param name="logos" select="document('usel://logos_tematica')/udata" />
					</xsl:apply-templates>
				</ul>
				<div class="right-filters__button">
					<a href="#" title="" class="btn btn-primary btn_small js--filters-submit">
						Применить
					</a>
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="item" mode="right_filters_spikers">
		<xsl:param name="logos" />
		<xsl:variable name="curr_name" select="text()" />

		<li class="right-filters__item">
			<xsl:if test="@is-selected = '1'">
				<xsl:attribute name="class">right-filters__item active</xsl:attribute>
			</xsl:if>

			<a href="#" class="right-filters__item-link js--right-filters" data-index="{position()-1}" data-filter-value="{$curr_url}?filter[tip_meropriyatiya][]={text()}">
				<span class="right-filters__item-left">
					<img src="{$logos/item[@name = $curr_name]//property[@name='logo']/value}" style="max-width:65px;" alt="{text()}" class="right-filters__item-img" />
				</span>
				<span class="right-filters__item-right">
					<span class="right-filters__item-checkbox"></span>
					<span class="right-filters__item-label">
						<xsl:value-of select="text()" />
					</span>
				</span>
			</a>
		</li>
	</xsl:template>
</xsl:stylesheet>