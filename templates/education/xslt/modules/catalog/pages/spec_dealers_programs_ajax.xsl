<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:date="http://exslt.org/dates-and-times"
	xmlns:udt="http://umi-cms.ru/2007/UData/templates"
	xmlns:xlink="http://www.w3.org/TR/xlink"
	exclude-result-prefixes="xsl date udt xlink">
	
	<xsl:output encoding="utf-8" method="html" indent="yes" />
 
	<xsl:template match="/">
		<div id="filter_result">
			
			<xsl:choose>
				<xsl:when test="udata/lines/item">
					<xsl:apply-templates select="udata/lines/item" mode="spec_dealers_programs"/>
				</xsl:when>
				<xsl:otherwise>
					<div class="white-pnl clearfix dealer-event"><article>Результатов не найдено! Попробуйте изменить параметры фильтра</article></div>
				</xsl:otherwise>
			</xsl:choose>
			
	  </div>              
	</xsl:template>
	
	<xsl:template match="item" mode="spec_dealers_programs">
		<xsl:variable name="speaker_id" select="document(concat('udata://catalog/getPageIdByObject/', .//property[@name='speaker']/value/item[1]/@id))/udata" />
		<xsl:variable name="speaker" select="document(concat('upage://', $speaker_id))/udata" />


		<div class="col-sm-12 col-xs-12" tt="{.//property[@name='speaker']/value/item[1]/@id}">
			<div class="card-item program">
				<div class="card-item__checkbox js--pdf-download-item" data-pdf-id="{@id}"></div>
                <div class="card-item__left">
                    <a href="{$speaker/page/@link}" class="rhref" data-href="{$speaker/page/@link}">
                    	<img src="{$speaker//property[@name='foto_spikera']/value}" class="img-responsive" alt="{$speaker/page/name}">
                    		<xsl:if test="not($speaker//property[@name='foto_spikera']/value) or $speaker//property[@name='foto_spikera']/value=''"><xsl:attribute name="src">/templates/education/img/speakers/2.png</xsl:attribute></xsl:if>
                    	</img>
                    </a>
                </div>
                <div class="card-item__right">
                    <h3 class="card-item__title">
                        <a href="{@link}" class="rhref" data-href="{@link}">
                        	<xsl:value-of select="text()" />
                        </a>
                    </h3>

                    <h4 class="card-item__username">

                        <a href="{$speaker/page/@link}" class="rhref" data-href="{$speaker/page/@link}" ><xsl:value-of select="$speaker/page/name" disable-output-escaping="yes" /></a>
                    </h4>

                    <ul class="metadata-list metadata-list_inline">
                    	<xsl:if test=".//property[@name='uroven_meropriyatiya']/value">
                            <li class="metadata-list__item">
                                <i class="new-icon new-icon_hat metadata-list__item-icon"></i>
                                <span class="metadata-list__item-text">
                                	<xsl:apply-templates select=".//property[@name='uroven_meropriyatiya']/value/item" mode="uroven_meropriyatiya" />
                                </span>
                            </li>
                        </xsl:if>
                        <xsl:if test=".//property[@name='tip_meropriyatiya']/value">
                            <li class="metadata-list__item">
                                <i class="new-icon new-icon_user metadata-list__item-icon"></i>
                                <span class="metadata-list__item-text">
                                    <xsl:apply-templates select=".//property[@name='tip_meropriyatiya']/value/item" mode="tip_meropriyatiya" />
                                    <!-- <xsl:value-of select=".//property[@name='tip_meropriyatiya']/value/item/@name"  /> -->
                                </span>
                            </li>
                        </xsl:if>
                        <xsl:if test=".//property[@name='kolvo_dnej']/value">
                            <li class="metadata-list__item">
                                <i class="new-icon new-icon_calendar metadata-list__item-icon"></i>
                                <span class="metadata-list__item-text">
                                    <!-- Кол-во дней:  --><xsl:value-of select=".//property[@name='kolvo_dnej']/value/item/@name" disable-output-escaping="yes" />
                                </span>
                            </li>
                        </xsl:if>
                        
                        <xsl:if test=".//property[@name='seminar_na_kafedre']/value/item/@name='Да'">
                            <li class="metadata-list__item">
                                <i class="new-icon new-icon_small new-icon_speaker  metadata-list__item-icon"></i>
                                
                            </li>
                        </xsl:if>
                           
                          <li class="hidden-xs metadata-list__item right_side">
														<a href="{@link}" title="" class="btn btn-blue rhref" data-href="{@link}">
											
						                    Подробнее
						                </a>
                          </li>
                          <li class="hidden-xs metadata-list__item right_side">
														<xsl:apply-templates select=".//property[@name='tematika_provodimyh_meropriyatij']" mode="tematika_provodimyh_meropriyatij" />
                          </li>
                    </ul>
                </div>
                <div class="hidden-sm hidden-md hidden-lg">
                	<hr class="card-item__hr" />
	                <xsl:apply-templates select=".//property[@name='tematika_provodimyh_meropriyatij']" mode="tematika_provodimyh_meropriyatij" />
	
	                <a href="{@link}" title="" class="btn btn-blue card-item__more rhref" data-href="{@link}">
	                    Подробнее
	                </a>
                </div>
                
                <!-- <hr class="card-item__hr" />
                <xsl:apply-templates select=".//property[@name='tematika_provodimyh_meropriyatij']" mode="tematika_provodimyh_meropriyatij" />

                <a href="{@link}" title="" class="rhref btn btn-blue card-item_ _more" data-href="{@link}">
                    Подробнее
                </a> -->
            </div>
        </div>
	</xsl:template>
	
	<!-- gorod_prozhivaniya -->
	<xsl:template match="property" mode="gorod_prozhivaniya">
		<li class="metadata-list__item">
            <i class="new-icon new-icon_home metadata-list__item-icon"></i>
            <span class="metadata-list__item-text">
                Город проживания: <xsl:value-of select="value/item/@name" />
            </span>
        </li>
	</xsl:template>

	<!-- tip_meropriyatiya -->
	<xsl:template match="property" mode="tip_meropriyatiya">
		<li class="metadata-list__item">
            <i class="new-icon new-icon_user metadata-list__item-icon"></i>
            <span class="metadata-list__item-text">
                <xsl:apply-templates select="value/item" mode="tip_meropriyatiya" />
            </span>
        </li>
	</xsl:template>
	<xsl:template match="item" mode="tip_meropriyatiya">
		<xsl:if test="not(position()=1)">, </xsl:if><xsl:value-of select="@name" />
	</xsl:template>
	
	<!-- uroven_meropriyatiya -->
	<xsl:template match="property" mode="uroven_meropriyatiya">
		<xsl:apply-templates select="value/item" mode="uroven_meropriyatiya" />

	</xsl:template>
	<xsl:template match="item" mode="uroven_meropriyatiya">
		<xsl:if test="not(position()=1)">, </xsl:if><xsl:value-of select="substring-before(@name,' уровень')" />
	</xsl:template>

	<!-- tematika_provodimyh_meropriyatij -->
	<xsl:template match="property" mode="tematika_provodimyh_meropriyatij">
		<ul class="types-list">
			<xsl:apply-templates select="value/item" mode="tematika_provodimyh_meropriyatij" />
        </ul>
	</xsl:template>
	<xsl:template match="item" mode="tematika_provodimyh_meropriyatij">
        <xsl:variable name="logo" select="document(concat('uobject://',@id,'.logo'))//value" />


        <li class="types-list__item">
            <img src="{$logo}" alt="{@name}" class="types-list__item-img" data-toggle="tooltip" data-placement="top" data-html="true">
                <xsl:attribute name="title"><xsl:value-of select="@name" /></xsl:attribute>
            </img>
        </li>
	</xsl:template>

	

</xsl:stylesheet>
