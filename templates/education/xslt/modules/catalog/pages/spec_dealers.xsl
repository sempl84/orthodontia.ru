<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'catalog' and @method='category' and page/@id = &spec_dealers_pid;]">
		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />
		
		<!--Content -->
	    <div class="container-fluid dealer-page">
	        <div class="row">
	            <div class="content-col col-md-10 col-md-offset-1 col-xs-12">
	                <div class="row">
	                    <div class="col-lg-5 col-lg-offset-1 col-md-6 col-sm-6 col-xs-12">
	                        <a href="{document('upage://&spec_dealers_speakers_pid;')/udata/page/@link}" title="Выберите спикера" class="dealer-choose">
	                            <i class="new-icon new-icon_xlarge new-icon_speaker dealer-choose__icon"></i>
	                            <h3 class="dealer-choose__title">Выберите спикера</h3>
	                            <span class="btn btn-blue dealer-choose__link">
	                                Подробнее
	                            </span>
	                        </a>
	                    </div>
	                    <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">
	                        <a href="{document('upage://&spec_dealers_programs_pid;')/udata/page/@link}" title="Выберите программу" class="dealer-choose">
	                            <i class="new-icon new-icon_xlarge new-icon_calendar-big dealer-choose__icon"></i>
	                            <h3 class="dealer-choose__title">Выберите программу</h3>
	                            <span class="btn btn-blue dealer-choose__link">
	                                Подробнее
	                            </span>
	                        </a>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    
	</xsl:template>
	

</xsl:stylesheet>