<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">
	<xsl:template match="result[@pageId = &consultation_pid;]">
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">Меню раздела</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
		                <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

		<!--Content -->
	    <div class="container-fluid events-page">
	        <div class="page-header">
	            <div class="row">
	                <div class="col-xs-12">
	                    <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
	                    <h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
	                </div>
	            </div>
	        </div>
	        <div class="row">
	            <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
	                <xsl:apply-templates select="document(concat('udata://catalog/getSmartFilters//', $document-page-id, '/0/100'))/udata" mode="search" />
	                <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalogPro//', $document-page-id, '///2/publish_date/1/?extProps=speaker,photo,gorod_in,price_string,about_reg,tematika_konsultacii'))/udata" mode="consultant" />
	            </div>
	            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
	                <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
	                <xsl:call-template name="calendar" />
	                <xsl:apply-templates select="." mode="right_col" />
	            </div>
	        </div>

			<xsl:call-template name="purchase_consultation_form" />
	    </div>
	</xsl:template>


	<xsl:template match="udata" mode="consultant">
		<div class="white-pnl clearfix">Актуальных консультаций не найдено.</div>
	</xsl:template>
	<xsl:template match="udata[lines/item]" mode="consultant">
		<div class="items row consultation_items ga_items_list">
			<xsl:apply-templates select="lines/item" mode="consultant" />
		</div>
		<xsl:apply-templates select="total" />
	</xsl:template>


	<xsl:template match="item" mode="consultant">
		<xsl:variable name="speaker_photo">
			<xsl:choose>
				<xsl:when test=".//property[@name = 'photo']/value">
					<xsl:value-of select=".//property[@name = 'photo']/value" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="document(concat('uobject://', .//property[@name = 'speaker']/value/item/@id, '.photo'))//value" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<div class="item col-xs-12 col-md-6 ga_items_list_item" data-position="{position()}" data-name="{.//property[@name = 'speaker']/value/item/@name}" data-id="{@id}" data-price="{.//property[@name = 'price_string']/value}" data-event_category="Консультация">
			<div class="white-pnl clearfix block_round_height">
				<div class="sub_block_area">
					<div class="row">
						<div class="col-xs-3 no-padding-right">
							<img src="{$speaker_photo}" class="img-responsive img-circle" alt="{.//property[@name = 'speaker']/value/item/@name}">
								<xsl:if test="not($speaker_photo) or $speaker_photo = ''">
									<xsl:attribute name="src">&empty-photo-speaker;</xsl:attribute>
								</xsl:if>
							</img>
						</div>
						<div class="col-xs-9">
							<h4>
								<a href="{@link}">
									<xsl:value-of select=".//property[@name = 'speaker']/value/item/@name" disable-output-escaping="yes" />
								</a>
							</h4>
							<xsl:apply-templates select=".//property[@name = 'gorod_in']/value" mode="consultation_city" />
							<xsl:apply-templates select=".//property[@name = 'tematika_konsultacii']/value/item" mode="consultation_type" />
						</div>
					</div>

                    <xsl:if test=".//property[@name = 'about_reg']/value">
                        <div class="row">
                            <div class="col-xs-12">
                                Контакты:<br />
                                <xsl:value-of select=".//property[@name = 'about_reg']/value" disable-output-escaping="yes" />
                            </div>
                        </div>
                    </xsl:if>
				</div>

				<div class="row">
					<div class="col-xs-12 buttons">
						<span class="btn btn-blue">
							от <xsl:value-of select=".//property[@name = 'price_string']/value" disable-output-escaping="yes" /> руб.
						</span>
						<xsl:choose>
							<xsl:when test="$user-type = 'guest'">
								<a href="#buyModal" data-toggle="modal" data-target="#buyModal" class="btn btn-green ga_item_add_basket" data-name="{.//property[@name = 'speaker']/value/item/@name}" data-id="{@id}" data-price="{.//property[@name = 'price_string']/value}" data-event_category="Консультация">Записаться</a>
							</xsl:when>
							<xsl:otherwise>
								<a href="#purchase_consultation" data-consultation_id="{@id}" data-toggle="modal" data-target="#purchase_consultation_form" class="btn btn-green consultation_button ga_item_add_basket" data-name="{.//property[@name = 'speaker']/value/item/@name}" data-id="{@id}" data-price="{.//property[@name = 'price_string']/value}" data-event_category="Консультация">Записаться</a>
							</xsl:otherwise>
						</xsl:choose>
					</div>
				</div>
			</div>
		</div>
	</xsl:template>


    <xsl:template match="item" mode="consultation_type">
        <span class="seminar-type-link css-transparent">
			<xsl:value-of select="@name" />
        </span>
    </xsl:template>


    <xsl:template match="value" mode="consultation_city">
        <p class="location">
			<i class="fa fa-map-pin" aria-hidden="true"></i>
			<xsl:apply-templates select="item" mode="consultation_city" />
        </p>
    </xsl:template>
    <xsl:template match="item" mode="consultation_city">
		<xsl:if test="position() &gt; 1">
			<xsl:text>, </xsl:text>
		</xsl:if>
		<xsl:value-of select="@name" />
    </xsl:template>


	<xsl:template match="result[@module = 'catalog' and @method='object' and parents/page/@id = &consultation_pid;]">
        <xsl:variable name="speaker_photo">
            <xsl:choose>
                <xsl:when test=".//property[@name='photo']/value">
                    <xsl:value-of select=".//property[@name='photo']/value" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="document(concat('uobject://', .//property[@name='speaker']/value/item/@id,'.photo'))//value" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
	                    <xsl:apply-templates select="document('udata://menu/draw/right_menu_events')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

        <!--Breadcrumbs -->
        <xsl:apply-templates select="document('udata://core/navibar/')/udata" />

        <!--Content -->
        <div class="container-fluid event-page" data-name="{@header}" data-position="{position()}">
            <div class="page-header">
                <div class="row">
                    <div class="col-xs-12">
                        <h1><xsl:value-of select=".//property[@name='h1']/value" disable-output-escaping="yes" /></h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12 ga_item" data-id="{page/@id}" data-list="{/result//parents/page[position() = last()]/name}" data-name="{.//property[@name='speaker']/value/item/@name}" data-price="{.//property[@name = 'price_string']/value}" data-event_category="Консультации">
                    <div class="white-pnl preview">
						<h4>
							<xsl:apply-templates select=".//property[@name='speaker']/value" mode="speaker_into_event" />
						</h4>

						<div>
							<xsl:apply-templates select=".//property[@name = 'gorod_in']/value" mode="consultation_city" />
						</div>

						<xsl:apply-templates select=".//property[@name = 'tematika_konsultacii']/value/item" mode="consultation_type" />

						<div class="content_wrap" umi:element-id="{@id}" umi:field-name="description" umi:empty="&empty-page-content;">
							<xsl:value-of select=".//property[@name = 'description']/value" disable-output-escaping="yes" />
						</div>

						<hr />

						<div class="buttons text-right">
							<span class="btn btn-blue">
								от <xsl:value-of select=".//property[@name = 'price_string']/value" disable-output-escaping="yes" /> руб.
							</span>

							<xsl:choose>
								<xsl:when test="$user-type = 'guest'">
									<a href="#buyModal" data-toggle="modal" data-target="#buyModal" class="btn btn-green ga_item_add_basket" data-name="{.//property[@name='speaker']/value/item/@name}" data-id="{page/@id}" data-price="{.//property[@name = 'price_string']/value}" data-event_category="Консультации">Записаться</a>
								</xsl:when>
								<xsl:otherwise>
									<a href="#purchase_consultation" data-consultation_id="{$document-page-id}" data-toggle="modal" data-target="#purchase_consultation_form" class="btn btn-green consultation_button ga_item_add_basket" data-name="{.//property[@name='speaker']/value/item/@name}" data-id="{page/@id}" data-price="{.//property[@name = 'price_string']/value}" data-event_category="Консультации">Записаться</a>
								</xsl:otherwise>
							</xsl:choose>
						</div>

						<div class="socials-pnl">
							Поделиться:
							<div class="ya-share2" data-services="vkontakte,odnoklassniki,twitter" data-size="s"></div>
						</div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
                    <xsl:apply-templates select="document('udata://menu/draw/right_menu_events')/udata" mode="right_menu" />

                    <!-- calendar -->
                    <xsl:call-template name="calendar">
                        <xsl:with-param name="parent_id" select="page/@parentId" />
                    </xsl:call-template>
                    <xsl:apply-templates select="." mode="right_col" />
                </div>
            </div>

			<xsl:call-template name="purchase_consultation_form" />
        </div>
    </xsl:template>


	<xsl:template name="purchase_consultation_form">
		<div class="modal fade" id="purchase_consultation_form" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form class="buyOneClick" action="/emarket/submit_consultation_order_form/" method="post" enctype="multipart/form-data">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<i class="fa fa-times" aria-hidden="true"></i>
							</button>
							<h4 class="modal-title" id="gridAuth">Запись на консультацию</h4>
						</div>
						<div class="modal-body">
							<input type="hidden" class="consultation_id" name="page_id" value="" />
							<xsl:apply-templates select="document('udata://data/getCreateForm/&order-tid;//(form_params)(dannye_dlya_konsultacii)')" mode="simple_form_order"/>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-green pull-left buyOneClickButton">Записаться</button>
						</div>
					</form>
				</div>
			</div>
		</div>
    </xsl:template>


	<xsl:template match="result[@method = 'submit_consultation_order_form']">
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">Меню раздела</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
						<xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

		<!--Content -->
		<div class="container-fluid events-page">
			<div class="page-header">
				<div class="row">
					<div class="col-xs-12">
						<i class="fa fa-calendar-check-o" aria-hidden="true"></i>
						<h1>
							Запись на консультацию
						</h1>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
					<xsl:choose>
						<xsl:when test="udata/is_error = 'true'">
							<p>В процессе записи произошли следующие ошибки:</p>
							<p class="errors">
								<ul>
									<li>
										<xsl:value-of select="udata/error_message" disable-output-escaping="yes" />
									</li>
								</ul>
							</p>
							<p>
								<a href="{udata/referer}">Вернуться назад</a>
							</p>
						</xsl:when>
						<xsl:otherwise>
                            <p>Вы успешно записались на консультацию!</p>
                            <p>Ваш запрос уже отправлен эксперту.</p>
                            <p>В ближайшее время эксперт свяжется с Вами для уточнения времени консультации и других деталей.</p>
                            <p>А сейчас Вы можете посмотреть <a href="/events/">расписание запланированных семинаров</a>.</p>
						</xsl:otherwise>
					</xsl:choose>
				</div>
				<div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
					<xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
					<xsl:call-template name="calendar" />
					<xsl:apply-templates select="." mode="right_col" />
				</div>
			</div>
		</div>
    </xsl:template>
</xsl:stylesheet>