<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi"
	exclude-result-prefixes="umi">

    <xsl:template match="result[@module = 'catalog' and @method='object']">
        <xsl:variable name="date_begin" select=".//property[@name = 'publish_date']/value/@unix-timestamp" />
        <xsl:variable name="date_end" select=".//property[@name = 'finish_date']/value/@unix-timestamp" />
        <xsl:variable name="dateru" select="document(concat('udata://data/dateruPeriod/', $date_begin, '/', $date_end))/udata" />
        <xsl:variable name="max_reserv" select="number(.//property[@name = 'max_reserv']/value)" />
        <xsl:variable name="curr_reserv" select="number(.//property[@name = 'curr_reserv']/value)" />
        <xsl:variable name="reserv" select="$max_reserv - $curr_reserv" />
        <xsl:variable name="showDiscountByDate" select="document(concat('udata://emarket/showDiscountByDate/',page/@id))/udata" />
        <xsl:variable name="isFirstPurchase" select="document(concat('udata://emarket/checkSecondPurchase/',page/@id,'/',$user-info//property[@name='e-mail']/value))/udata" />
        <xsl:variable name="loyalty" select="document(concat('udata://custom/outputEventLoyalty/',page/@id))/udata" />

        <xsl:variable name="currency-suffix">
            <xsl:choose>
                <xsl:when test=".//property[@name='vybrana_drugaya_valyuta']/value"><xsl:value-of select="document(concat('uobject://', .//property[@name='vybrana_drugaya_valyuta']/value/item/@id, '.postfiks_valyuty'))//value" /></xsl:when>
                <xsl:otherwise><xsl:value-of select="$cart/summary/price/@suffix" /></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="speaker_photo">
            <xsl:choose>
                <xsl:when test=".//property[@name='photo']/value">
                    <xsl:value-of select=".//property[@name='photo']/value" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="document(concat('uobject://', .//property[@name='speaker']/value/item/@id,'.photo'))//value" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <!-- ga params -->
        <xsl:variable name="ga_price">
            <xsl:choose>
                <xsl:when test="contains(.//property[@name='event_url_redirect']/value, 'orthodontia.ru') ">Лендинг</xsl:when>
                <xsl:when test="($showDiscountByDate &gt; 0 and $showDiscountByDate &gt; 0) or .//property[@name='price']/value &gt; 0">Платное</xsl:when>
                <xsl:otherwise>Бесплатное</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="ga_price_float">
            <xsl:choose>
                <xsl:when test="$showDiscountByDate &gt; 0 and $showDiscountByDate &gt; 0">
                    <xsl:value-of select="$showDiscountByDate" />
                </xsl:when>
                <xsl:when test=".//property[@name='price']/value &gt; 0">
                    <xsl:value-of select=".//property[@name='price']/value" />
                </xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="ga_list">
            <xsl:choose>
                <xsl:when test="contains(.//property[@name='event_url_redirect']/value, 'orthodontia.ru')">Лендинг</xsl:when>
                <xsl:when test="$parents/page/@id = &all_event_pid; or $document-page-id = &all_event_pid;">Все мероприятия</xsl:when>
                <xsl:when test="$parents/page/@id = &ormco_club_pid; or $document-page-id = &ormco_club_pid;">Клуб Ormco</xsl:when>
                <xsl:when test="$is_default_page = 1">Главная страница</xsl:when>
            </xsl:choose>
        </xsl:variable>

		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
	                    <xsl:apply-templates select="document('udata://menu/draw/right_menu_events')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

        <!--Breadcrumbs -->
        <xsl:apply-templates select="document('udata://core/navibar/')/udata" />

        <!--Content -->
        <div class="container-fluid event-page ga_addProduct ga_item" data-name="{@header}" data-category="{$ga_price}" data-list="{$ga_list}" data-position="{position()}" data-price="{$ga_price_float}" data-id="{page/@id}" data-event_category="{.//property[@name='event_type']/value/item/@name}">
            <div class="page-header">
                <div class="row">
                    <div class="col-xs-12">
                        <h1>
                            <xsl:value-of select="@header" disable-output-escaping="yes" />
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <div class="white-pnl preview">
                        <xsl:if test="$loyalty/add_loyalty/@potential_bonus">
                            <a
                                href="?link_potential_bonus_infoж"
                                class="event-points-info"
                                data-toggle="tooltip"
                                data-placement="top"
                                target="_blank"
                                title="Эти звезды вы можете получить за участие в мероприятии"
                            >
                                <i class="fa fa-star event-points-info__icon" aria-hidden="true"></i>
                                <span class="event-points-info__count">
                                    <xsl:value-of select="$loyalty/add_loyalty/@potential_bonus" /> звезд</span>
                            </a>
                        </xsl:if>

                        <xsl:if test=".//property[@name='akkreditaciya_star']/value">
                            <span class="akkr_info"></span>
                        </xsl:if>

                        <xsl:apply-templates select=".//property[@name='speaker']/value" mode="speaker_into_event" />

                        <span class="date">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            <xsl:value-of select="$dateru/first_line" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="$dateru/second_line" />
                            <xsl:text> (</xsl:text>
                            <xsl:value-of select="$dateru/days" />
                            <xsl:text>)</xsl:text>
                        </span>

                        <xsl:apply-templates select=".//property[@name='mesto_provedeniya']/value" mode="mesto_provedeniya" />
                        <xsl:apply-templates select=".//property[@name='event_type']/value" mode="event_type" />
                        <xsl:apply-templates select=".//property[@name='level']/value" mode="level" />
                        <xsl:apply-templates select=".//property[@name='organizer']/value" mode="organizer" />
	                    <xsl:apply-templates select=".//property[@name='uchebnyj_centr']" mode="uchebnyj_centr" />
                        <hr />
                        <div class="row">
                            <div class="col-sm-5 col-xs-5 registration-info">
                                <xsl:apply-templates select=".//group[@name='information' and property]" mode="information" />
                            </div>
                            <div class="col-sm-7 col-xs-7 btns">
                                <xsl:choose>
                                    <xsl:when test=".//property[@name='price_origin']/value &gt; 0">
                                        <span class="btn old-price">
                                            <xsl:value-of select="concat(format-number(.//property[@name='price_origin']/value, '#&#160;###','price'), ' ', $currency-suffix)" />
                                        </span>

                                        <!-- нужно ли выводить скидку лояльности -->
                                        <xsl:choose>
                                            <xsl:when test="$loyalty/discount_loyalty/@isallow &gt; 0">
                                                <div class="btn-group">
                                                    <span class="btn btn-blue" data-ga-no-event="true">
                                                        <xsl:value-of select="concat(format-number(.//property[@name='price']/value, '#&#160;###','price'), ' ', $currency-suffix)" />
                                                    </span>
                                                    <xsl:call-template name="discount_loyalty" />
                                                </div>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <span class="btn btn-blue" data-ga-no-event="true">
                                                    <xsl:value-of select="concat(format-number(.//property[@name='price']/value, '#&#160;###','price'), ' ', $currency-suffix)" />
                                                </span>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:when>
                                    <xsl:when test="$showDiscountByDate &gt; 0">
                                        <span class="btn old-price" data-ga-no-event="true">
                                            <xsl:value-of select="concat(format-number(.//property[@name='price']/value, '#&#160;###','price'), ' ', $currency-suffix)" />
                                        </span>

                                        <!-- нужно ли выводить скидку лояльности -->
                                        <xsl:choose>
                                            <xsl:when test="$loyalty/discount_loyalty/@isallow &gt; 0">
                                                <div class="btn-group">
                                                    <span class="btn btn-blue" data-ga-no-event="true">
                                                        <xsl:value-of select="concat(format-number($showDiscountByDate, '#&#160;###','price'), ' ', $currency-suffix)" />
                                                    </span>
                                                    <xsl:call-template name="discount_loyalty" />
                                                </div>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <span class="btn btn-blue" data-ga-no-event="true">
                                                    <xsl:value-of select="concat(format-number($showDiscountByDate, '#&#160;###','price'), ' ', $currency-suffix)" />
                                                </span>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <!-- нужно ли выводить скидку лояльности -->
                                        <xsl:choose>
                                            <xsl:when test="$loyalty/discount_loyalty/@isallow &gt; 0">
                                                <div class="btn-group">
                                                    <xsl:apply-templates select="document(concat('udata://emarket/price/', $document-page-id))/udata">
                                                        <xsl:with-param name="currency-suffix" select="$currency-suffix" />
                                                    </xsl:apply-templates>
                                                    <xsl:call-template name="discount_loyalty" />
                                                </div>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:apply-templates select="document(concat('udata://emarket/price/', $document-page-id))/udata">
                                                    <xsl:with-param name="currency-suffix" select="$currency-suffix" />
                                                </xsl:apply-templates>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:otherwise>
                                </xsl:choose>

                                <!-- Показываем цену и кнопку регистрации только если мы в разделе все мероприятия или в разделе клуб ормко -->
                                <xsl:choose>
                                    <xsl:when test="$stub_vds=1">
                                        <a href="#stub_vds_modal" data-toggle="modal" data-target="#stub_vds_modal" class="btn btn-green buy-button buy-button-event ga_item_add_basket" data-name="{@header}" data-id="{page/@id}" data-price="{$ga_price_float}"  data-event_category="{.//property[@name='event_type']/value/item/@name}">Зарегистрироваться</a>
                                    </xsl:when>
                                    <xsl:when test="$e=7">
                                        <a href="#buyModal" data-page_id="{$document-page-id}" data-toggle="modal" data-target="#buyModal" class="btn btn-green buy-button buy-button-event ga_item_add_basket" data-name="{@header}" data-id="{page/@id}" data-price="{$ga_price_float}" data-event_category="{.//property[@name='event_type']/value/item/@name}">Зарегистрироваться</a>
                                    </xsl:when>
                                    <!-- есть галочка "прячем кнопку"" -->
                                    <xsl:when test=".//property[@name='hide_reg_button']/value"></xsl:when>
                                    <!-- нет мест прячем кнопку" -->
                                    <xsl:when test="$max_reserv &gt; 0 and  $curr_reserv &gt; 0 and not($reserv &gt; 0)"></xsl:when>
                                    <!-- ШОО и есть ссылка то делаем кнопку редиректа" -->
                                    <xsl:when test=".//property[@name='event_url_redirect']/value">
                                        <a href="{.//property[@name='event_url_redirect']/value}" class="btn btn-green buy-button buy-button-event ga_item_add_basket" data-name="{@header}" data-id="{page/@id}" data-price="{$ga_price_float}" data-event_category="{.//property[@name='event_type']/value/item/@name}">Перейти к регистрации</a>
                                    </xsl:when>

                                    <!-- отображать сообщение, если человек уже регистрировался на данное мероприятие -->
                                    <xsl:when test="$isFirstPurchase = 0 and ($parents/page/@id = &ormco_club_pid; or $document-page-id = &ormco_club_pid; or $parents/page/@id = &all_event_pid; or $document-page-id = &all_event_pid;)">
                                        <a href="#buyModalSecondPurchase" data-page_id="{$document-page-id}" data-toggle="modal" data-target="#buyModalSecondPurchase" class="btn btn-green buy-button buy-button-event ga_item_add_basket" data-name="{@header}" data-id="{page/@id}" data-price="{$ga_price_float}" data-event_category="{.//property[@name='event_type']/value/item/@name}">Зарегистрироваться</a>
                                    </xsl:when>

                                    <xsl:when test="$parents/page/@id = &ormco_club_pid; or $document-page-id = &ormco_club_pid; or $parents/page/@id = &all_event_pid; or $document-page-id = &all_event_pid;">
                                        <a href="#buyModal" data-page_id="{$document-page-id}" data-toggle="modal" data-target="#buyModal" class="btn btn-green buy-button buy-button-event ga_item_add_basket" data-name="{@header}" data-id="{page/@id}" data-price="{$ga_price_float}" data-event_category="{.//property[@name='event_type']/value/item/@name}">Зарегистрироваться</a>
                                    </xsl:when>
                                </xsl:choose>

                                <xsl:choose>
                                    <xsl:when test="$max_reserv &gt; 0 and  not($curr_reserv)">
                                        <span class="reserv hidden-xs0">Места еще есть</span>
                                    </xsl:when>
                                    <xsl:when test="$max_reserv &gt; 0 and  $curr_reserv &gt; 0 and $reserv &gt; 0">
                                        <span class="reserv hidden-xs0">Места еще есть</span>
                                    </xsl:when>
                                    <xsl:when test="$max_reserv &gt; 0 and  $curr_reserv &gt; 0 and not($reserv &gt; 0)">
                                        <span class="reserv hidden-xs0">Мест нет</span>
                                    </xsl:when>
                                </xsl:choose>
                            </div>
                        </div>
                    </div>
                    <xsl:apply-templates select=".//property[@name='speaker']/value/item" mode="speaker_block" />

                    <div class="white-pnl clearfix">
                        <article>
                            <div class="content_wrap" umi:element-id="{@id}" umi:field-name="description" umi:empty="&empty-page-content;">
                                <xsl:value-of select=".//property[@name = 'description']/value" disable-output-escaping="yes" />
                            </div>

                            <div class="row">
                                <div class="col-xs-12 btns">
                                    <!-- Показываем цену и кнопку регистрации только если мы в разделе все мероприятия или в разделе клуб ормко -->
                                    <xsl:choose>
                                        <xsl:when test=".//property[@name='price_origin']/value &gt; 0">
                                            <span class="btn old-price">
                                                <xsl:value-of select="concat(format-number(.//property[@name='price_origin']/value, '#&#160;###','price'), ' ', $currency-suffix)" />
                                            </span>

                                            <!-- нужно ли выводить скидку лояльности -->
                                            <xsl:choose>
                                                <xsl:when test="$loyalty/discount_loyalty/@isallow &gt; 0">
                                                    <div class="btn-group event-bottom-group">
                                                        <span class="btn btn-blue" data-ga-no-event="true">
                                                            <xsl:value-of select="concat(format-number(.//property[@name='price']/value, '#&#160;###','price'), ' ', $currency-suffix)" />
                                                        </span>
                                                        <xsl:call-template name="discount_loyalty" />
                                                    </div>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <span class="btn btn-blue" data-ga-no-event="true">
                                                        <xsl:value-of select="concat(format-number(.//property[@name='price']/value, '#&#160;###','price'), ' ', $currency-suffix)" />
                                                    </span>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:when>
                                        <xsl:when test="$showDiscountByDate &gt; 0">
                                            <span class="btn old-price">
                                                <xsl:value-of select="concat(format-number(.//property[@name='price']/value, '#&#160;###','price'), ' ', $currency-suffix)" />
                                            </span>

                                            <!-- нужно ли выводить скидку лояльности -->
                                            <xsl:choose>
                                                <xsl:when test="$loyalty/discount_loyalty/@isallow &gt; 0">
                                                    <div class="btn-group event-bottom-group">
                                                        <span class="btn btn-blue" data-ga-no-event="true">
                                                            <xsl:value-of select="concat(format-number($showDiscountByDate, '#&#160;###','price'), ' ', $currency-suffix)" />
                                                        </span>
                                                        <xsl:call-template name="discount_loyalty" />
                                                    </div>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <span class="btn btn-blue" data-ga-no-event="true">
                                                        <xsl:value-of select="concat(format-number($showDiscountByDate, '#&#160;###','price'), ' ', $currency-suffix)" />
                                                    </span>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <!-- нужно ли выводить скидку лояльности -->
                                            <xsl:choose>
                                                <xsl:when test="$loyalty/discount_loyalty/@isallow &gt; 0">
                                                    <div class="btn-group event-bottom-group">
                                                        <xsl:apply-templates select="document(concat('udata://emarket/price/', $document-page-id))/udata">
                                                            <xsl:with-param name="currency-suffix" select="$currency-suffix" />
                                                        </xsl:apply-templates>
                                                        <xsl:call-template name="discount_loyalty" />
                                                    </div>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:apply-templates select="document(concat('udata://emarket/price/', $document-page-id))/udata">
                                                        <xsl:with-param name="currency-suffix" select="$currency-suffix" />
                                                    </xsl:apply-templates>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:otherwise>
                                    </xsl:choose>

                                    <xsl:choose>
                                        <xsl:when test="$stub_vds=1">
                                            <a href="#stub_vds_modal" data-toggle="modal" data-target="#stub_vds_modal"  class="btn btn-green buy-button buy-button-event">Зарегистрироваться</a>
                                        </xsl:when>
                                        <!-- есть галочка "прячем кнопку"" -->
                                        <xsl:when test=".//property[@name='hide_reg_button']/value"></xsl:when>
                                        <!-- нет мест прячем кнопку" -->
                                        <xsl:when test="$max_reserv &gt; 0 and  $curr_reserv &gt; 0 and not($reserv &gt; 0)"></xsl:when>
                                        <!-- есть ссылка то делаем кнопку редиректа" -->
                                        <xsl:when test=".//property[@name='event_url_redirect']/value">
                                            <a href="{.//property[@name='event_url_redirect']/value}" class="btn btn-green buy-button buy-button-event">Перейти к регистрации</a>
                                        </xsl:when>
                                        <!-- отображать сообщение, если человек уже регистрировался на данное мероприятие -->
                                        <xsl:when test="$isFirstPurchase = 0 and ($parents/page/@id = &ormco_club_pid; or $document-page-id = &ormco_club_pid; or $parents/page/@id = &all_event_pid; or $document-page-id = &all_event_pid;)">
                                            <a href="#buyModalSecondPurchase" data-page_id="{$document-page-id}" data-toggle="modal" data-target="#buyModalSecondPurchase" class="btn btn-green buy-button buy-button-event">Зарегистрироваться</a>
                                        </xsl:when>
                                        <xsl:when test="$parents/page/@id = &ormco_club_pid; or $document-page-id = &ormco_club_pid; or $parents/page/@id = &all_event_pid; or $document-page-id = &all_event_pid;">
                                            <a href="#buyModal" data-page_id="{$document-page-id}" data-toggle="modal" data-target="#buyModal" class="btn btn-green buy-button buy-button-event">Зарегистрироваться</a>
                                        </xsl:when>
                                    </xsl:choose>
                                    <xsl:choose>
                                        <xsl:when test="$max_reserv &gt; 0 and  not($curr_reserv)">
                                            <span class="reserv hidden-xs0">Места еще есть</span>
                                        </xsl:when>
                                        <xsl:when test="$max_reserv &gt; 0 and  $curr_reserv &gt; 0 and $reserv &gt; 0">
                                            <span class="reserv hidden-xs0">Места еще есть</span>
                                        </xsl:when>
                                        <xsl:when test="$max_reserv &gt; 0 and  $curr_reserv &gt; 0 and not($reserv &gt; 0)">
                                            <span class="reserv hidden-xs0">Мест нет</span>
                                        </xsl:when>
                                    </xsl:choose>
                                </div>
                            </div>

                            <!-- Файлы -->
                            <xsl:apply-templates select=".//group[@name='files_arr' and property[not(contains(@name,'name'))]/value]" mode="event_files_arr" />

                            <!-- слайдер с текущей страницы  -->
                            <xsl:apply-templates select=".//property[@name='event_photos' and value]" mode="event_photos" />

                            <div class="socials-pnl">
                                Поделиться:
                                <div class="ya-share2" data-services="vkontakte,odnoklassniki,twitter" data-size="s"></div>
                            </div>
                        </article>
                    </div>

                    <!-- слайдер из прикрепленного альбома  -->
                    <xsl:apply-templates select="document(concat('udata://photoalbum/album/',.//property[@name='event_album']/value/page/@id, '?extProps=photo,descr'))/udata" mode="event_album"/>

					<xsl:apply-templates select=".//property[@name='otzyvy']" mode="reviews_slider" />
                </div>
                <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
                    <xsl:apply-templates select="document('udata://menu/draw/right_menu_events')/udata" mode="right_menu" />

                    <!-- calendar -->
                    <xsl:call-template name="calendar" >
                        <xsl:with-param name="parent_id" select="page/@parentId" />
                    </xsl:call-template>
                    <xsl:apply-templates select="." mode="right_col" />
                </div>
            </div>
        </div>
    </xsl:template>

    <!-- шаблон для вывода списка спикеров -->
    <xsl:template match="value" mode="speaker_into_event">
        <span class="speaker">
            <xsl:apply-templates select="item" mode="speaker_into_event" />
        </span>
    </xsl:template>
    <xsl:template match="item" mode="speaker_into_event">
        <xsl:value-of select="@name" />
        <xsl:if test="not(position()=last())">,<xsl:text> </xsl:text></xsl:if>
    </xsl:template>

    <!-- шаблон для вывода блоков спикеров -->
    <xsl:template match="item" mode="speaker_block">
        <xsl:variable name="speaker" select="document(concat('uobject://', @id))/udata" />
        <xsl:variable name="speaker_link" select="document(concat('udata://catalog/getLinkByObject/', @id))/udata" />
        <xsl:variable name="speaker_photo" select="$speaker//property[@name='photo']/value" />

        <div class="white-pnl speaker">
            <div class="left">
                <a href="{$speaker_link}">
                    <xsl:call-template name="makeThumbnailFull_ByPath">
                        <xsl:with-param name="source" select="$speaker_photo" />
                        <xsl:with-param name="width" select="80" />
                        <xsl:with-param name="height" select="80" />
                        <xsl:with-param name="empty">&empty-photo-speaker;</xsl:with-param>
                        <xsl:with-param name="alt" select="@name" />
                        <xsl:with-param name="class" select="'speaker'" />
                    </xsl:call-template>
                </a>
            </div>
            <div class="right">
                <h4>Спикер: <a href="{$speaker_link}" umi:object-id="{@id}" umi:field-name="name">
                        <xsl:value-of select="@name" disable-output-escaping="yes" />
                    </a>
                </h4>
                <p umi:object-id="{@id}" umi:field-name="short_desrc" umi:empty="&empty-page-content;">
                    <xsl:value-of select="$speaker//property[@name = 'short_desrc']/value" disable-output-escaping="yes" />
                </p>
            </div>
            <div class="about">
                <div umi:object-id="{@id}" umi:field-name="full_desrc" umi:empty="&empty-page-content;">
                    <xsl:value-of select="$speaker//property[@name = 'full_desrc']/value" disable-output-escaping="yes" />
                </div>
                <a class="link-right-arrow" href="&all_event_url;?filter[speaker][2]={@name}&amp;sn={@name}">Мероприятия спикера <i class="fa fa-angle-right" aria-hidden="true"></i></a>
            </div>
        </div>
    </xsl:template>


    <!-- шаблон для вывода прикрепленных файлов -->
    <xsl:template match="group" mode="event_files_arr">
        <h3>
            Приложенные файлы
        </h3>
        <xsl:apply-templates select="property[not(contains(@name,'name'))]" mode="event_files_arr" />
    </xsl:template>

    <xsl:template match="property" mode="event_files_arr">
        <xsl:variable name="file_name_field" select="concat(@name,'_name')" />
        <xsl:variable name="file_size_mb" select="document(concat('udata://system/getSize/(',value,')/(M)/1'))" />
        <xsl:variable name="file_size_kb" select="document(concat('udata://system/getSize/(',value,')/(K)/1'))" />

        <a class="document" href="{value}" target="_blank">
            <i class="fa fa-file-text" aria-hidden="true"></i>
            <xsl:value-of select="../property[@name=$file_name_field]/value" />
        </a>
        <span class="document-size">
            <xsl:choose>
                <xsl:when test="$file_size_mb &gt; 0"><xsl:value-of select="$file_size_mb" /> Мб,</xsl:when>
                <xsl:otherwise><xsl:value-of select="$file_size_kb" /> Кб,</xsl:otherwise>
            </xsl:choose>
            &#160;
            <xsl:value-of select="value/@ext" />
        </span>
    </xsl:template>

    <!-- шаблон для вывода слайдер с текущей страницы -->
    <xsl:template match="property" mode="event_photos">
        <div id="photo-slider" class="photo-slider carousel slide js-photo-slider" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <xsl:apply-templates select="value" mode="event_photos" />
            </div>
            <!-- numbers -->
            <div class="numbers js-numbers"></div>
            <!-- Controls -->
            <a class="left carousel-control" href="#photo-slider" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#photo-slider" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </xsl:template>

    <xsl:template match="value" mode="event_photos">
        <div class="item text-center">
            <xsl:if test="position()=1">
                <xsl:attribute name="class">item text-center active</xsl:attribute>
            </xsl:if>
            <img src="{text()}" alt="{@alt}" />
        </div>
    </xsl:template>


    <!-- шаблон для вывода слайдера из фотоальбома -->
    <xsl:template match="udata[@module = 'photoalbum' and @method = 'album']" mode="event_album"/>
    <xsl:template match="udata[@module = 'photoalbum' and @method = 'album' and items/item]" mode="event_album">
        <div class="white-pnl clearfix">
            <article>
                <h3>
                    <xsl:value-of select="document(concat('upage://',id,'.h1'))//value" />
                </h3>
                <div id="photo-slider-{id}" class="photo-slider carousel slide js-photo-slider" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <xsl:apply-templates select="items/item" mode="event_album"/>
                    </div>
                    <!-- numbers -->
                    <div class="numbers js-numbers"></div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#photo-slider-{id}" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#photo-slider-{id}" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </article>
        </div>
    </xsl:template>

    <xsl:template match="udata[@module = 'photoalbum' and @method = 'album']/items/item" mode="event_album">
        <div class="item text-center">
            <xsl:if test="position()=1">
                <xsl:attribute name="class">item text-center active</xsl:attribute>
            </xsl:if>
            <img src="{.//property[@name='photo']/value}" alt="{.//property[@name='h1']}" />
        </div>
    </xsl:template>


    <!-- Вывод полей "информация о регистрации" -->
    <xsl:template match="group" mode="information">
        <div>
            <span class="blue">Контакты:</span>
            <br/>
            <xsl:apply-templates select=".//property[@name='about_reg' and value]" mode="about_reg"/>
            <xsl:apply-templates select=".//property[@name='info_phone' and value]" mode="info_phone"/>
            <xsl:apply-templates select=".//property[@name='info_email' and value]" mode="info_email"/>
            <xsl:apply-templates select=".//property[@name='info_contact_person' and value]" mode="info_contact_person"/>
        </div>
    </xsl:template>
    <xsl:template match="property" mode="about_reg">
        <xsl:value-of select="value" disable-output-escaping="yes"/>
    </xsl:template>

    <xsl:template match="property" mode="info_phone">
        <span class="phone">
            <xsl:value-of select="value" />
        </span>
        <br/>
    </xsl:template>

    <xsl:template match="property" mode="info_email">
        <a href="mailto:{value}">
            <xsl:value-of select="value" />
        </a>
        <br/>
    </xsl:template>

    <xsl:template match="property" mode="info_contact_person">
        <span class="text-nowrap">контактное лицо <xsl:value-of select=".//property[@name='h1']/value" /></span>
    </xsl:template>
</xsl:stylesheet>