<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'catalog' and @method='object' and parents/page/@id = &spec_dealers_programs_pid;]">
		<xsl:variable name="filters" select="document(concat('udata://catalog/getSmartFilters//',&spec_dealers_speakers_pid;, '/0/100'))" />
		<xsl:variable name="speaker_id" select="document(concat('udata://catalog/getPageIdByObject/', .//property[@name='speaker']/value/item[1]/@id))/udata" />
		<xsl:variable name="speaker" select="document(concat('upage://', $speaker_id))/udata" />

		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
						<xsl:apply-templates select="document('udata://menu/draw/spec_dealers')/udata" mode="right_menu" />
						<xsl:apply-templates select="$filters" mode="right_filters_spikers_no_link" />
					</div>
				</div>
			</div>
		</nav>

		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

		<!--Content -->
		<div class="container-fluid dealer-event-page">
			<div class="row">
				<div class="col-xs-12">
					<div class="top-header">
						<i class="new-icon new-icon_large new-icon_calendar-big top-header__icon"></i>
						<h1 class="top-header__title">
							<xsl:value-of select="@header" disable-output-escaping="yes" />
						</h1>

						<div class="header-metadata">
							<div class="header-metadata__item">
								<ul class="metadata-list metadata-list_inline">
									<xsl:if test=".//property[@name='uroven_meropriyatiya']/value">
										<li class="metadata-list__item">
											<i class="new-icon new-icon_hat metadata-list__item-icon"></i>
											<span class="metadata-list__item-text">
												<xsl:apply-templates select=".//property[@name='uroven_meropriyatiya']/value/item" mode="uroven_meropriyatiya" />
											</span>
										</li>
									</xsl:if>
									<xsl:if test=".//property[@name='tip_meropriyatiya']/value">
										<li class="metadata-list__item">
											<i class="new-icon new-icon_user metadata-list__item-icon"></i>
											<span class="metadata-list__item-text">
												<xsl:apply-templates select=".//property[@name='tip_meropriyatiya']/value/item" mode="tip_meropriyatiya" />
												<!-- <xsl:value-of select=".//property[@name='tip_meropriyatiya']/value/item/@name"  /> -->
											</span>
										</li>
									</xsl:if>
									<xsl:if test=".//property[@name='kolvo_dnej']/value">
										<li class="metadata-list__item">
											<i class="new-icon new-icon_calendar metadata-list__item-icon"></i>
											<span class="metadata-list__item-text">
												Кол-во дней: <xsl:value-of select=".//property[@name='kolvo_dnej']/value/item/@name" disable-output-escaping="yes" />
											</span>
										</li>
									</xsl:if>
									<xsl:if test=".//property[@name='kolvo_chelovek']/value">
										<li class="metadata-list__item">
											<i class="new-icon new-icon_users metadata-list__item-icon"></i>
											<span class="metadata-list__item-text">
												Кол-во человек: <xsl:value-of select=".//property[@name='kolvo_chelovek']/value" disable-output-escaping="yes" />
											</span>
										</li>
									</xsl:if>
								</ul>
							</div>
							<div class="header-metadata__item">
								<xsl:apply-templates select=".//property[@name='tematika_provodimyh_meropriyatij']" mode="tematika_provodimyh_meropriyatij" />
							</div>
						</div>
					</div>
				</div>


				<div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
	                <div class="download-list">
	                    <div class="download-list__inner">
	                        <h4 class="download-list__header">
	                            Скачайте информацию о семинаре к себе на устройство в формате .pdf
	                        </h4>

	                        <div class="download-list__list js--filters" data-type="program-item" data-category="{$document-page-id}">>
	                            <div class="download-list__item">
	                                <a href="#" title="" class="pdf_speakers_full btn btn-primary btn_icon-download js--pdf-download-link">
	                                    Скачать pdf
	                                </a>
	                            </div>
	                            <a class="help_ico" href="#helpModalDownloadForDealers" data-toggle="modal" data-target="#helpModalDownloadForDealers"><i class="fa fa-question-circle " aria-hidden="true"></i></a>
	                        </div>
	                    </div>
	                </div>

	                <div class="card-item card-item_extended js--pdf-download-item" data-pdf-id="{$document-page-id}">
	                    <div class="card-item__left">
	                        <a href="{$speaker/page/@link}" >
		                    	<img src="{$speaker//property[@name='foto_spikera']/value}" class="img-responsive" alt="{.//property[@name='speaker']/value/page/name}">
		                    		<xsl:if test="not($speaker//property[@name='foto_spikera']/value) or $speaker//property[@name='foto_spikera']/value=''"><xsl:attribute name="src">&empty-photo-speaker;</xsl:attribute></xsl:if>
		                    	</img>
		                    </a>
	                    </div>
	                    <div class="card-item__right">
	                    	<h3 class="card-item__title"><a href="{$speaker/page/@link}"><xsl:value-of select="$speaker/page/name" disable-output-escaping="yes" /></a></h3>
	                        <xsl:if test="$speaker//property[@name='regalii']/value">
		                        <div class="card-item__info">
		                            <xsl:value-of select="$speaker//property[@name='regalii']/value" disable-output-escaping="yes" />
		                        </div>
	                        </xsl:if>

	                        <ul class="metadata-list">
	                            <xsl:apply-templates select="$speaker//property[@name='gorod_prozhivaniya']" mode="gorod_prozhivaniya" />
	                        </ul>
	                    </div>
	                    <div class="card-item__description">
	                        <xsl:value-of select="$speaker//property[@name='opisanie_spikera']/value" disable-output-escaping="yes" />
	                    </div>
	                </div>

					<xsl:if test=".//property[@name='tezisy']/value">
						<div class="white-pnl clearfix dealer-event">
		                    <article>
		                    	<h2>Тезисы</h2>
		                        <xsl:value-of select=".//property[@name='tezisy']/value" disable-output-escaping="yes" />
		                    </article>
		                </div>
					</xsl:if>

					<xsl:if test=".//property[@name='trebovaniya']/value">
						<div class="white-pnl clearfix dealer-event">
		                    <article>
		                    	<h2>Требования к проведению</h2>
		                        <xsl:value-of select=".//property[@name='trebovaniya']/value" disable-output-escaping="yes" />
		                    </article>
		                </div>
					</xsl:if>

					<a href="#" title="Назад" class="btn btn-primary btn_icon-back program_back" onclick="javascript:window.history.back(-1);return false;">
                       <i class="fa fa-reply" aria-hidden="true"></i>  Назад
                    </a>
	            </div>

	            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
	                <xsl:apply-templates select="document('udata://menu/draw/spec_dealers')/udata" mode="right_menu" />
					<xsl:apply-templates select="$filters" mode="right_filters_spikers_no_link" />
	            </div>
	        </div>
	    </div>

	</xsl:template>

	<!-- right_filters_spikers -->
	<xsl:template match="udata" mode="right_filters_spikers_no_link">
		<div class="right-filters">
            <div class="right-filters__inner">
                <ul class="right-filters__list">
                	<xsl:apply-templates select=".//field[@name='tematika_provodimyh_meropriyatij']/item" mode="right_filters_spikers_no_link" >
                		<xsl:with-param name="logos" select="document('usel://logos_tematica')/udata" />
                	</xsl:apply-templates>
                </ul>
            </div>
        </div>
	</xsl:template>

	<xsl:template match="item" mode="right_filters_spikers_no_link">
		<xsl:param name="logos" />
		<xsl:variable name="curr_name" select="text()" />

		<li class="right-filters__item">
			<div class="right-filters__item-link" >
            	<span class="right-filters__item-left">
                    <img src="{$logos/item[@name = $curr_name]//property[@name='logo']/value}" style="max-width:65px;" alt="{text()}" class="right-filters__item-img" />
                </span>
                <span class="right-filters__item-right">
                	<span class="right-filters__item-label">
                        <xsl:value-of select="text()" />
                    </span>
                </span>
			</div>
        </li>
	</xsl:template>


	<!-- filters_spikers -->
	<!-- <xsl:template match="udata" mode="filters_spikers">
		<div class="top-selectors">
			<xsl:apply-templates select=".//field[@data-type = 'relation']" mode="filters_spikers" />
            <a class="help_ico" href="#helpModalDealerssFilter" data-toggle="modal" data-target="#helpModalDealerssFilter"><i class="fa fa-question-circle " aria-hidden="true"></i></a>
            <a href="#" class="reset_filter" onclick="document.location.href = '?'"><i class="fa fa-times" aria-hidden="true"></i>Сбросить</a>
        </div>
	</xsl:template>

	<xsl:template match="field[@data-type = 'relation']" mode="filters_spikers">
		<select id="{@name}" name="fields_filter[{@name}]" title="{@title}" class="selectpicker show-tick ">
            <option value="" selected="selected"><xsl:value-of select="@title" /></option>
			<xsl:apply-templates select="." mode="filter_field"/>
        </select>
	</xsl:template> -->


	<!-- right_filters_spikers -->
	<!-- <xsl:template match="udata" mode="right_filters_spikers">
		<div class="right-filters">
            <div class="right-filters__inner">
                <ul class="right-filters__list">
                	<xsl:apply-templates select=".//field[@name='tematika_provodimyh_meropriyatij']/item" mode="right_filters_spikers" >
                		<xsl:with-param name="logos" select="document('usel://logos_tematica')/udata" />
                	</xsl:apply-templates>

                </ul>
            </div>
        </div>
	</xsl:template>

	<xsl:template match="item" mode="right_filters_spikers">
		<xsl:param name="logos" />
		<xsl:variable name="curr_name" select="text()" />

		<li class="right-filters__item">
			<xsl:if test="@is-selected = '1'">
				<xsl:attribute name="class">right-filters__item active</xsl:attribute>
			</xsl:if>
			<a href="{$curr_url}?filter[tip_meropriyatiya][4]={@name}" class="right-filters__item-link">
				<span class="right-filters__item-left">
					<img src="{$logos/item[@name = $curr_name]//property[@name='logo']/value}" alt="{text()}" class="right-filters__item-img" />
				</span>
				<span class="right-filters__item-right">
					<span class="right-filters__item-label">
						<xsl:value-of select="text()" />
					</span>
				</span>
			</a>
		</li>
	</xsl:template> -->
</xsl:stylesheet>