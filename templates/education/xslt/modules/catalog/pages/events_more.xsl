<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'catalog' and (@pageId=&events_more_pid; or parents/page/@id = &events_more_pid;)]">
		<xsl:value-of select="document('udata://content/redirect/(&all_event_url;)')/udata" />
	</xsl:template>
	
	

</xsl:stylesheet>