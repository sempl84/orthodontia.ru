<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi">

    <xsl:template match="result[@module = 'catalog' and @method='category' and @pageId=&my_events_pid;]">
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
	                    <xsl:apply-templates select="document('udata://menu/draw/right_menu_lk')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

        <!--Breadcrumbs -->
        <xsl:apply-templates select="document('udata://core/navibar/')/udata" />

        <!--Content -->
        <div class="container-fluid events-page">
            <div class="page-header">
                <div class="row">
                    <div class="col-xs-12">
                        <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                        <h1>
                            <xsl:value-of select="@header" disable-output-escaping="yes" />
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <!-- <xsl:apply-templates select="document(concat('udata://catalog/getSmartFilters//',&all_event_pid;, '/0/100'))" mode="search" /> -->
                    <xsl:apply-templates select="document('udata://emarket/eventsList/?extProps=speaker,publish_date,finish_date,mesto_provedeniya,event_type,level,organizer,price_string')/udata" mode="my_events"/>
                </div>
                <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
                    <xsl:apply-templates select="document('udata://menu/draw/right_menu_lk')/udata" mode="right_menu" />

                    <!-- calendar -->
                    <xsl:call-template name="calendar" />
                    <xsl:apply-templates select="." mode="right_col" />
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="udata" mode="my_events">
        <div class="white-pnl clearfix">Похоже, вы не зарегистрированы ни на одно мероприятие. Нужно это исправлять! Перейти в <a href="/events/">расписание занятий</a></div>
    </xsl:template>
    <xsl:template match="udata[items/item/lines/item]" mode="my_events">
        <ul class="items">
            <xsl:apply-templates select="items/item[lines/item]" mode="my_events">
                <xsl:sort select="lines/item/@publish_date" order="ascending"/>
            </xsl:apply-templates>
        </ul>
    </xsl:template>

    <xsl:template match="item" mode="my_events">
        <li class="item">
            <div class="white-pnl clearfix">
                <div class="left"></div>
                <div class="right">нет информации о мероприятии</div>
            </div>
        </li>
    </xsl:template>

    <xsl:template match="item[lines/item]" mode="my_events">
        <xsl:param name="with_event_participation" select="0" />

        <xsl:apply-templates select="lines/item" mode="my_events_info">
            <xsl:with-param name="with_event_participation" select="$with_event_participation" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="lines/item" mode="my_events_info">
        <xsl:param name="with_event_participation" select="0" />

        <xsl:variable name="date_begin" select=".//property[@name = 'publish_date']/value/@unix-timestamp" />
        <xsl:variable name="date_end" select=".//property[@name = 'finish_date']/value/@unix-timestamp" />
        <xsl:variable name="dateru" select="document(concat('udata://data/dateruPeriod/', $date_begin, '/', $date_end))/udata" />
        <xsl:variable name="max_reserv" select="number(.//property[@name = 'max_reserv']/value)" />
        <xsl:variable name="curr_reserv" select="number(.//property[@name = 'curr_reserv']/value)" />
        <xsl:variable name="reserv" select="$max_reserv - $curr_reserv" />
		<xsl:variable name="price" select="document(concat('uobject://', @order_id, '.total_price'))//value" />
		<xsl:variable name="is_active" select="document(concat('upage://', @id))/udata/page/@is-active" />

        <xsl:variable name="currency-suffix">
            <xsl:choose>
                <xsl:when test="document(concat('upage://', @id, '.vybrana_drugaya_valyuta'))//value"><xsl:value-of select="document(concat('uobject://', document(concat('upage://', @id, '.vybrana_drugaya_valyuta'))//value/item/@id, '.postfiks_valyuty'))//value" /></xsl:when>
                <xsl:otherwise><xsl:value-of select="$cart/summary/price/@suffix" /></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="speaker_photo">
            <xsl:choose>
                <xsl:when test=".//property[@name='photo']/value">
                    <xsl:value-of select=".//property[@name='photo']/value" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="document(concat('uobject://', .//property[@name='speaker']/value/item/@id,'.photo'))//value" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

		<xsl:if test="$is_active = 1">
			<li class="item">
				<div class="white-pnl clearfix">
					<div class="left">
						<a href="{@link}" class="ga_eventclick">
							<img src="{$speaker_photo}" class="speaker" alt="{.//property[@name='speaker']/value/item/@name}">
								<xsl:if test="not($speaker_photo)">
									<xsl:attribute name="src">&empty-photo-speaker;</xsl:attribute>
								</xsl:if>
							</img>
						</a>
					</div>
					<div class="right">
						<a href="{@link}" class="ga_eventclick">
							<h4>
								<xsl:value-of select="text()" />
								<xsl:if test=".//property[@name='level_shoo']/value/item/@name">
									<xsl:text>, уровень </xsl:text>
									<xsl:value-of select=".//property[@name='level_shoo']/value/item/@name" />
									<xsl:if test=".//property[@name='gorod_in']/value/item/@name">
										<xsl:text>, </xsl:text>
										<xsl:value-of select=".//property[@name='gorod_in']/value/item/@name" />
									</xsl:if>
								</xsl:if>
							</h4>
						</a>
						<xsl:apply-templates select=".//property[@name='speaker']/value/item" mode="speaker" />

						<!-- <xsl:apply-templates select=".//property[@name='publish_date']/value" mode="publish_date" /> -->
						<span class="date">
							<i class="fa fa-calendar" aria-hidden="true"></i>
							<xsl:text>Дата: </xsl:text>
							<xsl:value-of select="$dateru/first_line" />
							<xsl:text> </xsl:text>
							<xsl:value-of select="$dateru/second_line" />
							<xsl:text> (</xsl:text>
							<xsl:value-of select="$dateru/days" />
							<xsl:text>)</xsl:text>
						</span>

						<xsl:apply-templates select=".//property[@name='mesto_provedeniya']/value" mode="mesto_provedeniya" />
						<xsl:apply-templates select=".//property[@name='event_type']/value" mode="event_type" />
						<xsl:apply-templates select=".//property[@name='level']/value" mode="level" />
						<xsl:apply-templates select=".//property[@name='organizer']/value" mode="organizer" />
					</div>

					<div class="row">
						<div class="col-xs-12 text-right buttons">
							<hr />
                            <xsl:if test="$with_event_participation = 1">
                                <xsl:choose>
                                    <xsl:when test="document(concat('uobject://', @order_id, '.event_participation'))//value = 1">
                                        <div class="event_participation event_participation_true">
                                            <i class="fa fa-check"></i>
                                            Присутствие
                                        </div>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <div class="event_participation event_participation_false">
                                            <i class="fa fa-times"></i>
                                            Отсутствие
                                        </div>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:if>

							<xsl:choose>
								<xsl:when test="$max_reserv &gt; 0 and  not($curr_reserv)">
									<span class="reserv hidden-xs">Места еще есть</span>
								</xsl:when>
								<xsl:when test="$max_reserv &gt; 0 and  $curr_reserv &gt; 0 and $reserv &gt; 0">
									<span class="reserv hidden-xs">Места еще есть</span>
								</xsl:when>
								<xsl:when test="$max_reserv &gt; 0 and  $curr_reserv &gt; 0 and not($reserv &gt; 0)">
									<span class="reserv hidden-xs">Мест нет</span>
								</xsl:when>
							</xsl:choose>

							<xsl:choose>
								<xsl:when test="$price &gt; 0">
									<span class="btn btn-blue">
										<xsl:value-of select="concat(@prefix, ' ', format-number($price, '#&#160;###', 'price'), ' ', $currency-suffix)" />
									</span>
								</xsl:when>
								<xsl:otherwise>
									<span class="btn btn-orange">Бесплатно</span>
								</xsl:otherwise>
							</xsl:choose>

							<xsl:if test="$price &gt; 0">
								<xsl:variable name="order_status" select="document(concat('uobject://', @order_id, '.status_id'))" />
								<xsl:variable name="order_payment_status" select="document(concat('uobject://', @order_id, '.payment_status_id'))" />

								<!--переменные для вывода статуса оплаты-->
								<xsl:variable name="order_payment_status_1C" select="document(concat('uobject://', @order_id, '.payment_status_1C'))" />
								<xsl:variable name="order_na_moderacii" select="document(concat('uobject://', @order_id, '.na_moderacii'))" />
								<xsl:variable name="order_partner_event" select="document(concat('uobject://', @order_id, '.partner_event'))" />
								<xsl:variable name="order_payment_id" select="document(concat('uobject://', @order_id, '.payment_id'))" />

								<!--Прошедшее мероприятие-->
								<xsl:variable name="is_order_not_finish" select="document('udata://system/convertDate/now/U')/udata &lt; $date_begin" />

								<xsl:variable name="is_online_payment_show">
									<xsl:choose>
										<!--Оплачено из платежной системы 115 - Оплачивается, 307 - оплата принята -->
										<xsl:when test="$order_status//value/item/@id = '115' and $order_payment_status//value/item/@id = '307'">0</xsl:when>
										<!--На модерации-->
										<xsl:when test="document(concat('uobject://', @order_id, '.na_moderacii'))//value = 1">0</xsl:when>
										<!--Партнерское мероприятие-->
										<xsl:when test="$order_partner_event//value = 1">0</xsl:when>
										<!--Оплачено из 1С 1019451 - оплачено (статус из 1С)-->
										<xsl:when test="$order_payment_status_1C//value/item/@id = 1019451">0</xsl:when>
										<xsl:otherwise>1</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>

								<xsl:if test="$is_order_not_finish and $is_online_payment_show = 1">
									<a href="/payment/oplatit-zakaz-po-nomeru/?order_id={@order_id}" target="_blank" class="btn btn-green">Оплатить онлайн</a>
                                    <a href="/tcpdf/docs/receipt.php?oi={@order_id}" target="_blank" class="btn btn-green">Открыть квитанцию в банк</a>
								</xsl:if>
								<xsl:if test="$is_order_not_finish and ../../@paymentTypeName = 'invoice' and not($order_payment_status_1C//value/item/@id = 1019451)">
									<a href="/tcpdf/docs/invoicee.php?oi={@order_id}" target="_blank" class="btn btn-green">Открыть счет для юр.лиц</a>
								</xsl:if>

								<!--Вывод статуса оплаты-->
								<xsl:choose>
									<!--если нет способа оплаты, то мероприятие бессплатное и статус оплаты не выводим-->
									<xsl:when test="not($order_payment_id//value/item/@id)"></xsl:when>
									<!--если это партнерское мероприятие не выводим-->
									<xsl:when test="$order_partner_event//value = 1"></xsl:when>
									<!--если явно выставлен статус оплаты из 1С-->
									<xsl:when test="$is_order_not_finish and $order_payment_status_1C//value/item/@id">
										<span class="btn btn-text"><xsl:value-of select="$order_payment_status_1C//value/item/@name"/></span>
									</xsl:when>
									<!--если заказ на модерации-->
									<xsl:when test="$is_order_not_finish and $order_na_moderacii = 1">
										<span class="btn btn-text">на модерации</span>
									</xsl:when>
									<!--если 307 - оплата НЕ принята, заказ не на модерации (проверяли выше), он ожидает оплаты-->
									<xsl:when test="$is_order_not_finish and not($order_payment_status//value/item/@id = '307')">
										<span class="btn btn-text">ожидает оплаты</span>
									</xsl:when>
									<!--если оплата  принята (id 307), например после онлайн оплаты, но из 1С еще не пришел ответ-->
									<xsl:when test="$is_order_not_finish and $order_payment_status//value/item/@id = '307'">
										<span class="btn btn-text">оплачено</span>
									</xsl:when>
								</xsl:choose>
							</xsl:if>
						</div>
					</div>
				</div>
			</li>
		</xsl:if>
    </xsl:template>
</xsl:stylesheet>