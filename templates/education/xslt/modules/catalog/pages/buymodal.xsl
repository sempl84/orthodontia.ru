<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet	version="1.0"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:date="http://exslt.org/dates-and-times"
                xmlns:udt="http://umi-cms.ru/2007/UData/templates"
                xmlns:xlink="http://www.w3.org/TR/xlink"
                exclude-result-prefixes="xsl date udt xlink">

    <xsl:output encoding="utf-8" method="html" indent="yes" />

    <xsl:template match="/">
        <xsl:apply-templates select="udata" />
    </xsl:template>

    <xsl:template match="udata" />
    <xsl:template match="udata[count(item) &gt; 0 or promocode/@active = 1]">
        <div class="event_discount_wrap">
            <!--Добавлено, чтобы начислялись потенциальные бонусы-->
            <xsl:if test="count(item) = 1 and not(promocode/@active = 1)">
                <xsl:attribute name="style">display:none;</xsl:attribute>
            </xsl:if>
            <div class="form-group agree ">
                <xsl:if test="not(item/@listeners_speakers = 1)">
                    <h4>Скидка</h4>
                </xsl:if>

                <xsl:apply-templates select="item" />

                <xsl:if test="promocode/@active = 1">
                    <div class="promocode_wrap">
                        <p>Если у вас есть промокод, укажите его, чтобы получить скидки</p>
                        <input type='text' class='form-control input_promocode' placeholder='Промокод' />
                        <button class='btn btn-primary apply_promocode'>Применить</button>
                    </div>
                </xsl:if>
            </div>
            <div class="discount_info">
                <xsl:if test="not(item/@listeners_speakers = 1)">
                    <xsl:value-of select="document('upage://765.discount_info')//value" disable-output-escaping="yes" />
                </xsl:if>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="item">
        <div>
            <label for="{@id}">
                <input id="{@id}" type="radio" name="data[new][skidka]" data-name="{@name}" data-loyalty_bonus="{@loyalty_bonus}" data-freez_ormco_star="{@freez_ormco_star}" class="fn_skidka_item form-control checkbox" value="{@price}">
                    <xsl:if test="position()=1">
                        <xsl:attribute name="checked">checked</xsl:attribute>
                        <xsl:attribute name="data-name"></xsl:attribute>
                        <xsl:attribute name="value"></xsl:attribute>
                    </xsl:if>
                    <xsl:if test="@speakers-left &lt;= 0">
                        <xsl:attribute name="disabled">disabled</xsl:attribute>
                    </xsl:if>
                </input>
                <span>
                    <xsl:choose>
                        <xsl:when test="@listeners_speakers = 1 and @name = 'без скидки'">
                            Слушатель - <xsl:value-of select="@price" /> руб.
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="@name" /> - <xsl:value-of select="@price" /> руб.
                        </xsl:otherwise>
                    </xsl:choose>

                    <xsl:choose>
                        <xsl:when test="@speakers-left &gt; 0"> (осталось мест - <xsl:value-of select="@speakers-left" />)</xsl:when>
                        <xsl:when test="@speakers-left &lt;= 0"> (нет мест)</xsl:when>
                    </xsl:choose>

                    <xsl:if test="not(@loyalty_bonus = 'false') and @loyalty_bonus &gt; -1">
                        <span class="event-points-badge"><b>+<xsl:value-of select="@loyalty_bonus" /></b> звезд</span>
                    </xsl:if>
                </span>
            </label>
        </div>
    </xsl:template>

    <xsl:template match="item[@isallow = 0]">
        <div class="event-points-option event-points-option_disabled">
            <label for="{@id}">
                <input disabled="" id="{@id}" type="radio" name="data[new][skidka]" data-name="{@name}" data-loyalty_bonus="{@loyalty_bonus}" class="fn_skidka_item form-control checkbox" value="{@price}">
                    <xsl:if test="position()=1">
                        <xsl:attribute name="checked">checked</xsl:attribute>
                        <xsl:attribute name="data-name"></xsl:attribute>
                        <xsl:attribute name="value"></xsl:attribute>
                    </xsl:if>
                </input>
                <span>
                    <xsl:value-of select="@name" /> - <xsl:value-of select="@price" /> руб.
                </span>
                <div class="event-points-option__message">
                    У вас недостаточно звезд, чтобы воспользоваться этой скидкой, зарабатывайте больше звезд регистрируясь на мероприятиях!
                </div>
            </label>
        </div>
    </xsl:template>
</xsl:stylesheet>
