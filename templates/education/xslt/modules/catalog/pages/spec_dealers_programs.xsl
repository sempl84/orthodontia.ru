<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'catalog' and @method='category' and page/@id = &spec_dealers_programs_pid;]">
		<xsl:variable name="filters" select="document(concat('udata://catalog/getSmartFilters//',$document-page-id, '/0/100'))" />

		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
						<xsl:apply-templates select="document('udata://menu/draw/spec_dealers')/udata" mode="right_menu" />
						<xsl:apply-templates select="$filters" mode="right_filters_spikers" />
					</div>
				</div>
			</div>
		</nav>

		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

		<!--Content -->
		<div class="container-fluid dealers-speakers-page">
			<div class="row">
				<div class="col-xs-12">
					<div class="top-header">
						<i class="new-icon new-icon_large new-icon_calendar-big top-header__icon"></i>
						<h1 class="top-header__title">
							<xsl:value-of select="@header" disable-output-escaping="yes" />
						</h1>
					</div>
				</div>

				<div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
					<xsl:if test="$filters[.//field[@data-type = 'relation' and @name='tematika_provodimyh_meropriyatij']]">
						<hr />
						<xsl:apply-templates select="$filters" mode="filters_programs" >
							<!-- <xsl:with-param name="action" select="concat('/udata/catalog/getSmartCatalogPro//', $document-page-id, '/1000/1/2///?extProps=foto_spikera,uroven_meropriyatiya,tip_meropriyatiya,gorod_prozhivaniya,tematika_provodimyh_meropriyatij')" /> -->
							<xsl:with-param name="action" select="'/templates/education/programs.html'" />
						</xsl:apply-templates>
						<div style="position:absolute; left:-100000px;">
							<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate" ></span>
						</div>
					</xsl:if>

					<div class="download-list">
						<div class="download-list__inner">
							<h4 class="download-list__header">
								Скачайте информацию о семинарах к себе на устройство в формате .pdf
							</h4>

							<div class="download-list__list">
								<div class="download-list__item">
									<a href="#" title="" class="pdf_speakers_short btn btn-primary btn_icon-download js--pdf-download-link">
										Все кратко
									</a>
								</div>
								<div class="download-list__item">
									<a href="#" title="" class="pdf_speakers_full btn btn-primary btn_icon-download js--pdf-download-link">
										Все подробно
									</a>
								</div>

								<div class="download-list__item">
									<a href="#" title="Скачать выбранное (кратко)" class="pdf_speakers_selected btn btn-primary btn_icon-download js--pdf-download-link">Выбранное кратко</a>
								</div>
								<div class="download-list__item">
									<a href="#" title="Скачать выбранное (подробно)" class="pdf_speakers_selected_full btn btn-primary btn_icon-download js--pdf-download-link">Выбранное подробно</a>
								</div>
								<a class="help_ico" href="#helpModalDownloadForDealers" data-toggle="tooltip" data-placement="top" title="Скачайте на свое устройство в формате pdf краткую или подробную информацию о всех мероприятиях, либо отмечайте галочкой нужные мероприятия и скачивайте краткую информацию только о них.">
									<i class="fa fa-question-circle " aria-hidden="true"></i>
								</a>
							</div>
						</div>
					</div>

					<div class="row" id="dealers-list">
						<xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalogPro//', $document-page-id, '/1000/1/2///?extProps=speaker,uroven_meropriyatiya,tip_meropriyatiya,gorod_prozhivaniya,tematika_provodimyh_meropriyatij,kolvo_dnej,seminar_na_kafedre'))/udata/lines/item" mode="spec_dealers_programs"/>
					</div>
				</div>

				<div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
					<xsl:apply-templates select="document('udata://menu/draw/spec_dealers')/udata" mode="right_menu" />
					<xsl:apply-templates select="$filters" mode="right_filters_spikers" />
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="item" mode="spec_dealers_programs">
		<xsl:variable name="speaker_id" select="document(concat('udata://catalog/getPageIdByObject/', .//property[@name='speaker']/value/item[1]/@id))/udata" />
		<xsl:variable name="speaker" select="document(concat('upage://', $speaker_id))/udata" />

		<div class="col-sm-12 col-xs-12">
			<div class="card-item program">
				<div class="card-item__checkbox js--pdf-download-item" data-pdf-id="{@id}"></div>
				<div class="card-item__left">
					<a href="{$speaker/page/@link}" class="rhref" data-href="{$speaker/page/@link}">
						<img src="{$speaker//property[@name='foto_spikera']/value}" class="img-responsive" alt="{$speaker/page/name}">
							<xsl:if test="not($speaker//property[@name='foto_spikera']/value) or $speaker//property[@name='foto_spikera']/value=''">
								<xsl:attribute name="src">&empty-photo-speaker;</xsl:attribute>
							</xsl:if>
						</img>
					</a>
				</div>
				<div class="card-item__right">
					<h3 class="card-item__title">
						<a href="{@link}" class="rhref" data-href="{@link}">
							<xsl:value-of select="text()" />
						</a>
					</h3>
					<xsl:if test="not($speaker/page/name='Главная')">
						<h4 class="card-item__username">
							<a href="{$speaker/page/@link}" class="rhref" data-href="{$speaker/page/@link}">
								<xsl:value-of select="$speaker/page/name" disable-output-escaping="yes" />
							</a>
						</h4>
					</xsl:if>

					<ul class="metadata-list metadata-list_inline">
						<xsl:if test=".//property[@name='uroven_meropriyatiya']/value">
							<li class="metadata-list__item">
								<i class="new-icon new-icon_hat metadata-list__item-icon"></i>
								<span class="metadata-list__item-text">
									<xsl:apply-templates select=".//property[@name='uroven_meropriyatiya']/value/item" mode="uroven_meropriyatiya" />
								</span>
							</li>
						</xsl:if>
						<xsl:if test=".//property[@name='tip_meropriyatiya']/value">
							<li class="metadata-list__item">
								<i class="new-icon new-icon_user metadata-list__item-icon"></i>
								<span class="metadata-list__item-text">
									<xsl:apply-templates select=".//property[@name='tip_meropriyatiya']/value/item" mode="tip_meropriyatiya" />
								</span>
							</li>
						</xsl:if>
						<xsl:if test=".//property[@name='kolvo_dnej']/value">
							<li class="metadata-list__item">
								<i class="new-icon new-icon_calendar metadata-list__item-icon"></i>
								<span class="metadata-list__item-text">
									<!-- Кол-во дней:  -->
									<xsl:value-of select=".//property[@name='kolvo_dnej']/value/item/@name" disable-output-escaping="yes" />
								</span>
							</li>
						</xsl:if>
						<xsl:if test=".//property[@name='seminar_na_kafedre']/value/item/@name='Да'">
							<li class="metadata-list__item">
								<i class="new-icon new-icon_small new-icon_speaker  metadata-list__item-icon"></i>
							</li>
						</xsl:if>

						<li class="hidden-xs metadata-list__item right_side">
							<a href="{@link}" title="" class="btn btn-blue  rhref" data-href="{@link}">
								Подробнее
							</a>
						</li>
						<li class="hidden-xs metadata-list__item right_side">
							<xsl:apply-templates select=".//property[@name='tematika_provodimyh_meropriyatij']" mode="tematika_provodimyh_meropriyatij" />
						</li>
					</ul>
				</div>

				<div class="hidden-sm hidden-md hidden-lg">
					<hr class="card-item__hr" />
					<xsl:apply-templates select=".//property[@name='tematika_provodimyh_meropriyatij']" mode="tematika_provodimyh_meropriyatij" />

					<a href="{@link}" title="" class="btn btn-blue card-item__more rhref" data-href="{@link}">
						Подробнее
					</a>
				</div>
			</div>
		</div>
	</xsl:template>

	<!-- filters_programs -->
	<xsl:template match="udata" mode="filters_programs">
		<xsl:param name="action"  />

		<div class="top-selectors js--filters" data-action="{$action}" data-type="program" data-category="{$document-page-id}">
			<xsl:apply-templates select=".//group[@name='parametry_programmy']/field[@data-type = 'relation' and @name='speaker' ]" mode="filters_spikers" />
			<xsl:apply-templates select=".//field[@data-type = 'relation' and @name='uroven_meropriyatiya']" mode="filters_spikers" />
			<xsl:apply-templates select=".//field[@data-type = 'relation' and @name='tematika_provodimyh_meropriyatij']" mode="filters_spikers" />
			<xsl:apply-templates select=".//field[@data-type = 'relation' and @name='kolvo_dnej']" mode="filters_spikers" />
			<xsl:apply-templates select=".//field[@data-type = 'relation' and @name='tip_meropriyatiya']" mode="filters_spikers" />
			<xsl:apply-templates select=".//field[@data-type = 'relation' and @name='seminar_na_kafedre']" mode="filters_spikers" />

			<a href="#" class="reset_filter" onclick="document.location.href = '?'" data-toggle="tooltip" data-placement="top" title="Сбросить фильтры" >
				<i class="fa fa-times-circle-o" aria-hidden="true"></i>
			</a>
			<a class="help_ico" href="#helpModalDealerssFilter" data-toggle="tooltip" data-placement="top" title="Воспользуйтесь фильтрами&lt;br/&gt; для быстрого доступа к интересным именно вам программам мероприятий">
				<i class="fa fa-question-circle " aria-hidden="true"></i>
			</a>
		</div>
	</xsl:template>
</xsl:stylesheet>