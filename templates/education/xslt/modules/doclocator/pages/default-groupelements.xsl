<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

    <xsl:template match="result[@module = 'doclocator']">
        <xsl:variable name="clinics" select="document(concat('udata://doclocator/listElements/',$document-page-id,'//1000/1/?extProps=adres_clinic,phone_clinic,url_clinic,lat,lng'))/udata"  />

		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
						<xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

        <!--Breadcrumbs -->
        <xsl:apply-templates select="document('udata://core/navibar/')/udata" />

        <!--Content -->
        <div class="container-fluid news-page search-clinic-page">
            <div class="page-header">
                <div class="row">
                    <div class="col-xs-12">
                        <i class="fa fa-building" aria-hidden="true">
                            <xsl:if test=".//property[@name='icon_style']/value">
                                <xsl:attribute name="class">
                                    <xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" />
                                </xsl:attribute>
                            </xsl:if>
                        </i>
                        <h1>
                            <xsl:value-of select="@header" disable-output-escaping="yes" />
                        </h1>
                    </div>
                </div>
            </div>

            <div class="white-pnl">
                <div class="row">
                    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
                        <div class="select-on-map-pnl">
    <!--                        <div class="selectors">
                                <select id="coutries" class="selectpicker show-tick js- -clinics-regions">
                                    <option value="none" selected="selected">Выбрать регион</option>
                                </select>
                                <select id="cities" class="selectpicker show-tick js- -clinics-cities">
                                    <option value="none" selected="selected">Выбрать город</option>
                                </select>

								<div class="clinics-helper js-privacy-lnk" data-placement="top" data-original-title="Выберите регион и город чтобы найти подходящую клинику">
									<i class="fa fa-question" aria-hidden="true"></i>
								</div>
							</div>-->
							<div class="map js--clinics-map"></div>

							<div class="clinics-view-data js--clinics-view-data"></div>
							<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
							<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpGO1gZOJsEJ9FdcwHdvwAgXgLQ2M5H2E"></script>
							<script>
								<!--                                var regions = [
									<xsl:apply-templates select="$clinics/regions/item" mode="regions" />
								];
								var cities = [
									<xsl:apply-templates select="$clinics/cities/item" mode="cities" />
								];-->
								var clinics = [
								<xsl:apply-templates select="$clinics/items/item" mode="clinics" />
								];
							</script>
							<script src="/templates/education/js/search-clinic.js"></script>
						</div>
					</div>
					<div class="col-lg-3 col-md-4">
						<div id="center_list">
							<xsl:apply-templates select="$clinics/items/item" mode="centers" />
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
					<div class="white-pnl">
						<article class="pc-{$document-page-id} content_wrap" umi:element-id="{$document-page-id}" umi:field-name="content" umi:empty="&empty-page-content;">
							<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
						</article>
						<div class="content_soc_wrap">
							<xsl:call-template name='social_links'>
								<xsl:with-param name="class" select="'pull-right'"/>
							</xsl:call-template>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
					<xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
					<xsl:apply-templates select="." mode="right_col" />
				</div>
			</div>
		</div>

		<link href="/templates/education/css/search-clinic.css?v=26" rel="stylesheet" />
	</xsl:template>

	<!-- regions -->
	<xsl:template match="item" mode="regions" >{ id: <xsl:value-of select="@id" />, title: '<xsl:value-of select="text()" />' },</xsl:template>

	<!-- cities -->
	<xsl:template match="item" mode="cities" >{ id: <xsl:value-of select="@id" />,region_id: <xsl:value-of select="@region_id" />,  title: '<xsl:value-of select="text()" />' },</xsl:template>

	<!-- clinics -->
	<xsl:template match="item" mode="clinics" >{ id: <xsl:value-of select="@id" />, city_id: <xsl:value-of select="@city_id" />, city_name: '<xsl:value-of select="@city_name" />', title: '<xsl:value-of select="text()" />', address: '<xsl:value-of select=".//property[@name='adres_clinic']/value" />', phone: '<xsl:value-of select=".//property[@name='phone_clinic']/value" />', url: '<xsl:value-of select=".//property[@name='url_clinic']/value" />', lat: <xsl:value-of select=".//property[@name='lat']/value" />, lng: <xsl:value-of select=".//property[@name='lng']/value" />},</xsl:template>

	<!-- centers -->
	<xsl:template match="item" mode="centers">
		<a href="{@link}" class="one_center_row" data-clinic-id="{@id}">
			<span class="center_row_name">
				<xsl:value-of select="text()" />
			</span>
			<span class="center_row_adress">
				<xsl:value-of select="@city_name" />, <xsl:value-of select=".//property[@name='adres_clinic']/value" />
			</span>
		</a>
	</xsl:template>
</xsl:stylesheet>