<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi"
        xmlns:php="http://php.net/xsl"
        exclude-result-prefixes="php">

    <xsl:template match="result[@module = 'doclocator' and @method='item_element']">
        <!--<xsl:value-of select="document(concat('udata://content/redirect/(', $parents/page[position() = last()]/@link, ')'))/udata" />-->
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
                        <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

        <!--Breadcrumbs -->
        <xsl:apply-templates select="document('udata://core/navibar/')/udata" />

        <!--Content -->
        <div class="container-fluid news-page">
            <div class="page-header">
                <div class="row">
                    <div class="col-xs-12">
                        <h1><xsl:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes" /></h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <div class="white-pnl">
                        <div class="center_details">
                            <div class="center_site"><a href="{.//property[@name = 'url_clinic']/value}" class="btn btn-primary">Перейти на сайт центра</a></div>
                            <div class="center_adress">Адрес: <xsl:value-of select=".//property[@name = 'adres_clinic']/value" /></div>
                            <div class="center_phone">Тел.: <xsl:value-of select=".//property[@name = 'phone_clinic']/value" /></div>
                            <xsl:if test=".//property[@name = 'email']/value">
                                <div class="center_email">E-mail:
                                    <xsl:call-template name="center_email">
                                        <xsl:with-param name="email" select=".//property[@name = 'email']/value" />
                                    </xsl:call-template>
                                </div>
                            </xsl:if>
                        </div>

                        <xsl:if test=".//property[@name = 'video']/value">
                            <xsl:variable name="slash_position" select="php:function('strrpos', string(.//property[@name = 'video']/value/text()), '/')" />

                            <div class="embed-responsive embed-responsive-4by3 center_details_video">
                                <iframe src="https://www.youtube.com/embed/{substring(.//property[@name = 'video']/value, $slash_position + 2)}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                            </div>
                        </xsl:if>

                        <article class="pc-{$document-page-id} content_wrap" umi:element-id="{$document-page-id}" umi:field-name="content" umi:empty="&empty-page-content;">
                            <xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
                        </article>
                        <div class="content_soc_wrap">
                            <xsl:call-template name='social_links'>
                                <xsl:with-param name="class" select="'pull-right'"/>
                            </xsl:call-template>
                        </div>
                    </div>

                    <xsl:apply-templates select="document(concat('udata://catalog/get_events_by_center/', $document-page-id))/udata" mode="closest_events" />
                </div>

                <div class="reviews_on_center_clearer" />

                <div class="col-lg-3 col-md-4 right-pnl">
                    <!--<xsl:apply-templates select=".//property[@name = 'otzyvy']" mode="reviews_on_center" />-->
                    <xsl:apply-templates select="document(concat('usel://included_reviews/?parent=', $document-page-id, '&amp;limit=6'))/udata" mode="reviews_on_center" />
                    <!--<div style="display:none;"><xsl:copy-of select="document(concat('usel://included_reviews/?parent=', $document-page-id, '&amp;limit=6'))" /></div>-->

                    <div class="hidden-sm hidden-xs">
                        <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
                        <xsl:apply-templates select="." mode="right_col" />
                    </div>
                </div>
            </div>
        </div>
        <link href="/templates/education/css/search-clinic.css?v=26" rel="stylesheet" />
    </xsl:template>

    <xsl:template match="udata" mode="closest_events" />
    <xsl:template match="udata[lines/item]" mode="closest_events">
        <div class="row events">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="header2">Ближайшие мероприятия</div>
            </div>
            <ul class="list js-tiles-resize">
                <xsl:apply-templates select="lines/item" mode="closest_events_block"/>
            </ul>
        </div>
    </xsl:template>

    <xsl:template match="item" mode="closest_events_block">
        <xsl:variable name="page" select="document(concat('upage://', @id))" />
        <xsl:variable name="date_begin" select="$page//property[@name = 'publish_date']/value/@unix-timestamp" />
        <xsl:variable name="date_end" select="$page//property[@name = 'finish_date']/value/@unix-timestamp" />
        <xsl:variable name="dateru" select="document(concat('udata://data/dateruPeriod/', $date_begin, '/', $date_end))/udata" />
        <xsl:variable name="max_reserv" select="number($page//property[@name = 'max_reserv']/value)" />
        <xsl:variable name="curr_reserv" select="number($page//property[@name = 'curr_reserv']/value)" />
        <xsl:variable name="reserv" select="$max_reserv - $curr_reserv" />
        <xsl:variable name="showDiscountByDate" select="document(concat('udata://emarket/showDiscountByDate/',@id))/udata" />
        <xsl:variable name="isFirstPurchase" select="document(concat('udata://emarket/checkSecondPurchase/',@id,'/',$user-info//property[@name='e-mail']/value))/udata" />
        <xsl:variable name="loyalty" select="document(concat('udata://custom/outputEventLoyalty/',@id))/udata" />

        <xsl:variable name="speaker_photo" select="document(concat('uobject://', $page//property[@name='speaker']/value/item/@id,'.photo'))//value" />

        <!-- ga params -->
        <xsl:variable name="ga_price">
            <xsl:choose>
                <xsl:when test="contains($page//property[@name='event_url_redirect']/value, 'orthodontia.ru') ">Лендинг</xsl:when>
                <xsl:when test="($showDiscountByDate &gt; 0 and $showDiscountByDate &gt; 0) or $page//property[@name='price']/value &gt; 0">Платное</xsl:when>
                <xsl:otherwise>Бесплатное</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="ga_list">
            <xsl:choose>
                <xsl:when test="contains($page//property[@name='event_url_redirect']/value, 'orthodontia.ru')">Лендинг</xsl:when>
                <xsl:when test="$parents/page/@id = &all_event_pid; or $document-page-id = &all_event_pid;">Все мероприятия</xsl:when>
                <xsl:when test="$parents/page/@id = &ormco_club_pid; or $document-page-id = &ormco_club_pid;">Клуб Ormco</xsl:when>
                <xsl:when test="$is_default_page = 1">Главная страница</xsl:when>
            </xsl:choose>
        </xsl:variable>

        <li class="col-md-4 col-sm-4 col-xs-6 ga_addImpression" data-name="{text()}" data-category="{$ga_price}" data-list="{$ga_list}" data-position="{position()}">
            <div class="thumbnail yellow">
                <!-- вызов шаблона, который по id типа мероприятия возвращает css класс -->
                <xsl:call-template name="add_event_css_class_short">
                    <xsl:with-param name="old_class" select="'thumbnail'"/>
                    <xsl:with-param name="event_type_id" select="$page//property[@name='event_type']/value/item/@id"/>
                </xsl:call-template>

                <xsl:apply-templates select="$page//property[@name='level']/value" mode="short_event_block_level" />
                <span class="date">
                    <xsl:value-of select="$dateru/first_line" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$dateru/second_line" />
                    <xsl:text> (</xsl:text>
                    <xsl:value-of select="$dateru/days" />
                    <xsl:text>)</xsl:text>
                </span>
                <span class="location">
                    <xsl:value-of select="$page//property[@name='event_type']/value/item/@name" />
                    <xsl:if test="$page//property[@name='gorod_in']/value/item/@name">
                        <xsl:text>, </xsl:text>
                        <xsl:value-of select="$page//property[@name='gorod_in']/value/item/@name" />
                    </xsl:if>
                </span>
                <div class="topic-wrapper">
                    <div class="topic">
                        <a href="{@link}">
                            <xsl:value-of select="text()" />
                        </a>
                    </div>
                </div>
                <div class="author">
                    <img src="{$speaker_photo}" alt="{$page//property[@name='speaker']/value/item/@name}">
                        <xsl:if test="not($speaker_photo)">
                            <xsl:attribute name="src">&empty-photo-speaker;</xsl:attribute>
                        </xsl:if>
                    </img>
                    <span>
                        <xsl:value-of select="$page//property[@name='speaker']/value/item/@name" />
                    </span>
                </div>
                <div class="price-border">
                    <xsl:choose>
                        <xsl:when test="$page//property[@name='price_origin']/value &gt; 0">
                            <span class="price">
                                <span class="old-price">
                                    <xsl:value-of select="concat(format-number($page//property[@name='price_origin']/value, '#&#160;###','price'), ' ', $currency-suffix)" />
                                </span>
                                <span class="new-price">
                                    <xsl:value-of select="concat(format-number($page//property[@name='price']/value, '#&#160;###','price'), ' ', $currency-suffix)" />
                                </span>

                                <xsl:if test="$loyalty/discount_loyalty/@isallow &gt; 0">
                                    <xsl:call-template name="discount_loyalty_short" />
                                </xsl:if>
                            </span>
                        </xsl:when>
                        <xsl:when test="$showDiscountByDate &gt; 0">
                            <span class="price">
                                <span class="old-price">
                                    <xsl:value-of select="concat(format-number($page//property[@name='price']/value, '#&#160;###','price'), ' ', $currency-suffix)" />
                                </span>
                                <span class="new-price">
                                    <xsl:value-of select="concat(format-number($showDiscountByDate, '#&#160;###','price'), ' ', $currency-suffix)" />
                                </span>

                                <xsl:if test="$loyalty/discount_loyalty/@isallow &gt; 0">
                                    <xsl:call-template name="discount_loyalty_short" />
                                </xsl:if>
                            </span>
                            <!-- <span class="btn old-price"><xsl:value-of select="concat(format-number($page//property[@name='price']/value, '#&#160;###','price'), ' ', $currency-suffix)" /></span>
                            <span class="btn btn-blue"><xsl:value-of select="concat(format-number($showDiscountByDate, '#&#160;###','price'), ' ', $currency-suffix)" /></span> -->
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="document(concat('udata://emarket/price/', @id,'//0'))/udata" mode="short_event_price" >
                                <xsl:with-param name="loyalty" select="$loyalty"/>
                            </xsl:apply-templates>
                        </xsl:otherwise>
                    </xsl:choose>
                    <!-- <span class="price">15 000 р.</span> -->
                </div>
                <xsl:choose>
                    <xsl:when test="$stub_vds=1">
                        <a href="#stub_vds_modal" data-toggle="modal" data-target="#stub_vds_modal"  class="btn btn-green buy-button register">Зарегистрироваться</a>
                    </xsl:when>
                    <!-- есть галочка "прячем кнопку"" -->
                    <xsl:when test="$page//property[@name='hide_reg_button']/value"></xsl:when>
                    <!-- нет мест прячем кнопку" -->
                    <xsl:when test="$max_reserv &gt; 0 and  $curr_reserv &gt; 0 and not($reserv &gt; 0)">
                        <span class="fake_btn">Мест нет</span>
                    </xsl:when>
                    <!-- есть ссылка то делаем кнопку редиректа" -->
                    <!-- <xsl:when test="$page//property[@name='event_type']/value/item/@id = &event_type_WOO-oid; and $page//property[@name='event_url_redirect']/value"> -->
                    <xsl:when test="$page//property[@name='event_url_redirect']/value">
                        <a href="{$page//property[@name='event_url_redirect']/value}" data-price="{$page//property[@name='price']/value}"  class="btn btn-green buy-button register">Перейти к регистрации</a>
                    </xsl:when>
                    <!-- ШОО  прячем кнопку" -->
<!--                    <xsl:when test="$page//property[@name='event_type']/value/item/@id = &event_type_WOO-oid;">
                        <span class="fake_btn"></span>
                    </xsl:when>-->
                    <!-- отображать сообщение, если человек уже регистрировался на данное мероприятие -->
                    <xsl:when test="$isFirstPurchase = 0">
                        <a href="#buyModalSecondPurchase" data-price="{$page//property[@name='price']/value}"  data-page_id="{@id}" data-toggle="modal" data-target="#buyModalSecondPurchase" class="btn btn-green buy-button register">Зарегистрироваться</a>
                    </xsl:when>
                    <xsl:otherwise>
                        <a href="#buyModal" data-price="{$page//property[@name='price']/value}"  data-page_id="{@id}" data-toggle="modal" data-target="#buyModal" class="btn btn-green buy-button register">Зарегистрироваться</a>
                    </xsl:otherwise>
                    <!-- есть ссылка то делаем кнопку редиректа" -->
                    <!-- <xsl:when test="$page//property[@name='event_url_redirect']/value">
                        <a href="{$page//property[@name='event_url_redirect']/value}" class="btn btn-green buy-button register">Перейти к регистрации</a>
                    </xsl:when> -->

                    <!-- ШОО  прячем кнопку" -->
                    <!-- <xsl:when test="$page//property[@name='event_type']/value/item/@id = &event_type_WOO-oid;"><a href="#" class="btn btn-green buy-button register empty_button">&#160;</a></xsl:when>
                    <xsl:otherwise>
                        <a href="#buyModal" data-page_id="{@id}" data-toggle="modal" data-target="#buyModal" class="btn btn-green buy-button register">Зарегистрироваться</a>
                    </xsl:otherwise> -->
                </xsl:choose>
                <!-- <a href="#buyModal" data-toggle="modal" data-target="#buyModal" class="btn btn-green buy-button register">Зарегистрироватся</a> -->

                <xsl:if test="$page//property[@name='akkreditaciya_star']/value">
                    <span class="akkr"></span>
                </xsl:if>

                <a href="{@link}" class="ga_eventclick link"></a>
                <xsl:if test="$loyalty/add_loyalty/@potential_bonus">
                    <a  href="&link_potential_bonus_info;"
                        class="event-points-info"
                        data-toggle="tooltip"
                        data-placement="top"
                        target="_blank"
                        title="Эти звезды вы можете получить за участие в мероприятии">
                        <i class="fa fa-star event-points-info__icon" aria-hidden="true"></i>
                        <span class="event-points-info__count">
                            <xsl:value-of select="$loyalty/add_loyalty/@potential_bonus" />
                        </span>
                    </a>
                </xsl:if>
            </div>
        </li>
    </xsl:template>

    <xsl:template match="udata" mode="reviews_on_center" />
    <xsl:template match="udata[page]" mode="reviews_on_center">
        <div id="review_slider" class="carousel slide reviews_on_center_slider" data-ride="carousel">
            <div class="white-pnl">
                <div class="carousel-inner" role="listbox">
                    <xsl:apply-templates select="page" mode="reviews_on_center" />
                </div>
            </div>
            <ol class="carousel-indicators">
                <xsl:apply-templates select="page" mode="reviews_on_center_li" />
            </ol>
        </div>
    </xsl:template>

<!--    <xsl:template match="property" mode="reviews_on_center" />
    <xsl:template match="property[value]" mode="reviews_on_center">
        <div id="review_slider" class="carousel slide reviews_on_center_slider" data-ride="carousel">
            <div class="white-pnl">
                <div class="carousel-inner" role="listbox">
                    <xsl:apply-templates select="value/page" mode="reviews_on_center" />
                </div>
            </div>
            <ol class="carousel-indicators">
                <xsl:apply-templates select="value/page" mode="reviews_on_center_li" />
            </ol>
        </div>
    </xsl:template>-->

    <xsl:template match="page" mode="reviews_on_center">
        <div class="item reviews_on_center_body">
            <xsl:if test="position() = 1">
                <xsl:attribute name="class">item active reviews_on_center_body</xsl:attribute>
            </xsl:if>

            <div class="feedback content_wrap">
                <xsl:value-of select="document(concat('upage://', @id, '.content'))//value" disable-output-escaping="yes" />
            </div>
            <div class="author">— <xsl:value-of select="document(concat('upage://', @id, '.author_info'))//value" disable-output-escaping="yes" /></div>
        </div>
    </xsl:template>

    <xsl:template match="page" mode="reviews_on_center_li">
        <li data-target="#review_slider" data-slide-to="{position()-1}">
            <xsl:if test="position() = 1">
                <xsl:attribute name="class">active</xsl:attribute>
            </xsl:if>
        </li>
    </xsl:template>

    <xsl:template name="center_email">
        <xsl:param name="email" />

        <xsl:variable name="one_email" select="php:function('trim', substring-before($email, ','))" />
        <xsl:variable name="two_email" select="php:function('trim', substring($email, string-length($one_email)+2))" />

        <xsl:choose>
            <xsl:when test="string-length($one_email) &gt; 0">
                <a href="mailto:{$one_email}"><xsl:value-of select="$one_email" /></a>
                <xsl:if test="string-length($one_email) &gt; 0">
                    <xsl:text>, </xsl:text>
                    <xsl:call-template name="center_email">
                        <xsl:with-param name="email" select="$two_email" />
                    </xsl:call-template>
                </xsl:if>
            </xsl:when>
            <xsl:otherwise>
                <a href="mailto:{$email}"><xsl:value-of select="$email" /></a>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>