<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'cliniccourse' and @method='item_element']">
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
						<xsl:apply-templates select="document('udata://menu/draw/right_menu_events')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

		<!--Content -->
		<div class="container-fluid cliniccourse-page">
			<div class="page-header">
				<div class="row">
					<div class="col-xs-12">
						<i class="fa fa-medkit" aria-hidden="true"></i>
						<h1>
							<xsl:value-of select="@header" disable-output-escaping="yes" />
						</h1>
					</div>
				</div>
			</div>
			<div class="row">
                <div style="display:none;"><xsl:copy-of select="." /></div>
				<div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12 ga_item" data-id="{page/@id}" data-list="{/result//parents/page[position() = last()]/name}" data-name="{@header}" data-price="0" data-event_category="Клинические курсы">
					<div class="white-pnl">
						<article class="content_wrap" umi:element-id="{$document-page-id}" umi:field-name="content" umi:empty="&empty-page-content;">
							<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />
						</article>

						<a href="#courseFormModal" data-from_course="{page/@link}" data-from_course_name="{@header}" data-toggle="modal" data-target="#courseFormModal" class="btn btn-green course-info ga_item_add_basket" data-name="{@header}" data-id="{page/@id}" data-price="0" data-event_category="Клинические курсы">Подать заявку</a>
						<hr/>

						<xsl:call-template name='social_links'>
							<xsl:with-param name="class" select="'pull-right'"/>
						</xsl:call-template>
						<a class="link-right-arrow" href="../">Вернуться к списку <i class="fa fa-angle-right" aria-hidden="true"></i></a>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
					<xsl:apply-templates select="document('udata://menu/draw/right_menu_events')/udata" mode="right_menu" />
					<xsl:apply-templates select="." mode="right_col" />
				</div>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>