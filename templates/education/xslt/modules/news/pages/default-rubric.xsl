<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'news']">
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
		                <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

		<!--Content -->
	    <div class="container-fluid news-page">
	        <div class="page-header">
	            <div class="row">
	                <div class="col-xs-12">
	                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
	                    <h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
	                </div>
	            </div>
	        </div>
	        <div class="row">
	            <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
	                <xsl:apply-templates select="document('udata://news/lastlist/?extProps=publish_time,anons_pic,anons')/udata" />
	            </div>
	            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
	                <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
	                <xsl:apply-templates select="." mode="right_col" />
	            </div>
	        </div>
	    </div>
	</xsl:template>

	<xsl:template match="udata[@module='news' and @method='lastlist']">
		Ничего не найдено
	</xsl:template>
	<xsl:template match="udata[@module='news' and @method='lastlist' and items/item]">
		<ul class="items">
            <xsl:apply-templates select="items/item" />
        </ul>
        <xsl:apply-templates select="total" />
	</xsl:template>

	<xsl:template match="udata[@module='news' and @method='lastlist']/items/item" >
			<li class="item">
                <div class="white-pnl clearfix">
                    <div class="row">
                        <div class="col-xs-3 left">
                            <a href="{@link}">
                            	<xsl:choose>
									<xsl:when test=".//property[@name='anons_pic']/value">
										<xsl:call-template name="makeThumbnail_ByPath">
											<xsl:with-param name="source" select=".//property[@name='anons_pic']/value" />
											<xsl:with-param name="width" select="175" />
											<xsl:with-param name="height" select="'auto'" />

											<xsl:with-param name="empty">&empty-photo-news;</xsl:with-param>
											<xsl:with-param name="element-id" select="@id" />
											<xsl:with-param name="field-name" select="'anons_pic'" />

											<xsl:with-param name="alt" select="text()" />
											<xsl:with-param name="class" select="'img-responsive'" />
										</xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<div class="default-photo">
		                                    <img class="img-responsive" src="&empty-photo-news;" alt="{text()}" />
		                                </div>
									</xsl:otherwise>
								</xsl:choose>
                            </a>
                        </div>
                        <div class="col-xs-9 right">
                            <h4><a href="{@link}"><xsl:value-of select="text()" disable-output-escaping="yes" /></a></h4>
                            <span class="date"><i class="fa fa-calendar" aria-hidden="true"></i><xsl:value-of select="document(concat('udata://catalog/dateru/',.//property[@name='publish_time']/value/@unix-timestamp))/udata"/></span>
                            <div class="preview content_wrap">
								<xsl:value-of select=".//property[@name = 'anons']/value" disable-output-escaping="yes" />
							</div>
                        </div>
                    </div>
                </div>
            </li>
	</xsl:template>

</xsl:stylesheet>