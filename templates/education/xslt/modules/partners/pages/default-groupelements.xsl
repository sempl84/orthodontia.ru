<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'partners']">
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
		                <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

		<!--Content -->
	    <div class="container-fluid partners-page">
	        <div class="page-header">
	            <div class="row">
	                <div class="col-xs-12">
	                    <i class="fa fa-briefcase" aria-hidden="true"></i>
	                    <h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
	                </div>
	            </div>
	        </div>
	        <div class="row">
	            <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
	                <hr />

	                <div class="top-selectors">
	                    <select id="coutries" class="selectpicker show-tick js-coutries">
	                        <option selected="selected">Выбор страны</option>
	                    </select>
	                    <select id="cities" class="selectpicker show-tick js-cities">
	                        <option selected="selected">Выбор города</option>
	                    </select>
	                </div>

	                <xsl:apply-templates select="document('udata://partners/listElements///1000/1?extProps=country,partner_web,diler,adres,partner_phone,partner_email')/udata" />
	            </div>
	            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
	                <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
	                <xsl:apply-templates select="." mode="right_col" />
	            </div>
	        </div>
	    </div>
	</xsl:template>

	<xsl:template match="udata[@module='partners' and @method='listElements']">
		<div class="white-pnl clearfix">
            Ничего не найдено
        </div>
	</xsl:template>
	<xsl:template match="udata[@module='partners' and @method='listElements' and items/item]">
		<div class="dealers-table table-responsive js-dealers-table">
            <table>
                <thead>
                    <tr>
                        <th>Страна</th>
                        <th>Дилер</th>
                        <th>Адрес</th>
                        <th>Телефон</th>
                        <th>E-mail</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- <xsl:apply-templates select="items/item" /> -->
                </tbody>
            </table>
        </div>
	</xsl:template>

	<xsl:template match="udata[@module='partners' and @method='listElements']/items/item" >
		<tr>
            <td><xsl:value-of select=".//property[@name='country']/value/item/@name" /></td>
            <td>
            	<xsl:choose>
					<xsl:when test=".//property[@name='partner_web']/value">
						<a href="{.//property[@name='partner_web']/value}"><xsl:value-of select=".//property[@name='diler']/value" disable-output-escaping="yes"/></a>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select=".//property[@name='diler']/value" disable-output-escaping="yes" />
					</xsl:otherwise>
				</xsl:choose>
			</td>
            <td><xsl:value-of select=".//property[@name='adres']/value" disable-output-escaping="yes" /></td>
            <td><xsl:value-of select=".//property[@name='partner_phone']/value" disable-output-escaping="yes" /></td>
            <td>
            	<xsl:if test=".//property[@name='partner_email']/value">
					<a href="mailto:{.//property[@name='partner_email']/value}"><xsl:value-of select=".//property[@name='partner_email']/value" disable-output-escaping="yes"/></a>
				</xsl:if>
			</td>
        </tr>
	</xsl:template>

</xsl:stylesheet>