<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi">

    <!-- Header menu -->
    <xsl:template match="udata[@module = 'menu']" mode="top_menu">
        <ul class="nav navbar-nav top js-top-menu">
            <li class="dropdown corporation js-corporation">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
					<xsl:value-of select="$settings//property[@name = 'nazvanie_verhnego_menyu']/value" disable-output-escaping="yes" />
					<span class="caret"></span>
				</a>
                <ul class="dropdown-menu">
                    <xsl:apply-templates select="item" mode="top_menu" />
                    <li class="kerr top_partner_logo">
                        <a href="http://kerruniversity.ru/?utm_source=orthodontia.ru&amp;utm_medium=headerlink&amp;utm_campaign=synergy">
                            <img src="{$template-resources}img/kerr.png" alt="Kerr" />
                        </a>
                    </li>
                    <li class="kavo top_partner_logo">
                        <!-- <a href="https://kavouniversity.ru/?utm_source=orthodontia.ru&amp;utm_medium=headerlink&amp;utm_campaign=synergy"> -->
                        <a href="http://www.dexisuniversity.ru/?utm_source=orthodontia.ru&amp;utm_medium=headerlink&amp;utm_campaign=synergy">
                            <img src="{$template-resources}img/kavo.png" style="max-width: 100%;" alt="Kavo" />
                        </a>
                    </li>
                    <li class="nb top_partner_logo">
                        <a href="https://store.nobelbiocare.com/ru/ru/courses?utm_source=orthodontia.ru&amp;utm_medium=headerlink&amp;utm_campaign=synergy">
                            <img src="{$template-resources}img/nb.png" alt="Nobel Biocare" />
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </xsl:template>

    <xsl:template match="udata[@module = 'menu']/item" mode="top_menu">
        <li>
            <xsl:if test="@id = $document-page-id or @id = $parents/page/@id">
                <xsl:attribute name="class">active</xsl:attribute>
            </xsl:if>
            <a href="{@link}">
                <xsl:value-of select="node()" />
            </a>
        </li>
    </xsl:template>


    <!-- main menu -->
    <xsl:template match="udata[@module = 'menu']" mode="main_menu">
        <ul class="nav navbar-nav bottom js-main-menu">
            <xsl:apply-templates select="item" mode="main_menu" />
            <li class=" academy js-academy">
                <a href="https://orthodontexpert.ru/" target="_blank"><i class="fa fa-graduation-cap" aria-hidden="true"></i>Школа ортодонтии</a>
            </li>
            <xsl:apply-templates select="$user" mode="header_auth_link" />
        </ul>
    </xsl:template>

    <xsl:template match="udata[@module = 'menu']//item" mode="main_menu">
        <li>
            <xsl:if test="@id = $document-page-id or @id = $parents/page/@id">
                <xsl:attribute name="class">active</xsl:attribute>
            </xsl:if>
            <a href="{@link}">
                <xsl:value-of select="node()" />
            </a>
        </li>
    </xsl:template>

    <xsl:template match="udata[@module = 'menu']//item[items/item]" mode="main_menu">
        <li class="dropdown">
            <xsl:if test="@id = $document-page-id or @id = $parents/page/@id or items/item/@id = $document-page-id or items/item/@id = $parents/page/@id">
                <xsl:attribute name="class">dropdown active</xsl:attribute>
            </xsl:if>
            <a href="{@link}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <xsl:apply-templates select="current()" mode="main_menu_event_text" />
                <xsl:value-of select="node()" />
                <span class="caret"></span>
            </a>
            <xsl:apply-templates select="items[item]" mode="main_sub_menu" />
        </li>
    </xsl:template>

    <xsl:template match="item" mode="main_menu_event_text" />

    <xsl:template match="item[@id = '4']" mode="main_menu_event_text">
        <xsl:attribute name="data-gta-event-text">мероприятия_меню</xsl:attribute>
    </xsl:template>

    <xsl:template match="item[@id = '267']" mode="main_menu_event_text">
        <xsl:attribute name="data-gta-event-text">библиотека_меню</xsl:attribute>
    </xsl:template>

    <xsl:template match="items" mode="main_sub_menu">
        <ul class="dropdown-menu">
            <xsl:apply-templates select="item" mode="main_sub_menu" />
        </ul>
    </xsl:template>

    <xsl:template match="item" mode="main_sub_menu">
        <li>
            <xsl:if test="@id = $document-page-id or @id = $parents/page/@id">
                <xsl:attribute name="class">active</xsl:attribute>
            </xsl:if>
            <a href="{@link}">
                <xsl:value-of select="node()" />
            </a>
        </li>
    </xsl:template>

    <xsl:template match="item[items/item]" mode="main_sub_menu">
        <li class="dropdown dropdown-submenu">
            <xsl:if test="@id = $document-page-id or @id = $parents/page/@id or items/item/@id = $document-page-id or items/item/@id = $parents/page/@id">
                <xsl:attribute name="class">dropdown dropdown-submenu active</xsl:attribute>
            </xsl:if>
            <a href="{@link}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <xsl:value-of select="node()" />
            </a>
            <xsl:apply-templates select="items" mode="main_sub_menu" />
        </li>
    </xsl:template>

	<!-- right_menu -->
    <xsl:template match="udata[@module = 'menu']" mode="right_menu">
        <div class="right-nav" role="navigation">
            <ul>
                <xsl:apply-templates select="item" mode="right_menu" />
            </ul>
        </div>
    </xsl:template>

    <xsl:template match="udata[@module = 'menu']/item" mode="right_menu">
        <li class="item{@id}">
            <xsl:if test="@id = $document-page-id or @id = $parents/page/@id or (concat('/',$module,'/',$method,'/') = @link)">
                <xsl:attribute name="class">active <xsl:value-of select="concat('item', @id)" /></xsl:attribute>
            </xsl:if>

            <a href="{@link}">
                <xsl:value-of select="node()" />
            </a>
        </li>
    </xsl:template>
</xsl:stylesheet>