<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">
    <xsl:include href="purchase/payment.xsl" />

    <xsl:template match="/result[@method = 'purchase']">
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
	                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

        <!--Breadcrumbs -->
        <xsl:apply-templates select="document('udata://core/navibar/')/udata" />

        <!--Content -->
        <div class="container-fluid news-page">
            <div class="page-header">
                <div class="row">
                    <div class="col-xs-12">
                        <i class="fa fa-credit-card" aria-hidden="true">
                            <xsl:if test=".//property[@name='icon_style']/value">
                                <xsl:attribute name="class"><xsl:value-of select="concat('fa ',.//property[@name='icon_style']/value)" /></xsl:attribute>
                            </xsl:if>
                        </i><h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <div class="white-pnl">
                        <article class="content_wrap" umi:element-id="{$document-page-id}" umi:field-name="content" umi:empty="&empty-page-content;">
                            <xsl:apply-templates select="document('udata://emarket/purchase')" />
                        </article>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
                    <xsl:apply-templates select="document('udata://menu/draw/right_menu')/udata" mode="right_menu" />
                    <xsl:apply-templates select="." mode="right_col" />
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="purchasing">
        <h4>
            <xsl:text>Purchase is in progress: </xsl:text>
            <xsl:value-of select="concat(@stage, '::', @step, '()')" />
        </h4>
    </xsl:template>

    <xsl:template match="purchasing[@stage = 'result']">
        <p>
            <xsl:text>&emarket-order-failed;</xsl:text>
        </p>
    </xsl:template>

    <xsl:template match="purchasing[@stage = 'result' and @step = 'successful']">
        <xsl:variable name="order" select="document(concat('uobject://', order/@id))" />
        <xsl:variable name="event" select="document(concat('upage://', $order//property[@name = 'page_id']/value))" />
        <xsl:variable name="event_type" select="$event//property[@name = 'event_type']//item[position() = 1]/@id" />
        <xsl:variable name="event_price" select="$event//property[@name = 'price']/value" />

        <h3 style="line-height:30px">Вы успешно зарегистрировались на мероприятие <xsl:value-of select="document(concat('udata://emarket/eventInfo/', order/@id))/udata" disable-output-escaping="yes" /> !</h3>

        <p>Номер Вашей регистрации <xsl:value-of select="document(concat('udata://emarket/eventOrderNum/', order/@id))/udata" disable-output-escaping="yes" /> (отправлен на электронную почту)</p>

        <xsl:choose>
            <xsl:when test="($event_type = 1278 or $event_type = 1280 or $event_type = 1282 or $event_type = 1281 or $event_type = 1971 or $event_type = 1901823) and not($event_price > 0)">
                <xsl:apply-templates select="." mode="free_event_result" />
            </xsl:when>
            <xsl:when test="$event_type = 1279 and not($event_price > 0)">
                <xsl:apply-templates select="." mode="free_webinar_result">
                    <xsl:with-param name="link" select="$event/udata/page/@link" />
                </xsl:apply-templates>
            </xsl:when>
            <xsl:when test="$event_type = 1280 and $event_price > 0">
                <xsl:apply-templates select="." mode="partner_event_result" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="." mode="common_event_result" />
            </xsl:otherwise>
        </xsl:choose>

        <!--<p>Информацию о мероприятии можно просматривать в <a href="/my-events/">&personal-account;</a></p>-->
        <!--<xsl:value-of select="document('udata://emarket/eventOrderPdf/')/udata" disable-output-escaping="yes" />-->
        
        <div class="ga_successful" data-id="{order/@id}" data-price="{$event_price}" data-product_name="{$event//property[@name = 'h1']/value}" data-product_id="{$order//property[@name = 'page_id']/value}" data-product_price="{$event_price}" data-event_category="{$event_type}" />

        <p>&nbsp;</p>
        <p>Перейти к:</p>
        <ul>
            <li><a href="{document('udata://content/get_page_url/&all_event_pid;')/udata}">Мероприятиям</a>&nbsp;</li>
            <li><a href="{document('udata://content/get_page_url/&library_pid;')/udata}">Библиотеке</a></li>
            <li><a href="{document('udata://content/get_page_url/&webinars_pid;')/udata}">Вебинарам</a></li>
            <li><a href="https://ormco.ru/">покупкам в Интернет-магазине Ormco</a></li>
        </ul>

        <xsl:call-template name="modal_interview" />
    </xsl:template>


    <xsl:template match="purchasing | udata" mode="free_event_result">
        <p>Будем Вас ждать!</p>
    </xsl:template>

    <xsl:template match="purchasing | udata" mode="free_webinar_result">
        <xsl:param name="link" select="'#'" />

        <p>Ссылка для подключения к вебинару будет отправлена на ваш email в течение 15 минут. Если вы не получили ссылку, пожалуйста, свяжитесь с нашим менеджером. Контакты вы найдете <a href="{$link}" target="_blank">тут</a>.</p>
        <p>Будем Вас ждать!</p>
    </xsl:template>

    <xsl:template match="purchasing | udata" mode="partner_event_result">
        <p>Не забудьте, пожалуйста, оплатить участие в мероприятии.</p>
        <p>Будем Вас ждать!</p>
    </xsl:template>

    <xsl:template match="purchasing | udata" mode="common_event_result">
        <p>После проверки регистрации менеджером Ormco на вашу электронную почту придет подтверждение, либо корректировка выбранной скидки, а также информация о дальнейшей оплате Вашего участия.</p>
        <p>Если ничего не происходит в течение 3 рабочих дней, свяжитесь с нашими менеджера по телефонам, указанным на сайте <a href="/contacts/">orthodontia.ru</a></p>
        <p>Пожалуйста, ожидайте!</p>
    </xsl:template>
</xsl:stylesheet>