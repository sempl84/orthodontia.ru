<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="blocks/price.xsl" />
	<xsl:include href="pages/personal.xsl" />
	<xsl:include href="pages/purchase.xsl" />
	<xsl:include href="pages/registerEventOneClick.xsl" />
</xsl:stylesheet>