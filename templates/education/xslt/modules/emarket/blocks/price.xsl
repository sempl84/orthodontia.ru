<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="udata[@method = 'price']">
        <xsl:param name="currency-suffix"/>

        <xsl:apply-templates select="price">
            <xsl:with-param name="currency-suffix" select="$currency-suffix" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="price">
        <xsl:param name="currency-suffix" select="./@suffix"/>

        <span class="btn btn-blue">
            <xsl:value-of select="concat(@prefix, ' ', format-number(actual, '#&#160;###','price'), ' ', $currency-suffix)" />
        </span>
    </xsl:template>

    <xsl:template match="price[original]">
        <xsl:param name="currency-suffix" select="@suffix"/>
        
        <span class="btn old-price">
            <xsl:value-of select="concat(@prefix, ' ', format-number(original, '#&#160;###','price'), ' ', $currency-suffix)" />
        </span>
        <span class="btn btn-blue">
            <xsl:value-of select="concat(@prefix, ' ', format-number(actual, '#&#160;###','price'), ' ', $currency-suffix)" />
        </span>
    </xsl:template>

    <xsl:template match="price[not(actual &gt; 0)]">
        <span class="btn btn-orange">Бесплатно</span>
    </xsl:template>


    <xsl:template match="udata[@method = 'price']" mode="short_event_price">
        <xsl:param name="loyalty" select="''"/>
        <xsl:param name="currency-suffix" select="@suffix"/>

        <xsl:apply-templates select="price" mode="short_event_price">
            <xsl:with-param name="loyalty" select="$loyalty"/>
            <xsl:with-param name="currency-suffix" select="$currency-suffix" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="price" mode="short_event_price">
        <xsl:param name="loyalty" select="''"/>
        <xsl:param name="currency-suffix" select="@suffix"/>

        <span class="price">
            <xsl:value-of select="concat(@prefix, ' ', format-number(actual, '#&#160;###','price'), ' ', $currency-suffix)" />

            <xsl:if test="not($loyalty = '') and $loyalty/discount_loyalty/@isallow &gt; 0">
                <xsl:call-template name="discount_loyalty_short" />
            </xsl:if>
        </span>
    </xsl:template>

    <xsl:template match="price[original]" mode="short_event_price">
        <xsl:param name="loyalty" select="''"/>
        <xsl:param name="currency-suffix" select="@suffix"/>

        <span class="price">
            <span class="old-price">
                <xsl:value-of select="concat(@prefix, ' ', format-number(original, '#&#160;###','price'), ' ', $currency-suffix)" />
            </span>
            <span class="new-price">
                <xsl:value-of select="concat(@prefix, ' ', format-number(actual, '#&#160;###','price'), ' ', $currency-suffix)" />
            </span>

            <xsl:if test="not($loyalty = '') and $loyalty/discount_loyalty/@isallow &gt; 0">
                <xsl:call-template name="discount_loyalty_short" />
            </xsl:if>
        </span>
    </xsl:template>

    <xsl:template match="price[not(actual &gt; 0)]" mode="short_event_price">
        <xsl:attribute name="class">price-border free_price</xsl:attribute>
        <span class="price ">Бесплатно</span>
    </xsl:template>
</xsl:stylesheet>