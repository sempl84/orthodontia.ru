<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="udata[@module='slider' and @method='listElements']" mode="slider_block"></xsl:template>
	<xsl:template match="udata[@module='slider' and @method='listElements' and items/item]" mode="slider_block">
        <div class="col-lg-9 col-md-8">
            <div class="row carousel-holder">
                <div class="col-md-12">
                    <div id="slider" class="carousel slide top-slider" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <xsl:apply-templates select="items/item" mode="carousel-indicators" />
                        </ol>
                        <div class="carousel-inner">
                            <xsl:apply-templates select="items/item" mode="slider_block" />
       
                        </div>
                        <a class="left carousel-control" href="#slider" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#slider" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
	</xsl:template>
	
	<xsl:template match="udata[@module='slider' and @method='listElements']/items/item" mode="slider_block">	
		
		
		<div class="item">
			<xsl:if test="position()=1">
				<xsl:attribute name="class">item active</xsl:attribute>
			</xsl:if>
            <div class="row">
            	<xsl:if test=".//property[@name='analytics_flag']/value">
            		<xsl:attribute name="class">row ga_addImpression</xsl:attribute>
            		<xsl:attribute name="data-name"><xsl:value-of select="text()" /></xsl:attribute>
            		<xsl:attribute name="data-category">Лендинг</xsl:attribute>
            		<xsl:attribute name="data-list">Лендинг</xsl:attribute>
            		<xsl:attribute name="data-position"><xsl:value-of select="position()" /></xsl:attribute>
            	</xsl:if>
                <div class="col-md-6 col-sm-6 info">
                    <a href="{.//property[@name='link_for_slide']/value}" ><div class="header2"><xsl:value-of select="text()" /></div></a>
                    <xsl:value-of select=".//property[@name='content']/value" disable-output-escaping="yes" />
                    <a class="btn btn-primary" href="{.//property[@name='link_for_slide']/value}" >
                    	<xsl:if test=".//property[@name='analytics_flag']/value">
                    		<xsl:attribute name="class">btn btn-primary ga_eventclick</xsl:attribute>
                    	</xsl:if>
                    	Подробнее
                    </a>
                </div>
                <div class="col-md-6 col-sm-6 hidden-xs">
                    <xsl:if test=".//property[@name='right_img']/value">
                    	<a href="{.//property[@name='link_for_slide']/value}" >
                    		<xsl:if test=".//property[@name='analytics_flag']/value">
	                    		<xsl:attribute name="class">ga_eventclick</xsl:attribute>
	                    	</xsl:if>
                    		<img class="img-responsive" src="{.//property[@name='right_img']/value}" alt="{text()}" />
                    	</a>
                    </xsl:if>
                </div>
            </div>
        </div>
	</xsl:template>
	
	<xsl:template match="udata[@module='slider' and @method='listElements']/items/item" mode="carousel-indicators">	
		<li data-target="#slider" data-slide-to="{position()-1}">
			<xsl:if test="position()=1">
				<xsl:attribute name="class">active</xsl:attribute>
			</xsl:if>
		</li>
	</xsl:template>

</xsl:stylesheet>