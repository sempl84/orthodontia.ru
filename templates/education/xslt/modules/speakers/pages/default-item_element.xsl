<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'speakers' and @method='item_element']">
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
		                <xsl:apply-templates select="document('udata://menu/draw/right_menu_events')/udata" mode="right_menu" />
					</div>
				</div>
			</div>
		</nav>

		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

		<!--Content -->
	    <div class="container-fluid speakers-page">
	    	<div class="page-header">
	            <div class="row">
	                <div class="col-xs-12">
	                    <i class="fa fa-users" aria-hidden="true"></i>
	                    <h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
	                </div>
	            </div>
	        </div>
	        <div class="row">

	            <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
	                <div class="white-pnl speaker-info">
	                	<div class="left">
	                		<xsl:choose>
								<xsl:when test=".//property[@name='photo']/value">
									<img src="{.//property[@name='photo']/value}" class="img-responsive" alt="{text()}" />
								</xsl:when>
								<xsl:otherwise>
									<img src="&empty-photo-speaker;" class="img-responsive" alt="{text()}" />
								</xsl:otherwise>
							</xsl:choose>
		                    <!-- <a href="{@link}">
		                    	<xsl:call-template name="makeThumbnailFull_ByPath">
									<xsl:with-param name="source" select=".//property[@name='photo']/value" />
									<xsl:with-param name="width" select="80" />
									<xsl:with-param name="height" select="80" />

									<xsl:with-param name="empty">&empty-photo-speaker;</xsl:with-param>
									<xsl:with-param name="element-id" select="$document-page-id" />
									<xsl:with-param name="field-name" select="'photo'" />

									<xsl:with-param name="alt" select=".//property[@ame='h1']/value" />
									<xsl:with-param name="class" select="'img-responsive'" />
								</xsl:call-template>
		                    </a> -->
		                </div>

		                <div class="right">
		                    <p umi:element-id="{@id}" umi:field-name="short_desrc" umi:empty="&empty-page-content;">
								<xsl:value-of select=".//property[@name = 'short_desrc']/value" disable-output-escaping="yes" />
							</p>
							<p>
								<a class="link-right-arrow" href="&all_event_url;?filter[speaker][2]={page/name}">Мероприятия спикера <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                			</p>
		                </div>


		                <div class="about">
		                	<div class="content_wrap" umi:element-id="{@id}" umi:field-name="full_descr2" umi:empty="&empty-page-content;">
								<xsl:value-of select=".//property[@name = 'full_descr2']/value" disable-output-escaping="yes" />
		                	</div>
		                </div>
	                    <!-- <article umi:element-id="{$document-page-id}" umi:field-name="full_descr2" umi:empty="&empty-page-content;">
							<xsl:value-of select=".//property[@name = 'full_descr2']/value" disable-output-escaping="yes" />

	                    </article> -->

	                    <hr/>

	                    <xsl:call-template name='social_links'>
	                    	<xsl:with-param name="class" select="'pull-right'"/>
	                    </xsl:call-template>
						<a class="link-right-arrow" href="../">Вернуться к списку спикеров <i class="fa fa-angle-right" aria-hidden="true"></i></a>

	                </div>

	                <!-- Все мероприятия спикера-->
	                <!-- <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalogPro//&all_event_pid;/10/1/2///(', page/@object-id ,')?extProps=speaker,publish_date,mesto_provedeniya,event_type,level,organizer,price_string'))/udata" mode="speaker_events"/>
	                 -->

	            </div>

	            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
	                <xsl:apply-templates select="document('udata://menu/draw/right_menu_events')/udata" mode="right_menu" />
	                <xsl:apply-templates select="." mode="right_col" />
	            </div>
	        </div>
	    </div>
	</xsl:template>

	<xsl:template match="udata" mode="speaker_events"/>
	<xsl:template match="udata[lines/item]" mode="speaker_events">
		<div class="content-col events-page">
        	<h3>Мероприятия спикера</h3>
            <ul class="items">
	            <xsl:apply-templates select="lines/item" />
	        </ul>
	    </div>

	</xsl:template>

</xsl:stylesheet>