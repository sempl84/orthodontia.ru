<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'library' and @method='item_element']">
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
		                <xsl:apply-templates select="user" mode="right_col_auth" />
					</div>
				</div>
			</div>
		</nav>

		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

		<!--Content -->
	    <div class="container-fluid library">
	    	<div class="page-header">
	            <div class="row">
	                <div class="col-xs-12">
	                    <i class="fa fa-book" aria-hidden="true"></i>
	                    <h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
	                </div>
	            </div>
	        </div>
	        <div class="row">
	            <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
	                <div class="white-pnl">
	                    <article class="content_wrap" umi:element-id="{$document-page-id}" umi:field-name="content" umi:empty="&empty-page-content;">
							<xsl:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes" />

							<xsl:if test=".//property[@name = 'video']/value">
								<div class="video">
									<xsl:value-of select=".//property[@name = 'video']/value" disable-output-escaping="yes" />
								</div>
							</xsl:if>

	                    </article>
	                    <hr/>
	                    <div class="items">
		                    <div class="item">
		                    	<xsl:apply-templates select=".//group[@name='files_arr' and property[@type='file']/value]" mode="library_files_arr" >
									<xsl:with-param name="pageId" select="$document-page-id" />
								</xsl:apply-templates>
		                    </div>
	                    </div>

	                    <xsl:call-template name='social_links'>
	                    	<xsl:with-param name="class" select="'pull-right'"/>
	                    </xsl:call-template>
	                    <a class="link-right-arrow" href="../">Вернуться к списку материалов <i class="fa fa-angle-right" aria-hidden="true"></i></a>

	                </div>
	            </div>

	            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
	                <xsl:apply-templates select="user" mode="right_col_auth" />

	                <xsl:apply-templates select="." mode="right_col" />
	            </div>
	        </div>
	    </div>
	</xsl:template>
</xsl:stylesheet>