<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'library']">
		<nav class="submenu">
			<div class="submenu__inner">
				<div class="submenu__top">
					Меню раздела
				</div>
				<div class="submenu__content">
					<div class="submenu__content-inner">
		                <xsl:apply-templates select="document('udata://menu/draw/right_menu_events')/udata" mode="right_menu" />
		                <!--<xsl:apply-templates select="user" mode="right_col_auth" />-->
					</div>
				</div>
			</div>
		</nav>

		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />

		<!--Content -->
	    <div class="container-fluid library">
	        <div class="page-header">
	            <div class="row">
	                <div class="col-xs-12">
	                    <i class="fa fa-book" aria-hidden="true"></i>
	                    <h1><xsl:value-of select="@header" disable-output-escaping="yes" /></h1>
	                </div>
	            </div>
	        </div>
	        <div class="row">

	            <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
	                <xsl:apply-templates select="document('udata://catalog/search/&library_pid;')/udata" />

	                <xsl:apply-templates select="document('udata://library/listElements/?extProps=library_type,library_allow,short_desrc&amp;extGroups=files_arr')/udata" />
	            </div>
	            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
                    <xsl:apply-templates select="document('udata://menu/draw/right_menu_events')/udata" mode="right_menu" />

	                <xsl:apply-templates select="user" mode="right_col_auth" />

	                <xsl:apply-templates select="." mode="right_col" />
	            </div>
	        </div>
	    </div>
	</xsl:template>

	<xsl:template match="udata[@module='library' and @method='listElements']">
		<div class="white-pnl clearfix">
            Ничего не найдено
        </div>
	</xsl:template>
	<xsl:template match="udata[@module='library' and @method='listElements' and items/item]">
		<ul class="items">
            <xsl:apply-templates select="items/item" />
        </ul>
        <xsl:apply-templates select="total" />
	</xsl:template>

	<xsl:template match="udata[@module='library' and @method='listElements']/items/item" >

			<li class="item">
                <div class="white-pnl clearfix">
                    <div class="left">
                    	<xsl:choose>
                    		<!-- TODO права доступа -->
							<xsl:when test=".//property[@name='library_allow']/value = &disallow-oid;">
								<img src="{$template-resources}img/lock-icon.png" />
							</xsl:when>
							<xsl:when test=".//property[@name='library_type']/value/item/@id = &library_video-oid;">
								<img src="{$template-resources}img/video-icon.png" />
							</xsl:when>
							<xsl:otherwise>
								<img src="{$template-resources}img/document-icon.png" />
							</xsl:otherwise>
						</xsl:choose>

                    </div>
                    <div class="right">
                        <a href="{@link}"><h4><xsl:value-of select="text()" disable-output-escaping="yes" /></h4></a>
                        <div umi:element-id="{@id}" umi:field-name="short_desrc" umi:empty="&empty-page-content;">
							<xsl:value-of select=".//property[@name = 'short_desrc']/value" disable-output-escaping="yes" />

							<xsl:apply-templates select=".//group[@name='files_arr' and property[@type='file']/value]" mode="library_files_arr" >
								<xsl:with-param name="pageId" select="@id" />
							</xsl:apply-templates>
						</div>
					</div>
                </div>
            </li>
	</xsl:template>

	<!-- шаблон для вывода прикрепленных файлов -->
	<xsl:template match="group" mode="library_files_arr">
		<xsl:param name="pageId" />

		<xsl:apply-templates select="property[@type='file']" mode="library_files_arr" >
			<xsl:with-param name="pageId" select="$pageId" />
		</xsl:apply-templates>
	</xsl:template>

	<xsl:template match="property" mode="library_files_arr">
		<xsl:param name="pageId" />
		<xsl:variable name="file_name_field" select="concat(@name,'_name')" />

		<div class="document">
			<a class="js-count-download" href="{value}" data-count_url="{$pageId}/{@name}" target="_blank">
				<i class="fa fa-file-text" aria-hidden="true"></i>
				<xsl:value-of select="../property[@name=$file_name_field]/value" />
			</a>
		</div>
	</xsl:template>

	<xsl:template match="item[@auth-required = '1']//property" mode="library_files_arr">
		<xsl:variable name="file_name_field" select="concat(@name,'_name')" />

		<div class="document">
			<a href="{ancestor::item/@link}">
				<i class="fa fa-file-text" aria-hidden="true"></i>
				<xsl:value-of select="../property[@name=$file_name_field]/value" />
			</a>
		</div>
	</xsl:template>
</xsl:stylesheet>