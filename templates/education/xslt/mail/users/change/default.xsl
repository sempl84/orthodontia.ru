<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output encoding="utf-8" method="html" indent="yes" />

    <xsl:template match="confirm_email_letter_subject">
        <xsl:text>Подтверждение изменения e-mail адреса</xsl:text>
    </xsl:template>

    <xsl:template match="confirm_email_letter_content">
        <p>
            <xsl:text>Здравствуйте!</xsl:text>
            <br />
            <xsl:text>Кто-то, возможно Вы, пытается изменить контактный e-mail для пользователя "</xsl:text>
            <xsl:value-of select="login" />
            <xsl:text>" на сайте </xsl:text>
            <a href="http://{domain}">
                <xsl:value-of select="domain" />
            </a>.
        </p>
        <p>
            <xsl:text>Если это не Вы, просто проигнорируйте данное письмо. Не волнуйтесь, Ваши данные в безопасности!</xsl:text>
        </p>
        <p>
            <xsl:text>Если Вы действительно хотите изменить свой контакнтый e-mail, кликните по этой ссылке:</xsl:text>
            <br />
            <a href="{change_link}">
                <xsl:value-of select="change_link" />
            </a>
        </p>
        <p>
            <xsl:text>С уважением,</xsl:text>
            <br />
            <b>
                <xsl:text>Администрация сайта </xsl:text>
                <a href="http://{domain}">
                    <xsl:value-of select="domain" />
                </a>
            </b>
        </p>
    </xsl:template>
</xsl:stylesheet>