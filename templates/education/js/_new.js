$(document).ready(function () {
  /**
   * Download list
   */

  var downloadList = {
    $list: $('#dealers-list'),
    $url: '/templates/education/speakers.html',

    init: function () {
      var $this = this;

      // Resize
      $(window).resize(function() {
        if ($(window).width() <= 991) {
          $('.download-list').removeClass('download-list_sticky');
        }
      });

      // Scroll
      $(window).scroll(function() {
        if ($(document).scrollTop() > 320 && $(window).width() > 991) {
          $('.download-list').addClass('download-list_sticky');
        } else {
          $('.download-list').removeClass('download-list_sticky');
        }
      });

      // Click on "checkbox"
      $this.$list.on('click', '.js--pdf-download-item', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var $target = $(e.currentTarget ? e.currentTarget : e.target);
        var isActive = $target.hasClass('card-item__checkbox_checked');

        if (!isActive) {
          $target.addClass('card-item__checkbox_checked');
        } else {
          $target.removeClass('card-item__checkbox_checked');
        }
      });

      // Click on download link
      $('.download-list').on('click', '.js--pdf-download-link', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var pdfIdsArray = [];

        $('.js--pdf-download-item')
          .each(function () {
            var $item = $(this);
            if ($item.hasClass('card-item__checkbox_checked')) {
              var pdfId = $item.data('pdf-id');
              pdfIdsArray.push(pdfId);
            }
          });

        if (pdfIdsArray.length > 0) {
          window.open('/catalog/genpdf/(' + pdfIdsArray.join(',') + ')', '_blank');
        }
      });
    }
  };


  /**
   * Filters
   */

  var filterContent = {
    SUBMIT_URL: '/udata/catalog/getSmartFilter/',
    $listWrapper: $('#dealers-list'),

    init: function () {
      var $this = this;

      // Add buttons for multiple select
      $('.multiple-custom').on('loaded.bs.select', function (e) {
        var $target = $(e.currentTarget ? e.currentTarget : e.target);

        var $dropdownMenu = $target.parent().find('div.dropdown-menu');
        var $dropdownMenuButtonContainer = $('<div class="dropdown-menu__button-container"></div>').appendTo($dropdownMenu);

        $('<a class="btn btn-primary btn_small js--filters-submit"></a>')
          .appendTo($dropdownMenuButtonContainer)
          .attr('href', '#')
          .text('Применить');
      });

      // On change
      $('.js--filters select').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
        var select = e.currentTarget ? e.currentTarget : e.target;
        var isMultipleSelect = select.multiple;

        if (!isMultipleSelect) {
          var params = $this.getParams();
          $this.submitData(params);
        }
      });

      // On click
      $(document).on('click', '.js--filters-submit', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var params = $this.getParams();
        var action = $(this).parents('.js--filters').data('action');
        //console.log(action);
        $this.submitData(params,action);
      });

      // On click
      $('.right-filters').on('click', '.js--right-filters', function(e) {
        e.preventDefault();
        e.stopPropagation();
        var $target = $(e.currentTarget ? e.currentTarget : e.target);

        if ($target.hasClass('right-filters__item-link_active')) {
          $target.removeClass('right-filters__item-link_active');
          $target
            .find('.right-filters__item-checkbox')
            .removeClass('right-filters__item-checkbox_checked');
        } else {
          $target.addClass('right-filters__item-link_active');
          $target
            .find('.right-filters__item-checkbox')
            .addClass('right-filters__item-checkbox_checked');
        }
      });
    },

    getParams: function () {
      var filters = {};

      $('.js--filters select')
        .each(function () {
            var item = this;

            if ($(item).val()) {
            	// if($(item).val() == 'Insignia'){
            		// $url = '/templates/education/speakers2.html';
            	// }else{
            		// $url = '/templates/education/speakers.html';
            	// }
//             	
            	// if($(item).val() == 'Москва'){
            		// $url = '/templates/education/speakers3.html';
            		// $("span.text:contains('Insignia')").attr('style','color:grey').parent().parent().find('.glyphicon.glyphicon-ok.check-mark').attr('style','color:grey');
            	// }
            	
              var filter = {};
              filter[item.name] = $(item).val();
              filters = Object.assign({}, filters, filter);
              
              console.log(filter);
              console.log(filters);
            }
          });

      return filters;
    },

    submitData: function (params,action) {
      var $this = this;
		
      $.ajax({
        type: 'GET',
        // url: $this.SUBMIT_URL,
        url: $this.$url,
        dataType: 'html',
        data: params,
        success: function (data) {
          $this.$listWrapper.html(data);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          console.error(XMLHttpRequest, textStatus, errorThrown);
        }
      });
    }
  };


  /**
   * Custom file input
   */

  var customFileInput = {
    init: function () {
      $('.input-file').before(function() {
        var $this = $(this);

        if (!$this.prev().hasClass('input-ghost')) {
          var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
          element.attr('name', $this.data('name'));
          element.change(function(){
            element.next(element).find('input').val((element.val()).split('\\').pop());
          });

          $this.click(function(){
            element.click();
          });

          // $this.find('button.btn-reset').click(function(){
          //   element.val(null);
          //   $this.parents('.input-file').find('input').val('');
          // });

          $this.find('input').css('cursor', 'pointer');
          $this.find('input').mousedown(function() {
            $this.parents('.input-file').prev().click();

            return false;
          });

          return element;
        }
      });
    }
  };


  // Init all
  downloadList.init();
  filterContent.init();
  customFileInput.init();
});
