var interviewModal = {
  modalSelector: '#interviewModal',
  modalSelectorResult: '#interviewModalResult',
  postDataUrl: '/udata/emarket/add_vote_comment.json',
  cookieName: 'interview',
  cookieExpireInDays: 7,
  starsThreshold: 4,

  init: function () {
    var that = this;
    var $modalSelector = $(that.modalSelector);

	 
	if(!parseInt($('.js-interview--orderId').val()) > 0){
		$('.js-interview--orderId').val(this.getCookie('o'));
	}
	
	
    // Modal visibility
    if (!this.getCookie(this.cookieName)) {
      $modalSelector.modal({ show: true, backdrop: 'static' });
    } else {
      $modalSelector.modal('hide');
    }


    // Set defaults
    that.clearErrors();
    that.setStarsActive();
    that.setCommentVisibility(true);


    // Actions
    $modalSelector.on('click', '.js-interview--send', function (e) {
      e.preventDefault();
		
	  
      var orderId = $('.js-interview--orderId').val();
      var starNumber = $('.js-interview--starNumber').val();
      var comment = $('.js-interview--comment').val();

      // Clear errors before validation
      that.clearErrors();

      // Validation & data send
      var isStarNumberZero = Number(starNumber) === 0;
      var isEmptyComment = starNumber < that.starsThreshold && comment === '' && $('.js-interview--comment-block').is(':visible');

      if (isStarNumberZero || isEmptyComment) {
        if (isStarNumberZero) {
          that.setErrors('Необходимо выбрать оценку от 1 до 5');
        }

        if (isEmptyComment) {
          that.setErrors('Поле комментария обязательно для заполнения');
        }
      } else {
        that.sendData({
          orderId: orderId,
          starNumber: starNumber,
          comment: comment
        });
      }
    });

    $modalSelector.on('click', '.js-interview--later', function (e) {
      e.preventDefault();

      // Clear errors
      that.clearErrors();

      $modalSelector.modal('hide');
      that.saveCookie();
    });

    $modalSelector
      .on('click', '.js-interview--star-item', function (e) {
      e.preventDefault();

      // Clear errors
      that.clearErrors();

      var starNumber = $(this).data('starnumber');
      $('.js-interview--starNumber').val(starNumber);
      that.setStarsActive();
      that.setCommentVisibility();

      // Clear comment if starNumber >= starsThreshold
      if (starNumber >= that.starsThreshold) {
        $('.js-interview--comment').val('');
      }
    })
      .on('mouseenter', '.js-interview--star-item', function () {
        var starNumber = $(this).data('starnumber');
        that.setStarsActive(starNumber);
      })
      .on('mouseleave', '.js-interview--star-item', function () {
        that.setStarsActive();
      });
  },

  saveCookie: function () {
    this.setCookie(this.cookieName, true, this.cookieExpireInDays);
  },

  setCookie: function (cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));

    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  },

  getCookie: function (cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];

      while (c.charAt(0) === ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return '';
  },

  sendData: function (data) {
    var that = this;
    var $modalSelector = $(that.modalSelector);

    $.ajax({
      type: 'POST',
      url: that.postDataUrl,
      dataType: 'json',
      data: data,
      //contentType: 'application/json; charset=utf-8',
      success: function (data) {
      	if(data.result == 1){
      		$modalSelector.modal('hide');
        	that.saveCookie();
        	
        	
        	$(that.modalSelector).modal('hide');
        	$(that.modalSelectorResult).modal('show');
        	setTimeout(function(){
				$(that.modalSelectorResult).modal('hide');
			}, 3000);
      	}else{
      		alert('Ошибка отправки формы');
      	}
        
      }
    });
  },

  setStarsActive: function (number) {
    var i = 0;
    var starNumber = number || $('.js-interview--starNumber').val();

    $('.js-interview--star-item')
      .removeClass('interview__stars-item_active')
      .each(function () {
        if (i < starNumber) {
          $(this).addClass('interview__stars-item_active');
        }

        i++;
      });
  },

  setCommentVisibility: function (isFirstLoad) {
    var that = this;
    var starNumber = $('.js-interview--starNumber').val();
    var commentBlock = $('.js-interview--comment-block');

    if (isFirstLoad && Number(starNumber) === 0) {
      commentBlock.hide();
    } else {
      if (Number(starNumber) < that.starsThreshold) {
        commentBlock.slideDown(300);
      } else {
        commentBlock.slideUp(300);
      }
    }
  },

  setErrors: function (content) {
    const $errorElement = $('.js-interview--errors');

    $('<div class="alert alert-danger" role="alert"></div>')
      .appendTo($errorElement)
      .html(content);

    $errorElement.fadeIn(150);
  },

  clearErrors: function() {
    $('.js-interview--errors')
      .empty()
      .hide();
  }
};
//if($('.js-interview--orderId').val() > 0){
if($('.js-interview--orderId').length > 0){
	interviewModal.init();
}else{
	// save order id
	var $o = parseInt($('#payment_choose').attr('o'));
	if($o > 0){
		interviewModal.setCookie('o', $o, 25);
	}
}

