jQuery(function ($) {
    $.datepicker.regional['ru'] = {
        closeText: 'Закрыть',
        prevText: '&#x3c;Пред',
        nextText: 'След&#x3e;',
        currentText: 'Сегодня',
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
		'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
		'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
        dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
        dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        dateFormat: 'dd.mm.yy', firstDay: 1,
        isRTL: false
    };
    $.datepicker.setDefaults($.datepicker.regional['ru']);
});

var seminars = {
    $wrapCalendar: null,
    $calendar: null,
    arrayDays: [],
    types: [],
    page_id: '',
    init: function (dates, types, page_id = '') {
        var cthis = this;
		this.page_id = page_id;
        this.$wrapCalendar = $('.js-calendar');
        if (this.$wrapCalendar.length != 0) {
            this.setData(dates, types);
            this.$calendar = $('<div/>').appendTo(this.$wrapCalendar).attr('class', 'calendar');
            this.$calendar.datepicker({
                showOtherMonths: true,
                beforeShowDay: function (date) {
                    var cday = cthis.getDay(date);
                    if (cday != null) {
                        return [true, cthis.types[cday.type].className +' '+cday.periodcss, cday.description];
                    }
                    return [true];
                },
                onSelect: function (dateText, inst) {
                    var paths = dateText.split('.')
                    var cday = cthis.getDay(new Date(parseInt(paths[2]), parseInt(paths[1]) - 1, parseInt(paths[0]), 0, 0, 0, 0));
                    if (cday != null) {
                        window.location = cday.link;
                    }
                }
            });
            this.setTypes();
        }
    },
    setData: function (dates, types) {
        this.arrayDays = dates;
        this.types = types;
    },
    setTypes: function () {
        var $wraper = $('<div/>').appendTo(this.$wrapCalendar).attr('class', 'seminar-types');
        for (var i = 0; i < this.types.length; i++) {
            $('<a/>').appendTo($wraper).attr('class', 'seminar-type-link ' + this.types[i].className).
            attr('href', this.types[i].link).text(this.types[i].description);
        }
    },
    getDay: function (date) {
        var cday = null;
        var m = date.getMonth(),
        d = date.getDate(),
        y = date.getFullYear();

//        for (var i = 0; i < this.arrayDays.length; i++) {
//			console.log(this.arrayDays[i]);
//            if (this.arrayDays[i].date == d + '.' + (m + 1) + '.' + y) {
//                cday = this.arrayDays[i];
//                if(this.arrayDays[i].type != 6){
//                    break;
//                }
//            }
//        }

		if(!(y in this.arrayDays)){
			this.arrayDays[y] = new Array();
		}

		if(!(m in this.arrayDays[y])){
			this.getDaysByMonthAndYear(m, y);
		}
        for (var i = 0; i < this.arrayDays[y][m].length; i++) {
            if (this.arrayDays[y][m][i].date == d + '.' + (m + 1) + '.' + y) {
                cday = this.arrayDays[y][m][i];
                if(this.arrayDays[y][m][i].type != 6){
                    break;
                }
            }
        }
        return cday;
    },
	getDaysByMonthAndYear: function(month, year){
		var days_of_month = new Array();
		$.ajax({
			type: "GET",
			dataType: "json",
			url: '/udata/catalog/get_month_for_' + (this.page_id == 694 ? 'webinar_' : '') + 'calendar/.json',
			data: {
				month: month,
				year: year,
				page_id: this.page_id,
			},
			async: false,
			success: function (data) {
				if(data.items.item){
					$.each(data.items.item, function(i, item) {
						days_of_month.push(item);
					});
				}
			}
		});
		this.arrayDays[year][month] = days_of_month;
	}
}

$(document).ready(function () {
    if (typeof settings != typeof undefined){
        seminars.init(settings.dates, settings.types, settings.page_id);
	}
});