var page_url = document.location.protocol + '//' + document.location.host + document.location.pathname;

$(document).ready(function () {
    window.dataLayer = window.dataLayer || [];

    $(document).on('click', '*[data-gta-event=ButtonClicks]', function () {
        sendGaEventButtonClicks($(this).text());
    });

    $(document).on('click', '.ui-datepicker-prev', function () {
        sendGaEventButtonClicks('календарь_назад');
    });

    $(document).on('click', '.ui-datepicker-next', function () {
        sendGaEventButtonClicks('календарь_вперед');
    });

    $('a.email').on('click', function() {
        sendGaEventButtonClicks('форма_шапка');
    });

    // Меню
    $('.js-main-menu a').click(function () {
        let text = $(this).data('gta-event-text') ?? $(this).text();

        sendGaEventButtonClicks(text);
    });

    $('.js-user-profile-form a').on('click', function() {
        sendGaEventButtonClicks($(this).text());
    });

    $('.reset_filter').click(function () {
        sendGaEventButtonClicks($(this).text());
    });

    $(document).on('click', '.js-calendar a', function () {
        sendGaEventButtonClicks('сбросить_фильтры');
    });

    // Ссылки
    $('a.ga_btn').click(function () {
      let text = $(this).text();
      text !== '' ? sendGaEventButtonClicks($(this).text()) : null;
    });

    $('.top-arrow').click(function () {
        sendGaEventButtonClicks('скролл_топ');
    });

    $('#auth .pull-right a').click(function () {
        sendGaEventButtonClicks($(this).text());
    });

    // Кнопка закрытия модального окна
    $('.close').click(function () {
        sendGaEventButtonClicks('Close');
    });

    // Ссылка на возврат к чему-то
    $('.link-right-arrow').click(function () {
        sendGaEventButtonClicks($(this).text());
    });

    // Календарь
    $('body').on('click', 'events-calendar .a, events-calendar .ui-datepicker-month, events-calendar .ui-datepicker-year', function () {
        sendGaEventButtonClicks($(this).text());
    });

    // Все кнопки
    $('body').on('click', '.btn', function () {
        let text = $(this).text() ?? $(this).val();

        if(!$(this).data('ga-no-event')&&$(this)[0].tagName!=='SPAN') {
            sendGaEventButtonClicks(text);
        }
    });

    // Все кнопки
    $('body').on('click', '.right-nav a', function () {
        sendGaEventButtonClicks($(this).val());
    });

    // Поиск
    $('.ga_search').submit(function () {
        sendGaEventButtonClicks($(this).find('.text-field').val());
    });

    // Чекбоксы
    $('form input[type="checkbox"]').on('change', function () {
      let name = null;
      if ($(this).attr('name')) {
        name = $(this).attr('name');
      } else {
        if (this.parentElement.querySelector('span')) {
          if (this.parentElement.querySelector('span').textContent.trim().indexOf(' ') >0) {
            let nname = this.parentElement.querySelector('span').textContent.trim().split(' ');
            name = [nname[0], nname[1]||'', nname[2]||''].join(' ');
          } else {
            name = this.parentElement.querySelector('span').textContent.trim()
          }
        } else {
          name = 'Чекбокс на странице ' + page_url;
        }
      }
      let obj = {
        'event': 'selector',
        'formName': name, //название селектора ( название переменной оставлено старое, чтобы не плодить сущности)
        'pageUrl': page_url,
        'selectorAdditionalFieldType': $(this).is(':checked') ? 'on' : 'off' //выбранный пользователем статус/селектор/доп параметр не являющийся текстовым полем, например on/off
      }
        dataLayer.push(obj);
        console.log(obj)
    });

    // Списки
    if ($('.ga_items_list').length > 0) {
        $('.ga_items_list').each(function () {
            let events = [];
            let list_name = $(this).data('name') ? $(this).data('name') : $('h1').text();
            $(this).find('.ga_items_list_item').each(function () {
                events.push({
                    'name': $(this).data('name'),
                    'id': $(this).data('id'),
                    'price': $(this).data('price'),
                    'category': $(this).data('event_category'),
                    'list': list_name,
                    'position': $(this).data('position')
                });
            });

            dataLayer.push({
                'event': 'ecommerceImpressions',
                'ecommerce': {
                    'impressions': events
                }
            });
        });

        $('.ga_items_list_item a').click(function () {
            let ga_items_list_item = $(this).closest('.ga_items_list_item');
            let list_name = $(this).closest('.ga_items_list').data('name') ? $(this).closest('.ga_items_list').data('name') : $('h1').text();
            dataLayer.push({
                'event': 'ecommerceClick',
                'ecommerce': {
                    'click': {
                        'actionField': {
                            'list': list_name
                        },
                        'products': [{
                            'name': $(ga_items_list_item).data('name'),
                            'id': $(ga_items_list_item).data('id'),
                            'price': $(ga_items_list_item).data('price'),
                            'category': $(ga_items_list_item).data('event_category'),
                            'position': $(ga_items_list_item).data('position')
                        }]
                    }
                }
            });
        });
    }

    // Страница консультации / мероприятия
    if ($('.ga_item').length > 0) {
        dataLayer.push({
            'event': 'ecommerceDetail',
            'ecommerce': {
                'detail': {
                    'actionField': {
                        'list': $('.ga_item').data('list')
                    },
                    'products': [{
                        'name': $('.ga_item').data('name'),
                        'id': $('.ga_item').data('id'),
                        'price': $('.ga_item').data('price'),
                        'category': $('.ga_item').data('event_category')
                    }]
                }
            }
        });
    }

    // Попытка заказать мероприятие
    $('.ga_item_add_basket').click(function () {
        dataLayer.push({
            'event': 'ecommerceAdd',
            'ecommerce': {
                'add': {
                    'products': [{
                        'name': $(this).data('name'),
                        'id': $(this).data('id'),
                        'price': $(this).data('price'),
                        'category': $(this).data('event_category'),
                        'quantity': 1
                    }]
                }
            }
        });
    });

    // Успешное оформление заказа
    if ($('.ga_successful').length > 0) {
        dataLayer.push({
            'event': 'ecommercePurchase',
            'ecommerce': {
                'purchase': {
                    'actionField': {
                        'id': $('.ga_successful').data('id'),
                        'revenue': $('.ga_successful').data('price'),
                    },
                    'products': [{
                        'name': $('.ga_successful').data('product_name'),
                        'id': $('.ga_successful').data('product_id'),
                        'price': $('.ga_successful').data('product_price'),
                        'category': $('.ga_successful').data('event_category'),
                        'quantity': 1
                    }]
                }
            }
        });
    }

    if (page_url.indexOf('ormco-stars') > 0) {
      document.addEventListener('click', (e)=>{
        const target = e.target;
        ormcoStarsGA(target);
      })
    }
});

function ormcoStarsGA(target) {
  let text = '';
  if (target.tagName ==='A') {
    let link = target;
    text = link.textContent;
    if (link.hasAttribute('target')&&link.getAttribute('target') === '_blank') {
      text = 'переход на страницу' + link.getAttribute('href');
      sendGaEventButtonClicks(text);
      return;
    }
    if (text !== (''&&null&&undefined)) {
      sendGaEventButtonClicks(text);
      return;
    }
    if (link.getAttribute('href').indexOf('#')>0) {
      if (link.getAttribute('href').replaceAll('#', '')!=='') {
        text = `плавный переход к блоку ${link.getAttribute('href').replaceAll('#', '')}`;
      } else {
        text = 'logo';
      }
      sendGaEventButtonClicks(text);
      return;
    }
  }
  if (target.classList.contains('t-cover__arrow')) {
    text = 'плавный переход ко второму блоку';
    sendGaEventButtonClicks(text);
    return;
  }
}

function sendGaEventButtonClicks(text) {
    console.log('ButtonClicks - ' + text);

    dataLayer.push({
        'event' : 'ButtonClicks',
        'pageUrl' : page_url,
        'clickText' : text
    });
}