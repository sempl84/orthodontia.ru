(function ($, jQuery) {
    var searchClinics = {
        $clinicsMapWrapper: null,
        $clinicsViewData: null,
        clinicsData: [],
        googleMap: null,
        markers: [],
        allCitiesValue: 'Выбрать город',
        init: function () {
            // Set defaults
            this.$clinicsMapWrapper = $('.js--clinics-map');
            this.$clinicsViewData = $('.js--clinics-view-data');

            // Set initial data
            this.clinicsData = clinics;

            // Init map
            if (this.$clinicsMapWrapper.length !== 0) {
                var mapOptions = {
                    center: new google.maps.LatLng(-34.397, 150.644),
                    zoom: 8,
                    scrollwheel: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                this.googleMap = new google.maps.Map(this.$clinicsMapWrapper[0], mapOptions);

                searchClinics.setClinicsData();
            }

            google.maps.event.addDomListener(this.$clinicsMapWrapper, 'mousedown', function () {
                google.maps.event.addListenerOnce(searchClinics.googleMap, "dragstart", function () {
                    isDragging = true;
                });
                isDragging = false;
            });
        },
        setClinicsData: function () {
            var countItem = 0;
            var lastLocation = null;
            var mapBounds = new google.maps.LatLngBounds();
            this.$clinicsViewData.empty();

            // Markers & data
            var markers = [];
            for (var i = 0; i < this.clinicsData.length; i++) {
                var item = this.clinicsData[i];
                var location = new google.maps.LatLng(item.lat, item.lng);
                lastLocation = location;

                if (!item.marker) {
                    item.marker = this.createClinicMarker(item, location, item.city_name);
                }

                markers.push(item.marker);
                mapBounds.extend(location);
                countItem++;
            }

            if (countItem === 1) {
                this.googleMap.setOptions({
                    center: lastLocation,
                    zoom: 10
                });
            } else {
                this.googleMap.fitBounds(mapBounds);
            }

            var markerCluster = new MarkerClusterer(this.googleMap, markers, {
                imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
            });
        },
        createClinicMarker: function (clinic, location, clinicCity) {
            var marker = new google.maps.Marker({
                id: clinic.id,
                position: location,
                title: clinic.title,
                icon: '/templates/education/img/marker_icon.png'
            });

            var infoWindow = new google.maps.InfoWindow({
                content: '<b>' + clinic.title + '</b><br/>г.' + clinicCity + ', ' + clinic.address + '<br/>' + clinic.phone
            });

            google.maps.event.addListener(marker, 'mouseover', function () {
                infoWindow.open(searchClinics.googleMap, marker);
                marker.setIcon('/templates/education/img/marker_icon_active.png');
            });
            google.maps.event.addListener(marker, 'mouseout', function () {
                infoWindow.close();
                marker.setIcon('/templates/education/img/marker_icon.png');
            });

            google.maps.event.addListener(marker, 'click', function () {
                searchClinics.googleMap.setZoom(14);
                searchClinics.googleMap.setCenter(marker.getPosition());
            });

            $(document)
                .on('mouseover', '.one_center_row[data-clinic-id="' + clinic.id + '"]', function (e) {
                    infoWindow.open(searchClinics.googleMap, marker);
                    marker.setIcon('/templates/education/img/marker_icon_active.png');
                })
                .on('mouseout', '.one_center_row[data-clinic-id="' + clinic.id + '"]', function (e) {
                    infoWindow.close();
                    marker.setIcon('/templates/education/img/marker_icon.png');
                });

            return marker;
        }
    };

    jQuery(document).ready(function () {
        searchClinics.init();
    });
})(jQuery, jQuery);