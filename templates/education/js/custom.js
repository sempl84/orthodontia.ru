var menuService = {
    init: function () {
        $(window).resize(function () {
            var className = '.js-main-menu';
            if ($(window).width() < 854) {
                className = '.js-top-menu';
                $('.js-academy').insertAfter(className + ' > li:last');
                $('.js-cabinet').insertAfter(className + ' > li:last');
                $('.js-top-menu > li.js-corporation').addClass('dropdown');
                $('.js-top-menu > li.js-corporation > ul').addClass('dropdown-menu');
            }
            else {
                $('.js-academy').insertAfter(className + ' > li:last');
                $('.js-cabinet').insertAfter(className + ' > li:last');
                $('.js-top-menu > li.js-corporation').removeClass('dropdown');
                $('.js-top-menu > li.js-corporation > ul').removeClass('dropdown-menu');
            }
        }).resize();

        $(window).scroll(function () {
            if ($(document).scrollTop() > 50) {
                $('nav').addClass('shrink');
            } else {
                $('nav').removeClass('shrink');
            }
        });
    }
};

var userProfileService = {
    init: function () {
        var $mainPanel = $('.js-user-profile-form');

        $mainPanel.on('submit', function(e) {
            if(!validateFormDadataAddress($(this))) {
                e.preventDefault();
            }
        });

        // default check agree on registrate
        $('#registrate, #registerModal').each(function(){
            $(this).find('[name="agree_fake"]').trigger('click');
            $(this).find('[name="data[new][agree]"]').val(true);
        })

        if ($mainPanel.length != 0) {
            var maskArmenia = function (val) {
                return val.replace(/\D/g, '').length == 13 ? '+374 (000) 0000000' : '+374 (00) 00000009';
            },
            optionArmenia = {
                placeholder: '+374 (___) ___-____',
                onKeyPress: function (val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };


            // Грузия +995 (000) 0000000
            // Украина +380 (00) 0000000
            // Азербайджан +994 (00) 0000000
            $mainPanel.find('#country668,#country').change(function () {
                console.log('counry');
                var $option = $(this).find('option:selected');
                $mainPanel.find('.js-phone').unmask().attr('placeholder', '');
                if ($option.data('mask') == 'maskArmenia') {
                    var Minlength = 18;
                    $mainPanel.find('.js-phone').addClass('show-field-placeholder').mask(maskArmenia, optionArmenia).attr('data-val-length-min', Minlength).rules('add', {
                        minlength: Minlength,
                    });
                } else if ($option.data('mask')) {
                    var Minlength = $option.data('mask').length;
                    $mainPanel.find('.js-phone').addClass('show-field-placeholder').mask($option.data('mask'), {
                        translation: {
                            'r': {
                                pattern: /[9]/,
                                fallback: '9'
                            },
                            'y': {
                                pattern: /[0]/,
                                fallback: '0'
                            }
                        }, placeholder: $option.data('mask-placeholder')
                    }).attr('data-val-length-min', Minlength).rules('add', {
                        minlength: Minlength,
                    });
                } else {
                    var Minlength = 11;
                    $mainPanel.find('.js-phone').removeClass('show-field-placeholder').attr('placeholder', 'Мобильный телефон*').attr('data-val-length-min', Minlength).rules('add', {
                        minlength: Minlength,
                    });
                }
            }).change();
            //$mainPanel.find('#clinic_phone').mask($option.data('mask'), { placeholder: $option.data('mask-placeholder') });
        }
    }
};

var photoSlider = {
    init: function () {
        var $pnl = $('.js-photo-slider');
        if ($pnl.length != 0) {
            var totalItems = $pnl.find('.item').length;
            var currentIndex = $pnl.find('div.active').index() + 1;

            $pnl.find('.js-numbers').html(currentIndex + ' / ' + totalItems);
            $pnl.bind('slid.bs.carousel', function () {
                var currentIndex = $pnl.find('div.active').index() + 1;
                $pnl.find('.js-numbers').html(currentIndex + ' / ' + totalItems);
            });
        }
    }
};


var initPhotoSwipeFromDOM = function (gallerySelector) {
    //add figure html tag from img alt tag
    $(gallerySelector).find('a').each(function () {
        var $lnk = $(this);
        var $img = $lnk.find('img:first');
        if ($lnk.find('figure').length == 0 && $img.length != 0) {
            var $figure = $('<figure/>').prependTo($lnk);
            if ($img.attr('alt')){
                $figure.text($img.attr('alt'));
            }
        } else {
            var $figure = $('<figure/>').appendTo($lnk);
            if ($lnk.attr('title')){
                $figure.text($lnk.attr('title'));
            }
        }
    });

    var parseThumbnailElements = function (el) {
        var thumbElements = $(el).find('a.js-photo').toArray(),
            numNodes = thumbElements.length,
            items = [],
            el,
            childElements,
            size,
            item;

        for (var i = 0; i < numNodes; i++) {
            el = thumbElements[i];

            // include only element nodes
            if (el.nodeType !== 1) {
                continue;
            }

            childElements = el.children;

            size = el.getAttribute('data-size').split('x');

            // create slide object
            item = {
                src: el.getAttribute('href'),
                w: parseInt(size[0], 10),
                h: parseInt(size[1], 10),
                author: ''
            };

            item.el = el; // save link to element for getThumbBoundsFn

            if (childElements.length > 0) {
                item.msrc = childElements[0].getAttribute('src'); // thumbnail url
                item.title = childElements[0].innerHTML;
            }

            var mediumSrc = el.getAttribute('data-med');
            if (mediumSrc) {
                size = el.getAttribute('data-med-size').split('x');
                // "medium-sized" image
                item.m = {
                    src: mediumSrc,
                    w: parseInt(size[0], 10),
                    h: parseInt(size[1], 10)
                };
            }
            // original image
            item.o = {
                src: item.src,
                w: item.w,
                h: item.h
            };

            items.push(item);
        }

        return items;
    };

    var closest = function closest(el, fn) {
        return el && (fn(el) ? el : closest(el.parentNode, fn));
    };

    var onThumbnailsClick = function (e) {
        e = e || window.event;
        e.preventDefault ? e.preventDefault() : e.returnValue = false;

        var eTarget = e.target || e.srcElement;

        var clickedListItem = closest(eTarget, function (el) {
            return el.tagName === 'A';
        });

        if (!clickedListItem) {
            return;
        }

        var clickedGallery = $(clickedListItem).parents('.js-photos:first')[0];

        var childNodes = $(clickedGallery).find('a').toArray(),
            numChildNodes = childNodes.length,
            nodeIndex = 0,
            index;

        for (var i = 0; i < numChildNodes; i++) {
            if (childNodes[i].nodeType !== 1) {
                continue;
            }

            if (childNodes[i] === clickedListItem) {
                index = nodeIndex;
                break;
            }
            nodeIndex++;
        }

        if (index >= 0) {
            openPhotoSwipe(index, clickedGallery);
        }
        return false;
    };

    var photoswipeParseHash = function () {
        var hash = window.location.hash.substring(1),
            params = {};

        if (hash.length < 5) { // pid=1
            return params;
        }

        var vars = hash.split('&');
        for (var i = 0; i < vars.length; i++) {
            if (!vars[i]) {
                continue;
            }
            var pair = vars[i].split('=');
            if (pair.length < 2) {
                continue;
            }
            params[pair[0]] = pair[1];
        }

        if (params.gid) {
            params.gid = parseInt(params.gid, 10);
        }

        return params;
    };

    var openPhotoSwipe = function (index, galleryElement, disableAnimation, fromURL) {
        var pswpElement = document.querySelectorAll('.pswp')[0],
            gallery,
            options,
            items;

        items = parseThumbnailElements(galleryElement);

        options = {
            galleryUID: galleryElement.getAttribute('data-pswp-uid'),
            getThumbBoundsFn: function (index) {
                // See Options->getThumbBoundsFn section of docs for more info
                var thumbnail = items[index].el.children[0],
                    pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                    rect = thumbnail.getBoundingClientRect();
                return {x: rect.left, y: rect.top + pageYScroll, w: rect.width};
            },
            addCaptionHTMLFn: function (item, captionEl, isFake) {
                if (!item.title) {
                    captionEl.children[0].innerText = '';
                    return false;
                }
                captionEl.children[0].innerHTML = item.title + '<br/><small>Photo: ' + item.author + '</small>';
                return true;
            }
        };

        if (fromURL) {
            if (options.galleryPIDs) {
                for (var j = 0; j < items.length; j++) {
                    if (items[j].pid == index) {
                        options.index = j;
                        break;
                    }
                }
            } else {
                options.index = parseInt(index, 10) - 1;
            }
        } else {
            options.index = parseInt(index, 10);
        }

        // exit if index not found
        if (isNaN(options.index)) {
            return;
        }

        var radios = document.getElementsByName('gallery-style');
        for (var i = 0, length = radios.length; i < length; i++) {
            if (radios[i].checked) {
                if (radios[i].id == 'radio-all-controls') {
                } else if (radios[i].id == 'radio-minimal-black') {
                    options.mainClass = 'pswp--minimal--dark';
                    options.barsSize = {top: 0, bottom: 0};
                    options.captionEl = false;
                    options.fullscreenEl = false;
                    options.shareEl = false;
                    options.bgOpacity = 0.85;
                    options.tapToClose = true;
                    options.tapToToggleControls = false;
                }
                break;
            }
        }

        if (disableAnimation) {
            options.showAnimationDuration = 0;
        }

        gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);

        var realViewportWidth,
            firstResize = true;

        gallery.listen('beforeResize', function () {
            var dpiRatio = window.devicePixelRatio ? window.devicePixelRatio : 1;
            dpiRatio = Math.min(dpiRatio, 2.5);
            realViewportWidth = gallery.viewportSize.x * dpiRatio;

            if (!firstResize) {
                gallery.invalidateCurrItems();
            }

            if (firstResize) {
                firstResize = false;
            }
        });

        gallery.listen('gettingData', function (index, item) {
            item.src = item.o.src;
            item.w = item.o.w;
            item.h = item.o.h;
        });

        gallery.init();
    };

    var galleryElements = document.querySelectorAll(gallerySelector);
    for (var i = 0, l = galleryElements.length; i < l; i++) {
        galleryElements[i].setAttribute('data-pswp-uid', i + 1);
        galleryElements[i].onclick = onThumbnailsClick;
    }

    var hashData = photoswipeParseHash();
    if (hashData.pid && hashData.gid) {
        openPhotoSwipe(hashData.pid, galleryElements[hashData.gid - 1], true, true);
    }
};

var topArrow = {
    init: function () {
        var $topLink = $('<a/>').attr('href', 'javascript:void(0)').addClass('top-arrow').appendTo($(document.body));
        $topLink.click(function () {
            $('body,html').stop().animate({scrollTop: 0}, 500);
            return false;
        });

        $(window).scroll(function () {
            if ($(document).scrollTop() < 50)
                $topLink.hide();
            else
                $topLink.css('display', 'block');
        }).scroll();
    }
};

var photosService = {
    init: function () {
        var $filterByDate = $('#filterByDate');
        if ($filterByDate.length != 0) {
            $filterByDate.removeClass('hidden').hide().datepicker({
                beforeShow: function (input, inst) {
                    $('#ui-datepicker-div').addClass('blue-skin');
                }
            });
            $('.js-show-calendar').click(function () {
                $filterByDate.show().focus().hide();
            });
        }

        var $filterByDateWithDate = $('#filterByDateWithDate');
        if ($filterByDateWithDate.length != 0) {
            seminars.setData(settings.dates, settings.types);
            $filterByDateWithDate.removeClass('hidden').hide().datepicker({
                beforeShow: function (input, inst) {
                    $('#ui-datepicker-div').addClass('blue-skin');
                },
                beforeShowDay: function (date) {
                    var cday = seminars.getDay(date);
					if (cday != null && seminars.types[cday.type] !== undefined) {
						return [true, seminars.types[cday.type].className + ' ' + cday.periodcss, cday.description];
					}
                    return [true];
                },
                onSelect: function (dateText, inst) {
                    var paths = dateText.split('.')
                    var cday = seminars.getDay(new Date(parseInt(paths[2]), parseInt(paths[1]) - 1, parseInt(paths[0]), 0, 0, 0, 0));
                    if (cday != null) {
                        window.location = cday.link;
                    }
                }
            });
            seminars.setTypes();
            $('.js-show-calendar').click(function () {
                $filterByDateWithDate.show().focus().hide();
            });
        }
    }
};

var collapseService = {
    init: function () {
        //alert(Collapse.TRANSITION_DURATION);
        $('.js-items div.collapse').on('hide.bs.collapse', function () {
            $('html,body').animate({
                scrollTop: $(this).parents('.white-pnl').offset().top - 100
            }, 600);
        });
    }
};

var dealers = {
    $delears_tables: null,
    $ddl_cities: null,
    $ddl_coutries: null,
    dealers_data: [],
    all_cities_value: "",
    init_controls: function () {
        this.$delears_tables = $('.js-dealers-table tbody');
        if (this.$delears_tables.length != 0) {
            this.$ddl_cities = $('.js-cities').change(function () {
                dealers.set_dealers_data();
            });
            this.$ddl_coutries = $('.js-coutries').change(function () {
                dealers.set_cities_ddl();
                dealers.set_dealers_data();
            });
            this.all_cities_value = this.$ddl_cities.val();
            this.load_dealers_data();
        }
    },
    load_dealers_data: function () {
        $.ajax({
            type: "GET",
            //url: "dealers.txt",
            url: "/udata/partners/jsonlist/670//10000/1",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            processData: false,
            success: function (data) {
                dealers.dealers_data = data;
                dealers.set_coutries_ddl();
                dealers.set_cities_ddl(true);
                dealers.set_dealers_data();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(textStatus);
                console.log(errorThrown);
                //alert("Status: " + textStatus);
                //alert("Error: " + errorThrown);
            }
        });
    },
    set_coutries_ddl: function () {
        var coutries = {};
        for (var i = 0; i < this.dealers_data.length; i++) {
            var item = this.dealers_data[i];
            if (!coutries.hasOwnProperty(item.country)) {
                coutries[item.country] = 1;
                $("<option></option>").appendTo(this.$ddl_coutries).text(item.country).val(item.country);
            }
        }
        this.$ddl_coutries.selectpicker('refresh');
    },
    set_cities_ddl: function (first) {
        var coutries = {};
        var selected_country = this.$ddl_coutries.val();
        this.$ddl_cities.empty();
        $("<option></option>").appendTo(this.$ddl_cities).text(this.all_cities_value).val(this.all_cities_value);
        var cities = [];
        for (var i = 0; i < this.dealers_data.length; i++) {
            var item = this.dealers_data[i];
            if ((!coutries.hasOwnProperty(item.city) && selected_country == item.country) || first) {
                coutries[item.city] = 1;
                cities.push(item.city);
            }
        }
        cities.sort();
        for (var i = 0; i < cities.length; i++) {
            $("<option></option>").appendTo(this.$ddl_cities).text(cities[i]).val(cities[i]);
        }
        this.$ddl_cities.selectpicker('refresh');
    },
    set_dealers_data: function () {
        var last_location = null;
        this.$delears_tables.empty();
        var selected_country = this.$ddl_coutries.val();
        var selected_city = this.$ddl_cities.val();
        var selected_country_index = this.$ddl_coutries.children().index(this.$ddl_coutries.find("option:selected"));
        var selected_city_index = this.$ddl_cities.children().index(this.$ddl_cities.find("option:selected"));
        for (var i = 0; i < this.dealers_data.length; i++) {
            var item = this.dealers_data[i];
            if ((item.city == selected_city || selected_city_index == 0) &&
                    (item.country == selected_country || selected_country_index == 0)) {
                var $row = $("<tr></tr>").appendTo(this.$delears_tables);
                $("<td></td>").appendTo($row).text(item.country);
                if (typeof item.webaddress != 'undefined') {
                    $("<a></a>").appendTo($("<td></td>").appendTo($row)).attr("href", item.webaddress).attr("target", "_blank").attr("rel", "nofollow").text(item.name);
                } else {
                    $("<td></td>").appendTo($row).text(item.name);
                }
                $("<td></td>").appendTo($row).text(item.address);
                $("<td></td>").appendTo($row).html(item.phone.split(',').join('<br/>'));
                $("<a></a>").appendTo($("<td></td>").appendTo($row)).attr("href", item.email).text(item.email);
            }
        }
    }
};

var tilesResize = {
    init: function () {
        var $items = $('.js-tiles-resize > li > div');
        var $wrapperItems = $('.js-tiles-resize > li > div > .topic-wrapper');
        if ($items.length != 0) {
            $(window).resize(function () {
                if ($(window).width() > 580) {
                    var maxHeight = 0;
                    $items.css('min-height', '');
                    $wrapperItems.css('min-height', '');
                    $items.each(function () {
                        maxHeight = Math.max(maxHeight, $(this).height());
                    });
                    maxHeight += 20;

                    $wrapperItems.each(function () {
                        $(this).css('min-height', $(this).height() + (maxHeight - $(this).parent().height()));
                    });
                    $items.css('min-height', maxHeight + 'px');
                }
                else {
                    $wrapperItems.each(function () {
                        $(this).css('min-height', '');
                    });
                    $items.css('min-height', '');
                }
            }).resize();
        }
    }
};

$('[data-ride="carousel"]').carousel({
    swipe: 50
});

var modalService = {
    init: function () {
        var nb = $('nav.navbar-fixed-top');
        $('.modal').on('show.bs.modal', function () {
            nb.width(nb.width());
        }).on('hidden.bs.modal', function () {
            nb.width(nb.width('auto'));
        });
    }
};

var events = {
    init: function () {
        console.log('start');
        jQuery('body').on('click', '.buy-button', function () {
            var ga_datas = $(this).parents('.ga_addImpression:first'),
                page_id = jQuery(this).data('page_id'),
                is_event_place = jQuery(this).hasClass('buy-button-event'),
                price = jQuery(this).data('price');

            jQuery('#page_id_input').val(page_id);
            jQuery('#event_price').val(price);
            console.log(jQuery('#page_id_input').val());

            $.ajax({
                url: "/udata/custom/outputEventDiscount/" + page_id + "/?transform=modules/catalog/pages/buymodal.xsl",
                dataType: "html",
                success: function (data) {
                    // analytics
                    var event_price_type = (parseFloat(price) > 0) ? 'paid' : 'free';
                    var event_place_type = is_event_place ? 'button-product' : 'button-list';

                    // ec analytics
                    // добавление товара в корзину
                    ga('ec:addProduct', {
                        'name': ga_datas.data('name'),
                        'category': ga_datas.data('category'),
                        'price': parseFloat(price),
                        'quantity': 1
                    });
                    ga('ec:setAction', 'add');

                    // переход к оформлению заказа
                    ga('ec:addProduct', {
                        'name': ga_datas.data('name'),
                        'category': ga_datas.data('category'),
                        'price': parseFloat(price),
                        'quantity': 1
                    });
                    ga('ec:setAction', 'checkout');


                    dataLayer.push({
                        "ecommerce": {
                            "add": {
                                "products": [
                                    {
                                        "name": ga_datas.data('name'),
                                        "price": parseFloat(price),
                                        "category": ga_datas.data('category'),
                                        "quantity": 1
                                    }
                                ]
                            }
                        }
                    });

                    // END.ec analytics

                    ga('send', 'event', 'ET', 'ecommerceCheckout', event_place_type + '_' + event_price_type + '_id' + page_id);
                    if (typeof yaCounter23063434 !== 'undefined') {
                        yaCounter23063434.reachGoal('EventClick');
                    }
                    // END.analytics

                    $(".event_discount_wrap").remove();
                    $(data).insertBefore("#fblock_order_agree");
                    //$('.event_discount_wrap').html(data);

                    $('#fblock_order_upload').insertAfter('.event_discount_wrap');

                    // проверка на необходимость отображения заголовка
					jQuery('.registration_on_event_form_area').each(check_registration_head_show);

                    var loyalty_bonus = jQuery('.event_discount_wrap input[type="radio"]:checked').data('loyalty_bonus');
                    //change loyalty_bonus
                    if (loyalty_bonus > 0) {
                        $('.event-points-amount').show();
                        $('.event-points-amount b').text(loyalty_bonus + ' баллов');
                    } else {
                        $('.event-points-amount').hide();
                    }

                    jQuery('.event_discount_wrap input[type="radio"]').change(function () {
                        var is_checked = jQuery(this).prop('checked'),
                            id = jQuery(this).attr('id'),
                            loyalty_bonus = jQuery(this).data('loyalty_bonus');

                        //change loyalty_bonus
                        if (loyalty_bonus > 0) {
                            $('.event-points-amount').show();
                            $('.event-points-amount b').text(loyalty_bonus + ' баллов');
                        } else {
                            $('.event-points-amount').hide();
                        }
                        if (is_checked && (id == 'discount_ordinator' || id == 'discount_ordinator_30' || id == 'discount_doctor' || id == 'discount_teacher' || id == 'discount_poo')) {
                            $('#fblock_order_upload').fadeIn(200);
                        } else {
                            $('#fblock_order_upload').fadeOut(200);
                        }
                        $('#fblock_order_upload input[type=file],#fblock_order_upload input[type=text]').removeClass('input-validation-error');
                    });

                    jQuery('.apply_promocode').click(function () {
                        var promokod_value = jQuery('.input_promocode').val();
                        $.ajax({
                            url: "/udata/emarket/check_promokod/" + page_id + "/" + promokod_value + "/.json",
                            dataType: "json",
                            success: function (data) {
                                jQuery('.promocode_result').remove();
                                var discount_str = '';
                                if (data.result == 1) {
                                    var price = data.price;
                                    console.log(data.discount_abs);
                                    console.log(data.discount);
                                    if ('discount_abs' in data) {
                                        console.log("'discount_abs' in data");
                                        if (data.discount_abs != '' && parseInt(data.discount_abs) >= 0) {
                                            console.log("data.discount_abs !='' && parseInt(data.discount_abs) >= 0");
                                            discount_str = "";
                                            price = data.discount_abs;
                                        } else {
                                            console.log("else");
                                            if ('discount' in data) {
                                                console.log("'discount' in data");
                                                discount_str = "(скидка " + data.discount + "%)";
                                            }
                                        }
                                    } else if ('discount' in data) {
                                        console.log("'discount' in data 22");
                                        discount_str = "(" + data.discount + "%)";
                                    }

                                    var name = "Применен промокод \"" + data.name + "\" " + discount_str + ". Стоимость участия - " + price + " руб.",
                                        promokod_radio = "<div class='form-group agree promocode_result'><input id='' type='radio' name='data[new][skidka]' data-name='" + name + "' class='fn_skidka_item form-control checkbox' value='" + price + "' checked='checked'><span>" + name + "</span></div>";
                                } else {
                                    var promokod_radio = "<div class='form-group promocode_result'><span style='color:red;'>Неверный промокод</span></div>";
                                }
                                $(promokod_radio).insertBefore(".discount_info");
                            }
                        });
                        return false;
                    });
                }
            });
        });
        jQuery('body').on('click', '.consultation_button', function () {
			jQuery('.consultation_id').val(jQuery(this).data('consultation_id'));
		});
        // jQuery('body').on('change','#ordinator_fake',function(){
        // jQuery('#ordinator').val((jQuery(this).prop('checked')));
        // if (jQuery(this).prop('checked')) {
        // $('#fblock_order_upload').fadeIn(200);
        // }else{
        // $('#fblock_order_upload').fadeOut(200);
        // }
        // })
    },
    buy: function () {
        jQuery('.buyOneClick').submit(function (event) {
            jQuery('.waiting').show();
            jQuery('.buyOneClickButton').prop("disabled", true);
            var _this = $(this),
                form_modal = _this.closest('.register-modal.in'),
                skidka_id = $('.fn_skidka_item:checked', form_modal).attr('id'),
				is_event_place = $('.buy-button-event').length > 0,
				order_upload = $('input[type=file]', form_modal);
            order_upload_fake = $('input.file_load', form_modal);
            //order_upload = $('#fblock_order_upload input[name="data[new][order_upload]"]',form_modal);
            console.log('buyOneClick');
            //console.log(form_modal);
            //console.log(skidka_id);
            //console.log(order_upload);
            //console.log(order_upload.attr('name'));

            //check upload file
            console.log('check upload file');
            console.log(skidka_id);
            if (skidka_id == 'discount_ordinator' || skidka_id == 'discount_ordinator_30' || skidka_id == 'discount_doctor' || skidka_id == 'discount_teacher' || skidka_id == 'discount_poo') {
                console.log(order_upload.val());
                if (order_upload.val() == '') {
                    order_upload.addClass('input-validation-error');
                    order_upload_fake.addClass('input-validation-error');
                    //jQuery('.buyOneClickButton').prop("disabled",false);
                    //return false;
                } else {
                    order_upload.removeClass('input-validation-error');
                    order_upload_fake.removeClass('input-validation-error');
                }
            }

            if (jQuery('.input-validation-error', _this).length > 0) {
                console.log('error form');
                jQuery('.buyOneClickButton').prop("disabled", false);
                $('body,html').stop().animate({scrollTop: jQuery('.input-validation-error:first', _this).offset().top - 120}, 500);
                jQuery('.waiting').hide();
                return false;
            } else {
                console.log('send form');
                $(' input[name="event_discount_name"]').val($('.fn_skidka_item:checked').data('name'));
                $(' input[name="event_discount_value"]').val($('.fn_skidka_item:checked').val());
                $(' input[name="event_discount_id"]').val($('.fn_skidka_item:checked').attr('id'));
                $(' input[name="potential_ormco_star"]').val($('.fn_skidka_item:checked').data('loyalty_bonus'));
                $(' input[name="freez_ormco_star"]').val($('.fn_skidka_item:checked').data('freez_ormco_star'));

                // analytics
                var event_price_type = (parseFloat(jQuery('#event_price').val()) > 0) ? 'paid' : 'free';
				var event_place_type = is_event_place ? 'button-product' : 'button-list';

                // ec
                var page_id = jQuery('#page_id_input').val(),
                        ga_datas = $("a[data-page_id='" + page_id + "']").parents('.ga_addImpression:first');

                console.log('addProduct purchase');
                console.log(ga_datas);

                ga('ec:addProduct', {
                    'name': ga_datas.data('name'),
                    'category': ga_datas.data('category'),
                    'price': parseFloat(jQuery('#event_price').val()),
                    'quantity': 1
                });

                ga('send', 'event', 'ET', 'event_registration', event_place_type + '_' + event_price_type + '_id' + page_id);
                if (typeof yaCounter23063434 !== 'undefined') {
                    yaCounter23063434.reachGoal('EventRegistration');
                }

                // END. analytics

                return true;
                // setTimeout(function(){
                // _this.trigger( 'reset' );
                // form_modal.modal('hide');
                // jQuery('#formResultModal .modal-content .modal-body').html('Заявка на регистрацию отправлена');
                // jQuery('#formResultModal').modal('show');
                // }, 500);
            }
            event.preventDefault();
            jQuery('.buyOneClickButton').prop("disabled", false);
            jQuery('.waiting').hide();
            return false;
        });
    },
    pay: function () {
        var container = '#invoice',
            newObjectBlock = '#new-legal-person';
        var block = jQuery(newObjectBlock);

        if (block.size() === 0) {
            return;
        }

        if (jQuery('input[type=radio][value!=new]', container).size() > 0) {
            if (jQuery('input[type=radio]:checked', container).val() !== 'new') {
                block.hide();
            }
        }

        jQuery('input[type=radio]', container).click(function () {
            if (jQuery(this).val() !== 'new') {
                block.hide();
            } else {
                block.show();
            }
        });
    }
};
/*
var form_ui = {
	init: function() {
		jQuery('.checkCB').submit(function(event){
		    console.log('start');
		    jQuery.each(jQuery('input[type="checkbox"]',this), function(){
		    	console.log(jQuery(this).attr('name'));
		    })

		});
	}
};*/

var webform = {
    captcha: function () {
        var captcha_reset = jQuery('.captcha_reset');
        captcha_reset.click(function () {
            var d = new Date();
            jQuery('.captcha_img', captcha_reset.parent()).attr('src', '/captcha.php?reset&' + d.getTime());
        });
    },
    send: function () {
        jQuery('.ajaxSend').submit(function (event) {
            jQuery('.waiting').show();
            var _this = $(this),
                _this_form_id = ( $(this).hasClass('contactsForm') ? '_contacts_' : '') + jQuery('input[name="system_form_id"]', _this).val(),
                form_modal = _this.closest('.register-modal.in');

            jQuery.ajax({
                url: '/udata/webforms/checkdata/.json',
                dataType: 'json',
                async: true,
                data: _this.serialize(), //data: _this.serializeArray(),
                success: function (data) {
                    let formName = _this.data('gtm-event-form');

                    if (data.status == 'error') {
                        if (data.code == 'errorFields') {
                            for (i in data.er) {
                                fieldName = data.er[i];
                                jQuery('#' + fieldName + _this_form_id + ', #' + fieldName + _this_form_id + '_fake').addClass('input-validation-error');
                            }
                        }
                        if (data.code == 'errorFormId') {
                            // не найдена форма по заданному type_id
                        }
                        jQuery('.waiting').hide();

                        dataLayer.push({
                            'event': 'formFilling',
                            'formName': formName,
                            'fillingStatus': 'ошибка',
                            'pageUrl': page_url
                        });
                    } else {
                        jQuery.ajax({
                            url: '/udata/' + _this.attr('action') + '.json',
                            dataType: 'json',
                            async: false,
                            data: _this.serialize(),
							success: function (answer) {
                                if (answer.status) {
									if (answer.status == 'error') {
										if (answer.code == 'errorFields') {
											for (i in answer.er) {
												fieldName = answer.er[i];
												jQuery('#' + fieldName + _this_form_id + ', #' + fieldName + _this_form_id + '_fake').addClass('input-validation-error');
											}
										}
										jQuery('.waiting').hide();
                                        dataLayer.push({
                                            'event': 'formFilling',
                                            'formName': formName,
                                            'fillingStatus': 'ошибка',
                                            'pageUrl': page_url
                                        });
									}else{
										_this.trigger('reset');
										jQuery('.waiting').hide();
										form_modal.modal('hide');
										jQuery('#formResultModal .modal-content .modal-body').html(answer.message);
										jQuery('#formResultModal').modal('show');
                                        dataLayer.push({
                                            'event': 'formFilling',
                                            'formName': formName,
                                            'fillingStatus': 'успешно',
                                            'pageUrl': page_url
                                        });
									}
                                }
							},
							error: function(data){
								console.log('error');
								console.log(data);
							}
                        });

                        // send analytics
                        // from contacts page
                        if (_this.hasClass('contactsForm')) {
                            ga('send', 'event', 'form', 'feedback_contacts');
                            if (typeof yaCounter23063434 !== 'undefined') {
                                yaCounter23063434.reachGoal('FeedbackContacts');
                            }
                        }
                        // from modal form
                        if (_this.parents('#feedbackModal').length > 0) {
                            ga('send', 'event', 'form', 'feedback_form');
                            if (typeof yaCounter23063434 !== 'undefined') {
                                yaCounter23063434.reachGoal('FeedbackForm');
                            }

                        }
                        // END. send analytics

//                        jQuery.ajax({
//                            url: '/udata/webforms/posted/' + _this_form_id + '.json',
//                            dataType: 'json',
//                            async: false,
//                            success: function (answer) {
//                                if (answer.result) {
//                                    _this.trigger('reset');
//                                    jQuery('.waiting').hide();
//                                    form_modal.modal('hide');
//                                    jQuery('#formResultModal .modal-content .modal-body').html(answer.result);
//                                    jQuery('#formResultModal').modal('show');
//                                }
//                            }
//                        });
                    }
                }
            });
            event.preventDefault();
        });

        //course form
        jQuery('.course-info').click(function () {
            var link = $(this).data('from_course'),
                name = $(this).data('from_course_name');
//            console.log(link);
//            console.log(name);
            jQuery('#from_course').val(link);
            jQuery('#from_course_name').val(name);
        });
    }
};

var uploadFile = {
    init: function () {
        $('.js-file-upload').each(function () {
            var $control = $(this);
            $control.find('input').change(function () {
                $control.find('span').text($(this).val());
            });
        });
    }
};

var countFile = {
    init: function () {
        $('.js-count-download').click(function () {
            var url = '/udata/library/countDownloads/' + $(this).data('count_url') + '.json';
            jQuery.ajax({
                url: url,
                dataType: 'json',
                async: false
            });
            return true;
        });
    }
};

var ga_events = {
    init: function () {
        //ga_addImpression
        ga_events.ga_addImpression();
        ga_events.ga_addProduct();
        ga('send', 'pageview');

        $('#registerModal form, form#registrate').submit(function () {
          let errObj = {
            'event' : 'formFilling',
            'formName' : 'Форма регистрации', //название формы
            'fillingStatus' : 'ошибка_регистрации', //статус заполнения формы
            'pageUrl' : page_url //url страницы, на которой произошло событие без get-параметров
          }
            var prof_status = $('#prof_status', $(this)).val(),
                prof_status_val = '',
                _this = $(this),
                valid = false;
            console.log($('.input-validation-error', $(this)).length);
            if ($('.input-validation-error', $(this)).length > 0) {
                console.log('ga register-false');
                console.log('registration false');
                dataLayer.push(errObj);
                jQuery('.waiting').hide();
                return false;
            } else {
                // check reg datas
                jQuery.ajax({
                    url: '/udata/users/checkreg/.json',
                    dataType: 'json',
                    async: false,
                    data: _this.serialize(), //data: _this.serializeArray(),
                    success: function (data) {
                        console.log(data);
                        if (data.status == 'error') {
                            console.log(data.field);
                            console.log(jQuery('#' + data.field));
                            dataLayer.push(errObj);

                            jQuery('#' + data.field).addClass('input-validation-error').removeClass('valid');
                            console.log('registration false');
                            jQuery('.waiting').hide(); // спрятать прелоалер если ajax ответил ошибкой
                            valid = false;
                        } else {
                            switch (prof_status) {
                                //Ортодонт
                                case '12135':
                                    prof_status_val = 'orthodontist';
                                    break
                                    //Хирург
                                case '12136':
                                    prof_status_val = 'surgeon';
                                    break
                                    //Другая специализация
                                case '12137':
                                    prof_status_val = 'other';
                                    break
                                    //Не врач
                                case '12138':
                                    prof_status_val = 'not a doctor';
                                    break
                                default:
                                    prof_status_val = 'empty';
                                    break
                            }
                            console.log('registration');
                            ga('send', 'event', 'form', 'registration', prof_status_val);
                            window.dataLayer=window.dataLayer || [];
                            dataLayer.push({
                                'event' : 'formFilling',
                                'formName' : 'Форма регистрации', //название формы
                                'fillingStatus' : 'успешная_регистрация', //статус заполнения формы
                                'pageUrl' : page_url //url страницы, на которой произошло событие без get-параметров
                            });
                            if (typeof yaCounter23063434 !== 'undefined') {
                                yaCounter23063434.reachGoal('UserRegistration');
                            }
                            valid = true;
                        }
                    }
                });
            }
            return valid;
        });
        $('#authModal form, form#auth').submit(function (event) {
          let errObj = {
            'event' : 'formFilling',
            'formName' : 'Форма авторизация', //название формы
            'fillingStatus' : 'ошибка_логин_пароль', //статус заполнения формы
            'pageUrl' : page_url //url страницы, на которой произошло событие без get-параметров
          }
	        jQuery('.waiting').show();
			    jQuery('.popup_auth_error_message').hide();

            if ($('.input-validation-error', $(this)).length > 0) {
                console.log('ga auth-false');
                dataLayer.push(errObj);
		            jQuery('.waiting').hide();
				        event.preventDefault();
            } else {
                // check reg datas
                jQuery.ajax({
                    url: '/udata/users/checkauth/.json',
                    dataType: 'json',
                    async: false,
                    data: jQuery(this).serialize(),
                    success: function (data) {
                        if (data.status == 'error') {
                            dataLayer.push(errObj);
                            jQuery('.popup_auth_error_message').text(data.result).show();
                            jQuery('.waiting').hide(); // спрятать прелоалер если ajax ответил ошибкой
							              event.preventDefault();
                        } else {
                            console.log('auth');
                            window.dataLayer=window.dataLayer || [];
                            dataLayer.push({
                                'event' : 'formFilling',
                                'formName' : 'Форма авторизация', //название формы
                                'fillingStatus' : 'успешная_авторизация', //статус заполнения формы
                                'pageUrl' : page_url //url страницы, на которой произошло событие без get-параметров
                            });
                            ga('send', 'event', 'form', 'authorization');
                            if (typeof yaCounter23063434 !== 'undefined') {
                                yaCounter23063434.reachGoal('UserAuthorization');
                            }
                        }
                    }
                });
            }
        });
    },
    ga_eventclick: function () {
        $("body").on("click", ".ga_eventclick", function () {
            var ga_datas = $(this).parents('.ga_addImpression:first');
            ga('ec:addProduct', {
                'name': ga_datas.data('name'),
                'category': ga_datas.data('category'),
                'position': ga_datas.data('position')
            });
            ga('ec:setAction', 'click', {list: ga_datas.data('list')});

            ga('send', 'event', 'ET', 'eventclick');
            //yaCounter23063434.reachGoal('EventClick');
        });
    },
    ga_applestore: function () {
        $("body").on("click", ".ga_applestore", function () {
            ga('send', 'event', 'click', 'app_ios');
            if (typeof yaCounter23063434 !== 'undefined') {
                yaCounter23063434.reachGoal('AppIos');
            }
        });
    },
    ga_googleplay: function () {
        $("body").on("click", '.ga_googleplay', function () {
            ga('send', 'event', 'click', 'app_android');
            if (typeof yaCounter23063434 !== 'undefined') {
                yaCounter23063434.reachGoal('AppAndroid');
            }
        });
    },
    ga_page404: function () {
        console.log('is_page404');
        if ($(".page404").length > 0) {
            var url = $(".page404").data('url');
            console.log('page404');
            console.log(url);
            ga('send', 'event', '404error', url, {nonInteraction: true});
            if (typeof yaCounter23063434 !== 'undefined') {
                yaCounter23063434.reachGoal('404Error');
            }
        }
    },
    ga_addImpression: function () {
        console.log('addImpression');
        $('.ga_addImpression').each(function () {
            var ga_datas = $(this);
            ga('ec:addImpression', {
                'name': ga_datas.data('name'),
                'category': ga_datas.data('category'),
                'list': ga_datas.data('list'),
                'position': ga_datas.data('position'),
            });
        });
    },
    ga_addProduct: function () {
        console.log('addProduct');
        if ($('.ga_addProduct').length > 0) {
            var ga_datas = $('.ga_addProduct:first');

            ga('ec:addProduct', {
                'name': ga_datas.data('name'),
                'category': ga_datas.data('category')
            });
            ga('ec:setAction', 'detail');
            dataLayer.push({
                "ecommerce": {
                    "detail": {
                        "products": [
                            {
                                "name": ga_datas.data('name'),
                                "price": ga_datas.data('price'),
                                "category": ga_datas.data('category')
                            }
                        ]
                    }
                }
            });
        }
    }
};

/**
 * Custom file input
 */
var customFileInput = {
    init: function () {
        $('.input-file').before(function () {
            var $this = $(this);
            if (!$this.prev().hasClass('input-ghost')) {
                var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
                element.attr('name', $this.data('name'));
                element.change(function () {
                    element.next(element).find('input').val((element.val()).split('\\').pop());
                });

                $this.click(function () {
                    element.click();
                });

                // $this.find('button.btn-reset').click(function(){
                //   element.val(null);
                //   $this.parents('.input-file').find('input').val('');
                // });

                $this.find('input').css('cursor', 'pointer');
                $this.find('input').mousedown(function () {
                    $this.parents('.input-file').prev().click();

                    return false;
                });

                return element;
            }
        });
    }
};


var mobileSearch = {
	init: function () {
		var $btn = $('.js-mobile-search-btn');
		var $searchPnl = $('.js-mobile-search-pnl');

		$btn.click(function () {
			if ($btn.hasClass('open')) {
				$btn.removeClass('open');
				$searchPnl.stop().slideUp(500);
			} else {
				$btn.addClass('open');
				$searchPnl.stop().slideDown(500);
			}
		});

		$(window).resize(function () {
			if ($(window).width() <= 767) {
				if ($btn.hasClass('open')) {
					$searchPnl.show();
				} else {
					$searchPnl.hide();
				}
			} else {
				$searchPnl.hide();
			}
		}).resize();
	}
}


jQuery(document).ready(function () {
    modalService.init();
    tilesResize.init();
    dealers.init_controls();
    customFileInput.init();

    initPhotoSwipeFromDOM('.js-photos');
    photoSlider.init();
    menuService.init();
    userProfileService.init();
    topArrow.init();
    photosService.init();
    collapseService.init();

    //form_ui.init();
    uploadFile.init();

    webform.captcha();
    webform.send();
    events.init();
    events.buy();
    events.pay();

    //analytics events
    ga_events.init();
    ga_events.ga_eventclick();
    ga_events.ga_applestore();
    ga_events.ga_googleplay();
    ga_events.ga_page404();
    // end. analytics events

    countFile.init();

    mobileSearch.init();

    $('[data-toggle="tooltip"]').tooltip();

	// показывать отметку в личном кабинете о изменении рейтинга
    var ormco_star_notify = $.cookie("ormco_star_notify");
    console.log(ormco_star_notify);

    if (typeof ormco_star_notify === 'undefined' || ormco_star_notify == null) {
  	console.log('ormco_star_notify');
  	$.ajax({
            type: "POST",
            url: '/udata/users/is_loyalty_change.json',
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                ormco_star_notify = false;
                if (data.status === 'successful') {
                    ormco_star_notify = 'yes';
                } else if (data.status === 'error') {
                    ormco_star_notify = 'no';
                }
                var date = new Date();
                var minutes = 5;
                date.setTime(date.getTime() + (minutes * 60 * 1000));
                jQuery.cookie('ormco_star_notify', ormco_star_notify, {
                    expires: date, // the number of days cookie  will be effective
                    path: '/'
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(textStatus);
                console.log(errorThrown);
                //alert("Status: " + textStatus);
                //alert("Error: " + errorThrown);
            }
        });
    }

    if (ormco_star_notify == 'yes') {
        jQuery('.dropdown.cabinet.js-cabinet > a').addClass('event-points-dot').addClass('event-points-dot_menu');
        jQuery('.item2599 > a').addClass('event-points-dot').addClass('event-points-dot_submenu');
    }

    if (jQuery('.loyalty-page').length > 0) {
        var date = new Date();
        var minutes = 5;
        date.setTime(date.getTime() + (minutes * 60 * 1000));
        jQuery.cookie('ormco_star_notify', 'no', {
            expires: date, // the number of days cookie  will be effective
            path: '/'
        });
    }

    if(jQuery('.reviews_on_center_body').length > 0){
        var max_width = 0;
        jQuery('.reviews_on_center_body').each(function(){
            max_width = jQuery(this).height() > max_width ? jQuery(this).height() : max_width;
        });
        jQuery('.reviews_on_center_body').each(function(){
            jQuery(this).height(max_width);
        });
    }

    if(jQuery('#e-mail').length > 0){
        jQuery('#e-mail').change(function(){
            jQuery(this).val(jQuery(this).val().trim());
        });
    }
    if(jQuery('#email2').length > 0){
        jQuery('#email2').change(function(){
            jQuery(this).val(jQuery(this).val().trim());
        });
    }
    if(jQuery('#email3').length > 0){
        jQuery('#email3').change(function(){
            jQuery(this).val(jQuery(this).val().trim());
        });
    }
    if(jQuery('input[name=password]').length > 0){
        jQuery('input[name=password]').change(function(){
            jQuery(this).val(jQuery(this).val().trim());
        });
    }

    //js для формы регистрации
    // отображать текстовое поле при выборе в поле "кто вы" варианта "другая специальность"
    $('.prof_status_field').on('change', function() {
        var option_val = this.value;
        var this_form = $(this).parents('form');
        var targ = $('.prof_status_field option[value="'+option_val+'"]', this_form).attr('target');
        //console.log($('#prof_status option[value="'+option_val+'"]'));

        // другая специальность item id = 12137
        if(option_val == '12137'){
            targ = 'ord_dop_fields3';
        }
        // студент item id = 1773599
        if(option_val == '1773599'){
            targ = 'student_dop_field ';
        }

        $('.ord_dop_fields', this_form).collapse('hide');
        $('.'+targ, this_form).collapse('show');
    });

    /*$('.is_student').on('change', function() {
        var this_form = $(this).closest('form');
		$('.student_dop_field', this_form).stop(true, true).slideToggle(200);
    });*/

    // блокировать определенные регионы, если в поле "Страна" выбрана не "Россия"
    $('.country_field').on('change', function() {
        toggleDadataAddressFields($(this));
    });

    $('#address_form').on('submit', function(e) {
        if(!validateFormDadataAddress($(this))) {
            e.preventDefault();
        }
    });

    //\js для формы регистрации
});

// show loading
jQuery(document).ready(function(){
    jQuery('#registerModal form, form#registrate, form#personal_information_form').submit(function(){
        jQuery('.waiting').show();
        if (jQuery('.input-validation-error', jQuery(this)).length > 0) {
            jQuery('.waiting').hide();
        }
    });
    jQuery('.no_ajax_form_waiting_show').submit(function(){
        jQuery('.waiting').show();
    });
    jQuery('.no_ajax_link_waiting_show').click(function(){
        jQuery('.waiting').show();
    });
});


jQuery(document).ready(function(){
	if(jQuery('.top_info_string_close').length > 0){
		jQuery('.top_info_string_close').click(function(event){
			event.preventDefault();
			jQuery(this).closest('body').removeClass('with_top_info_string');
			jQuery(this).closest('.top_information_string').hide();
			jQuery.cookie('top_information_string_close', 1, { expires: 14, path: "/" });
		});
	}
});


jQuery(document).ready(function(){
	jQuery('.registration_on_event_form_area').each(check_registration_head_show);
});

// проверка на необходимость отображения заголовка в форме регистрации
function check_registration_head_show(){
	var is_head_showing = false;
	jQuery(this).find('> div.form-group').each(function () {
		if (jQuery(this).hasClass('event_discount_wrap')) {
			return false;
		}
		if (jQuery(this).hasClass('agree')) {
			return false;
		}
		if (jQuery(this).is('#fblock_order_upload')) {
			return false;
		}
		if (!jQuery(this).hasClass("form-group-hidden")) {
			is_head_showing = true;
			return false;
		}
	});

	if (is_head_showing) {
		jQuery(this).find('.personal_data_head').show();
	} else {
		jQuery(this).find('.personal_data_head').hide();
	}
}

jQuery(document).ready(function(){
	jQuery('#ya_vrach207_fake').change(function(){
		if(jQuery(this).prop("checked")){
			jQuery(this).closest('form').find('.doctor_only').addClass('is_doctor_selected');//.find('input').attr('required', 'required');
		}else{
			jQuery(this).closest('form').find('.doctor_only').removeClass('is_doctor_selected');//.find('input').removeAttr('required');
		}
	});
	jQuery('#ya_vrach_contacts_207_fake').change(function(){
		if(jQuery(this).prop("checked")){
			jQuery(this).closest('form').find('.doctor_only').addClass('is_doctor_selected');//.find('input').attr('required', 'required');
		}else{
			jQuery(this).closest('form').find('.doctor_only').removeClass('is_doctor_selected');//.find('input').removeAttr('required');
		}
	});

	jQuery('#tema207').change(function(){
		jQuery(this).closest('form').find('.system_email_to').val(jQuery(this).find('option:selected').data('tema-id'));
	});

	jQuery('#tema_contacts_207').change(function(){
		jQuery(this).closest('form').find('.system_email_to').val(jQuery(this).find('option:selected').data('tema-id'));
	});

	jQuery('input.form-control').change(function(){
		jQuery(this).val(jQuery(this).val().trim().replace(new RegExp('ㅤ', 'g') , '').replace(new RegExp('ㅤ', 'g') , ''));
	});
});

(function($){
	$(document).ready(function(){
		$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
			event.preventDefault();
			event.stopPropagation();
			$(this).parent().siblings().removeClass('open');
			$(this).parent().toggleClass('open');
		});
	});
})(jQuery);


(function($){
	$(document).ready(function(){
		$('.one_legal_person_head').click(function(){
			$(this).closest('.one_legal_person').toggleClass('opened');
		});

		if($('.block_round_height').length > 0){
			let max_block_height = 0;
			let max_sub_block_height = 0;

			$('.block_round_height').each(function(){
				let current_block_height = parseInt($(this).css('height'));
				if(current_block_height > max_block_height){
					max_block_height = current_block_height;
				}

				let current_sub_block_height = parseInt($(this).find('.sub_block_area').css('height'));
				if(current_sub_block_height > max_sub_block_height){
					max_sub_block_height = current_sub_block_height;
				}
			});

			$('.block_round_height').css('height', max_block_height + 'px');
			$('.sub_block_area').css('height', max_sub_block_height + 'px');
		}
	});
})(jQuery);