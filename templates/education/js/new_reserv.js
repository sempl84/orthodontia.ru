$(document).ready(function () {
  /**
   * Download list
   */

  var downloadList = {
    $list: $('#dealers-list'),
    $url: '/templates/education/speakers.html',
    $categoryId: $('.js--filters').data('category'),
    $categoryType: $('.js--filters').data('type'),
    $categorySpeaker: $('.js--filters').data('speaker'),

    init: function () {
      var $this = this;

      // Resize
      $(window).resize(function() {
        if ($(window).width() <= 991) {
          $('.download-list').removeClass('download-list_sticky');
        }
      });

      // Scroll
      $(window).scroll(function() {
        if ($(document).scrollTop() > 320 && $(window).width() > 991) {
          $('.download-list').addClass('download-list_sticky');
        } else {
          $('.download-list').removeClass('download-list_sticky');
        }
      });

      // Click on "checkbox"
      $this.$list.on('click', '.js--pdf-download-item', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var $target = $(e.currentTarget ? e.currentTarget : e.target);
        var isActive = $target.hasClass('card-item__checkbox_checked');

        if (!isActive) {
          $target.addClass('card-item__checkbox_checked');
        } else {
          $target.removeClass('card-item__checkbox_checked');
        }
      });

      // Click on "checkbox"
      $this.$list.on('click', '.card-item', function(e) {
      	console.log('click - .card-item');
        e.preventDefault();
        e.stopPropagation();

        var $target = $(e.currentTarget ? e.currentTarget : e.target);
        var url = $target.data('href');

        if (url) {
          window.location.href = url;
        }
      });

      // Click on download link
      $('.download-list').on('click', '.js--pdf-download-link', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var $mode = 'short';

        if($(this).hasClass('pdf_speakers_short')){
        	var pdfIdsArray = [];
			$mode = 'short';

	        $('.js--pdf-download-item')
	          .each(function () {
	            var $item = $(this);
	            var pdfId = $item.data('pdf-id');

	            pdfIdsArray.push(pdfId);

	          });

        }
        if($(this).hasClass('pdf_speakers_full')){
        	var pdfIdsArray = [];
			$mode = 'full';

	        $('.js--pdf-download-item')
	          .each(function () {
	            var $item = $(this);
	            var pdfId = $item.data('pdf-id');

	            pdfIdsArray.push(pdfId);

	          });

        }
        if($(this).hasClass('pdf_speakers_selected')){
        	var pdfIdsArray = [];
			$mode = 'short';

	        $('.js--pdf-download-item')
	          .each(function () {
	            var $item = $(this);
	            if ($item.hasClass('card-item__checkbox_checked')) {
	              var pdfId = $item.data('pdf-id');
	              pdfIdsArray.push(pdfId);
	            }
	          });


        }
				if($(this).hasClass('pdf_speakers_selected_full')){
        	var pdfIdsArray = [];
			$mode = 'full';

	        $('.js--pdf-download-item')
	          .each(function () {
	            var $item = $(this);
	            if ($item.hasClass('card-item__checkbox_checked')) {
	              var pdfId = $item.data('pdf-id');
	              pdfIdsArray.push(pdfId);
	            }
	          });


        }
        if (pdfIdsArray.length > 0) {
          window.open('/pdf/dealers.php?type='+$this.$categoryType+'&mode=' + $mode +'&ids=' + pdfIdsArray.join(',') + '', '_blank');
        }


      });


      // Click on "href program"
      $this.$list.on('click', '.rhref', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var $target = $(e.currentTarget ? e.currentTarget : e.target);
        var url = $target.data('href');

        if (url) {
          window.location.href = url;
        }
      });
    }
  };


  /**
   * Filters
   */

  var filterContent = {
    SUBMIT_URL: 'catalog/getSmartFilter',
    $listWrapper: $('#dealers-list'),
    $categoryId: $('.js--filters').data('category'),
    $categoryType: $('.js--filters').data('type'),
    $categorySpeaker: $('.js--filters').data('speaker'),

    init: function () {
      var $this = this;

      // Add buttons for multiple select
      $('.multiple-custom').on('loaded.bs.select', function (e) {
      	console.log('ggg');
        var $target = $(e.currentTarget ? e.currentTarget : e.target);

        var $dropdownMenu = $target.parent().find('div.dropdown-menu');
        var $dropdownMenuButtonContainer = $('<div class="dropdown-menu__button-container"></div>').appendTo($dropdownMenu);

        $('<a class="btn btn-primary btn_small js--filters-submit"></a>')
          .appendTo($dropdownMenuButtonContainer)
          .attr('href', '#')
          //.text('Применить');
          .html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>Применить');
          //.html('Применить');
          
        $('.js--filters-submit').html('Применить');
      });



      // On change
      $('.js--filters select').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
        var select = e.currentTarget ? e.currentTarget : e.target;
        var isMultipleSelect = select.multiple;

				// change right filter
				console.log($(select).attr('id') == 'tematika_provodimyh_meropriyatij');
				if($(select).attr('id') == 'tematika_provodimyh_meropriyatij'){
					var $right_synh_el = $('.right-filters .js--right-filters[data-index='+clickedIndex+']');
					if (!isSelected) {
	          $right_synh_el.removeClass('right-filters__item-link_active');
	          $right_synh_el
	            .find('.right-filters__item-checkbox')
	            .removeClass('right-filters__item-checkbox_checked');
	        } else {
	          $right_synh_el.addClass('right-filters__item-link_active');
	          $right_synh_el
	            .find('.right-filters__item-checkbox')
	            .addClass('right-filters__item-checkbox_checked');
	        }
       	}

        //ajax change other filter
        //$(select).parents('.multiple-custom').find('.js--filters-submit .glyphicon-refresh-animate').show();
        //$('.js--filters-submit').addClass('refresh');
        $('.js--filters-submit').attr('disabled','disabled').html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate" ></span> Применить' );
        console.log('refresh');
		setTimeout(function(){
        //var $categoryId = 2260;
        //var $categoryId = 2259;

        //перебираем все параметры, получаем значение адаптивных фильтров без текущего параметра, а затем возвращаем в него все отмеченные значения
        //disable все свойства
				var curr_select_name = $(select).attr('id');
				//console.log($('.js--filters .selectpicker option'));
				$('.js--filters .selectpicker').each(function(){
					var item_select_id = $(this).attr('id');
					var tmp_params = $this.getParams();

					if($this.$categoryType == 'program_speakers'){
						tmp_params['filter[speaker][]'] = $this.$categorySpeaker;
					}
					//$('option:not(:selected)', $(this)).prop('disabled','true');
					$('option', $(this)).prop('disabled','true');

					//console.log(tmp_params);
      		delete tmp_params['filter['+item_select_id+'][]'];
      		//console.log(tmp_params);
					$.ajax({
		        type: 'GET',
		        async: false,
		        //contentType: 'application/json',
		        url: '/udata/catalog/getSmartFilters//'+$this.$categoryId+'/1/100/.json',
		        data: tmp_params,
		        dataType: 'json' ,
		        success: function (data) {
		        	// перебираем все группы
		        	for (var i in data.group) {
								for (var j in data.group[i].field) {
									var select_name =  data.group[i].field[j].name;
									if(item_select_id == select_name){
										for (var k in data.group[i].field[j].item) {
											var option_value = data.group[i].field[j].item[k].value;
											$('select#'+select_name+' option[value="'+option_value+'"]').prop("disabled", false);
										}
									}
								}
							}
		        },
		        error: function (XMLHttpRequest, textStatus, errorThrown) {
		          console.error(XMLHttpRequest, textStatus, errorThrown);
		        }
		      });
				});
				$('.selectpicker').selectpicker('refresh');
				//$(select).parents('.multiple-custom').find('.js--filters-submit .glyphicon-refresh-animate').hide();
				//$('.js--filters-submit').removeClass('refresh');
				$('.js--filters-submit').removeAttr('disabled').html('Применить');;

				console.log('refresh end');
        }, 50);


        if (!isMultipleSelect) {
          var params = $this.getParams();
          $this.submitData(params);
        }
      });

      // $('body').on('click', 'li .text', function(e){
		  // $('.js--filters-submit').html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate" ></span>Применить' );
        // console.log('refresh');
		// });


      // On click
      $(document).on('click', '.js--filters-submit', function(e) {
      	$('.js--filters-submit').html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate" ></span> Применить' );
        console.log('refresh');

        e.preventDefault();
        e.stopPropagation();



			    var params = $this.getParams();
	        $this.submitData(params);
	        $('.js--filters-submit').html('Применить');

				console.log('refresh end');


      });

      // On click
      $('.right-filters').on('click', '.js--right-filters', function(e) {
        e.preventDefault();
        e.stopPropagation();
        var $target = $(e.currentTarget ? e.currentTarget : e.target),
        	selected = false;

        if ($target.hasClass('right-filters__item-link_active')) {
          $target.removeClass('right-filters__item-link_active');
          $target
            .find('.right-filters__item-checkbox')
            .removeClass('right-filters__item-checkbox_checked');
          selected = false;
        } else {
          $target.addClass('right-filters__item-link_active');
          $target
            .find('.right-filters__item-checkbox')
            .addClass('right-filters__item-checkbox_checked');

          selected = true;
        }

        //sync with filter
        var index = $(this).data('index');
        console.log(4);
        console.log(index);
        console.log(selected);
        console.log($('select#tematika_provodimyh_meropriyatij option:eq('+index+')'));
        $('select#tematika_provodimyh_meropriyatij').parents('.multiple-custom').find('.dropdown-menu li[data-original-index='+index+'] a').click();// option:eq('+index+')').click();//attr('selected',selected);
        //$('.selectpicker').selectpicker('refresh');
      });
    },

    getParams: function () {
      var filters = {};

      $('.js--filters select')
        .each(function () {
            var item = this;

            if ($(item).val()) {
              var filter = {};
              filter[item.name] = $(item).val();
              filters = Object.assign({}, filters, filter);
            }
          });

      return filters;
    },

    submitData: function (params) {
      var $this = this,
      		template = 'modules/catalog/pages/spec_dealers_speakers_ajax.xsl';//,
      		//$categoryId = 2260;
      		//$categoryId = 2259;

      console.log(params);
		if($this.$categoryType == 'program_speakers'){
			delete params['filter[speaker][]'];
			params['filter[speaker][]'] = $this.$categorySpeaker;
			template = 'modules/catalog/pages/spec_dealers_program_speaker_ajax.xsl';
		}
		if($this.$categoryType == 'program'){
			template = 'modules/catalog/pages/spec_dealers_programs_ajax.xsl';
		}
      $.ajax({
        type: 'GET',
        // url: $this.SUBMIT_URL,
        //url: $this.$url,
        //url: '/udata/catalog/getSmartCatalogAjax//'+$this.$categoryId+'/1000/1/2////'+$this.$categoryType+'/?transform=modules/catalog/pages/spec_dealers_speakers_ajax.xsl',
        url: '/udata/catalog/getSmartCatalogAjax//'+$this.$categoryId+'/1000/1/2////'+$this.$categoryType+'/?transform='+template,
        data: params,
        dataTyp: 'html' ,
        success: function (data) {
        	$(".js--filters .multiple-custom.open .dropdown-toggle").dropdown("toggle");
          $this.$listWrapper.html($(data).find('#filter_result').html());
          tooltipInit.init();
          //$('#dealers-list').html($(data).filter('#filter_result'));
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          console.error(XMLHttpRequest, textStatus, errorThrown);
        }
      });
    }
  };


  /**
   * Custom file input
   */

  var customFileInput = {
    init: function () {
      $('.input-file').before(function() {
        var $this = $(this);

        if (!$this.prev().hasClass('input-ghost')) {
          var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
          element.attr('name', $this.data('name'));
          element.change(function(){
            element.next(element).find('input').val((element.val()).split('\\').pop());
          });

          $this.click(function(){
            element.click();
          });

          // $this.find('button.btn-reset').click(function(){
          //   element.val(null);
          //   $this.parents('.input-file').find('input').val('');
          // });

          $this.find('input').css('cursor', 'pointer');
          $this.find('input').mousedown(function() {
            $this.parents('.input-file').prev().click();

            return false;
          });

          return element;
        }
      });
    }
  };
  
  /**
   * Submenu
   */

  var subMenu = {
    init: function () {
      $('.submenu').on('click', '.submenu__top', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var $subMenu = $('.submenu');
        var $subMenuContent = $('.submenu__content');
        var isOpen = $subMenu.hasClass('submenu_is-open');

        if (!isOpen) {
          $subMenu.addClass('submenu_is-open');
        } else {
          $subMenu.removeClass('submenu_is-open');
        }
        $subMenuContent.slideToggle();
      });
    }
  };
  
  /**
   * download big photo
   */

  var speakerBigPhoto = {
    init: function () {
			// Gets the video src from the data-src on each button
			var $imageSrc;  
			$('.modal_lightbox').click(function() {
			    $imageSrc = $(this).data('img');
			});
			//console.log($imageSrc);
			  
			  
			  
			// when the modal is opened autoplay it  
			$('#speakerBigImage').on('shown.bs.modal', function (e) {
			    
			// set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
			
				$("#speakerBigImageSrc").attr('src', $imageSrc  ); 
			})
			  
			  
			// reset the modal image
			$('#speakerBigImage').on('hide.bs.modal', function (e) {
			    // a poor man's stop video
			    $("#speakerBigImageSrc").attr('src',''); 
			}) 
    }
  };
  /**
   * tooltip init
   */

  var tooltipInit = {
    init: function () {
			$('[data-toggle="tooltip"]').tooltip({
          html: true,
          track: true,
          container: "body",
          trigger : 'hover' 
      })
    }
  };


  // Init all
  downloadList.init();
  filterContent.init();
  customFileInput.init();
  subMenu.init();
  speakerBigPhoto.init();
  tooltipInit.init();

  // $(function () {
      // console.log($('[data-toggle="tooltip"]'));
      // $('[data-toggle="tooltip"]').tooltip({
          // html: true,
          // track: true,
          // container: "body",
          // trigger : 'hover' 
      // })
  // })
});
