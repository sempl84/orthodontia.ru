jQuery(document).ready(function () {
    let $input = jQuery('input[data-dadata=address]');

    $input.suggestions({
        token: $input.data('token'),
        type: "ADDRESS",
        onSearchStart: function () {
            $input.data('suggestion', null);
        },
        onSelect: function (suggestion) {
            $input.data('suggestion', suggestion);
        }
    });
});

function toggleDadataAddressFields($input) {
    let value = $input.find('option:selected').val(),
        $dadataContainer = $input.parents('form').find('div[data-address=dadata-container]'),
        $addressContainer = $input.parents('form').find('div[data-address=address-container]');

    if(value === '12115') {
        $dadataContainer.show();
        $addressContainer.hide();
    } else {
        $dadataContainer.hide();
        $addressContainer.show();
    }
}

function validateFormDadataAddress($form) {
    let $input = $form.find('input[data-dadata=address]:visible');

    if (!$input.length) {
        return true;
    }

    let suggestion = $input.data('suggestion');

    if (!suggestion) {
        alert('Введите адрес');
        return false;
    }

    if (!suggestion['data']['house']) {
        alert('Не указан дом');
        return false;
    }

    $form.find('textarea[data-dadata=input]').val(JSON.stringify(suggestion));

    return true;
}