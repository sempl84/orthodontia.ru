/**
 * Phone validator
 */

var phoneValidator = {
  // Urls
  IS_PHONE_VALID_URL: '/udata/users/is_phone_valid.json',
  SEND_CODE_URL: '/udata/users/send_code.json',
  APPLY_CODE_URL: '/udata/users/apply_code.json',
  CHANGE_PHONE_URL: '/udata/users/change_phone.json',
  MASK_FORMAT: '+7 (000) 0000000',
  MASK_PLACEHOLDER: '+7 (___) ___-____',

  // Defaults
  $phoneWrapper: undefined,
  $phoneInput: undefined,
  $phoneInputValue: $('.js-phone--input:first').val(),

  init: function (element) {
    var $this = this;

    // Defaults
    $this.$phoneWrapper = $(element);
    $this.$phoneInput = $this.$phoneWrapper.find('.js-phone--input');

	$this.$phoneInput.mask($this.MASK_FORMAT, {
		 translation: {
	        'r': {
	          pattern: /[9]/,
	          fallback: '9'
	        },
	        'y': {
	          pattern: /[0]/,
	          fallback: '0'
	        },
	      },placeholder: $this.MASK_PLACEHOLDER
	});

    // Not validated check
    //const isNotValidated = $this.$phoneWrapper.hasClass('js-phone--not-validated');
    var isNotValidated = $this.$phoneWrapper.hasClass('js-phone--not-validated');
    if (isNotValidated) {
      $this.removeStartUpContent();
      $this.removeCheckCodeContent();
      $this.removeMessageContent();
      $this.addStartUpContent();
    }

    // Input changes
    $this.$phoneInput.on('change keyup paste', function (e) {
      var value = e.target.value;
      $this.removeStartUpContent();
      $this.removeCheckCodeContent();
      $this.removeMessageContent();

      $this.updateInputValue(value);
      //console.log('updateInputValue');
      //console.log($this.$phoneInputValue);
      var phoneCode = value.substring(0, 2);
      if (phoneCode === '+7' && value.length === 12) {
        //$this.addStartUpContent();
      }
    });

    // Clicks
    $this.$phoneWrapper
      .on('click', '.js-phone--later', function (e) {
        e.preventDefault();

        $this.$phoneInput.attr('readonly', false);
        $this.removeStartUpContent();
        $this.removeCheckCodeContent();
      })
      .on('click', '.js-phone--send-code', function (e) {
        e.preventDefault();

        var phoneValue = $this.$phoneInputValue;
        $this.sendCode(phoneValue);
      })
      .on('click', '.js-phone--validate', function (e) {
        e.preventDefault();

        var code = $this.$phoneWrapper
          .find('.js-phone--code-input')
          .val();

        if (code) {
          $this.applyCode(code);
        }
      })
      .on('click', '.js-phone--repeat', function (e) {
        e.preventDefault();
		if($(this).hasClass('disabled')){
			return ;
		}
        var phoneValue = $this.$phoneInputValue;
        $this.sendCode(phoneValue);
      })
      .on('click', '.js-phone--change-phone', function (e) {
        e.preventDefault();

        $('#phoneValidateModal').modal('hide');
        $('#phoneChangeModal').modal('show');
      })
      .on('click', '.js-phone--feedback', function (e) {
        e.preventDefault();

		//('#phoneValidateModal').modal('hide');
        //$('#phoneChangeModal').modal('hide');
        $('#feedbackModal').modal('show');


      })
      .on('click', '.js-phone--change-phone-apply', function (e) {
        e.preventDefault();


		// $('.phoneChangeText').hide();
		// $this.$phoneInput.hide();
		// $this.$phoneInput.next("br").hide();
		//$(this).hide();
		var phoneValue = $this.$phoneInputValue;
		//var phoneValue = jQuery(this).parents('.js-phone--wrapper').find('.js-phone--code-input').val();
		//console.log(phoneValue);
        $this.changePhone(phoneValue);


      });
  },

  /**
   * API methods
   */

  updateInputValue: function(value) {
    this.$phoneInputValue = value;
  },

  checkPhone: function (phone) {
    var $this = this;

    $.ajax({
      type: "POST",
      url: $this.IS_PHONE_VALID_URL,
      dataType: "json",
      data: {phone: phone},
      contentType: "application/json; charset=utf-8",
      success: function (data) {
        if (data.status === 'successful') {
            console.log('checkPhone: successful');

          //alert('checkPhone: successful');
        } else if (data.status === 'error') {
            console.log('checkPhone: error');
          //alert('checkPhone: error');
        }
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
          console.log("Status: " + textStatus);
          console.log("Error: " + errorThrown);
        //alert("Status: " + textStatus);
        //alert("Error: " + errorThrown);
      }
    })
  },

  sendCode: function (phone) {
    var $this = this;


    $.ajax({
      type: "POST",
      url: $this.SEND_CODE_URL,
      dataType: "json",
      data: {phone: phone},
      //contentType: "application/json; charset=utf-8",
      success: function (data) {
        if (data.status === 'successful') {
          $this.removeMessageContent();
          $this.removeStartUpContent();
          $this.removeCheckCodeContent();

          $this.addCheckCodeContent();
        } else if (data.status === 'error') {
          $this.removeMessageContent();
          //alert('checkPhone: error');

        }
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
      	$this.removeMessageContent();
      	console.log("Status: " + textStatus);
      	console.log("Error: " + errorThrown);
        //alert("Status: " + textStatus);
        //alert("Error: " + errorThrown);
      }
    })
  },

  applyCode: function (code) {
    var $this = this;
    var $phoneInput = $this.$phoneInput;

    $this.clearCodeInputErrors();
    $.ajax({
      type: "POST",
      url: $this.APPLY_CODE_URL,
      dataType: "json",
      data: {code: code},
      //contentType: "application/json; charset=utf-8",
      success: function (data) {
        if (data.status === 'successful') {
          $phoneInput.attr('readonly', false);
          $this.removeCheckCodeContent();
          $this.addMessageContent('Ваш телефон успешно подтверждён!');

          // перегрузка страницы личного кабинета
          if($('form[action="/users/settings_do/"]').length > 0){
	          setTimeout(function () {
			      location.reload();
			  }, 2000);
		  }
        } else if (data.status === 'error') {
          $this.showCodeInputErrors(data.error.message);
        }
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
          console.log("Status: " + textStatus);
          console.log("Error: " + errorThrown);
        //alert("Status: " + textStatus);
        //alert("Error: " + errorThrown);
      }
    })
  },

  changePhone: function (phone) {
    var $this = this;
    var $phoneInput = $this.$phoneInput;

    $this.clearChangePhoneInputErrors();
    $.ajax({
      type: "POST",
      url: $this.CHANGE_PHONE_URL,
      dataType: "json",
      data: {phone: phone},
      //contentType: "application/json; charset=utf-8",
      success: function (data) {
      	console.log(data);
        if (data.status === 'successful') {
          $('.js-phone--change-phone-apply').hide();
          $phoneInput.attr('readonly', false);
          $this.removeCheckCodeContent();
          $this.addMessageContent('Ваш телефон успешно изменён! <br/>Нажмите кнопку "Отправить код" чтобы подтвердить его.');

          // show apply code block
          $this.removeStartUpContent();
	      $this.removeCheckCodeContent();
	      //$this.removeMessageContent();

	      $this.addStartUpContent();
        } else if (data.status === 'error') {
        	console.log(data.error.message);
          //$this.showCodeInputErrors(data.error.message);
          $this.showChangePhoneInputErrors(data.error.message);

        }
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
          console.log("Status: " + textStatus);
          console.log("Error: " + errorThrown);
        //alert("Status: " + textStatus);
        //alert("Error: " + errorThrown);
      }
    })
  },


  /**
   * Content methods
   */

  // Start Up Content
  addStartUpContent: function () {
    var $this = this;
    var $wrapper = $this.$phoneWrapper;

    // Content
    var $content = $('<div class="phone-validator js-phone--StartUpContent"></div>').appendTo($wrapper);
    $('<h5></h5>').appendTo($content).text('Подтвердите ваш номер мобильного телефона с помощью SMS-кода');
    $('<a class="btn btn-primary js-phone--send-code"></a>')
      .appendTo($content)
      .attr('href', '#')
      .text('Отправить код');
    /*$('<a class="btn btn-link js-phone--later"></a>')
      .appendTo($content)
      .attr('href', '#')
      .text('Подтвердить позже');*/
  },

  removeStartUpContent: function () {
    var $wrapper = this.$phoneWrapper;

    $wrapper
      .find('.js-phone--StartUpContent')
      .remove();
  },


  // Check Code Content
  addCheckCodeContent: function () {
    var $this = this;
    var $phoneInput = this.$phoneInput;
    var $wrapper = this.$phoneWrapper;

    // Readonly
    $phoneInput.attr('readonly', true);

    // Content
    var $content = $('<div class="phone-validator js-phone--CheckCodeContent"></div>').appendTo($wrapper);
    $('<p>На номер '+$phoneInput.val()+' в течение 1,5 минуты придет SMS с кодом подтверждения</p>')
      .appendTo($content);

    $('<input class="form-control js-phone--code-input" />')
      .appendTo($content)
      .attr('type', 'text')
      .attr('placeholder', 'Введите код')
      .attr('data-val', true)
      .attr('data-val-required', true);

    $('<a class="btn btn-primary js-phone--validate"></a>')
      .appendTo($content)
      .attr('href', '#')
      .text('Подтвердить номер');

    /*$('<a class="btn btn-link js-phone--later"></a>')
      .appendTo($content)
      .attr('href', '#')
      .text('Подтвердить позже');*/

    $phoneInput.attr('readonly', false);
    $('<a class="btn btn-link js-phone--repeat disabled"></a>')
      .appendTo($content)
      .attr('href', '#')
      .html('Отправить код повторно <span class="timer_wrap">через ...</span>');

    $this.timerStart(90);

    $('<a class="btn-link js-phone--feedback"></a>')
      .appendTo($content)
      .attr('href', '#')
      .html('<br/>Возникли сложности? Cвяжитесь с нами');

    /*setTimeout(function () {
      $phoneInput.attr('readonly', false);
      $('<a class="btn btn-link js-phone--repeat"></a>')
        .appendTo($content)
        .attr('href', '#')
        .html('Отправить код повторно через <span class="timer">1:30</span>');
    }, 90000);*/
  },

  timerStart: function (count) {
  	if(timing === false){
		var counter=setInterval(timer, 1000); //1000 will  run it every 1 second
	}

	function timer()
	{
	  count=count-1;
	  if (count <= 0)
	  {
	     clearInterval(counter);
	     $('.timer_wrap').html('');
	     $('.js-phone--repeat').removeClass('disabled');
	     timing = false;
	     return;
	  }
	  $('.js-phone--repeat').addClass('disabled');
	  var  currentMinutes = Math.floor(count / 60);
           currentSeconds = count % 60;
	  $(".timer_wrap").text('через '+currentMinutes+':'+currentSeconds);
	  timing = true;
	}
  },

  removeCheckCodeContent: function () {
    var $wrapper = this.$phoneWrapper;

    $wrapper
      .find('.js-phone--CheckCodeContent')
      .remove();
  },

  // Errors change number
  showChangePhoneInputErrors: function (message) {
    var $this = this;

    $this.$phoneInput.addClass('input-validation-error');
console.log($this.$phoneWrapper);
console.log($this.$phoneInput);
console.log(message);
    if (message) {
      $this.$phoneInput.after('<div class="phone-validator__error">' + message + '</div>');
    }
  },

  clearChangePhoneInputErrors: function () {
    var $this = this;
    $this.$phoneInput.removeClass('input-validation-error');

    $this.$phoneWrapper
      .find('.phone-validator__error')
      .remove();
  },

  // Errors apply code
  showCodeInputErrors: function (message) {
    var $this = this;
    var $codeInput = $this.$phoneWrapper.find('.js-phone--code-input');

    $codeInput.addClass('input-validation-error');
console.log($this.$phoneWrapper);
console.log($codeInput);
console.log(message);
    if (message) {
      $codeInput.after('<div class="phone-validator__error">' + message + '</div>');
    }
  },

  clearCodeInputErrors: function () {
    var $this = this;
    $this.$phoneWrapper
      .find('.js-phone--code-input')
      .removeClass('input-validation-error');

    $this.$phoneWrapper
      .find('.phone-validator__error')
      .remove();
  },

  // Messages
  addMessageContent: function (message, isError) {
    var type = isError ? 'error' : 'success';

    var $this = this;
    var $wrapper = $this.$phoneWrapper;

    // Content
    var $content = $('<div class="phone-validator js-phone--MessageContent"></div>').appendTo($wrapper);
    $('<div class="phone-validator__message phone-validator__message_' + type + '"></div>')
      .appendTo($content)
      .html(message);
  },

  removeMessageContent: function () {
    var $this = this;
    var $wrapper = $this.$phoneWrapper;

    $wrapper
      .find('.js-phone--MessageContent')
      .remove();
      //.fadeOut(500, function() { $(this).remove(); });
  }
};

// Init phone magic
var timing =false,
	phone_static = jQuery('.js-phone--input:first').val(),
	is_phone_valid = false;


$( document ).ready(function() {
	$('.js-phone--wrapper').each(function () {
	  var phoneValidatorObject = Object.create(phoneValidator);
	  phoneValidatorObject.init(this);
	});

	// Open validation modal (visibility by default)
	$.ajax({
	  type: "POST",
	  url: '/udata/users/is_phone_valid.json',
	  async: false,
	  dataType: "json",
	  data: {phone: phone_static},
	  contentType: "application/json; charset=utf-8",
	  success: function (data) {
	    if (data.status === 'successful') {
	      is_phone_valid = true;
	    } else if (data.status === 'error') {

		    var phone_validation = $.cookie("phone_validation");
		    if (phone_validation == null) {
		    	console.log('need to show');
		    	var date = new Date();
				var minutes = 30;
				date.setTime(date.getTime() + (minutes * 60 * 1000));
		        jQuery.cookie('phone_validation', 'yes', {
			        expires: date, // the number of days cookie  will be effective
			        path: '/'
			    });

		        $('#phoneValidateModal').modal('show');
		    }else{
		    	console.log('nooo need to show');
		    }


	    }
	  },
	  error: function (XMLHttpRequest, textStatus, errorThrown) {
          console.log("Status: " + textStatus);
          console.log("Error: " + errorThrown);
	    //alert("Status: " + textStatus);
	    //alert("Error: " + errorThrown);
	  }
	})



	// add phone change button on setting page
    var $phoneInput = $('form[action="/users/settings_do_pre/"] .js-phone:first');

    if($phoneInput.length > 0){
    	//$phoneInput.prop('readonly','true');
    	$phoneInput.prop('disabled','true').attr('style','color:#999');

	    var $phoneInputWrapper = $phoneInput.parent();
	    	$phoneFormGroup = $phoneInput.parents('.form-group');

	    $phoneInputWrapper.attr('class','col-xs-4');
	    var $btnWrap = $('<div class="col-xs-5"></div>')
	      .appendTo($phoneFormGroup);




	    $('<a class="btn btn-primary js-phone--change-phone"></a>')
	      .appendTo($btnWrap)
	      .attr('href', '#')
	      .text('Изменить')
	      .on('click', function (e) {
	        e.preventDefault();
			$('#phoneValidateModal').modal('hide');
	        $('#phoneChangeModal').modal('show');
	      });

	     if(!is_phone_valid){
			$('<a class="btn btn-primary js-phone--send-code"></a>')
		      .appendTo($btnWrap)
		      .attr('href', '#')
		      .attr('style', 'margin-left:10px')
		      .text('Подтвердить')
		      .on('click', function (e) {
		        e.preventDefault();
		        $('#phoneChangeModal').modal('hide');
				$('#phoneValidateModal').modal('show');
		      });
		}
	     console.log(is_phone_valid);
	     // прячем кнопку подтвердить код телефона, если он уже подтвержден
		if(is_phone_valid){
			$('.js-phone--send-code').hide();
		}
    }
});