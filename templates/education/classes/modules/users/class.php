<?php
class users_custom extends def_module {
    public function __construct($self) {
        $self->__loadLib("/phone_validator.php", (dirname(__FILE__)));
        $self->__implement("users_phone_validator");


        $self->__loadLib("/users_sync.php", (dirname(__FILE__)));
        $self->__implement("users_sync");

        $self->__loadLib("/users_sync_export.php", (dirname(__FILE__)));
        $self->__implement("users_sync_export");

        $self->__loadLib("/users_sync_import.php", (dirname(__FILE__)));
        $self->__implement("users_sync_import");

        $self->__loadLib("/ormcostars.php", (dirname(__FILE__)));
        $self->__implement("ormcostars");
    }

    public function login_do_pre() {
        $_REQUEST['login'] = $_REQUEST['email'] = $_REQUEST['e-mail'];
        return $this->login_do();
    }

    public function checkauth() {
        $_REQUEST['login'] = $_REQUEST['email'] = $_REQUEST['e-mail'];

        $login = getRequest('login');
        $password = getRequest('password');

        $permissions = permissionsCollection::getInstance();

        $user = $permissions->checkLogin($login, $password);

        if ($user instanceof iUmiObject) {
            return array('status' => 'successful');
        } else {
            return array(
                'status' => 'error',
                'field' => 'email2',
                'result' => 'Неверный логин или пароль'
            );
        }
    }

    public function checkreg() {
        $_REQUEST['login'] = $_REQUEST['email'] = $_REQUEST['data']['new']['e-mail'];

        $without_act = (bool) regedit::getInstance()->getVal("//modules/users/without_act");

        // проверка логина
        $login = getRequest('login');
        $userId = false;
        $public = true;
        $valid = false;
        //Filters
        $login = trim($login);
        $valid = $login ? $login : (bool) $login;
        //Validators
        $minLength = 1;
        if (!preg_match("/^\S+$/", $login) && $login) {
            return array(
                'status' => 'error',
                'field' => 'e-mail',
                'result' => 'Неверный формат email'
            );
        }
        if ($public) {
            $minLength = 3;
            if (mb_strlen($login, 'utf-8') > 40) {
                return array(
                    'status' => 'error',
                    'field' => 'e-mail',
                    'result' => 'Слишком длинный e-mail'
                );
            }
        }
        if (mb_strlen($login, 'utf-8') < $minLength) {
            return array(
                'status' => 'error',
                'field' => 'e-mail',
                'result' => 'Слишком короткий e-mail'
            );
        }
        if (!$this->checkIsUniqueLogin($login, $userId)) {
            return array(
                'status' => 'error',
                'field' => 'e-mail',
                'result' => 'Пользователь с таким e-mail уже существует'
            );
        }
        if (!umiMail::checkEmail($login)) {
            return array(
                'status' => 'error',
                'field' => 'e-mail',
                'result' => 'Неверный формат email'
            );
        }
        if (!$this->checkIsUniqueEmail($login, $userId)) {
            return array(
                'status' => 'error',
                'field' => 'e-mail',
                'result' => 'Пользователь с таким e-mail уже существует'
            );
        }

        // проверка пароля
        $password = getRequest('password');
        $passwordConfirmation = getRequest('password_confirm');
        $login = getRequest('login');

        $valid = false;
        //Filters
        $password = trim($password);
        $valid = $password ? $password : (bool) $password;
        //Validators
        $minLength = 1;
        if (!preg_match("/^\S+$/", $password) && $password) {
            return array(
                'status' => 'error',
                'field' => 'password',
                'result' => 'Неверный формат пароля'
            );
        }
        if ($login && ($password == trim($login))) {
            return array(
                'status' => 'error',
                'field' => 'password',
                'result' => 'Пароль совпадает с e-mail'
            );
        }
        if ($public) {
            $minLength = 3;
            if (!is_null($passwordConfirmation)) {
                if ($password != $passwordConfirmation) {
                    return array(
                        'status' => 'error',
                        'field' => 'password',
                        'result' => 'Ошибка ввода подтверждения пароля'
                    );
                }
            }
        }
        if (mb_strlen($password, 'utf-8') < $minLength) {
            return array(
                'status' => 'error',
                'field' => 'password',
                'result' => 'Слишком короткий пароль'
            );
        }

        if (!umiCaptcha::checkCaptcha()) {
            return array(
                'status' => 'error',
                'result' => 'Неверная капча'
            );
        }

        return array('status' => 'successful');
    }

    public function registrate_do_pre() {
		$data = $_REQUEST['data'];
		if (isset($data['new']['phone1']) && !empty($data['new']['phone1'])) {
			$this->redirect($this->pre_lang . "/users/registrate_done/");
		}

        $_REQUEST['login'] = $_REQUEST['email'] = $_REQUEST['data']['new']['e-mail'];
        $without_act = (bool) regedit::getInstance()->getVal("//modules/users/without_act");

        $login = $this->validateLogin(getRequest('login'), false, true);
        $password = $this->validatePassword(getRequest('password'), getRequest('password_confirm'), getRequest('login'), true);
        $email = $this->validateEmail(getRequest('email'), false, !$without_act);

        if (!umiCaptcha::checkCaptcha()) {
            $this->errorAddErrors('errors_wrong_captcha');
        }
        if (!umiCaptcha::checkCaptcha() || !$login || !$password || !$email) {
            $this->errorSetErrorPage('/users/registrate/');
            $this->errorThrow('public');
        }
        return $this->registrate_do();
    }

    public function settings_do_pre() {
        // take current user id
        $userId = $this->user_id;

        if(permissionsCollection::getInstance()->getGuestId() == $userId){
            return $this->redirect('/users/auth/');
        }

        $user = $this->umiObjectsCollection->getObject($userId);
        if ($user instanceof iUmiObject) {
            $email = $_REQUEST['data'][$userId]['e-mail'];

			if($user->login != $email ){
				$_REQUEST['login'] = $_REQUEST['data'][$userId]['login'] = $_REQUEST['data'][$userId]['email'] = $_REQUEST['data'][$userId]['e-mail'] = $user->login;
				if ($this->checkIsUniqueEmail($email, $userId)) {
					$user->setValue("email_dlya_izmeneniya", $email);
					$user->setValue("pole_koda_aktivaciii_novogo_email", sha1(time(true)));
					$user->setValue("data_popytki_izmeneniya_email", date('c'));
					$user->commit();

					$this->send_confirm_email_letter();
				}
			}

//            if ($this->checkIsUniqueEmail($email, $userId)) {
//                if($user->login != $email){
//                    $oEventPoint = new umiEventPoint("users_settings_do");
//                    $oEventPoint->setMode("before");
//                    $oEventPoint->setParam("user_id", $userId);
//                    $this->setEventPoint($oEventPoint);
//
//                    $user->setName($email);
//                    $user->login = $email;
//
//                    $oEventPoint->setMode("after");
//                    $this->setEventPoint($oEventPoint);
//                }
//            }
        }

        return $this->settings_do();
    }

    public static function getRandomPassword($length = 12) {
        $length = 8;
        // if(function_exists('openssl_random_pseudo_bytes')) {
        // $password = base64_encode(openssl_random_pseudo_bytes($length, $strong));
        // if($strong == TRUE)
        // return substr($password, 0, $length);
        // }
        $avLetters = "1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
        $size = strlen($avLetters);

        $npass = "";
        for ($i = 0; $i < $length; $i++) {
            $c = rand(0, $size - 1);
            $npass .= $avLetters[$c];
        }
        return $npass;
    }

    public function lp_t() {
        $objectId = 27298;
        $objectId = 59;
        $object = umiObjectsCollection::getInstance()->getObject($objectId);
        var_dump($object->lname);
        var_dump($object->password);
        exit('ggg');
    }

    //lp_order user reg
    public function lpreg() {
        file_put_contents(CURRENT_WORKING_DIR . '/traceajaxlp.txt', "\n\n user_reg" . print_r($_REQUEST, true) . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);

        /*
         * email
         * password
         * password2
         * lname
         * fname
         * father_name
         * bd
         * phone
         * city
         * who
         * company
         * */

        /*
         * login
         * password
         * password_confirm
         * data[new][lname]
         * data[new][fname]
         * data[new][father_name]
         * data[new][e-mail]
         * data[new][country]
         * data[new][phone]
         * data[new][region]
         * data[new][city]
         * data[new][prof_status]
         * data[new][bd]
         * */

        /* test */
        /*
          $_REQUEST['email'] = 'reg3@test.ru';
          $_REQUEST['password'] = '12345';
          $_REQUEST['password2'] = '12345';;
          $_REQUEST['lname'] = 'test1';
          $_REQUEST['fname'] = 'test2';
          $_REQUEST['father_name'] = 'test3';
          $_REQUEST['phone'] = '89111112233';
          $_REQUEST['city'] = 'Санкт Петербург';
          $_REQUEST['who'] = 'Ортодонт';
          $_REQUEST['bd'] = '1980-01-04';
          $_REQUEST['company'] = 'test4';
         */
        $login = $email = $_REQUEST['login'] = $_REQUEST['email'] = $_REQUEST['e-mail'] = $_REQUEST['data']['new']['email'];
        $_REQUEST['password'] = $_REQUEST['data']['new']['password'];

        $_REQUEST['password_confirm'] = $_REQUEST['data']['new']['password2'];

        unset($_REQUEST['data']['new']['password']);
        unset($_REQUEST['data']['new']['password2']);
        //$_REQUEST['data']['new']['lname'] = $_REQUEST['lname'];
        //$_REQUEST['data']['new']['fname'] = $_REQUEST['fname'];
        //$_REQUEST['data']['new']['father_name'] = $_REQUEST['father_name'];
        $_REQUEST['data']['new']['e-mail'] = $email;
        //$_REQUEST['data']['new']['country'] = 12115;// Россия //$_REQUEST['email']; // TODO
        //$_REQUEST['data']['new']['phone'] = $_REQUEST['phone'];
        //$_REQUEST['data']['new']['region'] = 9870;//МОсква и МО //$_REQUEST['email']; // TODO
        if ($_REQUEST['data']['new']['country'] != 'Россия') {
            $_REQUEST['data']['new']['region'] = 16761; // Не Россия
        }
        //$_REQUEST['data']['new']['city'] = $_REQUEST['city'];
        $_REQUEST['data']['new']['prof_status'] = $_REQUEST['data']['new']['who']; //$_REQUEST['who'];
        //$_REQUEST['data']['new']['bd'] = $_REQUEST['bd'];
        //$_REQUEST['data']['new']['company'] = $_REQUEST['company'];
        $_REQUEST['data']['new']['agree'] = 1;

        //$email = $_REQUEST['login'] = $_REQUEST['email'] = $_REQUEST['e-mail'] = $_REQUEST['data']['new']['e-mail'];

        if ($this->is_auth()) {
            return array(
                'status' => 'error',
                'field' => 'e-mail',
                'result' => 'Вы уже авторизованы'
            );
        }

        $template = 'default';

        $objectTypes = umiObjectTypesCollection::getInstance();
        $regedit = regedit::getInstance();

        $without_act = true; //(bool) $regedit->getVal("//modules/users/without_act");

        $objectTypeId = $objectTypes->getTypeIdByHierarchyTypeName("users", "user");

        // проверка логина
        $login = getRequest('login');
        $userId = false;
        $public = true;
        $valid = false;

        //Filters
        $login = trim($login);
        $valid = $login ? $login : (bool) $login;
        //Validators
        $minLength = 1;
        if (!preg_match("/^\S+$/", $login) && $login) {
            return array(
                'status' => 'error',
                'field' => 'e-mail',
                'result' => 'Неверный формат email'
            );
        }
        if ($public) {
            $minLength = 3;
            if (mb_strlen($login, 'utf-8') > 40) {
                return array(
                    'status' => 'error',
                    'field' => 'e-mail',
                    'result' => 'Слишком длинный e-mail'
                );
            }
        }
        if (mb_strlen($login, 'utf-8') < $minLength) {
            return array(
                'status' => 'error',
                'field' => 'e-mail',
                'result' => 'Слишком короткий e-mail'
            );
        }
        if (!$this->checkIsUniqueLogin($login, $userId)) {
            return array(
                'status' => 'error',
                'field' => 'e-mail',
                'result' => 'Пользователь с таким e-mail уже существует'
            );
        }
        if (!umiMail::checkEmail($login)) {
            return array(
                'status' => 'error',
                'field' => 'e-mail',
                'result' => 'Неверный формат email'
            );
        }
        if (!$this->checkIsUniqueEmail($login, $userId)) {
            return array(
                'status' => 'error',
                'field' => 'e-mail',
                'result' => 'Пользователь с таким e-mail уже существует'
            );
        }

        // проверка пароля
        $password = getRequest('password');
        $passwordConfirmation = getRequest('password_confirm');
        $login = getRequest('login');

        $valid = false;
        //Filters
        $password = trim($password);
        $valid = $password ? $password : (bool) $password;
        //Validators
        $minLength = 1;
        if (!preg_match("/^\S+$/", $password) && $password) {
            return array(
                'status' => 'error',
                'field' => 'password',
                'result' => 'Неверный формат пароля'
            );
        }
        if ($login && ($password == trim($login))) {
            return array(
                'status' => 'error',
                'field' => 'password',
                'result' => 'Пароль совпадает с e-mail'
            );
        }
        if ($public) {
            $minLength = 3;
            if (!is_null($passwordConfirmation)) {
                if ($password != $passwordConfirmation) {
                    return array(
                        'status' => 'error',
                        'field' => 'password',
                        'result' => 'Ошибка ввода подтверждения пароля'
                    );
                }
            }
        }
        if (mb_strlen($password, 'utf-8') < $minLength) {
            return array(
                'status' => 'error',
                'field' => 'password',
                'result' => 'Слишком короткий пароль'
            );
        }

        if (!umiCaptcha::checkCaptcha()) {
            return array(
                'status' => 'error',
                'result' => 'Неверная капча'
            );
        }

        $oEventPoint = new umiEventPoint("users_registrate");
        $oEventPoint->setMode("before");
        $oEventPoint->setParam("login", $login);
        $oEventPoint->addRef("password", $password);
        $oEventPoint->addRef("email", $email);
        users::setEventPoint($oEventPoint);

        $objectId = umiObjectsCollection::getInstance()->addObject($login, $objectTypeId);
        $activationCode = md5($login . time());

        $object = umiObjectsCollection::getInstance()->getObject($objectId);

        $object->setValue("login", $login);
        $object->setValue("password", md5($password));
        //file_put_contents(CURRENT_WORKING_DIR . '/traceajaxlp.txt', "\n password" . print_r($password, true) . "\n", FILE_APPEND);

        $object->setValue("e-mail", $email);

        $object->setValue("is_activated", $without_act);
        $object->setValue("activate_code", $activationCode);
        $object->setValue("referer", urldecode(getSession("http_referer")));
        $object->setValue("target", urldecode(getSession("http_target")));
        $date = new umiDate();
        $object->setValue("register_date", $date->getCurrentTimeStamp());
        $object->setOwnerId($objectId);

        if ($without_act) {
            $_SESSION['cms_login'] = $login;
            $_SESSION['cms_pass'] = md5($password);
            $_SESSION['user_id'] = $objectId;
            session_commit();
        }

        $group_id = $regedit->getVal("//modules/users/def_group");
        $object->setValue("groups", Array($group_id));

        /**
         * @var data|DataForms $data_module
         */
        $data_module = cmsController::getInstance()->getModule('data');
        $data_module->saveEditedObjectWithIgnorePermissions($objectId, true, true);

        $object->commit();
        //Forming mail...
        list(
                $template_mail, $template_mail_subject, $template_mail_noactivation, $template_mail_subject_noactivation
                ) = def_module::loadTemplatesForMail(
                        "users/register/" . $template, "mail_registrated", "mail_registrated_subject", "mail_registrated_noactivation", "mail_registrated_subject_noactivation"
        );

        if ($without_act && $template_mail_noactivation && $template_mail_subject_noactivation) {
            $template_mail = $template_mail_noactivation;
            $template_mail_subject = $template_mail_subject_noactivation;
        }

        $mailData = array(
            'user_id' => $objectId,
            'domain' => $domain = cmsController::getInstance()->getCurrentDomain()->getCurrentHostName(),
            'activate_link' => getSelectedServerProtocol() . "://" . $domain . $this->pre_lang . "/users/activate/" . $activationCode . "/",
            'login' => $login,
            'password' => $password,
            'lname' => $object->getValue("lname"),
            'fname' => $object->getValue("fname"),
            'father_name' => $object->getValue("father_name"),
        );

        $mailContent = users::parseTemplateForMail($template_mail, $mailData, false, $objectId);
        $mailSubject = users::parseTemplateForMail($template_mail_subject, $mailData, false, $objectId);

        $fio = $object->getValue("lname") . " " . $object->getValue("fname") . " " . $object->getValue("father_name");

        $email_from = $regedit->getVal("//settings/email_from");
        $fio_from = $regedit->getVal("//settings/fio_from");

        $registrationMail = new umiMail();
        $registrationMail->addRecipient($email, $fio);
        $registrationMail->setFrom($email_from, $fio_from);
        $registrationMail->setSubject($mailSubject);
        $registrationMail->setContent($mailContent);
        $registrationMail->commit();
        $registrationMail->send();

        $oEventPoint = new umiEventPoint("users_registrate");
        $oEventPoint->setMode("after");
        $oEventPoint->setParam("user_id", $objectId);
        $oEventPoint->setParam("login", $login);
        users::setEventPoint($oEventPoint);

        // sync user to other site
        $this->exportNewUser($object);

        return array('status' => 'successful', 'user_id' => $objectId, 'email' => $login, 'password' => $password);
    }

    public function update_password($activate_code = false) {
        $object = false;
        $block_arr = Array();
        if (!$activate_code) {
            $activate_code = (string) getRequest('param0');
            $activate_code = trim($activate_code);
        }

        $sel = new selector('objects');
        $sel->types('object-type')->name('users', 'user');
        $sel->where('activate_code')->equals($activate_code);
        $sel->limit(0, 1);
        if ($sel->first) {
            $object = $sel->first;
        }

        $block_arr['attribute:activate_code'] = $activate_code;
        $block_arr['attribute:header'] = 'Восстановление пароля';
        if ($object instanceof umiObject) {
            $block_arr['attribute:status'] = "success";
            $block_arr['login'] = $object->name;
            $block_arr['id'] = $object->id;
        } else {
            $block_arr['attribute:status'] = "fail";
        }

        return $block_arr;
    }

    public function set_new_password($activate_code = false, $template = "default") {
        $object = false;
        $user_id = false;
        $block_arr = Array();

        list(
            $template_restore_failed_block, $template_restore_ok_block, $template_mail_password, $template_mail_password_subject
            ) = def_module::loadTemplatesForMail("users/forget/" . $template, "restore_failed_block", "restore_ok_block", "mail_password", "mail_password_subject"
        );

        if (!$activate_code) {
            $activate_code = (string) getRequest('param0');
            $activate_code = trim($activate_code);
        }

        $password = (string) getRequest('new_password');
        $password = trim($password);

        $sel = new selector('objects');
        $sel->types('object-type')->name('users', 'user');
        $sel->where('activate_code')->equals($activate_code);
        $sel->limit(0, 1);

        if ($sel->first) {
            $object = $sel->first;
            $user_id = $object->id;
        }

        $block_arr['attribute:header'] = 'Результат изменения пароля';

        if ($object instanceof umiObject && $activate_code && $password) {
//                $password = self::getRandomPassword();
            $login = $object->getValue("login");
            $email = $object->getValue("e-mail");
            $fio = $object->getValue("lname") . " " . $object->getValue("fname") . " " . $object->getValue("father_name");

            $oEventPoint = new umiEventPoint("users_restore_password_do");
            $oEventPoint->setMode("before");
            $oEventPoint->setParam("user_id", $object->id);
            $this->setEventPoint($oEventPoint);

            $object->setValue("password", md5($password));
            $object->setValue("activate_code", "");
            $object->commit();

            $oEventPoint->setMode("after");
            $this->setEventPoint($oEventPoint);


            $email_from = regedit::getInstance()->getVal("//settings/email_from");
            $fio_from = regedit::getInstance()->getVal("//settings/fio_from");

            $mail_arr = Array();
            $mail_arr['domain'] = $domain = $_SERVER['HTTP_HOST'];
            $mail_arr['password'] = $password;
            $mail_arr['login'] = $login;

            $mail_subject = def_module::parseTemplateForMail($template_mail_password_subject, $mail_arr, false, $user_id);
            $mail_content = def_module::parseTemplateForMail($template_mail_password, $mail_arr, false, $user_id);

            $someMail = new umiMail();
            $someMail->addRecipient($email, $fio);
            $someMail->setFrom($email_from, $fio_from);
            $someMail->setSubject($mail_subject);
            $someMail->setContent($mail_content);
            $someMail->commit();
            $someMail->send();

            $block_arr['attribute:status'] = "success";
            $block_arr['login'] = $login;
            $block_arr['password'] = $password;
            $block_arr['restore_result'] = 'Ваш пароль успешно изменен.<br />';

            $eventPoint = new umiEventPoint('successfulPasswordRestoring');
            $eventPoint->setMode('after');
            $eventPoint->setParam('userId', $user_id);
            $eventPoint->call();

            return $result[$template] = def_module::parseTemplate($template_restore_ok_block, $block_arr, false, $user_id);
        } else {
            $block_arr['attribute:status'] = "fail";
            $block_arr['restore_result'] = 'В процессе изменения пароля произошла ошибка. Пожалуйста, обратитесь к администратору.';
            return $result[$template] = def_module::parseTemplate($template_restore_failed_block, $block_arr);
        }
    }

    public function is_empty_field($field_name){
        $userId = $this->user_id;
        $user = $this->umiObjectsCollection->getObject($userId);
        $real_field_name = str_replace('order_', '', $field_name);
        if ($user instanceof iUmiObject) {
            return empty(trim($user->getValue($real_field_name))) ? 'true' : 'false';
        }
        return 'true';
	}

	/**
	 * Получение h1 для страницы, закрытой авторизацией, на странице авторизации
	 * @param type $path
	 * @return string
	 */
    public function get_page_name_for_not_permited_page($path){
		$hierarhy = umiHierarchy::getInstance();
		$page_id = $hierarhy->getIdByPath($path);
		$page = $hierarhy->getElement($page_id, true);
		if($page instanceof umiHierarchyElement){
			return $page->getValue('h1');
		}
		return 'Страница, требующая авторизации';
	}


	/**
	 * Отмена изменения e-mail адреса
	 * @param type $user_id
	 * @return type
	 */
	public function cancel_change_email() {
		$result = array("result" => "fail");
        $user_id = $this->user_id;
        $user = $this->umiObjectsCollection->getObject($user_id);
		if($user instanceof umiObject){
			if(!empty($user->getValue('email_dlya_izmeneniya'))){
				$user->setValue('email_dlya_izmeneniya', '');
				$user->setValue('pole_koda_aktivaciii_novogo_email', '');
				$user->setValue('data_popytki_izmeneniya_email', '');
				$user->commit();
			}
			$result["result"] = "ok";
		}

        $referer = getServer('HTTP_REFERER');
        $noRedirect = getRequest('no-redirect');

        if (!$noRedirect) {
            $this->redirect($referer);
        }

		return $result;
	}

	/**
	 * Отправка сообщения о смене контактного e-mail адреса
	 */
	public function send_confirm_email_letter() {
		$result = array("result" => "Ошибка");
        $user_id = $this->user_id;
        $user = $this->umiObjectsCollection->getObject($user_id);
		if($user instanceof umiObject){
			if(!empty($user->getValue('email_dlya_izmeneniya'))){
				$email_from = regedit::getInstance()->getVal("//settings/email_from");
				$fio_from = regedit::getInstance()->getVal("//settings/fio_from");

				list(
					$template_confirm_email_letter_content, $template_confirm_email_letter_subject
					) = def_module::loadTemplatesForMail("users/change/default", "confirm_email_letter_content", "confirm_email_letter_subject"
				);

				$mail_arr = Array();
				$mail_arr['domain'] = $_SERVER['HTTP_HOST'];
				$mail_arr['login'] = $user->getValue('login');
				$mail_arr['change_link'] = 'https://' . $_SERVER['HTTP_HOST'] . '/users/change_email_from_letter/' . $user_id . '/' . $user->getValue('pole_koda_aktivaciii_novogo_email');

				$mail_subject = def_module::parseTemplateForMail($template_confirm_email_letter_subject, $mail_arr, false, $user_id);
				$mail_content = def_module::parseTemplateForMail($template_confirm_email_letter_content, $mail_arr, false, $user_id);

	            $email = $user->getValue('email_dlya_izmeneniya');
	            $fio = trim($user->getValue("lname") . ' ' . $user->getValue("fname") . ' ' . $user->getValue("father_name"));

				$someMail = new umiMail();
				$someMail->addRecipient($email, $fio);
				$someMail->setFrom($email_from, $fio_from);
				$someMail->setSubject($mail_subject);
				$someMail->setContent($mail_content);
				$someMail->commit();
				$someMail->send();

				$result["result"] = "Письмо с подтверждением отправлено";
			}
		}

        $referer = getServer('HTTP_REFERER');
        $noRedirect = getRequest('no-redirect');

        if (!$noRedirect) {
            $this->redirect($referer);
        }

		return $result;
	}

	/**
	 * Функция, вызываемся при клике на ссылку в письме о смене e-mail адреса
	 * @param type $user_id
	 */
	public function change_email_from_letter($user_id, $confirm_code) {
		$result = array("attribute:status" => "error", "result" => "Ошибка");
        $user = $this->umiObjectsCollection->getObject($user_id);
		if($user instanceof umiObject){
			if($confirm_code == $user->getValue('pole_koda_aktivaciii_novogo_email')){
				$email = $user->getValue('email_dlya_izmeneniya');
				if ($this->checkIsUniqueEmail($email, $user_id)) {
					$oEventPoint = new umiEventPoint("users_settings_do");
					$oEventPoint->setMode("before");
					$oEventPoint->setParam("user_id", $user_id);
					$this->setEventPoint($oEventPoint);

					$user->setValue("e-mail", $email);
					$user->setValue("login", $email);

					$user->setValue('email_dlya_izmeneniya', '');
					$user->setValue('pole_koda_aktivaciii_novogo_email', '');
					$user->setValue('data_popytki_izmeneniya_email', '');
					$user->commit();

					$oEventPoint->setMode("after");
					$this->setEventPoint($oEventPoint);
					$result["attribute:status"] = "complete";
					$result["result"] = "ok";
				}
			}
		}

		return $result;
	}
}