<?php
class ormcostars extends def_module {
    /**
     * получение от 1С информации об ormco stars баллах
     * @return array
     */
    public function syncormcostars() {
//        $str = '{"Клиенты": [{"Клиент": "59", "ПотенциальныеБаллы": "150.00", "НакопленныеБаллы": "140", "ЗамороженныеБаллы": "70", "Дата сгорания": "2020-12-31", "Количество сгорающих баллов": "5" }]}';
//        $input = (array)json_decode($str, true);
        
        $logDir = CURRENT_WORKING_DIR . '/sys-temp/ormco_stars';
        if(!is_dir($logDir)) {
            mkdir($logDir, 0775, true);
        }

        file_put_contents($logDir . '/' . date('Ymd_His') . '.json', file_get_contents('php://input'));
        $input = (array) json_decode(file_get_contents('php://input'), true);
        $item_arr = array();
        if (isset($input['Клиенты'])) {
            $objectTypes = umiObjectTypesCollection::getInstance();
            $regedit = regedit::getInstance();

            $without_act = (bool) $regedit->getVal("//modules/users/without_act");
            $objectTypeId = $objectTypes->getTypeIdByHierarchyTypeName("users", "user");

            foreach ((array) $input['Клиенты'] as $item) {
                //$item = (array)$item;
                $item = (array) $item;
                //$uid = $discount = false;
                $email = '';

                $uid = (isset($item['Клиент']) && intval($item['Клиент']) > 0) ? intval($item['Клиент']) : false;

                //get user id by commonUID
                $commonUid = (isset($item['common_uid']) && intval($item['common_uid']) > 0) ? intval($item['common_uid']) : false;
                if($commonUid !== false){
                    $sel = new selector('objects');
                    $sel->types('object-type')->name('users', 'user');
                    $sel->where('common_uid')->equals($commonUid);
                    $sel->option('return')->value('id');
                    $sel->limit(0, 1);

                    if($user = $sel->first){
                        $uid = isset($user["id"]) ? $user["id"] : false;
                    }
                }


                $yet_ormco_star = (isset($item['НакопленныеБаллы']) && intval($item['НакопленныеБаллы']) >= 0) ? intval($item['НакопленныеБаллы']) : false;
                $potential_ormco_star = (isset($item['ПотенциальныеБаллы']) && intval($item['ПотенциальныеБаллы']) >= 0) ? intval($item['ПотенциальныеБаллы']) : false;
                $freez_ormco_star = (isset($item['ЗамороженныеБаллы']) && intval($item['ЗамороженныеБаллы']) >= 0) ? intval($item['ЗамороженныеБаллы']) : false;
                $lost_date_ormco_star = (isset($item['Дата сгорания']) && !empty($item['Дата сгорания'])) ? new umiDate(strtotime($item['Дата сгорания'])) : null;
                $lost_size_ormco_star = (isset($item['Количество сгорающих баллов']) && intval($item['Количество сгорающих баллов']) >= 0) ? intval($item['Количество сгорающих баллов']) : false;

                // У клиента нет UID - нужно создать клиента или проверить, что он уже есть
                /*if ($uid === false){
                    $email = isset($item['email']) ? $item['email'] : false;

                    // ищем клиента по e-mail
                    $users = new selector('objects');
                    $users->types('object-type')->name('users', 'user');
                    $users->where('login')->equals($email);
                    $users->limit(0, 1);
                    foreach ($users as $user){
                        $uid = $user->id;
                    }

                    // создаем клиента
                    if ($uid === false){
                        $password = $this->get_random_password();

                        $fname = isset($item['Имя']) ? $item['Имя'] : '';
                        $lname = isset($item['Фамилия']) ? $item['Фамилия'] : '';
                        $father_name = isset($item['Отчество']) ? $item['Отчество'] : '';

                        $oEventPoint = new umiEventPoint("users_registrate");
                        $oEventPoint->setMode("before");
                        $oEventPoint->setParam("login", $email);
                        $oEventPoint->addRef("password", $password);
                        $oEventPoint->addRef("email", $email);
                        users::setEventPoint($oEventPoint);

                        $uid = umiObjectsCollection::getInstance()->addObject($email, $objectTypeId);
                        $activationCode = md5($email . time());

                        $object = umiObjectsCollection::getInstance()->getObject($uid);

                        $object->setName($email);
                        $object->setValue("login", $email);
                        $object->setValue("password", md5($password));
                        $object->setValue("e-mail", $email);

                        $object->setValue("is_activated", $without_act);
                        $object->setValue("activate_code", $activationCode);
                        $object->setValue("referer", urldecode(getSession("http_referer")));
                        $object->setValue("target", urldecode(getSession("http_target")));
                        $date = new umiDate();
                        $object->setValue("register_date", $date->getCurrentTimeStamp());
                        $object->setOwnerId($uid);

                        $group_id = $regedit->getVal("//modules/users/def_group");
                        $object->setValue("groups", Array($group_id));

                        $object->setValue('fname', $fname);
                        $object->setValue('lname', $lname);
                        $object->setValue('father_name', $father_name);

                        $object->commit();

                        //Forming mail...
                        list(
                            $template_mail, $template_mail_subject, $template_mail_noactivation, $template_mail_subject_noactivation
                            ) = def_module::loadTemplatesForMail(
                            "users/register/default",
                            "mail_registrated",
                            "mail_registrated_subject",
                            "mail_registrated_noactivation",
                            "mail_registrated_subject_noactivation"
                        );

                        if ($without_act && $template_mail_noactivation && $template_mail_subject_noactivation) {
                            $template_mail = $template_mail_noactivation;
                            $template_mail_subject = $template_mail_subject_noactivation;
                        }

                        $mailData = array(
                            'user_id' => $uid,
                            'domain' => $domain = cmsController::getInstance()->getCurrentDomain()->getCurrentHostName(),
                            'activate_link' => getSelectedServerProtocol() . "://" . $domain . $this->module->pre_lang . "/users/activate/" . $activationCode . "/",
                            'login' => $email,
                            'password' => $password,
                            'lname' => $object->getValue("lname"),
                            'fname' => $object->getValue("fname"),
                            'father_name' => $object->getValue("father_name"),
                        );

                        $mailContent = users::parseTemplateForMail($template_mail, $mailData, false, $uid);
                        $mailSubject = users::parseTemplateForMail($template_mail_subject, $mailData, false, $uid);

                        $fio = $object->getValue("lname") . " " . $object->getValue("fname") . " " . $object->getValue("father_name");

                        $email_from = $regedit->getVal("//settings/email_from");
                        $fio_from = $regedit->getVal("//settings/fio_from");

                        $registrationMail = new umiMail();
                        $registrationMail->addRecipient($email, $fio);
                        $registrationMail->setFrom($email_from, $fio_from);
                        $registrationMail->setSubject($mailSubject);
                        $registrationMail->setContent($mailContent);
                        $registrationMail->commit();
                        $registrationMail->send();

                        $oEventPoint = new umiEventPoint("users_registrate");
                        $oEventPoint->setMode("after");
                        $oEventPoint->setParam("user_id", $uid);
                        $oEventPoint->setParam("login", $email);
                        users::setEventPoint($oEventPoint);
                    }
                }*/

                if ($uid !== false && $yet_ormco_star !== false && $potential_ormco_star !== false && $freez_ormco_star !== false) {
                    //save discount
                    $user = umiObjectsCollection::getInstance()->getObject($uid);
                    $status = 'notfound';
                    if ($user instanceof umiObject) {
                        $changed = $user->yet_ormco_star != $yet_ormco_star
                            || $user->potential_ormco_star != $potential_ormco_star
                            || $user->freez_ormco_star != $freez_ormco_star
                            || $user->data_sgoraniya_ballov != $lost_date_ormco_star
                            || $user->kolichestvo_sgorayuwih_ballov != $lost_size_ormco_star;
                        if ($changed) {
                            $user->yet_ormco_star = $yet_ormco_star;
                            $user->potential_ormco_star = $potential_ormco_star;
                            $user->freez_ormco_star = $freez_ormco_star;
                            $user->setValue('data_sgoraniya_ballov', $lost_date_ormco_star);
                            $user->setValue('kolichestvo_sgorayuwih_ballov', $lost_size_ormco_star);
                            $user->ormco_star_notify = 1;
                            if (!($user->start_ormco_star)) {
                                $user->start_ormco_star = time();
                            }
                            $user->commit();

                            // send mail
                            $this->sendOrmcoStarsNotify($user);

                            $status = 'saved';
                        } else {
                            $status = 'not changed';
                        }
                    }

                    $item_arr[] = array(
                        'uid' => $uid,
                        'user_email' => $email,
                        'НакопленныеБаллы' => $yet_ormco_star,
                        'ПотенциальныеБаллы' => $potential_ormco_star,
                        'ЗамороженныеБаллы' => $freez_ormco_star,
                        'ДатаCгорания' => $lost_date_ormco_star instanceof umiDate ? $lost_date_ormco_star->getFormattedDate('d.m.Y') : false,
                        'КоличествоСгорающихБаллов' => $lost_size_ormco_star,
                        'status' => $status);
                }
            }
            $result = array('status' => 'successfull', 'item' => $item_arr);
        } else {
            $result = array('status' => 'fail');
        }

        $buffer = outputBuffer::current('HTTPOutputBuffer');
        $buffer->charset('utf-8');
        $buffer->contentType('application/json');
        $buffer->push(json_encode($result));
        $buffer->end();
    }

    public function sendOrmcoStarsNotify($user, $force = false) {
        if (!$user) {
            return;
        }

        $regedit = regedit::getInstance();
        $cmsController = cmsController::getInstance();
        $domains = domainsCollection::getInstance();
        $domainId = $cmsController->getCurrentDomain()->getId();
        $defaultDomainId = $domains->getDefaultDomain()->getId();

        if ($regedit->getVal("//modules/emarket/from-email/{$domainId}")) {
            $fromMail = $regedit->getVal("//modules/emarket/from-email/{$domainId}");
            $fromName = $regedit->getVal("//modules/emarket/from-name/{$domainId}");
        } elseif ($regedit->getVal("//modules/emarket/from-email/{$defaultDomainId}")) {
            $fromMail = $regedit->getVal("//modules/emarket/from-email/{$defaultDomainId}");
            $fromName = $regedit->getVal("//modules/emarket/from-name/{$defaultDomainId}");
        } else {
            $fromMail = $regedit->getVal("//modules/emarket/from-email");
            $fromName = $regedit->getVal("//modules/emarket/from-name");
        }

        $email = $name = $user->getValue('e-mail');

        $yet_ormco_star = $user->yet_ormco_star;
        $potential_ormco_star = $user->potential_ormco_star;
        $freez_ormco_star = $user->freez_ormco_star;

        $hash = md5(implode('::', [
            $yet_ormco_star,
            $potential_ormco_star,
            $freez_ormco_star
        ]));

        if(!$force && $user->getValue(SiteUsersUserModel::field_ormco_stars_notify_hash) == $hash) {
            return;
        }

        $subject = "Изменение звезд Ormco Stars";

        $content = "Сейчас у Вас имеется следующее количество звезд. <br/> Накопленные звезды: $yet_ormco_star<br/>  Потенциальные звезды: $potential_ormco_star<br/>   Замороженные звезды: $freez_ormco_star<br/> ";
        $letter = new umiMail();
        $letter->addRecipient($email, $name);
        $letter->setFrom($fromMail, $fromName);
        $letter->setSubject($subject);
        $letter->setContent($content);
        $letter->commit();
        $letter->send();

        $user->setValue(SiteUsersUserModel::field_ormco_stars_notify_hash, $hash);
        $user->commit();
    }
    
    public function testSendOrmcoStars() {
        $user = umiObjectsCollection::getInstance()->getObject(1033337);
        if(!$user instanceof umiObject) {
            throw new publicException('Не найден пользователь 1033337');
        }
        
        $this->sendOrmcoStarsNotify($user);
    }

    public function is_loyalty_change() {
        if ($this->is_auth()) {
            $umiPropertiesHelper = $this->umiPropertiesHelper;
            $userId = $this->user_id;
            $userTypeId = $this->umiTypesHelper->getObjectTypeIdByGuid('users-user');

            $phone_valid = $umiPropertiesHelper->getPropertyValue($userId, 'ormco_star_notify', $userTypeId);
            if ($phone_valid == 1) {
                $result = array(
                    'status' => 'successful'
                );
            } else {
                $result = array(
                    'result' => 0,
                    'status' => 'error',
                );
            }
        } else {
            $result = array(
                'result' => 0,
                'status' => 'error'
            );
        }
        return $result;
    }

    public function del_loyalty_notify() {
        if ($this->is_auth()) {
            $userId = $this->user_id;
            $user = umiObjectsCollection::getInstance()->getObject($userId);
            if ($user instanceof umiObject) {
                $user->ormco_star_notify = 0;
            }
        }
        return;
    }

    public function loyalty() {
        if ($this->is_auth()) {
            $userId = $this->user_id;
            $user = umiObjectsCollection::getInstance()->getObject($userId);
            if ($user instanceof umiObject) {
                $result = array(
                    'yet_ormco_star' => $user->yet_ormco_star,
                    'potential_ormco_star' => $user->potential_ormco_star,
                    'freez_ormco_star' => $user->freez_ormco_star,
                    'kolichestvo_sgorayuwih_ballov' => $user->getValue('kolichestvo_sgorayuwih_ballov'),
                );
                if ($user->start_ormco_star instanceof umiDate) {
                    $start_ormco_star = $user->start_ormco_star->getFormattedDate("U");
                    $result['start_ormco_star'] = date("d.m.Y", $start_ormco_star);
                }
                if ($user->getValue('data_sgoraniya_ballov') instanceof umiDate) {
                    $finish_ormco_star = $user->getValue('data_sgoraniya_ballov')->getFormattedDate("U");

                    $day_left = floor(($finish_ormco_star - time()) / (60 * 60 * 24));
//                    $day_left = '9';
                    $day_form = '';
                    $last_digit = substr($day_left, strlen($day_left) - 1, 1);
                    $last_two_digit = substr($day_left, strlen($day_left) - 2, 2);
                    if ($last_digit == 1 && $last_two_digit != 11) {
                        $day_form = 'день';
                    } elseif (in_array($last_digit, array(2, 3, 4)) && !in_array($last_two_digit, array(12, 13, 14))) {
                        $day_form = 'дня';
                    } else {
                        $day_form = 'дней';
                    }
                    $result['finish_ormco_star'] = date("d.m.Y", $finish_ormco_star);
                    $result['day_left'] = $day_left . ' ' . $day_form;
                }
                return $result;
            }
        }
        return;
    }

    private function get_random_password($length = 10) {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array();
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

    public function check_user_syncormcostars(){
//        $str = '{ "email": "aghigay@gmail.com", "Создавать": "1", "Фамилия": "Иванов", "Имя": "Иван", "Отчество": "Иванович", "Телефон" : "test", "Статус" : "Другая специализация", "Страна" : "Россия", "Регион" : "Санкт-Петербург и ЛО", "Город" : "test"}';
//        file_put_contents('./ormco_check_user_star.txt', date("Y-m-d H:i:s", time()) . ' - ' . $str . "\n", FILE_APPEND);
//        $input = (array) json_decode($str, true);

        file_put_contents('./ormco_check_user_star.txt', date("Y-m-d H:i:s", time()) . ' - ' . file_get_contents('php://input') . "\n", FILE_APPEND);
        $input = (array) json_decode(file_get_contents('php://input'), true);

        $uid = false;
        $result = array('uid' => null);
        if (isset($input['email'])) {
            $objectTypes = umiObjectTypesCollection::getInstance();
            $regedit = regedit::getInstance();

            $without_act = (bool) $regedit->getVal("//modules/users/without_act");
            $objectTypeId = $objectTypes->getTypeIdByHierarchyTypeName("users", "user");

            $email = isset($input['email']) ? $input['email'] : false;

            // Проверяем, что клиент уже есть
            $users = new selector('objects');
            $users->types('object-type')->name('users', 'user');
            $users->where('login')->equals($email);
            $users->limit(0, 1);
            foreach ($users as $user) {
                $uid = $user->id;
                //добавить commonUID
                $common_uid = $this->checkCommonUid($user);
                //$result = array('uid' => $uid);
                $result = array('uid' => $uid, 'commonUid' => $common_uid);
            }

            $is_create = isset($input['Создавать']) ? $input['Создавать'] == 1 : false;
            if($is_create && $uid === false) {
                // создаем клиента
                $password = self::get_random_password();

                $fname = isset($input['Имя']) ? $input['Имя'] : '';
                $lname = isset($input['Фамилия']) ? $input['Фамилия'] : '';
                $father_name = isset($input['Отчество']) ? $input['Отчество'] : '';

                $phone = isset($input['Телефон']) ? $input['Телефон'] : '';
                $prof_status = isset($input['Статус']) ? $input['Статус'] : '';
                $country = isset($input['Страна']) ? $input['Страна'] : '';
                $region = isset($input['Регион']) ? $input['Регион'] : '';
                $city = isset($input['Город']) ? $input['Город'] : '';

                $oEventPoint = new umiEventPoint("users_registrate");
                $oEventPoint->setMode("before");
                $oEventPoint->setParam("login", $email);
                $oEventPoint->addRef("password", $password);
                $oEventPoint->addRef("email", $email);
                users::setEventPoint($oEventPoint);

                $uid = umiObjectsCollection::getInstance()->addObject($email, $objectTypeId);
                $activationCode = md5($email . time());

                $object = umiObjectsCollection::getInstance()->getObject($uid);

                $object->setName($email);
                $object->setValue("login", $email);
                $object->setValue("password", md5($password));
                $object->setValue("e-mail", $email);

                $object->setValue("is_activated", $without_act);
                $object->setValue("activate_code", $activationCode);
                $object->setValue("referer", urldecode(getSession("http_referer")));
                $object->setValue("target", urldecode(getSession("http_target")));
                $date = new umiDate();
                $object->setValue("register_date", $date->getCurrentTimeStamp());
                $object->setOwnerId($uid);

                $group_id = $regedit->getVal("//modules/users/def_group");
                $object->setValue("groups", Array($group_id));

                $object->setValue('fname', $fname);
                $object->setValue('lname', $lname);
                $object->setValue('father_name', $father_name);
                $object->setValue('phone', $phone);
                $object->setValue('prof_status', $prof_status);
                $object->setValue('country', $country);
                $object->setValue('region', $region);
                $object->setValue('city', $city);

                $object->commit();



                //Forming mail...
                list($template_mail, $template_mail_subject, $template_mail_noactivation, $template_mail_subject_noactivation) = def_module::loadTemplatesForMail("users/register/default", "mail_registrated", "mail_registrated_subject", "mail_registrated_noactivation", "mail_registrated_subject_noactivation");

                if ($without_act && $template_mail_noactivation && $template_mail_subject_noactivation) {
                    $template_mail = $template_mail_noactivation;
                    $template_mail_subject = $template_mail_subject_noactivation;
                }

                $mailData = array(
                    'user_id' => $uid,
                    'domain' => $domain = cmsController::getInstance()->getCurrentDomain()->getCurrentHostName(),
                    'activate_link' => getSelectedServerProtocol() . "://" . $domain . $this->module->pre_lang . "/users/activate/" . $activationCode . "/",
                    'login' => $email,
                    'password' => $password,
                    'lname' => $object->getValue("lname"),
                    'fname' => $object->getValue("fname"),
                    'father_name' => $object->getValue("father_name"),
                );

                $mailContent = users::parseTemplateForMail($template_mail, $mailData, false, $uid);
                $mailSubject = users::parseTemplateForMail($template_mail_subject, $mailData, false, $uid);

                $fio = $object->getValue("lname") . " " . $object->getValue("fname") . " " . $object->getValue("father_name");

                $email_from = $regedit->getVal("//settings/email_from");
                $fio_from = $regedit->getVal("//settings/fio_from");

                $registrationMail = new umiMail();
                $registrationMail->addRecipient($email, $fio);
                $registrationMail->setFrom($email_from, $fio_from);
                $registrationMail->setSubject($mailSubject);
                $registrationMail->setContent($mailContent);
                $registrationMail->commit();
                $registrationMail->send();

                $oEventPoint = new umiEventPoint("users_registrate");
                $oEventPoint->setMode("after");
                $oEventPoint->setParam("user_id", $uid);
                $oEventPoint->setParam("login", $email);
                users::setEventPoint($oEventPoint);

                //добавить commonUID
                $common_uid = $this->checkCommonUid($object);
                //$result = array('uid' => $uid);
                $result = array('uid' => $uid, 'commonUid' => $common_uid);
            }
        }else{
            $result = array('status' => 'no email');
        }

        $buffer = outputBuffer::current('HTTPOutputBuffer');
        $buffer->charset('utf-8');
        $buffer->contentType('application/json');
        $buffer->push(json_encode($result));
        $buffer->end();

        return $result;
    }
}
