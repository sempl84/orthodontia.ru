<?php
class users_phone_validator extends def_module {

	const SMS_API_KEY = '5bgbbjggd8td1hfqwgbtt6kyn178ytdfjrk55esa';
	const M_USER_NOT_AUTH = 'Вы не авторизованы';
	const M_PHONE_NOT_VALIDATED = 'Телефон не подтвержден';

	/**
	 * Подтвержден ли мобильный телефон пользователя
	 * @return array
	 */
	public function is_phone_valid() {
		if($this->is_auth()){
			$umiPropertiesHelper = $this->umiPropertiesHelper;
			$userId = $this->user_id;
			$userTypeId = $this->umiTypesHelper->getObjectTypeIdByGuid('users-user');

			$phone_valid = $umiPropertiesHelper->getPropertyValue($userId, 'phone_valid', $userTypeId);
			if($phone_valid == 1){
				$result = array(
					'status'=> 'successful'
				);
			}else{
				$result = array(
					'result' => 0,
					'status' => 'error',
					'error'  => array(
						"message" => M_PHONE_NOT_VALIDATED
					)
				);
			}

		}else{
			$result = array(
				'result' => 0,
				'status' => 'error',
				'error'  => array(
					"message" => M_USER_NOT_AUTH
				)
			);
		}


		return $result;
	}

	/**
	 * Отправка кода пордтверждения на телефон пользователя
	 * @return array
	 */
	public function send_code($phone = NULL) {
		if(!$phone) $phone = $_REQUEST['phone'];

		$phone = $this->checkPhoneFormat($phone);
		$user = $this->getUserInfo();

		if($phone != 0 && $user){
			//save code and phone to user
			$code = $this->code_generate();

			$user->phone_valide_code=$code;
			$user->commit();

			//$sendResult = 1;//
			$sendResult = $this->sendSms($phone, $code);
			if($sendResult == 1){
				$result = array(
					'result'=> 1,
					'status'=> 'successful'
				);
			}else{
				$result = array(
					'result' => 0,
					'status' => 'error',
					'error'  => array(
						"message"=> $sendResult
					)
				);
			}
		}else{
			$result = array(
				'result' => 0,
				'status' => 'error',
				'error'  => array(
					"message"=> "Вы не авторизованы или указан неверный номер телефона"
				)
			);
		}

		return $result;
	}

	// Отправка кода пордтверждения на телефон пользователя
	public function apply_code($code=NULL) {
		if(!$code) $code = $_REQUEST['code'];
		if(!$code){
			return 	array(
						'result' => 0,
						'status' => 'error',
						'error'  => array(
							"message" => "Не указан проверочный код"
						)
					);
		}

		$user = $this->getUserInfo();

		if($user){
			if($user->phone_valide_code==$code){

				$user->phone_valide_code = '';
				$user->phone_valid=1;
				$user->commit();

				//изменить или создать аккаунт на другом сайте (ormco и orthodontia)
				$this->syncUserPhone($user);

				$result = array(
					'result'=> 1,
					'status'=> 'successful'
				);
			}else{
				$result = array(
					'result' => 0,
					'status' => 'error',
					'error'  => array(
						"message"=> "Неверный проверочный код"
					)
				);
			}
		}else{
			$result = array(
				'result' => 0,
				'status' => 'error',
				'error'  => array(
					"message"=> "Вы не авторизованы на сайте"
				)
			);
		}

		return $result;
	}

	// Отправка кода пордтверждения на телефон пользователя
	public function change_phone($phone = NULL) {
		if(!$phone) $phone = $_REQUEST['phone'];
		//return $phone;
		$phone = $this->checkPhoneFormat($phone);
		$user = $this->getUserInfo();

		if($phone != 0 && $user){
			$user = $this->getUserInfo();
			$user->phone=$phone;
			$user->phone_valid=0;
			$user->commit();
			$result = array(
				'result'=> 1,
				'status'=> 'successful'
			);

		}else{
			$result = array(
				'result' => 0,
				'status' => 'error',
				'error'  => array(
					"code" => 1,
					"message" => "Вы не авторизованы или указан неверный номер телефона"
				)
			);
		}

		return $result;
	}

	// Геннерация кода для смс
	public function code_generate() {
		$string = '0123456789';
    	$string_shuffled = str_shuffle($string);
    	$code = substr($string_shuffled, 1, 4);

		return $code;
	}

	// Проверка формата телефона
	public function checkPhoneFormat($phone = NULL) {
		// валидация телефона
		$phone = preg_replace('~[^0-9]~', '', $phone);// вырезаем все не цифры
		if (!preg_match("~\\d{7,14}~", $phone))
			return 0;
		// если ведущей семерки нет, то ставим ее
		if (strlen($phone) == 10) $phone = '7'.$phone;

		// если ведущей не является семерка (к примеру, 8), то заменяем ее на семерку
		if (strlen($phone) == 11 && $phone[0] == '8') $phone[0] = '7';

		if (!preg_match("~^7[89]\\d{9}$~", $phone))
			$phone = 0; // номер невалидный (обычный номер - длина должна быть 11, а вторая цифра - девятка или восьмерка)

		return $phone;
	}



	/**/
	public function getUserInfo() {
		if($this->is_auth()){
			$userId = $this->user_id;
			$user = $this->umiObjectsCollection->getObject($userId);
			if($user instanceof iUmiObject == false) return;
			return $user;
		}
		return ;
	}

	// запрос на синхронизацию
	public function syncUserPhone($user = NULL) {
		if(!$user) return;
		$phone = $user->phone;
		$email = $user->getValue('e-mail');
        $commonUid = $user->getValue('common_uid');
		$syncResult = json_decode(umiRemoteFileGetter::get('https://ormco.ru/udata/users/validateSync/' . $email.'/'.$phone.'/'.$commonUid.'/.json'));

		return $syncResult;
	}

	// синхронизация на текущем сайте при запросе
	public function validateSync($email = NULL , $phone = NULL, $commonUid = NULL) {
		if(!$email) $email = getRequest('param0');
		if(!$phone) $phone = getRequest('param1');
		if(!$commonUid) $phone = getRequest('param2');
		if(!$phone || !$email) return;

        if($commonUid){
            $sel = new selector('objects');
            $sel->types('object-type')->name('users', 'user');
            $sel->where('common_uid')->equals($commonUid);
            $sel->limit(0, 1);

            if($user = $sel->first){
                $user->phone = $phone;
                $user->phone_valide_code = '';
                $user->phone_valid=1;
                $user->commit();
                return 'successful';
            }
        }

        $sel = new selector('objects');
        $sel->types('object-type')->name('users', 'user');
        $sel->where('e-mail')->equals($email);
        $sel->limit(0, 1);

        if($user = $sel->first) {
            $user->phone = $phone;
            $user->phone_valide_code = '';
            $user->phone_valid=1;
            $user->commit();
            return 'successful';
        }


        return ;
	}


	public function sendSms($sms_to = NULL,$sms_text = NULL) {
		$sms_from = 'Ormco';

		if(!$sms_text || !$sms_to || !$sms_from){
			return "Wrong phone, or other params";
		}
		// Создаём POST-запрос
		$POST = array (
		  'api_key' => self::SMS_API_KEY,
		  'phone' => $sms_to,
		  'sender' => $sms_from,
		  'text' => $sms_text
		);

		// Устанавливаем соединение
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $POST);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_URL,
		            'https://api.unisender.com/ru/api/sendSms?format=json');
		$result = curl_exec($ch);

		if ($result) {
		  // Раскодируем ответ API-сервера
		  $jsonObj = json_decode($result);

		  if(null===$jsonObj) {
		    // Ошибка в полученном ответе
		    return "Invalid JSON";

		  }
		  elseif(!empty($jsonObj->error)) {
		    // Ошибка отправки сообщения
		    return "An error occured: " . $jsonObj->error . "(code: " . $jsonObj->code . ")";

		  } else {
		    // Сообщение успешно отправлено
		    return 1;
		    //echo "SMS message is sent. Message id " . $jsonObj->result->sms_id;
		    //echo "SMS cost is " . $jsonObj->result->price . " " . $jsonObj->result->currency;

		  }
		} else {
		  // Ошибка соединения с API-сервером
		  return "API access error";
		}
	}

	/*разделение телефона на части для 1с*/
	public function parsephone($phone_origin = NULL) {
		//return 'fff';
		if(!$phone_origin) $phone_origin = getRequest('param0');
		// валидация телефона
		$phone = $this->checkPhoneFormat($phone_origin);
		if($phone != 0){
			$sArea = substr($phone, 0,1);
		    $sPrefix = substr($phone,1,3);
		    $sNumber = substr($phone,4,7);
		    $sNumber_1 = substr($sNumber,0,3);
		    $sNumber_2 = substr($sNumber,3,2);
		    $sNumber_3 = substr($sNumber,5,2);
			return array(
				'src'=>'+'.$sArea.' ('.$sPrefix.') '.$sNumber_1.'-'.$sNumber_2.'-'.$sNumber_3,
				'area'=>$sArea,
				'prefix'=>$sPrefix,
				'number'=>$sNumber_1.'-'.$sNumber_2.'-'.$sNumber_3,
			);
		}

		return array(
			'src'=>$phone_origin
		);
	}

    /*разделение телефона (в том числе не подтвержденного) на части для 1с*/
    public function try_parse_phone($country = NULL,$phone_origin = NULL) {
        /*<object id="12115" name="Россия" type-id="184" update-time="1491318068" ownerId="59" xlink:href="uobject://12115"/>
        <object id="12116" name="Азербайджан" type-id="184" update-time="1517225085" ownerId="59" xlink:href="uobject://12116"/>
        <object id="12117" name="Армения" type-id="184" update-time="1517217523" ownerId="59" xlink:href="uobject://12117"/>
        <object id="12118" name="Белоруссия" type-id="184" update-time="1491318068" ownerId="59" xlink:href="uobject://12118"/>
        <object id="12119" name="Грузия" type-id="184" update-time="1517225256" ownerId="59" xlink:href="uobject://12119"/>
        <object id="12120" name="Украина" type-id="184" update-time="1517225270" ownerId="59" xlink:href="uobject://12120"/>
        <object id="12121" name="Казахстан" type-id="184" update-time="1491318069" ownerId="59" xlink:href="uobject://12121"/>
        <object id="12122" name="Другие" type-id="184" update-time="1517220839" ownerId="59" xlink:href="uobject://12122"/>
        <object id="1754228" name="Узбекистан" type-id="184" update-time="1595885258" ownerId="59" xlink:href="uobject://1754228"/>
        <object id="1754245" name="Киргизия" type-id="184" update-time="1595886123" ownerId="59" xlink:href="uobject://1754245"/>*/

        $error_msg = 'wrong_format';
        $phone_valid = $country_name = false;

        if(!$country) $country = getRequest('param0');
        if(!$phone_origin) $phone_origin = getRequest('param1');
        // валидация телефона
        $phone = preg_replace('~[^0-9]~', '', $phone_origin);// вырезаем все не цифры



        switch ($country){
            case 12115: //Россия - код оператора - 3 цифры, номер - 7 цифр | +7 (___) ___-____
                $country_name = 'Россия';
                if (strlen($phone) != 11) break;

                $sArea = substr($phone, 0,1);
                $sPrefix = substr($phone,1,3);
                $sNumber = substr($phone,4,7);
                $phone_valid = true;
                break;
            case 12116: //Азербайджан- код оператора - 2 цифры, номер - 7 цифр | +994 (__) ___-____
                $country_name = 'Азербайджан';
                if (strlen($phone) != 12) break;

                $sArea = substr($phone, 0,3);
                $sPrefix = substr($phone,3,2);
                $sNumber = substr($phone,5,7);
                $phone_valid = true;
                break;
            case 12117: //Армения - код оператора - 2 или 3 цифры, номер - 6 цифр | +374 (___) ___-____
                $country_name = 'Армения';
                if (strlen($phone) == 11){
                    $sArea = substr($phone, 0,3);
                    $sPrefix = substr($phone,3,2);
                    $sNumber = substr($phone,5,6);
                    $phone_valid = true;
                }
                if(strlen($phone) == 12){
                    $sArea = substr($phone, 0,3);
                    $sPrefix = substr($phone,3,3);
                    $sNumber = substr($phone,6,6);
                    $phone_valid = true;
                }

                break;
            case 12118: //Белоруссия - код оператора - 2 цифры, номер - 7 цифр | +375 (__) ___-____
                $country_name = 'Белоруссия';
                if (strlen($phone) != 12) break;

                $sArea = substr($phone, 0,3);
                $sPrefix = substr($phone,3,2);
                $sNumber = substr($phone,5,7);
                $phone_valid = true;
                break;
            case 12119: //Грузия - код оператора - 3 цифры, номер - 6 или 7 цифр | +995 (___) ___-____
                $country_name = 'Грузия';
                if (strlen($phone) == 12){
                    $sArea = substr($phone, 0,3);
                    $sPrefix = substr($phone,3,3);
                    $sNumber = substr($phone,5,6);
                    $phone_valid = true;
                }
                if (strlen($phone) == 13){
                    $sArea = substr($phone, 0,3);
                    $sPrefix = substr($phone,3,3);
                    $sNumber = substr($phone,5,7);
                    $phone_valid = true;
                }
                break;
            case 12120: //Украина - код оператора - 2 цифры, номер - 7 цифр | +380 (__) ___-____
                $country_name = 'Украина';
                if (strlen($phone) != 12) break;

                $sArea = substr($phone, 0,3);
                $sPrefix = substr($phone,3,2);
                $sNumber = substr($phone,5,7);
                $phone_valid = true;

                break;
            case 12121: //Казахстан - код оператора - 3 цифры, номер - 7 цифр | +7 (___) ___-____
                $country_name = 'Казахстан';
                if (strlen($phone) != 11) break;

                $sArea = substr($phone, 0,1);
                $sPrefix = substr($phone,1,3);
                $sNumber = substr($phone,4,7);
                $phone_valid = true;

                break;
            case 1754228: //Узбекистан - Код страны +998, 2 цифры кода оператора, до 7 цифр номера телефона | +998 (__) ___-____
                $country_name = 'Узбекистан';
                if (strlen($phone) != 12) break;

                $sArea = substr($phone, 0,3);
                $sPrefix = substr($phone,3,2);
                $sNumber = substr($phone,5,7);
                $phone_valid = true;

                break;
            case 1754245: //Киргизия - Код страны +996, 2 цифры кода оператора, до 7 цифр номера телефона | +996 (__) ___-____
                $country_name = 'Киргизия';
                if (strlen($phone) != 12) break;

                $sArea = substr($phone, 0,3);
                $sPrefix = substr($phone,3,2);
                $sNumber = substr($phone,5,7);
                $phone_valid = true;

                break;

        }

        if($phone_valid){
            $result =  array(
                'src'=>$phone_origin,
                'country'=>$country_name,
                'area'=>$sArea,
                'prefix'=>$sPrefix,
                'number'=>$sNumber,
            );
        }else{
            $result =  array(
                'src'=>$phone_origin,

            );
            if($country_name !== false){
                $result['country'] = $country_name;
            }
        }


        return $result;

    }


    /*разделение телефона (в том числе не подтвержденного) на части для 1с*/
    public function test_try_parse_phone() {
        $phones_arr = array(
            //Россия
            array('country' => 12115,
                'phone_origin' => '+79111234567'),

            //Азербайджан
            array('country' => 12116,
                'phone_origin' => '+994 (44) 123-4567'),

            //Армения
            array('country' => 12117,
                'phone_origin' => '+374 (98) 757020'),
            //Армения
            array('country' => 12117,
                'phone_origin' => '+374 (989) 757020'),

            //Белоруссия
            array('country' => 12118,
                'phone_origin' => '+375 (25)975-8183'),

            //Грузия
            array('country' => 12119,
                'phone_origin' => '+995 (568)538-558'),
            //Грузия
            array('country' => 12119,
                'phone_origin' => '+995 (568)538-5589'),
            //Украина
            array('country' => 12120,
                'phone_origin' => '+380 (95) 6613251'),
            //Казахстан
            array('country' => 12121,
                'phone_origin' => '+7 (701) 8876325'),
            //Узбекистан
            array('country' => 1754228,
                'phone_origin' => '+998 (90) 1990550'),
            //Киргизия
            array('country' => 1754245,
                'phone_origin' => '+996 (77) 6850850'),
        );

        foreach($phones_arr as $phone_item){
            $phone_origin = $phone_item['phone_origin'];
            $country = $phone_item['country'];
            $result = $this->try_parse_phone($country,$phone_origin);
            $lines[] = $result;
        }
            return array('nodes:item'=>$lines);
    }





}