<?php
class users_sync extends def_module {

	const url = 'https://ormco.ru/';
	const hash = '3gdtrfxcvfh';

	// Перебор пользователей
	public function checkAllRemoteUser() {
		return ;

		$sel = new selector('objects');
		$sel->types('object-type')->name('users', 'user');
		$sel->limit(4760, 10);

		$items_arr=array();
		foreach ($sel as $user) {
			$item=array();
			$newUser = '';

			$syncResult = $this->checkRemoteUser($user);
			$email = $user->getValue('e-mail');
			$password = $user->getValue('password');


			if($syncResult == 'userExist'){
				// sync data
			}
			if($syncResult == 'noUser'){
				// create user
				$newUser = (array)json_decode(umiRemoteFileGetter::get(self::url . 'udata/users/createRemoteUser/' . $email.'/'.$password.'/.json'));

			}

			$item['attribute:newUser'] = $newUser['result'];
			$item['attribute:email'] = $email;
			$item['node:text'] = $syncResult;

			$items_arr[] = $item;
		}
		//var_dump($items_arr);
		//exit('ggg');
		return array(
			'subnodes:item'=>$items_arr
		);
	}

	// Проверка наличия такого пользователя в другой базе
	public function checkRemoteUser($user = NULL) {
		if(!$user) return 'noUser';
		//$phone = $user->phone;
		$email = $user->getValue('e-mail');
		$syncResult = (array)json_decode(umiRemoteFileGetter::get(self::url . 'udata/users/isRemoteUserExist/' . $email.'/.json'));


		return $syncResult['result'];
	}

	/*принимающие макросы*/

	// есть ли такой пользователь
	public function isRemoteUserExist($email = NULL) {
		if(!$email) $email = getRequest('param0');
		if(!$email) return 'noEmail';

		$sel = new selector('objects');
		$sel->types('object-type')->name('users', 'user');
		$sel->where('e-mail')->equals($email);
		$sel->limit(0, 1);

		if($user = $sel->first) {
			return 'userExist';
		}

		return 'noUser';;
	}


	// есть ли такой пользователь
	public function createRemoteUser($email = NULL, $password = NULL) {
		//return ;

		if(!$email || !$password) return 'noData';
		$group_id = regedit::getInstance() -> getVal("//modules/users/def_group");
		$objectTypes = umiObjectTypesCollection::getInstance();
		$objectTypeId = $objectTypes -> getBaseType("users", "user");

		//Creating user...
		$object_id = umiObjectsCollection::getInstance() -> addObject($email, $objectTypeId);
		$object = umiObjectsCollection::getInstance() -> getObject($object_id);

		$object -> setValue("login", $email);
		$object -> setValue("password", $password);
		$object -> setValue("e-mail", $email);
		$object -> setValue("is_activated", true);
		$object -> setValue("register_date", time());
		$object -> setValue("groups", Array($group_id));
		$object -> setValue("sync_new_2", 1);

		$object -> commit();

		return $object_id;
	}

	/*автоматическая авторизация пользователя после авторизации на ormco.ru*/
	public function sa ($userEmail = NULL) {
		$permissions = permissionsCollection::getInstance();

		if(!$userEmail)$userEmail = getRequest('param0');

		$sel = new selector('objects');
		$sel->types('object-type')->name('users', 'user');
		$sel->where('e-mail')->equals($userEmail);
		$sel->limit(0, 1);

		if($user = $sel->first) {
			$userId = $user->id;
			$permissions -> loginAsUser($userId);
			//$this->redirect('/');
			//return 1;
		}
		$this->redirect('/');
		//return 0;

	}


	/*сброс полей для гостя*/
	public function guest_restore () {
		return 'switch off';
		$object = umiObjectsCollection::getInstance() -> getObject(335);

		$object -> lname=false;
		/*$object -> setValue("father_name", '');
		$object -> setValue("country", '');
		$object -> setValue("password", '');
		$object -> setValue("phone", '');
		$object -> setValue("region", '');
		$object -> setValue("city", '');
		$object -> setValue("agree", false);*/

		//$object -> commit();

	}


	/*проверяем есть ли такой пользователь на другом сайте и создаем его если надо*/
	public function syncRemoteUser (umiEventPoint $event) {
  	if ($event->getMode() == "after") {
     	$user_id  = $event->getParam('user_id');
     	$objects = umiObjectsCollection::getInstance();
     	$user = $objects->getObject($user_id);
     	$email = $user->getValue('e-mail');

		 	$syncResult = $this->checkRemoteUser($user);
			$email = $user->getValue('e-mail');
			$password = $user->getValue('password');


			if($syncResult == 'userExist'){
				// sync data
			}
			if($syncResult == 'noUser'){
				// create user
				$newUser = (array)json_decode(umiRemoteFileGetter::get(self::url . 'udata/users/createRemoteUser/' . $email.'/'.$password.'/.json'));

			}
  	}
  	}

	/*TEST отправка запроса на создание пользователя*/
	public function testSyncCreateUser () {
		$user_id  = 626668;
   	$objects = umiObjectsCollection::getInstance();
   	$user = $objects->getObject($user_id);
   	$email = $user->getValue('e-mail');

	 	return $this->syncCreateUser($user);
	}

	/*отправка запроса на создание пользователя*/
	public function syncCreateUser ($user = NULL) {

		if(!$user) return 'noUser';
		$url = self::url . 'udata/users/createForeignUser/';

		$fieldsStringToSend = array(
			'lname',
			'fname',
			'father_name',
			'e-mail',
			'phone',
			'phone_office',
			'city',
			'bd',
			'company',
			'agree',
		);
		$fieldsRelativeToSend = array(
			'country',
			'region',
			'prof_status',
		);

		$post = array(
			'ps' => $user->getValue('password'),
			'referer' => 'https://orthodontia.ru/autoreg'
		);
		foreach($fieldsStringToSend as $fieldName){
			$fieldValue = $user->getValue($fieldName);
			if($fieldValue){
				$post[$fieldName] = $fieldValue;
			}

		}

		foreach($fieldsRelativeToSend as $fieldName){
			$fieldValueId = $user->getValue($fieldName);
			$fieldValueObj = umiObjectsCollection::getInstance() -> getObject($fieldValueId);
			if($fieldValueObj){
				$fieldValue = $fieldValueObj->name;
				$post[$fieldName] = $fieldValue;
			}
		}



		$hash = hash('sha256', implode('|',$post) . self::hash);
		$post['hash'] = $hash;


		//var_dump(serialize($post));
		//var_dump($post);
		//exit('qq');

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

		// execute!
		$response = curl_exec($ch);

		curl_close ($ch);
		print_r($response);
		exit('gg');
		// Further processing ...
		//if ($response == "OK") {
		//
		//} else {
		//
		//}
  }
}