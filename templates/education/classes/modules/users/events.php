<?php
new umiEventListener("users_activate", "users", "syncNewUser");
new umiEventListener("users_settings_do", "users", "syncModifyUser");
//new umiEventListener("users_registrate ", "users", "syncNewUser");
//new umiEventListener("users_login_successfull", "users", "syncAuth");

// Логирование изменений в пользователе из административной зоны
new umiEventListener("systemModifyPropertyValue", "data", "update_fast_property_user_save_to_log");
new umiEventListener("systemModifyObject", "data", "update_user_save_to_log");

// Логирование изменений в пользователе при выгрузке из 1С
new umiEventListener("exchangeOnUpdateObject", "data", "update_user_save_to_log");

// Логирование изменений в пользователе при обновлении в личном кабинете
new umiEventListener("users_settings_do", "data", "update_user_from_personal_save_to_log");

// Логирование изменений в пользователе при регистрации на мероприятие
new umiEventListener("users_register_on_event_do", "data", "update_user_from_event_registration_save_to_log");

// Логирование изменений в пользователе при регистрации на мероприятие с лендинга
new umiEventListener("users_register_on_event_from_landing_do", "data", "update_user_from_event_registration_from_landing_save_to_log");

// Логирование изменений в пользователе при обновлении в личном кабинете
new umiEventListener("users_restore_password_do", "data", "update_user_restore_password_save_to_log");

// синхронизация пользователей при работе в админке
new umiEventListener("systemCreateObject", "users", "syncAdminModifyUser");
new umiEventListener("systemModifyObject", "users", "syncAdminModifyUser");
new umiEventListener("systemModifyPropertyValue", "users", "syncAdminFastModifyUser");