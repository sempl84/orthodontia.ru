<?php
class users_sync_export extends def_module {

	const url = 'https://ormco.ru/';
	//const url = 'http://ormco-test2.dpromo.su/';
	const hash = '3gdtrfxcvfh';
	const referer = 'https://orthodontia.ru/autoreg';
	const referermodify = 'https://orthodontia.ru/automodify';

    // Перебор пользователей
    public function checkAllRemoteUser() {
        return ;

        $sel = new selector('objects');
        $sel->types('object-type')->name('users', 'user');
        //$sel->where('id')->equals(array(27457));
        //$sel->limit(3, 100);
        //$sel->limit(100, 1000);
        //$sel->limit(1000, 2000);
        //$sel->limit(2000, 3000);
        $per_page = 2000;
        $p=5;
        $offer = $per_page * $p ;
        $sel->limit($offer, $per_page);


        //$sel->where('common_uid')->isnotnull(true);
        //$sel->limit(0, 10);
        //var_dump($sel->length);
        //exit('hhh');
        var_dump('page: '. $p);
        var_dump('offer: '. $offer);
        var_dump('per_page: '. $per_page);
        $items_arr=array();
        foreach ($sel as $user) {
            var_dump($user->id);
            var_dump($user->getValue('e-mail'));
            // TODO return new UID
            //$result = $this->exportNewUser($user);

            //$user->common_uid = '';
            //$commonUid = $this->checkCommonUid($user);
            //var_dump($commonUid);

            $result = $this->exportModifyUser($user);
            var_dump(print_r($response, true));

        }

        exit('ggg');

    }

    // Добавить всем пользователям commonUid и синхронизировать их со вторым сайтом
    public function addCommonUidForLeftUsers() {
        return ;

        $sel = new selector('objects');
        $sel->types('object-type')->name('users', 'user');
        $sel->where('common_uid')->isnull(true);
        $sel->where('is_activated')->equals(1);
        //$sel->limit(3, 100);
        //$sel->limit(100, 1000);
        //$sel->limit(1000, 2000);
        //$sel->limit(2000, 3000);
        $per_page = 2000;
        $p=0;
        $offer = $per_page * $p ;
        $sel->limit($offer, $per_page);
        $total = $sel->total;


        //$sel->where('common_uid')->isnotnull(true);
        //$sel->limit(0, 10);
        //var_dump($sel->length);
        //exit('hhh');
        var_dump('page: '. $p);
        var_dump('offer: '. $offer);
        var_dump('per_page: '. $per_page);
        $items_arr=array();
        foreach ($sel as $user) {
            //var_dump($user->id);
            echo $user->id.';"' . $user->getValue('e-mail') . '";' . "\n";
            // TODO return new UID
            //$result = $this->exportNewUser($user);

            //$user->common_uid = '';
            //$commonUid = $this->checkCommonUid($user);
            //var_dump($commonUid);

            $result = $this->exportModifyUser($user);
            //var_dump(print_r($response, true));

        }
        var_dump($total);
        exit('ggg');

    }

    // сбросить галочку "пользователей синхронизован с 1С"
    public function clearCommonUid_1cSync() {
        return ;

        $sel = new selector('objects');
        $sel->types('object-type')->name('users', 'user');
        $sel->where('common_uid_1c_sync')->equals(1);

        //$sel->limit(3, 100);
        //$sel->limit(100, 1000);
        //$sel->limit(1000, 2000);
        //$sel->limit(2000, 3000);
        $per_page = 7000;
        $p=0;
        $offer = $per_page * $p ;
        $sel->limit($offer, $per_page);
        $total = $sel->total;


        //$sel->where('common_uid')->isnotnull(true);
        //$sel->limit(0, 10);
        //var_dump($sel->length);
        //exit('hhh');
        var_dump('page: '. $p);
        var_dump('offer: '. $offer);
        var_dump('per_page: '. $per_page);
        $items_arr=array();
        foreach ($sel as $user) {
            //var_dump($user->id);
            echo $user->id.';"' . $user->getValue('e-mail') . '";' . "\n";

//            $user->setValue(common_uid_1c_sync,0);
//            $user->commit();
        }
        var_dump($total);
        exit('ggg');

    }


	/*пытаемся создать пользователя при регистрации*/
	public function syncNewUser (umiEventPoint $event) {
        if ($event->getMode() == "after") {
            $user_id  = $event->getParam('user_id');
            //if($user_id == 1022553){ // ortho reg@umihelp.ru id
                $objects = umiObjectsCollection::getInstance();
                $user = $objects->getObject($user_id);

                // TODO return new UID
                $this->exportNewUser($user);
            //}
        }
  	}

    /*пытаемся обновить или создать пользователя при изменении настроек*/
    public function syncModifyUser (umiEventPoint $event) {
        if ($event->getMode() == "after") {
            $user_id  = $event->getParam('user_id');
            //if($user_id == 1022553){ // ortho reg@umihelp.ru id
                $objects = umiObjectsCollection::getInstance();
                $user = $objects->getObject($user_id);

                // TODO return new UID
                $this->exportModifyUser($user);
            //}
        }
    }

    // синхронизация пользователя при добавлении или изменений пользователя в административной зоне
    public function syncAdminModifyUser(iUmiEventPoint $event) {
        if ($event->getMode() == 'after') {
            $user = $event->getRef('object');
            $objectTypeId = $user->getTypeId();
            $user_type_id = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName('users', 'user');
            if ($objectTypeId !== $user_type_id) { // не пользователь
                return true;
            }
            $this->exportModifyUser($user);
        }
        return true;
    }

    // синхронизация пользователя при изменении в административной зоне на списке пользователей
    public function syncAdminFastModifyUser(iUmiEventPoint $event) {
        if ($event->getMode() == 'after') {
            $user = $event->getRef('entity');
            //$property_name = $event->getParam('property');

            $objectTypeId = $user->getTypeId();
            $user_type_id = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName('users', 'user');
            if ($objectTypeId !== $user_type_id) { // не пользователь
                return true;
            }
            $this->exportModifyUser($user);
        }
        return true;
    }

	/*отправка запроса на создание пользователя*/
	public function exportNewUser ($user = NULL) {
        file_put_contents(CURRENT_WORKING_DIR . '/us_log.txt', "\n\n exportNewUser - start " . $user->id . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);

        if(!$user) return 'noUser';
		$url = self::url . 'udata/users/importNewUser/';

		$fieldsStringToSend = array(
			'lname',
			'fname',
			'father_name',
			'e-mail',
			'phone',
            'phone_valid',
            'phone_valide_code',
			'phone_office',
			'city',
			'bd',
			'company',
			'agree',
		);
		$fieldsRelativeToSend = array(
			'country',
			'region',
			'prof_status',
		);


		$post = array(
			'ps' => $user->getValue('password'),
			'active' => $user->getValue('is_activated'),
			'common_uid' => $this->checkCommonUid($user),
			'referer' => self::referer
		);
		foreach($fieldsStringToSend as $fieldName){
			$fieldValue = $user->getValue($fieldName);
			if($fieldValue){
				$post[$fieldName] = $fieldValue;
			}

		}

		foreach($fieldsRelativeToSend as $fieldName){
			$fieldValueId = $user->getValue($fieldName);
			$fieldValueObj = umiObjectsCollection::getInstance() -> getObject($fieldValueId);
			if($fieldValueObj){
				$fieldValue = $fieldValueObj->name;
				$post[$fieldName] = $fieldValue;
			}
		}

		$hash = hash('sha256', implode('|',$post) . self::hash);
		$post['hash'] = $hash;

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

		// execute!
		$response = curl_exec($ch);

		curl_close ($ch);

        file_put_contents(CURRENT_WORKING_DIR . '/us_log.txt', "\n\n exportNewUser finish " . $user->id . "\n"  . print_r($response, true) . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);
        return $response;
	}

    /*отправка запроса на обновление информации о пользователе*/
	public function exportModifyUser ($user = NULL) {
        file_put_contents(CURRENT_WORKING_DIR . '/us_log.txt', "\n\n exportModifyUser - start " . $user->id . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);

        if(!$user) return 'noUser';
		$url = self::url . 'udata/users/importModifyUser/';

		$fieldsStringToSend = array(
			'lname',
			'fname',
			'father_name',
			'e-mail',
			'phone',
			'phone_valid',
			'phone_valide_code',
			'phone_office',
			'city',
			'bd',
			'company',
			'agree',
            SiteUsersUserModel::field_address_postal_code,
            SiteUsersUserModel::field_address_country,
            SiteUsersUserModel::field_address_country_iso,
            SiteUsersUserModel::field_address_region,
            SiteUsersUserModel::field_address_area,
            SiteUsersUserModel::field_address_city,
            SiteUsersUserModel::field_address_settlement,
            SiteUsersUserModel::field_address_street,
            SiteUsersUserModel::field_address_house,
            SiteUsersUserModel::field_address_corpus,
            SiteUsersUserModel::field_address_building,
            SiteUsersUserModel::field_address_liter,
            SiteUsersUserModel::field_address_room,
            SiteUsersUserModel::field_address_flat,
            SiteUsersUserModel::field_address_office,
            SiteUsersUserModel::field_address_raw,
		);
		$fieldsRelativeToSend = array(
			'country',
			'region',
			'prof_status',
		);

		$post = array(
			'ps' => $user->getValue('password'),
            'active' => $user->getValue('is_activated'),
            'common_uid' => $this->checkCommonUid($user),
            'referer' => self::referermodify
		);
		foreach($fieldsStringToSend as $fieldName){
			$fieldValue = $user->getValue($fieldName);
			if($fieldValue){
				$post[$fieldName] = $fieldValue;
			}

		}

		foreach($fieldsRelativeToSend as $fieldName){
			$fieldValueId = $user->getValue($fieldName);
			$fieldValueObj = umiObjectsCollection::getInstance() -> getObject($fieldValueId);
			if($fieldValueObj){
				$fieldValue = $fieldValueObj->name;
				$post[$fieldName] = $fieldValue;
			}
		}

		$hash = hash('sha256', implode('|',$post) . self::hash);
		$post['hash'] = $hash;

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

		// execute!
		$response = curl_exec($ch);

		curl_close ($ch);

        file_put_contents(CURRENT_WORKING_DIR . '/us_log.txt', "\n\n exportModifyUser finish " . $user->id . "\n"  . print_r($response, true) . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);
    }

    /*проверка наличия общего uid  и назначения его как самый старший UID + 1*/
	public function checkCommonUid ($user = NULL) {
        if(!$user) return ;
        $commonUid = $user->common_uid;
        //var_dump('1-'.$commonUid);
        if(!($commonUid > 0)){
            //create $commonUid
            //var_dump('2-create');
            $sel = new selector('objects');
            $sel->types('object-type')->name('users', 'user');
            $sel->where('common_uid')->isnull(false);
            $sel->order('common_uid')->desc();
            $sel->limit(0,1);
            $total = $sel->length;

                //var_dump('21-total' . $total);
            if($userTmp = $sel->first){
                // берем самый последний common uid +1
                $commonUid = (int)($userTmp->common_uid) + 1;
                //var_dump('31-id-' . $userTmp->id);
                //var_dump('31-uid' . $userTmp->common_uid);
                //var_dump('31-'.$commonUid);
            }else{
                // новый отчет common uid
                $commonUid = 1;
                //var_dump('32-'.$commonUid);
            }
            $user->common_uid = $commonUid;
            $user->commit();
        }
        return $commonUid;
	}

    /*отправка запроса на синхронизацию отметки о существовании такого пользователя и commonUid в 1С*/
    public function exportCommonUid1cSync ($user = NULL) {
        file_put_contents(CURRENT_WORKING_DIR . '/us_log.txt', "\n\n exportCommonUid1cSync - start " . $user->id . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);

        if(!$user) return 'noUser';
        $url = self::url . 'udata/users/importCommonUid1cSync/';


        $post = array(
            'common_uid' => $user->getValue('common_uid')
        );

        //$hash = hash('sha256', implode('|',$post) . self::hash);
        //$post['hash'] = $hash;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        // execute!
        $response = curl_exec($ch);

        curl_close ($ch);

        file_put_contents(CURRENT_WORKING_DIR . '/us_log.txt', "\n\n exportCommonUid1cSync finish " . $user->id . "\n"  . print_r($response, true) . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);
    }

	/**
	 * Список пользователей с данными oldId, email, commonUID для синхронизации с 1С по контактным лицам (КЛ)
	 * @param type $offset
	 * @param type $limit
	 * @return type
	 */
    public function outputUsersCommonUID($offset = null, $limit = null) {
        $objects = umiObjectsCollection::getInstance();

		if ($offset == null){
			$offset = getRequest('param0');
		}
		if (empty($offset)){
			$offset = 0;
		}

		if ($limit == null){
			$limit = getRequest('param1');
		}
		if (empty($limit)){
			$limit = 50000;
		}

		// 12135 - Ортодонт, 12136 - Хирург, 1773599 - Студент, 12137 - Другая специализация
		$allowed_prof_statuses = array(12135, 12136, 1773599, 12137);

		$sel = new selector('objects');
		$sel->types('object-type')->name('users', 'user');
		$sel->where('common_uid')->isnull(false);
		$sel->where('common_uid_1c_sync')->notequals(1);
		$sel->where('prof_status')->equals($allowed_prof_statuses);
		$sel->order('id')->asc(true);
		$sel->limit($offset, $limit);

		$line_arr = array();
		foreach ($sel as $user) {
			$line = array(
				'oldID' => $user->getId(),
				'email' => $user->getValue('e-mail'),
				'commonUID' => $user->getValue('common_uid'),
				'status' => '',
				'dolgnost' => '',
				'vuz_id' => '',
				'vuz' => '',
				'lname' => $user->getValue('lname'),
				'fname' => $user->getValue('fname'),
				'father_name' => $user->getValue('father_name'),
				'city' => $user->getValue('city'),
				'phone' => $user->getValue('phone'),
				'region_id' => '',
				'region' => '',
			);

			$prof_status = $objects->getObject($user->getValue('prof_status'));
			if ($prof_status instanceof umiObject) {
				$line['status'] = $line['dolgnost'] = $prof_status->getName();

				// Cтудент
				if ($prof_status->getId() == 1773599) {
					$line['status'] = 'Не врач';

					$line['vuz_id'] = $vuz_id = $user->getValue('vuz');
					$vuz = $objects->getObject($vuz_id);
					if ($vuz instanceof umiObject) {
						$line['vuz'] = $vuz->getName();
					}
				}

				// Другая специализация
				if ($prof_status->getId() == 12137) {
					$line['dolgnost'] = $user->getValue('other_specialization');
				}
			}

			$region = $objects->getObject($user->getValue('region'));
			if ($region instanceof umiObject) {
				$line['region_id'] = $region->getId();
				$line['region'] = $region->getName();
			}
            
            if($raw = trim($user->getValue(SiteUsersUserModel::field_address_raw))) {
                $address = array(
                    'postal_code' => $user->getValue(SiteUsersUserModel::field_address_postal_code),
                    'country' => $user->getValue(SiteUsersUserModel::field_address_country),
                    'country_iso' => $user->getValue(SiteUsersUserModel::field_address_country_iso),
                    'region' => $user->getValue(SiteUsersUserModel::field_address_region),
                    'area' => $user->getValue(SiteUsersUserModel::field_address_area),
                    'city' => $user->getValue(SiteUsersUserModel::field_address_city),
                    'settlement' => $user->getValue(SiteUsersUserModel::field_address_settlement),
                    'street' => $user->getValue(SiteUsersUserModel::field_address_street),
                    'house' => $user->getValue(SiteUsersUserModel::field_address_house),
                    'corpus' => $user->getValue(SiteUsersUserModel::field_address_corpus),
                    'building' => $user->getValue(SiteUsersUserModel::field_address_building),
                    'liter' => $user->getValue(SiteUsersUserModel::field_address_liter),
                    'room' => $user->getValue(SiteUsersUserModel::field_address_room),
                    'flat' => $user->getValue(SiteUsersUserModel::field_address_flat),
                    'office' => $user->getValue(SiteUsersUserModel::field_address_office),
                    'raw' => $raw,
                );
                
                $line['address'] = $address;
            }

			$line_arr[] = $line;
		}

		return array('nodes:user' => $line_arr);
	}
}
