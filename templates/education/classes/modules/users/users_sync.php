<?php
class users_sync extends def_module {


    // вывод общих цифр по синхронизированным пользователям
    public function commonUidCount(){
        $sel = new selector('objects');
        $sel->types('object-type')->name('users', 'user');
        $sel->where('common_uid')->isnull(false);
        $sel->order('common_uid')->desc();
        $sel->limit(0,1);
        $total = $sel->length;

        $commonUid = false;
        if($userTmp = $sel->first){
            $commonUid = $userTmp->common_uid;
        }

        $sel_all = new selector('objects');
        $sel_all->types('object-type')->name('users', 'user');
        $sel_all->limit(0,1);
        $total_all = $sel_all->length;

        $sel_noID = new selector('objects');
        $sel_noID->types('object-type')->name('users', 'user');
        $sel_noID->where('common_uid')->isnull(true);
        $sel_noID->limit(0,1);
        $total_noID = $sel_noID->length;

        return array(
            "lastCommonId"=>$commonUid,
            "totalWithCommonId"=>$total,
            "total_noID"=>$total_noID,
            "total"=>$total_all,
        );

    }
}
