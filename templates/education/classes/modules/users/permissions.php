<?php
$permissions = Array(
    'login' => array(
        'login_do_pre', 'sa', 'checkreg', 'checkauth',
        'validateSync', 'parsephone',
        'importNewUser','importModifyUser', 'importCommonUid1cSync',
        'getCommonUIDbyOldID','getCommonUIDbyEmail',
        'outputUsersCommonUID',
        'isRemoteUserExist', 'createRemoteUser',
        'syncormcostars', 'is_loyalty_change', 'del_loyalty_notify', 'loyalty_info',
        'check_user_syncormcostars',
        'update_password', 'set_new_password',
        'is_empty_field',
        'get_page_name_for_not_permited_page',
		'change_email_from_letter',
        'try_parse_phone','test_try_parse_phone'
    ),
    'registrate' => Array('registrate_do_pre', 'lpreg'),
    'settings' => Array(
		'settings_do_pre',
		'is_phone_valid',
		'send_code',
		'apply_code',
		'change_phone',
		'loyalty',
		'cancel_change_email',
		'send_confirm_email_letter',
	),
);
