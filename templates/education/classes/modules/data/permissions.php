<?php
$permissions = Array(
    'main' => Array(
        'dateru',
        'dateruPeriod',
        'currentTimestamp',
        'tmsDatepicker',
        'systemMeta',
        'dateFilter',
        'dateFilterSmart',
        'save_users_change_log',
        'get_users_change_log',
        'get_all_vuz_by_region',
        'push_notification_in_3_days',
        'push_notification_in_1_day',
    ),
	'trash' => array(
		'clear_cache'
	),
);
