<?php
class data_custom extends def_module {
    public function __construct($self) {
        $self->__loadLib("/__search.php", (dirname(__FILE__)));
        $self->__implement("data_custom_search");
    }

    public function getMonthRu($month, $year, $type = 0) {
        // Проверка существования месяца
        if (!checkdate($month, 1, $year)) {
            throw new publicException("Проверьте порядок ввода даты.");
        }
        $months_ru = Array(1 => 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
        $months_ru1 = Array(1 => 'январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь');
        if ($type == 1) {
            return $months_ru1[$month];
        }
        return $months_ru[$month];
    }

    public function dateru($time) {
        if (!$time){
            return;
        }
        $day = date('j', $time);
        $month = date('n', $time);
        $year = date('Y', $time);
        $hour = date('g', $time);
        $minutes = date('i', $time);
        $seconds = date('s', $time);

        $temp = Array(
            'month' => $this->getMonthRu($month, $year),
            'day' => $day,
            'year' => $year,
            'hour' => $hour,
            'minutes' => $minutes,
            'seconds' => $seconds
        );
        return def_module::parseTemplate('', $temp);
    }

    // test data:
    // 1352204856 Tue, 06 Nov 2012 12:27:36 GMT
    // 1352377656 Thu, 08 Nov 2012 12:27:36 GMT
    // 1355660856 Sun, 16 Dec 2012 12:27:36 GMT
    // 1357475256 Sun, 06 Jan 2013 12:27:36 GMT
    // 1383740856 Wed, 06 Nov 2013 12:27:36 GMT
    public function dateruPeriod($timeBegin = NULL, $timeEnd = NULL) {
        if (!$timeBegin || !$timeEnd){
            return "-";
        }
        if (!is_numeric($timeBegin) || !is_numeric($timeEnd)){
            return "=";
        }
        if ($timeEnd < $timeBegin) {
            $tmp = $timeEnd;
            $timeEnd = $timeBegin;
            $timeBegin = $tmp;
        }

        $dayBegin = date('d', $timeBegin);
        $monthBegin = date('n', $timeBegin);
        $yearBegin = date('Y', $timeBegin);
        $dayEnd = date('d', $timeEnd);
        $monthEnd = date('n', $timeEnd);
        $yearEnd = date('Y', $timeEnd);

        $timeEnd_forCalc = strtotime("$dayEnd-$monthEnd-$yearEnd ");
        $timeBegin_forCalc = strtotime("$dayBegin-$monthBegin-$yearBegin ");

        $yearCurrent = date('Y');

        if (($monthBegin != $monthEnd) || ($yearBegin != $yearEnd)) {
            $firstLine = $dayBegin . ' ' . $this->getMonthRu($monthBegin, $yearBegin) . ' ';
            $secondLine = $dayEnd . ' ' . $this->getMonthRu($monthEnd, $yearEnd);
            /* if($yearBegin != $yearEnd) {
              $secondLine .= ' '.$yearEnd.' г.';
              } */
            $firstLine .= '-';
        } else {
            $firstLine = ($dayBegin != $dayEnd) ? $dayBegin . '-' . $dayEnd : $dayBegin;
            $secondLine = $this->getMonthRu($monthBegin, $yearBegin);
        }

        //if(($yearEnd != $yearCurrent) && ($yearBegin == $yearEnd)) {
        $secondLine .= ' ' . $yearEnd . ' г.';
        //}
        $days = (int) (($timeEnd_forCalc - $timeBegin_forCalc) / 86400) + 1; // секунд в 24 часах
        $temp = Array(
            'first_line' => $firstLine,
            'second_line' => $secondLine,
            'days' => $days . ' ' . $this->wordDays($days)
        );
        return def_module::parseTemplate('', $temp);
    }

    public function wordDays($days) {
        $units = $days - (int) ($days / 10) * 10;
        $tens = $days - (int) ($days / 100) * 100 - $units;
        if ($tens == 10){
            return 'дней'; // 11-19 дней
        }

        if ($units == 1){
            return 'день'; // 1 21 31... день
        }
        if (($units > 1) && ($units < 5)){
            return 'дня'; // 2 3 4 22 23 24 дня
        }
        return 'дней'; // 5 6 7 8 9 20 дней
    }

    public function currentTimestamp() {
        $temp = Array(
            'timestamp' => time()
        );
        return def_module::parseTemplate('', $temp);
    }

    public function tmsDatepicker($timestp = NULL) {
        if (!$timestp){
            return '';
        }
        $day = date('d', $timestp);
        $month = date('n', $timestp);
        $year = date('Y', $timestp);
        $month = $this->getMonthRu($month, $year);
        return "$day $month, $year";
    }

    public function systemMeta() {
        $cmsController = cmsController::getInstance();
        //175
        // определяем текущий модуль метод
        $module = $cmsController->getCurrentModule();
        $method = $cmsController->getCurrentMethod();

        $metas = new selector('objects');
        $metas->types('object-type')->id(175); // id справочника "МЕТА ТЕГИ ДЛЯ СИСТЕМНЫХ СТРАНИЦ"
        $metas->where('module')->equals($module);
        $metas->where('method')->equals($method);
        $metas->limit(0, 1);

        foreach ($metas as $meta) {
            $item = array();
            $item['attribute:title'] = $meta->title;
            $item['attribute:description'] = $meta->description;
            $item['attribute:keywords'] = $meta->keywords;
            return $item;
        }

        return;
    }

    public function dateFilter($time) {
        // взять время
        // сформировать начало и окончание дня
        $dayBegin = date('d', $time);
        $monthBegin = date('n', $time);
        $yearBegin = date('Y', $time);
        $dayEnd = date('d', $time);
        $monthEnd = date('n', $time);
        $yearEnd = date('Y', $time);

        $timeEnd_forCalc = strtotime("$dayEnd-$monthEnd-$yearEnd 23:59:59");
        $timeBegin_forCalc = strtotime("$dayBegin-$monthBegin-$yearBegin 00:00:00");
        // проверить ссылки в адресной строке

        $params = $_REQUEST;
        unset($params['fields_filter']['publish_time'][1]);
        unset($params['fields_filter']['publish_time'][0]);
        unset($params['path']);
        unset($params['umi_authorization']);
        unset($params['scheme']);

        $params['fields_filter']['publish_time'][1] = $timeEnd_forCalc;
        $params['fields_filter']['publish_time'][0] = $timeBegin_forCalc;

        $params = self::protectParams($params);

        $q = (sizeof($params)) ? http_build_query($params, '', '&') : "";

        $q = urldecode($q);
        $q = str_replace(array("%", "<", ">", "%3C", "%3E"), array("&#037;", "&lt;", "&gt;", "&lt;", "&gt;"), $q);

        return "?" . $q;
    }

    public function dateFilterSmart($time) {
        // взять время
        // сформировать начало и окончание дня
        $dayBegin = date('d', $time);
        $monthBegin = date('n', $time);
        $yearBegin = date('Y', $time);
        $dayEnd = date('d', $time);
        $monthEnd = date('n', $time);
        $yearEnd = date('Y', $time);

        $timeEnd_forCalc = strtotime("$dayEnd-$monthEnd-$yearEnd 23:59:59");
        $timeBegin_forCalc = strtotime("$dayBegin-$monthBegin-$yearBegin 00:00:00");
        // проверить ссылки в адресной строке

        $params = array();
        $params['filter']['publish_date']['to'] = $timeEnd_forCalc;
        $params['filter']['finish_date']['from'] = $timeBegin_forCalc;

        $params = self::protectParams($params);

        $q = (sizeof($params)) ? http_build_query($params, '', '&') : "";

        $q = urldecode($q);
        $q = str_replace(array("%", "<", ">", "%3C", "%3E"), array("&#037;", "&lt;", "&gt;", "&lt;", "&gt;"), $q);

        return "?" . $q;
    }

    protected static function protectParams($params) {
        foreach ($params as $i => $v) {
            if (is_array($v)) {
                $params[$i] = self::protectParams($v);
            } else {
                $v = htmlspecialchars($v);
                $params[$i] = str_replace(array("%", "<", ">", "%3C", "%3E"), array("&#037;", "&lt;", "&gt;", "&lt;", "&gt;"), $v);
            }
        }
        return $params;
    }

    /**
     * Сохранение факта изменения данных пользователя
     * @param type $obj_id - идентификатор объекта пользователя
     * @param type $author - имя автора изменений, строка
     * @param type $details - текстовое описание события
     */
    public function save_users_change_log($obj_id, $author, $message, $details) {
        $author = mysql_real_escape_string($author);
        $message = mysql_real_escape_string($message);
        $details = serialize($details);
        $connection = ConnectionPool::getInstance()->getConnection();
        $connection->query("INSERT INTO `users_change_log`(`obj_id`, `author`, `message`, `details`) VALUES ({$obj_id}, '{$author}', '{$message}', '{$details}')");
    }

    /**
     * Получение лога изменений данных пользователя по его идентификатору
     * @param type $obj_id - идентификатор объекта пользователя
     * @return type
     */
    public function get_users_change_log() {
        $obj_id = getRequest('obj_id');
        $res = array();
        $connection = ConnectionPool::getInstance()->getConnection();
        $result = $connection->queryResult("SELECT * FROM `users_change_log` WHERE `obj_id` = '{$obj_id}' ORDER BY `created` DESC ");
        $result->setFetchType(IQueryResult::FETCH_ASSOC);
        foreach ($result as $row) {
            $res[] = $row;
        }
        return array("nodes:item" => $res);
    }

    // Логирование изменений в пользователе из административной зоны и при выгрузке из 1С
    public function update_user_save_to_log(iUmiEventPoint $event) {
        static $old_serialize_object;

        $object = $event->getRef('object');
        $objectTypeId = $object->getTypeId();
        $user_type_id = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName('users', 'user');
        if ($objectTypeId !== $user_type_id) { // не пользователь
            return true;
        }

        if ($event->getMode() == 'before') {
            $old_serialize_object = $this->create_object_snapshot($object);
        }elseif ($event->getMode() == 'after') {
            $creater = '';
            if($event->getParam('source_id') !== false){
                $creater = "Обмен данными с 1С";
            }else{
                $permissions = permissionsCollection::getInstance();
                $current_user_id = $permissions->getUserId();
                $user = umiObjectsCollection::getInstance()->getObject($current_user_id);
                if ($user instanceof umiObject) {
                    $creater = $user->getValue('login') . ' (' . $current_user_id . ')';
                }
            }

            $new_serialize_object = $this->create_object_snapshot($object);
            $res = $this->create_diff_details($old_serialize_object, $new_serialize_object);

            $this->save_users_change_log($object->id, $creater, $res['message'], $res['details']);
        }
        return true;
    }

    // Логирование изменений в пользователе из административной зоны на списке пользователей
    public function update_fast_property_user_save_to_log(iUmiEventPoint $event) {
        static $old_serialize_property;

        $object = $event->getRef('entity');
        $property_name = $event->getParam('property');

        $objectTypeId = $object->getTypeId();
        $user_type_id = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName('users', 'user');
        if ($objectTypeId !== $user_type_id) { // не пользователь
            return true;
        }

        if ($event->getMode() == 'before') {
            $old_serialize_property = $this->create_property_snapshot($object->getPropByName($property_name));
        }elseif ($event->getMode() == 'after') {
            $permissions = permissionsCollection::getInstance();
            $current_user_id = $permissions->getUserId();
            $user_name = '';
            $user = umiObjectsCollection::getInstance()->getObject($current_user_id);
            if ($user instanceof umiObject) {
                $user_name = $user->getValue('login');
            }

            $new_serialize_property = $this->create_property_snapshot($object->getPropByName($property_name));
            $res = $this->create_diff_details($old_serialize_property, $new_serialize_property);

            $this->save_users_change_log($object->id, $user_name . ' (' . $current_user_id . ')', $res['message'], $res['details']);
        }
        return true;
    }

    // Логирование изменений в пользователе из личного кабинета
    public function update_user_from_personal_save_to_log(iUmiEventPoint $event) {
        static $old_serialize_object;

        $current_user_id = $event->getParam('user_id');
        $object = umiObjectsCollection::getInstance()->getObject($current_user_id);

        if ($event->getMode() == 'before') {
            $old_serialize_object = $this->create_object_snapshot($object);
        }elseif ($event->getMode() == 'after') {
            $creater = $object->getValue('login') . ' (' . $current_user_id . '). Личный кабинет';

            $new_serialize_object = $this->create_object_snapshot($object);
            $res = $this->create_diff_details($old_serialize_object, $new_serialize_object);

            $this->save_users_change_log($object->id, $creater, $res['message'], $res['details']);
        }
        return true;
    }

    // Логирование изменений в пользователе при регистрации на мероприятие
    public function update_user_from_event_registration_save_to_log(iUmiEventPoint $event) {
        static $old_serialize_object;

        $current_user_id = $event->getParam('user_id');
        $object = umiObjectsCollection::getInstance()->getObject($current_user_id);

        if ($event->getMode() == 'before') {
            $old_serialize_object = $this->create_object_snapshot($object);
        }elseif ($event->getMode() == 'after') {
            $creater = $object->getValue('login') . ' (' . $current_user_id . '). Регистрация на мероприятие';

            $new_serialize_object = $this->create_object_snapshot($object);
            $res = $this->create_diff_details($old_serialize_object, $new_serialize_object);

            $this->save_users_change_log($object->id, $creater, $res['message'], $res['details']);
        }
        return true;
    }

    // Логирование изменений в пользователе при регистрации на мероприятие с лендинга
    public function update_user_from_event_registration_from_landing_save_to_log(iUmiEventPoint $event) {
        static $old_serialize_object;

        $current_user_id = $event->getParam('user_id');
        $object = umiObjectsCollection::getInstance()->getObject($current_user_id);

        if ($event->getMode() == 'before') {
            $old_serialize_object = $this->create_object_snapshot($object);
        }elseif ($event->getMode() == 'after') {
            $creater = $object->getValue('login') . ' (' . $current_user_id . '). Регистрация на мероприятие с лендинга';

            $new_serialize_object = $this->create_object_snapshot($object);
            $res = $this->create_diff_details($old_serialize_object, $new_serialize_object);

            $this->save_users_change_log($object->id, $creater, $res['message'], $res['details']);
        }
        return true;
    }

    // Логирование восстановления пароля пользователем
    public function update_user_restore_password_save_to_log(iUmiEventPoint $event) {
        static $old_serialize_object;

        $current_user_id = $event->getParam('user_id');
        $object = umiObjectsCollection::getInstance()->getObject($current_user_id);

        if ($event->getMode() == 'before') {
            $old_serialize_object = $this->create_object_snapshot($object);
        }elseif ($event->getMode() == 'after') {
            $creater = $object->getValue('login') . ' (' . $current_user_id . '). Восстановление пароля';

            $new_serialize_object = $this->create_object_snapshot($object);
            $res = $this->create_diff_details($old_serialize_object, $new_serialize_object);

            $this->save_users_change_log($object->id, $creater, $res['message'], $res['details']);
        }
        return true;
    }

    /**
     * Превращаем объект в массив свойств
     * @param umiObject $object
     * @return массив со значениями свойств
     */
    public function create_object_snapshot(umiObject $object){
        $result = array();
        $object_types_collection = umiObjectTypesCollection::getInstance();
        $object_type = $object_types_collection->getType($object->getTypeId());
        $object_fields = $object_type->getAllFields();
        foreach($object_fields as $field) {
            $row = $this->create_property_snapshot($object->getPropByName($field->getName()));
            if($row !== false){
                $result = $result + $row;
            }
        }
        return $result;
    }

    /**
     * Превращаем одно свойство объекта в массив
     * @param umiObjectProperty $property
     * @return boolean false если значение пусто или массив из правильных значений
     */
    public function create_property_snapshot(umiObjectProperty $property){
        try {
            $value = $property->getValue();
            if(!empty($value)) {
                $field = $property->getField();
                $res = array(
                    "id" => $field->getId(),
                    "type" => $field->getDataType(),
                    "name" => $property->getName(),
                    "title" => $property->getTitle()
                );
                if(is_array($value)){
                    $res['value'] = $value;
                }else{
                    $res['value'] = mysql_real_escape_string($value);
                }
                return array($field->getId() => $res);
            }
        } catch (Exception $exc) {
        }

        return false;
    }

    /**
     * Получаем разницу между двумя сериализованными объектами и сообщение об этом с деталями
     * @param type $old_value
     * @param type $new_value
     * @return type
     */
    public function create_diff_details($old_value, $new_value){
        $message = array();
        $details = array();
        $diff_props = array();

        // все ключи
        $keys = array_unique(array_merge(array_keys($old_value), array_keys($new_value)));
        foreach($keys as $key){
            // пропускаем пустой пароль
            if(key_exists($key, $old_value) && !key_exists($key, $new_value) && $old_value[$key]['title'] == 'Пароль'){
                continue;
            }

            // Дата 1970-01-01 03:00:00 - пустая, так как это стандарт пустого значения редактора дат. Чистим значение
            if(key_exists($key, $new_value) && $new_value[$key]['type'] == 'date' && $new_value[$key]['value'] == '1970-01-01 03:00:00'){
                unset($new_value[$key]);
            }

            // Файл, не являющийся файлом (директория) - пустой, так как это стандарт пустого значения выбора файла. Чистим значение
            if(key_exists($key, $new_value) && $new_value[$key]['type'] == 'file' && !is_file($_SERVER['DOCUMENT_ROOT'].$new_value[$key]['value'])){
                unset($new_value[$key]);
            }

            if(key_exists($key, $old_value) && !key_exists($key, $new_value)){
                $diff_props[$key] = array(
                    "name" => $old_value[$key]['title'],
                    "old" => $old_value[$key],
                    "new" => false
                );
            }elseif(!key_exists($key, $old_value) && key_exists($key, $new_value)){
                $diff_props[$key] = array(
                    "name" => $new_value[$key]['title'],
                    "old" => false,
                    "new" => $new_value[$key]
                );
            }elseif(key_exists($key, $old_value) && key_exists($key, $new_value)){
                if($old_value[$key]['value'] != $new_value[$key]['value']){
                    $diff_props[$key] = array(
                        "name" => $old_value[$key]['title'],
                        "old" => $old_value[$key],
                        "new" => $new_value[$key]
                    );
                }
            }
        }

        foreach($diff_props as $key => $values){
            $message[] = 'Изменено свойство "' . $values['name'] . '". Старое значение: "' . ($values['old'] === false ? '' : $values['old']['value']) . '", новое значение: "' . ($values['new'] === false ? '' : $values['new']['value']) . '".';
            $details[$key] = $values;
        }

        return array(
            "message" => implode("<br>", $message),
            "details" => $details
        );
    }

	/**
	 * Получение всех вузов с разделением по регионам
	 * @return type
	 */
	public function get_all_vuz_by_region(){
		$result = array();
		$items = new selector('objects');
		$items->types('object-type')->id(206);
		foreach ($items as $item){
			if(empty($item->getValue('region'))){
				$result[] = array(
					"attribute:id" => $item->getId(),
					"node:text" => $item->getValue('nazvanie_vuza')
				);
			}else{
				$result[$item->getValue('region')]["nodes:item"][] = array(
					"attribute:id" => $item->getId(),
					"node:text" => $item->getValue('nazvanie_vuza')
				);
			}
		}

		$collection = umiObjectsCollection::getInstance();
		foreach($result as $l=>$v){
			if(isset($result[$l]['nodes:item'])){
				$result[$l]['attribute:id'] = $l;
				$obj = $collection->getObject($l);
				if($obj instanceof umiObject){
					$result[$l]['attribute:name'] = $obj->getName();
				}
			}
		}

		return array("nodes:item" => $result);
	}


    /**
     * Попытка получения результата из файла кеша
     * @param type $path - путь до файла
     * @param string $file_name - имя файла
     * @param type $expires - время жизни кеша
     * @return содержимое файла или NULL
     */
    public function try_get_cache($path, $file_name, $expires = 10800) {
        $file_name = $path . $file_name;

        if (!file_exists($path) && !is_dir($path)) {
            mkdir($path, 0777);
        }

        if (file_exists($file_name) && time() - filemtime($file_name) < $expires) {
            return unserialize(file_get_contents($file_name));
        }
        return null;
    }

    /**
     * Сохранение в файл кеша
     * @param type $path
     * @param type $file_name
     * @param type $result
     *
     */
    public function save_cache($path, $file_name, $result) {
        file_put_contents($path . $file_name, serialize($result));
    }

	/**
	 * Очистка кеша по сайту
	 */
    public function clear_cache() {
		$directories = array(
			'/sys-temp/webinars_list_elements',
		);

		foreach($directories as $dir){
			$dir_name = $_SERVER['DOCUMENT_ROOT'] . $dir;
			$files = scandir($dir_name);
			foreach ($files as $file) {
				if (!in_array($file, array(".", ".."))) {
					unlink($dir_name . '/' . $file);
				}
			}
		}
		return $this->redirect($_SERVER['HTTP_REFERER']);
	}


    /**
     * Отправка уведомлений за 3 дня
     * @return type
     */
    public function push_notification_in_3_days(){
        $today = strtotime(date('d.m.Y', time()));
        $start_date = $today + (60*60*24*3);
        $end_date = $today + (60*60*24*4) - 1;

        return self::_push_notification_in_some_days($start_date, $end_date, 'Через 3 дня состоится ');
    }

    /**
     * Отправка уведомлений за 1 день
     * @return type
     */
    public function push_notification_in_1_day(){
        $today = strtotime(date('d.m.Y', time()));
        $start_date = $today + (60*60*24*1);
        $end_date = $today + (60*60*24*2) - 1;

        return self::_push_notification_in_some_days($start_date, $end_date, 'Уже завтра состоится ');
    }

    /**
     * Внутренняя функция для выявления мероприятий, по которым необходимо отправлять уведомления
     * @param type $start_date
     * @param type $end_date
     * @param type $message
     * @return type
     */
    private function _push_notification_in_some_days($start_date, $end_date, $message){
        $result = [
            'orders' => [],
            'customers_count' => 0,
            'orders_count' => 0,
        ];

        // Список мероприятий
        $events = new selector('pages');
        $events->types('hierarchy-type')->name('catalog', 'object');
        $events->where('publish_date')->between($start_date, $end_date);
        $all_events_id = [];
        foreach($events as $event){
            $all_events_id[$event->getId()] = $event->getValue('h1');
        }

        // Список позиций внутри заказа
        $order_items = new selector('objects');
        $order_items->types('object-type')->id(46); //name('emarket', 'orderItem');
        $order_items->where('item_link')->equals(array_keys($all_events_id));
        $order_items_id = [];
        foreach ($order_items as $order_item){
            $order_items_id[$order_item->getId()] = $order_item->getName();
        }

        // Список заказов для выявления списка клиентов
        $orders = new selector('objects');
        $orders->types('object-type')->name('emarket', 'order');
        $orders->where('order_items')->equals(array_keys($order_items_id));
        $customers = [];
        foreach ($orders as $order){
            $order_items = $order->getValue('order_items');
            $event_name = $order_items_id[$order_items[0]];

            $customers[$event_name][$order->getValue('customer_id')] = $order->getValue('customer_id');

            $result['orders'][] = $order->getId() . " - " . $order->getValue('status_id') . " - " . $order->getValue('customer_id') . " - " . $message . $order_items_id[$order_items[0]] . " - " . $order_items[0];
            $result['orders_count']++;
        }

        $result['customers'] = $customers;

        foreach($customers as $event_name => $event_customers){
            $result['response'][] = self::_send_one_signal_notification($event_customers, $message . $event_name);
            $result['customers_count'] += sizeof($event_customers);
        }

        return $result;
    }


    /**
     * Служебная функция для обращения к сервису One Signal
     * @param type $user_ids
     * @param type $message
     * @return type
     */
    private function _send_one_signal_notification($user_ids, $message){
        $fields = array(
            'app_id' => '0a393a9f-be08-44ec-8201-c90fb37783f6',
            'contents' => ['en' => $message, 'ru' => $message],
            'filters' => []
        );

        $filters = [];
        foreach($user_ids as $user_id){
            if(sizeof($filters) > 0){
                $filters[] = ["operator" => "OR"];
            }
            $filters[] = ["field" => "tag", "key" => "user_id", "relation" => "=", "value" => $user_id];
        }

        $fields['filters'] = $filters;
        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic YmY2NWZmN2UtYzQxNS00OWVkLWEzY2EtZDMyNDBjOTc0Mjlm'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }
}