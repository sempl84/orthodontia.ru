<?php
class emarket_custom_webinar_api extends def_module {
    // Проверка наличия пользователя в базе
    public function add_webinar_guest($order, $event, $email, $name, $pattrName, $secondName, $company, $prof_status) {
        $webinarEventId = $event->webinar_event_id;
        if(!$webinarEventId) {
			return 'no';
		}

        $url =  'https://userapi.webinar.ru/v3/events/'.$webinarEventId.'/register';
        $data = array(
            'email'=> $email,
            //'isAutoEnter'=> false,
            'isAccepted' => true,
            'name'=> $name,
            'pattrName'=> $pattrName,
            'secondName'=> $secondName,
            'organization'=> $company,	// организация
            'position'=> $prof_status	// должность
        );

        $data['additionalFields']['b972361926f1a3e5e04c9caddf77f28d']=$prof_status;
        $data['additionalFields']['d9800911a9155661a44b8aae9566f0f0']=$company; // компания

        $tokenid = '2771b2e9eeb3e5ed1fa6647b10226f04';
        $postData = http_build_query($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'x-auth-token: ' . $tokenid,
			'Content-Type: application/x-www-form-urlencoded; charset=utf-8')
        );

        $result = curl_exec($ch);
        $json_result = json_decode($result, true);
        curl_close($ch);

        $order->dannye_s_webinarru = $result;
        if(isset($json_result['link'])){
            $link = $json_result['link'];
            $order->webinar_room_link = $link;
        }
        $order->commit();

        return $json_result;
    }
};