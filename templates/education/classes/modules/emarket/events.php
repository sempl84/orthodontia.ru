<?php
// Notification listeners
new umiEventListener('systemModifyPropertyValue', 'emarket', 'onModifyPropertyCustom');
new umiEventListener('systemModifyObject', 'emarket', 'onModifyObjectCustom');
new umiEventListener('order-status-changed', 'emarket', 'onStatusChangedCustom');
//new umiEventListener('order-payment-status-changed', 'emarket', 'onPaymentStatusChangedCustom');
//new umiEventListener('order-delivery-status-changed', 'emarket', 'onDeliveryStatusChangedCustom');