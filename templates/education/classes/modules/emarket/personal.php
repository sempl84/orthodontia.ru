<?php
class emarket_custom_personal extends def_module {
    /**
     * Получить список всех мероприятий текущего пользователя
     *
     * @param string $template Шаблон(для TPL)
     * @param string $sort Сортировка
     * @param boolean $old выводить прошедшие мероприятия
     *
     * @return mixed
     *
     * TODO сортировать заказы по дате мероприятия и выводить либо те, что еще будут, либо те, что уже прошли
     */
    public function eventsList($template = 'default', $sort = "asc", $old = false) {
        $domainId = cmsController::getInstance()->getCurrentDomain()->getId();
        
        $select = new selector('objects');
        $select->types('object-type')->name('emarket', 'order');
        $select->where('customer_id')->equals(customer::get()->getId());
        $select->where('name')->isNull(false);
        $select->where('domain_id')->equals($domainId);
        $select->option('no-length')->value(true);
        $select->option('load-all-props')->value(true);
        
        if (in_array($sort, array("desc"))) {
            call_user_func(array($select->order('id'), $sort));
        }
        
        $umiHierarchy = umiHierarchy::getInstance();
        $umiLinksHelper = umiLinksHelper::getInstance();
        $umiObjects = umiObjectsCollection::getInstance();
        
        $itemsArray = array();
        foreach ($select->result() as $order) {
            $items = array();
            
            $orderObject = order::get($order->getId());
            //информация о мероприятии
            foreach ($orderObject->getItems() as $orderItem) {
                if(!$orderItem instanceof orderItem) {
                    continue;
                }
                
                $orderItemElementId = $orderItem->getItemElement()->getId();
                $element = $umiHierarchy->getElement($orderItemElementId);
                if ($element instanceof umiHierarchyElement) {
                    if($element->getObjectTypeId() == 226){ // консультации не попадают в список
                        continue;
                    }
                    
                    $publish_date = ($element->publish_date) ? $element->publish_date->getFormattedDate("U") : '';
                    
                    $itemDay = date('d', $publish_date);
                    $itemMonth = date('n', $publish_date);
                    $itemYear = date('Y', $publish_date);
                    $itemTime = strtotime("$itemDay-$itemMonth-$itemYear");
                    
                    $currDay = date('d');
                    $currMonth = date('n');
                    $currYear = date('Y');
                    $currTime = strtotime("$currDay-$currMonth-$currYear");
                    
                    if ($old && $currTime < $itemTime) {
                        continue;
                    } elseif (!$old && $currTime > $itemTime) {
                        continue(2);
                    }
                    
                    if(!$old) {
                        $actualPrice = $element->getValue(SiteCatalogObjectModel::field_price);
                        $eventDiscountValue = 0;
                        
                        if($orderObject->getValue('school')) {
                            $eventDiscountValue = ceil($actualPrice * 0.5);
                        } elseif($orderObject->getValue('ordinator')) {
                            $eventDiscountValue = ceil($actualPrice * 0.6);
                        } elseif($orderObject->getValue('ordinator_30')) {
                            $eventDiscountValue = ceil($actualPrice * 0.7);
                        } elseif($orderObject->getValue('doctor')) {
                            $eventDiscountValue = ceil($actualPrice * 0.7);
                        } elseif($orderObject->getValue('teacher')) {
                            $eventDiscountValue = ceil($actualPrice * 0.5);
                        } elseif($orderObject->getValue('poo')) {
                            $eventDiscountValue = ceil($actualPrice * 0.85);
                        } elseif($orderObject->getValue('loyalty')) {
                            $eventDiscountValue = ceil($actualPrice * 0.5);
                        } elseif($eventDiscountName = $orderObject->getValue('event_discount_name')) {
                            for($i = 1; $i <= 5; $i++) {
                                if($orderObject->getValue('nazvanie_skidki_' . $i) != $eventDiscountName) {
                                    continue;
                                }
                                
                                $eventDiscountValue = $orderObject->getValue('stoimost_meropriyatiya_s_uchetom_skidki_' . $i);
                                break;
                            }
                        }
                        
                        $actualPrice = $actualPrice - $eventDiscountValue;
                        if($actualPrice != $orderItem->getTotalActualPrice()) {
                            $orderItem->setActualPrice($actualPrice)->commit();
                            $orderObject->setValue('event_discount_value', $eventDiscountValue);
                            $orderObject->refresh();
                        }
                    }
                    
                    $item = array();
                    $elementId = $element->getId();
                    $item['attribute:order_id'] = $order->getId();
                    $item['attribute:actualPrice'] = $orderItem->getTotalActualPrice();
                    $item['attribute:id'] = $elementId;
                    $item['attribute:alt_name'] = $element->getAltName();
                    $item['attribute:link'] = $umiLinksHelper->getLinkByParts($element);
                    $item['attribute:publish_date'] = ($element->publish_date) ? $element->publish_date->getFormattedDate("U") : '';
                    $item['xlink:href'] = 'upage://' . $elementId;
                    $item['node:text'] = $element->getName();
                    $items[] = def_module::parseTemplate('', $item, $elementId);
                    def_module::pushEditable('catalog', 'object', $elementId);
                    $umiHierarchy->unloadElement($elementId);
                }
            }
            
            $paymentName = "";
            $paymentStatus = "";
            $paymentTypeName = "";
            try {
                $payment = payment::get($order->payment_id, $orderObject);
                
                if ($payment) {
                    $paymentName = $payment->name;
                    $paymentStatus = order::getCodeByStatus($orderObject->getPaymentStatus());
                    
                    $paymentObject = $payment->getObject();
                    $paymentTypeId = $paymentObject->getValue('payment_type_id');
                    $paymentTypeName = $umiObjects->getObject($paymentTypeId)->getValue('class_name');
                }
            } catch (coreException $e) {
                $paymentName = "";
                $paymentStatus = "";
                $paymentTypeName = "";
            }
            
            $order_item = array(
                'attribute:id' => $order->id,
                'attribute:name' => $order->name,
                'attribute:type-id' => $order->typeId,
                'attribute:guid' => $order->GUID,
                'attribute:type-guid' => $order->typeGUID,
                'attribute:ownerId' => $order->ownerId,
                'attribute:paymentName' => $paymentName,
                'attribute:paymentStatus' => $paymentStatus,
                'attribute:paymentTypeName' => $paymentTypeName,
                'xlink:href' => $order->xlink,
                'subnodes:lines' => $items
            );
            
            $itemsArray[] = def_module::parseTemplate('', $order_item, false, $order->getId());
        }
        
        return def_module::parseTemplate('', array('subnodes:items' => $itemsArray));
    }
}