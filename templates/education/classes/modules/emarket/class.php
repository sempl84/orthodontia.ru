<?php
class emarket_custom extends def_module {
    public function __construct($self) {
        $self->__loadLib("/notification_new.php", (dirname(__FILE__)));
        $self->__implement("emarket_custom_notification_new");

        $self->__loadLib("/personal.php", (dirname(__FILE__)));
        $self->__implement("emarket_custom_personal");

        $self->__loadLib("/moderate_order.php", (dirname(__FILE__)));
        $self->__implement("emarket_custom_moderate_order");

        $self->__loadLib("/lp_api.php", (dirname(__FILE__)));
        $self->__implement("emarket_custom_lp_api");

        $self->__loadLib("/promocode.php", (dirname(__FILE__)));
        $self->__implement("emarket_custom_promocode");

        $self->__loadLib("/import_user_discount.php", (dirname(__FILE__)));
        $self->__implement("emarket_custom_import_user_discount");

        $self->__loadLib("/import_orders.php", (dirname(__FILE__)));
        $self->__implement("emarket_custom_import_orders_payment_status");

        $self -> __loadLib("/webinar_api.php", (dirname(__FILE__)));
        $self -> __implement("emarket_custom_webinar_api");

        $self -> __loadLib("/orders_clear.php", (dirname(__FILE__)));
        $self -> __implement("emarket_orders_clear");

        $self -> __loadLib("/order_consultation.php", (dirname(__FILE__)));
        $self -> __implement("emarket_order_consultation");
    }



    public function registerEventOneClick($elementId = NULL) {
        //global $_FILES;
        file_put_contents(CURRENT_WORKING_DIR . '/traceajaxord.txt', "\n\n orderinf" . print_r($_REQUEST, true) . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);

        //получить и проверить товар
        if (!$elementId) {
            $elementId = getRequest('page_id');
        }
        if (!$elementId) {
            $elementId = getRequest('param0');
        }

        $customer = customer::get();
        if(!$customer->isUser()){
            return 'Необходимо войти в личный кабинет для регистрации на мероприятие';
        }

        $customer_email = $customer->getValue('e-mail');
        if ($this->checkSecondPurchase($elementId, $customer_email) == 0) {
            return 'Вы уже зарегистрированы на данное мероприятие';
        }

        $hierarchy = umiHierarchy::getInstance();
        $controller = cmsController::getInstance();

        $element = $hierarchy->getElement($elementId);
        $isCatalogItem = false;
        if ($element instanceof umiHierarchyElement) {
            $hierarchyType = umiHierarchyTypesCollection::getInstance()->getTypeByName("catalog", "object");
            $hierarchyTypeId = $hierarchyType->getId();

            if($element->getTypeId() == $hierarchyTypeId){
                $isCatalogItem = true;
            }
        }
        if ($isCatalogItem) {
            $isSpeakersDiscountEnabled = getArrayKey(getArrayKey(getRequest('data'), 'new'), 'skidka');
            $isSpeakersDiscountEvent = SiteCatalogObjectModel::isSpeakersDiscountEvent($element);

            if($isSpeakersDiscountEnabled && $isSpeakersDiscountEvent && intval($element->getValue(SiteCatalogObjectModel::field_speakers_left)) <= 0) {
                return 'Закончились места для выступающих на мероприятии';
            }

            $order = $this->getBasketOrder(false);

            // проверяем не перезаписывается ли существующий заказ
            $orderNumber = $order->getNumber();
            if ($orderNumber > 1) {
                $order->order();
            }
            $order = $this->getBasketOrder(false);

            $dataModule = $controller->getModule('data');
            // добавляем товар
            $_REQUEST['no-redirect'] = 1;
            $this->basket('put', 'element', $elementId);

            //очищаем корзину от лишних товаров
            foreach ($order->getItems() as $orderItem) {
                $orderItemElementId = $orderItem->getItemElement()->id;
                if ($orderItemElementId != $elementId) {
                    $order->removeItem($orderItem);
                } else {
                    if ($orderItem->getAmount() > 1) {
                        $orderItem->setAmount(1);
                        $orderItem->refresh();
                    }
                }
            }
            $order->refresh();

            //сохраняем все сырые данные в заказ (в том числе название и id товара)
            $orderId = $order->id;
            $dataModule->saveEditedObjectWithIgnorePermissions($orderId, true, true);

            // пытаемся дополнить данные пользователя сырыми данными из формы
            $dataForm = getRequest('data');
            $dataForm = (isset($dataForm['new'])) ? $dataForm['new'] : array();

            $oEventPoint = new umiEventPoint("users_register_on_event_do");
            $oEventPoint->setMode("before");
            $oEventPoint->setParam("user_id", $customer->id);
            $this->setEventPoint($oEventPoint);

            foreach ($dataForm as $fieldName => $fieldValue) {
                $fieldName = str_replace("order_", "", $fieldName);
                if (trim($customer->getValue($fieldName)) == '') {
                    $customer->setValue($fieldName, $fieldValue);
                }
            }
            $customer->commit();

            $oEventPoint->setMode("after");
            $this->setEventPoint($oEventPoint);

            $check_fields_name = array(
                'order_lname',
                'order_fname',
                'order_father_name',
                'order_e-mail',
                'order_phone',
                'order_country',
                'order_region',
                'order_city',
            );
            foreach($check_fields_name as $field_name){
                if(empty(trim($order->getValue($field_name)))){
                    $order->setValue($field_name, $customer->getValue(str_replace("order_", "", $field_name)));
                }
            }

            // сохраняем информацию о мероприятии
            $order->order_date = time();
            $order->page_id = $elementId;
            $order->event_name = $element->h1;

            // скидки
            $event_discount_name = getRequest('event_discount_name');
            $order->setValue('event_discount_name', $event_discount_name);
            $event_discount_value = getRequest('event_discount_value');
            $order->setValue('event_discount_value', $event_discount_value);
            $event_discount_id = getRequest('event_discount_id');
            switch ($event_discount_id) {
                case 'discount_ordinator':
                    $order->ordinator = 1;
                    break;
                case 'discount_ordinator_30':
                    $order->ordinator_30 = 1;
                    break;
                case 'discount_doctor':
                    $order->doctor = 1;
                    break;
                case 'discount_teacher':
                    $order->teacher = 1;
                    break;
                case 'discount_poo':
                    $order->poo = 1;
                    break;
                case 'discount_shoo':
                    $order->school = 1;
                    break;
                case 'discount_loyalty':
                    $order->loyalty = 1;
                    break;
            }

            // сохраняем потенциальные и заморожденные баллы ORMCO STARS
            $potential_ormco_star = (int) getRequest('potential_ormco_star');
            if ($potential_ormco_star > 0) {
                $order->setValue('potential_ormco_star', $potential_ormco_star);
                $customer->setValue('potential_ormco_star', $customer->getValue('potential_ormco_star') + $potential_ormco_star);
            }
            $freez_ormco_star = (int) getRequest('freez_ormco_star');
            if ($freez_ormco_star > 0) {
                $order->setValue('freez_ormco_star', $freez_ormco_star);
                $customer->setValue('freez_ormco_star', $customer->getValue('freez_ormco_star') + $freez_ormco_star);
                $customer->setValue('yet_ormco_star', $customer->getValue('yet_ormco_star') - $freez_ormco_star);
            }
            // отправка уведомления об OrmcoStars
            if ($freez_ormco_star > 0 || $potential_ormco_star > 0) {
                $customer->commit();
                $usersModule = $controller->getModule('users');
                if ($usersModule) {
                    $usersModule->sendOrmcoStarsNotify($customer);
                }
            }

            $event_discount_name = str_replace("%", "%", $event_discount_name);
            $event_discount_name = str_replace("&#37;", "%", $event_discount_name);
            if (strpos($event_discount_name, 'Промокод') !== false || strpos($event_discount_name, 'промокод') !== false) {
                $start = strpos($event_discount_name, '"');
                $finish = strpos($event_discount_name, '"');
                if ($start !== false && $finish !== false) {
                    $promocode = substr($event_discount_name, $start, $finish);

                    $promocodes_str = $element->promokod_arr;
                    $promocodes_arr = json_decode($promocodes_str);
                    $promocodes_arr_new = array();
                    foreach ($promocodes_arr as $promocodeItem) {
                        $pr_name = $pr_discount = $pr_count = $pr_active = null;

                        $pr_name = $promocodeItem->name;
                        $pr_discount = $promocodeItem->discount;
                        $pr_discount_abs = $promocodeItem->discount_abs;
                        $pr_text = $promocodeItem->text;
                        $pr_count = $promocodeItem->count;
                        $pr_active = $promocodeItem->active;
                        if ($pr_name == $promocode) {
                            $pr_count = $pr_count - 1;
                            $pr_active = ($pr_count < 1) ? 0 : 1;
                        }
                        //[{"name":"testpromo","discount":"10","count":"1","active":"1"},{"name":"111","discount":"20","count":"15","active":"1"},{"name":"2222","discount":"50","count":"12","active":"0"}]
                        $promocodes_arr_new[] = array(
                            "name" => $pr_name,
                            "discount" => $pr_discount,
                            "discount_abs" => $pr_discount_abs,
                            "text" => $pr_text,
                            "count" => $pr_count,
                            "active" => $pr_active
                        );
                    }

                    $element->promokod_arr = json_encode($promocodes_arr_new);
                    $element->commit();
                }
            }

            $gorod_in = $element->gorod_in;
            $gorod_in_object = umiObjectsCollection::getInstance()->getObject($gorod_in);
            if ($gorod_in_object) {
                $order->event_gorod_in = $gorod_in_object->name;
            }

            $publish_date = $element->publish_date;
            if ($publish_date) {
                $publish_date = $publish_date->getFormattedDate("U");
            }
            $finish_date = $element->finish_date;
            if ($finish_date) {
                $finish_date = $finish_date->getFormattedDate("U");
            }
            if ($finish_date && $publish_date) {
                $event_data = $dataModule->dateruPeriod($publish_date, $finish_date);
                $event_data_str = (isset($event_data['first_line'])) ? $event_data['first_line'] : '';
                $event_data_str .= (isset($event_data['second_line'])) ? ' ' . $event_data['second_line'] : '';
                $event_data_str .= (isset($event_data['days'])) ? ' (' . $event_data['days'] . ')' : '';
            } else {
                $event_data_str = 'дата не определена';
            }
            $order->event_data = $event_data_str;

            if($element->getValue('otpravlyat_vse_registracii_na_moderaciyu') == 1 || !empty($event_discount_name) || !empty($event_discount_value)){
                $order->setValue('na_moderacii', 1);
            }

            // get customer data
            $customer_lname = $customer->lname;
            $customer_fname = $customer->fname;
            $customer_father_name = $customer->father_name;
            $customer_phone = $customer->phone;
            $customer_email = $customer->getValue('e-mail');
            if (!$customer_email){
                $customer_email = $customer->getValue('email');
            }
            $customer_city = $customer->city;
            $customer_region = $this->get_relation_item_name($customer->region);
            $customer_company = $customer->company;
            $customer_prof_status_id = $customer->prof_status;
            $customer_prof_status = $this->get_relation_item_name($customer_prof_status_id);
            $customer_other_specialization = $customer->other_specialization;




            // отправка данных в webinar.ru
            $webinar_lname = $customer_lname;
            $webinar_fname = $customer_fname;
            $webinar_father_name = $customer_father_name;
            $webinar_email = $customer_email;
            $webinar_company = ($customer_company != '') ? $customer_company : ' '; // клиника/ВУЗ
            $webinar_prof_status = $customer_prof_status;
            $webinar_prof_status_id =  $customer_prof_status_id;
            if($webinar_prof_status_id == 12137){ // Другая специализация
                $webinar_prof_status = $customer_other_specialization;
            }


            $this->add_webinar_guest($order, $element, $webinar_email, $webinar_fname, $webinar_father_name, $webinar_lname, $webinar_company, $webinar_prof_status);


            //1. если это партнерское мероприятие, то завершаем заказ и убираем синхронизацию с 1С и отправляем письмо партнерским менеджерам
            if ($element->partner_event) {
                $order->partner_event = true;

                $order->commit();
                $order->order();

                // TODO add xslt mail template
                $order_lname = $customer_lname;
                $order_fname = $customer_fname;
                $order_father_name = $customer_father_name;
                $order_phone = $customer_phone;
                $order_email = $customer_email;
                $order_city = $customer_city;

                $order_region = $customer_region;
                $event_name = $order->event_name;
                $event_gorod_in = $order->event_gorod_in;
                $event_data = $order->event_data;

                if ($event_discount_name && $event_discount_value) {
                    $event_discount = "Скидка участника: {$event_discount_name} - {$event_discount_value} <br/>";
                }
                if ($order->ordinator) {
                    $order_upload = $order->order_upload;
                    $event_ordinator = "Я ординатор: <a href='{$order_upload}'>Удостоверение</a><br/>";
                }
                if ($order->ordinator_30) {
                    $order_upload = $order->order_upload;
                    $event_ordinator = "Я ординатор: <a href='{$order_upload}'>Удостоверение</a><br/>";
                }
                if ($order->doctor) {
                    $order_upload = $order->order_upload;
                    $event_doctor = "Я врач 1-2 года: <a href='{$order_upload}'>Удостоверение</a><br/>";
                }
                if ($order->teacher) {
                    $order_upload = $order->order_upload;
                    $event_teacher = "Я преподаватель кафедры: <a href='{$order_upload}'>Удостоверение</a><br/>";
                }

                $order_want_to_buy = ($order->order_want_to_buy) ? 'да' : 'нет';
                $order_how_you_been_edu = ($order->order_how_you_been_edu) ? 'да' : 'нет';

                $content = <<<END
Фамилия: {$order_lname}<br/>
Имя: {$order_fname}<br/>
Отчество: {$order_father_name}<br/>
Телефон: {$order_phone}<br/>
Эл. почта: {$order_email}<br/>
Регион участника: {$order_region}<br/>
Город участника: {$order_city}<br/>
Семинар: {$event_name}<br/>
Город семинара: {$event_gorod_in}<br/>
Дата: {$event_data}<br/>
Планируете ли вы покупать продукцию Ormco в ближайшее время (1-2 месяца)?: {$order_want_to_buy}<br/>
Принимали ли Вы участие в образовательных мероприятиях Ormco в течение последнего года?: {$order_how_you_been_edu}<br/>
{$event_discount}
{$event_ordinator}
{$event_doctor}
{$event_teacher}
END;

                $cmsController = cmsController::getInstance();
                $domains = domainsCollection::getInstance();
                $domainId = $cmsController->getCurrentDomain()->getId();

                $regedit = regedit::getInstance();
                $fromMail = $regedit->getVal("//modules/emarket/from-email/{$domainId}");
                $fromName = $regedit->getVal("//modules/emarket/from-name/{$domainId}");

                $letter = new umiMail();
                $webform_email = $element->webform_email;
                $arrMails = explode(",", $webform_email);
                $arrMails = array_map("trim", $arrMails);
                foreach ($arrMails as $sEmail) {
                    $letter->addRecipient($sEmail, 'Партнер');
                }
                $letter->setFrom($fromMail, $fromName);
                $letter->setSubject('Партнерское мероприятие - ' . $order->event_name);
                $letter->setContent($content);
                $letter->commit();
                $letter->send();

                $settings = SiteContentPageSettingsModel::getSettingsObject();
                if($settings instanceof umiHierarchyElement) {
                    $clientTemplate = $settings->getValue(SiteContentPageSettingsModel::field_mail_template_register_seminar_client);
                    $clientTemplate = def_module::parseTPLMacroses($clientTemplate, false, false, array(
                        'event_name' => $event_name,
                        'event_city' => $event_gorod_in,
                        'event_date' => $event_data
                    ));

                    if(trim(strip_tags($clientTemplate))) {
                        $letter = new umiMail();
                        $letter->addRecipient($order_email, $order_fname);
                        $letter->setFrom($fromMail, $fromName);
                        $letter->setSubject($order->event_name);
                        $letter->setContent($clientTemplate);
                        $letter->commit();
                        $letter->send();
                    }
                }

                if ($element->partner_event_1c_send) {
                    $order->setValue('need_export_new', 1);
                } else {
                    $order->setValue('need_export_new', 0);
                }
                $order->commit();

                // увеличение кол-ва регистраций на мероприятие
                $element->curr_reserv = $element->curr_reserv + 1;
                $element->commit();

                return $this->redirect('/partner_event_apply_successful/?o=' . $orderId);
            }

			$order->setValue('need_export', 0);
			$order->setValue('need_export_new', 1);

            //2. если нет скидки или это юр лицо переходим к оплате
            // TODO  нет проверки на юр лицо
            if ($element->getValue('otpravlyat_vse_registracii_na_moderaciyu') != 1 && !$event_discount_name && !$event_discount_value && ($element->price != 0 || $element->price != '')) {
                // увеличение кол-ва регистраций на мероприятие
                $element->curr_reserv = $element->curr_reserv + 1;
                $element->commit();
                $this->redirect($this->pre_lang . '/' . $controller->getUrlPrefix() . 'emarket/purchase/payment/choose/');
            }

            //2.1 исключение для мероприятий с псевдоскидкой "Выступающий" и тех, что всегда отправляются на модерацию
            //"Damon клуб: открытый микрофон, 13.03.20, Москва" id = 2814
            //"Damon клуб: открытый микрофон, 16.04.20, Санкт-Петербург" id = 2827
            //"Damon клуб: открытый микрофон, 09.06.20, Москва" id = 2872
            if ($element->getValue('otpravlyat_vse_registracii_na_moderaciyu') != 1 && $element->listeners_speakers == 1 && empty($event_discount_name)) {
                // увеличение кол-ва регистраций на мероприятие
                $element->curr_reserv = $element->curr_reserv + 1;
                $element->commit();

                // снимаем галочку модерации
                $order->setValue('na_moderacii', 0);
                //Применить скидку
                $this->applyEventDiscount($orderId);

                $this->redirect($this->pre_lang . '/' . $controller->getUrlPrefix() . 'emarket/purchase/payment/choose/');
            }

            if($isSpeakersDiscountEnabled && $isSpeakersDiscountEvent) {
                $element->setValue(SiteCatalogObjectModel::field_speakers_left, $element->getValue(SiteCatalogObjectModel::field_speakers_left) - 1);
            }

            //3. если не партнерское мероприятие и есть потребности в модерации, то  завершаем заказ с уведомлением о модерации
            $order->order();

            $order->setValue('need_export', 0);
            $order->setValue('need_export_new', 1);

            // увеличение кол-ва регистраций на мероприятие
            $element->curr_reserv = $element->curr_reserv + 1;
            $element->commit();

            return $this->redirect('/event_apply_successful/?o=' . $orderId);
        }
        return 'Мероприятие не найдено';
    }

    // вывод информацию о мероприятии на страницу с сообщением о проверке заказа менеджером
    /*public function eventId() {
        $orderId = getRequest('o');
        //return $orderId;
        if (!$orderId && getRequest('MNT_TRANSACTION_ID')) {
            $orderId = getRequest('MNT_TRANSACTION_ID');
            $pos = strpos($orderId, '.');
            $orderId = substr($orderId, 0, $pos);
        }
        $order = umiObjectsCollection::getInstance()->getObject($orderId);
        if (!$orderId) {
            $customer = customer::get();

            $cmsController = cmsController::getInstance();
            $domain = $cmsController->getCurrentDomain();
            $domainId = $domain->getId();

            $orderId = $customer->getLastOrder($domainId);
            $order = umiObjectsCollection::getInstance()->getObject($orderId);
        }
        if (!$order) {
            return;
        }
        return $orderId;
    }*/

    // вывод информацию о мероприятии на страницу с сообщением о проверке заказа менеджером
    public function eventId() {
        $orderId = getRequest('o');
        //return $orderId;
        if (!$orderId && getRequest('MNT_TRANSACTION_ID')) {
            $orderId = getRequest('MNT_TRANSACTION_ID');
            $pos = strpos($orderId, '.');
            $orderId = substr($orderId, 0, $pos);
        }
        $order = umiObjectsCollection::getInstance()->getObject($orderId);
        if (!$order) {
            $customer = customer::get();
            $customer_id = $customer->id;

            $cmsController = cmsController::getInstance();
            $domain = $cmsController->getCurrentDomain();
            $domainId = $domain->getId();

            $orderId = $customer->getLastOrder($domainId);
            $order = umiObjectsCollection::getInstance()->getObject($orderId);

            if(!$order){
                $sel = new selector('objects');
                $sel->types('object-type')->name('emarket', 'order');
                $sel->where('status_id')->isNull(false);
                $sel->where('name')->notequals('dummy');
                $sel->where('customer_id')->equals($customer_id);
                $sel->order('status_change_date')->desc(true);


                if($order = $sel->first){
                    $orderId = $order->id;
                }
            }
        }
        if (!$order) {
            return;
        }
        return $orderId;



    }

    // вывод информацию о мероприятии на страницу с сообщением о проверке заказа менеджером
    public function eventInfo($order_id_from_page) {
        $orderId = getRequest('o');
        if (!$orderId && getRequest('MNT_TRANSACTION_ID')) {
            $orderId = getRequest('MNT_TRANSACTION_ID');
            $pos = strpos($orderId, '.');
            $orderId = substr($orderId, 0, $pos);
        }
        if (!$orderId) {
            $orderId = $order_id_from_page;
        }

        $order = umiObjectsCollection::getInstance()->getObject($orderId);
        if (!$order) {
            $customer = customer::get();

            $cmsController = cmsController::getInstance();
            $domain = $cmsController->getCurrentDomain();
            $domainId = $domain->getId();

            $orderId = $customer->getLastOrder($domainId);
            $order = umiObjectsCollection::getInstance()->getObject($orderId);
        }
        if (!$order) {
            return;
        }
        $seminar_name = $order->event_name;
        $seminar_name = ($seminar_name) ? "<br/><strong>'{$seminar_name}'</strong>" : '';
        $data = $order->event_data;
        $data = ($data) ? "<br/>{$data}" : '';
        $gorod_in = $order->event_gorod_in;
        $gorod_in = ($gorod_in) ? "<br/>{$gorod_in}" : '';
        return $seminar_name . $data . $gorod_in;
    }

	/**
	 * Вывод строки пояснения к заказу, исходя из того, является ли мероприятие платным или нет
	 * @param type $order_id_from_page
	 * @return type
	 */
    public function eventAddInfo($order_id_from_page) {
        $orderId = getRequest('o');
        if (!$orderId && getRequest('MNT_TRANSACTION_ID')) {
            $orderId = getRequest('MNT_TRANSACTION_ID');
            $pos = strpos($orderId, '.');
            $orderId = substr($orderId, 0, $pos);
        }
        if (!$orderId) {
            $orderId = $order_id_from_page;
        }

        $order = umiObjectsCollection::getInstance()->getObject($orderId);
        if (!$order) {
            $customer = customer::get();

            $cmsController = cmsController::getInstance();
            $domain = $cmsController->getCurrentDomain();
            $domainId = $domain->getId();

            $orderId = $customer->getLastOrder($domainId);
            $order = umiObjectsCollection::getInstance()->getObject($orderId);
        }
        if (!$order) {
            return;
        }

		$result = '<p>После проверки регистрации менеджером Ormco, на Вашу электронную почту придет&nbsp;подтверждение, либо корректировка выбранной скидки, а также информация о дальнейшей&nbsp;оплате Вашего участия.</p>';

		$seminar_page = umiHierarchy::getInstance()->getElement($order->getValue('page_id'));
		if($seminar_page instanceof umiHierarchyElement && empty($seminar_page->getValue('price'))){
			$result = '<p>После проверки регистрации менеджером Ormco, на Вашу электронную почту придет&nbsp;подтверждение, либо корректировка выбранного мероприятия.</p>';
		}

        return $result;
    }

    // вывод номера мероприятия
    public function eventOrderNum($order_id_from_page) {
        $orderId = getRequest('o');
        if (!$orderId) {
            $orderId = $order_id_from_page;
        }

        $order = umiObjectsCollection::getInstance()->getObject($orderId);
        if (!$order) {
            $customer = customer::get();

            $cmsController = cmsController::getInstance();
            $domain = $cmsController->getCurrentDomain();
            $domainId = $domain->getId();

            $orderId = $customer->getLastOrder($domainId);
            $order = umiObjectsCollection::getInstance()->getObject($orderId);
        }
        if (!$order) {
            return;
        }
        return $order->number;
    }

    // вывод ссылки на pdf версию счета для оплаты или квитанции
    public function eventOrderPdf() {
        $orderId = getRequest('o');
        $order = umiObjectsCollection::getInstance()->getObject($orderId);
        if (!$order) {
            $customer = customer::get();

            $cmsController = cmsController::getInstance();
            $domain = $cmsController->getCurrentDomain();
            $domainId = $domain->getId();

            $orderId = $customer->getLastOrder($domainId);
            $order = umiObjectsCollection::getInstance()->getObject($orderId);
        }

        if (!$order) {
            return;
        }

        switch ($order->payment_id) {
            case '4663':
                $result = "<p><a href='/tcpdf/docs/receipt.php?oi={$orderId}' target='_blank'>Открыть квитанцию для оплаты</a></p>";
                break;
            case '4666':
                $result = "<p><a href='/tcpdf/docs/invoicee.php?oi={$orderId}' target='_blank'>Открыть счет для оплаты</a></p>";
                break;
            default:
                $result = "";
                break;
        }

        return $result;
    }

    public function eventFieldValue($fieldName) {
        $orderId = getRequest('o');
        $order = umiObjectsCollection::getInstance()->getObject($orderId);
        if (!$order) {
            return;
        }
        $value = $order->getValue($fieldName);
        return $value;
    }

    // формирует новый номер для счета для бухгалтерии
    public function buhgalt_order_number($old_id = NULL) {
        if (!$old_id) {
            $old_id = getRequest('param0');
        }
        $res = "СТ" . str_pad($old_id, 9, "0", STR_PAD_LEFT);
        return $res;
    }

    // для вывода квитка на печать
    public function receiptt($orderId = NULL) {
        if (!$orderId) {
            $orderId = getRequest('param0');
        }
        // = 636870;
        $uri = "uobject://{$orderId}/?transform=sys-tpls/emarket-receipt-simple.xsl";
        //return file_get_contents($uri);

        $result = file_get_contents($uri);
        $buffer = outputBuffer::current();
        $buffer->charset('utf-8');
        $buffer->contentType('text/html');
        $buffer->clear();
        $buffer->push($result);
        $buffer->end();
    }

    // для вывода счета для юр лиц на печать
    public function invoicee($orderId = NULL) {
        if (!$orderId) {
            $orderId = getRequest('param0');
        }
        // = 636870;
        $uri = "uobject://{$orderId}/?transform=sys-tpls/emarket-invoice.xsl";
        //return file_get_contents($uri);

        $result = file_get_contents($uri);
        $buffer = outputBuffer::current();
        $buffer->charset('utf-8');
        $buffer->contentType('text/html');
        $buffer->clear();
        $buffer->push($result);
        $buffer->end();
    }

    // для вывода квитка на печать в pdf
    public function receiptPrint($orderId = NULL) {
        if (!$orderId) {
            $orderId = getRequest('param0');
        }
        // = 636870;
        $uri = "uobject://{$orderId}/?transform=sys-tpls/emarket-receipt-simple-print.xsl";
        //return file_get_contents($uri);

        $result = file_get_contents($uri);
        $buffer = outputBuffer::current();
        $buffer->charset('utf-8');
        $buffer->contentType('text/html');
        $buffer->clear();
        $buffer->push($result);
        $buffer->end();
    }

    // для вывода счета для юр лиц на печать в pdf
    public function invoiceePrint($orderId = NULL) {
        if (!$orderId) {
            $orderId = getRequest('param0');
        }
        // = 636870;
        $uri = "uobject://{$orderId}/?transform=sys-tpls/emarket-invoice-print.xsl";
        //return file_get_contents($uri);

        $result = file_get_contents($uri);
        $buffer = outputBuffer::current();
        $buffer->charset('utf-8');
        $buffer->contentType('text/html');
        $buffer->clear();
        $buffer->push($result);
        $buffer->end();
    }

    public function lporder() {
        $referer = getServer('HTTP_REFERER');
        $hierarchy = umiHierarchy::getInstance();

        $page_id = getRequest('events');
        file_put_contents(CURRENT_WORKING_DIR . '/traceajax.txt', "\n\n" . print_r($_REQUEST, true) . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);
        $element = $hierarchy->getElement($page_id);
        if ($element instanceof umiHierarchyElement) {
            $order = $this->getBasketOrder(false);

            // проверяем не перезаписывается ли существующий заказ
            $orderNumber = $order->getNumber();
            if ($orderNumber > 1) {
                $order->order();
            }
            $order = $this->getBasketOrder(false);

            // добавляем товар
            $_REQUEST['no-redirect'] = 1;
            $this->basket('put', 'element', $page_id);

            //очищаем корзину от лишних товаров
            foreach ($order->getItems() as $orderItem) {
                $orderItemElementId = $orderItem->getItemElement()->id;
                if ($orderItemElementId != $page_id) {
                    $order->removeItem($orderItem);
                } else {
                    if ($orderItem->getAmount() > 1) {
                        $orderItem->setAmount(1);
                        $orderItem->refresh();
                    }
                }
            }
            $order->bonus = 0;
            $order->refresh();

            //сохранение данных о покупателе
            $customer = customer::get();

            $fname = getRequest('fname');
            $father_name = getRequest('father_name');
            $lname = getRequest('lname');
            $company = getRequest('company');
            //Компания
            $gorod_in = getRequest('gorod_in');
            //Город семинара
            $gorod_from = getRequest('gorod_from');
            //Город покупателя

            $region = getRequest('region');
            // регион

            $ordinator = (getRequest('ordinator') == 'Ординатор') ? 1 : 0;
            $ordinator_30 = (getRequest('ordinator_30') == 'Ординатор 30') ? 1 : 0;
            $doctor = (getRequest('doctor') == 'Врач 1-2 года') ? 1 : 0;
            $teacher = (getRequest('teacher') == 'Преподаватель кафедры') ? 1 : 0;
            $school = (getRequest('school') == 'Участник Школы Ортодонтии') ? 1 : 0;
            $poo = (getRequest('poo') == 'Член ПОО') ? 1 : 0;
            //Ординатор (для скидки)

            $email = getRequest('email');
            //Email
            $phone = getRequest('phone');
            //телефон
            $want_to_buy = (getRequest('want_to_buy') == 1) ? 1 : 0;
            //Планирую покупку продукции Ormco в ближайшее время (1-2 месяца)

            $how_you_been_edu = (getRequest('how_you_been_edu') == 1) ? 60757 : 60758;
            //Email
            // если пользователь совсем новый, заполняем его данные из формы
            if ($customer->email == '') {
                $customer->email = $email;
            }
            if ($customer->fname == '') {
                $customer->fname = $fname;
            }
            if ($customer->lname == '') {
                $customer->lname = $lname;
            }
            // if($customer->father_name == '') $customer->father_name = $father_name;
            // if($customer->phone == '') $customer->phone = $phone;
            // if($customer->city == '') $customer->city = $gorod_from;
            // $customer->agree = 1;

            $customer->commit();

            // сохраняем данные в заказ
            $orderId = $order->id;
            //$order -> setValue('seminar_name', $element->name);
            $order->setValue('order_fname', $fname);
            $order->setValue('order_lname', $lname);
            $order->setValue('order_father_name', $father_name);
            $order->setValue('order_company', $company);
            $order->setValue('order_gorod_in', $gorod_in);
            $order->setValue('order_city', $gorod_from);
            if (!$region) {
                $region = 9870;
                // Москва
                $order->setValue('no_region', 1);
            }
            $order->setValue('order_region', $region);

            $order->setValue('ordinator', $ordinator);
            $order->setValue('ordinator_30', $ordinator_30);
            $order->setValue('doctor', $doctor);
            $order->setValue('teacher', $teacher);

            //Ординатор удостоверение
            $uploadfile = getRequest('filename');
            if (!$uploadfile){
                $uploadfile = getRequest('fileordinator');
            }
            if ($uploadfile){
                $order->setValue('order_upload', $uploadfile);
            }

            //удостоврения члена ПОО
            $filepoo = getRequest('filename1');
            if (!$filepoo) {
                $filepoo = getRequest('filepoo');
            }
            if ($filepoo) {
                $order->setValue('filepoo', $filepoo);
            }

            // сохранение скидки на мероприятие в заказ (скидка храниться всегда в первых полях)
            if ($page_id == 1351) {// для http://ormco.ru/frost2017/
                if ($ordinator) {
                    $event_discount_name = $element->nazvanie_skidki_2;
                    $order->setValue('event_discount_name', $event_discount_name);
                    $event_discount_value = $element->stoimost_meropriyatiya_s_uchetom_skidki_2;
                    $order->setValue('event_discount_value', $event_discount_value);
                }

                $order->setValue('school', $school);

                // Являюсь членом Профессионального общества ортодонтов
                if ($school) {
                    $event_discount_name = $element->nazvanie_skidki_1;
                    $order->setValue('event_discount_name', $event_discount_name);
                    $event_discount_value = $element->stoimost_meropriyatiya_s_uchetom_skidki_1;
                    $order->setValue('event_discount_value', $event_discount_value);
                }

                $order->setValue('poo', $poo);
                // Я участник Школы Ортодонтии ОРМКО
                if ($poo) {
                    $event_discount_name = $element->nazvanie_skidki_3;
                    $order->setValue('event_discount_name', $event_discount_name);
                    $event_discount_value = $element->stoimost_meropriyatiya_s_uchetom_skidki_3;
                    $order->setValue('event_discount_value', $event_discount_value);
                }
            } else {
                // сохранение скидок для всех остальных мероприятий
                $price = $element->price;
                $event_discount_name = $event_discount_value = false;
                if ($school) {
                    $order->school = 1;
                    $event_discount_name = 'Участникам Школы Ортодонтии ОРМКО (50%)';
                    $event_discount_value = ceil($price * 0.5);
                } elseif ($ordinator) {
                    $order->ordinator = 1;
                    $event_discount_name = 'Ординаторам (40%)';
                    $event_discount_value = ceil($price * 0.6);
                } elseif ($ordinator_30) {
                    $order->ordinator_30 = 1;
                    $event_discount_name = 'Ординаторам (30%)';
                    $event_discount_value = ceil($price * 0.7);
                } elseif ($doctor) {
                    $order->doctor = 1;
                    $event_discount_name = 'Врачам 1-2 (30%)';
                    $event_discount_value = ceil($price * 0.7);
                } elseif ($teacher) {
                    $order->teacher = 1;
                    $event_discount_name = 'Преподавателям (50%)';
                    $event_discount_value = ceil($price * 0.5);
                } elseif ($poo) {
                    $order->poo = 1;
                    $event_discount_name = 'Членам Профессионального общества ортодонтов (15%)';
                    $event_discount_value = ceil($price * 0.85);
                }
                if ($event_discount_name && $event_discount_value) {
                    $order->setValue('event_discount_name', $event_discount_name);
                    $order->setValue('event_discount_value', $event_discount_value);
                    $order->commit();
                }
            }

            $order->setValue('order_e-mail', $email);
            $order->setValue('order_phone', $phone);
            $order->setValue('order_want_to_buy', $want_to_buy);
            $order->setValue('order_how_you_been_edu', $how_you_been_edu);
            $order->setValue('order_who', getRequest('who'));

            $order->setValue('from_landing', 1);
            $order->setValue('order_agree', 1);
            $order->order_date = time();

            $order->page_id = $page_id;
            $order->event_name = $element->name;

            $gorod_in = $element->gorod_in;
            $gorod_in_object = umiObjectsCollection::getInstance()->getObject($gorod_in);
            if ($gorod_in_object) {
                $order->event_gorod_in = $gorod_in_object->name;
            }

            $publish_date = $element->publish_date;
            if ($publish_date) {
                $publish_date = $publish_date->getFormattedDate("U");
            }
            $finish_date = $element->finish_date;
            if ($finish_date) {
                $finish_date = $finish_date->getFormattedDate("U");
            }
            if ($finish_date && $publish_date) {
                $dataModule = cmsController::getInstance()->getModule('data');

                $event_data = $dataModule->dateruPeriod($publish_date, $finish_date);
                $event_data_str = (isset($event_data['first_line'])) ? $event_data['first_line'] : '';
                $event_data_str .= (isset($event_data['second_line'])) ? ' ' . $event_data['second_line'] : '';
                $event_data_str .= (isset($event_data['days'])) ? ' (' . $event_data['days'] . ')' : '';
            } else {
                $event_data_str = 'дата не определена';
            }
            $order->event_data = $event_data_str;

            // save data from landing form
            $input = isset($_REQUEST) ? $_REQUEST : "";
            $input_str = '';
            unset($input['path']);
            unset($input['umi_authorization']);
            foreach ($input as $key => $value) {
                $input_str .= $key . '=' . htmlspecialchars_decode($value) . "\n";
            }
            $order->data_from_landing = $input_str;
            $order->commit();

            $paymentId = getRequest('payment_id');
            $bonus = 0;

            if ($paymentId) {
                $order_obj = order::get($orderId);

                //применение скидки на мероприятие сохраненной в заказе
                if ($ordinator || $ordinator_30 || $doctor || $teacher || $school || $poo) {
                    $originPrice = $order_obj->getOriginalPrice();
                    $discount = (float) $order->event_discount_value;
                    $bonus = $originPrice - $discount;

                    $order->setValue('bonus', $bonus);
                    $order->commit();
                    $order->refresh();
                }

                $payment = payment::get($paymentId, $order_obj);
                if ($payment instanceof payment) {
                    $order->setValue('payment_id', $paymentId);
                    $order->commit();

                    // если нет скидки
                    if (!$ordinator && !$ordinator_30 && !$doctor && !$teacher && !$school && !$poo) {
                        $paymentName = $payment->getCodeName();
                        $url = "{$this->pre_lang}/" . cmsController::getInstance()->getUrlPrefix() . "emarket/purchase/payment/{$paymentName}/";
                        $this->redirect($url);
                    }

                    //3. если не партнерское мероприятие и нет потребности в модерации, то  завершения заказа
                    $order->order();

                    // увеличение кол-ва регистраций на мероприятие
                    $element->curr_reserv = $element->curr_reserv + 1;
                    $element->commit();

                    return $this->redirect('/event_apply_successful/?o=' . $orderId);
                }
            }
            return $this->redirect($referer);
        }
        return $this->redirect($referer);
    }

    public function lpordertmp() {
        var_dump($_REQUEST);
        exit('hhh');
    }

    public function refreshOrderItemLink() {
        return 'switch off';
        $hierarchy = umiHierarchy::getInstance();
        $newElement = 916;
        $element = $hierarchy->getElement($newElement);
        if ($element instanceof iUmiHierarchyElement == false) {
            throw new publicException("Page #{$newElement} not found");
        }

        $ids = array(5220, 5197, 5648, 5640, 5638, 5631, 5634, 5622, 5553, 5550, 5534, 5532, 5529, 5168, 5511, 5445, 5441, 5439, 5406, 5394, 5333, 5315, 5312, 5302, 5284, 5223, 5215, 5189, 5143, 5140, 5116, 5135, 5126, 5080, 5070, 5062, 5059, 5055);

        foreach ($ids as $orderId) {
            //$orderId = 5220;
            $order = order::get($orderId);

            foreach ($order->getItems() as $orderItem) {
                $orderItem->item_link = $element;
                //$orderItemId = $orderItem->id;//item_link = $element;
                //var_dump($orderItemId);
                //exit('yy2');
                //$gorod_in_object = umiObjectsCollection::getInstance() -> getObject($gorod_in);
                //$orderItem -> refresh();
            }
        }
        return 'ok';
    }

    public function change1CKey($id1C = NULL, $new_page_id = NULL) {
        //return 'switch off';
        $id1C = '53e2341b-7403-11e6-9462-005056b0350d';
        $new_page_id = 657;

        $relation = umiImportRelations::getInstance();
        $source_id = $relation->getSourceId('commerceML2');

        //$id1C = umiHierarchy::getInstance()->isExists($id1C);
        if (!$id1C){
            var_dump('1с id = ' . $id1C . ' не существует в системе;');
        }

        $is_new_exists = umiHierarchy::getInstance()->isExists($new_page_id);
        if (!$is_new_exists){
            var_dump('Новой страницы с id = ' . $new_page_id . ' не существует в системе;');
        }

        //$id1C = $relation -> getOldIdRelation($source_id, $old_page_id);

        if ($source_id && $id1C && $new_page_id) {
            $relation->setIdRelation($source_id, $id1C, $new_page_id);

            $element = umiHierarchy::getInstance()->getElement($new_page_id);
            if ($element instanceof umiHierarchyElement) {
                $element->setValue('1c_product_id', $id1C);
                $element->commit();
            }
            var_dump('ok');
        }
        exit('end');
    }

    public function info1COld() {
        return 'switch off';

        $xsltDom = new DomDocument;
        $xsltDom->resolveExternals = true;
        $xsltDom->substituteEntities = true;
        // $filePath - путь к xsl-шаблону трансформации.
        $filePath = CURRENT_WORKING_DIR . '/xsltTpls/old1c.xsl';
        //var_dump(file_get_contents($filePath));
        $xsltDom->load($filePath, DOM_LOAD_OPTIONS);

        $xslt = new xsltProcessor;
        $xslt->registerPHPFunctions();
        $xslt->importStyleSheet($xsltDom);

        $dom_new = new DOMDocument("1.0", "utf-8");
        // $xml - xml-данные для трансформации.
        $xml = file_get_contents(CURRENT_WORKING_DIR . '/files/tmp/import.xml');
        //var_dump($xml);
        //exit('aaa');
        $dom_new->loadXML($xml);

        //производим трансформацию
        $result = $xslt->transformToXML($dom_new);

        //html-данные необходимо включить в CDATA и в какой либо корневой узел.
        echo $result;
        exit('');
        $result = '<udata><![CDATA[' . $result . ']]></udata>';

        // данный принцип возвращения данных отключает xslt-трансформацию системой UMI.CMS
        return array('plain:result' => $result);
    }

    public function info1C() {
        return 'switch off';

        $categoryId = 269;
        $categoryId2 = 4;
        $level = 10;
        $products = new selector('pages');
        $products->types('hierarchy-type')->name('catalog', 'object');
        $products->where('hierarchy')->page($categoryId)->childs($level);
        $products->where('hierarchy')->page($categoryId2)->childs($level);
        $products->where('is_active')->equals(array(0, 1));
        $products->where('partner_event')->notequals(1);
        //$products->where('event_type')->equals(array(1278,1279,1282)); // Семинары ormco, Вебинар, Конференция
        $products->order('publish_date')->asc();

        $umiLinksHelper = umiLinksHelper::getInstance();
        $umiHierarchy = umiHierarchy::getInstance();
        $controller = cmsController::getInstance();
        $dataModule = $controller->getModule('data');
        foreach ($products as $page) {
            $item = array();
            $pageId = $page->getId();
            $name = $page->getName();
            $item['attribute:id'] = $pageId;
            $item['attribute:link'] = $link = $umiLinksHelper->getLinkByParts($page);

            $gorod_in = $page->gorod_in;
            $gorod_in_object = umiObjectsCollection::getInstance()->getObject($gorod_in);
            $is_active = ($page->getIsActive()) ? 1 : 0;
            $city = '';
            if ($gorod_in_object) {
                $item['attribute:city'] = $city = $gorod_in_object->name; // TODO city not relation, city is string
            }

            $event_type = $page->event_type;
            $event_type_object = umiObjectsCollection::getInstance()->getObject($event_type);
            $event_type_name = '';
            if ($event_type_object) {
                $item['attribute:event_type'] = $event_type_name = $event_type_object->name;
            }

            $publish_date = $page->publish_date;
            if ($publish_date) {
                $publish_date = $publish_date->getFormattedDate("U");
            }
            $finish_date = $page->finish_date;
            if ($finish_date) {
                $finish_date = $finish_date->getFormattedDate("U");
            }
            if ($finish_date && $publish_date) {
                $event_data = $dataModule->dateruPeriod($publish_date, $finish_date);
                $event_data_str = (isset($event_data['first_line'])) ? $event_data['first_line'] : '';
                $event_data_str .= (isset($event_data['second_line'])) ? ' ' . $event_data['second_line'] : '';
                $event_data_str .= (isset($event_data['days'])) ? ' (' . $event_data['days'] . ')' : '';
            } else {
                $event_data_str = 'дата не определена';
            }
            $item['attribute:date'] = $event_data_str;
            $item['node:text'] = $name;
            $items[] = $item;
            $umiHierarchy->unloadElement($pageId);
            $result_str .= "$pageId;$name;$event_type_name;$is_active;$city;$event_data_str;$link;\n";
        }

        $result['subnodes:lines'] = $items;
        echo $result_str;
        exit();
        return $result;
    }

    // вывод информацию о мероприятии на страницу с сообщением о проверке заказа менеджером
    public function showDiscountByDate($elementId) {
        $hierarchy = umiHierarchy::getInstance();

        $element = $hierarchy->getElement($elementId);
        if (!$element){
            return "0";
        }

        $discountFieldsCheck = array('data_okonchaniya_skidki_1' => 'skidka_ogranichena_datoj_1', 'data_okonchaniya_skidki_2' => 'skidka_ogranichena_datoj_2', 'data_okonchaniya_skidki_3' => 'skidka_ogranichena_datoj_3', 'data_okonchaniya_skidki_4' => 'skidka_ogranichena_datoj_4', 'data_okonchaniya_skidki_5' => 'skidka_ogranichena_datoj_5');
        $i = 0;
        foreach ($discountFieldsCheck as $discountDate => $discountCheck) {
            $i = $i + 1;
            $check = $element->getValue($discountCheck);
            if ($check) {
                $date = $element->getValue($discountDate);
                if (!$date){
                    continue;
                }
                $dateU = $date->getFormattedDate("U");

                if (time() < $dateU) {
                    $name = $element->getValue('nazvanie_skidki_' . $i);
                    $value = $element->getValue('stoimost_meropriyatiya_s_uchetom_skidki_' . $i);
                    return $value;
                }
            }
        }
        return 0;
    }

    // меняем покупателя у заказа
    public function changeCustomer($orderId = NULL, $newCustomerId = NULL) {
        //return 'switch off';
        if (!$orderId){
            return 'не найден заказ';
        }
        if (!$newCustomerId){
            return 'не найден пользователь';
        }
        $order = umiObjectsCollection::getInstance()->getObject($orderId);
        if (!$order){
            return 'не найден заказ';
        }
        $newCustomer = umiObjectsCollection::getInstance()->getObject($newCustomerId);
        if (!$newCustomer){
            return 'не найден пользователь';
        }
        $customer_id = $order->customer_id;
        $order->old_customer = $customer_id;
        $order->customer_id = $newCustomerId;
        $order->commit();

        return 'ok';
    }

    // меняем бонус у заказа
    /* public function changeBonus() {
      $orderId = 1111;
      $bonus = 0;

      $orderObj = umiObjectsCollection::getInstance() -> getObject($orderId);
      $orderObj -> setValue('bonus', $bonus);
      $orderObj -> commit();
      $order -> refresh();
      return 'ok';

      } */

    /* add legal, or save it
     * * legal id take from request info
     * * for example new legal : <input name="legal-person" value="new" />
     * * or legal with id = 1112 <input name="legal-person" value="1112" />
     * * may add ref_onsuccess url (page id or page url) by this <input type="hidden" name="ref_onsuccess" value="" />
     */
    public function legalAdd($template = 'default') {
        $collection = umiObjectsCollection::getInstance();
        $types = umiObjectTypesCollection::getInstance();
        $typeId = $types->getBaseType("emarket", "legal_person");
        $customer = customer::get();

        //$personId = getRequest('legal-person');
        $personId = getRequest('param0');
        $isNew = ($personId == null || $personId == 'new');
        if ($isNew) {
            $typeId = $types->getBaseType("emarket", "legal_person");
            $customer = customer::get();
            $personId = $collection->addObject("", $typeId);
            $customer->legal_persons = array_merge($customer->legal_persons, array($personId));
        }
        $controller = cmsController::getInstance();
        $data = getRequest('data');
        if ($data && $dataModule = $controller->getModule("data")) {
            $key = $isNew ? 'new' : $personId;
            $person = $collection->getObject($personId);
            $person->setName($data[$key]['name']);
            $dataModule->saveEditedObject($personId, $isNew, true);
        }

        $url = getRequest('ref_onsuccess');
        if ($url){
            $this->redirect($url);
        }

        $referer_url = $_SERVER['HTTP_REFERER'];
        $this->redirect($referer_url);
    }

    /*
	 * Удаление юридического лица
     */
    public function legal_delete($template = 'default') {
		$collection = umiObjectsCollection::getInstance();

		$person_id = getRequest('legal_person');
		$legal_person = $collection->getObject($person_id);

		if ($legal_person instanceof umiObject) {
			$customer = customer::get();
			$customer->legal_persons = array_diff($customer->legal_persons, array($person_id));
			$customer->commit();

			$collection->delObject($person_id);
		}

		$url = getRequest('ref_onsuccess');
		if ($url){
			$this->redirect($url);
		}

		$referer_url = $_SERVER['HTTP_REFERER'];
		$this->redirect($referer_url);
	}

	/* output legal list on template /tpls/emarket/payment/invoice/{template}.tpl
     */
    public function legalList($template = 'default') {
        /* list($tpl_block, $tpl_item) = def_module::loadTemplates("./tpls/emarket/payment/invoice/{$template}.tpl",
          'legal_person_block', 'legal_person_item'); */
        $tpl_block = $tpl_item = '';
        $collection = umiObjectsCollection::getInstance();
        $types = umiObjectTypesCollection::getInstance();
        $typeId = $types->getBaseType("emarket", "legal_person");
        $customer = customer::get();
        //var_dump($customer);
        //exit('aa');

        $items = array();
        $persons = $customer->legal_persons;
        if (is_array($persons))
            foreach ($persons as $personId) {
                $person = $collection->getObject($personId);
                $item_arr = array('attribute:id' => $personId, 'attribute:name' => $person->name);
                $items[] = def_module::parseTemplate($tpl_item, $item_arr, false, $personId);
            }
        if ($tpl_block) {
            return def_module::parseTemplate($tpl_block, array('items' => $items, 'type_id' => $typeId));
        } else {
            return array('attribute:type-id' => $typeId, 'xlink:href' => 'udata://data/getCreateForm/' . $typeId, 'items' => array('nodes:item' => $items));
        }
    }

    /* получения всех мероприятий оформленных с лендинга */
    public function allLandingEvents($curr_id = NULL) {
        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
        $sel->where('status_id')->isNull(false);
        $sel->where('name')->notequals('dummy');
        $sel->where('from_landing')->equals(1);
        $sel->where('page_id')->isnotnull(true);
        $sel->option('return')->value('page_id');
        $sel->group('page_id');

        $lines_arr = [];
        foreach ($sel as $object) {
            $lines_arr[$object['page_id']] = $object['page_id'];
        }
        ksort($lines_arr);
        $options = [];
        $hierarchy = umiHierarchy::getInstance();
        foreach ($lines_arr as $id) {
            $event = $hierarchy->getElement($id);
            $name = ($event) ? $event->name : $object->event_name;
            $selected = ($curr_id == $id) ? 'selected="selected"' : '';
            $options[] = "<option {$selected} value=\"{$id}\">({$id}) {$name}</option>";
        }
        return implode("\n", $options);
    }

    /* проверка существование файла */
    public function isOrmcoFile($path = NULL) {
        if (file_exists($path)) {
            return;
        }
        return 'http://ormco.ru';
    }

    /* выгрузка параметров лендинга для определенного мероприятия */
    public function exportLandingOrders($eventid = NULL) {
        $hierarchy = umiHierarchy::getInstance();
        $collection = umiObjectsCollection::getInstance();

        if (!$eventid){
            $eventid = getRequest('param0');
        }

        $event = $hierarchy->getElement($eventid);
        if (!($event instanceof umiHierarchyElement)) {
            $buffer = OutputBuffer::current('HTTPOutputBuffer');
            $buffer->charset("utf-8");
            $buffer->contentType("text/html");
            $buffer->push('no event id');
            $buffer->end();
        }

        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=event" . $eventid . ".csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
        $sel->where('status_id')->isNull(false);
        $sel->where('name')->notequals('dummy');
        $sel->where('from_landing')->equals(1);
        $sel->where('page_id')->equals($eventid);
        $total = $sel->length();

        $fp = fopen("php://output", "w");

        $title_arr = array('OrderId');

        $landing_csv_title = $event->landing_csv_title;
        if (!$landing_csv_title) {
            $landing_csv_title = 'mclasses1;mclasses2;mcl1;mcl2;p1;p2;file;events;email;lname;fname;father_name;bd;phone;country;region;city;who;other_specialization;company;payment_id;yurname;fio_rukovoditelya;dolzhnost;contact_person;yuremail;phone_number;fax;inn;kpp;account;bik;bank_account;ogrn;legal_address;defacto_address;postal_address;';
        }
        $landing_csv_title_arr_dirty = explode(";", $landing_csv_title);
        foreach ($landing_csv_title_arr_dirty as $value) {
            $landing_csv_title_arr[] = trim($value);
        }
        $title_arr = array_merge($title_arr, $landing_csv_title_arr);
        fputcsv($fp, $title_arr, ';');
        $lines_arr = $empty_orders = array();

        foreach ($sel as $object) {
            $line = $data_from_landing_arr_final = array();
            $line['attribute:id'] = $object->id;
            $line['node:text'] = $object->name;
            $lines_arr[] = $line;

            $data_from_landing = $object->data_from_landing;

            $data_from_landing_arr_new = array("OrderId" => $object->id);
            if (!$data_from_landing) {
                $data_from_landing_arr_new[] = iconv('utf-8', 'windows-1251//IGNORE', 'нет информации');
                fputcsv($fp, $data_from_landing_arr_new, ';');
                continue;
            }

            $data_from_landing_arr = explode("\n", $data_from_landing);
            foreach ($data_from_landing_arr as $param) {
                $pos = strpos($param, '=');

                if ($pos !== false) {
                    $title = substr($param, 0, $pos);
                    $param = substr($param, $pos + 1);
                    $param = strip_tags(htmlspecialchars_decode($param));

                    if ($title == 'events') {
                        $param = ($event) ? $event->name : $object->event_name;
                    }
                    if ($title == 'region' or $title == 'payment_id') {
                        $paramObject = $collection->getObject($param);
                        $param = ($paramObject) ? $paramObject->name : $param;
                    }
                    $param = iconv('utf-8', 'windows-1251//IGNORE', $param);
                    $data_from_landing_arr_new[$title] = $param;
                }
            }

            // вывод данных согласно указанной последовательности колонок
            foreach ($title_arr as $const_title) {
                $data_from_landing_arr_final[] = (isset($data_from_landing_arr_new[$const_title])) ? $data_from_landing_arr_new[$const_title] : '';
            }
            fputcsv($fp, $data_from_landing_arr_final, ';');
        }

        fclose($fp);
        exit('');
    }

    /* отзыв о качестве сервиса регистрации на мероприятие */
    public function add_vote_comment() {
        $orderId = $_REQUEST['orderId'];
        $starNumber = $_REQUEST['starNumber'];
        $comment = $_REQUEST['comment'];

        $order = umiObjectsCollection::getInstance()->getObject($orderId);
        if ($order) {
            $order->vote_comment = $comment;
            $order->vote_stars = $starNumber;
            $order->commit();
            return 1;
        }
        return;
    }

    /* выгрузка отзывов о заказах */
    public function exportFeedbacks() {
        $hierarchy = umiHierarchy::getInstance();
        $collection = umiObjectsCollection::getInstance();

        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=votes.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
        $sel->where('status_id')->isNull(false);
        $sel->where('name')->notequals('dummy');
        $sel->where('vote_stars')->isNull(false);
        $total = $sel->length();

        $title_arr = array('Order Id', 'Order Name', iconv('utf-8', 'windows-1251//IGNORE', 'Кол-во звезд'), iconv('utf-8', 'windows-1251//IGNORE', 'Комментарий'));
        $more_params_arr = array('vote_stars', 'vote_comment');

        $fp = fopen("php://output", "w");
        fputcsv($fp, $title_arr, ';');
        $lines_arr = array();
        foreach ($sel as $object) {
            $line = array();
            $line['attribute:id'] = $object->id;
            $line['node:text'] = $object->name;
            $lines_arr[] = $line;

            $data_from_landing_arr_new = array($object->id, iconv('utf-8', 'windows-1251//IGNORE', $object->name));
            foreach ($more_params_arr as $param) {
                $param = $object->getValue($param);
                $param = iconv('utf-8', 'windows-1251//IGNORE', $param);
                $data_from_landing_arr_new[] = $param;
            }
            fputcsv($fp, $data_from_landing_arr_new, ';');
        }

        fclose($fp);
        exit('');
    }

    /* atol send data */
    //public function atolSendDocument(order $order){
    /*public function atolSendDocument($orderId = NULL) {
        return 'switch off';
        file_put_contents(CURRENT_WORKING_DIR . '/traceajaxpawatolhand.txt', date('H:i:s') . " | start:\n", FILE_APPEND);

        $tokenid = self::atolGetToken();
        if (!$tokenid) {
            file_put_contents(CURRENT_WORKING_DIR . '/traceajaxpawatolhand.txt', date('H:i:s') . " | atolGetToken false:\n", FILE_APPEND);
            return;
        }

        $objects = umiObjectsCollection::getInstance();
        $kassaApiUrl = 'https://online.atol.ru/possystem';
        $kassaApiVersion = 'v3';
        $groupCode = 'orthodontia-ru_579';
        $url = $kassaApiUrl . "/" . $kassaApiVersion . "/";
        $method = $groupCode . "/sell";

        $inn = '7806182285';
        $callback_url = '';
        $paymentAddress = 'Москва, Волгоградский проспект, 42, к. 9';

        $sno = 'osn';
        $paymentType = 1;
        $tax = 'vat18';
        $taxMultiplier = 18;

        //=====orto-flex@mail.ru=====
        //uuid-text:85595e88-7bc6-4764-a8cc-ae99e4ba0e79
//          $external_id = 'atol-2105041992';
//          $name = 'Оплата за четвертый период обучения (Уровень 3)';
//          $email = 'orto-flex@mail.ru';
//          $static_price = 27000;
        //https://online.atol.ru/possystem/v3/orthodontia-ru_579/report/85595e88-7bc6-4764-a8cc-ae99e4ba0e79?tokenid=0ed9d8c5e4e048eabfd06b43fbde2e2e
//        ответ
//        {"uuid": "85595e88-7bc6-4764-a8cc-ae99e4ba0e79", "error": null, "status": "done", "payload": {"total": 27000, "fns_site": "www.nalog.ru", "fn_number": "8710000100944731", "shift_number": 69, "receipt_datetime": "07.09.2017 16:53:00", "fiscal_receipt_number": 3, "fiscal_document_number": 307, "ecr_registration_number": "0000887316046372", "fiscal_document_attribute": 704529899}, "timestamp": "07.09.2017 16:54:26", "group_code": "orthodontia-ru_579", "daemon_code": "prod-agent-6", "device_code": "KSR13.11-11-11", "callback_url": ""}


        //===== nastia.leonenko@mail.ru=====
        //uuid-text:f019d5c9-c417-4b38-964c-973ac6448adb
          $external_id = 'atol-2105042040';
          $name = 'Оплата за четвертый период обучения (Уровень 3)';
          $email = 'nastia.leonenko@mail.ru';
          $static_price = 27000;
        //https://online.atol.ru/possystem/v3/orthodontia-ru_579/report/f019d5c9-c417-4b38-964c-973ac6448adb?tokenid=0ed9d8c5e4e048eabfd06b43fbde2e2e
//        ответ
//        {"uuid": "f019d5c9-c417-4b38-964c-973ac6448adb", "error": null, "status": "done", "payload": {"total": 27000, "fns_site": "www.nalog.ru", "fn_number": "8710000100944731", "shift_number": 69, "receipt_datetime": "07.09.2017 16:56:00", "fiscal_receipt_number": 4, "fiscal_document_number": 308, "ecr_registration_number": "0000887316046372", "fiscal_document_attribute": 465432433}, "timestamp": "07.09.2017 16:56:58", "group_code": "orthodontia-ru_579", "daemon_code": "prod-agent-6", "device_code": "KSR13.11-11-11", "callback_url": ""}


        //===== mus4uk@gmail.com=====
        //uuid-text:e53c409b-5b54-4df9-b61c-f5f93151c87d
        $external_id = 'atol-2105079486';
        $name = 'Оплата за четвертый период обучения (Уровень 3), г. Минск';
        $email = 'mus4uk@gmail.com';
        $static_price = 27000;
        //https://online.atol.ru/possystem/v3/orthodontia-ru_579/report/e53c409b-5b54-4df9-b61c-f5f93151c87d?tokenid=0ed9d8c5e4e048eabfd06b43fbde2e2e

        $strName = (string) $name;
        $strName = preg_replace_callback('/u([0-9a-fA-F]{4})/', function ($match) {
            return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
        }, $strName);
        $name = str_replace('\\', '', $strName);
        $price = $sum = $itemsTotalSum = $static_price;
        $tax_sum = (double) round(round((($sum * $taxMultiplier) / (100 + $taxMultiplier)) * 100) / 100, 2);
        $price = $sum = $itemsTotalSum = (double) number_format($static_price, 2, '.', '');

        $data = array(
            'timestamp' => date('d.m.Y H:i:s'),
            'external_id' => $external_id,
            'service' => array(
                'inn' => $inn,
                'callback_url' => $callback_url,
                'payment_address' => $paymentAddress,
            ),
            'receipt' => array(
                'items' => array(
                    0 => array(
                        'name' => $name,
                        'price' => $price,
                        'quantity' => 1,
                        'tax' => $tax,
                        'sum' => $sum,
                        'tax_sum' => $tax_sum,
                    )
                ),
                'total' => $itemsTotalSum,
                'payments' => array(
                    array(
                        'sum' => $itemsTotalSum,
                        'type' => $paymentType,
                    ),
                ),
                'attributes' => array(
                    'sno' => $sno,
                    'email' => $email,
                    'phone' => '',
                ),
            ),
        );

        file_put_contents(CURRENT_WORKING_DIR . '/traceajaxpawatolhand.txt', date('H:i:s') . " | send Datas:\n" . print_r($data, true) . "\n", FILE_APPEND);

        $respond = self::sendHttpRequest($url, $method, $data, $tokenid);
        file_put_contents(CURRENT_WORKING_DIR . '/traceajaxpawatolhand.txt', date('H:i:s') . " | result send Datas:\n" . print_r($respond, true) . "\n", FILE_APPEND);

        $result = 'no';
        // пример ответа
        // {"uuid":"ea5991ab-05f3-4c10-980a-3b3f3d58ed13","timestamp":"18.05.2017 16:33:23","status":"wait","error":null}
        if ($respond) {
            $respondArray = @json_decode($respond, true);
            if (is_array($respondArray) && count($respondArray)) {
                foreach ($respondArray AS $respondItemKey => $respondItemValue) {
                    if ($respondItemKey == 'error' && (!$respondItemValue || $respondItemValue == 'null')) {
                        $result = 'error';
                    }
                }
            }
            //$orderObj = $objects->getObject($orderId);
            if (isset($respondArray['error']['text']) && $respondArray['error']['text'] != '') {
                $result = 'error-text:' . $respondArray['error']['text'];
            } else {
                $result = 'uuid-text:' . $respondArray['uuid'];
            }
        }
        var_dump($respond);
        var_dump(@json_decode($respond, true));
        var_dump($result);
        exit('hhhh');

        return $result;
    }*/

    private function sendHttpRequest($url, $method, $data, $tokenid = null) {
        // запрос надо сделать через curl
        $jsonData = json_encode($data);
        $operationUrl = $url . $method;
        if ($tokenid) {
            $operationUrl .= "?tokenid=" . $tokenid;
        }
        file_put_contents(CURRENT_WORKING_DIR . '/traceajaxpawatolhand.txt', date('H:i:s') . " | sendHttpRequest atolonline send pre:\n" . $operationUrl . "\n", FILE_APPEND);
        file_put_contents(CURRENT_WORKING_DIR . '/traceajaxpawatolhand.txt', date('H:i:s') . " | sendHttpRequest atolonline send data pre:\n" . print_r($jsonData, true) . "\n", FILE_APPEND);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $operationUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonData))
        );

        $result = curl_exec($ch);

        $res = curl_exec($ch);
        file_put_contents(CURRENT_WORKING_DIR . '/traceajaxpawatolhand.txt', date('H:i:s') . " | sendHttpRequest atolonline Response error:\n" . var_export(curl_error($ch), true) . "\n", FILE_APPEND);
        file_put_contents(CURRENT_WORKING_DIR . '/traceajaxpawatolhand.txt', date('H:i:s') . " | sendHttpRequest atolonline Response:\n" . print_r($result, true) . "\n", FILE_APPEND);

        curl_close($ch);
        return $result;
    }

    public function atolGetToken() {
        //https://online.atol.ru/possystem/v3/getToken?login=%3Clogin%3E&pass=%3Cpass%3E
        $login = 'orthodontia-ru';
        $pass = '3*cVJliH';
        $kassaApiUrl = 'https://online.atol.ru/possystem';
        $kassaApiVersion = 'v3';
        $method = 'getToken';

        $url = $kassaApiUrl . "/" . $kassaApiVersion . "/";

        $data = array("login" => $login, "pass" => $pass);

        $result = self::sendHttpRequest($url, $method, $data);
        file_put_contents(CURRENT_WORKING_DIR . '/traceajaxpawatolhand.txt', date('H:i:s') . " | atolGetToken result:\n" . print_r($result, true) . "\n", FILE_APPEND);

        $result = @json_decode($result, true);

        //$er_text = isset($result['text'])? $result['text'] : 'getToken error';
        //return (isset($result['token']) && $result['token']!='') ? $result['token'] : $er_text;
        return (isset($result['token']) && $result['token'] != '') ? $result['token'] : false;
    }

    // тестовое формирование информации для атол у конкретной задачи
    /*public function atolInfotest($orderId = NULL) {
        return 'switch off';
        //$atolinfo = self::atolInfo(654801);
        $orderId = 654801;

        if (!$orderId) {
            return;
        }

        $objects = umiObjectsCollection::getInstance();

        $tax = "1102"; //'vat18'; // 2474 id мероприятия с ндс 0  еще 2411 и 2294
        $taxMultiplier = 18;
        $itemsTotalSum = '';

        $order = order::get($orderId);
        $orderObj = $objects->getObject($orderId);

        //$customerEmail
        $customer_id = $order->getCustomerId();
        $customer = $objects->getObject($customer_id);
        $customerEmail = $customer->getValue('e-mail');
        if (!$customerEmail)
            $customerEmail = $customer->getValue('email');

        $orderItems = $order->getItems();
        $discount = 1;
        if ($order->getActualPrice() - $order->getDeliveryPrice() != $order->getOriginalPrice()) {
            $discount = ($order->getActualPrice() - $order->getDeliveryPrice()) / $order->getOriginalPrice();
        }
        foreach ($orderItems as $item) {
            $element = $item->getItemElement();
            $name = $item->getName();
            $price = (double) number_format($item->getTotalActualPrice() / $item->getAmount() * $discount, 2, '.', '');
            $quantity = (double) $item->getAmount();
            $vat = $tax;

            $elementId = $element->id;
            //2474 id мероприятия с ндс 0  еще 2411 и 2294
            if (in_array($elementId, array(2474, 2411, 2294))) {
                $vat = "1105"; // 1105 - НДС не облагается
            }

            $sum = (double) $quantity * $price;
            //$tax_sum = (double) round($sum * $taxMultiplier, 2);
            $tax_sum = (double) round(round((($sum * $taxMultiplier) / (100 + $taxMultiplier)) * 100) / 100, 2);

            $itemsTotalSum += $sum;
            $items[] = array(
                'n' => "Услуга семинар: " . $name,
                'p' => $price,
                'q' => $quantity,
                't' => $vat
            );
        }
        // if($order->getDeliveryPrice()){
        // $deliveryPrice = (double) number_format($order->getDeliveryPrice(), 2, '.', '');
        // $items[] = array(
        // 'n'=>'Доставка',
        // 'p'=>$deliveryPrice,
        // 'q'=>1,
        // 't'=>'none',
        // 'sum'=>$deliveryPrice,
        // 'tax_sum'=> '',
        // );
        // };

        $result = array(
            "customer" => $customerEmail,
            "items" => $items,
            "fio" => $customer->lname . ' ' . $orderObj->fname . ' ' . $orderObj->father_name
        );


//          array(
//          "customer" => "test@test.ru",
//          "items" =>
//          array(
//          array("n" => "product 1 name", "p" => "product 1 price", "q" => "product 1
//          quantity", "t" => "vatType"),
//          array("n" => "product 2 name", "p" => "product 2 price", "q" => "product 2
//          quantity", "t" => "vatType"),
//          ),
//          )

        return $jsonData = json_encode($result);
    }*/

    public function finishOrder() {
        return 'switch off';
        $orderId = 791357;
        $order = order::get($orderId);
        $order->order();

        return 'ok';
    }

    public function change1Cflag() {
        return 'switch off';
        $duplicate_id_arr = array(1223921, 1224189, 1224555, 1224987, 1225018, 1225022, 1225096, 1225135, 1225145, 1225380, 1225394, 1225400, 1225461, 1225526, 1225584, 1225590, 1225647, 1225712, 1225720, 1225740, 1225768, 1226040, 1226100, 1226380, 1226414, 1226438, 1226667, 1226717, 1226735, 1227532, 1228051, 1228063, 1228105, 1228154, 1225022, 1228193, 1228222, 1228232, 1228268);
        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
        $sel->where('id')->equals($duplicate_id_arr);

        $orders = $sel->result;
        foreach ($orders as $order) {
            $order->setValue('need_export', 1);
            $order->commit();

            var_dump($order->number . '-'. $order->need_export. '-'. $order->need_export_new);
        }
        var_dump($sel->total);
        exit('ok');
        return 'ok';
    }

    public function try_order_pay() {
        $payanyway_id = 17688;

        $order_number = getRequest('order_number');
        $order_id = getRequest('order_id');

        if (!empty($order_number)) {
            $orders = new selector('objects');
            $orders->types('object-type')->name('emarket', 'order');
            $orders->where('number')->equals($order_number);
            foreach ($orders as $order) {
                $order_id = $order->id;
                break;
            }
        }

        $order_obj = umiObjectsCollection::getInstance()->getObject($order_id);
        if ($order_obj instanceof umiObject && $order_obj->getTypeId() == 51) {
            // 232 - готов, 240 - отклонен, 54 - отменен
            if ($order_obj->getValue('status_id') == 232 || $order_obj->getValue('status_id') == 240 || $order_obj->getValue('status_id') == 54) {
                return array(
                    "attribute:order_number" => $order_obj->getValue('number'),
                    "error" => 'Данный заказ не может быть оплачен'
                );
            }

            // 307 - принята
            if ($order_obj->getValue('payment_status_id') == 307) {
                return array(
                    "attribute:order_number" => $order_obj->getValue('number'),
                    "error" => 'Данный заказ не может быть оплачен'
                );
            }

            // 1019451 - оплачено
            if ($order_obj->getValue('payment_status_1c') == 1019451) {
                return array(
                    "attribute:order_number" => $order_obj->getValue('number'),
                    "error" => 'Данный заказ не может быть оплачен'
                );
            }

            $order_obj->setValue('status_id', 174); // ожидает проверки - чтобы не отправлялось уведомление
            $order_obj->setValue('payment_id', $payanyway_id);
            $order_obj->commit();

            $order = order::get($order_id);
            if ($order instanceof order) {
                $payment = payment::get($payanyway_id, $order);
                $result = array(
                    "purchasing" => $payment->process()
                );
                $result["purchasing"]["attribute:stage"] = 'payment';
                $result["purchasing"]["attribute:step"] = 'payanyway';
                $result['attribute:order_id'] = $order_id;
                $result['attribute:order_number'] = $order_number;
                return $result;
            }
        }
    }


    // сохранение cert template в мероприятие
    public function cert_template_save(){
        //get event id
        $eventPid = isset($_REQUEST['id']) ? $_REQUEST['id'] : false;
        //get template
        $template = isset($_REQUEST['template']) ? $_REQUEST['template'] : false;
        //$template = str_replace(array('&lt;','&gt;'), array('<','>'), $template); // replace cert_template
        if($eventPid){
            $hierarchy = umiHierarchy::getInstance();
            $element = $hierarchy->getElement($eventPid);
            if($element instanceof umiHierarchyElement) {
                $old_mode = umiObjectProperty::$IGNORE_FILTER_INPUT_STRING;
                umiObjectProperty::$IGNORE_FILTER_INPUT_STRING = true;
                $element->cert_template = $template;
                $element->commit();
                umiObjectProperty::$IGNORE_FILTER_INPUT_STRING = $old_mode;
                return array('status'=>'successful','template'=>$template);
            }
        }

        return array('status'=>'error');

    }

    // формирование pdf на основе html шаблона и заказа
    public function cert_pdf_gen($orderId = NULL)
    {
        if (!$orderId) {
            $orderId = getRequest('param0');
        }

        if (!$orderId) {
            $result = 'Недостаточно данных';
            return $this->output_html($result);
        }

        $replace = $with = $replaceData = array();
        $orderFields = array(
            'event_name',
            'event_gorod_in',
            'event_data',
        );

        $customerFields = array(
            'fname',
            'lname',
            'father_name',
            'e-mail',
            'phone',
            'country'
        );

        $permissions = permissionsCollection::getInstance();
        $objects = umiObjectsCollection::getInstance();
        $hierarchy = umiHierarchy::getInstance();

        $order = $objects->getObject($orderId);



        if ($order instanceof umiObject) {


            //get order replace data
            foreach ($orderFields as $fieldName) {
                $replaceData[$fieldName] = $order->getValue($fieldName);
            }

            //get customer replace data
            $customer_id = $order->customer_id;
            $customer = $objects->getObject($customer_id);

            foreach ($customerFields as $fieldName) {
                $prepare_field_name = 'order_' . $fieldName;

                if($fieldName == 'e-mail'){
                    //$customerEmail
                    $customer_field_value = $customer->getValue('e-mail');
                    if (!$customer_field_value){
                        $customer_field_value = $customer->getValue('email');
                    }
                }else{
                    $customer_field_value = $customer->getValue($fieldName);
                }
                $replaceData[$prepare_field_name] = $customer_field_value;
            }

            //get event replace data
            $eventPid = $order->page_id;
            $element = $hierarchy->getElement($eventPid);
            if ($element instanceof umiHierarchyElement) {

                $replaceData['cert_template'] = $element->getValue('cert_template');
                $bg_file = $element->getValue('cert_bg');
                if ($bg_file) {
                    $replaceData['cert_bg'] = $bg_file->getFilePath(true);
                }
            } else {
                $result = 'Мероприятие не найдено';
                return $this->output_html($result);
            }
        } else {
            $result = "Не удалось получить объект.";
            return $this->output_html($result);
        }


        foreach($replaceData as $replaceValue => $withValue){
            $replace[] = '{' . $replaceValue . '}';
            $with[] = $withValue;
        }
// common cert template
$html = <<<END
<html>
	<head>
		<style>
    	html,body{
    		width: 100%;
			height: 100%;
			font-family: Arial;
    	}

		.drop {
			position: absolute;
		    top: 0;
		    left: 0;
			width: 1121px;
			height: 794px;
			background:url('/images/blank.gif') no-repeat center center;
			background-size: contain;
		}

		.drag {
		  position: absolute;
		}
    </style>
	</head>
	<body>
		{cert_template}
	</body>
</html>
END;
        $html = str_replace($replace, $with, $html); // replace cert_template
        $html = str_replace($replace, $with, $html); // replace other
        return $this->output_html($html);
    }

    // отдать html ответ
    public function output_html($result = NULL) {
        $buffer = outputBuffer::current();
        $buffer->charset('utf-8');
        $buffer->contentType('text/html');
        $buffer->clear();
        $buffer->push($result);
        $buffer->end();
    }

    // метод для 1С чтобы снять галочку с заказа "Выгружать новый заказ в 1С при следующем сеансе связи"
    public function unmarkNewOrderFlag($orderId) {
        $order = umiObjectsCollection::getInstance()->getObject($orderId);
        $result = array('status'=>'error', 'msg'=>'notFindOrder');
        if ($order) {
            $order->setValue('need_export_new', 0);
            $order->commit();

            SiteEmarketOrderModel::add1cLog($order->getId(), 'Деактивировано значение "Выгружать новый заказ в 1С при следующем сеансе связи"');

            $result = array('status'=>'successful', 'commonUID'=>$commonUID);
        }

        return $result;
    }

    /*создать новый справочник посетителей для данного мероприятия*/
    public function addVisitorsGuide($eventId = NULL){
        // проверить что такое мероприятие есть
        $hierarchy = umiHierarchy::getInstance();
        $objectTypes = umiObjectTypesCollection::getInstance();

        $event = $hierarchy -> getElement($eventId);
        if ($event instanceof umiHierarchyElement) {


            // проверить что у него еще нет привязанного справочника
            $visitorsGuideTid = $event->cert_person;
            if($visitorsGuideTid && $visitorsGuideTid!=''){
                $visitorsGuide = $objectTypes->getType($visitorsGuideTid);
                if($visitorsGuide instanceof umiObjectType) {
                    $referer = getServer('HTTP_REFERER');
                    return $this -> redirect($referer);
                }
            }

            // найти справочник родитель
            $visitorsGuideParentTid = 210;
            // создать дочерний тип данных и изменить название
            $eventName = $event->name;
            $visitorsGuideTid = $objectTypes->addType($visitorsGuideParentTid, "Посетители мероприяти: ".$eventName);

            // изменить галочки (использовать как справочник и общедоступный)
            $visitorsGuide = $objectTypes->getType($visitorsGuideTid);
            if($visitorsGuide instanceof umiObjectType) {
                $visitorsGuide->setIsGuidable(true);
                $visitorsGuide->setIsPublic(true);
                $visitorsGuide->commit();

                // сохранить id справочника в мероприятие
                $event->cert_person = $visitorsGuideTid;
                $event->commit();

                // редирект на справочник
                $this->redirect($this->pre_lang . "/admin/data/guide_items/" . $visitorsGuideTid . "/");
            }else{
                return 'ошибка создания списка посетителей';
            }
        }else{
            return 'мероприятие не найдено';
        }
    }


    /*загрузка пользователей на посетивших мероприятие, для функционала отправки писем для посетителей, без создания заказа*/
    public function updateEventVisitor($guideId = NULL) {

        $hierarchy = umiHierarchy::getInstance();
        $collection = umiObjectsCollection::getInstance();

        //$guideId = 263;
        if (!$guideId)
            $guideId = getRequest('param0');

        if (!$guideId)
            $guideId = getRequest('eventid');

        //TODO check guide id is exist
        /*$event = $hierarchy -> getElement($eventid);
        if (!($event instanceof umiHierarchyElement)) {
            $buffer = OutputBuffer::current('HTTPOutputBuffer');
            $buffer->charset("utf-8");
            $buffer->contentType("text/html");
               $buffer->push('no event id');
               $buffer->end();
        }*/



        if (!isset($_FILES['users-file'])) {
            $buffer = OutputBuffer::current('HTTPOutputBuffer');
            $buffer->charset("utf-8");
            $buffer->contentType("text/html");
            $buffer->push('File is not posted');
            $buffer->end();
        }

        $fileInfo = getArrayKey($_FILES, 'users-file');
        $name = getArrayKey($fileInfo, 'name');
        $tempPath = getArrayKey($fileInfo, 'tmp_name');
        $error = getArrayKey($fileInfo, 'error');
        $size = getArrayKey($fileInfo, 'size');

        if ($error) {
            $buffer = OutputBuffer::current('HTTPOutputBuffer');
            $buffer->charset("utf-8");
            $buffer->contentType("text/html");
            $buffer->push('Failed to upload file');
            $buffer->end();
        }


        $config = mainConfiguration::getInstance();
        $file = umiFile::manualUpload($name, $tempPath, $size, $config->includeParam('system.runtime-cache'));

        if (!($file instanceof iUmiFile) || $file->getIsBroken()) {
            $buffer = OutputBuffer::current('HTTPOutputBuffer');
            $buffer->charset("utf-8");
            $buffer->contentType("text/html");
            $buffer->push('Upload file is broken');
            $buffer->end();
        }

        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=update_visitors_" . $guideId . ".csv");
        header("Pragma: no-cache");
        header("Expires: 0");
        $fp = fopen("php://output", "w");
        $title_arr = array('Фамилия','Имя','Отчество', 'Email', 'Статус');
        $title_arr_1251 = array_map(function($val) {
            return iconv('UTF-8', 'CP1251', $val);
        }, $title_arr);

        fputcsv($fp, $title_arr_1251, ';');

        $handle = fopen($file->getFilePath(), "r"); //Открываем csv для чтения
        $i = 1;
        while (($line = fgetcsv($handle, 0, ";")) !== FALSE) {
            if($i == 1) {
                $i++;
                continue;
            }
            $i++;

            $last_name = isset($line[0]) ? iconv('CP1251', 'UTF-8', $line[0]) : '';
            $first_name = isset($line[1]) ? iconv('CP1251', 'UTF-8', $line[1]) : '';
            $father_name = isset($line[2]) ? iconv('CP1251', 'UTF-8', $line[2]) : '';
            // $last_name = isset($line[0]) ? $line[0] : '';
            // $first_name = isset($line[1]) ? $line[1] : '';
            // $father_name = isset($line[2]) ? $line[2] : '';
            $email = isset($line[3]) ? $line[3] : false;

            $updateStatus = 'no email';
            if($email){
                $sel = new selector('objects');
                $sel -> types('object-type') -> id($guideId);
                $sel -> where('name') -> equals($email);
                $total = $sel -> length();
                if($visitor = $sel->first){
                    $updateStatus = 'visitor in list';
                }else{
                    // create guide item
                    $visitorId = umiObjectsCollection::getInstance()->addObject($email, $guideId);

                    $visitor = $collection->getObject($visitorId);

                    $visitor->setValue("last_name", $last_name);
                    $visitor->setValue("first_name", $first_name);
                    $visitor->setValue("father_name", $father_name);

                    $visitor->commit();
                    $updateStatus = 'create visitor';


                }
            }
            fputcsv($fp, array(iconv('UTF-8', 'CP1251', $last_name),iconv('UTF-8', 'CP1251', $first_name),iconv('UTF-8', 'CP1251',$father_name),iconv('UTF-8', 'CP1251',$email),iconv('UTF-8', 'CP1251',$updateStatus)), ';');
        }
        fclose($handle); //Закрываем файл

        fclose($fp);
        exit('');


        /*$buffer = OutputBuffer::current('HTTPOutputBuffer');
            $buffer->charset("utf-8");
            $buffer->contentType("text/html");
               $buffer->push('successful');
               $buffer->end(); 	*/

        //return array('nodes:lost'=>$allowEmails);
        //var_dump($allowEmails);
        //exit('123');
        //$referer = getServer('HTTP_REFERER');
        //$this->redirect($referer);


    }

    // формирование pdf на основе html шаблона и информации о посетителе мероприятия (через справочник, а не через заказ) . Вызов функции из файла /pdf/certificats_visitor.php
    public function cert_pdf_gen_by_event($itemId = NULL) {
        $permissions = permissionsCollection::getInstance();
        $objects = umiObjectsCollection::getInstance();
        $hierarchy = umiHierarchy::getInstance();

        if (!$itemId){
            $itemId = getRequest('param0');
        }

        // проверяем что посетитель мероприятий является объектом
        $visitor = $objects->getObject($itemId);
        if ($visitor instanceof umiObject) {
            // найти мероприятие для данного посетителя

            // найти мероприятие, связанное со справочником (otid) содержащим информацию о посетителях
            $eventPid = $this->getEventByVisitor($visitor);

            // сформировать список данных
            $replace = $with = $replaceData = array();

            $element = $hierarchy->getElement($eventPid);
            if ($element instanceof umiHierarchyElement) {
                $replace = $with = $replaceData = array();

                $replaceData = array(
                    'order_fname' => $visitor -> first_name,
                    'order_lname' => $visitor -> last_name,
                    'order_father_name' => $visitor -> father_name,
                    'order_e-mail' => $visitor -> getName(),
                    'event_date_cert' => $element->event_date_cert,
                    'event_name' => $element->getName()
                );

                if($element->use_default_online_template_cert){
                    $settingsPid = 765; // id страницы настройки
                    $settings = $hierarchy->getElement($settingsPid);
                    if ($settings instanceof umiHierarchyElement) {
                        $replaceData['cert_template'] = $settings->getValue('cert_template');
                        $bg_file = $settings->getValue('cert_bg');
                        if ($bg_file) {
                            $replaceData['cert_bg'] = $bg_file->getFilePath(true);
                        }
                    } else {
                        $result = 'Общий шаблон онлайн мероприятий не найден';
                        return $this->output_html($result);
                    }
                }else{
                    $replaceData['cert_template'] = $element->getValue('cert_template');
                    $bg_file = $element->getValue('cert_bg');
                    if ($bg_file) {
                        $replaceData['cert_bg'] = $bg_file->getFilePath(true);
                    }
                }


            } else {
                $result = 'Мероприятие не найдено';
                return $this->output_html($result);
            }
        } else {
            $result = "Не удалось получить объект.";
            return $this->output_html($result);
        }

        foreach($replaceData as $replaceValue => $withValue){
            $replace[] = '{' . $replaceValue . '}';
            $with[] = $withValue;
        }

// common cert template
        $html = <<<END
<html>
	<head>
		<style>
    	html,body{
    		width: 100%;
			  height: 100%;
			  font-family: Arial;
    	}

			.drop {
			  position: absolute;
		    top: 0;
		    left: 0;
			  width: 1121px;
			  height: 794px;
			  background:url('/images/blank.gif') no-repeat center center;
			  background-size: contain;
			}

			.drag {
			  position: absolute;
			}
    </style>
	</head>
	<body>
		{cert_template}
	</body>
</html>
END;
        $html = str_replace($replace, $with, $html); // replace cert_template
        $html = str_replace($replace, $with, $html); // replace other
        return $this->output_html($html);

    }

    // пройтись по списку посетителей и у тех, у кого не отправлен письмо со ссылкой на сертификат (через справочник, а не через заказ)
    public function send_sert_for_visitors($guideId = NULL) {

        $hierarchy = umiHierarchy::getInstance();
        $objects = umiObjectsCollection::getInstance();

        //$guideId = 263;
        if (!$guideId)
            $guideId = getRequest('param0');

        if (!$guideId)
            $guideId = getRequest('eventid');

        $sel = new selector('objects');
        $sel -> types('object-type') -> id($guideId);
        $sel -> where('send_sert') -> notequals(1);
        $total = $sel -> length();
        foreach($sel as $visitor){
            $this->send_sert_for_visitor_item($visitor);
        }
        $referer = getServer('HTTP_REFERER');
        $this->redirect($referer);
    }

    // отправка письмо со ссылкой на сертификат посетителю (через справочник, а не через заказ)
    public function send_sert_for_visitor_item($visitor = NULL) {
        $hierarchy = umiHierarchy::getInstance();

        if (!($visitor instanceof umiObject)) {
            return 'wrong visitor';
        }

        // найти мероприятие, связанное со справочником (otid) содержащим информацию о посетителях
        $eventPid = $this->getEventByVisitor($visitor);


        // сформировать список данных
        $replace = $with = $replaceData = array();

        $element = $hierarchy->getElement($eventPid);
        if ($element instanceof umiHierarchyElement) {
            $replace = $with = $replaceData = array();

            $order_email = $visitor -> getName();
            $order_fname = $visitor -> last_name . ' ' . $visitor -> first_name . ' ' . $visitor -> father_name;

            $replaceData = array(
                'order_id' => $visitor->id,
                'order_fname' => $visitor -> first_name,
                'order_lname' => $visitor -> last_name,
                'order_father_name' => $visitor -> father_name,
                'order_e-mail' => $order_email,
                'event_date_cert' => $element->event_date_cert,
                'event_name' => $element->getName()
            );



            $visitor -> send_sert = 1;
            $visitor -> commit();

            // <p>Материалы мероприятия: <br/><a href="ссылка на материалы">Скачать материалы</a></p>
            $content = <<<END
		Здравствуйте!

<p>Вы участвовали в мероприятии "{event_name}"</p>
<p>Ваш сертификат участника: <br/><a href="https://orthodontia.ru/pdf/certificats_visitor.php?id={order_id}">Открыть сертификат</a></p>
<p>Спасибо за то, что выбираете обучение с Ormco! До новых встреч!</p>
END;


            foreach($replaceData as $replaceValue => $withValue){
                $replace[] = '{' . $replaceValue . '}';
                $with[] = $withValue;
            }

            $content = str_replace($replace, $with, $content); // replace data

            $cmsController = cmsController::getInstance();
            $domains = domainsCollection::getInstance();
            $domainId = $cmsController -> getCurrentDomain() -> getId();

            $regedit = regedit::getInstance();
            $fromMail = "noreply@ormcomail.ru";//$regedit -> getVal("//modules/emarket/from-email/{$domainId}");
            $fromName = $regedit -> getVal("//modules/emarket/from-name/{$domainId}");

            $letter = new umiMail();

            $letter -> addRecipient($order_email, $order_fname);
            $letter -> setFrom($fromMail, $fromName);
            $letter -> setSubject('Сертификат об участии');
            $letter -> setContent($content);
            $letter -> commit();
            $letter -> send();

            return 'successful';
        }

        return 'wrong send';

    }

    // получить мероприятие по посетителю (через справочник, а не через заказ)
    public function getEventByVisitor($visitor = NULL){

        if ($visitor instanceof umiObject) {
            $visitorTid = $visitor->getTypeId();

            $events = new selector('pages');
            $events -> types('hierarchy-type') -> name('catalog', 'object');
            $events -> where('cert_person') -> equals($visitorTid);
            $events -> where('is_active') -> equals(array(0, 1));

            if ($event = $events->first) {
                return $event->id;
            }
        }
        return ;

    }

    // получить значение выпадающего списка по его id
    public function get_relation_item_name($item_id = NULL){
        $item_object = umiObjectsCollection::getInstance()->getObject($item_id);
        if ($item_object instanceof umiObject) {
            return $item_object->name;
        }
        return ;
    }
}