<?php
class emarket_orders_clear extends def_module {

	/**
	 * Очистка устаревших незарегистрированных пользователей - аналог того, что по крону, только быстрее и без заморозки. По крону видимо не успевает
	 * Запускается через http://ortho-test1.dpromo.su/udata/emarket/clear_empty_non_users
	 */
	public function clear_empty_non_users() {
		$start = 0;
		$limit = 500;

		$customerTypeId = umiObjectTypesCollection::getInstance()->getTypeIdByGUID('emarket-customer');
        $expiration = umiObjectsExpiration::getInstance();
		$customers = $expiration->getExpiredObjectsByTypeId($customerTypeId, 1000000);

		sort($customers);
		echo "Total customers = " . sizeof($customers);
		echo "\nStart = " . $start;
		echo "\nLimit = " . $limit;

		$customers = array_slice($customers, $start, $limit);

		$orders = array();
		foreach($customers as $customer_id){
			$orders[$customer_id]['customer_id'] = $customer_id;
		}

		echo "\n\nCustomers = " . sizeof($customers);
//		print_r($customers);
        $customers[] = 0;

		$sql = "SELECT o.id as id, oc_122_lj.rel_val as user_id FROM cms3_object_types t, cms3_objects o LEFT JOIN cms3_object_content oc_122_lj ON oc_122_lj.obj_id=o.id AND oc_122_lj.field_id = '122' WHERE o.type_id IN (51) AND t.id = o.type_id AND oc_122_lj.rel_val in (" . implode(',', $customers) . ") group by o.id order by o.id ";

		$connection = ConnectionPool::getInstance()->getConnection();
		$result = $connection->queryResult($sql);
		$result->setFetchType(IQueryResult::FETCH_ASSOC);

		$count = 0;
		foreach($result as $row){
			$orders[$row['user_id']]['orders'][] = $row['id'];
			$count++;
//			echo $row['id'] . "=" . $row['user_id'] . "\n";
		}

		echo "\nOrders = " . $count . "\n";

		$count = 0;

		$collection = umiObjectsCollection::getInstance();
		foreach($orders as $id_user => $user_details){
            echo $id_user;
			if(!isset($user_details['orders']) || sizeof($user_details['orders']) == 0){
                echo " - DIE\n";
				$customer = $collection->getObject($id_user);
				if($customer instanceof umiObject){
					$orders[$id_user]['name'] = $customer->getName();
					$deliveryAddresses = $customer->delivery_addresses;
					if (!is_null($deliveryAddresses) && is_array($deliveryAddresses) && sizeof($deliveryAddresses) > 0) {
						foreach($deliveryAddresses as $addressId) {
							$objects->delObject($addressId);
						}
					}
					$customer->delete();

					$count++;
				}
            }else{
                echo " - FREEZE\n";
                $customer_customer = new customer($collection->getObject($id_user));
                $customer_customer->freeze();
                foreach($user_details['orders'] as $order_id) {
                    $order = $collection->getObject($order_id);
                    if($order instanceof umiObject){
                        if (is_null($order->status_id)) {
                            if (!$expiration->isExpirationExists($order->getId())) {
                                $expiration->add($order->getId());
                            }
                        }
                    }
                }
			}
		}
		echo "\nDeleted users = " . $count . "\n";

		die();
	}

	/**
	 * Очистка устаревших неоформленных заказов в корзине от незарегистрированных пользователей
	 */
	public function clear_non_user_orders() {
		$start = 0;
		$limit = 100;

		$orderTypeId = umiObjectTypesCollection::getInstance()->getTypeIdByGUID('emarket-order');
		$expiration = umiObjectsExpiration::getInstance();
		$orders = $expiration->getExpiredObjectsByTypeId($orderTypeId, 1000000);

		sort($orders);
		echo "Total orders = " . sizeof($orders);
		echo "\nStart = " . $start;
		echo "\nLimit = " . $limit;

		$orders = array_slice($orders, $start, $limit);

		$count = 0;
		if (sizeof($orders) > 0) {
			$objects = umiObjectsCollection::getInstance();
			foreach($orders as $orderId) {
				$order = $objects->getObject($orderId);
				if (is_null($order->status_id)) {
					$order = order::get($orderId);
					$items = $order->getItems();
					foreach($items as $item) {
						$orderItem = orderItem::get($item->getId());
						$orderItem->remove();
					}
					$customerId = $order->customer_id;
					if (!$expiration->isExpirationExists($customerId)) {
						$expiration->add($customerId);
					}
					$order->delete();
					$count++;
                }else{
                    $expiration->clear($orderId);
                }
			}
		}
		echo "\nDeleted orders = " . $count . "\n";
		die();
	}
}