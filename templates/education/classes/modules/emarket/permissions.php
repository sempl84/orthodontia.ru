<?php

$permissions = array(
    'purchasing' => array(
        'showDiscountByDate',
        'registerEventOneClick',
        'eventId', 'eventInfo', 'eventAddInfo', 'eventOrderNum', 'eventOrderPdf', 'buhgalt_order_number', 'eventFieldValue',
        'lporder', 'receiptt', 'invoicee',
        'receiptPrint', 'invoiceePrint',
        'simplePaymentsList',
        'legalList', 'legalAdd',
        'isOrmcoFile',
        'checkSecondPurchase',
        'is_user_exist', 'is_first_purchase', 'logout_user', 'get_user_info', 'add_legal_item', 'forget_pas', 'lporderSmart', 'avaliable_reserv',
        'legalListNew',
        'show_promokod', 'check_promokod',
        'add_vote_comment',
        'payment_status_1c',
        'finishOrder',
        'try_order_pay',
        'cert_pdf_gen','cert_template_save',
        'cert_pdf_gen_by_event','getEventByVisitor',
        'lporderSmartFast', 'lporderSmartFaster',
		'submit_consultation_order_form',
        'clear_empty_non_users',
        'clear_non_user_orders',
    ),
    'personal' => array(
        'eventsList', 'legalList', 'legalAdd', 'legal_delete'
    ),
    'control' => array(
        'applyEventDiscount', 'applyOrder'
    )
);
