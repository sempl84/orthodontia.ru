<?php

class emarket_custom_import_orders_payment_status extends def_module
{
    
    /*импорт скидко пользователей по uid из 1С*/
    public function payment_status_1c()
    {
        header('Content-Type: application/json; charset=utf-8');
        
        $input = json_decode(file_get_contents('php://input'), true);
        
        //$content = '{"Заказы": [ {   "Заказ": {   "ИД": "СУ001017497" , "СтатусОплаты": "оплачено" } }]}';
        
        //$input = json_decode($content, true);
        file_put_contents(CURRENT_WORKING_DIR . '/traceajaxOrders.txt', "\n" . date("c") . "\n", FILE_APPEND);
        file_put_contents(CURRENT_WORKING_DIR . '/traceajaxOrders.txt', print_r(file_get_contents('php://input'), true) . "\n", FILE_APPEND);
//        var_dump((array)json_decode(file_get_contents('php://input'), true));
        //exit('ggg');
        
        $result_arr = $result_common = array();
        if (json_last_error() == JSON_ERROR_NONE) {
            $input = (array)$input;
            if (isset($input['Заказы'])) {
                //var_dump($input['Заказы']);
                //exit('aaa');
                $umiObjectsCollection = umiObjectsCollection::getInstance();
                $hierarchy = umiHierarchy::getInstance();
                
                foreach ((array)$input['Заказы'] as $orderData) {
                    $resultInfo = $resultError = array();
                    $orderData = (array)$orderData['Заказ'];
                    //var_dump($orderData);
                    //exit('aaa');
                    $order1CId = $orderData["ИД"];
                    $orderIdTmp = str_replace(array('\u0421\u0423', 'СУ'), '', $order1CId);
                    $orderIdTmp = ltrim($orderIdTmp, '0');
                    $orderId = $orderIdTmp;
                    $orderPaymentStatus1C = $orderData["СтатусОплаты"];
                    
                    // check isset order
                    $order = $umiObjectsCollection->getObject($orderId);
                    $paymentStatus = 'notfound';
                    if ($order instanceof umiObject) {
                        $resultInfo['order1CId'] = $order1CId;
                        $resultInfo['orderId'] = $orderId;
                        
                        // $order->payment_status_1C = 1019453;// "на модерации"
                        switch ($orderPaymentStatus1C) {
                            case 'вОплате':
                                $order->payment_status_1C = 1019450; // "ожидает оплаты",
//                                if($order->na_moderacii == 1){
//                                    $this->applyOrder($orderId); // одобрить оплату заказа и отправить письмо клиенту
//                                }
                                break;
                            
                            case 'Оплачен':
                                $order->payment_status_1C = 1019451; //"оплачено"
                                // выставляет отметку что оплата принята
                                $order->payment_status_id = 307; // 307 - статус оплаты в заказе: оплата принята
//                                $order->payment_status_id = 45; // 45 - статус заказа: принят
                                break;
                            
                            default:
                                $resultError['NotValidPaymentStatus'] = $orderPaymentStatus1C;
                                break;
                        }
                        
                        $order->setValue(SiteEmarketOrderModel::field_event_participation, getArrayKey($orderData, 'ПрисутствиеНаСеминаре') == 1);
                        $order->commit();
                    } else {
                        $resultError['NotFoundOrder'] = 'Not found order ID = ' . $orderId;
                    }
                    
                    if (sizeof($resultError) > 0) {
                        $resultError['order1CId'] = $order1CId;
                        $resultError['orderId'] = $orderId;
                        $result = array('status' => 'error', 'msg' => $resultError);
                    } else {
                        $result = array('status' => 'successfull', 'msg' => $resultInfo);
                    }
                    
                    $result_arr[] = $result;
                }
                
                $result_common = array('status' => 'successfull', 'orders' => $result_arr);
            }
        } else {
            $resultError['JsonEr'] = 'Json format error';
            $result_common = array('status' => 'error', 'msg' => $resultError);
        }
        
        
        //var_dump($result_common);
        //exit('aaa');
        
        $buffer = outputBuffer::current('HTTPOutputBuffer');
        $buffer->charset('utf-8');
        $buffer->contentType('application/json');
        $buffer->push(json_encode($result_common));
        $buffer->end();
    }
}