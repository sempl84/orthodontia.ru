<?php
class emarket_custom_lp_api extends def_module {

	// Проверка наличия пользователя в базе
	public function is_user_exist($email = NULL) {
		if (!$email)
			$email = getRequest('param0');
		if (!$email)
			return 0;

		file_put_contents(CURRENT_WORKING_DIR . '/traceajaxlp.txt', "\n\n is_user_exist" . print_r($_REQUEST, true) . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);

		$existUser = new selector('objects');
		$existUser -> types('object-type') -> name('users', 'user');
		$existUser -> where('login') -> equals($email);
		$existUser -> option('no-length') -> value(true);
		$existUser -> option('return') -> value('id');
		$existUser -> limit(0, 1);

		return ($existUser -> result()) ? 1 : 0;

	}

	/*Проверка наличия регистрации пользователя на мероприятие*/
	public function is_first_purchase($email = NULL, $eventid = NULL) {
		if (!$email)
			$email = getRequest('param0');
		if (!$eventid)
			$eventid = getRequest('param1');
		if (!$email or !$eventid)
			return 1;

		// кастомизация для лендинга с форумом 2018
		$forumEventIds = array(1647,1648,1649,1650,1651,1652,1653);
		$isForumEvent = (in_array($eventid, $forumEventIds)) ? 1:0;
		// кастомизация для лендинга с форумом 2018

		$sel = new selector('objects');
		$sel -> types('object-type') -> name('emarket', 'order');
		$sel -> where('status_id') -> isNull(false);
		$sel -> where('name') -> notequals('dummy');
		$sel -> where('order_e-mail') -> equals($email);

		//$sel -> where('page_id') -> equals($eventid);
		// кастомизация для лендинга с форумом 2018
		$isForumEvent = 0; // временно отключаем кастыль для форума 2018 СПб
		if($isForumEvent == 1){
			$sel -> where('page_id') -> equals($forumEventIds);
		}else{
			$sel -> where('page_id') -> equals($eventid);
		}

		$sel -> option('no-length') -> value(true);
		$sel -> option('return') -> value('id');
		$sel -> limit(0, 1);

		//file_put_contents(CURRENT_WORKING_DIR . '/tracelpcheck.txt', print_r($_REQUEST['path'], true) ."\nresul:". $result ."\ndate:". date('Y-n-j H:i:s') . "\n\n", FILE_APPEND);
		//return ($sel -> result()) ? 1 : 0;

		if($first = $sel -> result){
			$result = array('result'=>"1", 'order-id'=>$first[0]['id']);
		}else{
			$result = array('result'=>"0");
		}
		return $result;
	}

	/*logout users*/
	public function logout_user() {
		$users_module = $this -> cmsController -> getModule("users");
		if (!$users_module) return array('result'=>0, 'error'=>'no users module');
		if($users_module->is_auth()){
			$session = session::getInstance();

			$session->del(array(
				'cms_login', 'cms_pass', 'user_id',
				'u-login', 'u-password', 'u-password-md5', 'u-session-id'
			));

			setcookie('u-login', "", time() - 3600, "/");
			setcookie('u-password', "", time() - 3600, "/");
			setcookie('u-password-md5', "", time() - 3600, "/");

			setcookie(ini_get('session.name'), "", time() - 3600, "/");
			system_removeSession();

			setcookie ('eip-panel-state-first', "", time() - 3600, "/");

			session::destroy();
			return 1;
		}else{
			return array('result'=>0, 'error'=>'not auth');
		}

	}
	/*Подгрузка данных пользователя*/
	public function get_user_info($email = NULL, $password = NULL) {
		if (!$email) $email = getRequest('param0');
		if (!$password) $password = getRequest('param1');
		if (!$email || !$password) return array('result'=>0, 'error'=>'empty email or password');

		$extendedGroups = array('short_info','short_info_more', SiteUsersUserModel::group_address);
		//$extendedGroups = array('short_info','short_info_more');

		$login = $email;

		$permissions = permissionsCollection::getInstance();
		$cmsController = cmsController::getInstance();
		$users_module = $this -> cmsController -> getModule("users");
		if (!$users_module) return array('result'=>0, 'error'=>'no users module');

		$user = $permissions -> checkLogin($login, $password);
		//var_dump($login);
		//var_dump($password);
		//var_dump($user);
		//exit('ggg');
		if ($user instanceof iUmiObject) {
			$permissions -> loginAsUser($user);


			// login
			$session = session::getInstance();

			if ($permissions -> isAdmin($user -> id)) {
				$session -> set('csrf_token', md5(rand() . microtime()));
				if ($permissions -> isSv($user -> id)) {
					$session -> set('user_is_sv', true);
				}
			}

			$session -> setValid();
			session::commit();
			system_runSession();

			$def_value = translatorWrapper::$showEmptyFields;
			translatorWrapper::$showEmptyFields = true;
			$data = translatorWrapper::get($user) -> translate($user);

			$data['extended'] = array();
			$data['extended']['groups'] = array();
			$data['extended']['groups']['nodes:group'] = array();

			$objectType = $user -> getType();

			foreach ($extendedGroups as $extendedGroup) {
				$group = $objectType -> getFieldsGroupByName($extendedGroup);
				if (!$group instanceof umiFieldsGroup)
					continue;

				$data['extended']['groups']['nodes:group'][] = translatorWrapper::get($group) -> translateProperties($group, $user);
			}
			return $data;
			translatorWrapper::$showEmptyFields = $def_value;

		}
		return array('result'=>0, 'error'=>'wrong email or password');

	}

	/*Получение списка  юр. лицо*/
	public function legalListNew($template = 'default') {
		$permissions = permissionsCollection::getInstance();
		$currentUserId = $permissions->getUserId();

		return array('currentUserId' => $currentUserId);
	}

	/*Добавление нового юр. лицо*/
	public function add_legal_item() {
		$users_module = $this -> cmsController -> getModule("users");
		if (!$users_module) return array('result'=>0, 'error'=>'no users module');

		if($users_module->is_auth()){
			$collection = umiObjectsCollection::getInstance();
			$types = umiObjectTypesCollection::getInstance();

			$isNew = true;
			$typeId = $types -> getBaseType("emarket", "legal_person");

			$personId = $collection -> addObject("", $typeId);

			$controller = cmsController::getInstance();
			$data = getRequest('data');
			if ($data && $dataModule = $controller -> getModule("data")) {
				$key = $isNew ? 'new' : $personId;
				$person = $collection -> getObject($personId);
				$person -> setName($data[$key]['name']);

				$dataModule -> saveEditedObjectWithIgnorePermissions($personId, $isNew, true);
				$customer = customer::get();
				$customer -> legal_persons = array_merge($customer -> legal_persons, array($personId));
			}

			return $personId;
		}
		return 0;
	}

	/*проверка повторной регистрации оставленно для совместимости*/
	public function checkSecondPurchase($eventid = NULL, $email = NULL) {
            if (!$eventid){
                $eventid = getRequest('param0');
            }
            if (!$email){
                $email = getRequest('param1');
            }

            $sel = new selector('objects');
            $sel -> types('object-type') -> name('emarket', 'order');
            $sel -> where('status_id') -> isNull(false);
            $sel -> where('name') -> notequals('dummy');
            $sel -> where('order_e-mail') -> equals($email);
            $sel -> where('page_id') -> equals($eventid);
            $sel -> limit(0, 1);

            if ($sel -> first) {
                $result = 0;
            } else {
                $result = 1;
            }

            //file_put_contents(CURRENT_WORKING_DIR . '/tracelpcheck.txt', print_r($_REQUEST['path'], true) ."\nresul:". $result ."\ndate:". date('Y-n-j H:i:s') . "\n\n", FILE_APPEND);
            return $result;
	}
	/*восстановление пароля*/
	public function forget_pas($forget_email = NULL) {
		if(strlen($forget_email) == 0) return array('status'=>'error', 'result'=>'no email');
		$user_id = false;
		$template = 'default';

		$sel = new selector('objects');
		$sel->types('object-type')->name('users', 'user');
		$sel->where('e-mail')->equals($forget_email);
		$sel->limit(0, 1);

		$user_id = ($sel->first) ? $sel->first->id : false;

		if ($user_id) {
			list($template_mail_verification, $template_mail_verification_subject) = def_module::loadTemplatesForMail("users/forget/".$template, "mail_verification", "mail_verification_subject");

			$activate_code = md5(self::getRandomPassword());

			$object = umiObjectsCollection::getInstance()->getObject($user_id);

			$regedit = regedit::getInstance();

			$object->setValue("activate_code", $activate_code);
			$object->commit();

			$email = $object->getValue("e-mail");
			$fio = $object->getValue("lname") . " " . $object->getValue("fname") . " " . $object->getValue("father_name");

			$email_from = $regedit->getVal("//settings/email_from");
			$fio_from = $regedit->getVal("//settings/fio_from");

			$mail_arr = Array();
			$mail_arr['domain'] = $domain = $_SERVER['HTTP_HOST'];
			$mail_arr['restore_link'] = getSelectedServerProtocol() . "://" . $domain . $this->pre_lang . "/users/restore/" . $activate_code . "/";
			$mail_arr['login'] = $object->getValue('login');
			$mail_arr['email'] = $object->getValue('e-mail');

			$mail_subject = def_module::parseTemplateForMail($template_mail_verification_subject, $mail_arr, false, $user_id);
			$mail_content = def_module::parseTemplateForMail($template_mail_verification, $mail_arr, false, $user_id);

			$someMail = new umiMail();
			$someMail->addRecipient($email, $fio);
			$someMail->setFrom($email_from, $fio_from);
			$someMail->setSubject($mail_subject);
			$someMail->setPriorityLevel("highest");
			$someMail->setContent($mail_content);
			$someMail->commit();
			$someMail->send();

			$block_arr = Array();
			$block_arr['attribute:status'] = "success";
			return array('status'=>'success', 'restore_link'=> $mail_arr['restore_link']);//$macrosResult = def_module::parseTemplate($template_forget_sended, $block_arr);
		}
		return array('status'=>'error', 'result'=>'no user');
	}

	public static function getRandomPassword ($length = 12) {
		$length = 6;
		$avLetters = "1234567890qwertyuiopasdfghjklzxcvbnm";

		$size = strlen($avLetters);

		$npass = "";
		for($i = 0; $i < $length; $i++) {
			$c = rand(0, $size - 1);
			$npass .= $avLetters[$c];
		}
		return $npass;
	}

    public function lporderSmart() {
        // заглушка для vds
        //$this -> redirect('/event_mobile_reg/?stub_vds=1');
        // end заглушки
        $referer = getServer('HTTP_REFERER');
        $json_data = getRequest('json'); // param to return array without redirect
        $json = ($json_data == 1) ? true : false;

        $hierarchy = umiHierarchy::getInstance();
        $controller = cmsController::getInstance();

        // если покупатель не авторизован редиректим обратно
        $users_module = $controller->getModule("users");
        if ($users_module and ! $users_module->is_auth()) {
            if($json){
                return array(
                    'status'=>'error',
                    'message'=>'user not authorized'
                );
            }
            return $this->redirect($referer);
        }

        $page_id = getRequest('events');
		//Email
		$email = getRequest('email');

        // проверить не повторный ли это заказ
        $is_first_purchase = $this->is_first_purchase($email, $page_id);
        if($is_first_purchase['result'] == 1){
            if($json){
                return array(
                    'status'=>'error',
                    'message'=>'has order yet'
                );
            }
            return $this->redirect($referer);
        }

        //if(!$page_id) // TODO show error
        //var_dump($_REQUEST);
        file_put_contents(CURRENT_WORKING_DIR . '/traceajaxlp.txt', "\n\n" . print_r($_REQUEST, true) . "\n" . date('Y-n-j H:i:s') . "\n", FILE_APPEND);
        //var_dump('step1');
        $element = $hierarchy->getElement($page_id);
        $isCatalogItem = false;
        if ($element instanceof umiHierarchyElement) {
			if($element->getIsActive() != true){
				if($json){
					return array(
						'status'=>'error',
						'message'=>'event is not active'
					);
				}
				return $this->redirect($referer);
			}

            $hierarchyType = umiHierarchyTypesCollection::getInstance()->getTypeByName("catalog", "object");
            $hierarchyTypeId = $hierarchyType->getId();

            if($element->getTypeId() == $hierarchyTypeId){
                $isCatalogItem = true;
            }
        }
        if ($isCatalogItem) {
            $order = $this->getBasketOrder(false);

            // проверяем не перезаписывается ли существующий заказ
            $orderNumber = $order->getNumber();
            if ($orderNumber > 1) {
                $order->order();
            }
            $order = $this->getBasketOrder(false);

            // добавляем товар
            $_REQUEST['no-redirect'] = 1;
            $this->basket('put', 'element', $page_id);

            //очищаем корзину от лишних товаров
            foreach ($order->getItems() as $orderItem) {
                $orderItemElementId = $orderItem->getItemElement()->id;
                if ($orderItemElementId != $page_id) {
                    $order->removeItem($orderItem);
                } else {
                    if ($orderItem->getAmount() > 1) {
                        $orderItem->setAmount(1);
                        $orderItem->refresh();
                    }
                }
            }
            $order->bonus = 0;
            $order->refresh();
            //return '-';
            // if($_SERVER['REMOTE_ADDR'] == '109.167.142.195'){
            // echo "good \n";
            // exit();
            // }
            //сохранение данных о покупателе
            $customer = customer::get();

            $oEventPoint = new umiEventPoint("users_register_on_event_from_landing_do");
            $oEventPoint->setMode("before");
            $oEventPoint->setParam("user_id", $customer->id);
            $this->setEventPoint($oEventPoint);

            $fname = getRequest('fname');
            $father_name = getRequest('father_name');
            $lname = getRequest('lname');
            //Компания
            $company = getRequest('company');
            //Город семинара
            $gorod_in = getRequest('gorod_in');
            //Город покупателя
            $gorod_from = getRequest('city');
            // регион
            $region = getRequest('region');

            //Дата семинара
            $data = getRequest('data');
    
            if($dadata = trim(getRequest('dadata'))) {
                $dadataData = null;
    
                if(OrthoSiteDataDadataHelper::isDadataEnabled() && getRequest('country') == 'Россия') {
                    $dadataData = json_decode($dadata, true);
                    
                    try {
                        OrthoSiteDataDadataHelper::validateData(trim(getRequest('address')), $dadataData);
                    } catch (Exception $e) {
                        $dadataData = null;
                    }
                }
    
                OrthoSiteDataDadataHelper::setUserAddressData($customer->getObject(), $dadataData);
            }
    
            //Ординатор (для скидки)
            $ordinator = (getRequest('ordinator') == 'Ординатор') ? 1 : 0;
            $ordinator_30 = (getRequest('ordinator30') == 'Ординатор 30') ? 1 : 0;
            $doctor = (getRequest('doctor') == 'Врач 1-2 года') ? 1 : 0;
            $teacher = (getRequest('teacher') == 'Преподаватель кафедры') ? 1 : 0;
            $school = (getRequest('school') == 'Участник Школы Ортодонтии' || getRequest('school') == 'Действующий участник Школы ортодонтии') ? 1 : 0;
            $poo = (getRequest('poo') == 'Член ПОО') ? 1 : 0;


            $loyalty = (strpos(getRequest('loyalty'), 'по программе Ormco Stars') !== false) ? 1 : 0;

            //телефон
            $phone = getRequest('phone');
            //Планирую покупку продукции Ormco в ближайшее время (1-2 месяца)
            $want_to_buy = (getRequest('want_to_buy') == 1) ? 1 : 0;

            $how_you_been_edu = (getRequest('how_you_been_edu') == 1) ? 60757 : 60758;

            $other_specialization = getRequest('other_specialization');
            //Email
            // если пользователь совсем новый, заполняем его данные из формы
            if ($customer->email == ''){
                $customer->email = $email;
            }
            if ($customer->fname == ''){
                $customer->fname = $fname;
            }
            if ($customer->lname == ''){
                $customer->lname = $lname;
            }
            if ($customer->bd == ''){
                $customer->bd = getRequest('bd');
            }
            if ($customer->company == ''){
                $customer->company = getRequest('company');
            }

            if(!empty($fname)){
                $customer->setValue('fname', $fname);
            }
            if(!empty($father_name)){
                $customer->setValue('father_name', $father_name);
            }
            if(!empty($lname)){
                $customer->setValue('lname', $lname);
            }
            if(!empty($gorod_from)){
                $customer->setValue('city', $gorod_from);
            }
            if(!empty($region)){
                $customer->setValue('region', $region);
            }
            if(!empty($other_specialization)){
                $customer->setValue('other_specialization', $other_specialization);
            }

            // if($customer->father_name == '') $customer->father_name = $father_name;
            // if($customer->phone == '') $customer->phone = $phone;
            // if($customer->city == '') $customer->city = $gorod_from;
            // $customer->agree = 1;

            $customer->commit();

            $oEventPoint->setMode("after");
            $this->setEventPoint($oEventPoint);

            // сохраняем данные в заказ
            $orderId = $order->id;
            //$order -> setValue('seminar_name', $element->name);
            $order->setValue('order_fname', $fname);
            $order->setValue('order_lname', $lname);
            $order->setValue('order_father_name', $father_name);
            $order->setValue('order_company', $company);
            $order->setValue('order_gorod_in', $gorod_in);
            $order->setValue('order_city', $gorod_from);
            if (!$region) {
                $region = 9870;
                // Москва
                $order->setValue('no_region', 1);
            }
            $order->setValue('order_region', $region);

            //$order_region = $order -> order_region;
            //if($data == '') $data = $this->getSeminarDate($page_id);
            //$order -> setValue('data', $data);

            $order->setValue('ordinator', $ordinator);
            $order->setValue('ordinator_30', $ordinator_30);
            $order->setValue('doctor', $doctor);
            $order->setValue('teacher', $teacher);

            //Ординатор удостоверение
            $uploadfile = getRequest('file');
            if (!$uploadfile){
                $uploadfile = getRequest('uploadfile');
            }
            if ($uploadfile) {
                // проверка на относительный путь к файлу
                $uploadfile = str_replace('https://orthodontia.ru', '.', $uploadfile);
                if (!file_exists($uploadfile)){
                    $uploadfile = '.' . $uploadfile;
                }
                if (file_exists($uploadfile)) {
                    $field_value = new umiFile($uploadfile);
                    $order->setValue('filepoo', $field_value);
                }
            }

            // сохранение скидок
            $price = $element->price;
            $event_discount_name = $event_discount_value = false;
            if ($school) {
                $order->school = 1;
                $event_discount_name = 'Участникам Школы Ортодонтии ОРМКО (50%)';
                $event_discount_value = ceil($price * 0.5);
            } elseif ($ordinator) {
                $order->ordinator = 1;
                $event_discount_name = 'Ординаторам (40%)';
                $event_discount_value = ceil($price * 0.6);
            } elseif ($ordinator_30) {
                $order->ordinator_30 = 1;
                $event_discount_name = 'Ординаторам (30%)';
                $event_discount_value = ceil($price * 0.7);
            } elseif ($doctor) {
                $order->doctor = 1;
                $event_discount_name = 'Врачам 1-2 года (30%)';
                $event_discount_value = ceil($price * 0.7);
            } elseif ($teacher) {
                $order->teacher = 1;
                $event_discount_name = 'Преподавателям кафедры (50%)';
                $event_discount_value = ceil($price * 0.5);
            } elseif ($poo) {
                $order->poo = 1;
                $event_discount_name = 'Членам Профессионального общества ортодонтов (15%)';
                $event_discount_value = ceil($price * 0.85);
            }elseif ($loyalty) {
                $order->loyalty = 1;
                $event_discount_name = 'Скидка Ormco Stars (50%)';
                $event_discount_value = ceil($price * 0.5);
            }
            if ($event_discount_name && $event_discount_value) {
                $order->setValue('event_discount_name', $event_discount_name);
                $order->setValue('event_discount_value', $event_discount_value);
                $order->setValue('na_moderacii', 1);
                $order->commit();
            }

            // сохраняем потенциальные и заморожденные баллы ORMCO STARS
            $potential_ormco_star = (int) getRequest('potential_ormco_star');
            if ($potential_ormco_star > 0) {
                $order->setValue('potential_ormco_star', $potential_ormco_star);
                $customer->setValue('potential_ormco_star', $customer->getValue('potential_ormco_star') + $potential_ormco_star);
            }
            $freez_ormco_star = (int) getRequest('freez_ormco_star');
            if ($freez_ormco_star > 0) {
                $order->setValue('freez_ormco_star', $freez_ormco_star);
                $customer->setValue('freez_ormco_star', $customer->getValue('freez_ormco_star') + $freez_ormco_star);
                $customer->setValue('yet_ormco_star', $customer->getValue('yet_ormco_star') - $freez_ormco_star);
            }
            // отправка уведомления об OrmcoStars
            if ($freez_ormco_star > 0 || $potential_ormco_star > 0) {
                $customer->commit();
                $usersModule = $controller->getModule('users');
                if ($usersModule) {
                    $usersModule->sendOrmcoStarsNotify($customer);
                }
            }

            $event_discount_promocode_name = trim(getRequest('promo'));
            $event_discount_promocode_price = trim(getRequest('totalprice'));
            if ($event_discount_promocode_name && $event_discount_promocode_price != '') {
                if ($event_discount_name){
                    $event_discount_promocode_name = $event_discount_name . ", Промокод (" . $event_discount_promocode_name . ")";
                }
                $order->setValue('event_discount_name', $event_discount_promocode_name);
                $order->setValue('event_discount_value', $event_discount_promocode_price);
                $order->setValue('na_moderacii', 1);
                $order->commit();

                //$start = strpos($event_discount_promocode_name, '"');
                //$finish = strpos($event_discount_promocode_name, '"');
                //if ($start !== false && $finish !== false) {
                //$promocode = substr($event_discount_promocode_name, $start, $finish);
                $promocode = $event_discount_promocode_name;

                // кастомизация для лендинга с форумом 2018
                $forumEventIds = array(1647, 1648, 1649, 1650, 1651, 1652, 1653);
                if (in_array($page_id, $forumEventIds)) {
                    $forumEvent = $hierarchy->getElement(1647);
                    if ($forumEvent instanceof umiHierarchyElement) {
                    }
                } else {
                    $forumEvent = $element;
                }
                // кастомизация для лендинга с форумом 2018

                $promocodes_str = $forumEvent->promokod_arr;
                $promocodes_arr = json_decode($promocodes_str);
                $promocodes_arr_new = array();
                foreach ($promocodes_arr as $promocodeItem) {
                    $pr_name = $pr_discount = $pr_count = $pr_active = null;

                    $pr_name = $promocodeItem->name;
                    $pr_discount = $promocodeItem->discount;
                    $pr_discount_abs = $promocodeItem->discount_abs;
                    $pr_text = $promocodeItem->text;
                    $pr_count = $promocodeItem->count;
                    $pr_active = $promocodeItem->active;
                    if ($pr_name == $promocode) {
                        $pr_count = $pr_count - 1;
                        $pr_active = ($pr_count < 1) ? 0 : 1;
                    }
                    //[{"name":"testpromo","discount":"10","count":"1","active":"1"},{"name":"111","discount":"20","count":"15","active":"1"},{"name":"2222","discount":"50","count":"12","active":"0"}]
                    $promocodes_arr_new[] = array(
                        "name" => $pr_name,
                        "discount" => $pr_discount,
                        "discount_abs" => $pr_discount_abs,
                        "text" => $pr_text,
                        "count" => $pr_count,
                        "active" => $pr_active
                    );
                }
                // сохраняем резервку
                //backupModel::getInstance()->save($forumEvent->getId());

                $forumEvent->promokod_arr = json_encode($promocodes_arr_new);
                $forumEvent->commit();
                //}
            }

            $order->setValue('order_e-mail', $email);
            $order->setValue('order_phone', $phone);
            $order->setValue('order_want_to_buy', $want_to_buy);
            $order->setValue('order_how_you_been_edu', $how_you_been_edu);
            $order->setValue('order_who', getRequest('who'));
            $order->setValue('order_bd', getRequest('bd'));
            $order->setValue('order_company', getRequest('company'));

            $order->setValue('from_landing', 1);
            $order->setValue('order_agree', 1);
            $order->order_date = time();

            $order->page_id = $page_id;
            $order->event_name = $element->name;

            $gorod_in = $element->gorod_in;
            $gorod_in_object = umiObjectsCollection::getInstance()->getObject($gorod_in);
            if ($gorod_in_object) {
                $order->event_gorod_in = $gorod_in_object->name;
            }

            $publish_date = $element->publish_date;
            if ($publish_date) {
                $publish_date = $publish_date->getFormattedDate("U");
            }
            $finish_date = $element->finish_date;
            if ($finish_date) {
                $finish_date = $finish_date->getFormattedDate("U");
            }
            if ($finish_date && $publish_date) {
                $dataModule = cmsController::getInstance()->getModule('data');

                $event_data = $dataModule->dateruPeriod($publish_date, $finish_date);
                $event_data_str = (isset($event_data['first_line'])) ? $event_data['first_line'] : '';
                $event_data_str .= (isset($event_data['second_line'])) ? ' ' . $event_data['second_line'] : '';
                $event_data_str .= (isset($event_data['days'])) ? ' (' . $event_data['days'] . ')' : '';
            } else {
                $event_data_str = 'дата не определена';
            }
            $order->event_data = $event_data_str;

            // save data from landing form
            $input = isset($_REQUEST) ? $_REQUEST : "";
            $input_str = '';
            unset($input['path']);
            unset($input['umi_authorization']);
            foreach ($input as $key => $value) {
                $input_str .= $key . '=' . htmlspecialchars_decode($value) . "\n";
            }
            $order->data_from_landing = $input_str;

			$order->setValue('need_export', 0);
			$order->setValue('need_export_new', 1);

            //$controller -> getModule('data') -> saveEditedObject($orderId, true);
            $order->commit();

            $paymentId = getRequest('payment_id');
            $bonus = 0;

            // назначить оплату через юр счет, если выбран существующее юр лицо
            if (!$paymentId && isset($_REQUEST['yur'])) {
                $yur_id = $_REQUEST['yur'];
                $yur = umiObjectsCollection::getInstance()->getObject($yur_id);
                if ($yur) {
                    $paymentId = 4666; //payment id - Счет для юр.лиц
                }
            }
            if ($paymentId) {
                $order_obj = order::get($orderId);

                //применение скидки на мероприятие сохраненной в заказе
                /* if ($ordinator) {
                  $originPrice = $order_obj -> getOriginalPrice();
                  $discount = (float)$order -> event_discount_value;
                  $bonus = $originPrice - $discount;

                  $order -> setValue('bonus', $bonus);
                  $order -> commit();
                  $order -> refresh();
                  } */

                $payment = payment::get($paymentId, $order_obj);
                if ($payment instanceof payment) {
                    $paymentName = $payment->getCodeName();

                    if($element->getValue('otpravlyat_vse_registracii_na_moderaciyu') == 1){
                        $order->setValue('na_moderacii', 1);
                    }
                    $order->setValue('payment_id', $paymentId);
                    $order->commit();

                    // если есть необходимость в модерации и завершаем заказ и отправляем на успешную страницу с уведомлением о модерации
                    if($element->getValue('otpravlyat_vse_registracii_na_moderaciyu') == 1 || $ordinator || $ordinator_30 || $doctor || $teacher || $school || $poo || $loyalty || ($event_discount_promocode_name != '' && $event_discount_promocode_price != '')){

                        $order->order();

                        // увеличение кол-ва регистраций на мероприятие
                        $this->orthoq_reserv($page_id);
                        //$element->curr_reserv = $element->curr_reserv + 1;
                        $element->commit();

                        file_put_contents(CURRENT_WORKING_DIR . '/traceajaxlp.txt', "\n  отправка на модерацию \n", FILE_APPEND);

                        if($json){
                            return array(
                                'status'=>'successful',
                                'message'=>'order need moderate'
                            );
                        }
                        return $this->redirect('/event_apply_successful/?o=' . $orderId);
                    }

                    // если без модерации, то проверить не юр лицо ли там
                    $yur_id = $_REQUEST['yur'];
                    $yur = umiObjectsCollection::getInstance()->getObject($yur_id);
                    if ($yur && $paymentId == 4666) {
                        $order->setValue('legal_person', $yur_id);
                        $order->commit();

                        $url = "{$this->pre_lang}/" . cmsController::getInstance()->getUrlPrefix() . "emarket/purchase/payment/{$paymentName}/do/?legal-person=$yur_id";

                        // увеличение кол-ва регистраций на мероприятие
                        $this->orthoq_reserv($page_id);
                        //$element->curr_reserv = $element->curr_reserv + 1;
                        $element->commit();

                        //exit('Выбор способа оплаты');
                        file_put_contents(CURRENT_WORKING_DIR . '/traceajaxlp.txt', "\n Оплата ur lico \n" . print_r($url, true) . "\n", FILE_APPEND);

                        if($json){
                            $order->order();
                            $order->payment_document_num = $order->id;
                            $order->commit();

                            $this->sendInvoiceMail();
                            return array(
                                'status'=>'successful',
                                'message'=>'order for invoice created'
                            );
                        }

                        $this->redirect($url);
                    }

                    // если без модерации и не для юр лиц, то кидаем на выполнение способа оплаты
                    $url = "{$this->pre_lang}/" . cmsController::getInstance()->getUrlPrefix() . "emarket/purchase/payment/{$paymentName}/";

                    //var_dump($url);
                    //exit('Выбор способа оплаты');
                    file_put_contents(CURRENT_WORKING_DIR . '/traceajaxlp.txt', "\n Выбор способа оплаты \n" . print_r($url, true) . "\n", FILE_APPEND);

                    // увеличение кол-ва регистраций на мероприятие
                    $this->orthoq_reserv($page_id);
                    //$element->curr_reserv = $element->curr_reserv + 1;
                    $element->commit();

                    $order_total_price = $order_obj->getActualPrice();
                    if($json && $order_total_price == 0){
                        $order->order();

                        return array(
                            'status'=>'successful',
                            'message'=>'order created'
                        );
                    }

                    $this->redirect($url);
                }
            }

            file_put_contents(CURRENT_WORKING_DIR . '/traceajaxlp.txt', "\n  не выбран способ оплаты \n", FILE_APPEND);

            //exit('не выбран способ оплаты');

            if($json){
                return array(
                    'status'=>'error',
                    'message'=>'not found payment id'
                );
            }


            return $this->redirect($referer);
        }

        file_put_contents(CURRENT_WORKING_DIR . '/traceajaxlp.txt', "\n  нет такого мероприятия \n", FILE_APPEND);
        //exit('нет такого мероприятия');

        if($json){
            return array(
                'status'=>'error',
                'message'=>'not found event id'
            );
        }

        return $this->redirect($referer);
    }

    /*особая обработка мероприятий для семинара в компоновкой по дням
    * добавляем json со значениями 1,2,3 день
    * при покупке заполняем соответствующие скрытые поля, выбираем большее значение из них как кол-во забронированных мест
    * 1,2 день id = 2862
      1,2, 3 день id = 2863
      3 день id = 2864
    */
    public function orthoq_reserv($eventid = NULL) {
        if (!$eventid)
            $eventid = getRequest('param0');

        $orthoq_ids = array(
            2862 => array(
                'day1'=>1,
                'day2'=>1,
                'day3'=>0,
            ),
            2863 => array(
                'day1'=>1,
                'day2'=>1,
                'day3'=>1,
            ),
            2864 => array(
                'day1'=>0,
                'day2'=>0,
                'day3'=>1,
            ),
        );

        $hierarchy = umiHierarchy::getInstance();

        if(isset($orthoq_ids[$eventid])){

            //$event = $hierarchy -> getElement($eventid);
            //if ($event instanceof umiHierarchyElement) {

            // берем  параметры увеличения резервов по дням
            $reserv_rules = $orthoq_ids[$eventid];

            foreach($orthoq_ids as $eventIdTmp=>$val){
                $eventTmp = $hierarchy -> getElement($eventIdTmp);
                $max_value = 0;
                if ($eventTmp instanceof umiHierarchyElement) {
                    // берем текущее значение кол-во зарезервированных мест разбитое по дням
                    $curr_reserv_tmp_json = $eventTmp->curr_reserv_tmp;
                    $curr_reserv_tmp_arr = json_decode($curr_reserv_tmp_json, true);
                    $new_reserv_tmp = array();
                    foreach($reserv_rules as $day=>$count){
                        //var_dump($eventIdTmp .'-'.$day);
                        $old_value = isset($curr_reserv_tmp_arr[$day]) ? intval($curr_reserv_tmp_arr[$day]) : 0;
                        $new_value = $count + $old_value;
                        if($new_value > $max_value && isset($val[$day]) && $val[$day] > 0) {
                            $max_value = $new_value;
                            //var_dump('new_max='.$max_value);

                        }
                        $new_reserv_tmp[$day] = $new_value;
                    }
                    //var_dump('final_max='.$max_value);
                    //var_dump($new_reserv_tmp);
                    $eventTmp->curr_reserv_tmp = json_encode($new_reserv_tmp);
                    $eventTmp->curr_reserv = $max_value;
                }
            }

            //}
        }else{
            $event = $hierarchy -> getElement($eventid);

            if ($event instanceof umiHierarchyElement) {
                $event->curr_reserv = $event->curr_reserv + 1;
            }
        }

        return ;
    }

    /*проверка повторной регистрации оставленно для совместимости*/
	public function avaliable_reserv($eventid = NULL, $email = NULL) {
		if (!$eventid)
			$eventid = getRequest('param0');

		$hierarchy = umiHierarchy::getInstance();
		$event = $hierarchy -> getElement($eventid);
		if ($event instanceof umiHierarchyElement) {
			$amount_total = $event->max_reserv;
			$amount_reserv = $event->curr_reserv;
		}

		$amount_total = ($amount_total > 0 ) ? $amount_total : 0;
		$amount_reserv = ($amount_reserv > 0 ) ? $amount_reserv : 0;
		$amount_free = ($amount_total > 0) ? $amount_total - $amount_reserv : '9999';
		return array(
			'amount_total' => $amount_total,
			'amount_reserv' => $amount_reserv,
			'amount_free' => ($amount_free > 0) ? $amount_free : 0
		);
	}

    public function lporderSmartFast() {
        $objectsCollection = umiObjectsCollection::getInstance();
        $hierarchy = umiHierarchy::getInstance();
        $controller = cmsController::getInstance();

        $resultInfo = $resultError = array();
        $needEventRegPid=2945;

	    $email = getRequest('email'); // test data
        $eventId = getRequest('eventId'); //2973; // test data
        //$fast_reg=1;

        $allowEventIds = array(2973,2970,2971,2972);
        /*Предложение Ормко – 2973
        Предложение Каво – 2970
        Предложение Керр – 2971
        Предложение Нобель – 2972*/
        $isAllowEventIds = (in_array($eventId, $allowEventIds)) ? 1:0;

        // проверить не повторный ли это заказ
        $is_first_purchase = $this->is_first_purchase($email,$eventId);

        // проверить есть ли заказ предыдущий
        $is_needEvent_purchase = $this->is_first_purchase($email,$needEventRegPid);

        //if(!($is_first_purchase['result'] == 1) && $is_needEvent_purchase['result'] == 1 && $isAllowEventIds && $fast_reg == 1){
        if($is_first_purchase['result'] == 1){
            $resultError['notFirstPurchase'] = 'Has order yet';
        }
        if(!($is_needEvent_purchase['result'] == 1)){
            $resultError['notAllowToPurchase'] = 'Has not order parent event';
        }
        if(!$isAllowEventIds){
            $resultError['disallowEvent'] = 'Disallow for this event id';
        }

        if (sizeof($resultError) > 0) {
            return $result = array('status' => 'error', 'msg' => $resultError);
        }

        // найти родительский заказ
        $needEventOrderId = $is_needEvent_purchase['order-id'] ;//1947559;// test data
        if(!$needEventOrderId){
            $resultError['notFoundParentId'] = "Can't find parent event id";
            return $result = array('status' => 'error', 'msg' => $resultError);
        }

        // создать копию заказа
        $newOrderId = $objectsCollection->cloneObject($needEventOrderId);

        $resultInfo['orderId'] = $newOrderId;

        // поменять номер нового заказа
        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
        $sel->order('number')->desc();
        $sel->limit(0, 1);
        $number = $sel->first ? ($sel->first->number + 1) : 1;

        regedit::getInstance()->setVal('emarket/lastOrderNumber', $number);

        $newOrder = $objectsCollection->getObject($newOrderId);
        $newOrder->name = getLabel('order-name-prefix', 'emarket', $number);
        $newOrder->number = $number;
        $newOrder->commit();

        // поменять ему товар
        $orderIns = order::get($newOrderId);
        if ($orderIns instanceof order == false) {
            $resultError['ErGetOrder'] = 'Error get Order instance with Id - ' . $newOrderId;
        } else {
            // чистим корзину
            $orderItems = $orderIns->getItems();
            foreach ($orderItems as $orderItem) {
                $orderIns->removeItem($orderItem);
            }
            $orderIns->refresh();

            $orderIns_add = order::get($newOrderId, true);

            // добавить товары
            $event = $hierarchy->getElement($eventId);
            if ($event instanceof umiHierarchyElement) {
                $resultInfo['cart_item_page_id'] = $eventId;
                $resultInfo['name'] = $event->name;

                //add cart item
                $orderItem = orderItem::create($eventId);
                $itemId = $orderItem->getId();
                $resultInfo['method'] = 'Add';

                $resultInfo['cart_item_id'] = $itemId;

                // set item params
                $orderItem->setAmount(1);
                $orderItem->refresh();

                $orderIns_add->appendItem($orderItem);

                $newOrder->order_date = time();
                $newOrder->status_change_date = time();
                $newOrder->page_id = $eventId;
                $newOrder->event_name = $event->name;

                // назначить дату мероприятия
                $dataModule = $controller->getModule('data');
                $publish_date = $event->publish_date;
                if ($publish_date) {
                    $publish_date = $publish_date->getFormattedDate("U");
                }
                $finish_date = $event->finish_date;
                if ($finish_date) {
                    $finish_date = $finish_date->getFormattedDate("U");
                }
                if ($finish_date && $publish_date) {
                    $event_data = $dataModule->dateruPeriod($publish_date, $finish_date);
                    $event_data_str = (isset($event_data['first_line'])) ? $event_data['first_line'] : '';
                    $event_data_str .= (isset($event_data['second_line'])) ? ' ' . $event_data['second_line'] : '';
                    $event_data_str .= (isset($event_data['days'])) ? ' (' . $event_data['days'] . ')' : '';
                } else {
                    $event_data_str = 'дата не определена';
                }
                $newOrder->event_data = $event_data_str;

                // save data from landing form
                $input = isset($_REQUEST) ? $_REQUEST : "";
                $input_str = '';
                unset($input['path']);
                unset($input['umi_authorization']);
                foreach ($input as $key => $value) {
                    $input_str .= $key . '=' . htmlspecialchars_decode($value) . "\n";
                }
                $newOrder->data_from_landing = $input_str;

                // seo data
                $newOrder->http_referer = getServer("HTTP_REFERER");
                $newOrder->http_target = getServer("REQUEST_URI");
                $newOrder->order_create_date = time();
                $newOrder->need_export_new = 1;

                $newOrder->commit();
            } else {
                $resultError['NoPage'] = 'Not found page with pageId = ' . $eventId ;
            }
            $orderIns_add->refresh();

            $orderIns_change = order::get($newOrderId, true);
            $orderIns_change->refresh();
        }

        if (sizeof($resultError) > 0) {
            $result = array('status' => 'error', 'msg' => $resultError);
        } else {
            $result = array('status' => 'successfull', 'msg' => $resultInfo);
        }

        return $result;
    }

    public function lporderSmartFaster() {
        $objectsCollection = umiObjectsCollection::getInstance();
        $hierarchy = umiHierarchy::getInstance();
        $controller = cmsController::getInstance();

        $resultInfo = $resultError = array();
        $needEventRegPid=1;

        /*
         * Само мероприятия DPT: 2998

https://orthodontia.ru/dpt

Предложение Ормко: 3002
Предложение Каво: 3000
Предложение Керр: 2999
Предложение Нобель: 3001*/

        $email = getRequest('email'); // test data
        $eventId = getRequest('eventId'); //2973; // test data
        //$fast_reg=1;

        $allowEventIds = array(
            2945 => array(2973,2970,2971,2972), // for DPT
            2998 => array(3002,3000,2999,3001), // for ...
        );
        /*Предложение Ормко – 2973
        Предложение Каво – 2970
        Предложение Керр – 2971
        Предложение Нобель – 2972*/
        $isAllowEventIds = 0;
        foreach($allowEventIds as $parent_event_id => $fast_reg_id_arr){
            if(in_array($eventId, $fast_reg_id_arr)){
                $isAllowEventIds = 1;
                $needEventRegPid = $parent_event_id;
            }
        }


        // проверить не повторный ли это заказ
        $is_first_purchase = $this->is_first_purchase($email,$eventId);

        // проверить есть ли заказ предыдущий
        $is_needEvent_purchase = $this->is_first_purchase($email,$needEventRegPid);

        //if(!($is_first_purchase['result'] == 1) && $is_needEvent_purchase['result'] == 1 && $isAllowEventIds && $fast_reg == 1){
        if($is_first_purchase['result'] == 1){
            $resultError['notFirstPurchase'] = 'Has order yet';
        }
        if(!($is_needEvent_purchase['result'] == 1)){
            $resultError['notAllowToPurchase'] = 'Has not order parent event';
        }
        if(!$isAllowEventIds){
            $resultError['disallowEvent'] = 'Disallow for this event id';
        }

        if (sizeof($resultError) > 0) {
            return $result = array('status' => 'error', 'msg' => $resultError);
        }

        // найти родительский заказ
        $needEventOrderId = $is_needEvent_purchase['order-id'] ;//1947559;// test data
        if(!$needEventOrderId){
            $resultError['notFoundParentId'] = "Can't find parent event id";
            return $result = array('status' => 'error', 'msg' => $resultError);
        }

        // создать копию заказа
        $newOrderId = $objectsCollection->cloneObject($needEventOrderId);

        $resultInfo['orderId'] = $newOrderId;

        // поменять номер нового заказа
        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
        $sel->order('number')->desc();
        $sel->limit(0, 1);
        $number = $sel->first ? ($sel->first->number + 1) : 1;

        regedit::getInstance()->setVal('emarket/lastOrderNumber', $number);

        $newOrder = $objectsCollection->getObject($newOrderId);
        $newOrder->name = getLabel('order-name-prefix', 'emarket', $number);
        $newOrder->number = $number;
        $newOrder->commit();

        // поменять ему товар
        $orderIns = order::get($newOrderId);
        if ($orderIns instanceof order == false) {
            $resultError['ErGetOrder'] = 'Error get Order instance with Id - ' . $newOrderId;
        } else {
            // чистим корзину
            $orderItems = $orderIns->getItems();
            foreach ($orderItems as $orderItem) {
                $orderIns->removeItem($orderItem);
            }
            $orderIns->refresh();


            // добавить товары
            $event = $hierarchy->getElement($eventId);
            if ($event instanceof umiHierarchyElement) {
                $resultInfo['cart_item_page_id'] = $eventId;
                $resultInfo['name'] = $event->name;

                //add cart item
                $orderItem = orderItem::create($eventId);
                $itemId = $orderItem->getId();
                $resultInfo['method'] = 'Add';

                $resultInfo['cart_item_id'] = $itemId;

                // set item params
                $orderItem->setAmount(1);
                $orderItem->refresh();

                $orderIns->appendItem($orderItem);

                $newOrder->order_date = time();
                $newOrder->status_change_date = time();
                $newOrder->page_id = $eventId;
                $newOrder->event_name = $event->name;

                // назначить дату мероприятия
                $dataModule = $controller->getModule('data');
                $publish_date = $event->publish_date;
                if ($publish_date) {
                    $publish_date = $publish_date->getFormattedDate("U");
                }
                $finish_date = $event->finish_date;
                if ($finish_date) {
                    $finish_date = $finish_date->getFormattedDate("U");
                }
                if ($finish_date && $publish_date) {
                    $event_data = $dataModule->dateruPeriod($publish_date, $finish_date);
                    $event_data_str = (isset($event_data['first_line'])) ? $event_data['first_line'] : '';
                    $event_data_str .= (isset($event_data['second_line'])) ? ' ' . $event_data['second_line'] : '';
                    $event_data_str .= (isset($event_data['days'])) ? ' (' . $event_data['days'] . ')' : '';
                } else {
                    $event_data_str = 'дата не определена';
                }
                $newOrder->event_data = $event_data_str;

                // save data from landing form
                /*$input = isset($_REQUEST) ? $_REQUEST : "";
                $input_str = '';
                unset($input['path']);
                unset($input['umi_authorization']);
                foreach ($input as $key => $value) {
                    $input_str .= $key . '=' . htmlspecialchars_decode($value) . "\n";
                }*/
                $data_from_landing = $newOrder->data_from_landing;
                $data_from_landing_arr = explode("\n", $data_from_landing);
                foreach ($data_from_landing_arr as $param) {
                    $pos = strpos($param, '=');

                    if ($pos !== false) {
                        $title = substr($param, 0, $pos);
                        $param = substr($param, $pos + 1);
                        $param = strip_tags(htmlspecialchars_decode($param));

                        if ($title == 'events') {
                            $param = $event->id;
                        }

                        $param = iconv('utf-8', 'windows-1251//IGNORE', $param);
                        $data_from_landing_arr_new[$title] = $param;
                    }
                }
                $input_str = '';
                foreach ($data_from_landing_arr_new as $key => $value) {
                    $input_str .= $key . '=' . htmlspecialchars_decode($value) . "\n";
                }
                $newOrder->data_from_landing = $input_str;

                // seo data
                $newOrder->http_referer = getServer("HTTP_REFERER");
                $newOrder->http_target = getServer("REQUEST_URI");
                $newOrder->order_create_date = time();
                $newOrder->need_export_new = 1;

                $newOrder->commit();

                // увеличение кол-ва регистраций на мероприятие
                $this->orthoq_reserv($eventId);
                //$element->curr_reserv = $element->curr_reserv + 1;
                $event->commit();
            } else {
                $resultError['NoPage'] = 'Not found page with pageId = ' . $eventId ;
            }
            $orderIns->refresh();
        }

        if (sizeof($resultError) > 0) {
            $result = array('status' => 'error', 'msg' => $resultError);
        } else {
            $result = array('status' => 'successfull', 'msg' => $resultInfo);
        }

        return $result;
    }

}
