<?php
class emarket_custom_notification_new extends def_module {
    /* отправка писме адмнамм о мероприятиях с 7 октября */
    public function sendMailLostOrders() {
        return 'switch off';
        $start_date = "07.10.2019, 00:01:00";
        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
        $sel->where('status_id')->isNull(false);
        $sel->where('name')->notequals('dummy');
        $sel->where('order_date')->eqmore(strtotime($start_date));
        $sel->order('order_date')->desc();
        //status_change_date
        //order_date
        foreach ($sel as $order) {
            var_dump($order->id);
            var_dump(date("d/m/Y H:i:s", $order->order_date->getFormattedDate("U")));
            var_dump(date("d/m/Y H:i:s", $order->status_change_date->getFormattedDate("U")));
            $order_id = $order->id;
            $order = order::get($order_id);
            $this->sendManagerNotificationNew($order);
        }
        exit('aaa');
    }

    /* отправка писме адмнамм о мероприятиях с 7 октября */
    public function csvLostOrders() {
        return 'switch off';
        $start_date = "07.10.2019, 00:01:00";
        $sel = new selector('objects');
        $sel->types('object-type')->name('emarket', 'order');
        $sel->where('status_id')->isNull(false);
        $sel->where('name')->notequals('dummy');
        $sel->where('order_date')->eqmore(strtotime($start_date));
        $sel->order('order_date')->desc();

        $fp = fopen('./lostOrdersNotify2.csv', 'w');
        $row = array('orderId', 'orderLink', 'eventId', 'eventName', 'order_price_with_discount', 'email 1', 'email 2', 'email 3');
        fputs($fp, chr(0xEF) . chr(0xBB) . chr(0xBF)); // BOM
        fputcsv($fp, $row, ';');
        foreach ($sel as $order) {
            var_dump($order->id);
            var_dump(date("d/m/Y H:i:s", $order->order_date->getFormattedDate("U")));
            var_dump(date("d/m/Y H:i:s", $order->status_change_date->getFormattedDate("U")));
            $order_id = $order->id;
            $order = order::get($order_id);

            $order_link = "https://orthodontia.ru/admin/emarket/order_edit/{$order_id}/";
            $event_name = $order->event_name;
            $eventId = $order->page_id;
            $event = $order->page_id;
            $event_discount_value = $order->event_discount_value;
            $event = umiHierarchy::getInstance()->getElement($eventId);
            if ($event instanceof umiHierarchyElement) {
                $row = array($order_id, $order_link, $eventId, $event_name);
                $row[] = $event_discount_value;
                $emails = $event->manager_email;
                foreach (explode(',', $emails) as $recipient) {
                    $recipient = trim($recipient);
                    if (strlen($recipient)) {
                        $row[] = $recipient;
                    }
                }
                fputcsv($fp, $row, ';');
            }
        }
        exit('aaa');
    }

    /* test mail */
    public function testMailS() {
        return 'switch off';
        $order_id = 750047;
        $order = order::get($order_id);
        $this->sendManagerNotificationNew($order);
        exit('hhhh222');

        $changedProperty = 'status_id';
        $order->need_export = true;
        $statusId = $order->getValue($changedProperty);
        $codeName = order::getCodeByStatus($statusId);
        if ($changedProperty == 'status_id' && (!$statusId || $codeName == 'payment')){
            return;
        }

        $this->sendCustomerNotificationNew($order, $changedProperty, $codeName);
        return 'ok';
    }

    // copy Notification functions for call New function
    // Notification events listeners
    public function onModifyPropertyCustom(iUmiEventPoint $event) {
        $entity = $event->getRef("entity");
        if ($entity instanceof iUmiObject) {
            $allowedProperties = array("status_id", "payment_status_id", "delivery_status_id");
            $typeId = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName('emarket', 'order');
            if (($entity->getTypeId() == $typeId) &&
                    (in_array($event->getParam("property"), $allowedProperties) ) &&
                    ($event->getParam("newValue") != $event->getParam("oldValue"))) {
                if ($event->getParam("property") == 'payment_status_id' && $event->getParam("newValue") == order::getStatusByCode('accepted', 'order_payment_status')) {
                    $this->addBonus($entity->getId());
                }
                if ($event->getParam("property") == 'status_id' && ($event->getParam("newValue") == order::getStatusByCode('canceled') || $event->getParam("newValue") == order::getStatusByCode('rejected'))) {
                    $this->returnBonus($entity->getId());
                }
                if ($event->getParam('property') == 'status_id' && $event->getParam('oldValue') != $event->getParam('newValue')) {
                    $object = $event->getRef('entity');
                    if ($event->getParam('newValue') == order::getStatusByCode('ready')) {
                        $emarketTop = new emarketTop();
                        $emarketTop->addOrder($object);
                    }
                    if ($event->getParam('oldValue') == order::getStatusByCode('ready')) {
                        $emarketTop = new emarketTop();
                        $emarketTop->delOrder($object);
                    }
                }
                $this->notifyOrderStatusChangeCustom(order::get($entity->getId()), $event->getParam("property"));
            }
        }
    }

    public function onModifyObjectCustom(iUmiEventPoint $event) {
        static $modifiedCache = array();
        static $orderStatus = array();
        $object = $event->getRef("object");
        $typeId = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName('emarket', 'order');
        if ($object->getTypeId() != $typeId){
            return;
        }
        if ($event->getMode() == "before") {
            $data = getRequest("data");
            $id = $object->getId();
            $newOrderStatus = getArrayKey($data[$id], 'status_id');
            $newPaymentStatus = getArrayKey($data[$id], 'payment_status_id');
            $newDeliveryStatus = getArrayKey($data[$id], 'delivery_status_id');
            switch (true) {
                case ($newOrderStatus != $object->getValue("status_id") ) : {
                        $modifiedCache[$object->getId()] = "status_id";
                        $orderStatus[$object->getId()]['old_status_id'] = $object->status_id;
                        break;
                    }
                case ($newDeliveryStatus != $object->getValue("delivery_status_id")) : $modifiedCache[$object->getId()] = "delivery_status_id";
                    break;
                case ($newPaymentStatus != $object->getValue("payment_status_id") ) : $modifiedCache[$object->getId()] = "payment_status_id";
                    break;
            }
        } else {
            if (isset($modifiedCache[$object->getId()])) {
                if ($modifiedCache[$object->getId()] == 'payment_status_id' && $object->getValue("payment_status_id") == order::getStatusByCode('accepted', 'order_payment_status')) {
                    $this->addBonus($object->getId());
                }
                if ($modifiedCache[$object->getId()] == 'status_id' && ($object->getValue("status_id") == order::getStatusByCode('canceled') || $object->getValue("status_id") == order::getStatusByCode('rejected'))) {
                    $this->returnBonus($object->getId());
                }
                if (array_key_exists($object->getId(), $orderStatus) && $orderStatus[$object->getId()]['old_status_id'] != $object->status_id) {
                    if ($object->status_id == order::getStatusByCode('ready')) {
                        $emarketTop = new emarketTop();
                        $emarketTop->addOrder($object);
                    }
                    if ($orderStatus[$object->getId()]['old_status_id'] == order::getStatusByCode('ready')) {
                        $emarketTop = new emarketTop();
                        $emarketTop->delOrder($object);
                    }
                }
                $this->notifyOrderStatusChangeCustom(order::get($object->getId()), $modifiedCache[$object->getId()]);
            }
        }
    }

    public function onStatusChangedCustom(iUmiEventPoint $event) {
        if ($event->getMode() == "after" && $event->getParam("old-status-id") != $event->getParam("new-status-id")) {
            if ($event->getParam("new-status-id") == order::getStatusByCode('canceled') || $event->getParam("new-status-id") == order::getStatusByCode('rejected')) {
                $this->returnBonus($event->getRef("order")->getId());
            }
            $order = $event->getRef("order");
            if ($event->getParam("old-status-id") != $event->getParam("new-status-id")) {
                if ($event->getParam("new-status-id") == order::getStatusByCode('ready')) {
                    $emarketTop = new emarketTop();
                    $emarketTop->addOrder($order);
                }
                if ($event->getParam("old-status-id") == order::getStatusByCode('ready')) {
                    $emarketTop = new emarketTop();
                    $emarketTop->delOrder($order);
                }
            }
            $this->notifyOrderStatusChangeCustom($order, "status_id");
        }
    }

    public function notifyOrderStatusChangeCustom(order $order, $changedProperty) {
        if(!$order->need_export_new && SiteEmarketOrderModel::canExportTo1C($order)) {
            $order->need_export = true;
        }

        if ($changedProperty == "status_id") {
            $order->status_change_date = new umiDate();
        }
        if (order::getCodeByStatus($order->getPaymentStatus()) == "accepted" && !$order->delivery_allow_date) {
            $sel = new selector('objects');
            $sel->types('hierarchy-type')->name('emarket', 'delivery');
            $sel->option('no-length')->value(true);
            if ($sel->first) {
                $order->delivery_allow_date = new umiDate();
            }
        }
        $statusId = $order->getValue($changedProperty);
        $codeName = order::getCodeByStatus($statusId);
        if ($changedProperty == 'status_id' && (!$statusId || $codeName == 'payment')){
            return;
        }
        $this->sendCustomerNotificationNew($order, $changedProperty, $codeName);

        if ($changedProperty == 'status_id' && $codeName == 'waiting') {
            if(!$order->payment_id && !$order->need_export && !$order->need_export_new) {
                $order->setValue('need_export', 0);
                $order->setValue('need_export_new', 1);
            }

            $this->sendManagerNotificationNew($order);
            $this->sendManagerPushNotification($order); // Уведомление на мобильное устройство
        }
    }

    public function sendCustomerNotificationNew(order $order, $changedStatus, $codeName) {
		$collection = umiObjectsCollection::getInstance();
		$cmsController = cmsController::getInstance();
        $hierarhy = umiHierarchy::getInstance();

        $customer = $collection->getObject($order->getCustomerId());
        $buyerOneClick = $collection->getObject($order->getValue('purchaser_one_click'));
        $emailOneClick = false;
        if ($buyerOneClick instanceof umiObject) {
            $emailOneClick = $buyerOneClick->email ? $buyerOneClick->email : $buyerOneClick->getValue("e-mail");
        }
        if ($emailOneClick) {
            $email = $emailOneClick;
        } else {
            $email = $customer->email ? $customer->email : $customer->getValue("e-mail");
        }

        // если партнерское мероприятие, клиенту уже ушло письмо, больше ничего не отправляем
        if ($order->partner_event) {
            return;
        }

        if ($email) {
            $name = $customer->lname . " " . $customer->fname . " " . $customer->father_name;
            $langs = $cmsController->langs;
            $statusString = "";
            $subjectString = $langs['notification-status-subject'];
            $regedit = regedit::getInstance();
            switch ($changedStatus) {
                case 'status_id' : {
                        if ($regedit->getVal('//modules/emarket/no-order-status-notification')){
                            return '';
                        }
                        if ($codeName == 'waiting') {
                            $paymentStatusCodeName = order::getCodeByStatus($order->getPaymentStatus());
                            $pkey = 'notification-status-payment-' . $paymentStatusCodeName;
                            $okey = 'notification-status-' . $codeName;
                            $statusString = ($paymentStatusCodeName == 'initialized') ?
                                    ( (isset($langs[$okey]) ? ($langs[$okey] . " " . $langs['notification-and']) : "") . (isset($langs[$pkey]) ? (" " . $langs[$pkey]) : "" ) ) :
                                    ( (isset($langs[$pkey]) ? ($langs[$pkey] . " " . $langs['notification-and']) : "") . (isset($langs[$okey]) ? (" " . $langs[$okey]) : "" ) );
                            $subjectString = $langs['notification-client-neworder-subject'];
                        } else {
                            $key = 'notification-status-' . $codeName;
                            $statusString = isset($langs[$key]) ? $langs[$key] : "_";
                        }
                        break;
                    }
                case 'payment_status_id': {
                        if ($regedit->getVal('//modules/emarket/no-payment-status-notification')){
                            return;
                        }
                        $key = 'notification-status-payment-' . $codeName;
                        $statusString = isset($langs[$key]) ? $langs[$key] : "_";
                        break;
                    }
                case 'delivery_status_id': {
                        if ($regedit->getVal('//modules/emarket/no-delivery-status-notification')){
                            return;
                        }
                        $key = 'notification-status-delivery-' . $codeName;
                        $statusString = isset($langs[$key]) ? $langs[$key] : "_";
                        break;
                    }
            }
            $paymentObject = $collection->getObject($order->payment_id);
            if ($paymentObject) {
                $paymentType = $collection->getObject($paymentObject->payment_type_id);
                $paymentClassName = $paymentType->class_name;
            } else {
                $paymentClassName = null;
            }
            $templateName = ($paymentClassName == "receipt") ? "status_notification_receipt" : "status_notification_new";
            list($template) = def_module::loadTemplatesForMail("emarket/mail/default", $templateName);

            // проверяем по какому поводу мы отправляем письмо и подключаем разные шаблоны
            $subject = "Регистрация на мероприятие";
            $settings_page_id = 765;
            $settings_page = $hierarhy->getElement($settings_page_id);
            if ($settings_page instanceof umiHierarchyElement) {
                $paymentStatusCodeName = order::getCodeByStatus($order->getPaymentStatus());

                $subject = "Проверка заявки на регистрацию №{$order->number}";
                $template = $settings_page->mail_user_neworder_check_discount;

                //если менялся статус заказа на waiting и нет payment статуса, то это первое оформление заказа с учетом
                // исключения для мероприятий с псевдоскидкой "Выступающий"
                //"Damon клуб: открытый микрофон, 13.03.20, Москва" id = 2814
                //"Damon клуб: открытый микрофон, 16.04.20, Санкт-Петербург" id = 2827
                //"Damon клуб: открытый микрофон, 09.06.20, Москва" id = 2872
                $elementId = $order->page_id;
                $event = $hierarhy->getElement($elementId);
                if ($event instanceof umiHierarchyElement) {
                    $listeners_speakers = ($event -> listeners_speakers == 1) ? 1 : 0;
                }

                if ($codeName == 'waiting' && $event->getObjectTypeId() == 226) {
                    // письма клиенту при успешном оформлении заказа на консультацию
                    $subject = "Регистрация на консультацию эксперта №{$order->number}";
                    $template = $settings_page->getValue('pismo_klientu_o_registracii_na_konsultaciyu');
				}else if ($codeName == 'waiting' && !$paymentStatusCodeName && $order->event_discount_value != '' && ($listeners_speakers == 1)) {
                    // письма клиенту при успешном оформлении заказа
                    $subject = "Регистрация на мероприятие №{$order->number}";
                    $template = $settings_page->mail_user_neworder;
                } elseif ($codeName == 'waiting' && !$paymentStatusCodeName && $order->event_discount_value != '') {
                    // письма клиенту при смене статуса заказа мероприятия и необходимости модерации скидки
                    $subject = "Проверка заявки на регистрацию №{$order->number}";
                    $template = $settings_page->mail_user_neworder_check_discount;
                } elseif ($order->payment_id && $codeName == 'waiting') {
                    // письма клиенту при успешном оформлении заказа
                    $subject = "Регистрация на мероприятие №{$order->number}";
                    $template = $settings_page->mail_user_neworder;
                    // нет оплаты [бесплатное мероприятие]
                } elseif (!($order->payment_id) && $codeName == 'waiting') {
                    // Регистрация на бесплатный семинар
                    $subject = "Регистрация на мероприятие №{$order->number}";
                    $template = $settings_page->mail_user_neworder_free;

                    //Регистрация на вебинар
                    foreach ($order->getItems() as $orderItem) {
                        $orderItemElementId = $orderItem->getItemElement()->getId();
                        $element = $hierarhy->getElement($orderItemElementId);
                        if ($element instanceof umiHierarchyElement) {
                            if ($element->event_type == 1279) { // Тип мероприятия вебинар
                                $template = $settings_page->mail_user_neworder_webinar;
                                break;
                            }
                        }
                    }
                } else {
                    return;
                }
            }
            $extParams = array(
                'event_name',
                'event_gorod_in',
                'event_data',
            );
            $extParamsCustomer = array(
                'order_lname',
                'order_fname',
                'order_father_name',
                'order_phone',
                'order_e-mail',
                'order_city'
            );

            $param = array();
            $param["order_id"] = $order->id;
            $param["order_name"] = $order->name;
            $param["order_number"] = $order->number;
            $param["status"] = $statusString;

            //get customer replace data
            foreach ($extParams as $extParam) {
                $param[$extParam] = $order->getValue($extParam);
            }
            foreach ($extParamsCustomer as $extParam) {
                $fieldName = str_replace("order_", "", $extParam);
                $param[$extParam] = $customer->getValue($fieldName);
            }

            $param["personal_params"] = $this->getPersonalLinkParams($customer->getId());

            $domain = $cmsController->getCurrentDomain();

            $param["domain"] = $domain->getCurrentHostName();

            if ($paymentClassName == "receipt") {
                $param["receipt_signature"] = sha1("{$customer->getId()}:{$customer->email}:{$order->order_date}");
            }

			if($event->getObjectTypeId() == 226) {
				$param["expert_name"] = '';
				$expert_id = $event->getValue('speaker');
				if(is_array($expert_id)){
					$expert_id = array_pop($expert_id);
				}
				$expert = $collection->getObject($expert_id);
				if($expert instanceof umiObject){
					$param["expert_name"] = $expert->getValue('h1');
				}
			}

            $content = def_module::parseTemplateForMail($template, $param);

            $letter = new umiMail();
            $letter->addRecipient($email, $name);

            $domains = domainsCollection::getInstance();
            $domainId = $cmsController->getCurrentDomain()->getId();
            $defaultDomainId = $domains->getDefaultDomain()->getId();

            if ($regedit->getVal("//modules/emarket/from-email/{$domainId}")) {
                $fromMail = $regedit->getVal("//modules/emarket/from-email/{$domainId}");
                $fromName = $regedit->getVal("//modules/emarket/from-name/{$domainId}");
            } elseif ($regedit->getVal("//modules/emarket/from-email/{$defaultDomainId}")) {
                $fromMail = $regedit->getVal("//modules/emarket/from-email/{$defaultDomainId}");
                $fromName = $regedit->getVal("//modules/emarket/from-name/{$defaultDomainId}");
            } else {
                $fromMail = $regedit->getVal("//modules/emarket/from-email");
                $fromName = $regedit->getVal("//modules/emarket/from-name");
            }

            $letter->setFrom($fromMail, $fromName);
            $letter->setSubject($subject);
            $letter->setContent($content);
            $letter->commit();
            $letter->send();
        }
    }

    public function sendManagerNotificationNew(order $order, $forceEmail = null) {
        $regedit = regedit::getInstance();
        $cmsController = cmsController::getInstance();
        $domains = domainsCollection::getInstance();
        $hierarhy = umiHierarchy::getInstance();
        $domainId = $cmsController->getCurrentDomain()->getId();
        $defaultDomainId = $domains->getDefaultDomain()->getId();

        if ($regedit->getVal("//modules/emarket/manager-email/{$domainId}")) {
            $emails = $regedit->getVal("//modules/emarket/manager-email/{$domainId}");
            $fromMail = $regedit->getVal("//modules/emarket/from-email/{$domainId}");
            $fromName = $regedit->getVal("//modules/emarket/from-name/{$domainId}");
        } elseif ($regedit->getVal("//modules/emarket/manager-email/{$domainId}")) {
            $emails = $regedit->getVal("//modules/emarket/manager-email/{$domainId}");
            $fromMail = $regedit->getVal("//modules/emarket/from-email/{$domainId}");
            $fromName = $regedit->getVal("//modules/emarket/from-name/{$domainId}");
        } elseif ($regedit->getVal("//modules/emarket/manager-email/{$defaultDomainId}")) {
            $emails = $regedit->getVal("//modules/emarket/manager-email/{$defaultDomainId}");
            $fromMail = $regedit->getVal("//modules/emarket/from-email/{$defaultDomainId}");
            $fromName = $regedit->getVal("//modules/emarket/from-name/{$defaultDomainId}");
        } else {
            $emails = $regedit->getVal('//modules/emarket/manager-email');
            $fromMail = $regedit->getVal("//modules/emarket/from-email");
            $fromName = $regedit->getVal("//modules/emarket/from-name");
        }

		$item_type_id = 0;
		$item_name = '';
        foreach ($order->getItems() as $orderItem) {
            $orderItemElementId = $orderItem->getItemElement()->getId();
            $element = $hierarhy->getElement($orderItemElementId);
            if ($element instanceof umiHierarchyElement && $element->manager_email) {
				$item_type_id = $element->getObjectTypeId();
				$item_name = $element->getValue('h1');
                $emails = $element->manager_email;
                break;
            }
        }

        if($forceEmail) {
            $emails = $forceEmail;
        }

        $letter = new umiMail();

        $recpCount = 0;
        foreach (explode(',', $emails) as $recipient) {
            $recipient = trim($recipient);
            if (strlen($recipient)) {
                $letter->addRecipient($recipient);
                $recpCount++;
            }
        }
        if (!$recpCount){
            return;
        }

        list($template) = def_module::loadTemplatesForMail("emarket/mail/default", "neworder_notification");
        try {
            $payment = payment::get($order->payment_id, $order);
            $paymentName = $payment ? $payment->name : '';
            $paymentStatus = order::getCodeByStatus($order->getPaymentStatus());
        } catch (coreException $e) {
            $paymentName = "";
            $paymentStatus = "";
        }

        $subject = "Регистрация на мероприятие";
        $settings_page_id = 765;

        $settings_page = $hierarhy->getElement($settings_page_id);
        if ($settings_page instanceof umiHierarchyElement) {
            $order_status_id_payment = 115;  // Статус заказа = Оплачивается
            $statusId = $order->status_id;
            $codeName = order::getCodeByStatus($statusId);
            // письма админу при смене статуса заказа мероприятия
            // нет статуса оплаты, но скидка указана
			if ($codeName == 'waiting' && $item_type_id == 226) {
                $subject = "Успешное оформление заявки на консультацию №{$order->number}";
                $template = $settings_page->pismo_menedzheru_o_registracii_na_konsultaciyu; //"neworder_notification_event");
			} elseif (!$paymentStatus && $order->event_discount_value != '') {
                $subject = "Проверка заявки на регистрацию №{$order->number}";
                $template = $settings_page->mail_manager_check_discount; //"neworder_notification_event");
                // есть способ оплаты и статус заказа не равен статусу "Оплачивается"
            } elseif (($order->payment_id && $statusId != $order_status_id_payment) || ($order->total_price == 0 && $codeName == 'waiting')) {
                $subject = "Успешное оформление заявки на регистрацию №{$order->number}";
                $template = $settings_page->mail_manager_successful; //"neworder_notification_event_successful");
            } elseif ($order->partner_event) {
                $subject = "Успешное оформление заявки на партнерское мероприятие. Регистрация №{$order->number}";
                $template = $settings_page->mail_manager_successful_partner_event; //"neworder_notification_event_successful");
            } else {
                return;
            }
        }

        $extParams = array(
            'event_name',
            'event_gorod_in',
            'event_data',
            'event_discount_name',
            'event_discount_value',
        );
        $extParamsCustomer = array(
            'order_lname',
            'order_fname',
            'order_father_name',
            'order_phone',
            'order_e-mail',
            'user_address_city',
            'order_prof_status'
        );

        $originalPrice = $order->getOriginalPrice();
        $actualPrice = $order->getActualPrice();
        if($originalPrice == $actualPrice) {
            $itemsOriginalPrice = 0;
            foreach($order->getItems() as $orderItem) {
                if(!$orderItem instanceof orderItem) {
                    continue;
                }

                $product = $orderItem->getItemElement(true);
                if(!$product instanceof umiHierarchyElement) {
                    continue;
                }

                $itemsOriginalPrice += floatval($product->getValue(SiteCatalogObjectModel::field_price));
            }

            if($itemsOriginalPrice > $originalPrice) {
                $originalPrice = $itemsOriginalPrice;
            }
        }

        $param = array();
        $param["order_id"] = $order->id;
        $param["order_name"] = $order->name;
        $param["order_number"] = $order->number;
        $param["order_upload"] = $order->order_upload;
        $param["payment_type"] = $paymentName;
        $param["payment_status"] = $paymentStatus;
        $param["price_origin"] = $originalPrice;
        $param["price"] = $actualPrice;
        $param["domain"] = $cmsController->getCurrentDomain()->getCurrentHostName();
        $param["item_name"] = $item_name;
        $param["kommentarij"] = $order->getValue('kommentarij');
        $param["prikreplennyj_fajl"] = $order->getValue('prikreplennyj_fajl') instanceof umiFile ? ' <a href="' . $order->getValue('prikreplennyj_fajl')->getFilePath(true) . '" target="_blank">' . $order->getValue('prikreplennyj_fajl')->getFileName() . '</a>' : ' нет';

        //get customer replace data
        $customer_id = $order->customer_id;
        $customer = umiObjectsCollection::getInstance()->getObject($customer_id);
        foreach ($extParams as $extParam) {
            $param[$extParam] = $order->getValue($extParam);
        }
        foreach ($extParamsCustomer as $extParam) {
            $fieldName = str_replace("order_", "", $extParam);
            $value = $customer->getValue($fieldName);
            if($fieldName == 'prof_status') {
                $value = ObjectHelper::getObjectName($value);
            }
            $param[$extParam] = $value;
        }

        $content = def_module::parseTemplateForMail($template, $param);

        $letter->setFrom($fromMail, $fromName);
        $letter->setSubject($subject);
        $letter->setContent($content);
        $letter->commit();
        $letter->send();
    }

    /** @noinspection PhpUnused */
    public function testSendManagerNotificationNew($orderId = null, $email = null)
    {
        $orderId = intval($orderId);
        if($orderId <= 0) {
            throw new publicException('Не передан id заказа');
        }

        $order = order::get($orderId);
        if(!$order instanceof order) {
            throw new publicException('Не найден заказ ' . $orderId);
        }

        $this->sendManagerNotificationNew($order, trim($email));
    }
}