<?php
class emarket_custom_promocode extends def_module {
	public function show_promokod($elementId) {
		$hierarchy = umiHierarchy::getInstance();

		// кастомизация для лендинга с форумом 2018
		$forumEventIds = array(1647,1648,1649,1650,1651,1652,1653);
		$elementId = (in_array($elementId, $forumEventIds)) ? 1647 : $elementId;

		// кастомизация для лендинга с форумом 2018
		$element = $hierarchy->getElement($elementId);
		if (!$element){
			return "0";
		}

		$promocodes_str = $element->promokod_arr;
		$promocodes_arr = json_decode($promocodes_str);
		foreach($promocodes_arr as $promocodeItem){
			$count = $promocodeItem->count;
			$active = $promocodeItem->active;
			if($count > 0 && $active == 1){
				return 1;
			}
		}
		return 0;
	}

	public function check_promokod($elementId, $promocode) {
		$hierarchy = umiHierarchy::getInstance();

		// кастомизация для лендинга с форумом 2018
		$forumEventIds = array(1647,1648,1649,1650,1651,1652,1653);
		$elementId = (in_array($elementId, $forumEventIds)) ? 1647 : $elementId;

		// кастомизация для лендинга с форумом 2018
		$element = $hierarchy->getElement($elementId);
		if (!$element){
			return "0";
		}

		$promocodes_str = $element->promokod_arr;
		$promocodes_arr = json_decode($promocodes_str);
		foreach($promocodes_arr as $promocodeItem){
			$name = $promocodeItem->name;
			$discount = $promocodeItem->discount;
			$discount_abs = $promocodeItem->discount_abs;
			$text = $promocodeItem->text;
			$count = $promocodeItem->count;
			$active = $promocodeItem->active;
			if($count > 0 && $active == 1 && $promocode == $name){
				$price = $element->price;
				$discount_price = $price;
				$discount_str = '';
				if($discount_abs!= '' && $discount_abs >= 0){
					$discount_price = $discount_abs;
				}elseif($discount >= 0){
					$discount_price = $price - ceil($price * ($discount / 100));
					$discount_str = " (скидка " . $discount . "%)";
				}

				if($text == ''){
					$discount_str = ' ';
					$text = 'Применен промокод "' . $name . '"' . $discount_str . '. Стоимость участия - ' . $discount_price . ' руб.';
				}

				return array(
					'result' => 1,
					'name' => $name,
					'discount' => $discount ,
					'discount_abs' => $discount_abs ,
					'price' => $discount_price,
					'text' => $text
				);
			}
		}
		return 0;
	}
}