<?php
class emarket_order_consultation extends def_module {
	/**
	 * Получение формы консультации
	 */
	public function submit_consultation_order_form(){
		$result = array(
			"is_error" => 'false',
			"error_message" => '',
			"referer" => getServer('HTTP_REFERER'),
		);

		//получить и проверить товар
		if (!$elementId) {
			$elementId = getRequest('page_id');
		}

		$customer = customer::get();
		if (!$customer->isUser()) {
			$result['is_error'] = 'true';
			$result['error_message'] = 'Необходимо войти в личный кабинет для записи на консультацию';
			return $result;
		}

		$customer_email = $customer->getValue('e-mail');

		$hierarchy = umiHierarchy::getInstance();
		$controller = cmsController::getInstance();

		$element = $hierarchy->getElement($elementId);
		$isCatalogItem = false;
		if ($element instanceof umiHierarchyElement) {
			$hierarchyType = umiHierarchyTypesCollection::getInstance()->getTypeByName("catalog", "object");
			$hierarchyTypeId = $hierarchyType->getId();

			if ($element->getTypeId() == $hierarchyTypeId) {
				$isCatalogItem = true;
			}
		}
		if (!$isCatalogItem) {
			$result['is_error'] = 'true';
			$result['error_message'] = 'Консультация не найдена';
			return $result;
		}

		$order = $this->getBasketOrder(false);
		// проверяем не перезаписывается ли существующий заказ
		$orderNumber = $order->getNumber();
		if ($orderNumber > 1) {
			$order->order();
			$order = $this->getBasketOrder(false);
		}

		$dataModule = $controller->getModule('data');
		// добавляем товар
		$_REQUEST['no-redirect'] = 1;
		$this->basket('put', 'element', $elementId);
		//очищаем корзину от лишних товаров
		foreach ($order->getItems() as $orderItem) {
			$orderItemElementId = $orderItem->getItemElement()->id;
			if ($orderItemElementId != $elementId) {
				$order->removeItem($orderItem);
			} else {
				if ($orderItem->getAmount() > 1) {
					$orderItem->setAmount(1);
					$orderItem->refresh();
				}
			}
		}
		$order->refresh();

		//сохраняем все сырые данные в заказ (в том числе название и id товара)
		$orderId = $order->id;
		$dataModule->saveEditedObjectWithIgnorePermissions($orderId, true, true);

		// пытаемся дополнить данные пользователя сырыми данными из формы
		$dataForm = getRequest('data');
		$dataForm = (isset($dataForm['new'])) ? $dataForm['new'] : array();

		$oEventPoint = new umiEventPoint("users_register_on_event_do");
		$oEventPoint->setMode("before");
		$oEventPoint->setParam("user_id", $customer->id);
		$this->setEventPoint($oEventPoint);

		foreach ($dataForm as $fieldName => $fieldValue) {
			$fieldName = str_replace("order_", "", $fieldName);
			if (trim($customer->getValue($fieldName)) == '') {
				$customer->setValue($fieldName, $fieldValue);
			}
		}
		$customer->commit();

		$oEventPoint->setMode("after");
		$this->setEventPoint($oEventPoint);

		$check_fields_name = array(
			'order_lname',
			'order_fname',
			'order_father_name',
			'order_e-mail',
			'order_phone',
			'order_country',
			'order_region',
			'order_city',
		);
		foreach ($check_fields_name as $field_name) {
			if (empty(trim($order->getValue($field_name)))) {
				$order->setValue($field_name, $customer->getValue(str_replace("order_", "", $field_name)));
			}
		}

		// сохраняем информацию о мероприятии
		$order->order_date = time();
		$order->page_id = $elementId;
		$order->event_name = $element->h1;

		$order->setValue('need_export', 0);
		$order->setValue('need_export_new', 1);
		$order->order();

		return $result;
	}
}