<?php
class emarket_custom_moderate_order extends def_module {
    // apply order to choose payment
    public function applyOrder($orderId) {
        if (!$orderId){
            $orderId = getRequest('param0');
        }

        $order = order::get($orderId);
        $order->setValue('na_moderacii', 0);
        $order->commit();

        //$order -> setOrderStatus(115);
        // оплачивается
        // инициализированно
        //Инициализирована
        //$orderObj -> setValue("need_export ", 1);
        //$orderObj -> commit();
        //$order -> refresh();
        //Применить скидку
        $this->applyEventDiscount($orderId);

        // отправляем письмо клиенту для выбора способа оплаты
        try {
            $this->sendApplyOrderEmail($order);
        } catch (Exception $e) {}

        return '<h1>Заказ одобрен.</h1>';
    }

    public function sendApplyOrderEmail(order $order, $email = null)
    {
        $customerId = $order->getCustomerId();
        if(!$customerId) {
            throw new publicException('Не указан id покупателя');
        }

        $customer = umiObjectsCollection::getInstance()->getObject($customerId);
        if(!$customer instanceof umiObject) {
            throw new publicException('Не найден объект покупателя');
        }

        $customer->setValue('last_order_sys', $order->getId());
        $customer->commit();

        if(!$email) {
            $email = $customer->getValue('email');
            if(!$email) {
                $email = $customer->getValue('e-mail');
            }
        }

        if(!$email) {
            throw new publicException('Не указан email получателя');
        }

        $name = $customer->getValue('lname') . " " . $customer->getValue('fname') . " " . $customer->getValue('father_name');

        $subject = "Выбор способа оплаты для регистрации №{$order->getNumber()}";
        $content = $this->prepareApplyOrderEmailContent($order);

        $regedit = regedit::getInstance();
        $cmsController = cmsController::getInstance();
        $domains = domainsCollection::getInstance();
        $domainId = $cmsController->getCurrentDomain()->getId();
        $defaultDomainId = $domains->getDefaultDomain()->getId();

        if ($regedit->getVal("//modules/emarket/from-email/{$domainId}")) {
            $fromMail = $regedit->getVal("//modules/emarket/from-email/{$domainId}");
            $fromName = $regedit->getVal("//modules/emarket/from-name/{$domainId}");
        } elseif ($regedit->getVal("//modules/emarket/from-email/{$defaultDomainId}")) {
            $fromMail = $regedit->getVal("//modules/emarket/from-email/{$defaultDomainId}");
            $fromName = $regedit->getVal("//modules/emarket/from-name/{$defaultDomainId}");
        } else {
            $fromMail = $regedit->getVal("//modules/emarket/from-email");
            $fromName = $regedit->getVal("//modules/emarket/from-name");
        }

        $letter = new umiMail();
        $letter->addRecipient($email, $name);
        $letter->setFrom($fromMail, $fromName);
        $letter->setSubject($subject);
        $letter->setContent($content);
        $letter->commit();
        $letter->send();
    }

    public function prepareApplyOrderEmailContent(order $order)
    {
        $settingsPage = umiHierarchy::getInstance()->getElement(765);
        if(!$settingsPage instanceof umiHierarchyElement) {
            throw new publicException('Не найдена страница настроек');
        }

        $customerId = $order->getCustomerId();
        if(!$customerId) {
            throw new publicException('Не указан id покупателя');
        }

        $customer = umiObjectsCollection::getInstance()->getObject($customerId);
        if(!$customer instanceof umiObject) {
            throw new publicException('Не найден объект покупателя');
        }

        $template = $settingsPage->getValue('mail_user_choose_payment');

        $param = [
            'order_id' => $order->getId(),
            'order_number' => $order->getNumber(),
            'domain' => cmsController::getInstance()->getCurrentDomain()->getHost()
        ];
        $extParams = [
            'event_name',
            'event_gorod_in',
            'event_data'
        ];
        $extParamsCustomer = [
            'order_lname',
            'order_fname',
            'order_father_name',
            'order_phone',
            'order_e-mail',
            'order_city'
        ];

        //get customer replace data
        foreach ($extParams as $extParam) {
            $param[$extParam] = $order->getValue($extParam);
        }
        foreach ($extParamsCustomer as $extParam) {
            $fieldName = str_replace("order_", "", $extParam);
            $param[$extParam] = $customer->getValue($fieldName);
        }

        $content = def_module::parseTemplateForMail($template, $param);

        return $content;
    }

    public function testSendApplyOrderEmail($orderId = null, $email = null)
    {
        $orderId = intval($orderId);
        if($orderId <= 0) {
            throw new publicException('Не передан id заказа');
        }

        $order = order::get($orderId);
        if(!$order instanceof order) {
            throw new publicException('Не найден объект заказа ' . $orderId);
        }

        $this->sendApplyOrderEmail($order, $email);

        return 'done';
    }

    public function testPrepareApplyOrderEmailContent($orderId = null)
    {
        $orderId = intval($orderId);
        if($orderId <= 0) {
            throw new publicException('Не передан id заказа');
        }

        $order = order::get($orderId);
        if(!$order instanceof order) {
            throw new publicException('Не найден объект заказа ' . $orderId);
        }

        $buffer = outputBuffer::current('HTTPOutputBuffer');
        if(!$buffer instanceof HTTPOutputBuffer) {
            throw new publicException('Неверный буфер вывода');
        }

        $buffer->clear();
        $buffer->contentType('text/html');
        $buffer->push($this->prepareApplyOrderEmailContent($order));
        $buffer->send();
        exit;
    }

    // apply event discount
    public function applyEventDiscount($orderId, $discount = null) {
        if (!$orderId){
            $orderId = getRequest('orderId');
        }

        $order = order::get($orderId);
        $oldBonus = $order->getValue('bonus');

        $originPrice = $order->getOriginalPrice();

        $orderObj = umiObjectsCollection::getInstance()->getObject($orderId);
        if (is_null($discount)) {
            $discount = floatval($orderObj->event_discount_value);
            $bonus = ($discount > 0) ? $originPrice - $discount : 0;
        } else {
            $discount = floatval($discount);
            $bonus = $originPrice - $discount;
        }

        if (!$orderObj){
            return 'no';
        }
        $orderObj->setValue('bonus', $bonus);
        $orderObj->commit();
        if(!$order->need_export_new && $bonus != $oldBonus && SiteEmarketOrderModel::canExportTo1C($order)) {
            $order->need_export = true;
        }
        $order->refresh();

        return 'ok';
    }

    // apply event discount dismiss
    public function applyEventDiscountDismiss($orderId, $dismissId) {
        if (!$orderId){
            $orderId = getRequest('orderId');
        }

        $order = order::get($orderId);

        $orderObj = umiObjectsCollection::getInstance()->getObject($orderId);
        $dismiss = umiObjectsCollection::getInstance()->getObject($dismissId);

        if (!$orderObj || !$dismiss){
            return 'no';
        }

        $email = $orderObj->getValue('order_e-mail');
        $name = $orderObj->getValue('order_e-mail');
        $event_name = $orderObj->getValue('event_name');
        $subject = 'Cкидки на мероприятие "' . $event_name . '" отклонена';
        if($orderObj->getValue('event_discount_dismiss') == 3331532){
            $subject = 'Места выступающих закончились';
        }
        $template = $dismiss->mail_content;

        $regedit = regedit::getInstance();
        $cmsController = cmsController::getInstance();
        $domains = domainsCollection::getInstance();
        $domainId = $cmsController->getCurrentDomain()->getId();
        $defaultDomainId = $domains->getDefaultDomain()->getId();

        if ($regedit->getVal("//modules/emarket/from-email/{$domainId}")) {
            $fromMail = $regedit->getVal("//modules/emarket/from-email/{$domainId}");
            $fromName = $regedit->getVal("//modules/emarket/from-name/{$domainId}");
        } elseif ($regedit->getVal("//modules/emarket/from-email/{$defaultDomainId}")) {
            $fromMail = $regedit->getVal("//modules/emarket/from-email/{$defaultDomainId}");
            $fromName = $regedit->getVal("//modules/emarket/from-name/{$defaultDomainId}");
        } else {
            $fromMail = $regedit->getVal("//modules/emarket/from-email");
            $fromName = $regedit->getVal("//modules/emarket/from-name");
        }

        $param = array();
        $param["order_id"] = $orderId;
        $param["order_number"] = $orderObj->number;
        $param["domain"] = cmsController::getInstance()->getCurrentDomain()->getHost();


        $extParams = array(
            'event_name',
            'event_gorod_in',
            'event_data'
        );
        $extParamsCustomer = array(
            'order_lname',
            'order_fname',
            'order_father_name',
            'order_phone',
            'order_e-mail',
            'order_city'
        );

        //get customer replace data
        foreach ($extParams as $extParam) {
            $param[$extParam] = $order->getValue($extParam);
        }

        $customer = umiObjectsCollection::getInstance()->getObject($order->getCustomerId());
        foreach ($extParamsCustomer as $extParam) {
            $fieldName = str_replace("order_", "", $extParam);
            $param[$extParam] = $customer->getValue($fieldName);
        }

        $content = def_module::parseTemplateForMail($template, $param);

        $letter = new umiMail();
        $letter->addRecipient($email, $name);
        $letter->setFrom($fromMail, $fromName);
        $letter->setSubject($subject);
        $letter->setContent($content);
        $letter->commit();
        $letter->send();

        $old_mode = umiObjectProperty::$IGNORE_FILTER_INPUT_STRING;
        umiObjectProperty::$IGNORE_FILTER_INPUT_STRING = true;
        $orderObj->setValue('send_content', $content);
        $orderObj->commit();
        umiObjectProperty::$IGNORE_FILTER_INPUT_STRING = $old_mode;

        $order->refresh();
        return 'ok';
    }

    // apply event discount
    public function simplePaymentsList($orderId) {
        if (!$orderId){
            $orderId = getRequest('param0');
        }

        $objects = umiObjectsCollection::getInstance();
        $order = $objects->getObject($orderId);

        if (!$order instanceof umiObject){
            return false;
        }
        if ($order->getTypeGUID() !== 'emarket-order'){
            return false;
        }

        $orderSys = order::get($orderId);
        // оплачивается
        $orderSys->setOrderStatus(115);
        // инициализированно
        $orderSys->setPaymentStatus(65);

        $order->commit();
        $orderSys->refresh();

        $userId = $order->getValue('customer_id');
        $customer = customer::get($userId);
        $cmsController = cmsController::getInstance();
        $domain = $cmsController->getCurrentDomain();
        $domainId = $domain->getId();

        $customer->setLastOrder($orderId, $domainId);
        permissionsCollection::getInstance()->loginAsUser((int) $userId);

        $url = "/emarket/purchase/payment/choose/";
        $this->redirect($url);
        //$this->actAsUserNew($order->getValue('customer_id'), $orderId);
    }

    // TODO DEL not use
    public function actAsUserNew($userId = false, $orderId = false) {
        if (!$userId){
            $userId = getRequest('param0');
        }

        $objects = umiObjectsCollection::getInstance();
        $user = $objects->getObject($userId);
        if (!$user instanceof umiObject) {
            return false;
        }

        session_destroy();

        setcookie('u-login', "", time() - 3600, "/");
        setcookie('u-password', "", time() - 3600, "/");
        setcookie('u-password-md5', "", time() - 3600, "/");
        setcookie('eip-panel-state-first', "", time() - 3600, "/");
        setcookie('customer-id', "", time() - 3600, "/");

        session_start();

        if ($user->getTypeGUID() == 'users-user') {
            $login = $user->getValue("login");
            $password = $user->getValue("password");

            $_SESSION['user_id'] = $userId;
            $_SESSION['cms_login'] = $login;
            $_SESSION['cms_pass'] = $password;

            setcookie('u-login', $login, time() - 3600, "/");
            setcookie('u-password-md5', $password, time() - 3600, "/");
        }

        setcookie('customer-id', $userId, (time() + customer::$defaultExpiration), '/');

        $this->redirect('/emarket/purchase/payment/choose/');
    }
}