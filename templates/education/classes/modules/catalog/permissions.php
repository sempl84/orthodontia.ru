<?php
$permissions = Array(
    'tree' => Array('autoArchive'),
    'view' => array(
		'pdf_dealers', 'gen_speakers', 'gen_events',
		'getSmartCatalogAjax', 'addFilterValueSpeaker', 'google_event',
		'get_events_by_center',
		'autoArchive',
		'autoReplacePriceAndPriceOrigin',
//		'getObjectsListWithCache',
		'get_month_for_calendar',
		'get_month_for_webinar_calendar',
        'get_ormco_main_catalog_list'
	)
);