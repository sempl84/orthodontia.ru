<?php
class catalog_custom extends def_module {
    public function __construct($self) {
        $self->__loadLib("/pdf_dealers.php", (dirname(__FILE__)));
        $self->__implement("catalog_pdf_dealers");

        $self->__loadLib("/analytics.php", (dirname(__FILE__)));
        $self->__implement("catalog_analytics_tags");
    }

    //перенос мероприятия в архив
    public function autoArchive()
    {
        $eventsCategoryId = 4;
        $orthoCategoryId = 748;
        $archiveCategoryId = 269;

        //поиск товара с заданным артикулом по всем товарам и перенос
        $sel = new selector('pages');
        $sel->types('object-type')->name('catalog', 'object');
        $sel->where('hierarchy')->page($eventsCategoryId)->childs(8);
        $sel->where('hierarchy')->page($orthoCategoryId)->childs(8);
        $sel->where('id')->notequals(SiteCatalogObjectModel::test_page_id);
        $sel->where('publish_date')->less(time());
        $sel->order('publish_date')->asc();

        $umiHierarchy = umiHierarchy::getInstance();
        $umiRedirectsCollection = umiRedirectsCollection::getInstance();
        /* @var $umiRedirectsCollection umiRedirectsCollection*/

        $return = array();
        foreach ($sel as $page) {
            if(!$page instanceof umiHierarchyElement) {
                continue;
            }

            $pageId = $page->getId();

            // сброс кол-ва свободных мест на 1 зарезервированных 10, чтобы писал "мест нет"
            $page->setValue('max_reserv', 1);
            $page->setValue('curr_reserv',10);
            $page->commit();

            $oldPath = $umiHierarchy->getPathById($pageId, false, false, true);
            $old_parent = $page->getRel();
            $umiHierarchy->unloadElement($pageId);

            $umiHierarchy->moveBefore($pageId, $archiveCategoryId);
            $umiHierarchy->rebuildRelationNodes($pageId);

            $newPath = $umiHierarchy->getPathById($pageId, false, false, true);
            $page = $umiHierarchy->getElement($pageId);
            $new_parent = $page->getRel();

            $umiRedirectsCollection->add($oldPath, $newPath);

            SiteCatalogObjectModel::addToLog($page, 'Мероприятие отправлено в архив');

            $return[] = array(
                'attribute:id' => $page->getId(),
                'node:name' => $page->getName(),
                'attribute:old_link' => $oldPath,
                'attribute:new_link' => $newPath,
                'attribute:old_parent' => $old_parent,
                'attribute:new_parent' => $new_parent
            );
        }

        return count($return) ? array('items' => array('nodes:item' => $return)) : '';
    }

    //проверяем не прошло ли время действия цены со скидкой (работас полями price и price_origin)
    public function autoReplacePriceAndPriceOrigin()
    {
        $events_pid = 4;
        $archive_pid = 269;
        $hierarchy = umiHierarchy::getInstance();
        //поиск товара с заданным артикулом по всем товарам и перенос
        $sel = new selector('pages');
        $sel->types('object-type')->name('catalog', 'object');
        $sel->where('hierarchy')->page($events_pid)->childs(8);
        $sel->where('hierarchy')->page($archive_pid)->childs(8);
        $sel->where('price_origin_till')->less(time());
        $sel->where('id')->notequals(SiteCatalogObjectModel::test_page_id);
        $sel->option('no-permissions', true);
        $sel->where('price_origin')->isnotnull(true);

        foreach ($sel as $page) {
            $page->price = $page->price_origin;
            $page->price_origin = NULL;
            $page->price_origin_till = NULL;
            //var_dump($page->price_origin);
            $page->commit();
        }
    }

		//проверяем не прошло ли время действия цены со скидкой (работас полями price и price_origin)
    public function autoReplacePriceAndPriceOriginTest() {
    		var_dump(date('D, d M Y H:i:s',time()));
        $events_pid = 4;
        $hierarchy = umiHierarchy::getInstance();
        //поиск товара с заданным артикулом по всем товарам и перенос
        $sel = new selector('pages');
        $sel->types('object-type')->name('catalog', 'object');
        $sel->where('hierarchy')->page($events_pid)->childs(8);
        $sel->where('price_origin_till')->less(time());
				$sel->option('no-permissions',true);
        $sel->where('price_origin')->isnotnull(true);

        foreach ($sel as $page) {
        	var_dump($page->id);
        	var_dump($page->name);
            //$page->price = $page->price_origin;
            //$page->price_origin = NULL;
            //$page->price_origin_till = NULL;
            //$page->commit();
        }
				exit('hhh');
    }

    //запись уровня, тематик, тип мероприятий из программ спец раздела диллеров в спискеров спец раздела  диллеров
    public function testspec() {
        return 'switch off';
        $events_pid = 2260;
        //$archive_pid = 2260;
        $hierarchy = umiHierarchy::getInstance();
        //поиск товара с заданным артикулом по всем товарам и перенос
        $sel = new selector('pages');
        $sel->types('object-type')->name('catalog', 'object');
        $sel->where('hierarchy')->page($events_pid)->childs(8);
        //$sel->limit(0,1);

        foreach ($sel as $page) {
            $speaker = $page->getObject();
            $speaker_id = $speaker->id;

            $program = new selector('pages');
            $program->types('object-type')->name('catalog', 'object');
            $program->where('hierarchy')->page(2259)->childs(8);
            $program->where('speaker')->equals($speaker_id);
            $temas = $levels = $tip_meropriyatiya = array();
            foreach ($program as $item) {
                foreach ($item->tematika_provodimyh_meropriyatij as $tematika) {
                    $temas[$tematika] = $tematika;
                }
                foreach ($item->uroven_meropriyatiya as $tematika) {
                    $levels[$tematika] = $tematika;
                }
                foreach ($item->tip_meropriyatiya as $tematika) {
                    $tip_meropriyatiya[$tematika] = $tematika;
                }
                // $temas[$item->tematika_provodimyh_meropriyatij] = $item->tematika_provodimyh_meropriyatij;
                // $levels[$item->uroven_meropriyatiya] = $item->uroven_meropriyatiya;
                // $tip_meropriyatiya[$item->tip_meropriyatiya] = $item->tip_meropriyatiya;
                var_dump($item->name);
                //var_dump($item->speaker);
                // var_dump($speaker_id);
                // var_dump($page->name);
                // var_dump($ids);
                // $temas = array_merge($temas,$ids);
                // $levels = array_merge($levels,$levels_item);
                // $tip_meropriyatiya = array_merge($tip_meropriyatiya,$tip_meropriyatiya_item);

                var_dump('====');
                //break;
            }
            var_dump($temas);
            var_dump($levels);
            var_dump($tip_meropriyatiya);
            $page->tematika_provodimyh_meropriyatij = $temas;
            $page->uroven_meropriyatiya = $levels;
            $page->tip_meropriyatiya = $tip_meropriyatiya;
            //$element_id = $page->id;
        }
        exit('ggg');
    }

    public function get_events_by_center($parent_id){
        $result = array();
        $pages = new selector('pages');
        $pages->types('hierarchy-type')->name('catalog', 'object');
        $pages->where('hierarchy')->page('/events/')->childs(3);
        $pages->where('uchebnyj_centr')->equals($parent_id);
        $pages->where('publish_date')->more(time());
        $pages->order('publish_date')->asc();
        $pages->limit(0, 3);
        foreach ($pages as $page){
            $result[] = $page;
        }
        return array("lines" => array("nodes:item" => $result));
    }

    public function pdf_certificate($elementId = NULL) {
        if(!$elementId) $elementId = getRequest('id');
        // = 636870;
        // var_dump($ids);
        // exit('exit');
        $uri = "udata://catalog/gen_speakers/".$type."/".$mode."/".$ids."?transform=sys-tpls/dealers_spec/speakers-".$mode.".xsl";
        $result = file_get_contents($uri);

        $buffer = outputBuffer::current();
        $buffer -> charset('utf-8');
        $buffer -> contentType('text/html');
        $buffer -> clear();
        $buffer -> push($result);
        $buffer -> end();
    }

    public function get_ormco_main_catalog_list($elementId = NULL) {
        $url = 'https://ormco.ru/udata/catalog/get_main_catalog_list/.json';
        $post = array();
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        // execute!
        $response = curl_exec($ch);

        curl_close ($ch);

        echo($response);
        die();
    }


	/**
	 * Получение данных по видеозаписям за конкретный месяц
	 * @return array
	 */
	public function get_month_for_webinar_calendar(){
		$result = array();
		$month = getRequest('month');
		$year = getRequest('year');

		$start_period = mktime(0, 0, 0, $month + 1, 1, $year);
		$end_period = mktime(0, 0, 0, $month + 2, 1, $year) - 1;

		$hierarhy = umiHierarchy::getInstance();

		$allowed_parents = array(694);
		foreach($allowed_parents as $one_parent){
			$parent = $hierarhy->getElement($one_parent);
			if(!$parent instanceof umiHierarchyElement){
				continue;
			}

			$pages = new selector('pages');
			$pages->types('object-type')->id(155);
			$pages->where('hierarchy')->page($parent->getId())->childs(10);
			$pages->where('publish_time')->between($start_period, $end_period);
			$pages->option('no-permissions')->value(true);
			foreach($pages as $page){
				$start_date = $page->getValue('publish_time')->getDateTimeStamp();
				$result[] = array(
					'type' => 0,
					'date' => $page->getValue('publish_time')->getFormattedDate('j.n.Y'),
					'link' => $parent->link . '?FilterByDate=' . $start_date,
					'description' => $page->getValue('h1'),
					'periodcss' => ''
				);
			}
		}

		return array('items' => array('nodes:item' => $result));
	}

    /**
	 * Получение данных по всем событиям за конкретный месяц
	 * @return array
	 */
	public function get_month_for_calendar(){
		$result = array();
		$month = getRequest('month');
		$year = getRequest('year');

		$start_period = mktime(0, 0, 0, $month + 1, 1, $year);
		$end_period = mktime(0, 0, 0, $month + 2, 1, $year) - 1;

		$event_types = array(
			'1278' => 0, // Семинар Ormco
			'1279' => 1, // Вебинар
			'1280' => 2, // Семинар Партнера
			'1281' => 3, // Семинар ШОО
			'1282' => 4, // Конференция
			'1971' => 5, // Клуб Ormco
			'1901823' => 6, // Онлайн курс
		);

		$data_module = cmsController::getInstance()->getModule('data');
		$hierarhy = umiHierarchy::getInstance();

		$allowed_parents = array(4, 748, 269);
		foreach($allowed_parents as $one_parent){
			$parent = $hierarhy->getElement($one_parent);
			if(!$parent instanceof umiHierarchyElement){
				continue;
			}

			$pages = new selector('pages');
			$pages->types('hierarchy-type')->name('catalog', 'object');
			$pages->where('hierarchy')->page($parent->getId())->childs(10);
			$pages->option('or-mode')->fields('publish_date', 'finish_date');
			$pages->where('publish_date')->between($start_period, $end_period);
			$pages->where('finish_date')->between($start_period, $end_period);
			foreach($pages as $page){
				$type = isset($event_types[$page->getValue('event_type')]) ? $event_types[$page->getValue('event_type')] : 0;
				$start_date = $page->getValue('publish_date')->getDateTimeStamp();
				$end_date = $page->getValue('finish_date')->getDateTimeStamp();

				$result[] = array(
					'type' => $type,
					'date' => date('j.n.Y', $start_date),
					'link' => $parent->link . $data_module->dateFilterSmart($start_date),
					'description' => $page->getValue('h1'),
					'periodcss' => $start_date == $end_date ? '' : 'calendar_start_date'
				);

				$current_date = $end_date;
				$is_last = true;
				$periodcss = 'calendar_finish_date';
				while ($current_date > $start_date){
					$result[] = array(
						'type' => $type,
						'date' => date('j.n.Y', $current_date),
						'link' => $parent->link . $data_module->dateFilterSmart($current_date),
						'description' => $page->getValue('h1'),
						'periodcss' => $periodcss
					);

					$current_date -= 86400;

					if($is_last){
						$is_last = false;
						$periodcss = 'calendar_middle_date';
					}
				}
			}
		}

		return array('items' => array('nodes:item' => $result));
	}

    /**
     * Переиндексация по крону только самой старой категории
     * @param iUmiEventPoint $event
     * @return boolean
     */
    public function reIndexOnCronCustom(iUmiEventPoint $event) {
        $categories = new selector('pages');
        $categories->types('object-type')->name('catalog', 'category');
        $categories->where(__filter_catalog::FILTER_INDEX_INDEXATION_NEEDED)->equals(true);
        $categories->order('index_date')->asc(true);
        $categories->limit(0, 1);
        if ($categories->length() == 0) {
            return false;
        }

        $umiHierarchy = umiHierarchy::getInstance();
        foreach ($categories as $indexedCategory) {
            $this->reIndexCategory($indexedCategory);
            $umiHierarchy->unloadElement($indexedCategory->getId());
        }

        return true;
    }
}
