<?php
class catalog_pdf_dealers extends def_module {
	// формирование pdf документа с информацией о спикерах или о программах
	public function pdf_dealers($type = NULL , $mode = NULL , $ids = NULL) {
		//var_dump($html);
		//exit('exit');
		if(!$type) $type = getRequest('type');
		if(!$mode) $mode = getRequest('mode');
		if(!$ids) $ids = getRequest('ids');
		// = 636870;
		// var_dump($ids);
		// exit('exit');
		if($type == 'speakers' || $type=='program_speakers'){
			$uri = "udata://catalog/gen_speakers/".$type."/".$mode."/".$ids."?transform=sys-tpls/dealers_spec/speakers-".$mode.".xsl";
		}elseif($type == 'program' || $type == 'program-item' ){
			$uri = "udata://catalog/gen_events/".$type."/".$mode."/".$ids."?transform=sys-tpls/dealers_spec/programs-".$mode.".xsl";
		}else{
			return 'Ошибка генерации pdf';
		}
		//$uri = "udata://catalog/gen_speakers/".$type."/".$mode."/".$ids."?transform=sys-tpls/dealers-speakers-short.xsl";
		//return file_get_contents($uri);

		$result = file_get_contents($uri);
		$buffer = outputBuffer::current();
		$buffer -> charset('utf-8');
		$buffer -> contentType('text/html');
		$buffer -> clear();
		$buffer -> push($result);
		$buffer -> end();
	}



	//генерация информации о спикерах для pdf
	public function gen_events($type = NULL , $mode = NULL , $ids = NULL) {
		$programs_tid = 191;
		$dealers_spec_pid = 2258;

		$umiLinksHelper = $this->umiLinksHelper;
		$umiHierarchy = umiHierarchy::getInstance();

		$extendedProperties = array('tezisy','trebovaniya','speaker','kolvo_dnej','uroven_meropriyatiya','tip_meropriyatiya','gorod_prozhivaniya','tematika_provodimyh_meropriyatij','seminar_na_kafedre');
		$extendedPropertiesSpeaker = array('foto_spikera','pdf_photo');

		if(!$type) $type = getRequest('param0');
		if(!$mode) $mode = getRequest('param1');
		if(!$ids) $ids = getRequest('param2');

		$ids = explode(',',$ids);
		// var_dump($ids);
		// exit('exit');
		//$ids = array(2263,2273);

		$sel = new selector('pages');
		$sel -> types('object-type') -> id($programs_tid);
		$sel->where('hierarchy')->page($dealers_spec_pid)->childs(8);
		$sel->order('ord');
		$sel->option('no-permissions', true);
		if(is_array($ids)) {
			$sel->where('id')->equals($ids);
		}
		$total = $sel->length();

		$lines = Array();
		foreach ($sel as $page) {
			$line_arr = Array();
	     if (count($extendedProperties) > 0) {
				$object = $page->getObject();
	     	$line_arr['extended'] = array();
        $line_arr['extended']['properties'] = array();
        foreach ($extendedProperties as $extendedPropery) {
             $property = $object->getPropByName($extendedPropery);
             if (!$property instanceof umiObjectProperty) continue;
             $line_arr['extended']['properties']['nodes:property'][] = translatorWrapper::get($property)->translate($property);
        }

				$speakerIds = $page->speaker;
				if(isset($speakerIds[0])){
					$speakerId = $speakerIds[0];
					$speakerObj = umiObjectsCollection::getInstance()->getObject($speakerId);
					if ($speakerObj instanceof umiObject) {

						foreach ($extendedPropertiesSpeaker as $extendedPropery) {
		             $property = $speakerObj->getPropByName($extendedPropery);
		             if (!$property instanceof umiObjectProperty) continue;
		             $line_arr['extended']['properties']['nodes:property'][] = translatorWrapper::get($property)->translate($property);
		        }
					}
				}


	     }

			$element_id = $page->id;

			$line_arr['attribute:id'] = $element_id;
			$line_arr['node:name'] = $page->getName();
			$line_arr['attribute:link'] = $umiLinksHelper->getLinkByParts($page);
			$line_arr['xlink:href'] = "upage://" . $element_id;

			$lines[] = $line_arr;

		}

		$block_arr['items'] =  array('nodes:item' => $lines);
		$block_arr['total'] = $total;

		return $block_arr;
	}
	//генерация информации о спикерах для pdf
	public function gen_speakers($type = NULL , $mode = NULL , $ids = NULL) {
		$speakers_tid = 190;
		$programs_tid = 191;
		$dealers_spec_pid = 2258;

		$umiLinksHelper = $this->umiLinksHelper;
		$umiHierarchy = umiHierarchy::getInstance();

		$extendedProperties = array('regalii','opisanie_spikera','foto_spikera','pdf_photo' ,'uroven_meropriyatiya','tip_meropriyatiya','gorod_prozhivaniya','tematika_provodimyh_meropriyatij','seminar_na_kafedre');
		$programsExtendedProperties = array('tezisy','trebovaniya','kolvo_dnej','uroven_meropriyatiya','tip_meropriyatiya','gorod_prozhivaniya','tematika_provodimyh_meropriyatij','seminar_na_kafedre');

		if(!$type) $type = getRequest('param0');
		if(!$mode) $mode = getRequest('param1');
		if(!$ids) $ids = getRequest('param2');

		$ids = explode(',',$ids);
		// var_dump($ids);
		// exit('exit');
		//$ids = array(2263,2273);

		$sel = new selector('pages');
		$sel -> types('object-type') -> id($speakers_tid);
		$sel->where('hierarchy')->page($dealers_spec_pid)->childs(8);
		$sel->order('ord');
		$sel->option('no-permissions', true);
		if(is_array($ids)) {
			$sel->where('id')->equals($ids);
		}
		$total = $sel->length();

		$lines = Array();
		foreach ($sel as $page) {
			$line_arr = Array();
	     if (count($extendedProperties) > 0) {
				$object = $page->getObject();
	     	$line_arr['extended'] = array();
	          $line_arr['extended']['properties'] = array();
	          foreach ($extendedProperties as $extendedPropery) {
	               $property = $object->getPropByName($extendedPropery);
	               if (!$property instanceof umiObjectProperty) continue;
	               $line_arr['extended']['properties']['nodes:property'][] = translatorWrapper::get($property)->translate($property);
	          }
	     }

			$element_id = $page->id;

			$line_arr['attribute:id'] = $element_id;
			$line_arr['node:name'] = $page->getName();
			$line_arr['attribute:link'] = $umiLinksHelper->getLinkByParts($page);
			$line_arr['xlink:href'] = "upage://" . $element_id;

			$programs = new selector('pages');
			$programs -> types('object-type') -> id($programs_tid);
			$programs->where('hierarchy')->page($dealers_spec_pid)->childs(8);
			$programs->option('no-permissions', true);
			$programs->where('speaker')->equals($object->id);

			$programs_total = $programs->length();

			$programs_lines = Array();
			foreach ($programs as $program) {
				$program_line_arr = Array();

				$program_id = $program->id;

				$program_line_arr['attribute:id'] = $program_id;
				$program_line_arr['node:name'] = $program->getName();
				$program_line_arr['attribute:link'] = $umiLinksHelper->getLinkByParts($program);
				$program_line_arr['xlink:href'] = "upage://" . $program_id;

		     if (count($programsExtendedProperties) > 0) {

					$program_object = $program->getObject();
		     	$program_line_arr['extended'] = array();
		          $program_line_arr['extended']['properties'] = array();
		          foreach ($programsExtendedProperties as $extendedPropery) {
		               $property = $program_object->getPropByName($extendedPropery);
		               if (!$property instanceof umiObjectProperty) continue;
		               $program_line_arr['extended']['properties']['nodes:property'][] = translatorWrapper::get($property)->translate($property);
		          }
		     }
				 $programs_lines[] = $program_line_arr;
			}

			$line_arr['programs'] =  array('nodes:program' => $programs_lines);

			$lines[] = $line_arr;

		}

		$block_arr['items'] =  array('nodes:item' => $lines);
		$block_arr['total'] = $total;

		return $block_arr;
	}

	/* доработанная функция вывода каталога
		 * Вывод спикеров из указанного раздела с доп информацией (expProps), для ajax запроса
		 * TODO Есть возможность выводить свежие мероприятия или архивные
		 * */
		public function getSmartCatalogAjax($template = 'default', $categoryId, $limit, $ignorePaging = false, $level = 1, $fieldName = false, $isAsc = true,$speaker = NULL, $extType = NULL) {
			if(!$extType)	$extType = getRequest('param8');


			switch ($extType) {
				case 'speakers':
					$extendedProperties = array('foto_spikera','uroven_meropriyatiya','tip_meropriyatiya','gorod_prozhivaniya','tematika_provodimyh_meropriyatij');

					break;
				case 'program':
					$extendedProperties = array('speaker','kolvo_dnej','uroven_meropriyatiya','tip_meropriyatiya','gorod_prozhivaniya','tematika_provodimyh_meropriyatij','seminar_na_kafedre');
					break;

				case 'program_speakers':
					$extendedProperties = array('speaker','kolvo_dnej','uroven_meropriyatiya','tip_meropriyatiya','gorod_prozhivaniya','tematika_provodimyh_meropriyatij','seminar_na_kafedre');
					break;

				default:
					$extendedProperties = array();
					break;
			}
			if (!is_string($template)) {
				$template = 'default';
			}

			list(
				$itemsTemplate,
				$emptyItemsTemplate,
				$emptySearchTemplates,
				$itemTemplate
			) = def_module::loadTemplates(
				'catalog/' . $template,
				'objects_block',
				'objects_block_empty',
				'objects_block_search_empty',
				'objects_block_line'
			);

			$umiHierarchy = umiHierarchy::getInstance();
			//@var iUmiHierarchyElement $category
			$category = $umiHierarchy->getElement($categoryId);

			if (!$category instanceof iUmiHierarchyElement) {
				throw new publicException(__METHOD__ . ': cant get page by id = '. $categoryId);
			}

			$limit = ($limit) ? $limit : $this->per_page;
			$currentPage = ($ignorePaging) ? 0 : (int) getRequest('p');
			$offset = $currentPage * $limit;

			if (!is_numeric($level)) {
				$level = 1;
			}

			$filteredProductsIds = null;
			$queriesMaker = null;
			if (is_array(getRequest('filter'))) {
				$emptyItemsTemplate = $emptySearchTemplates;
				$queriesMaker = $this->getCatalogQueriesMaker($category, $level);

				if (!$queriesMaker instanceof FilterQueriesMaker) {
					return $this->makeEmptyCatalogResponse($emptyItemsTemplate, $categoryId);
				}

				$filteredProductsIds = $queriesMaker->getFilteredEntitiesIds();

				if (count($filteredProductsIds) == 0) {
					return $this->makeEmptyCatalogResponse($emptyItemsTemplate, $categoryId);
				}
			}

			$products = new selector('pages');
			$products->types('hierarchy-type')->name('catalog', 'object');

			if (is_null($filteredProductsIds)) {
				$products->where('hierarchy')->page($categoryId)->childs($level);
			} else {
				$products->where('id')->equals($filteredProductsIds);
			}

			// add speaker filter
			if (!$speaker)$speaker = getRequest('param7');
			if ($speaker) {
				$products->where('speaker')->equals($speaker);
			}

			if ($fieldName) {
				if ($isAsc) {
					$products->order($fieldName)->asc();
				} else {
					$products->order($fieldName)->desc();
				}
			} else {
				$products->order('publish_date')->desc();
			}

			if ($queriesMaker instanceof FilterQueriesMaker) {
				if (!$queriesMaker->isPermissionsIgnored()) {
					$products->option('no-permissions')->value(true);
				}
			}

			$products->option('load-all-props')->value(true);
			$products->limit($offset, $limit);
			$pages = $products->result();
			$total = $products->length();

			if ($total == 0) {
				return $this->makeEmptyCatalogResponse($emptyItemsTemplate, $categoryId);
			}

			$result = array();
			$items = array();
			$umiLinksHelper = umiLinksHelper::getInstance();
			//@var iUmiHierarchyElement|umiEntinty $page
			foreach ($pages as $page) {
				$item = array();
				$pageId = $page->getId();
				$item['attribute:id'] = $pageId;
				$item['attribute:alt_name'] = $page->getAltName();
				$item['attribute:link'] = $umiLinksHelper->getLinkByParts($page);
				if ($publish_time = $page->getValue('publish_date')) {
					$item['attribute:publish_date'] = $publish_time->getFormattedDate("U");
				}
				$item['xlink:href'] ='upage://' . $pageId;
				$item['node:text'] = $page->getName();

				if (count($extendedProperties) > 0) {
					$object = $page->getObject();
		     	$item['extended'] = array();
		          $item['extended']['properties'] = array();
		          foreach ($extendedProperties as $extendedPropery) {
		               $property = $object->getPropByName($extendedPropery);
		               if (!$property instanceof umiObjectProperty) continue;
		               $item['extended']['properties']['nodes:property'][] = translatorWrapper::get($property)->translate($property);
		          }
		     }

				$items[] = def_module::parseTemplate($itemTemplate, $item, $pageId);
				def_module::pushEditable('catalog', 'object', $pageId);
				$umiHierarchy->unloadElement($pageId);
			}

			$result['subnodes:lines'] = $items;
			$result['numpages'] = umiPagenum::generateNumPage($total, $limit);
			$result['total'] = $total;
			$result['per_page'] = $limit;
			$result['category_id'] = $categoryId;

			return def_module::parseTemplate($itemsTemplate, $result, $categoryId);
		}


		/* доработанная функция вывода каталога
		 * Вывод спикеров из указанного раздела с доп информацией (expProps), для ajax запроса
		 * TODO Есть возможность выводить свежие мероприятия или архивные
		 * */
		public function addFilterValueSpeaker($name = NULL, $value = NULL) {
			$element_id = $this->cmsController->getCurrentElementId();
			$element = umiHierarchy::getInstance()->getElement($element_id);

			if ($element instanceof umiHierarchyElement) {
				$_REQUEST['filter']['speaker']=$element->name;

				//var_dump($_REQUEST);
				//exit('ggg');
				return 'ok';
			}


		}
}