<?php
class catalog_analytics_tags extends def_module {

	// apply ga analytics info
	public function addGaData() {
		$moduleEmarket = cmsController::getInstance()->getModule('emarket');
		return $this->addImpression() . $this->addDetail() . $moduleEmarket->addCheckoutStep1();
	}

	//добавить сптсок товаров в ga если это страница каталога
	public function addImpression() {
		//header('Content-Type: text/html; charset=utf-8');//TODEL

		$permissions = permissionsCollection::getInstance();
		$cmsController = cmsController::getInstance();
		$hierarchy = umiHierarchy::getInstance();

		// если это каталог с товарами
		if(!($cmsController->getCurrentModule() == 'catalog' && $cmsController->getCurrentMethod() == 'category')){
			return '';
		}


		$dataCategory = $parentName ='';
		$limit = $ignorePaging = false;

		// взять текущую страницу
		$currPageId = $cmsController -> getCurrentElementId();


		$currPage = $hierarchy->getElement($currPageId);
		$isChooseBrackets = $currPage->choose_brackets;

		if($isChooseBrackets){
			$limit = 10;
		}

		$limit = ($limit) ? $limit : $this->per_page;
		$currentPage = ($ignorePaging) ? 0 : (int) getRequest('p');
		$offset = $currentPage * $limit;


		//TODO

		// перебрать товары на данной странице
		$catalogPages = self::getSmartCatalogArray($currPageId,$limit,$ignorePaging);

		// получаем navibar для параметра category
		$parents = $hierarchy->getAllParents($currPageId);
		$parents[] = $currentElementId;

		$items = array();
		$firstItem = true;
		foreach($parents as $elementId) {
			if(!$elementId) continue;

			$element = $hierarchy->getElement($elementId);
			if($element instanceof iUmiHierarchyElement) {
				$dataCategory .= (!$firstItem) ? '/' : '';
				$dataCategory .= $element->name;

				$parentName = $element->name;
				$firstItem = false;
			}
		}

		//var_dump($dataCategory);
		//var_dump($catalogPages);
		// создать массив данных
		foreach($catalogPages as $page){
			$dataArtikul = $page->artikul;
			$dataName = $page->name;
			$dataList = $parentName;
			$dataPosition = $offset;

			$pages_info .= <<<END
ga('ec:addImpression', {
 'id': '{$dataArtikul}',
 'name': '{$dataName}',
 'category': '{$dataCategory}',
 'brand': '',
 'list': '{$dataList}',
 'position': {$dataPosition}
});
END;
			$offset++;
		}


		//var_dump($pages_info);
		//exit('sss');
		return (string)$pages_info;//def_module::parseTemplate('%pages%', array('pages'=>$page_info));
		//return $page_info;
	}

	/**
	 * Выводит данные для формирования списка объектов каталога, с учетом параметров фильтрации
	 * @param int $categoryId ид раздела каталога, объекты которого требуется вывести
	 * @param int $limit ограничение количества выводимых объектов каталога
	 * @param bool $ignorePaging игнорировать постраничную навигацию (то есть GET параметр 'p')
	 * @param int $level уровень вложенности раздела каталога $categoryId, на котором размещены необходимые объекты каталога
	 * @param bool $fieldName поле объекта каталога, по которому необходимо произвести сортировку
	 * @param bool $isAsc порядок сортировки
	 * @return mixed
	 */
	public function getSmartCatalogArray($categoryId, $limit, $ignorePaging = false, $level = 1, $fieldName = false, $isAsc = true) {
		$umiHierarchy = umiHierarchy::getInstance();
		$category = $umiHierarchy->getElement($categoryId);

		if (!$category instanceof iUmiHierarchyElement) {
			return;
		}

		$limit = ($limit) ? $limit : $this->per_page;
		$currentPage = ($ignorePaging) ? 0 : (int) getRequest('p');
		$offset = $currentPage * $limit;

		if (!is_numeric($level)) {
			$level = 1;
		}

		$filteredProductsIds = null;
		$queriesMaker = null;
		if (is_array(getRequest('filter'))) {
			$emptyItemsTemplate = $emptySearchTemplates;
			$queriesMaker = $this->getCatalogQueriesMaker($category, $level);

			if (!$queriesMaker instanceof FilterQueriesMaker) {
				return $this->makeEmptyCatalogResponse($emptyItemsTemplate, $categoryId);
			}

			$filteredProductsIds = $queriesMaker->getFilteredEntitiesIds();

			if (count($filteredProductsIds) == 0) {
				return $this->makeEmptyCatalogResponse($emptyItemsTemplate, $categoryId);
			}
		}

		$products = new selector('pages');
		$products->types('hierarchy-type')->name('catalog', 'object');

		if (is_null($filteredProductsIds)) {
			$products->where('hierarchy')->page($categoryId)->childs($level);
		} else {
			$products->where('id')->equals($filteredProductsIds);
		}

		if ($fieldName) {
			if ($isAsc) {
				$products->order($fieldName)->asc();
			} else {
				$products->order($fieldName)->desc();
			}
		} else {
			$products->order('ord')->asc();
		}

		if ($queriesMaker instanceof FilterQueriesMaker) {
			if (!$queriesMaker->isPermissionsIgnored()) {
				$products->option('no-permissions')->value(true);
			}
		}

		$products->option('load-all-props')->value(true);
		$products->limit($offset, $limit);
		$pages = $products->result();
		$total = $products->length();

		if ($total == 0) {
			return;
		}

		return $pages;
	}

	//добавить информацию о товаре в ga если это страница товара
	public function addDetail() {
		//header('Content-Type: text/html; charset=utf-8');//TODEL

		$permissions = permissionsCollection::getInstance();
		$cmsController = cmsController::getInstance();
		$hierarchy = umiHierarchy::getInstance();

		// если это страница товара
		if(!($cmsController->getCurrentModule() == 'catalog' && $cmsController->getCurrentMethod() == 'object')){
			return '';
		}


		$dataCategory = $parentName ='';
		// взять текущую страницу
		$currPageId = $cmsController -> getCurrentElementId();
		//$currPageId = 12760; // TODEL

		$currPage = $hierarchy->getElement($currPageId);

		// получаем navibar для параметра category
		$parents = $hierarchy->getAllParents($currPageId);
		$parents[] = $currentElementId;

		$items = array();
		$firstItem = true;
		foreach($parents as $elementId) {
			if(!$elementId) continue;

			$element = $hierarchy->getElement($elementId);
			if($element instanceof iUmiHierarchyElement) {
				$dataCategory .= (!$firstItem) ? '/' : '';
				$dataCategory .= $element->name;

				$parentName = $element->name;
				$firstItem = false;
			}
		}

		$dataArtikul = $currPage->artikul;
		$dataName = $currPage->name;

		$pages_info .= <<<END
ga('ec:addProduct', {
   'id': '{$dataArtikul}',
   'name': '{$dataName}',
   'category': '{$dataCategory}',
   'brand': ''
});
ga('ec:setAction', 'detail');
END;

		return (string)$pages_info;
	}

	// получить путь до страницы $currPageId
	public function getNavibarCategory($currPageId, $isParent = true) {
		$permissions = permissionsCollection::getInstance();
		$cmsController = cmsController::getInstance();
		$hierarchy = umiHierarchy::getInstance();

		$dataCategory = $parentName ='';
		// если она каталог с товарами
		$currPage = $hierarchy->getElement($currPageId);

		// получаем navibar для параметра category
		$parents = $hierarchy->getAllParents($currPageId);
		if($isParent) $parents[] = $currPageId;

		$items = array();
		$firstItem = true;
		foreach($parents as $elementId) {
			if(!$elementId) continue;

			$element = $hierarchy->getElement($elementId);
			if($element instanceof iUmiHierarchyElement) {
				$dataCategory .= (!$firstItem) ? '/' : '';
				$dataCategory .= $element->name;

				$parentName = $element->name;
				$firstItem = false;
			}
		}

		return (string)$dataCategory;
	}


	// apply yandex analytics info
	public function addMetrikData() {
		//$moduleEmarket = cmsController::getInstance()->getModule('emarket');

		$pages_info = <<<END
window.dataLayer = window.dataLayer || [];
END;

		return (string)$pages_info . $this->addDetail_metrik() ;
	}

	//добавить информацию о товаре в ga если это страница товара
	public function addDetail_metrik() {
		$permissions = permissionsCollection::getInstance();
		$cmsController = cmsController::getInstance();
		$hierarchy = umiHierarchy::getInstance();

		// если это страница товара
		if(!($cmsController->getCurrentModule() == 'catalog' && $cmsController->getCurrentMethod() == 'object')){
			return '';
		}

		$dataCategory = $parentName ='';

		// взять текущую страницу
		$currPageId = $cmsController -> getCurrentElementId();
		$currPage = $hierarchy->getElement($currPageId);

		$dataCategory = $dataCategory = $this->getNavibarCategory($currPageId);


		$dataArtikul = $currPage->artikul;
		$dataName = $currPage->name;
		$originalPrice = $currPage->price;
		$discount = itemDiscount::search($currPage);
		$actualPrice = ($discount instanceof itemDiscount) ? $discount->recalcPrice($originalPrice) : $originalPrice;


		$pages_info .= <<<END
dataLayer.push({
   "ecommerce": {
       "detail": {
           "products": [
               {
                   "id": "{$dataArtikul}",
                   "name" : "{$dataName}",
                   "price": "{$actualPrice}",
                   "brand": "",
                   "category": "{$dataCategory}",
                   }
           ]
       }
   }
});
END;

		return (string)$pages_info;
	}


	//добавить информацию о мероприятии в google event
	public function google_event() {
		//header('Content-Type: text/html; charset=utf-8');//TODEL

		$permissions = permissionsCollection::getInstance();
		$cmsController = cmsController::getInstance();
		$hierarchy = umiHierarchy::getInstance();
		$objectsColl = umiObjectsCollection::getInstance();

		// если это страница товара
		if(!($cmsController->getCurrentModule() == 'catalog' && $cmsController->getCurrentMethod() == 'object')){
			return '';
		}

		// взять текущую страницу
		$currPageId = $cmsController -> getCurrentElementId();
		//$currPageId = 2601; // TODEL

		$currPage = $hierarchy->getElement($currPageId);

		$pages_info = '';
		if($currPage instanceof iUmiHierarchyElement) {
			//$dataArtikul = $currPage->artikul;
			$name = htmlspecialchars($currPage->name);

			$startDate = $endDate = '';
			$publish_date = $currPage -> publish_date;
			if ($publish_date) {
				$publish_date = $publish_date -> getFormattedDate("U");
				$startDate = '"startDate": "'.date("c", $publish_date).'",';
			}
			$finish_date = $currPage -> finish_date;
			if ($finish_date) {
				$finish_date = $finish_date -> getFormattedDate("U");
				$endDate = '"endDate": "'.date("c", $finish_date).'",';
			}

			$gorod_in = $currPage -> gorod_in;
			$gorod_in_object = $objectsColl -> getObject($gorod_in);
			$city = '';
			if ($gorod_in_object) {
				$city = htmlspecialchars($gorod_in_object -> name);
			}
			$street = htmlspecialchars($currPage->mesto_provedeniya);
			$adress = $city . ' ' . $street;
			$description = str_replace(array("\r\n", "\r", "\n"), '', htmlspecialchars(str_replace('\\', ' ', $currPage->description)));
			$referer_url = 'https://orthodontia.ru' . $hierarchy->getPathById($currPageId);
			$price = $currPage->price;

			$speakers = '';
			$speakers_arr = array();
			$speaker_ids = $currPage -> speaker;
			foreach ($speaker_ids as $speaker_id){
				$speaker_id_object = $objectsColl -> getObject($speaker_id);
				if ($speaker_id_object) {
					$speakers_arr[] = htmlspecialchars($speaker_id_object -> name);
				}
			}
			if(sizeof($speakers_arr) > 0){
				$speakers = implode(', ' , $speakers_arr);
			}


			$pages_info .= <<<END
			{
			  "@context": "https://schema.org",
			  "@type": "Event",
			  "name": "{$name}",
			  {$startDate}
			  {$endDate}
			  "location": {
			    "@type": "Place",
			    "name": "{$adress}",
			    "address": {
			      "@type": "PostalAddress",
			      "streetAddress": "{$street}",
			      "addressLocality": "{$city}",
			      "addressCountry": "RU"
			    }
			  },
			  "description": "{$description}",
			  "offers": {
			    "@type": "Offer",
			    "url": "{$referer_url}",
			    "price": "{$price}",
			    "priceCurrency": "RUR",
			    "availability": "https://schema.org/InStock"
			  },
			  "performer": {
			    "@type": "PerformingGroup",
			    "name": "{$speakers}"
			  }
			}
END;

		}
		return (string)$pages_info;
	}
}