<?php
class webforms_custom extends def_module {
	/**
	 * Отправка формы с проверкой на отправку ботов
	 */
    public function send_form() {
		$data = getRequest('data');

		$checkdata = $this->checkdata();

		if (!(isset($checkdata['status']) && $checkdata['status'] == 'successful')) {
			return $checkdata;
		}elseif (isset($data['new']['phone1']) && !empty($data['new']['phone1'])) {
			return array('status' => 'successful', 'message' => 'Спасибо за сообщение! Мы ответим по e-mail в течение 1-2 дней.');
		} else {
			unset($_SERVER['HTTP_REFERER']);
			unset($_REQUEST['ref_onsuccess']);
			$this->send();
			return array('status' => 'successful', 'message' => 'Спасибо за сообщение! Мы ответим по e-mail в течение 1-2 дней.');
		}
	}

	/**
	 * Проверка формы на наличие обязательных полей Ajax-запросом
	 * @param type $typeId
	 * @return type
	 */
	public function checkdata($typeId) {
		if (!$typeId) {
			$typeId = isset($_REQUEST['system_form_id']) ? $_REQUEST['system_form_id'] : false;
		}
		if (!$typeId) {
			return array('status' => 'error', 'code' => 'errorFormId', 'result' => 'form_id error');
		}

		$errorFields = self::checkRequiredAndRestriction($typeId);
		if ($errorFields !== true) {
			return array(
				'status' => 'error',
				'code' => 'errorFields',
				'er' => $errorFields['name'],
				'result' => ' - ' . implode('<br/> - ', $errorFields['title'])
			);
		}
		return array('status' => 'successful');
	}

	/**
	 * Проверяем обязательность полей конкретного типа данных
	 * @param type $typeId
	 * @return type
	 * @throws coreException
	 */
	public function checkRequiredAndRestriction($typeId) {
		$type = umiObjectTypesCollection::getInstance()->getType($typeId);
		if (!$type instanceof umiObjectType) {
			throw new coreException(getLabel('label-cannot-detect-type'));
		}

		$allFields = $type->getAllFields();

		$inputData = getRequest('data');
		if ((!$inputData || !@is_array($inputData['new'])) && (!isset($_FILES['data']['name']['new']))) {
			$inputData = array();
		} else {
			$tmp = array();
			if (@is_array($inputData['new'])){
				$tmp = array_merge($tmp, $inputData['new']);
			}
			if (isset($_FILES['data']['name']['new']) && is_array($_FILES['data']['name']['new'])){
				$tmp = array_merge($tmp, $_FILES['data']['name']['new']);
			}
			$inputData = $tmp;
		}

		$errorFields = array();
		foreach ($allFields as $field) {
			$fieldName = $field->getName();
			$value = $inputData[$fieldName];
			// check required
			if ($field->getIsRequired()) {
				if (!isset($value) || empty($value)) {
					$fieldTitle = $field->getTitle();
					$fieldName = $field->getName();

					$errorFields[] = $fieldTitle;
					$errorFieldsName[] = $fieldName;
					continue;
				}
			}

			// Добавляем проверку на галочку "Я врач" - делаем обязательными поля клиники и адреса
			if($typeId == 207 &&  $inputData['ya_vrach'] == 'true' && ($field->getName() == 'nazvanie_kliniki' || $field->getName() == 'adres_kliniki')){
				if (!isset($value) || empty($value)) {
					$fieldTitle = $field->getTitle();
					$fieldName = $field->getName();

					$errorFields[] = $fieldTitle;
					$errorFieldsName[] = $fieldName;
					continue;
				}
			}

			// Добавляем проверку на согласие с политикой, которое иногда становится false вместо пустого значения
			if($field->getName() == 'allow_personal_data'){
				if (!isset($value) || empty($value) || $value == 'false') {
					$fieldTitle = $field->getTitle();
					$fieldName = $field->getName();

					$errorFields[] = $fieldTitle;
					$errorFieldsName[] = $fieldName;
					continue;
				}
			}

			// check restriction
			if ($restrictionId = $field->getRestrictionId()) {
				$restriction = baseRestriction::get($restrictionId);
				if ($restriction instanceof baseRestriction) {
					if ($restriction instanceof iNormalizeInRestriction) {
						$value = $restriction->normalizeIn($value);
					}

					if ($restriction->validate($value) == false) {
						$fieldTitle = $field->getTitle();
						$fieldName = $field->getName();

						$errorFields[] = " \"{$fieldTitle}\" - " . $restriction->getErrorMessage();
						$errorFieldsName[] = $fieldName;
					}
				}
			}
		}
		return !empty($errorFields) ? array('title' => $errorFields, 'name' => $errorFieldsName) : true;
	}
}