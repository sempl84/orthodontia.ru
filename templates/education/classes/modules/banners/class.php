<?php
class banners_custom extends def_module {

	/**
	 * Макрос быстрой вставки баннера на страницу.
	 * Отличается меньшей, по сравнению с banners::insert() функциональностью.
	 * @param string $placeName наименование места для вставки
	 * @return mixed|string|void
	 */
	public function fastInsertCatalogItem($placeName) {

		$placeId = $this -> getPlaceIdCatalogItem($placeName);
		if (!is_array($placeId) || !isset($placeId[0])) {
			//return "ttt";
		}
		//return 'aa235';
		$placeId = $placeId[0];
		$banners = new selector('objects');
		$banners -> types('hierarchy-type') -> name('banners', 'banner');
		$banners -> where('place') -> equals($placeId);
		$banners -> where('is_active') -> equals(true);
		$banners -> option('no-length') -> value(true);
		$banners -> option('load-all-props') -> value(true);
		$banners -> order('rand');
		$banners = $banners -> result();

		if (count($banners) == 0) {
			return;
		}

		foreach ($banners as $banner) {

			if (!$banner instanceof umiObject) {
				continue;
			}

			if (!$this -> checkIfValidParentCatalogItem($banner -> getValue('view_pages'), $banner -> getValue('not_view_pages'))) {
				continue;
			}

			if ($renderedBanner = $this -> renderBannerCatalogItem($banner -> getId())) {
				return $renderedBanner;
			}

		}
	}

	/**
	 * Возвращает идентификатор места показа баннера по его названию
	 * @param string $placeName ид места показа баннера
	 * @return array|bool
	 */
	protected function getPlaceIdCatalogItem($placeName) {
		static $cache = Array();
		$placeName = (string)$placeName;

		if (isset($cache[$placeName])) {
			return array(0 => (int)$cache[$placeName]);
		}

		$places = new selector('objects');
		$places -> types('hierarchy-type') -> name('banners', 'place');
		$places -> option('no-length') -> value(true);
		$places -> option('load-all-props') -> value(true);
		$places = $places -> result();

		if (count($places) == 0) {
			return false;
		}

		foreach ($places as $place) {
			if (!$place instanceof umiObject) {
				continue;
			}
			$cache[$place -> getName()] = $place -> getId();
		}

		if (isset($cache[$placeName])) {
			return array(0 => (int)$cache[$placeName]);
		}

		return false;
	}

	/**
	 * Валидирует текущую страниц и ее родителей по настройкам баннера.
	 * Возращает результат - показывать или нет.
	 * @param array $pages массив с ид страниц, на которых нужно показывать баннер
	 * @param array $notPages  массив с ид страниц, на которых не нужно показывать баннер
	 * @return bool
	 */
	protected function checkIfValidParentCatalogItem($pages, $notPages) {

		$currentPageId = $this -> cmsController -> getCurrentElementId();
		if (count($notPages)) {
			foreach ($notPages as $notPage) {
				if ($notPage -> getId() == $currentPageId)
					return false;
			}
		}

		if (!is_array($pages) || sizeof($pages) == 0) {
			return true;
		}

		$parents = $this -> getCurrentParents();

		foreach ($pages as $page) {
			if (in_array($page -> getId(), $parents)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Подготавливает данные баннера к шаблонизации
	 * @param integer $iObjId ид объекта баннера
	 * @return mixed|string
	 */
	protected function renderBannerCatalogItem($iObjId) {
		$block_arr = Array();
		$sResult = "";
		$oBanner = $this -> umiObjectsCollection -> getObject($iObjId);
		if ($oBanner instanceof umiObject) {
			$sBannerType = "";
			if ($oBanner -> getValue('catalog_item') !== false) {
				$banner = Array();
				$banner['attribute:type'] = 'catalog_item';

				$item = $items = array();
				$pages = $oBanner -> getValue('catalog_item');
				//var_dump($pages);
				//exit('aaa');
				//$extendedProperties = def_module::getMacrosExtendedProps();
				$extendedProperties = explode(",", getRequest('extProps'));
				foreach ($pages as $page) {
					$pageId = $page -> getId();
					$object = $page -> getObject();
					//var_dump($extendedProperties);

					$item['attribute:id'] = $pageId;
					$item['attribute:alt_name'] = $page -> getAltName();
					$item['attribute:link'] = umiLinksHelper::getInstance() -> getLinkByParts($page);
					$item['xlink:href'] = 'upage://' . $pageId;
					$item['node:text'] = $page -> getName();

					if (count($extendedProperties) > 0) {
						$data['extended']['properties'] = array();
						foreach ($extendedProperties as $extendedPropery) {
							$property = $object -> getPropByName($extendedPropery);
							if (!$property instanceof umiObjectProperty)
								continue;
							$item['extended']['properties']['nodes:property'][] = translatorWrapper::get($property) -> translate($property);
						}
					}

					$items[] = def_module::parseTemplate('', $item, $pageId);
					def_module::pushEditable('catalog', 'object', $pageId);
					umiHierarchy::getInstance() -> unloadElement($pageId);

					//exit('bbn');
					$banner['subnodes:lines'] = $items;

					break;
				}

				$block_arr['banner'] = $banner;
			}

			$iOldViewsCount = $oBanner -> getValue('views_count') + 1;
			$oBanner -> views_count = $iOldViewsCount;
			$this -> updatedBanners[] = $oBanner;

			if ($this -> disableUpdateOpt) {
				$this -> saveUpdates();
			}
		}

		$block_arr['attribute:id'] = $iObjId;
		if (isset($block_arr['banner'])) {
			$block_arr['banner']['xlink:href'] = "uobject://" . $iObjId;
		}
		$sResult = def_module::parseTemplate($sResult, $block_arr, false, $iObjId);
		return $sResult;
	}
}