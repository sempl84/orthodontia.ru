<?php
session_start();
header("HTTP/1.1 200 OK");
header('Content-Type: text/html; charset=UTF-8');

$pageurl = "https://orthodontia.ru/forum2018/";
$states = array(0 => "coffee.png", 1 => "speaker.jpg", 2 => "lunch.png", 3 => "ortho.jpg", 4 => "champ.jpg");
$programm = array(
	1 => array(	"date" => "2018-06-01",
				"time_start" => "9:00",
				"time_end" => "10:00",
				"name" => "Регистрация, welcome-coffee",
				"state" => 0,
				"speaker" => 0
	),
	2 => array(	"date" => "2018-06-01",
				"time_start" => "10:00",
				"time_end" => "11:30",
				"name" => "<strong>Завершение случаев с Damon System</strong><br />
<ul><li>Моя собственная эволюция: от лигатурных брекет-систем к самолигированию. Основные причины смены курса на всю жизнь.</li>
<li>Все точки над i в выборе торка: в каких случаях какой торк выбирать и почему?</li></ul>",
				"state" => 1,
				"speaker" => 2
	),
	3 => array(	"date" => "2018-06-01",
				"time_start" => "11:30",
				"time_end" => "12:00",
				"name" => "Кофе-брейк",
				"state" => 0,
				"speaker" => 0
	),
	4 => array(	"date" => "2018-06-01",
				"time_start" => "12:00",
				"time_end" => "13:30",
				"name" => "<strong>Завершение случаев с Damon System</strong><br />
<ul><li>Скученность  Использование аппарата D-Gainer.</li>
<li>Когда удалять? Изменение парадигмы.</li>
<li>Трансверзальная дизокклюзия. Перекрестный прикус.</li></ul>",
				"state" => 1,
				"speaker" => 2
	),
	5 => array(	"date" => "2018-06-01",
				"time_start" => "13:30",
				"time_end" => "14:30",
				"name" => "Обед",
				"state" => 2,
				"speaker" => 0
	),
	6 => array(	"date" => "2018-06-01",
				"time_start" => "14:30",
				"time_end" => "16:00",
				"name" => "<strong>Завершение случаев с Damon System</strong><br />
Сагиттальная дизокклюзия. Лечения II и III класса:
<ul>
<li>Возможности эластиков</li>
<li>Ортодонтический камуфляж для хирургических кейсов</li></ul>",
				"state" => 1,
				"speaker" => 2
	),
	7 => array(	"date" => "2018-06-01",
				"time_start" => "16:00",
				"time_end" => "16:30",
				"name" => "Кофе-брейк",
				"state" => 0,
				"speaker" => 0
	),
	8 => array(	"date" => "2018-06-01",
				"time_start" => "16:30",
				"time_end" => "18:00",
				"name" => "<strong>Завершение случаев с Damon System</strong><br />
Вертикальная дизокклюзия:
<ul>
<li>Глубокое резцовое перекрытие</li>
<li>Открытый прикус</li></ul>",
				"state" => 1,
				"speaker" => 2
	),
	9 => array(	"date" => "2018-06-02",
				"time_start" => "8:30",
				"time_end" => "10:00",
				"name" => "Орто-завтрак c Ormco «Первичная консультация: междисциплинарный подход» с участием ведущих российских спикеров разных специализаций: А.В. Тихонов, Я.Ю. Дьячкова, Л.М. Эмдин",
				"state" => 3,
				"speaker" => 0
	),
	10 => array("date" => "2018-06-02",
				"time_start" => "10:00",
				"time_end" => "11:30",
				"name" => "Как справляться с ловушками ортодонтии – от раннего лечения до дисфункции ВНЧС<br /><small>Формирование неправильного прикуса – раннее распознавание и профилактика</small>",
				"state" => 1,
				"speaker" => 1
	),
	11 => array("date" => "2018-06-02",
				"time_start" => "11:30",
				"time_end" => "12:00",
				"name" => "Кофе-брейк",
				"state" => 0,
				"speaker" => 0
	),
	12 => array("date" => "2018-06-02",
				"time_start" => "12:00",
				"time_end" => "13:30",
				"name" => "Как справляться с ловушками ортодонтии – от раннего лечения до дисфункции ВНЧС<br /><small>Развитие зубных дуг в трех плоскостях относительно положения нижней челюсти: от А до Я. Аппарат D-Gainer</small>",
				"state" => 1,
				"speaker" => 1
	),
	13 => array("date" => "2018-06-02",
				"time_start" => "13:30",
				"time_end" => "14:30",
				"name" => "Обед",
				"state" => 2,
				"speaker" => 0
	),
	14 => array("date" => "2018-06-02",
				"time_start" => "14:30",
				"time_end" => "16:00",
				"name" => "Как справляться с ловушками ортодонтии – от раннего лечения до дисфункции ВНЧС<br /><small>Практический подход к диагностике, лечению и профилактике дисфункции ВНЧС</small>",
				"state" => 1,
				"speaker" => 1
	),
	15 => array("date" => "2018-06-02",
				"time_start" => "16:00",
				"time_end" => "16:30",
				"name" => "Кофе-брейк",
				"state" => 0,
				"speaker" => 0
	),
	16 => array("date" => "2018-06-02",
				"time_start" => "16:30",
				"time_end" => "17:30",
				"name" => "Как справляться с ловушками ортодонтии – от раннего лечения до дисфункции ВНЧС<br /><small>Асимметрия и ее влияние на положение нижней челюсти и ВНЧС</small>",
				"state" => 1,
				"speaker" => 1
	),
	17 => array("date" => "2018-06-02",
				"time_start" => "19:00",
				"time_end" => "23:00",
				"name" => "Гала-ужин",
				"state" => 4,
				"speaker" => 0
	),
	18 => array("date" => "2018-06-03",
				"time_start" => "9:00",
				"time_end" => "10:00",
				"name" => "Welcome-coffee",
				"state" => 0,
				"speaker" => 0
	),
	19 => array("date" => "2018-06-03",
				"time_start" => "10:00",
				"time_end" => "12:00",
				"name" => "<strong>Уникальные возможности системы Insignia</strong>",
				"descr" => "
Insignia – это современная CAD/CAM система, которая позволяет увидеть итоговый результат лечения еще до его начала. На примере клинических случаев, демонстрирующих динамику лечения с каждым посещением, доктор Козловски расскажет, как можно вывести эффективность клинической практики и результаты лечения на совершенно новый уровень с помощью Insignia.
<ol>
<li>Учимся определять, для каких клинических случаев Insignia подходит лучше всего (подсказки, на что обращать внимание).</li>
<li>Как рациональность и эффективность механики Damon могут повлиять на качество ваших результатов и профитабельность практики.</li>
<li>Научитесь получать удовольствие от своей практики как от вождения Феррари благодаря четким протоколам лечения и грамотно составленному графику посещений.</li>
<li>Разобщение и эластики как кульминация эффективного лечения.</li>
<li>Как сделать сложные случаи не такими уж сложными с помощью системы Insignia и механики Damon.</li></ol>
",
				"state" => 1,
				"speaker" => 3
	),
	20 => array("date" => "2018-06-03",
				"time_start" => "12:00",
				"time_end" => "13:00",
				"name" => "Обед",
				"state" => 2,
				"speaker" => 0
	),
	21 => array("date" => "2018-06-03",
				"time_start" => "13:00",
				"time_end" => "15:00",
				"name" => "<strong>Брекеты Alias – будущее лингвальной ортодонтии</strong>",
				"descr" => "
Лингвальная техника прямой дуги была разработана докторами Джузеппе Скуццо и Киото Такемото для изменения традиционной «грибовидной» формы лингвальных дуг, на которые довольно сложно наносить детализирующие изгибы и которые могут ухудшать механику скольжения.
<br /><br />
Для компенсации этих нюансов был введен новый метод лингвальной техники прямой дуги (Lingual Straight Wire) в сочетании с пассивной самолигирующей конструкцией с квадратным пазом (Alias). Эта техника подразумевает плавную форму дуги и сводит к минимуму необходимость нанесения изгибов, пассивные самолигирующие брекеты призваны обеспечивать лучший контроль ротаций и торка и значительно улучшить скользящую механику. Лабораторный этап, включающий в себя создание цифрового сетапа позволяет изготовить индивидуализированную аппаратуру для максимально точных и предсказуемых результатов лечения.
<br /><br />
Презентация c обзором клинических случаев различной сложности познакомит участников с этим ноу-хау и продемонстрирует все возможности системы Alias.
<br /><br />
Доктор Джузеппе Скуццо – один из самых известных специалистов в области лингвальной ортодонтии. Он не просто лечит пациентов с использованием лингвальных брекетов. Он оставляет свое имя в истории. На Ormco Форуме 2018 Alias будут впервые представлены российской аудитории.
<ul>
<li>Концепция лингвальной техники прямой дуги</li>
<li>Конструкция пассивных самолигирующих брекетов Alias</li>
<li>Особенности биомеханики при использовании квадратного паза. Сравнение с прямоугольным пазом в лигатурных брекет-системах</li>
<li>Знакомство с системой Alias: программа 3D-viewer, планирование лечения</li>
<li>Особенности фиксации и снятия брекетов</li>
<li>Протокол смены дуг</li>
<li>Разбор клинических случаев</li></ul>",
				"state" => 1,
				"speaker" => 5
	),
	22 => array("date" => "2018-06-03",
				"time_start" => "15:00",
				"time_end" => "15:30",
				"name" => "Кофе-брейк",
				"state" => 0,
				"speaker" => 0
	),
	23 => array("date" => "2018-06-03",
				"time_start" => "15:30",
				"time_end" => "17:30",
				"name" => "<strong>Шах и мат сложным случаям</strong>",
				"descr" => "
В своей лекции доктор Ангильери проводит параллель между шахматной партией и ортодонтическим лечением. В обоих случаях важна выверенная стратегия. В этой лекции доставляет удовольствие все: детальный разбор клинических случаев, множество «трюков», представленных в виде анимированных роликов, стиль подачи информации – о сложном простыми словами и с тонким юмором, дизайн презентации в стилистике шахмат. Доктор Ангильери не стремится показать только красивые картинки – он открыто делится своими ошибками и подсказывает, как их можно избежать. Возможно, именно этим доктор Ангильери так полюбился публике: ведь теперь мы видим его имя в программе всех ближайших крупных мероприятий Ormco по всему миру. Не упустите возможность услышать его выступление в России!
<br /><br />
Диагностика, лечение клинических случаев с различными аномалиями окклюзии:
<ul><li>II Класс</li>
<li>III Класс</li>
<li>Открытый прикус</li>
<li>Сильная скученность</li>
<li>Узкие зубные дуги</li>
</ul>
Простые и эффективные решения трудностей, возникающих в ежедневной практике врача-ортодонта",
				"state" => 1,
				"speaker" => 4
	),
);
$allprices = array(
	1 => array(	"name" => "Форум",
				"price1" => "45000",
				"price2" => "55000",
	),
	2 => array(	"name" => "Орто-завтрак",
				"price1" => "2000",
				"price2" => "2000",
	),
	3 => array(	"name" => "Гала-ужин",
				"price1" => "7500",
				"price2" => "7500",
	),
);
$ids = array(
	"001" => 1651,
	"010" => 1652,
	"011" => 1653,
	"100" => 1649,
	"101" => 1650,
	"110" => 1647,
	"111" => 1648,
);
$discounts = array(
	1 => array(	"name" => "Скидка для членов ПОО",
				"discount" => 15
	)
);
$speakers = array(
	1 => array(	"name" => "Элизабет Менцель",
				"short" => "Хершинг-ам-Аммерзе, Германия",
				"about" => "Получила специализацию ортодонта во Франкфуртском университете, прошла обучение в частной клинике д-ра König-Tol Kronberg, д-ра Toll в Bad Soden, Германия и у профессора Axel Bumann в Kiel с углубленным изучением патологии ВНЧС и ее лечения. Последние 15 лет практикует в частной клинике в городе Хершинг-ам-Аммерзе, Германия. Занималась ортодонтическим лечением пациентов с тяжелой формой патологии ВНЧС в Великобритании и Литве. Международный спикер компании Ormco с 2006 года. Пользователь системы Damon с 2003 г.",
				"image" => "menzel.jpg",
	),
	2 => array(	"name" => "Рамон Перера",
				"short" => "Льеда, Испания",
				"about" => "Доктор Перера окончил университет Барселоны, получив сертификаты в области медицины и хирургии в 1981 году, затем продолжил обучение и получил сертификат по стоматологии в 1983 году. В 1986 году он окончил последипломное образование по специальности ортодонтия в Европейском ортодонтическом центре в Мадриде. С 1995 по 1997 годы он проводил цикл исследовательских работ под эгидой центра Рота-Вильямса. Доктор Перера был одним из первых последователей философии Damon System в Испании. Впервые он применил Damon System в 2001 году, и после его визита в хирургическую клинику доктора Дэймона в Спокэйн, он применяет исключительно данную систему в своих клиниках в Льеде и Таррагоне. Автор целого ряда научных статей, член многих ортодонтических ассоциаций, известный лектор и участник многочисленных курсов и конгрессов",
				"image" => "perera.jpg",
	),
	3 => array(	"name" => "Джефф Козловски",
				"short" => "Коннектикут, США",
				"about" => "Д-р Козловски получил степень бакалавра на экономическом факультете в Университете Сиракуз (Нью-Йорк, США), после этого получил сертификат по ортодонтии в Нью-Йоркском Государственном Университете в г. Буффало. Является автором многочисленных публикаций в ортодонтических журналах, включая такие журналы как Seminars in Orthodontics, Journal of Clinical Orthodontics, Clinical Impressions and The Progressive Orthodontist. На протяжении 10 лет проводит обучение по системе Damon, и 8 лет – по системе Insignia. Д-р Козловски ведет частную практику в Новом Лондоне и Ист Лайме (США). Д-р Козловски очень востребованный лектор по всему миру, в своих лекциях раскрывает такие темы как “Цифровая ортодонтия”, “Эстетика лица”, “Эффективность раннего лечения”, “Механика ортодонтического лечения и клиническая эффективность”",
				"image" => "kozlowski.jpg",
	),
	5 => array(	"name" => "Джузеппе Скуццо",
				"short" => "Рим, Италия",
				"about" => "Джузеппе Скуццо окончил медицинский факультет Римского университета в 1983 году. В 1987 году получил специализацию врача-стоматолога в этом же университете, а также ортодонта – в Университете Феррары. Является автором многочисленных публикаций и признанным специалистом в области лингвальной ортодонтии. Основатель итальянского общества лингвальной ортодонтии и бывший  президент Европейского общества лингвальной ортодонтии. Является профессором Университета Феррары и доцентом в Нью-Йоркском университете. Много лет сотрудничает с доктором Киото Такемото (Япония), совместно с которым разработал лингвальные брекеты STb, а затем первые лингвальные пассивные самолигирующие брекеты – Alias",
				"image" => "scuzzo.jpg",
	),
	4 => array(	"name" => "Матиас Ангильери",
				"short" => "Буэнос-Айрес, Аргентина",
				"about" => "Д-р Ангильери получил образование врача-стоматолога в Университете Буэнос Айреса. Является известным специалистом в области ортодонтии и функциональной ортопедии в Аргентине. Член Международной Федерации Ортодонтов. Преподавал стоматологию, вел курсы по технике прямой дуги, цифровой фотографии в ортодонтии в различных университетах Аргентины. Является приглашенным лектором на кафедре ортодонтии Католического университета Аргентины. Сертифицированный лектор по Damon System. Регулярно выступает с лекциями и семинарами в различных странах Латинской Америки: Колумбии, Эквадоре, Перу, Боливии, Парагвае и др.",
				"image" => "anghillieri.jpg",
	),
);
$speakers2 = array(
	1 => array(	"name" => "Андрей Тихонов",
				"about" => "Врач-ортодонт и консультант по применению продукции ORMCO. Учредитель и генеральный директор стоматологического центра «Полный Порядок» (Санкт-Петербург). Ассистент кафедры ортодонтии СЗГМУ им. И.И. Мечникова. Окончил СПбГМУ им. акад. И.П. Павлова в 2000 году, в 2003 г. прошел ординатуру по ортодонтии. <br />

Опубликовал 10 статей и 3 учебника по фундаментальным вопросам ортодонтии. <br />
Участник десятков международных ортодонтических конгрессов и стажировок. Провел сотни обучающих семинаров в более чем 20 городах России. Регулярно читает лекции для врачей-ортодонтов за рубежом. Вместе с братом основал проект «Школа ортодонтии ORMCO». <br />
Сертифицированный европейский лектор по тематике Damon System. <br />
Опыт работы с Damon System – c 2004 года.",
				"image" => "tikhonov.jpg",
	),
	2 => array(	"name" => "Яна Дьячкова",
				"about" => "Врач-ортодонт, кандидат медицинских наук. В 1999 году окончила МГМСУ им. А.И. Евдокимова с отличием. С 1999 по 2001 год обучалась в клинической ординатуре на кафедре ортодонтии и детского протезирования МГМСУ, в дальнейшем — в аспирантуре на той же кафедре. В 2009 году защитила кандидатскую диссертацию на тему «Совершенствование диагностики аномалий зубных рядов посредством компьютерных технологий». За время работы на кафедре ортодонтии и детского протезирования МГМСУ вела научную, преподавательскую деятельность, участвовала в многочисленных конференциях и съездах, была переводчиком-референтом журнала «Ортодонтия», принимала участие в подготовке кафедральных реферативных конференций. Является переводчиком с английского двух зарубежных монографий, впервые изданных в России: М. Б. Экермен «Ортодонтическое лечение. Теория и практика»; К. Г. Исааксон с соавторами «Съемные ортодонтические аппараты».

Неоднократно участвовала в конференциях и съездах врачей-ортодонтов, посещала семинары и лекции, посвященные работе брекет-системами разных производителей, особенностям лечения ортодонтических аномалий лингвальными брекетами, ортодонтическими имплантами, элайнерами, связи ортодонтических аномалий с нарушениями осанки, коррекции дисфункций ВНЧС. Предпочтение в клинической практике отдает системе Damon.",
				"image" => "dyachkova.jpg",
	),
	3 => array(	"name" => "Леонид Эмдин",
				"about" => "Врач-стоматолог-ортопед, ортодонт, высшая квалификационная категория. В 2000 году с отличием окончил стоматологический факультет СПбГМУ им. акад. И.П.Павлова. В 2001 году окончил интернатуру СЗГМУ им. И.И. Мечникова на кафедре «Терапевтической стоматологии №2» по специальности «Стоматология общей практики». В 2003 году окончил ординатуру СЗГМУ им. И.И. Мечникова на кафедре «Терапевтической стоматологии №2» под руководством профессора Б.Т. Мороза по специальности «Стоматология терапевтическая». В 2003 году получил дополнительную специализацию на курсе профессиональной переподготовки «Современные аспекты ортопедической стоматологии с курсом ортодонтии» в СЗГМУ им. И.И. Мечникова на кафедре «Ортопедической стоматологии» под руководством профессора А.В. Цимбалистова.

В 2005 году прошёл специализацию по ортодонтии в СПбГМУ им.акад. И.П. Павлова. Работает врачом-стоматологом-ортопедом, ортодонтом в ООО «Райден». С февраля 2017 года занимает должность заместителя главного врача по оказанию платной медицинской помощи в СПб ГБУЗ ГСП №33. Участник большого количества обучающих мероприятий в России и за рубежом.",
				"image" => "emdin.jpg",
	)
);
$mkcounts = explode("/", file_get_contents("mkcounts.txt"));
?>
<!DOCTYPE html>
<html>
	<head>
		<title>ОРМКО ФОРУМ 2018 1 - 3 июня 2018 года</title>
		<meta http–equiv="Content–Type" content="text/html; charset=UTF–8">
		<link rel="stylesheet" media="screen" type="text/css" href="main.css">
		<meta name="keywords" content="">
		<meta name="description" content="">
		<meta name="HandheldFriendly" content="true">
		<meta name="MobileOptimized" content="width">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="/favicon.gif" type="image/x-icon">
		<link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="js/jquery.touchSwipe.min.js" type="text/javascript"></script>
		<script src="js/ajaxupload.3.5.js" type="text/javascript"></script>
		<script src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyBoDhMbGeceqxv4_7SROd3YL1-0C-SyBGo" type="text/javascript"></script>
		<script>
			var userid = "";
			var price = 0;
			var promo_discount_val = 0;
			var promo_discount_abs = 0;
			var discount_val = 0;
			ids = new Array();
			eids = new Array();
			eids[1] = "0";
			eids[2] = "0";
			eids[3] = "0";
			<?
			foreach ($ids as $num => $id)
				{
				?>
				ids["<?=$num;?>"] = <?=$id;?>;
				<?
			}
			?>
			function number_format( number, decimals, dec_point, thousands_sep ) {	// Format a number with grouped thousands
				// 
				// +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
				// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
				// +	 bugfix by: Michael White (http://crestidg.com)

				var i, j, kw, kd, km;

				// input sanitation & defaults
				if( isNaN(decimals = Math.abs(decimals)) ){
					decimals = 2;
				}
				if( dec_point == undefined ){
					dec_point = ",";
				}
				if( thousands_sep == undefined ){
					thousands_sep = ".";
				}

				i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

				if( (j = i.length) > 3 ){
					j = j % 3;
				} else{
					j = 0;
				}

				km = (j ? i.substr(0, j) + thousands_sep : "");
				kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
				//kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
				kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");


				return km + kw + kd;
			}
			function getprice(num, pr, state) {
				if (num == 1) {
					if (state == 1) {
						$("#discounts").show();
						$("#nodiscounts").hide();
						$("#speciality").show();
					}
					else {
						$("#discounts").hide();
						$("#nodiscounts").show();
						$("#speciality").hide();
					}
				}
				if (state) {
					price = price + parseInt(pr);
					eids[num] = "1";
				}
				else {
					price = price - parseInt(pr);
					eids[num] = "0";
				}
				$("#eprice").text(number_format(price, 0, ".", " ") + " р.");
				$("#regularprice").val(price);
				$("#finalprice").text(price + " р.");
				$("#totalprice").val(price);
				var eid = "" + eids[1] + "" + eids[2] + "" + eids[3];
				// alert(eid);
				$("#events").val(ids[eid]);
				var ev = ids[eid];
				$.ajax({
					url:"https://orthodontia.ru/udata/emarket/show_promokod/" + ev + "/.json",
					xhrFields: {
					  withCredentials: true
					},
					dataType: "json",
					success:function(data) {
						if (data.result == 1) {
							// есть промокоды, показываем поле
							$("#promo_field").show();
						}
					}
				});
				if (price == 0)
					$("#startreg").attr("disabled", "disabled");
				else
					$("#startreg").removeAttr("disabled");
			}
			function showmes(mes) {
				$("#form_error").html(mes);
				$(".mask1").fadeIn(300);
				$(".form_error").fadeIn(300);
			}
			function remember_pass() {
				var e_email = $("#e_email").val();
				if (isEmail(e_email)) {
					$.ajax({
						url:"https://orthodontia.ru/udata/emarket/forget_pas/" + e_email + "/.json",
						xhrFields: {
						  withCredentials: true
						},
						dataType: "json",
						success:function(data) {
							if (data.status == "success") {
								$("#rp_ok").fadeIn(300);
								setTimeout(function() {$("#rp_ok").fadeOut(300)}, 15000);
							}
							else {
								$("#rp_fail").text("Пользователь не найден в базе, попробуйте еще раз").fadeIn(300);
								setTimeout(function() {$("#rp_fail").fadeOut(300)}, 15000);
							}
						}
					});
				}
				else {
					$("#rp_fail").text("Укажите свой email!").fadeIn(300);
					setTimeout(function() {$("#rp_fail").fadeOut(300)}, 15000);
				}
			}
			function countdown() {
				var events = $("#events").val();
				$.ajax({
					type: "POST",
					url: "xp_countdown.php",
					timeout: 5000,
					data: "events=" + events,
					success: function(data) {
					},
					error: function(data) {
					}
				});
			}
			function writedata() {
				var alldata = "";
				$('form input').each(function() {
					if ($(this).attr("id") != undefined)
						alldata += "&" + $(this).attr("id") + "=" + $(this).val();
				});
				$('form select').each(function() {
					if ($(this).attr("id") != undefined)
						alldata += "&" + $(this).attr("id") + "=" + $(this).val();
				});
				$.ajax({
					type: "POST",
					url: "xp_write.php",
					timeout: 5000,
					data: "alldata=" + alldata,
					success: function(data) {
					},
					error: function(data) {
					}
				});
			}
			function chk_promo() {
				var promo = $("#promo").val();
				if (promo == "")
					$("#promo_fail").fadeOut(300);
				var mainpr = $("#mainprice").val();
				var ev = $("#events").val();
				$.ajax({
					url:"https://orthodontia.ru/udata/emarket/check_promokod/" + ev + "/" + promo + "/.json",
					xhrFields: {
					  withCredentials: true
					},
					dataType: "json",
					success:function(data) {
						if (data.result == 1) {
							promo_discount_val = parseInt(data.discount);
							promo_discount_abs = parseInt(data.discount_abs);
							var pr = $("#regularprice").val();
							$("#promo_fail").fadeOut(300);
							var promo_descr = data.text;
							if (promo_descr == "")
								promo_descr = "Промокод применен!";
							if (promo_discount_val > 0) {
								// скидка в процентах
								// если надо добавить радиобаттон со скидкой
								// $("#promo_used").fadeOut(300);
								// $(".dscnt").not("#promo_dscnt").prop("checked", false);
								// $("#promo_dscnt").removeAttr("disabled").prop("checked", true).val("Скидка по промокоду " + promo_discount_val + "%");
								// $("#promo_d").text(promo_discount_val);
								// $("#promo_radio").fadeIn(300);
								// $("#discount").text(promo_discount_val);
								// $(".discount").fadeIn(300);
								pr = pr - parseInt(mainpr*(discount_val/100)) - parseInt(mainpr*(promo_discount_val/100));
							}
							else {
								// указана цена
								if (promo_discount_abs > 0) {
									pr = promo_discount_abs - parseInt(promo_discount_abs*(discount_val/100)) + parseInt(pr - mainpr);
								}
								else {
									// скидка 2000
									$("#promo_used").fadeIn(300);
									if (ev == 1649 || ev == 1650) {
										getprice(2, <?if (date("Y-m-d") < "2018-04-15") {?><?=$allprices[2]["price1"];?><?}else{?><?=$allprices[2]["price2"];?><?}?>, 1);
										$("#e_bf").prop("checked", true);
									}
									promo_discount_val = 2000;
									pr = pr - parseInt(mainpr*(discount_val/100)) - parseInt(promo_discount_val);
								}
							}
							$("#finalprice").text(pr + " р.");
							$("#totalprice").val(pr);
							$("#promo_used").text(promo_descr).fadeIn(300);
						}
						else {
							promo_discount_val = 0;
							$("#promo_used").fadeOut(300);
							$("#promo_fail").fadeIn(300);
							$("#promo_dscnt").prop("checked", false).attr("disabled", "disabled").val("Скидка по промокоду 0%");
							$("#promo_d").text(0);
							$("#promo").val("");
							$("#promo_radio").fadeOut(300);
							$("#discount").text(0);
							$(".discount").fadeOut(300);
							var pr = $("#regularprice").val();
							pr = pr - parseInt(mainpr*(discount_val/100)) - parseInt(promo_discount_val);
							$("#finalprice").text(pr + " р.");
							$("#totalprice").val(pr);
							$("#promo_discount").val("Промокод \"" + promo + "\" (" + promo_discount_val + "%) - " + pr + " руб.");
						}
					}
				});
			}
			function cleardiscounts() {
				var pr = $("#regularprice").val();
				promo_discount_val = 0;
				discount_val = 0;
				$(".dscnt").removeAttr("checked");
				$("#promo_used").fadeOut(300);
				$("#finalprice").text(pr + " р.");
				$("#totalprice").val(pr);
				$("#promo").val("");
			}
			function setdiscount(discount, element, chk) {
				$(".dscnt").not("#" + element).removeAttr("checked");
				var pr = $("#regularprice").val();
				var mainpr = $("#mainprice").val();
				if (chk) {
					$("#discount").text(discount);
					$(".discount").fadeIn(300);
					discount_val = discount;
				}
				else {
					$(".discount").fadeOut(300);
					$(".newform_upload").fadeOut(300);
					$("#uploaded").fadeOut(300);
					discount_val = 0;
				}
				$("#promo_used").fadeOut(300);
				$("#promo").val("");
				pr = pr - parseInt(mainpr*(discount_val/100));
				$("#finalprice").text(pr + " р.");
				$("#totalprice").val(pr);
			}
			function addnewyur() {
				$(".yurs").fadeOut(300);
				$(".newform_reg").fadeOut(300);
				$(".addyur").fadeOut(300, function() {
					$(".addyurform1").fadeIn(300);
				});
			}
			function cancelyur() {
				$(".addyurform1").fadeOut(300, function() {
					$(".yurs").fadeIn(300);
					$(".newform_reg").fadeIn(300);
					$(".addyur").fadeIn(300);
				});
				$(".addyurform2").fadeOut(300, function() {
					$(".yurs").fadeIn(300);
					$(".newform_reg").fadeIn(300);
					$(".addyur").fadeIn(300);
				});
			}
			function addyurajax() {
				var tosend = "id=" + userid;
				$('.addyurform input').each(function(index) {
					var did = $(this).attr("id");
					if (did == "yurname")
						did = "name";
					if (did == "yuremail")
						did = "email";
					var dval = $(this).val();
					tosend += "&data[new][" + did + "]=" + dval;
				});
				$('.addyurform textarea').each(function(index) {
					var did = $(this).attr("id");
					var dval = $(this).val();
					tosend += "&data[new][" + did + "]=" + dval;
				});
				$.ajax({
					url:"https://orthodontia.ru/udata/emarket/add_legal_item/.json?" + tosend,
					xhrFields: {
					  withCredentials: true
					},
					dataType: "json",
					success:function(data) {
						if (data.result != 0) {
							$(".addyurform").fadeOut(300).promise().done(function() {
								// добавляем новое юр. лицо в select
								$('#yur').append($('<option>', {
										value: data.result,
										text : $("#yurname").val()
								}));
								$(".newform_reg").fadeIn(300);
								$(".addyur").fadeIn(300);
								$(".yurs").fadeIn(300);
							});
						}
						else {
							$(".addyurform").fadeOut(300, function() {
								$(".newform_reg").fadeIn(300);
								$(".addyur").fadeIn(300);
								$(".yurs").fadeIn(300);
							});
							alert("Новое юр. лицо не добавлено! Проверьте данные!");
						}
					}
				});
			}
			function showyur(n) {
				var foo = 0;
				if (n == 2) {
					$(".addyurform1 .ness").each(function() {
						if ($(this).val() == '') {
							if (foo == 0) {
								$('html,body').animate({scrollTop: $(this).offset().top - 100}, 500);
							}
							$(this).addClass('redfld');
							foo = 1;
						}
						else {
							$(this).removeClass('redfld');
						}
					});
				}
				if (foo == 0) {
					$(".addyurform" + (3 - n)).fadeOut(300, function() {
						$(".addyurform" + n).fadeIn(300);
					});
				}
			}
			function showtab(tab) {
				if (tab == 1)
					$("#yur").attr("disabled", "disabled");
				else
					$("#yur").removeAttr("disabled");
				$(".tab" + (3 - tab)).fadeOut(300);
				$(".tab" + tab).fadeIn(300);
				$(".tabbut" + (3 - tab)).removeClass("active");
				$(".tabbut" + tab).addClass("active");
			}
			function showpart(part) {
				if (chkform(2)) {
					if (userid == "") {
						$.ajax({
							url:"https://orthodontia.ru/udata/emarket/logout_user/.json",
							xhrFields: {
							  withCredentials: true
							},
							dataType: "json",
							success:function(data) {
								// авторизация на сайте
								var tosend = "id=" + userid;
								$('.part1 input').each(function(index) {
									var did = $(this).attr("id");
									var dval = $(this).val();
									tosend += "&data[new][" + did + "]=" + dval;
								});
								$('.part1 select').each(function(index) {
									var did = $(this).attr("id");
									var dval = $(this).val();
									tosend += "&data[new][" + did + "]=" + dval;
								});
								$.ajax({
									url:"https://orthodontia.ru/udata/users/lpreg/.json?" + tosend,
									dataType: "json",
									success:function(data) {
										$(".f_anim").css("opacity", 0);
										if (data.status == "successful") {
											// пользователь зарегистрирован
											user_id = data.user_id;
											// отправляем данные в GA
											var utype = $("#who").val();
											var usertype = 'not a doctor';
											if (utype == "Ортодонт")
												usertype = "orthodontist";
											if (utype == "Хирург")
												usertype = "surgeon";
											if (utype == "Другая специализация")
												usertype = "other";
											console.log("usertype: " + usertype);
											ga('send', 'event', 'form', 'registration', usertype);
											yaCounter23063434.reachGoal('UserRegistration');
											$.ajax({
												url:"https://orthodontia.ru/udata/emarket/logout_user/.json",
												xhrFields: {
												  withCredentials: true
												},
												dataType: "json",
												success:function(data) {
													var newemail = $("#email").val();
													var newpassword = $("#password").val();
													$.ajax({
														xhrFields: {
														  withCredentials: true
														},
														dataType: "json",
														url:"https://orthodontia.ru/udata/emarket/get_user_info/" + newemail + "/" + newpassword + "/.json",
														success:function(data) {
															$(".part" + (3 - part)).fadeOut(300);
															$(".part" + part).fadeIn(300);
															$(".partbut" + (3 - part)).addClass("hidden");
															$(".partbut" + part).removeClass("hidden");
															$(".notfound").hide(0);
														}
													});
												}
											});
										}
										else {
											// неудачно, рисуем ошибку
											showmes(data.result);
										}
									}
								});
							}
						});
					}
					else {
						$(".part" + (3 - part)).fadeOut(300);
						$(".part" + part).fadeIn(300);
						$(".partbut" + (3 - part)).addClass("hidden");
						$(".partbut" + part).removeClass("hidden");
					}
				}
			}
			function chk_entry() {
				$("#entry_but").text("Проверяем...").attr("disabled", "disabled");
				$(".wrong").fadeOut(300);
				var e_email = $("#e_email").val();
				var e_pass = $("#e_pass").val();
				var eventid = $("#events").val();
				$.ajax({
					url:"https://orthodontia.ru/udata/emarket/logout_user/.json",
					xhrFields: {
					  withCredentials: true
					},
					dataType: "json",
					success:function(data) {
						$.ajax({
							xhrFields: {
							  withCredentials: true
							},
							dataType: "json",
							url:"https://orthodontia.ru/udata/emarket/get_user_info/" + e_email + "/" + e_pass + "/.json",
							success:function(data) {
								$(".f_anim").css("opacity", 0);
								if (typeof data.extended === 'object') {
									// пользователь ввел правильные данные
									console.log("user auth");
									// отправляем данные в GA
									ga('send', 'event', 'form', 'authorization');
									yaCounter23063434.reachGoal('UserAuthorization');
									var user = data;
									$(".wrong").fadeOut(300);
									$.ajax({
										url:"https://orthodontia.ru/udata/emarket/is_first_purchase/" + e_email + "/" + eventid + "/.json",
										xhrFields: {
										  withCredentials: true
										},
										dataType: "json",
										success:function(data) {
											$(".f_anim").css("opacity", 0);
											if (data.result == 1) {
												// пользователь уже зарегистрирован на мероприятие, выводим надпись
												$(".exists").css("opacity", 1);
											}
											else {
												userid = user.id;
												// alert(userid);
												// пользователь еще не зарегистрирован, все ОК, рисуем форму регистрации на мероприятие
												$(".openform").fadeOut(300, function() {
													$(".formbox2").hide(0);
													// console.table(user);
													// вставляем поля
													arruser = new Array();
													var allprops = user.extended.groups.group[0].property;
													$.each(allprops, function(index, value) {
														// console.log(this.name);
														arruser[this.name] = this.value;
													});
													if (typeof user.extended.groups.group[1] == "object") {
														var allprops = user.extended.groups.group[1].property;
														$.each(allprops, function(index, value) {
															// console.log(this.name);
															arruser[this.name] = this.value;
														});
													}
													// console.table(arruser);
													$("#fname").val(arruser["fname"].value);
													$("#lname").val(arruser["lname"].value);
													$("#father_name").val(arruser["father_name"].value);
													$("#email").val(arruser["e-mail"].value);
													$("#phone").val(arruser["phone"].value);
													$("#city").val(arruser["city"].value);
													$("#company").val(arruser["company"].value);
													$("#bd").val(arruser["bd"].value);
													$("#country").val(arruser["country"].item.name);
													$("#region").val(arruser["region"].item.id);
													if (typeof arruser["prof_status"] == "object")
														$("#who").val(arruser["prof_status"].item.name);
													// убираем пароли из формы
													$(".phide").hide();
													// рисуем форму
													$(".formbox3").show(0);
													$(".openform").fadeIn(300);
												});
												// подгружаем юр. лиц
												$.ajax({
													url:"https://orthodontia.ru/udata/emarket/legalList/.json",
													xhrFields: {
													  withCredentials: true
													},
													dataType: "json",
													success:function(data1) {
														// alert(data1.items.length);
														// добавляем юр. лица в select
														$.each(data1.items.item, function(index, value) {
															$('#yur').append($('<option>', {
																	value: value.id,
																	text : value.name
															}));
														});
														// console.table(data1.items);
													}
												});
											}
										},
										error:function(data) {
										}
									});
								}
								else {
									// пользователь ввел неверные данные
									$("#e_email").addClass("invalid");
									$("#e_pass").addClass("invalid");
									$(".wrong").fadeIn(300, function() {
										$("#entry_but").text("Войти").removeAttr("disabled");
									});
								}
							}
						});
					}
				});
			}
			function fixbody() {
				$("body").addClass("hold");
			}
			function unfixbody() {
				$("body").removeClass("hold");
			}
			function chg_passstate() {
				if ($("#e_pass").attr("type") == "password") {
					$("#e_pass").attr("type", "text");
				}
				else {
					$("#e_pass").attr("type", "password");
				}
			}
			function chk_f_email() {
				var f_email = $("#f_email").val();
				var eventid = $("#events").val();
				if (f_email != "" && isEmail(f_email)) {
					$(".exists").fadeOut(300);
					$(".f_anim").css("opacity", 1);
					$("#f_email").removeClass("invalid");
					// проверяем email
					$.ajax({
						url:"https://orthodontia.ru/udata/emarket/is_user_exist/" + f_email + "/.json",
						dataType: "json",
						success:function(data) {
							$(".f_anim").css("opacity", 0);
							if (data.result == 1) {
								// пользователь найден, проверяем, есть ли регистрация на мероприятие
								$.ajax({
									url:"https://orthodontia.ru/udata/emarket/is_first_purchase/" + f_email + "/" + eventid + "/.json",
									dataType: "json",
									success:function(data) {
										$(".f_anim").css("opacity", 0);
										if (data.result == 1) {
											// пользователь уже зарегистрирован на мероприятие, выводим надпись
											$(".exists").fadeIn(300);
										}
										else {
											// еще не зарегистрирован, рисуем форму входа
											$(".openform").fadeOut(300, function() {
												$("#email").val($("#f_email").val());
												$(".formbox1").hide(0);
												$("#e_email").val(f_email);
												$(".formbox2").show(0);
												$(".openform").fadeIn(300);
											});
										}
									}
								});
							}
							else {
								// пользователь не найден, отображаем форму регистрации пользователя
								$(".openform").fadeOut(300, function() {
									$("#email").val($("#f_email").val());
									$(".notfound").show(0);
									$(".formbox1").hide(0);
									$(".formbox3").show(0);
									$(".openform").fadeIn(300);
								});
							}
						},
						error:function(data) {
						}
					});
				}
				else {
					$("#f_email").addClass("invalid");
				}
			}
			jQuery(function($){
				$.datepicker.regional['ru'] = {
					closeText: 'Закрыть',
					prevText: '&#x3C;Пред',
					nextText: 'След&#x3E;',
					currentText: 'Сегодня',
					monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
					'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
					monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
					'Июл','Авг','Сен','Окт','Ноя','Дек'],
					dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
					dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
					dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
					weekHeader: 'Нед',
					dateFormat: 'yy-mm-dd',
					firstDay: 1,
					isRTL: false,
					showMonthAfterYear: false,
					yearSuffix: ''};
				$.datepicker.setDefaults($.datepicker.regional['ru']);
			});
			$(function() {
				$(".date1").datepicker({
					changeYear: true,
					yearRange: '1920:2020',
					defaultDate: '1980-01-01'
				});
			});
			lnks = new Array("form", "place", "price", "speakers", "programm", "about");
			var gmargin = 797;
			var curgal = 1;
			var maxx = 7;
			var curgal1 = 1;
			var maxx1 = 5;
			var igos = 0;
			var totigos = 2;
			var prods = 0;
			var filedone = 0;
			function pset(ord, val, state) {
				if (state) {
					prods++;
					$("#p" + ord).val(val);
					$(".prodsel span").text("Выбрано " + prods);
				}
				else {
					prods--;
					$("#p" + ord).val("");
					if (prods == 0)
						$(".prodsel span").text("-------");
					else
						$(".prodsel span").text("Выбрано " + prods);
				}
			}
			function igo(group, ord, theme, state, similar) {
				if (state) {
					igos++;
					var arr_s = similar.split(",");
					$(".prog[data-group=" + group + "]").addClass("faded").find(".igo").fadeOut(200);
					$(".prog[data-mk=" + ord + "]").removeClass("faded").find(".igo").fadeIn(200);
					$(".igoset[data-group=" + group + "]").addClass("faded").find("input").attr("disabled", "disabled");
					$(".igoset[data-mk=" + ord + "]").removeClass("faded").find("input").removeAttr("disabled").attr("checked", "checked");;
					$.each(arr_s, function(key, val) {
						$(".prog[data-mk=" + val + "]").addClass("faded").find(".igo").fadeOut(200);
						$(".igoset[data-mk=" + val + "]").addClass("faded").find("input").attr("disabled", "disabled");
					})
					$("#mk" + ord).parent().addClass("sel");
					$("#lf" + ord).text("Пойду!");
					$("#mk_" + ord).parent().addClass("sel");
					$("#lf_" + ord).text("Пойду!");
					var html = "<div id=\"chosen" + ord + "\" class=\"chitem group\"><div class=\"igonum\">" + igos + "</div><div class=\"igodesc\" id=\"igod" + ord + "\">" + theme + "</div><!--<div class=\"igodel\" onClick=\"igo(" + group + ", " + ord + ", '', false);\"><span>X</span> Удалить</div>--></div>";
					$(".chosen").append(html);
					$("#mclasses" + group).val(theme);
					$("#mcl" + group).val(ord);
					// для прокрутки вниз при выборе первого мастер-класса
					if (igos == 1)
						$('html,body').animate({scrollTop: $(".prog[data-group=" + (3 - group) + "]").offset().top - 200}, 500);
					
				}
				else {
					igos--;
					var arr_s = similar.split(",");
					$(".prog[data-group=" + group + "]").removeClass("faded").find(".igo").fadeIn(200);
					$(".igoset[data-group=" + group + "]").not(".cancelled").removeClass("faded").find("input").removeAttr("disabled");
					$.each(arr_s, function(key, val) {
						$(".prog[data-mk=" + val + "]").removeClass("faded").find(".igo").fadeIn(200);
						$(".igoset[data-mk=" + val + "]").not(".cancelled").removeClass("faded").find("input").removeAttr("disabled");
					})
					$(".igoset[data-mk=" + ord + "]").find("input").removeAttr("checked");
					$("#mk" + ord).parent().removeClass("sel");
					$("#lf" + ord).text("Хочу пойти");
					$("#mk_" + ord).parent().removeClass("sel");
					$("#lf_" + ord).text("Хочу пойти");
					$("#chosen" + ord).remove();
					$('.chitem').each(function(index) {
						$(this).find(".igonum").text(index + 1);
					});
					$("#mclasses" + group).val("");
					$("#mcl" + group).val("");
				}
			}
			function openmob(n) {
				var wdth = $(window).width();
				if (wdth < 960) {
					$("#event" + n).slideToggle(300);
					$("#prog" + n).toggleClass("open");
				}
			}
			function isEmail(email) {
				var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				return regex.test(email);
			}
			function chkigos() {
				if (igos < totigos) {						
					$('.prog1').fadeOut(200);
					$('.progtab.sel').removeClass('sel');
					$('.progtab2').addClass('sel');
					$('div[data-date=8]').fadeOut(300);
					$('h2[data-date=8]').fadeOut(300);
					$('.progspace').fadeIn(300);
					$('.mktime').css('display', 'table');
					$('div[data-date=9]').fadeIn(300);
					$('h2[data-date=9]').fadeIn(300);
					$('.prog2').fadeIn(200).promise().done(function() {
						$('html,body').animate({scrollTop: $(".prog2").offset().top - 100}, 500);
					});
					return false;
				}
				else
					return true;
			}
			function chkform(chkfile) {
				var foo = 0;
				if (chkfile == 1) {
					$("#register").attr("disabled", "disabled");
					// отправляем данные в GA
					ga('send', 'event', 'ET', 'event_click', 'LP');
					yaCounter23063434.reachGoal('EventClick');
				}
				if (chkfile == 1 && grecaptcha.getResponse() == "")	{
					foo = 1;
					$(".captcha_err").fadeIn(300);
				}
				if (chkfile != 0 && filedone == 0 && $('.upl:checked').length > 0)
					{
					if (foo == 0) {
						$('html,body').animate({scrollTop: $('.upl').offset().top - 100}, 500);
					}
					$('#upload').addClass('redfld');
					foo = 1;
				}
				$('.ness:visible').each(function() {
					if ($(this).val() == '') {
						if (foo == 0) {
							$('html,body').animate({scrollTop: $(this).offset().top - 100}, 500);
						}
						$(this).addClass('redfld');
						foo = 1;
					}
					else {
						$(this).removeClass('redfld');
					}
				});
				$('#email').each(function() {
					if (!isEmail($(this).val())) {
						if (foo == 0) {
							// alert($(this).offset().top);
							$('html,body').animate({scrollTop: $(this).offset().top - 100}, 500);
						}
						$(this).addClass('redfld');
						foo = 1;
					}
					else {
						$(this).removeClass('redfld');
					}
				});
				if (foo == 0) {
					if (userid != "" && chkfile == 1) {
						countdown();
						writedata();
						// отправляем данные в GA
						ga('send', 'event', 'ET', 'event_registration', 'LP');
						yaCounter23063434.reachGoal('EventRegistration');
					}
					return true;
				}
				else {
					$("#register").removeAttr("disabled");
					return false;
				}
			}
			$(window).resize(function() {
				var wdth = $(window).width();
				if (wdth < 960) {
					gmargin = wdth*0.9 - 20;
					$(".gal_slide").css("width", gmargin);
					var ghght = gmargin/600*425;
					$(".gal_slide").css("height", ghght);
					$(".gallery").css("height", ghght);
					$(".gallery1").css("height", ghght);
					$(".gal_navs").css("top", ghght - 80);
				}
				/*
				var gwdth = $(window).width();
				if (gwdth > 1250)
					gwdth = 1250;
				gmargin = gwdth;
				var ghght = gwdth/1250*669;
				if (gwdth < 480)
					$(".prog").css("width", gwdth - 40);
				$(".gal_slide").css("width", gwdth);
				$(".gal_slide").css("height", ghght);
				$(".gallery").css("height", ghght);
				$(".gal_navs").css("top", ghght - 80);
				*/
			})
			// $(window).load(function() {
				// $("#left").css("margin-left", "-1046px");
				// $("#right").css("margin-left", "250px");
			// });
			var spmargin = 0;
			var spmargin1 = 0;
			var swidth = 320;
			var curspeaker = 1;
			var curspeaker1 = 1;
 			function speaker1_left() {
				if (spmargin1 < 0)
					speaker1_go(curspeaker1 - 1);
			}
 			function speaker1_right() {
				var slide_count = $(".speakers1").find(".speaker:visible").length;
				var limit = (slide_count - 1) * swidth;
				if (spmargin1 > -limit)
					speaker1_go(curspeaker1 + 1);
			}
 			function speaker_left() {
				if (spmargin < 0) {
					spmargin = spmargin + swidth;
					$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 500);
					curspeaker--;
					$(".spbullets").find(".active").removeClass("active");
					$("#spbul_" + curspeaker).addClass("active");
				}
				else {
					$(".speakers").animate({margin: '0 0 0 20'}, 200, function() {
						$(".speakers").animate({margin: '0 0 0 0'}, 200);
					});
				}
			}
 			function speaker_right() {
				var slide_count = $(".speakers").find(".speaker:visible").length;
				var limit = (slide_count - 1) * swidth;
				if (spmargin > -limit) {
					spmargin = spmargin - swidth;
					$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 500);
					$(".spbullets").find(".active").removeClass("active");
					curspeaker++;
					$(".spbullets").find(".active").removeClass("active");
					$("#spbul_" + curspeaker).addClass("active");
				}
				else {
					$(".speakers").animate({margin: '0 0 0 ' + (spmargin - 20)}, 200, function() {
						$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 200);
					});
				}
			}
			function speaker1_go(sp) {
				spmargin1 = - swidth * (sp - 1);
				$(".speakers1").animate({margin: '0 0 0 ' + spmargin1}, 500);
				$(".spbullets1").find(".active").removeClass("active");
				$("#spbul_" + sp).addClass("active");
				curspeaker1 = sp;
			}
			function speaker_go(sp) {
				spmargin = - swidth * (sp - 1);
				$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 500);
				$(".spbullets").find(".active").removeClass("active");
				$("#spbul_" + sp).addClass("active");
				curspeaker = sp;
			}
			$(document).ready(function() {
				$("body").bind("click", function(e) {
					if ($(e.target).closest(".igosel").length > 0 || $(e.target).closest(".prodsel").length > 0 || $(e.target).closest(".igoset").length > 0 || $(e.target).closest(".prodset").length > 0) {
						return;
					}
					$(".igodd").fadeOut(500);
					$(".igosel").removeClass('sel');
					$(".proddd").fadeOut(500);
					$(".prodsel").removeClass('sel');
				});
				var wdth = $(window).width();
				if (wdth < 960) {
					gmargin = wdth*0.9 - 20;
					$(".gal_slide").css("width", gmargin);
					var ghght = gmargin/600*425;
					$(".gal_slide").css("height", ghght);
					$(".gallery").css("height", ghght);
					$(".gal_navs").css("top", ghght - 80);
					$(".speakers").swipe( {
						swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
							speaker_right();
						},
						swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
							speaker_left();
						},
					   threshold: 75
					});
					$(".speakers1").swipe( {
						swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
							speaker1_right();
						},
						swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
							speaker1_left();
						},
					   threshold: 75
					});
					$(".gallery").swipe( {
						swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
							gallery_right();
						},
						swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
							gallery_left();
						},
					   threshold: 75
					});
					$(".gallery1").swipe( {
						swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
							gallery_right1();
						},
						swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
							gallery_left1();
						},
					   threshold: 75
					});
					swidth = wdth;
					$(".speaker").css("width", wdth);
				}
				/*
				maxx = $(".gal_slide").length;
				var gwdth = $(window).width();
				if (gwdth > 1250)
					gwdth = 1250;
				gmargin = gwdth;
				var ghght = gwdth/1250*669;
				if (gwdth < 480)
					$(".prog").css("width", gwdth - 40);
				$(".gal_slide").css("width", gwdth);
				$(".gal_slide").css("height", ghght);
				$(".gallery").css("height", ghght);
				$(".gal_navs").css("top", ghght - 80);
				*/
				hghts = new Array();
				for (var key in lnks) {
					if ($("[name=" + lnks[key] +"]").length) {
						hghts[key] = $("[name=" + lnks[key] +"]").offset().top;
					}
				}
				var i = document.location.hash.replace("#", "");
				$("a[href*='#']:not([href='#'])").click(function() {
					if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
						|| location.hostname == this.hostname) {
						var target = $(this.hash);
						target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
						   if (target.length) {
							var i = target.attr("name");
							scrolling = 1;
							$("#mobmenu").slideUp(300);
							$('html,body').animate({
								 scrollTop: target.offset().top - 60
							}, 1000, function() {
								scrolling = 0;
								// alert(target.offset().top);
							});
							$(".ch").removeClass("ch");
							$(this).parent().addClass("ch");
							// history.pushState(null, i, i);
							return false;
						}
					}
				});
			});
			function gallery_go(n) {
				var curgmargin = -gmargin * (n - 1);
				$(".gal_slider").animate({marginLeft: curgmargin});
				curgal = n;
			}
			function gallery_left() {
				if (curgal > 1) {
					gallery_go(curgal - 1);
				}
				else {
					gallery_go(maxx);
				}
			}
			function gallery_right() {
				if (curgal < maxx) {
					gallery_go(curgal + 1);
				}
				else {
					gallery_go(1);
				}
			}
			function gallery_go1(n) {
				var curgmargin1 = -gmargin * (n - 1);
				$(".gal_slider1").animate({marginLeft: curgmargin1});
				curgal1 = n;
			}
			function gallery_left1() {
				if (curgal1 > 1) {
					gallery_go1(curgal1 - 1);
				}
				else {
					gallery_go1(maxx1);
				}
			}
			function gallery_right1() {
				if (curgal1 < maxx1) {
					gallery_go1(curgal1 + 1);
				}
				else {
					gallery_go1(1);
				}
			}
			var scrolling = 0;
			/*
			window.onscroll = function() {
				var hght = $('body').height();
				// alert(hght);
				var scrolled = window.pageYOffset || document.documentElement.scrollTop;
				if (scrolling == 0)
					{
					for (var key in hghts)
						{
						if (scrolled >= hghts[key] - 300) {
							$(".ch").removeClass("ch");
							$("#mi" + lnks[key]).addClass("ch");
							$("#mi" + lnks[key] + "1").addClass("ch");
							break;
						}
					}
				}
				var wdth = $(window).width();
				if (scrolled < 320 && wdth > 960) {
					var leftmarg = scrolled - 1146;
					var rightmarg = 450 - scrolled;
					$("#left").css("margin-left", leftmarg);
					$("#right").css("margin-left", rightmarg);
				}
			}
			*/
			function reload() {
				var src = document.captcha.src;
				document.captcha.src = '/images/loading.gif';
				document.captcha.src = src + '?rand='+Math.random();
			}
			var x1 = 59.9317953;
			var y1 = 30.3507702;
			var x2 = 25.217603;
			var y2 = 55.2828107;
			var myLatlng1;
			var myLatlng2;
			var map;
			var marker1;
			var marker2;
			function initialize_map() {
				myLatlng1 = new google.maps.LatLng(x1, y1);
				myLatlng2 = new google.maps.LatLng(x2, y2);
				var mapOptions = {
					center: myLatlng1,
					mapTypeControl:!1,
					streetViewControl:!1,
					scrollwheel:!1,
					panControl:!1,
					draggable:!1,
					zoomControlOptions:{position:google.maps.ControlPosition.LEFT_TOP},
					zoom: 17,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				map = new google.maps.Map(document.getElementById("map"),
					mapOptions);
				marker1 = new google.maps.Marker({
					position: myLatlng1,
					map: map,
					title: "г. Москва, улица Тверская, 26/1, отель \"Отель «Коринтия» Санкт-Петербург\""
				});
				var infowindow1 = new google.maps.InfoWindow({
					content: "<div class=\"onmap\"><h3>Отель «Коринтия», Санкт-Петербург</h3>, Невский пр., дом 57</div>"
				});
				marker1.addListener('click', function() {
					infowindow1.open(map, marker1);
				});
				/*
				marker2 = new google.maps.Marker({
					position: myLatlng2,
					map: map,
					title: "Санкт-Петербург"
				});
				*/
			}
			function map1() {
				map.setCenter(myLatlng1);
			}
			function map2() {
				map.setCenter(myLatlng2);
			}
			// загрузка изображения
			$(function(){
				var btnUploads = $("#upload");
				$(btnUploads).each(function() {
					new AjaxUpload(this, {
						action: "xp_upload.php",
						name: "uploadfile",
						onComplete: function(file, response){
							if (response.replace("Error", "") == response) {
								$("#upload").addClass("done").text("Файл загружен");
								if (response.replace(".pdf", "") == response && response.replace(".PDF", "") == response) {
									$("#uploaded").html("<img width=\"100%\" src=\"uploads/" + response + "\" alt=\"\" />");
								}
								// $("#file").val('<a href="<?=$_SERVER["REQUEST_URI"];?>uploads/' + response + '">скачать</a>');
								// $("#file").val('<?=$_SERVER["REQUEST_URI"];?>uploads/' + response);
								$("#file").val('<?=$pageurl;?>uploads/' + response);
								filedone = 1;
							}
							else {
								alert("Файл " + file + " не загружен! Ошибка: " + response.replace("Error", ""));
							}
						}
					});
				})
			});
		</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter23063434 = new Ya.Metrika({
                    id:23063434,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    ecommerce:"dataLayer"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/23063434" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67881160-2', 'auto');
  ga('require', 'displayfeatures');
  ga('require', 'linkid');
  ga('require', 'ec'); 
  ga('send', 'pageview');

</script>
<script src='https://www.google.com/recaptcha/api.js'></script>
	</head>
	<body>
		<div class="container">
			<a name="about"></a>
			<div class="section top">
				<div class="section header">
					<img src="images/logo_.png" />
					<div class="menu">
						<ul>
							<a href="#choose"><button>Регистрация</button></a>
							<li id="miprogramm"><a href="#programm">Программа</a></li>
							<li id="migala"><a href="#gala">Гала-ужин</a></li>
							<li id="mispeakers"><a href="#speakers">Спикеры</a></li>
							<li id="miprice"><a href="#price">Стоимость</a></li>
							<li id="miplace"><a href="#place">Место проведения</a></li>
						</ul>
					</div>
					<a href="/forum2018e"><div class="eng">En</div></a>
				</div>
			</div>
			<div class="section baseinfo">
				<!--<div class="superback">
					<div class="superback_">
						<div class="superback_left"><h1>ОРМКО ФОРУМ 2018</h1></div>
						<div class="superback_right">
							<h2 class="h2"><span>1 - 3</span> июня 2018 г.</h2>
							<h3 class="h3">Санкт-Петербург, Отель «Коринтия»</h3>
						</div>
					</div>
				</div>-->
				<div class="superback1">
					<div class="superback1_">
						<div class="superback_left"><h1>ORMCO<br />ФОРУМ<br />2018</h1></div>
						<div class="superback_right">
							<h2 class="h2"><span>1 - 3</span> июня 2018 г.</h2>
							<h3 class="h3">Санкт-Петербург, Отель «Коринтия»</h3>
						</div>
					</div>
				</div>
				<br />
				<br />
				<script src="https://e-timer.ru/js/etimer.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery(".eTimer").eTimer({
			etType: 0, etDate: "01.06.2018.9.0", etTitleText: "До события осталось", etTitleSize: 22, etShowSign: 1, etSep: ":", etFontFamily: "Trebuchet MS", etTextColor: "#ffffff", etPaddingTB: 9, etPaddingLR: 11, etBackground: "transparent", etBorderSize: 0, etBorderRadius: 2, etBorderColor: "transparent", etShadow: " 0px 0px 10px 0px transparent", etLastUnit: 4, etNumberFontFamily: "Trebuchet MS", etNumberSize: 38, etNumberColor: "white", etNumberPaddingTB: 0, etNumberPaddingLR: 2, etNumberBackground: "transparent", etNumberBorderSize: 0, etNumberBorderRadius: 5, etNumberBorderColor: "white", etNumberShadow: "none"
		});
	});
</script>
<div class="eTimer"></div>
				<div class="txtdiv nointerval"><br /><br /><h3>Насыщенная программа лекций, настоящие звезды мировой и отечественной ортодонтии, орто-завтрак, великолепный гала-ужин в Мраморном дворце, неповторимая атмосфера северной столицы и белых ночей, все это - Ormco Форум Россия 2018</h3></div>
				<br />
				<h3>Сергей Александрович Попов приглашает вас на Ormco Форум</h3><br />
				<iframe frameborder="0" src="https://www.youtube.com/embed/4SxWJDbs_pM" width="560" height="315"></iframe>
				<br />
				<div class="txtdiv nointerval"><br /><img src="images/logotype0.gif" /><br /><br />
				При поддержке Профессионального общества ортодонтов.<br /><br />
				Почетный гость Форума – <strong>Персин Леонид Семенович</strong>, заведующий кафедрой ортодонтии МГМСУ, профессор, доктор медицинских наук, член-корреспондент РАН,  заслуженный деятель науки РФ</div>
				<br />
				<div class="txtdiv nointerval">Заявка по учебному мероприятию представлена в Комиссию по оценке учебных мероприятий и материалов для НМО на соответствие установленным требованиям. 12 кредитов</div>
				<div class="main">
					<!-- Программа -->
					<a name="programm"></a>
					<h1>Программа</h1>
					<br />
					<?
					foreach ($programm as $prog)
						$progdates[] = $prog["date"];
					$progdates = array_unique($progdates);
					?>
					<div class="progtabbs">
						<div class="progline"></div>
						<div class="progtabs">
							<?
							$pdcnt = 0;
							foreach ($progdates as $pd => $progdate)
								{
								?>
								<div class="progtab progtab<?=$pd;?><?if ($pdcnt++ == 0) {?> sel<?}?>" onClick="$('.progtab.sel').removeClass('sel'); $(this).addClass('sel'); $('.prog[data-date!=<?=$progdate;?>]').fadeOut(300); $('.prog[data-date=<?=$progdate;?>]').fadeIn(300); $('.txt[data-date!=<?=$progdate;?>]').fadeOut(300); $('.txt[data-date=<?=$progdate;?>]').fadeIn(300);"><?=ourdate($progdate, 0);?></div>
								<?
							}
							?>
						</div>
						<div class="progline"></div>
					</div>
					<div class="txt" data-date="2018-06-01">В рамках лекции мы вместе пошагово разберем множество интересных клинических случаев, которые были пролечены с использованием Damon System. Эти примеры покажут, что независимо от характера и сложности первоначальной дизокклюзии, можно завершать каждый случай, одновременно добиваясь правильного положения зубов в вертикальной, трансверзальной и сагиттальной плоскостях, функционального и здорового прикуса и впечатляющих результатов в виде красивой улыбки и лицевой эстетики. Фокус на самых важных аспектах и огромное количество деталей, на которые стоит обращать внимание, чтобы всегда завершить случаи предсказуемо и превосходно</div>
					<div class="progs">
						<?
						$pdcnt = 0;
						foreach ($programm as $p => $prog)
							{
							$dt = explode(" ", ourdate($prog["date"], 0));
							?>
							<!-- <?=ourdate($prog["date"], 1);?> -->
							<!-- Событие <?=$p;?> -->
							<div class="prog group" data-date="<?=$prog["date"];?>" id="prog<?=$p;?>" onClick="openmob(<?=$p;?>);"<?if ($prog["date"] != $programm[1]["date"]) {?> style="display: none"<?}?>>
								<div class="event1">
									<time><?=$prog["time_start"];?> – <?=$prog["time_end"];?></time>
									<div class="eshort"><?=$prog["name"];?></div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/<?if ($prog["state"] == 1) {?><?=$speakers[$prog["speaker"]]["image"];?><?}else{?><?=$states[$prog["state"]];?><?}?>" /></div>
									<div class="psd"><h4><?=$prog["time_start"];?> – <?=$prog["time_end"];?></h4></div>
								</div>
								<div class="event">
									<?if ($prog["state"] == 1) {?><h4><?=$speakers[$prog["speaker"]]["name"];?></h4><button type="button">Лекция</button><?}?>
									<?
									if ($prog["descr"] != "")
										{
										?>
										<img id="imgopen<?=$p;?>" src="images/arr_open.png" onClick="$(this).fadeOut(200); $('#imgclose<?=$p;?>').fadeIn(200); $(this).parent().find('.elong').slideDown(300);" />
										<img id="imgclose<?=$p;?>" src="images/arr_close.png" style="display: none;" onClick="$(this).fadeOut(200); $('#imgopen<?=$p;?>').fadeIn(200); $(this).parent().find('.elong').slideUp(300);" />
										<?
									}
									?>
									<div class="eshort"><?=$prog["name"];?></div>
									<div class="elong"><?=$prog["descr"];?></div>
								</div>
								<div class="event2" id="event<?=$p;?>">
									<div class="psimg"><img align="left" src="images/<?if ($prog["state"] == 1) {?><?=$speakers[$prog["speaker"]]["image"];?><?}else{?><?=$states[$prog["state"]];?><?}?>" /></div>
									<div class="psd"><span><?=$dt[0];?></span> <?=$dt[1];?><br /></div>
								</div>
								<div class="dt">
									<span><?=$dt[0];?></span><br /><?=$dt[1];?>
								</div>
							</div>
							<?
						}
						?>
					</div>
					<!--<div class="prog1">Заявка по учебному мероприятию в установленные порядком сроки будет представлена в Комиссию по оценке учебных мероприятий и материалов на соответствие установленным требованиям для НМО</div>-->
					<div class="prog1">Осталось мест на основную программу <?=$mkcounts[0];?></div>
					<div class="txtdiv">
					1 июня – Синхронный перевод с испанского – Сергей Филоненко. Обеспечение коммуникации с д-ром Перерой – Кристина Есипович.<br />
					2-3 июня – Синхронный перевод с английского – Леонид Эмдин.</div>
					<div class="spec" onClick="$('.mask').fadeIn(300); $('#special').fadeIn(300);">Специальные условия проживания</div>
					<div class="txtdiv">Убедительно просим вас позаботиться о проживании и бронировать отели заранее! В июне Санкт-Петербург особенно прекрасен и пользуется огромной популярностью у туристов со всего мира. Кроме того, в 2018 году в Санкт-Петербурге весной и летом будут проходить Международный экономический форум и матчи в рамках Чемпионата мира по футболу, что также увеличит количество желающих приехать в Северную столицу в период белых ночей</div>
					<div class="spec_desc" id="special">
						<div class="close" onClick="$('.mask').fadeOut(300); $('.spec_desc').fadeOut(300);"><img src="images/close.png" /></div>
						<h2>Специальные условия проживания</h2>
						<?=nl2br("Участники мероприятия могут забронировать номера категории Deluxe в отеле Corinthia Hotel St. Petersburg по специальным ценам:

<strong>Одноместное размещение (1 ночь) – 12 500 р.</strong>
<strong>Двухместное размещение (1 ночь) – 14 500 р.</strong>

Для размещения брони, пожалуйста, <a href='uploads/Анкета_Коринтия.docx'><span>заполните эту анкету</span></a> и отправьте специалистам туристического агентства Starliner на e-mail адрес <a href='mailto:e.danilova@starliner.ru'>e.danilova@starliner.ru</a> (Елизавета) или <a href='mailto:j.brednikova@starliner.ru'>j.brednikova@starliner.ru</a> (Юлия). Starliner является партнером Ormco и организует для вас бронирование выбранного номера.

<span class='red'>Обратите внимание!</span> Количество номеров, возможных для бронирования по специальным ценам, ограничено! Доступно всего 80 номеров!");
						?>
					</div>
					<div class="spec" onClick="$('.mask').fadeIn(300); $('#football').fadeIn(300);">ВАЖНО! Правила регистрации в СПб в связи с ЧМ по футболу</div>
					<div class="spec_desc" id="football">
						<div class="close" onClick="$('.mask').fadeOut(300); $('.spec_desc').fadeOut(300);"><img src="images/close.png" /></div>
						<h2>Уважаемые участники Ormco Форума Россия 2018!</h2>
						<?=nl2br("Пожалуйста, ознакомьтесь с правилами регистрации в Санкт-Петербурге, введенными в связи с усилением мер безопасности с 25 мая по 25 июля 2018 года по случаю проведения Чемпионата мира по футболу FIFA.

Если вы являетесь гражданином Российской Федерации, при посещении Санкт-Петербурга в дни проведения Форума, вам необходимо зарегистрироваться по месту пребывания в течение 3 суток

Если вы являетесь гражданином иностранного государства, вам необходимо зарегистрироваться по месту пребывания в течение 1 суток

Если вы проживаете в гостинице, постановка на миграционный учет осуществляется сотрудниками гостиницы при заселении (приблизительная стоимость регистрации – 250 рублей)

Если вы проживаете не в гостинице, то вам необходимо подойти в одно из Территориальных подразделений по вопросам миграции (адреса, телефоны и график работы здесь), вместе с представителем принимающей стороны (родственник, друг, собственник съемного жилья) и оформить постановку на миграционный учет. Принимающая сторона (физ. лицо) должна предъявить оригиналы и копии паспорта и документов на помещение, по адресу которого регистрируется данный гражданин.

Обеспечение безопасности – одно из важнейших условий проведения крупных международных мероприятий.

Мы надеемся, что эти необходимые действия не будут для вас обременительными, и пребывание на Форуме Ormco подарит вам массу позитивных эмоций, полезных знакомств и новых знаний.

<strong>Ждем вас в Санкт-Петербурге!</strong>");
						?>
					</div>
					<a name="new"></a>
					<br />
					<div><a href="#choose"><button type="button">Регистрация</button></a></div>
					<br /><br />
					<iframe width="560" height="315" src="https://www.youtube.com/embed/JTg1VEDQKAM?rel=0&showinfo=0" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
					<!-- Гала-ужин -->
					<a name="gala"></a>
					<h1>Гала-ужин</h1>
					<div class="txtdiv">
					<div class="gallery1">
						<div class="gal_slider1 group">
							<div class="gal_slide"><img src="images/building7.jpg" /></div>
							<div class="gal_slide"><img src="images/building8.jpg" /></div>
							<div class="gal_slide"><img src="images/building9.jpg" /></div>
							<div class="gal_slide"><img src="images/building10.jpg" /></div>
							<div class="gal_slide"><img src="images/building11.jpg" /></div>
						</div>
						<div class="gal_navs">
							<div class="gal_nav" onClick="gallery_left1();"><img src="images/arr_left.png" /></div>
							<div class="gal_nav" onClick="gallery_right1();"><img src="images/arr_right.png" /></div>
						</div>
					</div>
					<?=nl2br("
Гала-ужин 2 июня пройдет в Мраморном дворце – уникальном памятнике архитектуры XVIII в. и одном из красивейших зданий Санкт-Петербурга. Он возводился в 1768 — 1785 гг. по проекту итальянского архитектора Антонио Ринальди по заказу императрицы Екатерины II для Григория Орлова – генгерал-фильдцейхмейстера и ее фаворита.

Г. Орлов не дожил до окончания строительства дворца. После его смерти Екатерина II выкупила дворец у его наследников — братьев Орловых, и подарила своему второму внуку Великому князю Константину Павловичу (1779-1831) к его бракосочетанию, которое состоялось в 1796 г. После его смерти император Николай I назначает дворец своему второму сыну великому князю Константину Николаевичу (1827-1892).

В 1844 — 1849 гг. Мраморный дворец и принадлежавший ему Служебный дом реконструировался по проекту архитектора Александра Брюллова (1798-1877) к бракосочетанию нового владельца дворца. Основные изменения коснулись второго этажа, где была создана новая планировочная структура, а парадные и жилые интерьеры получили новую художественную отделку. На месте существовавшего в XVIII в. висячего сада был создан зимний сад.

В 1892 г. Мраморный дворец наследует сын великого князя Константина Николаевича — великий князь Константин Константинович (1858-1915). Сыновья Великого князя Константина Константиновича, унаследовавшие Мраморный дворец со Служебным домом после его кончины, за неимением средств на содержание комплекса зданий были вынуждены продать его в национальную собственность. Эти события произошли осенью 1917 г., когда у власти в России уже было Временное правительство и во дворце размещалось Министерство труда.

В настоящее время Мраморный дворец находится в ведении Государственного Русского музея. В нем размещается постоянная экспозиция, в которой представлены произведения отечественных и зарубежных художников второй половины XX в.

Почувствуйте себя на настощем Русском балу XIX века!

<em>Рекомендуем вам бронировать билеты заранее, поскольку количество-мест на гала-ужин ограничено.</em>");?>
					</div>
					<br />
					<!-- Спикеры -->
					<a name="speakers"></a>
					<h1>Спикеры, основная программа</h1>
					<hr class="mobile" />
					<div class="speakersdiv">
						<div class="speakers group">
							<?
							$scnt = 0;
							foreach ($speakers as $speaker)
								{
								$scnt++;
								$arr_name = explode(" ", $speaker["name"]);
								?>
								<div class="speaker" onClick="$('.mask').fadeIn(300); $('#spdesc<?=$scnt;?>').fadeIn(300);">
									<img src="images/<?=$speaker["image"];?>" />
									<div class="speakerdata">
										<h4><?=$arr_name[0];?><br /><?=$arr_name[1];?> <?=$arr_name[2];?></h4>
									</div>
								</div>
								<div class="speaker_desc" id="spdesc<?=$scnt;?>">
									<div class="close" onClick="$('.mask').fadeOut(300); $('.speaker_desc').fadeOut(300);"><img src="images/close.png" /></div>
									<h4><?=$speaker["name"];?></h4>
									<h5><?=$speaker["short"];?></h5>
									<?=$speaker["about"];?>
								</div>
								<?
								if ($scnt > 0 && $scnt % 4 == 0)
									{
									?><!--<hr class="between" />--><?
								}
							}
							?>
						</div>
						<div class="arrleft" onClick="speaker_left();"><img src="images/arr_left.png" /></div>
						<div class="arrright" onClick="speaker_right();"><img src="images/arr_right.png" /></div>
					</div>
					<div class="spbullets group">
						<?
						$scnt = 0;
						foreach ($speakers as $speaker)
							{
							$scnt++;
							?>
							<div id="spbul_<?=$scnt;?>" class="spbullet<?if ($scnt == 1) {?> active<?}?>" onClick="speaker_go(<?=$scnt;?>);"></div>
							<?
						}
						?>
					</div>
					<h1>Спикеры, орто-завтрак</h1>
					<hr class="mobile" />
					<div class="speakersdiv">
						<div class="speakers1 group">
							<?
							foreach ($speakers2 as $speaker)
								{
								$scnt++;
								$arr_name = explode(" ", $speaker["name"]);
								?>
								<div class="speaker" onClick="$('.mask').fadeIn(300); $('#spdesc<?=$scnt;?>').fadeIn(300);">
									<img src="images/<?=$speaker["image"];?>" />
									<div class="speakerdata">
										<h4><?=$arr_name[0];?><br /><?=$arr_name[1];?> <?=$arr_name[2];?></h4>
									</div>
								</div>
								<div class="speaker_desc" id="spdesc<?=$scnt;?>">
									<div class="close" onClick="$('.mask').fadeOut(300); $('.speaker_desc').fadeOut(300);"><img src="images/close.png" /></div>
									<h4><?=$speaker["name"];?></h4>
									<h5><?=$speaker["short"];?></h5>
									<?=$speaker["about"];?>
								</div>
								<?
								if ($scnt > 0 && $scnt % 4 == 0)
									{
									?><!--<hr class="between" />--><?
								}
							}
							?>
						</div>
						<div class="arrleft" onClick="speaker1_left();"><img src="images/arr_left.png" /></div>
						<div class="arrright" onClick="speaker1_right();"><img src="images/arr_right.png" /></div>
					</div>
					<div class="spbullets1 group">
						<?
						$scnt = 0;
						foreach ($speakers2 as $speaker)
							{
							$scnt++;
							?>
							<div id="spbul_<?=$scnt;?>" class="spbullet<?if ($scnt == 1) {?> active<?}?>" onClick="speaker_go(<?=$scnt;?>);"></div>
							<?
						}
						?>
					</div>
					<a name="price"></a>
					<!-- Стоимость -->
					<h1>Стоимость участия <sup>*</sup></h1>
					<div class="txtdiv">Стоимость участия может измениться в зависимости от даты оплаты. Просим вас не затягивать и позаботиться об оплате мероприятия заблаговременно</div>
					<div class="txtdiv">В рамках мероприятия возможно выбрать интересный именно для Вас формат участия. Вы можете опционально зарегистрироваться только на основной программе Форума 1 - 3 июня, а также принять участие в орто-завтраке и гала-ужине, которые состоятся 2 июня. Пожалуйста, поставьте галочки возле активностей мероприятия, в которых планируете участвовать, и нажмите кнопку "Регистрация"</div>
					<br />
					<a name="choose"></a>
					<h2>Выберите способ участия:</h2>
					<div class="prices">
						<div class="pr_title">
						</div>
						<div class="price hdr semitransp">
							При оплате до<br />15 апреля 2018 г.
						</div>
						<div class="price hdr">
							При оплате после<br /> 15 апреля 2018 г.
						</div>
						<div class="pr_title">
							<div class="cb"><input<?if ($mkcounts[0] < 1) {?> disabled<?}?> type="checkbox" name="forum" id="e_forum" onClick="getprice(1, <?if (date("Y-m-d") < "2018-04-15") {?><?=$allprices[1]["price1"];?><?}else{?><?=$allprices[1]["price2"];?><?}?>, this.checked);" /><label for="e_forum1">Основная программа Форума (1 - 3 июня), осталось мест: <?=$mkcounts[0];?></label></div>
						</div>
						<div class="price<?if (date("Y-m-d") >= "2018-04-15") {?> desktop<?}?> semitransp">
							<div class="cb"><?=number_format($allprices[1]["price1"], 0, ".", " ");?> р.</div>
						</div>
						<div class="price<?if (date("Y-m-d") < "2018-04-15") {?> desktop<?}?>">
							<div class="cb"><?=number_format($allprices[1]["price2"], 0, ".", " ");?> р.</div>
						</div>
						<?if (date("Y-m-d") < "2018-04-15") {?><div class="mobile small">(при оплате до 15 апреля 2018 г., при оплате после 15 апреля 2018 г. стоимость <?=number_format($allprices[1]["price2"], 0, ".", " ");?> р.)</div><?}?>
						<div class="pr_title">
							<div class="cb"><input<?if ($mkcounts[1] < 1) {?> disabled<?}?> type="checkbox" name="bf" id="e_bf" onClick="getprice(2, <?if (date("Y-m-d") < "2018-04-15") {?><?=$allprices[2]["price1"];?><?}else{?><?=$allprices[2]["price2"];?><?}?>, this.checked);" /><label for="e_bf1">Орто-завтрак (утром 2 июня), осталось мест: <?=$mkcounts[1];?></label></div>
						</div>
						<div class="price<?if (date("Y-m-d") >= "2018-04-15") {?> desktop<?}?> semitransp">
							<div class="cb"><?=number_format($allprices[2]["price1"], 0, ".", " ");?> р.</div>
						</div>
						<div class="price<?if (date("Y-m-d") < "2018-04-15") {?> desktop<?}?>">
							<div class="cb"><?=number_format($allprices[2]["price2"], 0, ".", " ");?> р.</div>
						</div>
						<div class="pr_title">
							<div class="cb"><input<?if ($mkcounts[2] < 1) {?> disabled<?}?> type="checkbox" name="dinner" id="e_dinner" onClick="getprice(3, <?if (date("Y-m-d") < "2018-04-15") {?><?=$allprices[3]["price1"];?><?}else{?><?=$allprices[3]["price2"];?><?}?>, this.checked);" /><label for="e_dinner1">Гала-ужин (вечером 2 июня), осталось мест: <?=$mkcounts[2];?></label></div>
						</div>
						<div class="price<?if (date("Y-m-d") >= "2018-04-15") {?> desktop<?}?> semitransp">
							<div class="cb"><?=number_format($allprices[3]["price1"], 0, ".", " ");?> р.</div>
						</div>
						<div class="price<?if (date("Y-m-d") < "2018-04-15") {?> desktop<?}?>">
							<div class="cb"><?=number_format($allprices[3]["price2"], 0, ".", " ");?> р.</div>
						</div>
					</div>
					<br />
					<div class="txt">Регистрация окончена! Вы можете записаться в лист ожидания, связавшись с менеджером Ormco по телефону +7 (812) 324-42-60 или написав на e-mail <a href="mailto:Natalia.Postnikova@ormco.com">Natalia.Postnikova@ormco.com</a>, Наталья Постникова. Присутствие в листе ожидания не гарантирует участие. Надеемся на ваше понимание!</div>
					<div class="afterprices">
						Стоимость участия с учетом выбранных опций: <div class="afterprice" id="eprice">0 р.</div>
						<div class="button"><button id="startreg" type="button" onClick="if ($('#events').val() != '') {fixbody(); $('.mask').fadeIn(300); $('.openform').fadeIn(300);}else{showmes('Выберите способ участия!');}" disabled>Регистрация</button></div>
					</div>
					<h5><sup>*</sup> Членам Профессионального общества ортодонтов предоставляется скидка 15%</h5>
					<br />
					<div class="spec" onClick="$('.mask').fadeIn(300); $('#special').fadeIn(300);">Специальные условия проживания</div>
					<div class="txtdiv">Убедительно просим вас позаботиться о проживании и бронировать отели заранее! В июне Санкт-Петербург особенно прекрасен и пользуется огромной популярностью у туристов со всего мира. Кроме того, в 2018 году в Санкт-Петербурге весной и летом будут проходить Международный экономический форум и матчи в рамках Чемпионата мира по футболу, что также увеличит количество желающих приехать в Северную столицу в период белых ночей</div>
					<br />
					<!-- Место -->
					<a name="place"></a>
					<h1>Место проведения</h1>
					<div class="txtdiv">
						Отель «Коринтия»</h3><br />
						Санкт-Петербург, Невский пр., дом 57
					</div>
					<div class="places group">
						<div class="placeline"></div>
						<div class="placetabs group">
							<div class="placetab sel" onClick="$('.placetab.sel').removeClass('sel'); $(this).addClass('sel'); $('.map').fadeOut(300); $('.gallery').fadeIn(300);">Галерея</div>
							<div class="placetab" onClick="$('.placetab.sel').removeClass('sel'); $(this).addClass('sel'); $('.gallery').fadeOut(300); $('.map').fadeIn(300); initialize_map();">На карте</div>
						</div>
						<div class="placeline"></div>
					</div>
					<div class="gallery">
						<div class="gal_slider group">
							<div class="gal_slide"><img src="images/building.jpg" /></div>
							<div class="gal_slide"><img src="images/building1.jpg" /></div>
							<div class="gal_slide"><img src="images/building2.jpg" /></div>
							<div class="gal_slide"><img src="images/building3.jpg" /></div>
							<div class="gal_slide"><img src="images/building4.jpg" /></div>
							<div class="gal_slide"><img src="images/building5.jpg" /></div>
							<div class="gal_slide"><img src="images/building6.jpg" /></div>
						</div>
						<div class="gal_navs">
							<div class="gal_nav" onClick="gallery_left();"><img src="images/arr_left.png" /></div>
							<div class="gal_nav" onClick="gallery_right();"><img src="images/arr_right.png" /></div>
						</div>
					</div>
					<div class="map"><div id="map"></div></div>
					<a name="form"></a>
					<h1>Регистрация</h1>
					<div class="formmiddle">
						<div class="formtable group">
							<img src="images/face.jpg" />						
							<div class="formtxt">
								В случае появления вопросов или сложностей при регистрации, вы можете связаться с нами по телефону +7&nbsp;(812)&nbsp;324-42-60
							</div>
						</div>
						<a href="#choose"><button type="button">Зарегистрироваться</button></a>
					</div>
					<div class="section footer">
						Ormco Corporation &copy; 2017<br />
						<a href="http://ormco.ru">ormco.ru</a>
					</div>
				</div>
			</div>
		</div>
		<div class="openform">
			<div class="openform_">
				<div class="newform formbox1">
					<div class="formbox_close" onClick="unfixbody(); $('.mask').fadeOut(300); $('.openform').fadeOut(300);"><span class="shape1"></span><span class="shape2"></span></div>
					<h2>Регистрация</h2>
					<h4>Введите Ваш email. Если он уже есть в нашей базе, регистрация пройдет быстрее</h4>
					<input id="f_email" name="f_email" value="<?=$_SESSION["f_email"];?>" autofocus />
					<button type="button" onClick="chk_f_email();">Далее</button>
					<div class="f_anim">
						<div class="at">@</div>
						<div class="arr arr1">←</div>
						<div class="dash arr2"></div>
						<div class="dash arr3"></div>
						<div class="dash arr4"></div>
						<div class="arr arr5">→</div>
						<div class="db">
							<div class="db1"></div>
							<div class="db2"></div>
							<div class="db3"></div>
							<div class="db4"></div>
							<div class="db5"></div>
							<div class="db6"></div>
							<div class="db7"></div>
							<div class="db8"></div>
							<div class="db9"></div>
						</div>
						<div class="f_desc">Проверяем email на наличие в базе данных</div>
					</div>
					<div class="exists">Человек с данным Email уже зарегистрировался на данное мероприятие</div>
				</div>
				<div class="newform formbox2">
					<div class="formbox_close" onClick="unfixbody(); $('.mask').fadeOut(300); $('.openform').fadeOut(300);"><span class="shape1"></span><span class="shape2"></span></div>
					<h2>Вход в систему</h2>
					<h4>Email найден! Пожалуйста, введите пароль от вашей учетной записи Ormco (сайты ormco.ru или orthodontia.ru), чтобы зарегистрироваться на семинар</h4>
					<div class="newform_field group">
						<div class="newform_ttl">Логин (Email):</div>
						<div class="newform_fld"><input id="e_email" name="e_email" value="<?=$_SESSION["f_email"];?>" autofocus /></div>
					</div>
					<div class="newform_field group">
						<div class="newform_ttl">Пароль:</div>
						<div class="newform_fld"><input type="password" id="e_pass" name="e_pass" value="" /><img onClick="chg_passstate();" src="images/eye.svg" /></div>
					</div>
					<div class="newform_link"><span onClick="remember_pass();">Вспомнить пароль</span></div>
					<div id="rp_ok" class="form_msg">Письмо с дальнейшими инструкциями по восстановлению пароля отправлены на указанный email</div>
					<div id="rp_fail" class="form_msg"></div>
					<div class="newform_field group">
						<div class="newform_ttl"></div>
						<div class="newform_fld"><button type="button" id="entry_but" onClick="chk_entry();">Войти</button></div>
					</div>
					<div class="exists">Человек с данным Email уже зарегистрировался на данное мероприятие</div>
					<div class="wrong">Пароль или логин указаны неверно</div>
				</div>
				<div class="newform formbox3">
					<div class="formbox_close" onClick="cleardiscounts(); unfixbody(); $('.mask').fadeOut(300); $('.openform').fadeOut(300);"><span class="shape1"></span><span class="shape2"></span></div>
					<h2>Регистрация на мероприятие</h2>
					<div class="notfound red hidden">Пользователь не найден в базе данных. Вы можете зарегистрироваться на мероприятие, заполнив форму ниже:</div>
					<form action="https://orthodontia.ru/emarket/lporderSmart/" method="post" onSubmit="return chkform(1);" enctype="multipart/form-data">
					<input type="hidden" name="regularprice" id="regularprice" value="0" />
					<input type="hidden" name="mainprice" id="mainprice" value="<?if (date("Y-m-d") < "2018-04-15") {?><?=$allprices[1]["price1"];?><?}else{?><?=$allprices[1]["price2"];?><?}?>" />
					<input type="hidden" name="totalprice" id="totalprice" value="0" />
					<input type="hidden" name="mclasses1" id="mclasses1" value="" />
					<input type="hidden" name="mclasses2" id="mclasses2" value="" />
					<input type="hidden" name="mcl1" id="mcl1" value="" />
					<input type="hidden" name="mcl2" id="mcl2" value="" />
					<input type="hidden" name="p1" id="p1" value="" />
					<input type="hidden" name="p2" id="p2" value="" />
					<input type="hidden" name="file" id="file" value="" />
					<input type="hidden" name="events" id="events" value="" />
					<div class="partbuts">
						<!--<div class="partbut active partbut1" onClick="showpart(2);">Личные данные</div> → <div class="partbut partbut2" onClick="showpart(2);">Способ оплаты</div>
						<h4 class="partbut1">Личные данные</h4>
						<h4 class="partbut2 hidden">Способ оплаты</h4>-->
					</div>
					<hr class="hr">
					<div class="part1">
						<div class="newform_field group">
							<div class="newform_ttl">Email <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="email" id="email" /></div>
						</div>
						<div class="newform_field group phide">
							<div class="newform_ttl"></div>
							<div class="newform_fld small">На этот email будет отправлено подтверждение регистрации и ссылка для активации вашей учетной записи Ormco</div>
						</div>
						<div class="newform_field group phide">
							<div class="newform_ttl">Пароль <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="password" size="30" name="password" id="password" /></div>
						</div>
						<div class="newform_field group phide">
							<div class="newform_ttl">Пароль еще раз <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="password" size="30" name="password2" id="password2" /></div>
						</div>
						<br />
						<div class="newform_field group">
							<div class="newform_ttl">Фамилия <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="lname" id="lname" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Имя <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="fname" id="fname" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Отчество <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="father_name" id="father_name" /></div>
						</div>
						<div class="newform_field group" id="speciality" style="display: none;">
							<div class="newform_ttl big">Специальность для получения баллов по НМО<sup>*</sup></div>
							<div class="newform_fld"><select name="spec" id="spec" class="ness">
									<option value="">-= выберите Вашу специальность =-</option>
									<option value="Ортодонтия">Ортодонтия</option>
									<option value="Стоматология терапевтическая">Стоматология терапевтическая</option>
									<option value="Стоматология общей практики">Стоматология общей практики</option>
									<option value="Ортопедическая стоматология">Ортопедическая стоматология</option>
									<option value="Хирургическая стоматология">Хирургическая стоматология</option>
								</select>
							</div>
						</div>
						<div class="newform_field group" id="birthday">
							<div class="newform_ttl">Дата рождения<sup>*</sup></div>
							<div class="newform_fld"><input placeholder="" class="date1 ness" type="text" size="20" name="bd" id="bd" readonly /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Телефон <sup>*</sup></div>
							<div class="newform_fld"><input placeholder="+7 (987) 1234567" class="short ness" type="text" size="25" name="phone" id="phone" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Страна<sup>*</sup></div>
							<div class="newform_fld"><select name="country" id="country" class="ness" onChange="if (this.value == 'Россия') {$('.region_field').fadeIn(300);}else{$('.region_field').fadeOut(300);}">
									<option value="Россия">Россия</option>
									<option value="Азербайджан">Азербайджан</option>
									<option value="Армения">Армения</option>
									<option value="Белоруссия">Белоруссия</option>
									<option value="Грузия">Грузия</option>
									<option value="Украина">Украина</option>
									<option value="Другая">Другая</option>
								</select>
							</div>
						</div>
						<div class="newform_field group region_field">
							<div class="newform_ttl">Регион <sup>*</sup></div>
							<div class="newform_fld"><select name="region" id="region" class="ness">
								<option value="">-= выберите регион =-</option>
								<option value="9870">Москва и МО</option>
								<option value="9871">Санкт-Петербург и ЛО</option>
								<option value="9872">Северо-Западный</option>
								<option value="9873">Центральный</option>
								<option value="9874">Сибирский</option>
								<option value="9875">Приволжский</option>
								<option value="9876">Южный</option>
								<option value="9877">Северо-Кавказский</option>
								<option value="9878">Уральский</option>
								<option value="9879">Дальневосточный</option>
								<option value="16761">Не Россия</option>
							</select></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Город<sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="city" id="city" /></div>
						</div>
						<div class="newform_field group" id="speciality">
							<div class="newform_ttl">Кто Вы? <sup>*</sup></div>
							<div class="newform_fld"><select name="who" id="who" class="ness">
									<option value="">Кто Вы?</option>
									<option value="Ортодонт">Ортодонт</option>
									<option value="Хирург">Хирург</option>
									<option value="Другая специализация">Другая специализация</option>
									<option value="Не врач">Не врач</option>
								</select>
							</div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Клиника/ВУЗ<sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="company" id="company" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl"></div>
							<div class="newform_fld"><button type="button" id="entry_but" onClick="showpart(2);">Далее</button></div>
						</div>
					</div>
					<div class="part2 hidden">
						<h3>Выберите скидку. Обратите внимание: скидка будет направлена менеджеру Ormco для подтверждения</h3>
						<h3>Скидка распространяется только на стоимость основной программы</h3>
						<div id="nodiscounts" class="hidden">
							<h4>Выбрать скидку можно только при участии в основной программе</h4>
						</div>
						<div id="discounts">
							<div class="newform_field group">
								<div class="newform_flda"><input type="checkbox" class="dscnt upl" name="poo" id="poo" value="Член ПОО" onClick="$('.newform_upload').fadeIn(300); $('#uploaded').fadeIn(300); setdiscount(15, 'poo', this.checked);" /> <label for="poo">Являюсь членом Профессионального общества ортодонтов</label></div>
							</div>
							<!--<div class="newform_field group">
								<div class="newform_flda"><input type="checkbox" class="dscnt" name="school" id="school" value="Действующий участник Школы ортодонтии" onClick="$('.newform_upload').fadeOut(300); $('#uploaded').fadeOut(300); setdiscount(50, 'school', this.checked);" /> <label for="school">Действующий участник Школы ортодонтии</label></div>
							</div>
							<div class="newform_field group">
								<div class="newform_flda"><input type="checkbox" class="dscnt upl" name="ordinator" id="ordinator" value="Ординатор" onClick="$('.newform_upload').fadeIn(300); $('#uploaded').fadeIn(300); setdiscount(40, 'ordinator', this.checked);" /> <label for="ordinator">Я ординатор</label></div>
							</div>-->
							<div class="newform_field group hidden" id="promo_radio">
								<div class="newform_flda"><input type="checkbox" disabled checked class="dscnt" name="promo_dscnt" id="promo_dscnt" value="Скидка по промокоду 0%" onClick="$('.newform_upload').fadeOut(300); $('#uploaded').fadeOut(300); setdiscount(promo_dsc, 'promo_dscnt', this.checked);" /> <label for="promo_dscnt">Скидка по промо-коду <span id="promo_d">0</span>%</label></div>
							</div>
							<div class="newform_field newform_upload hidden group">
								<div class="newform_fldu">Загрузить удостоверение <button type="button" class="upload" name="upload" id="upload">Выберите файл</button></div>
							</div>
							<div class="uploaded hidden" id="uploaded">(форматы jpg, gif, png или pdf)</div>
							<div id="promo_field" class="hidden">
								<hr class="hr">
								<h4>Если у вас есть промокод, введите его в поле ниже и нажмите кнопку «Применить промокод»</h4>
								<div class="newform_field group">
									<div class="newform_ttl">Промо-код</div>
									<div class="newform_fld"><input type="text" size="30" name="promo" id="promo" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl"></div>
									<div class="newform_fld"><button type="button" onClick="chk_promo();">Применить промокод</button></div>
								</div>
								<div id="promo_fail" class="form_msg hidden">Вы указали неверный промокод</div>
								<div id="promo_used" class="form_msg1 hidden">Промокод применен!<br /> Добавлено бесплатное участие в орто-завтраке 2 июня!</div>
							</div>
							<div class="discount hidden">
								Ваша скидка <span id="discount">0</span> процентов
							</div>
						</div>
						<div class="finalprice">
							Стоимость участия <span id="finalprice">0 р.</span>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Оплата:</div>
							<div class="newform_fld"><div class="tabbut active tabbut1" onClick="showtab(1);">Как физ. лицо</div><div class="tabbut tabbut2" onClick="showtab(2);">Как юр. лицо</div></div>
						</div>
						<div class="tab1">
							<div class="newform_field group">
								<div class="newform_ttl">Способ оплаты:</div>
								<div class="newform_fld"><select name="payment_id" id="payment_id" class="ness" onChange="if (this.value == 4666) {showtab(2);}">
										<option value="">выберите способ оплаты</option>
										<option value="4663">Квитанция в банк</option>
										<option value="4664" comment="Не позднее, чем за 10 дней до мероприятия!">Наличными в офисе Ormco</option>
										<option value="17688" comment="Приготовьтесь сразу произвести оплату">Онлайн оплата</option>
										<option value="4666">Счет для юр. лиц</option>
									</select>
								</div>
							</div>
						</div>
						<div class="tab2 hidden">
							<div class="newform_field group yurs">
								<div class="newform_ttl">Юр. лицо:</div>
								<div class="newform_fld"><select name="yur" id="yur" class="ness" disabled>
									</select>
								</div>
							</div>
							<div class="addyur" onClick="addnewyur();">
								<span>+</span> Добавить новое юр. лицо
								<small>Приготовьтесь ввести реквизиты юр. лица</small>
							</div>
							<div class="addyurform addyurform1 hidden">
								<div class="addyur_close" onClick="$('.addyurform').fadeOut(300, function(){$('.newform_reg').fadeIn(300); $('.addyur').fadeIn(300); $('.yurs').fadeIn(300);})"><span class="shape1"></span><span class="shape2"></span></div>
								<div class="newform_field group">
									<div class="newform_ttl">Новое юр. лицо:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="yurname" id="yurname" /><small>Введите юридическое название организации</small></div>
								</div>
								<h4>Контактная информация</h4>
								<div class="newform_field group">
									<div class="newform_ttl">ФИО:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="fio_rukovoditelya" id="fio_rukovoditelya" /><small>ФИО руководителя организации</small></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Должность:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="dolzhnost" id="dolzhnost" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Контактное лицо:</div>
									<div class="newform_fld"><input type="text" size="30" name="contact_person" id="contact_person" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Email<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="yuremail" id="yuremail" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Телефон</div>
									<div class="newform_fld"><input class="short" type="text" size="30" name="phone_number" id="phone_number" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Факс</div>
									<div class="newform_fld"><input class="short" type="text" size="30" name="fax" id="fax" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl"></div>
									<div class="newform_fld"><button type="button" onClick="showyur(2);">Далее</button></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl"></div>
									<div class="newform_fld"><button type="button" onClick="cancelyur();">Отменить добавление</button></div>
								</div>
							</div>
							<div class="addyurform addyurform2 hidden">
								<div class="addyur_back" onClick="showyur(1);"></div>
								<div class="addyur_close" onClick="$('.addyurform').fadeOut(300, function(){$('.newform_reg').fadeIn(300); $('.addyur').fadeIn(300); $('.yurs').fadeIn(300);})"><span class="shape1"></span><span class="shape2"></span></div>
								<div class="newform_field group">
									<div class="newform_ttl">ИНН:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="inn" id="inn" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">КПП:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="kpp" id="kpp" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Рассчетный счет:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="account" id="account" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">БИК:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness short" type="text" size="30" name="bik" id="bik" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Корр. счет:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="bank_account" id="bank_account" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">ОГРН:</div>
									<div class="newform_fld"><input type="text" size="30" name="ogrn" id="ogrn" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Юридический адрес:<sup>*</sup></div>
									<div class="newform_fld"><textarea class="ness" name="legal_address" id="legal_address"></textarea></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Фактический адрес:<sup>*</sup></div>
									<div class="newform_fld"><textarea class="ness" name="defacto_address" id="defacto_address"></textarea></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Почтовый индекс:</div>
									<div class="newform_fld"><input type="text" size="30" name="postal_address" id="postal_address" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl"></div>
									<div class="newform_fld"><button type="button" onClick="addyurajax();">Добавить юр. лицо</button></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl"></div>
									<div class="newform_fld"><button type="button" onClick="cancelyur();">Отменить добавление</button></div>
								</div>
							</div>
						</div>
						<!--<div id="igosel">
							<h4>Лекция 8 февраля (осталось <?=$mkcounts[0];?> мест)</h4>
						</div>-->
						<div class="centered">
							<div class="g-recaptcha" data-sitekey="6LfeL0sUAAAAAOTlaQ2i74ORz6UmnK73xjEn8ULy"></div>
							<div class="captcha_err">Поставьте галочку, докажите, что Вы не робот</div>
						</div>
						<div class="newform_field newform_reg group">
							<div class="newform_ttl"></div>
							<div class="newform_fld"><button id="register">Зарегистрироваться</button></div>
						</div>
						<!--<div class="newform_field group">
							<div class="newform_ttla double">Планирую покупку продукции Ormco в ближайшее время (1-2 месяца)</div>
							<div class="newform_flda"><input type="checkbox" size="30" name="want_to_buy" id="want_to_buy" value="1" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttla double">Принимал(а) участие в образовательных мероприятиях Ormco в течение последнего года</div>
							<div class="flda"><input type="checkbox" size="30" name="how_you_been_edu" id="how_you_been_edu" value="1" /></div>
						</div>
						<div id="igosel">
							<h3>Выбранные мастер-классы</h3>
							<div class="igosel" onClick="$('.igodd').fadeToggle(200); $(this).toggleClass('sel');"><span>Выберите три мастер-класса</span></div>
							<div class="igodd">
								<?
								foreach ($mks as $n => $nks)
									{
									?>
									<div class="igotime igo<?=$n;?>"><?=$mktimes[$n];?></div>
									<?
									foreach ($nks as $m => $mk)
										{
										?>
										<div class="igoset igo<?=$n;?><?if ($mkcounts[$m - 1] <= 0) {?> faded cancelled<?}?>" data-group="<?=$n;?>" data-mk="<?=$m;?>" id="igo_<?=$m;?>"><input type="checkbox" <?if ($mkcounts[$m - 1] <= 0) {?>disabled <?}?>name="iset<?=$m;?>" id="iset<?=$m;?>" onClick="igo(<?=$n;?>, <?=$m;?>, '<strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?>', this.checked);" /><label for="iset<?=$m;?>"><strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?></label></div>
										<?
									}
								}
								?>
							</div>
							<div class="chosen"></div>
						</div>-->
						<div class="formagree">Нажимая «Зарегистрироваться», вы соглашаетесь с <span class="lnk" onClick="$('#pol').fadeIn(300);">политикой конфиденциальности</span> и <a target="_blank" href="https://orthodontia.ru/files/sys/Dogovor-prisoedineniya-Ormco.docx">условиями договора-оферты</a></div>
					</div>
					</form>
				</div>
			</div>
		</div>
		<div class="mask"></div>
		<div class="mask1"></div>
		<div class="burger" onClick="$('#mobmenu').slideDown(300);"><img src="images/burger.png" /></div>
		<div id="mobmenu" class="mobmenu">
			<div class="close2" onClick="$('#mobmenu').slideUp(300);"><img src="images/close2.png" /></div>
			<ul>
				<li id="miprogramm1"><a href="#programm">Программа</a></li>
				<li id="mispeakers1"><a href="#speakers">Спикеры</a></li>
				<li id="miprice1"><a href="#price">Стоимость</a></li>
				<li id="miplace1"><a href="#place">Место проведения</a></li>
				<li id="miform1"><a href="#choose">Регистрация</a></li>
			</ul>
		</div>
		<div class="form_error">
			<div class="close" onClick="$('.form_error').fadeOut(300); $('.mask1').fadeOut(300);"><img src="images/close.png" /></div>
			<h3 id="form_error"></h3>
		</div>
<div id="pol">
<div class="close" onClick="$('#pol').fadeOut(300);"><img src="images/close.png" /></div>
<div id="inpol">
<h2>Политика конфиденциальности</h2>
<h4>1. Общие положения</h4>
Корпорация Ormco в лице ООО «Ормко» (далее «Ормко») и её аффилированные лица с уважением относятся к конфиденциальной информации любого лица, ставшего посетителем данного сайта. Мы хотели бы проинформировать Вас о том, какие именно данные мы собираем и каким образом используем собранные данные. Вы также узнаете о том, как Вы можете проверить точность собранной информации и дать нам указание об удалении подобной информации. Данные собираются, обрабатываются и используются строго в соответствии с требованиями действующего законодательства того государства, в котором расположено соответствующее аффилированное лицо компании "Ормко", отвечающее за защиту персональных данных. Мы делаем все возможное для обеспечения соответствия требованиям действующего законодательства.
Данное заявление не распространяется на сайты, на которые сайт компании "Ормко" содержит гиперссылки.
<h4>2. Сбор, использование и переработка персональных данных</h4>
Мы осуществляем сбор информации, относящейся к определенным лицам, лишь в целях обработки и использования информации и только в том случае, если Вы добровольно предоставили информацию или явно выразили свое согласие на ее использование.
Когда Вы посещаете наш сайт, определенные данные автоматически записываются на серверы компании "Ормко" и/или её аффилированных лиц для целей системного администрирования или для статистических или резервных целей. Записываемая информация содержит название Вашего интернет-провайдера, в некоторых случаях Ваш IP-адрес, данные о версии программного обеспечения Вашего браузера, об операционной системе компьютера, с которого Вы посетили наш сайт, адреса сайтов, после посещения которых Вы по ссылке зашли на наш сайт, заданные Вами параметры поиска, приведшие Вас на наш сайт.
В зависимости от обстоятельств, подобная информация позволяет сделать выводы о том, какая аудитория посещает наш сайт. В данном контексте, однако, не используются никакие персональные данные. Использоваться может лишь анонимная информация. Если информация передается компанией "Ормко" и/или её аффилированными лицами внешнему провайдеру, принимаются все возможные технические и организационные меры, гарантирующие передачу данных в соответствии с требованиями действующего законодательства.
Если Вы добровольно предоставляете нам персональную информацию, мы обязуемся не использовать, не обрабатывать и не передавать такую информацию способом, выходящим за рамки требований действующего законодательства. Использование и распространение Ваших персональных данных без Вашего согласия возможно только на основании судебного решения или в ином порядке, предусмотренном законодательством РФ.
Любые изменения, которые будут внесены в правила по соблюдению конфиденциальности, будут размещены на данной странице. Это позволяет Вам в любое время получить информацию о том, какие данные у нас хранятся и о том, каким образом мы собираем и храним такие данные.
<h4>3. Безопасность данных</h4>
Компания "Ормко" и её аффилированные лица обязуется бережно хранить Ваши персональные данные и принимать все меры предосторожности для защиты Ваших персональных данных от утраты, неправильного использования или внесения в персональные данные изменений. Партнеры компании "Ормко" и её аффилированных лиц, которые имеют доступ к Вашим данным, необходимым им для предоставления Вам услуг от имени компании "Ормко" и её аффилированных лиц, несут перед компанией "Ормко" и её аффилированными лицами закрепленные в контрактах обязательства соблюдать конфиденциальность данной информации и не имеют права использовать предоставляемые данные для каких-либо иных целей.
<h4>4. Персональные данные несовершеннолетних потребителей</h4>
Компания "Ормко" и её аффилированные лица не ведет сбор информации в отношении потребителей, не достигших 14 лет. При необходимости, мы можем специально попросить ребенка не присылать в наш адрес никакой личной информации. Если родители или иные законные представители ребенка обнаружат, что дети сделали какую-либо информацию доступной для компании "Ормко" и её аффилированных лиц, и сочтут, что предоставленные ребенком данные должны быть уничтожены, таким родителям или иным законным представителям необходимо связаться с нами по нижеуказанному (см. п. 6) адресу. В этом случае мы немедленно удалим личную информацию о ребенке.
<h4>5. Файлы Cookie</h4>
Мы используем файлы cookie для того, чтобы предоставить Вам больше возможностей при использовании сайта. Файлы cookie представляют собой небольшие блоки данных, помещаемые Вашим браузером на временное хранение на жестком диске Вашего компьютера, необходимые для навигации по нашему сайту. Файлы cookie не содержат никакой личной информации о Вас и не могут использоваться для идентификации отдельного пользователя. Файл cookie часто содержит уникальный идентификатор, представляющий собой анонимный номер (генерируемый случайным образом), сохраняемый на Вашем устройстве. Некоторые файлы удаляются по окончании Вашего сеанса работы на сайте; другие остаются на Вашем компьютере дольше.
Приступая к использованию данного веб-сайта, вы соглашаетесь на использование файлов cookie, Вы также признаете, что в подобном контенте могут использоваться свои файлы cookie.
Компания Ормко не контролирует и не несет ответственность за файлы cookie сторонних разработчиков. Дополнительную информацию Вы можете найти на сайте разработчика.
<h4>6. Отслеживание через интернет</h4>
На данном сайте осуществляется сбор и хранение данных для маркетинга и оптимизации с использованием технологии Яндекс.Метрика и Google Analitycs. Эти данные могут использоваться для создания профилей пользователей под псевдонимами. Сайт может устанавливать файлы cookie.
Без ясно выраженного согласия наших пользователей данные, собираемые с помощью технологий Яндекс.Метрика и Google Analitycs, не используются для идентификации личности посетителя и не связываются с какими-либо другими личными данными носителя псевдонима.
Дополнительную информацию об отслеживании через интернет Вы можете найти на сайтах провайдеров сервисов Яндекс.Метрика и Google Analitycs.
<h4>7. Ваши пожелания и вопросы</h4>
Хранящиеся данные будут стерты компанией "Ормко" и/или её аффилированными лицами по истечении периода хранения, установленного законодательством или договором либо в случае если сама компания "Ормко" и/или её аффилированные лица отменит хранение тех или иных данных. Вы вправе в любое время потребовать удаления из базы данных компании "Ормко" и/или её аффилированных лиц информации о Вас. Вы также вправе в любое время отозвать Ваше согласие на использование или переработку Ваших персональных данных. В таких случаях, а также, если у Вас есть какие-либо иные пожелания, связанные с Вашими персональными данными, просим Вас выслать письмо по почте в адрес «Ормко» в России по адресу: 195112, Санкт-Петербург, Малоохтинский пр-т, д. 64, корп. 3. или по электронной почте dc-sales@ormco.com. Просим Вас также связаться с нами в случае, если Вам хотелось бы узнать, собираем ли мы данные о Вас и если да, то какие именно данные. Мы постараемся выполнить Ваши пожелания в возможно короткие сроки.
<h4>8. Законодательство по обработке персональных данных</h4>
Все действия с персональными данными, собираемыми на данном сайте, производятся в соответствии с Федеральным законом Российской Федерации №152-ФЗ от 27 июля 2006 года «О персональных данных».
<h4>(1) Заявленная цель сбора, обработки или использования данных:</h4>
•	Предметом деятельности «Ормко» и её аффилированных лиц является производство и распространение стоматологических продуктов всех типов, главным образом брекет-систем, ортодонтических инструментов, микроимплантов, адгезивов;
<h4>(2) Описание групп вовлеченных лиц и соответствующих данных или категорий данных:</h4>
Данные, касающиеся заказчиков, сотрудников, пенсионеров, сотрудников сторонних компаний (субподрядчиков), персонала, работающего по лизингу, претендентов на рабочие места, авторов изобретений, не являющихся сотрудниками компаний, или наследников, соответственно, поставщиков товаров и услуг, сторонних заказчиков, потребителей, добровольцев, участвующих в потребительских испытаниях, посетителей производственных объектов корпорации, инвесторов – насколько это необходимо для выполнения целей, определенных в пункте 4.
<h4>(3) Получатели или категории получателей, которым могут быть разглашены данные:</h4>
Органы власти, фонды страхования здоровья, ассоциация по страхованию ответственности работодателей при наличии соответствующего правового регулирования, сторонние подрядчики в соответствии сторонние поставщики услуг, ассоциация пенсионеров «Ормко», аффилированные лица и внутренние подразделения для выполнения целей, указанных в пункте 4.
<h4>(4) Периодичность регулярного удаления данных:</h4>
Юристами подготовлено множество инструкций, касающихся обязанностей по хранению данных и периодов хранения. Данные удаляются в установленном порядке по истечении указанных периодов. Данные, не подпадающие под действие данных условий, удаляются, если цели, указанные в пункте 4, перестают существовать.
<h4>(5) Запланированная передача данных другим странам:</h4>
В рамках всемирной системы информации о трудовых ресурсах, данные по персоналу должны быть доступны определенным руководящим работникам в других странах. Соответствующие соглашения о защите данных должны быть заключены с соответствующими компаниями в соответствии со стандартами ЕС.
<h4>9. Использование встраиваемых модулей для социальных сетей</h4>
На наших интернет-страницах предусмотрена возможность встраивания модулей для социальных сетей ВКонтакте, Twitter, Instagram, Youtube (далее – «провайдеры»).
Только если Вы активируете модуль, тем самым разрешая передачу данных, браузер создаст прямое соединение с сервером провайдера. Содержимое различных модулей впоследствии передается соответствующим провайдером непосредственно в Ваш браузер и выводится на экран Вашего компьютера.
Модуль сообщает провайдеру, на какую из страниц нашего сайта Вы вошли. Если во время просмотра нашего сайта Вы вошли на ВКонтакте, Instagram, Youtube или Twitter под своей учетной записью, провайдер может подобрать информацию, в соответствии с Вашими интересами, т.е. информацию, которую Вы просматриваете с помощью Вашей учетной записи. При использовании какой-либо функции встроенного модуля (например, кнопки “Мне нравится”, размещения комментария), эта информация также будет передана браузером непосредственно провайдеру для сохранения.
Дополнительную информацию по сбору и использованию данных социальными сетями, а также по правам и возможностям защиты Вашей конфиденциальности в данных обстоятельствах, можно найти в рекомендациях провайдеров по защите данных /конфиденциальности:
Для того, чтобы не подключаться к учетным записям провайдеров при посещении нашего сайта, Вам необходимо отключиться от соответствующей учетной записи перед посещением наших интернет-страниц.
</div>
</div>
	</body>
</html>
<?
function ourdate($dt, $incly) {
	$month = array("", "января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря");
	
	$newdt = date("d" ,strtotime($dt))." ".$month[date("n",strtotime($dt))];
	if ($incly == 1 && date("Y",strtotime($dt)) != date("Y"))
		$newdt .= " ".date("Y",strtotime($dt))." г.";
	return $newdt;
}
function places($num) {
	$rest = $num % 10;
	if ($num > 4 && $num < 21)
		return "мест";
	else {
		if ($num > 1 && $num < 5)
			return "места";
		else {
			if ($num == 1 || $rest == 1)
				return "место";
			else {
				if ($num > 20 && $rest > 1 && $rest < 5)
					return "места";
				else
					return "мест";
			}
		}
	}
}
?>