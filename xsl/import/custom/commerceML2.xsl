<?xml version="1.0" encoding="UTF-8"?>
<!--
TODO: // Write here your own templates
-->

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:php="http://php.net/xsl"
	xmlns:udt="http://umi-cms.ru/2007/UData/templates"
	extension-element-prefixes="php"
	exclude-result-prefixes="xsl php udt">
	
	<xsl:template match="Предложения/Предложение">
		<page id="{Ид}" update-only="1">
			<properties>
				<xsl:apply-templates select="Цены" />

				<group name="catalog_stores_props" title="Склады">
					
					<xsl:if test="NDS">
						<property name="nds" title="НДС" type="relation" is-public="1" allow-runtime-add="1">
							<type data-type="relation" />
							<title>НДС</title>
							<value>
								<item name="{NDS}" >
									<xsl:if test="NDS=0">
										<xsl:attribute name="name">Без НДС</xsl:attribute>
									</xsl:if>
								</item>
							</value>
						</property>
						
					</xsl:if> 
					
					
					<property name="common_quantity" title="Общее количество на складах" type="float" is-public="1" allow-runtime-add="1">
						<type data-type="float" />
						<title>Общее количество на складах</title>
						<value><xsl:value-of select="Количество"/></value>
					</property>
				</group>
			</properties>
		</page>
	</xsl:template>


</xsl:stylesheet>
