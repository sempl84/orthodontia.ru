<?xml version="1.0" encoding="UTF-8"?>

<!-- Шаблон преобразования формата CommerceML 2.04 в формат UMIDump 2.0 -->

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:php="http://php.net/xsl"
	xmlns:udt="http://umi-cms.ru/2007/UData/templates"
	extension-element-prefixes="php"
	exclude-result-prefixes="xsl php udt">

	<xsl:output encoding="utf-8" method="xml" indent="yes" cdata-section-elements="value" />

	<!-- Переменные -->
	<xsl:key name="property" match="/КоммерческаяИнформация/Классификатор/Свойства/*" use="Ид"/>
	<xsl:key name="price-definition" match="/КоммерческаяИнформация/ПакетПредложений/ТипыЦен/ТипЦены" use="Ид"/>

	<xsl:variable name="price-types" select="/КоммерческаяИнформация/ПакетПредложений/ТипыЦен/ТипЦены" />
	<xsl:variable name="settings" select="document('udata://exchange/getTranslatorSettings/')/udata" />

	<xsl:variable name="catalog_rubric_activity" select="number($settings//item[@key='exchange.translator.catalog_rubric_activity'])" />
	<xsl:variable name="catalog_rubric_visible" select="number($settings//item[@key='exchange.translator.catalog_rubric_visible'])" />

	<xsl:variable name="catalog_item_activity" select="number($settings//item[@key='exchange.translator.catalog_item_activity'])" />
	<xsl:variable name="catalog_item_visible" select="number($settings//item[@key='exchange.translator.catalog_item_visible'])" />

	<xsl:variable name="catalog_rubric_template" select="string($settings//item[@key='exchange.translator.catalog_rubric_template'])" />
	<xsl:variable name="catalog_item_template" select="string($settings//item[@key='exchange.translator.catalog_item_template'])" />

	<xsl:variable name="catalog-id" select="/КоммерческаяИнформация/Каталог/Ид" />

	<xsl:template match="/">
		<umidump xmlns:xlink="http://www.w3.org/TR/xlink" version="2.0">
			<xsl:apply-templates select="КоммерческаяИнформация/Классификатор" />
			<xsl:apply-templates select="КоммерческаяИнформация/Каталог" />
			<xsl:apply-templates select="КоммерческаяИнформация/ПакетПредложений" />

			<xsl:if test="count(КоммерческаяИнформация/Документ)">
				<xsl:apply-templates select="КоммерческаяИнформация" mode="document" />
			</xsl:if>
		</umidump>
	</xsl:template>

	<!-- Заказы -->
	<xsl:template match="КоммерческаяИнформация" mode="document">
		<meta>
			<source-name>commerceML2</source-name>
		</meta>

		<objects>
			<xsl:apply-templates select="Документ" />
		</objects>
	</xsl:template>

	<xsl:template match="Документ">
		<xsl:param name="properties" select="ЗначенияРеквизитов/ЗначениеРеквизита" />
		<xsl:param name="is_paid" select="string-length($properties[Наименование = 'Дата оплаты по 1С']/Значение) > 0" />
		<xsl:param name="is_dispatched" select="string-length($properties[Наименование = 'Дата отгрузки по 1С']/Значение) > 0" />
		<xsl:param name="is_canceled" select="$properties[Наименование = 'ПометкаУдаления']/Значение = 'true'" />

		<object id="{Номер}" update-only="1">
			<properties>
				<group name="order_props">
					<title>Свойства заказа</title>

					<xsl:if test="$is_paid">
						<property name="payment_status_id">
							<title>Статус оплаты</title>
							<value>accepted</value>
						</property>
					</xsl:if>

					<xsl:if test="$is_dispatched">
						<property name="status_id">
							<title>Статус заказа</title>
							<value>delivery</value>
						</property>
					</xsl:if>

					<xsl:if test="$is_canceled">
						<property name="status_id">
							<title>Статус заказа</title>
							<value>canceled</value>
						</property>
					</xsl:if>
				</group>

				<group name="integration_date">
					<title>Свойства для интеграции с 1С</title>

					<property name="1c_order_number" title="Номер заказа в 1С" type="string" allow-runtime-add="1">
						<type data-type="string" />
						<title>Номер заказа в 1С</title>
						<value><xsl:value-of select="$properties[Наименование = 'Номер по 1С']/Значение"/></value>
					</property>

					<xsl:if test="string-length($properties[Наименование = 'Дата оплаты по 1С']/Значение)">
						<property name="payment_date" title="Дата оплаты" type="date" allow-runtime-add="1">
							<type data-type="date" />
							<title>Дата оплаты</title>
							<value><xsl:value-of select="$properties[Наименование = 'Дата оплаты по 1С']/Значение" /></value>
						</property>
					</xsl:if>

					<xsl:if test="string-length($properties[Наименование = 'Номер оплаты по 1С']/Значение)">
						<property name="payment_document_num" title="Номер платежного документа" type="string" allow-runtime-add="1">
							<type data-type="string" />
							<title>Номер платежного документа</title>
							<value><xsl:value-of select="$properties[Наименование = 'Номер оплаты по 1С']/Значение"/></value>
						</property>
					</xsl:if>

					<property name="need_export" title="Выгружать заказ в 1С при следующем сеансе связи" type="boolean" allow-runtime-add="1">
						<type data-type="boolean" />
						<title>Выгружать заказ в 1С при следующем сеансе связи</title>
						<value>0</value>
					</property>
				</group>
			</properties>
		</object>
	</xsl:template>

	<!-- Предложения -->
	<xsl:template match="ПакетПредложений">
		<meta>
			<source-name>commerceML2</source-name>
		</meta>
		<pages>
			<xsl:apply-templates select="Предложения/Предложение" />
		</pages>
	</xsl:template>

	<xsl:template match="Предложения/Предложение">
		<page id="{Ид}" update-only="1">
			<properties>
				<xsl:apply-templates select="Цены" />

				<group name="catalog_stores_props" title="Склады">
					<property name="common_quantity" title="Общее количество на складах" type="float" is-public="1" allow-runtime-add="1">
						<type data-type="float" />
						<title>Общее количество на складах</title>
						<value><xsl:value-of select="Количество"/></value>
					</property>
				</group>
			</properties>
		</page>
	</xsl:template>

	<xsl:template match="Цены">
		<xsl:param name="default-price" select="Цена[ИдТипаЦены = string($settings//item[@key='exchange.translator.1c_price_type_id'])]" />
		<xsl:param name="count-price" select="$settings//item[@key='prices']" />

		<group name="cenovye_svojstva" title="Ценовые свойства">
			<xsl:choose>
				<xsl:when test="count(Цена) > 1 and $default-price">
					<xsl:apply-templates select="$default-price" mode="default-price"/>
				</xsl:when>
				<xsl:when test="count(Цена) > 1 and not($default-price)">
					<xsl:apply-templates select="Цена[position() = 1]" mode="default-price"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="Цена" mode="default-price"/>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="Цена" mode="other-price"/>
		</group>
	</xsl:template>

	<xsl:template match="Цена" mode="default-price">
		<xsl:variable name="currency_ref" select="concat('udata://exchange/getCurrencyCodeByAlias/?alias=', php:function('urlencode', string(Валюта)))" />
		<xsl:variable name="currency" select="document($currency_ref)/udata" />
		<xsl:variable name="definition" select="key('price-definition', ИдТипаЦены)" />

		<property name="price" title="Цена" type="price" is-public="1" allow-runtime-add="1">
			<type data-type="price" />
			<title>Цена</title>
			<value currency_code="{$currency}"><xsl:value-of select="ЦенаЗаЕдиницу"/></value>
		</property>
	</xsl:template>

	<xsl:template match="Цена" mode="other-price">
		<xsl:variable name="price_id" select="concat('exchange.translator.1c_price.', ИдТипаЦены)" />

		<xsl:if test="$settings//item[@key=$price_id]">
			<xsl:variable name="currency_ref" select="concat('udata://exchange/getCurrencyCodeByAlias/?alias=', php:function('urlencode', string(Валюта)))" />
			<xsl:variable name="currency" select="document($currency_ref)/udata" />

			<property name="{$settings//item[@key=$price_id]}" is-public="1" visible="visible" allow-runtime-add="1">
				<xsl:attribute name="title">
					<xsl:value-of select="key('price-definition', ИдТипаЦены)/Наименование"/>
				</xsl:attribute>

				<type data-type="price" />
				<title>
					<xsl:value-of select="key('price-definition', ИдТипаЦены)/Наименование"/>
				</title>
				<value currency_code="{$currency}">
					<xsl:value-of select="ЦенаЗаЕдиницу"/>
				</value>
			</property>
		</xsl:if>
	</xsl:template>

	<!-- Товары -->
	<xsl:template match="Классификатор">
		<meta>
			<source-name>commerceML2</source-name>
		</meta>

		<types>
			<!-- Корневой тип "Объект каталога" -->
			<type id="root-catalog-object-type" title="Объект каталога" parent-id="root-pages-type" locked="locked">
				<base module="catalog" method="object">Объекты каталога</base>
				<fieldgroups>
					<group name="common" title="Основные параметры">
						<field name="title" title="i18n::field-title">
							<type name="Строка" data-type="string"/>
						</field>

						<field name="h1" title="i18n::field-h1">
							<type name="Строка" data-type="string"/>
						</field>
					</group>
				</fieldgroups>
			</type>

			<!-- Корневой тип "Раздел каталога" -->
			<type id="root-catalog-category-type" title="Раздел каталога" parent-id="root-pages-type">
				<base module="catalog" method="category">Разделы каталога</base>
				<fieldgroups>
					<group name="common" title="Основные параметры" locked="locked">
						<field name="title" title="i18n::field-title">
							<type name="Строка" data-type="string"/>
						</field>

						<field name="h1" title="i18n::field-h1" visible="visible">
							<type name="Строка" data-type="string"/>
						</field>
					</group>
				</fieldgroups>
			</type>

			<xsl:apply-templates select="Группы" mode="types" />
		</types>
	</xsl:template>

	<xsl:template match="Группы/Группа" mode="types">
		<type id="{Ид}" title='1C: {Наименование}' parent-id="root-catalog-object-type">
			<xsl:if test="name(../../.) = 'Группа'">
				<xsl:attribute name="parent-id"><xsl:value-of select="../../Ид" /></xsl:attribute>
			</xsl:if>

			<base module="catalog" method="object">Объект каталога</base>

			<fieldgroups>
				<group name="product" title="1C: Общие свойства" visible="visible">
					<field name="photo" title="Картинка" visible="visible">
						<type name="Изображение" data-type="img_file"/>
					</field>

					<field name="1c_catalog_id" title="Идентификатор каталога 1С" visible="visible">
						<type name="Строка" data-type="string"/>
					</field>

					<field name="1c_product_id" title="Идентификатор в 1С" visible="visible">
						<type name="Строка" data-type="string"/>
					</field>

					<field name="artikul" title="Артикул" visible="visible">
						<type name="Строка" data-type="string"/>
					</field>

					<field name="bar_code" title="Штрих-код" visible="visible">
						<type name="Строка" data-type="string"/>
					</field>

					<field name="weight" title="Вес" visible="visible">
						<type name="Число с точкой" data-type="float"/>
					</field>
				</group>
			</fieldgroups>
		</type>

		<xsl:apply-templates select="Группы" mode="types" />
	</xsl:template>

	<xsl:template match="Каталог">
		<pages>
			<!-- Корневой каталог -->
			<page id="{$catalog-id}" type-id="root-catalog-category-type">
				<default-active>
					<xsl:value-of select="$catalog_rubric_activity" />
				</default-active>

				<default-visible>
					<xsl:value-of select="$catalog_rubric_visible" />
				</default-visible>

				<basetype module="catalog" method="category">Разделы каталога</basetype>

				<name><xsl:value-of select="Наименование" /></name>

				<xsl:if test="string-length($catalog_rubric_template)">
					<default-template>
						<xsl:value-of select="$catalog_rubric_template" />
					</default-template>
				</xsl:if>

				<properties>
					<group name="common">
						<title>Основные параметры</title>

						<property name="title" type="string">
							<title>i18n::field-title</title>
							<value><xsl:value-of select="Наименование" /></value>
						</property>

						<property name="h1" type="string">
							<title>i18n::field-h1</title>
							<value><xsl:value-of select="Наименование" /></value>
						</property>
					</group>
				</properties>
			</page>

			<!-- Разделы каталога -->
			<xsl:apply-templates select="/КоммерческаяИнформация/Классификатор/Группы" mode="groups" />

			<!-- Объекты каталога -->
			<xsl:apply-templates select="Товары/Товар" />
		</pages>
	</xsl:template>

	<xsl:template match="Группы/Группа" mode="groups">
		<page id="{Ид}" parentId="{$catalog-id}" type-id="root-catalog-category-type">
			<xsl:if test="name(../../.) = 'Группа'">
				<xsl:attribute name="parentId"><xsl:value-of select="../../Ид" /></xsl:attribute>
			</xsl:if>

			<default-active>
				<xsl:value-of select="$catalog_rubric_activity" />
			</default-active>

			<default-visible>
				<xsl:value-of select="$catalog_rubric_visible" />
			</default-visible>

			<basetype module="catalog" method="category">Разделы каталога</basetype>

			<name><xsl:value-of select="Наименование" /></name>

			<xsl:if test="string-length($catalog_rubric_template)">
				<default-template>
					<xsl:value-of select="$catalog_rubric_template" />
				</default-template>
			</xsl:if>

			<properties>
				<group name="common">
					<title>Основные параметры</title>

					<property name="title" type="string">
						<title>i18n::field-title</title>
						<default-value><xsl:value-of select="Наименование" /></default-value>
					</property>

					<property name="h1" type="string">
						<title>i18n::field-h1</title>
						<default-value><xsl:value-of select="Наименование" /></default-value>
					</property>
				</group>
			</properties>
		</page>

		<xsl:apply-templates select="Группы" mode="groups" />
	</xsl:template>

	<xsl:template match="Товары/Товар">
		<xsl:param name="group_id" select="string(Группы/Ид)" />

		<xsl:param name="name">
			<xsl:choose>
				<xsl:when test="string-length(ПолноеНаименование)">
					<xsl:value-of select="ПолноеНаименование" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="Наименование" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:param>

		<page id="{Ид}" parentId="{$group_id}" type-id="{$group_id}">
			<xsl:if test="Статус = 'Удален'">
				<xsl:attribute name="is-deleted">1</xsl:attribute>
			</xsl:if>

			<xsl:if test="not(Группы/Ид)">
				<xsl:attribute name="parentId"><xsl:value-of select="$catalog-id" /></xsl:attribute>
				<xsl:attribute name="type-id">root-catalog-object-type</xsl:attribute>
			</xsl:if>

			<default-active>
				<xsl:value-of select="$catalog_item_activity" />
			</default-active>

			<default-visible>
				<xsl:value-of select="$catalog_item_visible" />
			</default-visible>

			<basetype module="catalog" method="object">Объекты каталога</basetype>

			<name><xsl:value-of select="$name" /></name>

			<xsl:if test="string-length($catalog_item_template)">
				<default-template>
					<xsl:value-of select="$catalog_item_template" />
				</default-template>
			</xsl:if>

			<properties>
				<group name="common">
					<title>Основные параметры</title>

					<property name="title" type="string">
						<title>i18n::field-title</title>
						<default-value><xsl:value-of select="$name" /></default-value>
					</property>

					<property name="h1" type="string">
						<title>i18n::field-h1</title>
						<default-value><xsl:value-of select="$name" /></default-value>
					</property>
				</group>

				<group name="product">
					<title>1C: Общие свойства</title>

					<xsl:if test="string-length(Описание)">
						<property name="description" title="Описание" type="wysiwyg" allow-runtime-add="1">
							<type data-type="wysiwyg" />
							<title>Описание</title>
							<value>
								<xsl:choose>
									<xsl:when test="Описание/@ФорматHTML = 'true'">
										<xsl:value-of select="Описание"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="php:function('nl2br', string(Описание))" disable-output-escaping="yes" />
									</xsl:otherwise>
								</xsl:choose>
							</value>
						</property>
					</xsl:if>

					<property name="1c_catalog_id" type="string">
						<title>Идентификатор каталога 1С</title>
						<value><xsl:value-of select="$catalog-id" /></value>
					</property>

					<property name="1c_product_id" type="string">
						<title>Идентификатор в 1С</title>
						<value><xsl:value-of select="Ид" /></value>
					</property>

					<property name="artikul" type="string">
						<title>Артикул</title>
						<value><xsl:value-of select="Артикул" /></value>
					</property>

					<property name="bar_code" type="string">
						<title>Штрих-код</title>
						<value><xsl:value-of select="Штрихкод" /></value>
					</property>

					<property name="weight" type="float">
						<title>Вес</title>
						<value><xsl:value-of select="ЗначенияРеквизитов/ЗначениеРеквизита[Наименование = 'Вес']/Значение"/></value>
					</property>

					<xsl:apply-templates select="Картинка" />

					<xsl:apply-templates select="СтандартнаяСкидка/Значение" mode="discount_checkbox" />

					<xsl:if test="ДатаС and not(ДатаС = '')">
						<property name="publish_date" type="date">
							<title>Дата мероприятия</title>
							<value><xsl:value-of select="php:function('strtotime', string(ДатаС))" /></value>
						</property>
					</xsl:if>
					<xsl:if test="ДатаПо and not(ДатаПо = '')">
						<property name="finish_date" type="date">
							<title>Дата окончание мероприятия</title>
							<value><xsl:value-of select="php:function('strtotime', string(ДатаПо))" /></value>
						</property>
					</xsl:if>

					<xsl:choose>
						<xsl:when test="ЛекторыСписком/Значение">
							<property name="speaker" type="relation">
								<title>Спикер</title>
								<value>
									<xsl:for-each select="ЛекторыСписком/Значение">
										<item name="{text()}" />
									</xsl:for-each>
								</value>
							</property>
						</xsl:when>
						<xsl:when test="Лектор and not(Лектор = '')">
							<property name="speaker" type="relation">
								<title>Спикер</title>
								<value><item name="{Лектор}" /></value>
							</property>
						</xsl:when>
						<xsl:otherwise></xsl:otherwise>
					</xsl:choose>

					<xsl:if test="ТипМероприятия and not(ТипМероприятия = '')">
						<property name="event_type" type="relation">
							<title>Тип</title>
							<value><item name="{ТипМероприятия}" /></value>
						</property>
					</xsl:if>
					<xsl:if test="УровеньСеминара and not(УровеньСеминара = '')">
						<property name="level" type="relation">
							<title>Уровень</title>
							<value><item name="{УровеньСеминара}" /></value>
						</property>
					</xsl:if>
					<xsl:if test="ТематикаСеминара and not(ТематикаСеминара = '')">
						<property name="organizer" type="relation">
							<title>Тематика</title>
							<value><item name="{ТематикаСеминара}" /></value>
						</property>
					</xsl:if>

					<xsl:variable name="gorod_in_str" select="php:function('trim', string(МестоПроведения))" />
					<xsl:variable name="gorod_in" >
						<xsl:choose>
							<xsl:when test="$gorod_in_str = 'Москва г'"><xsl:text>Москва</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Санкт-Петербург г'"><xsl:text>Санкт-Петербург</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Новосибирск г'"><xsl:text>Новосибирск</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Ростов-на-Дону г'"><xsl:text>Ростов-на-Дону</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Краснодар г'"><xsl:text>Краснодар</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Архангельск г'"><xsl:text>Архангельск</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Казань г'"><xsl:text>Казань</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Самара г'"><xsl:text>Самара</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Хабаровск г'"><xsl:text>Хабаровск</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Красноярск г'"><xsl:text>Красноярск</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Минск г'"><xsl:text>Минск</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Воронеж г'"><xsl:text>Воронеж</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Саратов г'"><xsl:text>Саратов</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Нижний Новгород г'"><xsl:text>Нижний Новгород</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Волгоград г'"><xsl:text>Волгоград</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Кемерово г'"><xsl:text>Кемерово</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Омск г'"><xsl:text>Омск</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Челябинск г'"><xsl:text>Челябинск</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Иркутск г'"><xsl:text>Иркутск</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Якутск г'"><xsl:text>Якутск</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Мурманск г'"><xsl:text>Мурманск</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Уфа г'"><xsl:text>Уфа</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Екатеринбург г'"><xsl:text>Екатеринбург</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Владивосток г'"><xsl:text>Владивосток</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Калининград г'"><xsl:text>Калининград</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Ставрополь г'"><xsl:text>Ставрополь</xsl:text></xsl:when>
							<xsl:when test="$gorod_in_str = 'Тюмень г'"><xsl:text>Тюмень</xsl:text></xsl:when>
							<xsl:otherwise><xsl:value-of select="0" /></xsl:otherwise>
						</xsl:choose>
					</xsl:variable>

					<xsl:if test="not($gorod_in = 0)">
						<property name="gorod_in" type="relation">
							<title>Город</title>
							<value><item name="{$gorod_in}"></item></value>
						</property>
					</xsl:if>
					<xsl:if test="АдресПроведения and not(АдресПроведения = '')">
						<property name="mesto_provedeniya" type="string">
							<title>Место проведения</title>
							<value><xsl:value-of select="АдресПроведения" /></value>
						</property>
					</xsl:if>

					<xsl:if test="string-length(ОписаниеСеминара)">
						<property name="description_1c" title="Описание из 1С" type="wysiwyg">
							<type data-type="wysiwyg" />
							<title>Описание из 1С</title>
							<value>
								<xsl:value-of select="php:function('nl2br', string(ОписаниеСеминара))" disable-output-escaping="yes" />
							</value>
						</property>
					</xsl:if>

					<xsl:if test="string-length(СтоимостьОт)">
						<property name="price_string" title="Стоимость участия" type="string">
							<type data-type="wysiwyg" />
							<title>Стоимость участия</title>
							<value><xsl:value-of select="СтоимостьОт" /></value>
						</property>
					</xsl:if>

					<property name="tematika_konsultacii" type="relation">
						<title>Тематика консультации</title>
						<value>
							<xsl:for-each select="ТематикаКонсультации/Значение">
								<item name="{text()}" />
							</xsl:for-each>
						</value>
					</property>

					<xsl:if test="string-length(СправкаДляРегистрации)">
						<property name="about_reg_1c" title="Общий текст из 1С" type="wysiwyg">
							<type data-type="wysiwyg" />
							<title>Общий текст из 1С</title>
							<value>
								<xsl:value-of select="php:function('nl2br', string(СправкаДляРегистрации))" disable-output-escaping="yes" />
							</value>
						</property>
					</xsl:if>

					<xsl:if test="ПочтаМенеджера and not(ПочтаМенеджера = '')">
						<property name="manager_email" type="string">
							<title>Email менеджера Ormco</title>
							<value><xsl:value-of select="ПочтаМенеджера" /></value>
						</property>
					</xsl:if>

					<xsl:if test="КоличествоМестСеминар and not(КоличествоМестСеминар = '')">
						<property name="max_reserv" type="int">
							<title>Кол-во мест</title>
							<value><xsl:value-of select="КоличествоМестСеминар" /></value>
						</property>
					</xsl:if>


					<xsl:if test="НакоплениеБаллов=1">
						<property name="add_loyalty" type="boolean">
							<title>Получение баллов лояльности при покупке мероприятия</title>
							<value>1</value>
						</property>
					</xsl:if>
				</group>

				<xsl:apply-templates select="ЗначенияСвойств" />
			</properties>
		</page>
	</xsl:template>

	<xsl:template match="Значение" mode="discount_checkbox">
		<xsl:variable name="discount_value" select="text()" />
		<xsl:variable name="discount_str" select="php:function('trim', string($discount_value))" />
		<xsl:choose>
			<xsl:when test="$discount_str = 'Скидка для ординаторов (30%)'">
				<property name="discount_ordinator_30" type="boolean">
					<title>Скидка для ординаторов (30%)</title>
					<value>1</value>
				</property>
			</xsl:when>
			<xsl:when test="$discount_str = 'Скидка для ординаторов (40%)'">
				<property name="discount_ordinator" type="boolean">
					<title>Скидка для ординаторов (40%)</title>
					<value>1</value>
				</property>
			</xsl:when>
			<xsl:when test="$discount_str = 'Скидка врачам 1-2 года (30%)'">
				<property name="discount_doctor" type="boolean">
					<title>Скидка врачам 1-2 года (30%)</title>
					<value>1</value>
				</property>
			</xsl:when>
			<xsl:when test="$discount_str = 'Скидка преподавателям кафедры (50%)'">
				<property name="discount_teacher" type="boolean">
					<title>Скидка преподавателям кафедры (50%)</title>
					<value>1</value>
				</property>
			</xsl:when>
			<xsl:when test="$discount_str = 'Скидка для членов ПОО (15%)'">
				<property name="discount_poo" type="boolean">
					<title>Скидка для членов ПОО (15%)</title>
					<value>1</value>
				</property>
			</xsl:when>
			<xsl:when test="$discount_str = 'Скидка для ШОО (50%)'">
				<property name="discount_shoo" type="boolean">
					<title>Скидка для ШОО (50%)</title>
					<value>1</value>
				</property>
			</xsl:when>
			<xsl:when test="$discount_str = 'Скидка Ormco Stars (50%)'">
				<property name="discount_loyalty" type="boolean">
					<title>Скидка Ormco Stars (50%)</title>
					<value>1</value>
				</property>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="Товар/ЗначенияСвойств">
		<group name="special" title="1C: Специфические свойства" visible="visible">
			<xsl:apply-templates select="ЗначенияСвойства"/>
		</group>
	</xsl:template>

	<xsl:template match="ЗначенияСвойств/ЗначенияСвойства">
		<xsl:param name="property" select="key('property', Ид)" />

		<xsl:param name="value-id">
			<xsl:choose>
				<xsl:when test="ИдЗначения"><xsl:value-of  select="string(ИдЗначения)"/></xsl:when>
				<xsl:when test="Значение"><xsl:value-of  select="string(Значение)"/></xsl:when>
				<xsl:otherwise>string</xsl:otherwise>
			</xsl:choose>
		</xsl:param>

		<xsl:param name="property_name">
			<xsl:choose>
				<xsl:when test="$property/Имя"><xsl:value-of select="$property/Имя"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="$property/Наименование"/></xsl:otherwise>
			</xsl:choose>
		</xsl:param>

		<xsl:param name="data-type">
			<xsl:choose>
				<xsl:when test="$property/ТипыЗначений/ТипЗначений/Тип = 'Число'">float</xsl:when>
				<xsl:when test="$property/ТипыЗначений/ТипЗначений/Тип = 'Булево'">boolean</xsl:when>
				<xsl:when test="$property/ТипыЗначений/ТипЗначений/Тип = 'Дата'">date</xsl:when>
				<xsl:when test="$property/ТипыЗначений/ТипЗначений/Тип = 'Справочник'">relation</xsl:when>
				<xsl:when test="$property/ТипЗначений = 'Справочник'">relation</xsl:when>
				<xsl:otherwise>string</xsl:otherwise>
			</xsl:choose>
		</xsl:param>

		<xsl:if test="$property">
			<property name="{$property_name}" title="{$property/Наименование}" type="{$data-type}" is-public="1" visible="visible" allow-runtime-add="1">
				<type data-type="{$data-type}" />
				<title><xsl:value-of select="$property/Наименование"/></title>

				<value>
					<xsl:choose>
						<xsl:when test="$data-type = 'relation'">
							<xsl:choose>
								<xsl:when test="$property/ТипыЗначений/ТипЗначений/ВариантыЗначений/ВариантЗначения[Ид = $value-id]">
									<xsl:apply-templates select="$property/ТипыЗначений/ТипЗначений/ВариантыЗначений/ВариантЗначения[Ид = $value-id]" mode="relation-value" />
								</xsl:when>
								<xsl:otherwise>
									 <xsl:apply-templates select="$property/ВариантыЗначений/Справочник[ИдЗначения = $value-id]" mode="relation-value" />
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise><xsl:value-of select="Значение" /></xsl:otherwise>
					</xsl:choose>
				</value>
			</property>
		</xsl:if>
	</xsl:template>

	<xsl:template match="ВариантыЗначений/ВариантЗначения" mode="relation-value">
		<item name="{Значение}" />
	</xsl:template>

	<xsl:template match="ВариантыЗначений/Справочник" mode="relation-value">
		<item name="{Значение}" />
	</xsl:template>

	<xsl:template match="ЗначенияСвойства/Значение">
		<value><xsl:value-of select="."/></value>
	</xsl:template>

	<xsl:template match="Товар/Картинка">
		<xsl:if test="string-length(.)">
			<property name="photo" type="img_file">
				<title>Картинка</title>
				<value>./images/cms/data/<xsl:value-of select="."/></value>
			</property>
		</xsl:if>
	</xsl:template>

	<xsl:include href="custom/commerceML2.xsl" />
</xsl:stylesheet>