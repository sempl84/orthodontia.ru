<?xml version="1.0" encoding="UTF-8"?>
<!--
TODO: // Write here your own templates
-->
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:php="http://php.net/xsl"
                xmlns:udt="http://umi-cms.ru/2007/UData/templates"
                extension-element-prefixes="php"
                exclude-result-prefixes="xsl php udt">

    <xsl:template match="object" mode="customer">
        <xsl:param name="full_object" />

        <xsl:variable name="ur_lico_object" select="document(concat('uobject://',$full_object//property[@name='legal_person']/value/item/@id))/udata"/>
        <xsl:variable name="full_name">
            <xsl:choose>
                <xsl:when test="$ur_lico_object//property[@name='name']/value">
                    <xsl:value-of select="$ur_lico_object//property[@name='name']/value" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select=".//property[@name='lname']/value"/><xsl:text> </xsl:text><xsl:value-of select=".//property[@name='fname']/value"/><xsl:text> </xsl:text><xsl:value-of select=".//property[@name='father_name']/value"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <Контрагент>
            <Ид>
                <xsl:value-of select="@id" />
            </Ид>
            <ОбщийИд><xsl:value-of select=".//property[@name='common_uid']/value" /></ОбщийИд>
            <xsl:if test="$full_name">
                <Наименование>
                    <xsl:value-of select="$full_name"/>
                </Наименование>
            </xsl:if>
            <xsl:if test="$full_name">
                <НаименованиеПолное>
                    <xsl:value-of select="$full_name"/>
                </НаименованиеПолное>
            </xsl:if>
            <Роль>Покупатель</Роль>
            <xsl:if test=".//property[@name='e-mail']/value">
                <Email>
                    <xsl:value-of select=".//property[@name='e-mail']/value"/>
                </Email>
            </xsl:if>
            <xsl:if test=".//property[@name='email']/value">
                <Email>
                    <xsl:value-of select=".//property[@name='email']/value"/>
                </Email>
            </xsl:if>
            <xsl:choose>
                <!-- TODO take ur data from $full_object params -->
                <xsl:when test="$ur_lico_object//property[@name='inn']/value">
                    <ЮрФизЛицо>Юр. лицо</ЮрФизЛицо>
                    <xsl:if test="$ur_lico_object//property[@name='inn']/value">
                        <ИНН>
                            <xsl:value-of select="$ur_lico_object//property[@name='inn']/value"/>
                        </ИНН>
                    </xsl:if>
                    <xsl:if test="$ur_lico_object//property[@name='account']/value">
                        <РасчетныйСчет>
                            <xsl:value-of select="$ur_lico_object//property[@name='account']/value"/>
                        </РасчетныйСчет>
                    </xsl:if>
                    <xsl:if test="$ur_lico_object//property[@name='bik']/value">
                        <БИК>
                            <xsl:value-of select="$ur_lico_object//property[@name='bik']/value"/>
                        </БИК>
                    </xsl:if>

                    <Orthodontia>
                        <КонтактноеЛицоUID>
                            <xsl:value-of select="@id" />
                        </КонтактноеЛицоUID>
                        <КонтактноеЛицоОбщийUID><xsl:value-of select=".//property[@name='common_uid']/value" /></КонтактноеЛицоОбщийUID>
                        <КонтрагентUID>
                            <xsl:value-of select="$ur_lico_object//property[@name='inn']/value" />
                        </КонтрагентUID>
                    </Orthodontia>
                </xsl:when>
                <xsl:otherwise>
                    <ЮрФизЛицо>Физ. лицо</ЮрФизЛицо>
                    <Orthodontia>
                        <КонтактноеЛицоUID>
                            <xsl:value-of select="@id" />
                        </КонтактноеЛицоUID>
                        <КонтактноеЛицоОбщийUID><xsl:value-of select=".//property[@name='common_uid']/value" /></КонтактноеЛицоОбщийUID>
                        <КонтрагентUID>
                            <xsl:value-of select="@id" />
                        </КонтрагентUID>
                    </Orthodontia>
                </xsl:otherwise>
            </xsl:choose>

            <xsl:choose>
                <xsl:when test="@type-guid='emarket-customer'">
                    <customer_type>guest</customer_type>
                </xsl:when>
                <xsl:when test="@type-guid='users-user'">
                    <customer_type>user</customer_type>

                </xsl:when>
            </xsl:choose>

            <xsl:choose>
                <!-- Cтудент-->
                <xsl:when test=".//property[@name='prof_status']/value/item/@id = 1773599">
                    <СтатусПользователя>Не врач</СтатусПользователя>
                    <Должность>
                        <xsl:value-of select=".//property[@name='prof_status']/value/item/@name"/>
                    </Должность>
                    <xsl:if test=".//property[@name='vuz']/value/item/@id">
                        <ВУЗ>
                            <ИдВУЗ><xsl:value-of select=".//property[@name='vuz']/value/item/@id" /></ИдВУЗ>
                            <НазваниеВУЗ><xsl:value-of select=".//property[@name='vuz']/value/item/@name" /></НазваниеВУЗ>
                        </ВУЗ>
                    </xsl:if>
                </xsl:when>

                <!--Другая специализация-->
                <xsl:when test=".//property[@name='prof_status']/value/item/@id = 12137">
                    <СтатусПользователя>
                        <xsl:value-of select=".//property[@name='prof_status']/value/item/@name"/>
                    </СтатусПользователя>
                    <Должность>
                        <xsl:value-of select=".//property[@name='other_specialization']/value"/>
                    </Должность>
                </xsl:when>
                <xsl:otherwise>
                    <СтатусПользователя>
                        <xsl:value-of select=".//property[@name='prof_status']/value/item/@name"/>
                    </СтатусПользователя>
                    <Должность>
                        <xsl:value-of select=".//property[@name='prof_status']/value/item/@name"/>
                    </Должность>
                </xsl:otherwise>
            </xsl:choose>

            <xsl:if test=".//property[@name='phone']/value and not($ur_lico_object//property[@name='phone_number']/value)">
                <Телефон>
                    <xsl:value-of select=".//property[@name='phone']/value"/>
                </Телефон>
            </xsl:if>

            <ФамилияКЛ>
                <xsl:choose>
                    <xsl:when test=".//property[@name='lname']/value">
                        <xsl:value-of select=".//property[@name='lname']/value"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                </xsl:choose>
            </ФамилияКЛ>
            <ИмяКЛ>
                <xsl:choose>
                    <xsl:when test=".//property[@name='fname']/value">
                        <xsl:value-of select=".//property[@name='fname']/value"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                </xsl:choose>
            </ИмяКЛ>
            <ОтчествоКЛ>
                <xsl:choose>
                    <xsl:when test=".//property[@name='father_name']/value">
                        <xsl:value-of select=".//property[@name='father_name']/value"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                </xsl:choose>
            </ОтчествоКЛ>
            <ВнеСанкционногоСписка>1</ВнеСанкционногоСписка>
            <Регион>
                <xsl:choose>
                    <xsl:when test=".//property[@name='city']/value">
                        <xsl:value-of select=".//property[@name='city']/value"/>
                    </xsl:when>
                    <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
                </xsl:choose>
            </Регион>
            <xsl:if test=".//property[@name='region']/value/item">
                <РегионUID>
                    <UID><xsl:value-of select=".//property[@name='region']/value/item/@id"/></UID>
                    <Название><xsl:value-of select=".//property[@name='region']/value/item/@name"/></Название>
                </РегионUID>
            </xsl:if>

            <xsl:variable name="phone_src">
                <xsl:choose>
                    <!-- инфа из общей информации из незарег. и зарег. пользователя -->
                    <xsl:when test=".//property[@name='phone']/value and not(.//property[@name='phone']/value = '')">
                        <xsl:value-of select=".//property[@name='phone']/value"/>
                    </xsl:when>
                    <!-- инфа из юридической информации из рег. пользователя -->
                    <xsl:when test="$ur_lico_object//property[@name='phone_number']/value and not($ur_lico_object//property[@name='phone_number']/value = '')">
                        <xsl:value-of select="$ur_lico_object//property[@name='phone_number']/value"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="country" select=".//property[@name='country']/value/item/@id" />

<!--            <xsl:variable name="phone_parts" select="document(concat('udata://users/parsephone/',php:function('urlencode', $phone_src)))/udata" />-->
            <xsl:variable name="phone_parts" select="document(concat('udata://users/try_parse_phone/', $country ,'/', php:function('urlencode', $phone_src) ))/udata" />

            <xsl:choose>
                <xsl:when test="$phone_parts/prefix">
                    <ТелефонКЛ>
                        <xsl:value-of select="$phone_parts/src"/>
                    </ТелефонКЛ>
                    <ТелефонКЛСтрана>
                        <xsl:value-of select="$phone_parts/area"/>
                    </ТелефонКЛСтрана>
                    <ТелефонКЛКодГорода>
                        <xsl:value-of select="$phone_parts/prefix"/>
                    </ТелефонКЛКодГорода>
                    <ТелефонКЛНомер>
                        <xsl:value-of select="$phone_parts/number"/>
                    </ТелефонКЛНомер>
                </xsl:when>
                <xsl:otherwise>
                    <ТелефонКЛ>
                        <xsl:value-of select="$phone_src"/>
                    </ТелефонКЛ>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:if test=".//property[@name='phone_valid']/value = 1">
                <phoneapproved>1</phoneapproved>
            </xsl:if>

            <xsl:if test=".//property[@name = 'user_address_raw']">
                <Адрес>
                    <ПочтовыйИндекс><xsl:value-of select=".//property[@name = 'user_address_postal_code']/value" /></ПочтовыйИндекс>
                    <Страна iso="{.//property[@name = 'user_address_country_iso']/value}"><xsl:value-of select=".//property[@name = 'user_address_country']/value" /></Страна>
                    <Регион><xsl:value-of select=".//property[@name = 'user_address_region']/value" /></Регион>
                    <Район><xsl:value-of select=".//property[@name = 'user_address_area']/value" /></Район>
                    <Город><xsl:value-of select=".//property[@name = 'user_address_city']/value" /></Город>
                    <НаселенныйПункт><xsl:value-of select=".//property[@name = 'user_address_settlement']/value" /></НаселенныйПункт>
                    <Улица><xsl:value-of select=".//property[@name = 'user_address_street']/value" /></Улица>
                    <Дом><xsl:value-of select=".//property[@name = 'user_address_house']/value" /></Дом>
                    <Корпус><xsl:value-of select=".//property[@name = 'user_address_corpus']/value" /></Корпус>
                    <Строение><xsl:value-of select=".//property[@name = 'user_address_building']/value" /></Строение>
                    <Литер><xsl:value-of select=".//property[@name = 'user_address_liter']/value" /></Литер>
                    <Помещение><xsl:value-of select=".//property[@name = 'user_address_room']/value" /></Помещение>
                    <Квартира><xsl:value-of select=".//property[@name = 'user_address_flat']/value" /></Квартира>
                    <Офис><xsl:value-of select=".//property[@name = 'user_address_office']/value" /></Офис>
                    <ИсходныйАдрес><xsl:value-of select=".//property[@name = 'user_address_raw']/value" /></ИсходныйАдрес>
                </Адрес>
            </xsl:if>
        </Контрагент>
    </xsl:template>

    <xsl:template match="item" mode="order-item">
        <xsl:param name="one_click_purchase" />
		<xsl:param name="order_good_id" />
        <xsl:apply-templates select="document(concat('uobject://', @id))/udata/object" mode="order-item" >
            <xsl:with-param name="one_click_purchase" select="$one_click_purchase" />
            <xsl:with-param name="order_good_id" select="$order_good_id" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="object" mode="order-item">
        <xsl:param name="one_click_purchase" />
		<xsl:param name="order_good_id" />
		<xsl:param name="good-id">
            <xsl:choose>
                <xsl:when test="properties/group/property[@name='item_link']/value/page/@id"><xsl:value-of select="properties/group/property[@name='item_link']/value/page/@id" /></xsl:when>
                <xsl:otherwise><xsl:value-of select="$order_good_id" /></xsl:otherwise>
            </xsl:choose>
        </xsl:param>
        <xsl:param name="good" select="document(concat('upage://', $good-id))/udata/page" />
        <xsl:param name="item_price" select="properties/group/property[@name='item_price']/value" />
        <xsl:param name="item_amount" select="properties/group/property[@name='item_amount']/value" />
        <xsl:param name="item_total_price" select="properties/group/property[@name='item_total_price']/value" />

        <xsl:variable name="one_click_purchase_flag">Услуга</xsl:variable>

        <Товар>
            <xsl:choose>
                <xsl:when test="$good//property[@name = '1c_product_id']/value">
                    <Ид>
                        <xsl:value-of select="$good//property[@name = '1c_product_id']/value" />
                    </Ид>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:variable name="rel_1c" select="document(concat('udata://custom/get1cRel/',$good-id))/udata" />
                    <xsl:choose>
                        <xsl:when test="not($rel_1c = 0)">
                            <Ид>
                                <xsl:value-of select="$rel_1c" />
                            </Ид>
                        </xsl:when>
                        <xsl:otherwise>
                            <Ид>
                                <xsl:value-of select="$good-id" />
                            </Ид>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:if test="$good//property[@name = '1c_catalog_id']/value">
                <ИдКаталога>
                    <xsl:value-of select="$good//property[@name = '1c_catalog_id']/value" />
                </ИдКаталога>
            </xsl:if>

            <Наименование>
                <xsl:value-of select="$good/name | @name" />
            </Наименование>
            <БазоваяЕдиница Код="796" НаименованиеПолное="Штука" МеждународноеСокращение="PCE">шт</БазоваяЕдиница>

            <ЦенаЗаЕдиницу>
                <xsl:value-of select="$item_price" />
            </ЦенаЗаЕдиницу>
            <Сумма>
                <xsl:value-of select="$item_total_price" />
            </Сумма>
            <Количество>
                <xsl:value-of select="$item_amount" />
            </Количество>
            <Единица>шт</Единица>
            <Коэффициент>1</Коэффициент>

            <ЗначенияРеквизитов>
                <ЗначениеРеквизита>
                    <Наименование>ВидНоменклатуры</Наименование>
                    <Значение>
                        <xsl:value-of select="$one_click_purchase_flag" />
                    </Значение>
                </ЗначениеРеквизита>
                <ЗначениеРеквизита>
                    <Наименование>ТипНоменклатуры</Наименование>
                    <Значение>
                        <xsl:value-of select="$one_click_purchase_flag" />
                    </Значение>
                </ЗначениеРеквизита>
            </ЗначенияРеквизитов>
        </Товар>
    </xsl:template>


    <xsl:template match="object" mode="order">
        <xsl:param name="order_num" select="properties/group/property[@name='number']/value" />
        <xsl:param name="status_change_date" select="string(properties/group/property[@name='status_change_date']/value/@unix-timestamp)" />
        <xsl:param name="order_date" select="string(properties/group/property[@name='order_date']/value/@unix-timestamp)" />
        <xsl:param name="total_price" select="properties/group/property[@name='total_price']/value" />
        <xsl:param name="customer_id" select="properties/group/property[@name='customer_id']/value/item/@id" />
        <xsl:param name="customer_comments" select="properties/group/property[@name='comments']/value" />

        <xsl:param name="order_status_id" select="number(properties/group/property[@name='status_id']/value/item/@id)" />
        <xsl:param name="order_status" select="document(concat('uobject://', $order_status_id))/udata/object" />
        <xsl:param name="order_status_codename" select="string($order_status//property[@name='codename']/value)" />

        <xsl:param name="payment_date" select="string(properties/group/property[@name='payment_date']/value/@unix-timestamp)" />
        <xsl:param name="payment_document_num" select="string(properties/group/property[@name='payment_document_num']/value)" />
        <xsl:param name="payment_type" select="string(properties/group/property[@name='payment_id']/value)" />
        <xsl:param name="payment_status_id" select="number(properties/group/property[@name='payment_status_id']/value/item/@id)" />
        <xsl:param name="payment_status_codename" select="string(document(concat('uobject://', $payment_status_id))//property[@name='codename']/value)" />
        <xsl:param name="delivery_allow_date" select="string(properties/group/property[@name='delivery_allow_date']/value/@unix-timestamp)" />

        <xsl:param name="full_object" select="document(concat('uobject://', @id))/udata" />
        <xsl:param name="first_item_page" select="document(concat('uobject://', .//properties/group/property[@name='order_items']/value/item[1]/@id))//property[@name='item_link']/value/page/@id" />

        <xsl:param name="first_item_page_1C" select="document(concat('udata://custom/get1cRel/', $first_item_page))/udata" />
        <xsl:param name="payment_type_name" select="string($full_object//property[@name='payment_id']/value/item/@name)" />

		<xsl:variable name="order_good_id" select="//property[@name = 'page_id']/value" />

        <Документ>
            <Ид>
                <xsl:value-of select="@id" />
            </Ид>
            <Номер>
                <xsl:value-of select="document(concat('udata://custom/buhgalt_order_number/', (@id)))/udata" />
            </Номер>

            <Дата>
                <xsl:if test="string-length($order_date)">
                    <xsl:value-of select="php:function('date', 'Y-m-d', $order_date)" />
                </xsl:if>
            </Дата>
            <ХозОперация>Заказ товара</ХозОперация>
            <Роль>Продавец</Роль>
            <Валюта>руб</Валюта>
            <Курс>1</Курс>
            <Сумма>
                <xsl:value-of select="$total_price" />
            </Сумма>
            <xsl:if test="string-length($order_date)">
                <Время>
                    <xsl:value-of select="php:function('date', 'H:i:s', $order_date)" />
                </Время>
            </xsl:if>
            <Комментарий>Заказ №<xsl:value-of select="$order_num" /></Комментарий>
            <xsl:if test="$customer_id">
                <Контрагенты>
                    <xsl:apply-templates select="document(concat('uobject://', $customer_id))/udata/object" mode="customer" >
                        <xsl:with-param name="full_object" select="$full_object" />
                    </xsl:apply-templates>
                </Контрагенты>
            </xsl:if>

            <Товары>
                <xsl:apply-templates select="properties/group/property[@name='order_items']/value/item" mode="order-item" >
                    <xsl:with-param name="one_click_purchase" select="1" />
					<xsl:with-param name="order_good_id" select="$order_good_id" />
                </xsl:apply-templates>
            </Товары>
            <СеминарИд1С>
                <xsl:value-of select="$first_item_page_1C" />
                <xsl:if test="not($first_item_page_1C) and .//properties/group/property[@name='order_items']/value/item[1]/text() = 'Лечение патологии височно-нижнечелюстного сустава, Элизабет Менцель, 26-29.01.17, СПб'">
                    <xsl:text>8e0bddd1-9f7c-11e6-9467-005056b0350d</xsl:text>
                </xsl:if>
            </СеминарИд1С>

            <xsl:apply-templates select="$full_object//group[@name='events_params']/property" mode="order-item-event"/>
            <xsl:apply-templates select="$full_object//group[@name='form_params']/property" mode="order-item-event"/>
            <xsl:apply-templates select="$full_object//group[@name='events_discount']/property" mode="order-item-event"/>

            <ЗначенияРеквизитов>
                <xsl:if test="string-length($payment_date)">
                    <ЗначениеРеквизита>
                        <Наименование>Дата оплаты</Наименование>
                        <Значение>
                            <xsl:value-of select="php:function('date', 'Y-m-d', $payment_date)" />
                        </Значение>
                    </ЗначениеРеквизита>
                </xsl:if>

                <!-- ФормаОплаты -->
                <xsl:if test="string-length($payment_type_name)">
                    <ЗначениеРеквизита>
                        <Наименование>ФормаОплаты</Наименование>
                        <Значение>
                            <xsl:value-of select="$payment_type_name" />
                        </Значение>
                    </ЗначениеРеквизита>
                </xsl:if>

                <xsl:if test="string-length($payment_document_num)">
                    <ЗначениеРеквизита>
                        <Наименование>Номер платежного документа</Наименование>
                        <Значение>
                            <xsl:value-of select="$payment_document_num" />
                        </Значение>
                    </ЗначениеРеквизита>
                </xsl:if>

                <xsl:if test="string-length($payment_type)">
                    <ЗначениеРеквизита>
                        <Наименование>Метод оплаты</Наименование>
                        <Значение>
                            <xsl:value-of select="$payment_type" />
                        </Значение>
                    </ЗначениеРеквизита>
                </xsl:if>

                <xsl:if test="string-length($delivery_allow_date)">
                    <ЗначениеРеквизита>
                        <Наименование>Дата разрешения доставки</Наименование>
                        <Значение>
                            <xsl:value-of select="php:function('date', 'Y-m-d', $delivery_allow_date)" />
                        </Значение>
                    </ЗначениеРеквизита>
                    <ЗначениеРеквизита>
                        <Наименование>Доставка разрешена</Наименование>
                        <Значение>true</Значение>
                    </ЗначениеРеквизита>
                </xsl:if>

                <ЗначениеРеквизита>
                    <Наименование>Заказ оплачен</Наименование>
                    <Значение>
                        <xsl:choose>
                            <xsl:when test="$payment_status_codename = 'accepted'">true</xsl:when>
                            <xsl:otherwise>false</xsl:otherwise>
                        </xsl:choose>
                    </Значение>
                </ЗначениеРеквизита>

                <ЗначениеРеквизита>
                    <Наименование>Отменен</Наименование>
                    <Значение>
                        <xsl:choose>
                            <xsl:when test="$order_status_codename = 'canceled'">true</xsl:when>
                            <xsl:otherwise>false</xsl:otherwise>
                        </xsl:choose>
                    </Значение>
                </ЗначениеРеквизита>

                <ЗначениеРеквизита>
                    <Наименование>Финальный статус</Наименование>
                    <Значение>
                        <xsl:choose>
                            <xsl:when test="$order_status_codename = 'ready'">true</xsl:when>
                            <xsl:otherwise>false</xsl:otherwise>
                        </xsl:choose>
                    </Значение>
                </ЗначениеРеквизита>

                <ЗначениеРеквизита>
                    <Наименование>Статус заказа</Наименование>
                    <Значение>
                        <xsl:value-of select="$order_status/@name" />
                    </Значение>
                </ЗначениеРеквизита>

                <xsl:if test="string-length($status_change_date)">
                    <ЗначениеРеквизита>
                        <Наименование>Дата изменения статуса</Наименование>
                        <Значение>
                            <xsl:value-of select="php:function('date', 'Y-m-d H:i', $status_change_date)" />
                        </Значение>
                    </ЗначениеРеквизита>
                </xsl:if>

                <xsl:if test="$full_object//property[@name = 'kommentarij']/value">
                    <ЗначениеРеквизита>
                        <Наименование>Комментарий для консультации</Наименование>
                        <Значение>
                            <xsl:value-of select="$full_object//property[@name = 'kommentarij']/value" disable-output-escaping="yes" />
                        </Значение>
                    </ЗначениеРеквизита>
                </xsl:if>
            </ЗначенияРеквизитов>

            <!-- Ormco stars -->
            <xsl:if test="$full_object//property[@name='potential_ormco_star']/value &gt; 0">
                <ПрограммаЛояльности>
                    <ПотенциальныеБаллы>
                        <xsl:value-of select="$full_object//property[@name='potential_ormco_star']/value" />
                    </ПотенциальныеБаллы>
                </ПрограммаЛояльности>
            </xsl:if>
            <xsl:if test="$full_object//property[@name='freez_ormco_star']/value &gt; 0">
                <ПрограммаЛояльности>
                    <СкидкаЗаБаллы>
                        <ЗамороженныеБаллы>
                            <xsl:value-of select="$full_object//property[@name='freez_ormco_star']/value" />
                        </ЗамороженныеБаллы>
                        <НазваниеСкидки>
                            <xsl:value-of select="$full_object//property[@name='event_discount_name']/value" />
                        </НазваниеСкидки>
                        <СтоимостьМероприятияСУчетомСкидки>
                            <xsl:value-of select="$full_object//property[@name='event_discount_value']/value" />
                        </СтоимостьМероприятияСУчетомСкидки>
                    </СкидкаЗаБаллы>
                </ПрограммаЛояльности>
            </xsl:if>
        </Документ>

        <xsl:variable name="addLog" select="document(concat('udata://exchange/addOrdersCommerceMlLog/', @id, '/prepared-new/'))/udata" />
    </xsl:template>

    <xsl:template match="property" mode="order-item-event">
        <xsl:element name="{@name}">
            <xsl:value-of select="value"/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="property[@type='relation']" mode="order-item-event">
        <xsl:element name="{@name}">
            <xsl:value-of select="value/item/@name"/>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>