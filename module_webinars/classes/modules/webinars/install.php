<?php

$INFO = Array();

$INFO['name'] = "webinars";
$INFO['filename'] = "modules/webinars/class.php";
$INFO['config'] = "1";
$INFO['default_method'] = "listElements";
$INFO['default_method_admin'] = "lists";
$INFO['is_indexed'] = "1";
$INFO['per_page'] = "10";

$INFO['func_perms'] = "";

$COMPONENTS = array();

$COMPONENTS[0] = "./classes/modules/webinars/__admin.php";
$COMPONENTS[1] = "./classes/modules/webinars/class.php";
$COMPONENTS[2] = "./classes/modules/webinars/i18n.php";
$COMPONENTS[3] = "./classes/modules/webinars/lang.php";
$COMPONENTS[4] = "./classes/modules/webinars/permissions.php";
?>