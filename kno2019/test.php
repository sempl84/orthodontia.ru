<?php
session_start();
header("HTTP/1.1 200 OK");
header('Content-Type: text/html; charset=UTF-8');
?>
<!DOCTYPE html>
<html>
	<head>
		<title>ТЕСТ СПИСЫВАНИЯ ПО ОДНОМУ</title>
		<meta http–equiv="Content–Type" content="text/html; charset=UTF–8">
		<meta name="keywords" content="">
		<meta name="description" content="">
		<meta name="HandheldFriendly" content="true">
		<meta name="MobileOptimized" content="width">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="/favicon.gif" type="image/x-icon">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script>
			$(document).ready(function() {
				countdown();
			});
			function countdown() {
			$.ajax({
				type: "POST",
				url: "testing.php",
				timeout: 5000,
				data: "some=" + 1,
				success: function(data) {
					$("#data").html(data);
				},
				error: function(data) {
				}
			});
			}
		</script>
	</head>
	<body>
	Должно уменьшаться на 1 каждую перезагрузку:
	<div id="data"></div>
	</body>
</html>
