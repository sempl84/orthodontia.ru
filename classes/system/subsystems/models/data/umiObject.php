<?php
class umiObject extends umiEntinty implements iUmiEntinty, iUmiObject {
    private $name;
    private $type_id;
    private $is_locked;
    private $owner_id = false;
    private $properties = array();
    private $invertedProperties = array();
    private $type;
    private $prop_groups = array();
    private $guid = null;
    private $type_guid = null;
    private $updateTime = null;
    private $ord = null;
    protected $store_type = "object";

    public function getName($v550dc225cad913e4f765ae3862e42a3e = false) {
        $this->name = umiObjectProperty::filterOutputString($this->name);
        return $v550dc225cad913e4f765ae3862e42a3e ? $this->name : $this->translateLabel($this->name);
    }

    public function getTypeId() {
        return $this->type_id;
    }

    public function getTypeGUID() {
        return $this->type_guid;
    }

    public function getType() {
        if (!$this->type) {
            $v599dcce2998a6b40b1e38e8c6006cb0a = umiObjectTypesCollection::getInstance()->getType($this->type_id);
            if (!$v599dcce2998a6b40b1e38e8c6006cb0a) {
                throw new coreException("Can't load type in object's init");
            }
            $this->type = $v599dcce2998a6b40b1e38e8c6006cb0a;
        }
        return $this->type;
    }

    public function getIsLocked() {
        return $this->is_locked;
    }

    public function setName($vb068931cc450442b63f5b3d276ea4297) {
        $vb068931cc450442b63f5b3d276ea4297 = preg_replace('/([\x01-\x08]|[\x0B-\x0C]|[\x0E-\x1F])/', '', $vb068931cc450442b63f5b3d276ea4297);
        $vb068931cc450442b63f5b3d276ea4297 = $this->translateI18n($vb068931cc450442b63f5b3d276ea4297, "object-");
        if ($this->name != $vb068931cc450442b63f5b3d276ea4297) {
            $this->name = $vb068931cc450442b63f5b3d276ea4297;
            $this->setIsUpdated();
        }
    }

    public function setUpdateTime($v5fa40b2485d1096a170d2d7ecd0f7030) {
        $v9a5e73d67afbd1b69ff848efe5e73442 = (int) $v5fa40b2485d1096a170d2d7ecd0f7030;
        if ($this->updateTime !== $v9a5e73d67afbd1b69ff848efe5e73442) {
            $this->updateTime = $v9a5e73d67afbd1b69ff848efe5e73442;
            $this->setIsUpdated(true, false);
        }
        return true;
    }

    public function getUpdateTime() {
        return $this->updateTime;
    }

    public function getOrder() {
        return $this->ord;
    }

    public function setOrder($v70a17ffa722a3985b86d30b034ad06d7) {
        $v70a17ffa722a3985b86d30b034ad06d7 = (int) $v70a17ffa722a3985b86d30b034ad06d7;
        if ($this->ord !== $v70a17ffa722a3985b86d30b034ad06d7) {
            $this->ord = $v70a17ffa722a3985b86d30b034ad06d7;
            $this->setIsUpdated(true, false);
        }
        return true;
    }

    public function setTypeId($v94757cae63fd3e398c0811a976dd6bbe) {
        if ($this->type_id !== $v94757cae63fd3e398c0811a976dd6bbe) {
            $this->type_id = $v94757cae63fd3e398c0811a976dd6bbe;
            $this->setIsUpdated();
        }
        return true;
    }

    public function setIsLocked($v1945c9a2a5e2ba6133f1db6757a35fcb) {
        if ($this->is_locked !== ((bool) $v1945c9a2a5e2ba6133f1db6757a35fcb)) {
            $this->is_locked = (bool) $v1945c9a2a5e2ba6133f1db6757a35fcb;
            $this->setIsUpdated();
        }
    }

    public function setOwnerId($vb0ab4f7791b60b1e8ea01057b77873b0) {
        if (!is_null($vb0ab4f7791b60b1e8ea01057b77873b0) and umiObjectsCollection::getInstance()->isExists($vb0ab4f7791b60b1e8ea01057b77873b0)) {
            if ($this->owner_id !== $vb0ab4f7791b60b1e8ea01057b77873b0) {
                $this->owner_id = $vb0ab4f7791b60b1e8ea01057b77873b0;
                $this->setIsUpdated();
            }
            return true;
        } else {
            if (!is_null($this->owner_id)) {
                $this->owner_id = NULL;
                $this->setIsUpdated();
            }
            return false;
        }
    }

    public function getOwnerId() {
        return $this->owner_id;
    }

    public function isFilled() {
        $vd05b6ed7d2345020440df396d6da7f73 = $this->getType()->getAllFields();
        foreach ($vd05b6ed7d2345020440df396d6da7f73 as $v06e3d36fa30cea095545139854ad1fb9){
            if ($v06e3d36fa30cea095545139854ad1fb9->getIsRequired() && is_null($this->getValue($v06e3d36fa30cea095545139854ad1fb9->getName()))){
                return false;
            }
        }
        return true;
    }

    protected function save() {
        if (!$this->is_updated) {
            return true;
        }
        $vb068931cc450442b63f5b3d276ea4297 = umiObjectProperty::filterInputString($this->name);
        $v1e0ca5b1252f1f6b1e0ac91be7e7219e = umiObjectProperty::filterInputString($this->guid);
        $v94757cae63fd3e398c0811a976dd6bbe = (int) $this->type_id;
        $v1945c9a2a5e2ba6133f1db6757a35fcb = (int) $this->is_locked;
        $v5e7b19364b8de2dedd3aa48cf62706e3 = (int) $this->owner_id;
        $v4717d53ebfdfea8477f780ec66151dcb = ConnectionPool::getInstance()->getConnection();
        $vac5c74b64b4b8352ef2f181affb5ac2a = "START TRANSACTION /* Updating object #{$this->id} info */";
        $v4717d53ebfdfea8477f780ec66151dcb->query($vac5c74b64b4b8352ef2f181affb5ac2a);
        if ($v4717d53ebfdfea8477f780ec66151dcb->errorOccurred()) {
            throw new coreException($v4717d53ebfdfea8477f780ec66151dcb->errorDescription($vac5c74b64b4b8352ef2f181affb5ac2a));
        }
        $vc200d1cdcad0901ed1f5100f96a16c1e = $vb068931cc450442b63f5b3d276ea4297 ? "'{$vb068931cc450442b63f5b3d276ea4297}'" : "NULL";
        $v86171363b6d953b245819c04ee28a2de = is_null($this->getUpdateTime()) ? 'NULL' : $this->getUpdateTime();
        $v8bef1cc20ada3bef55fdf132cb2a1cb9 = (int) $this->ord;
        $vac5c74b64b4b8352ef2f181affb5ac2a = <<<QUERY
UPDATE cms3_objects
SET    NAME = ${vc200d1cdcad0901ed1f5100f96a16c1e},
   type_id = '${v94757cae63fd3e398c0811a976dd6bbe}',
   is_locked = '${v1945c9a2a5e2ba6133f1db6757a35fcb}',
   owner_id = '${v5e7b19364b8de2dedd3aa48cf62706e3}',
   guid = '${v1e0ca5b1252f1f6b1e0ac91be7e7219e}',
   updatetime = '${v86171363b6d953b245819c04ee28a2de}',
   ord = '${v8bef1cc20ada3bef55fdf132cb2a1cb9}'
WHERE  id = '{$this->id}'
QUERY;
        $v4717d53ebfdfea8477f780ec66151dcb->query($vac5c74b64b4b8352ef2f181affb5ac2a);
        if ($v4717d53ebfdfea8477f780ec66151dcb->errorOccurred()) {
            throw new coreException($v4717d53ebfdfea8477f780ec66151dcb->errorDescription());
        }
        foreach ($this->properties as $v23a5b8ab834cb5140fa6665622eb6417) {
            if ($v23a5b8ab834cb5140fa6665622eb6417 instanceof umiObjectProperty && $v23a5b8ab834cb5140fa6665622eb6417->getIsUpdated()) {
                $v23a5b8ab834cb5140fa6665622eb6417->commit();
            }
        }
        $vac5c74b64b4b8352ef2f181affb5ac2a = "COMMIT";
        $v4717d53ebfdfea8477f780ec66151dcb->query($vac5c74b64b4b8352ef2f181affb5ac2a);
        if ($v4717d53ebfdfea8477f780ec66151dcb->errorOccurred()) {
            throw new coreException($v4717d53ebfdfea8477f780ec66151dcb->errorDescription($vac5c74b64b4b8352ef2f181affb5ac2a));
        }
        return $this->setIsUpdated(false, false);
    }

    public function __construct($vb80bb7740288fda1f201890375a60c8f, $vf1965a857bc285d26fe22023aa5ab50d = false) {
        parent::__construct($vb80bb7740288fda1f201890375a60c8f, $vf1965a857bc285d26fe22023aa5ab50d);
    }

    protected function loadInfo($vf1965a857bc285d26fe22023aa5ab50d = false) {
        if ($vf1965a857bc285d26fe22023aa5ab50d === false || count($vf1965a857bc285d26fe22023aa5ab50d) < 6) {
            $v4717d53ebfdfea8477f780ec66151dcb = ConnectionPool::getInstance()->getConnection();
            $vac5c74b64b4b8352ef2f181affb5ac2a = <<<QUERY
SELECT o.name,
       o.type_id,
       o.is_locked,
       o.owner_id,
       o.guid AS `guid`,
       t.guid AS `type_guid`,
       o.updatetime,
       o.ord
FROM   cms3_objects `o`,
       cms3_object_types `t`
WHERE  o.id = '{$this->id}'
       AND o.type_id = t.id
QUERY;
            $result = $v4717d53ebfdfea8477f780ec66151dcb->queryResult($vac5c74b64b4b8352ef2f181affb5ac2a, true);
            if ($v4717d53ebfdfea8477f780ec66151dcb->errorOccurred()) {
                cacheFrontend::getInstance()->del($this->id, "object");
                throw new coreException($v4717d53ebfdfea8477f780ec66151dcb->errorDescription($vac5c74b64b4b8352ef2f181affb5ac2a));
            }
            $result->setFetchType(IQueryResult::FETCH_ROW);
            $vf1965a857bc285d26fe22023aa5ab50d = $result->fetch();
            if (!$vf1965a857bc285d26fe22023aa5ab50d) {
                throw new coreException("Object #{$this->id} doesn't exist");
            }
        }
        list($vb068931cc450442b63f5b3d276ea4297, $v94757cae63fd3e398c0811a976dd6bbe, $v1945c9a2a5e2ba6133f1db6757a35fcb, $v5e7b19364b8de2dedd3aa48cf62706e3, $v1e0ca5b1252f1f6b1e0ac91be7e7219e, $vca12b90a66059f75e25460e700ed4c61, $v5fa40b2485d1096a170d2d7ecd0f7030, $v8bef1cc20ada3bef55fdf132cb2a1cb9) = $vf1965a857bc285d26fe22023aa5ab50d;
        if (!$v94757cae63fd3e398c0811a976dd6bbe) {
            umiObjectsCollection::getInstance()->delObject($this->id);
            return false;
        }
        $this->name = $vb068931cc450442b63f5b3d276ea4297;
        $this->type_id = (int) $v94757cae63fd3e398c0811a976dd6bbe;
        $this->is_locked = (bool) $v1945c9a2a5e2ba6133f1db6757a35fcb;
        $this->owner_id = (int) $v5e7b19364b8de2dedd3aa48cf62706e3;
        $this->guid = $v1e0ca5b1252f1f6b1e0ac91be7e7219e;
        $this->type_guid = $vca12b90a66059f75e25460e700ed4c61;
        $this->updateTime = $v5fa40b2485d1096a170d2d7ecd0f7030;
        $this->ord = (int) $v8bef1cc20ada3bef55fdf132cb2a1cb9;
        return $this->loadFields();
    }

    public function loadFields() {
        $vb4c6fdbf208f0b52aa890f557924560b = umiTypesHelper::getInstance();
        $v5f694956811487225d15e973ca38fbab = $this->getTypeId();
        $vd05b6ed7d2345020440df396d6da7f73 = $vb4c6fdbf208f0b52aa890f557924560b->getFieldsByObjectTypeIds($v5f694956811487225d15e973ca38fbab);
        if (isset($vd05b6ed7d2345020440df396d6da7f73[$v5f694956811487225d15e973ca38fbab])) {
            $vd05b6ed7d2345020440df396d6da7f73 = $vd05b6ed7d2345020440df396d6da7f73[$v5f694956811487225d15e973ca38fbab];
            $this->invertedProperties = $vd05b6ed7d2345020440df396d6da7f73;
            $this->properties = array_flip($vd05b6ed7d2345020440df396d6da7f73);
        }
        return true;
    }

    public function getPropByName($vdfc394bd05a4b48161c790034af522a8) {
        $vdfc394bd05a4b48161c790034af522a8 = strtolower($vdfc394bd05a4b48161c790034af522a8);
        if (!$this->isPropertyNameExist($vdfc394bd05a4b48161c790034af522a8)) {
            return null;
        }
        $v758f9a7df94fc455044f05f11c9ee06e = (int) $this->invertedProperties[$vdfc394bd05a4b48161c790034af522a8];
        return $this->getPropById($v758f9a7df94fc455044f05f11c9ee06e);
    }

    public function getPropById($v3aabf39f2d943fa886d86dcbbee4d910) {
        if (!$this->isPropertyExists($v3aabf39f2d943fa886d86dcbbee4d910)) {
            return null;
        }
        if (!$this->properties[$v3aabf39f2d943fa886d86dcbbee4d910] instanceof umiObjectProperty) {
            $vb4c6fdbf208f0b52aa890f557924560b = umiTypesHelper::getInstance();
            $ve2aeb4e882d60b1eb4b7c8cd97986a28 = $vb4c6fdbf208f0b52aa890f557924560b->getFieldTypeIdByFieldId($v3aabf39f2d943fa886d86dcbbee4d910);
            $this->properties[$v3aabf39f2d943fa886d86dcbbee4d910] = umiObjectProperty::getProperty($this->id, $v3aabf39f2d943fa886d86dcbbee4d910, $this->type_id, $ve2aeb4e882d60b1eb4b7c8cd97986a28);
        }
        return $this->properties[$v3aabf39f2d943fa886d86dcbbee4d910];
    }

    public function isPropertyExists($v3aabf39f2d943fa886d86dcbbee4d910) {
        return (bool) isset($this->properties[$v3aabf39f2d943fa886d86dcbbee4d910]);
    }

    public function isPropertyNameExist($v972bf3f05d14ffbdb817bef60638ff00) {
        return (bool) isset($this->invertedProperties[$v972bf3f05d14ffbdb817bef60638ff00]);
    }

    public function isPropGroupExists($vb78a51df08ebf5c0b87001c180e3c2ab) {
        if (count($this->prop_groups) == 0) {
            $this->loadGroups();
        }
        return (bool) isset($this->prop_groups[$vb78a51df08ebf5c0b87001c180e3c2ab]);
    }

    private function loadGroups() {
        $v1471e4e05a4db95d353cc867fe317314 = $this->getType()->getFieldsGroupsList();
        foreach ($v1471e4e05a4db95d353cc867fe317314 as $vdb0f6f37ebeb6ea09489124345af2a45) {
            if (!$vdb0f6f37ebeb6ea09489124345af2a45 instanceof umiFieldsGroup || $vdb0f6f37ebeb6ea09489124345af2a45->getIsActive() == false) {
                continue;
            }
            $vd05b6ed7d2345020440df396d6da7f73 = $vdb0f6f37ebeb6ea09489124345af2a45->getFields();
            $this->prop_groups[$vdb0f6f37ebeb6ea09489124345af2a45->getId()] = Array();
            foreach ($vd05b6ed7d2345020440df396d6da7f73 as $v06e3d36fa30cea095545139854ad1fb9) {
                $this->prop_groups[$vdb0f6f37ebeb6ea09489124345af2a45->getId()][] = $v06e3d36fa30cea095545139854ad1fb9->getId();
            }
        }
    }

    public function getPropGroupId($v630d7459cbc61bfef50b1a0fff2ea42e) {
        $v041f36f9e13e5473c5b995506bad2aaa = $this->getType()->getFieldsGroupsList();
        foreach ($v041f36f9e13e5473c5b995506bad2aaa as $vdb0f6f37ebeb6ea09489124345af2a45) {
            if ($vdb0f6f37ebeb6ea09489124345af2a45->getName() == $v630d7459cbc61bfef50b1a0fff2ea42e) {
                return $vdb0f6f37ebeb6ea09489124345af2a45->getId();
            }
        }
        return false;
    }

    public function getPropGroupByName($v630d7459cbc61bfef50b1a0fff2ea42e) {
        if ($v0e939a4ffd3aacd724dd3b50147b4353 = $this->getPropGroupId($v630d7459cbc61bfef50b1a0fff2ea42e)) {
            return $this->getPropGroupById($v0e939a4ffd3aacd724dd3b50147b4353);
        } else {
            return false;
        }
    }

    public function getPropGroupById($vb78a51df08ebf5c0b87001c180e3c2ab) {
        if ($this->isPropGroupExists($vb78a51df08ebf5c0b87001c180e3c2ab)) {
            return $this->prop_groups[$vb78a51df08ebf5c0b87001c180e3c2ab];
        }
        $v599dcce2998a6b40b1e38e8c6006cb0a = $this->getType();
        $vdb0f6f37ebeb6ea09489124345af2a45 = $v599dcce2998a6b40b1e38e8c6006cb0a->getFieldsGroup($vb78a51df08ebf5c0b87001c180e3c2ab);
        if (!$vdb0f6f37ebeb6ea09489124345af2a45 instanceof umiFieldsGroup) {
            return false;
        }
        $v4bc546b2d8c994d9600d8bd980f28922 = $vdb0f6f37ebeb6ea09489124345af2a45->getFields();
        $v5bf771c31a2488978f269aface6f7a45 = array();
        foreach ($v4bc546b2d8c994d9600d8bd980f28922 as $v06e3d36fa30cea095545139854ad1fb9) {
            if (!$v06e3d36fa30cea095545139854ad1fb9 instanceof umiField) {
                continue;
            }
            $v5bf771c31a2488978f269aface6f7a45[] = $v06e3d36fa30cea095545139854ad1fb9->getId();
        }
        return $this->prop_groups[$vb78a51df08ebf5c0b87001c180e3c2ab] = $v5bf771c31a2488978f269aface6f7a45;
    }

    public function getValue($vdfc394bd05a4b48161c790034af522a8, $v21ffce5b8a6cc8cc6a41448dd69623c9 = NULL) {
        if ($v23a5b8ab834cb5140fa6665622eb6417 = $this->getPropByName($vdfc394bd05a4b48161c790034af522a8)) {
            return $v23a5b8ab834cb5140fa6665622eb6417->getValue($v21ffce5b8a6cc8cc6a41448dd69623c9);
        } else {
            return false;
        }
    }

    public function setValue($vdfc394bd05a4b48161c790034af522a8, $v2771be291c4a714ca95fd1f45a32403e) {
        if ($v23a5b8ab834cb5140fa6665622eb6417 = $this->getPropByName($vdfc394bd05a4b48161c790034af522a8)) {
            $v23a5b8ab834cb5140fa6665622eb6417->setValue($v2771be291c4a714ca95fd1f45a32403e);
            if ($v23a5b8ab834cb5140fa6665622eb6417->getIsUpdated()) {
                $this->setIsUpdated(true);
            }
            return true;
        } else {
            return false;
        }
    }

    public function commit() {
        if (!$this->is_updated) {
            return;
        }

        $this->log_order_change();

        $v4717d53ebfdfea8477f780ec66151dcb = ConnectionPool::getInstance()->getConnection();
        $v4717d53ebfdfea8477f780ec66151dcb->query("START TRANSACTION /* Saving object {$this->id} */");
        $v3c9c288f4021bd9f84c98cc0eddd1838 = umiObjectProperty::$USE_TRANSACTIONS;
        umiObjectProperty::$USE_TRANSACTIONS = false;
        if ($this->checkSelf()) {
            foreach ($this->properties as $v23a5b8ab834cb5140fa6665622eb6417) {
                if ($v23a5b8ab834cb5140fa6665622eb6417 instanceof umiObjectProperty && $v23a5b8ab834cb5140fa6665622eb6417->getIsUpdated()) {
                    $v23a5b8ab834cb5140fa6665622eb6417->commit();
                }
            }
        }
        parent::commit();
        $v4717d53ebfdfea8477f780ec66151dcb->query("COMMIT");
        umiObjectProperty::$USE_TRANSACTIONS = $v3c9c288f4021bd9f84c98cc0eddd1838;
    }

    public function checkSelf() {
        static $v9b207167e5381c47682c6b4f58a623fb;
        if ($v9b207167e5381c47682c6b4f58a623fb !== null) {
            return $v9b207167e5381c47682c6b4f58a623fb;
        }
        if (!cacheFrontend::getInstance()->getIsConnected()) {
            return $v9b207167e5381c47682c6b4f58a623fb = true;
        }
        $v4717d53ebfdfea8477f780ec66151dcb = ConnectionPool::getInstance()->getConnection();
        $vac5c74b64b4b8352ef2f181affb5ac2a = "SELECT id FROM cms3_objects WHERE id = '{$this->id}'";
        $result = $v4717d53ebfdfea8477f780ec66151dcb->queryResult($vac5c74b64b4b8352ef2f181affb5ac2a);
        if ($v4717d53ebfdfea8477f780ec66151dcb->errorOccurred()) {
            throw new coreException($v4717d53ebfdfea8477f780ec66151dcb->errorDescription($vac5c74b64b4b8352ef2f181affb5ac2a));
        }
        $v9b207167e5381c47682c6b4f58a623fb = (bool) $result->length();
        if (!$v9b207167e5381c47682c6b4f58a623fb) {
            cacheFrontend::getInstance()->flush();
        }
        return $v9b207167e5381c47682c6b4f58a623fb;
    }

    public function setIsUpdated($v8de61324edc43f3acb1b73da3c63e89e = true) {
        $args = func_get_args();
        $v8de61324edc43f3acb1b73da3c63e89e = array_shift($args);
        if (is_null($v8de61324edc43f3acb1b73da3c63e89e)) {
            $v8de61324edc43f3acb1b73da3c63e89e = true;
        }
        $vc52a30bf58b4a8565098be6965c341e2 = array_shift($args);
        if (is_null($vc52a30bf58b4a8565098be6965c341e2)) {
            $vc52a30bf58b4a8565098be6965c341e2 = true;
        }
        umiObjectsCollection::getInstance()->addUpdatedObjectId($this->id);
        parent::setIsUpdated($v8de61324edc43f3acb1b73da3c63e89e);
        if ($vc52a30bf58b4a8565098be6965c341e2) {
            $this->setUpdateTime(time());
        }
    }

    public function delete() {
        umiObjectsCollection::getInstance()->delObject($this->id);
    }

    public function __get($v51746fc9cfaaf892e94c2d56d7508b37) {
        switch ($v51746fc9cfaaf892e94c2d56d7508b37) {
            case "id": return $this->id;
            case "name": return $this->getName();
            case "ownerId": return $this->getOwnerId();
            case "typeId": return $this->getTypeId();
            case "GUID": return $this->getGUID();
            case "typeGUID":return $this->getTypeGUID();
            case "xlink": return 'uobject://' . $this->id;
            default: return $this->getValue($v51746fc9cfaaf892e94c2d56d7508b37);
        }
    }

    public function __isset($v23a5b8ab834cb5140fa6665622eb6417) {
        switch ($v23a5b8ab834cb5140fa6665622eb6417) {
            case 'id': case 'name': case 'ownerId': case 'typeId': case 'GUID': case 'typeGUID': case 'xlink': {
                return true;
            }
            default : {
                return ($this->getPropByName($v23a5b8ab834cb5140fa6665622eb6417) instanceof umiObjectProperty);
            }
        }
    }

    public function __set($v51746fc9cfaaf892e94c2d56d7508b37, $v2063c1608d6e0baf80249c42e2be5804) {
        switch ($v51746fc9cfaaf892e94c2d56d7508b37) {
            case "id": throw new coreException("Object id could not be changed");
            case "name": return $this->setName($v2063c1608d6e0baf80249c42e2be5804);
            case "ownerId": return $this->setOwnerId($v2063c1608d6e0baf80249c42e2be5804);
            default: return $this->setValue($v51746fc9cfaaf892e94c2d56d7508b37, $v2063c1608d6e0baf80249c42e2be5804);
        }
    }

    public function beforeSerialize($v324b23d9adc662d7f9e99634ed47ab65 = false) {
        static $vd14a8022b085f9ef19d479cbdd581127 = array();
        if ($v324b23d9adc662d7f9e99634ed47ab65 && isset($vd14a8022b085f9ef19d479cbdd581127[$this->type_id])) {
            $this->type = $vd14a8022b085f9ef19d479cbdd581127[$this->type_id];
        } else {
            $vd14a8022b085f9ef19d479cbdd581127[$this->type_id] = $this->type;
            $this->type = null;
        }
    }

    public function afterSerialize() {
        $this->beforeSerialize(true);
    }

    public function afterUnSerialize() {
        $this->getType();
    }

    public function getModule() {
        $vacf567c9c3d6cf7c6e2cc0ce108e0631 = umiObjectTypesCollection::getInstance()->getHierarchyTypeIdByObjectTypeId($this->getTypeId());
        $v89b0b9deff65f8b9cd1f71bc74ce36ba = umiHierarchyTypesCollection::getInstance()->getType($vacf567c9c3d6cf7c6e2cc0ce108e0631);
        if ($v89b0b9deff65f8b9cd1f71bc74ce36ba instanceof umiHierarchyType) {
            return $v89b0b9deff65f8b9cd1f71bc74ce36ba->getName();
        } else {
            return false;
        }
    }

    public function getMethod() {
        $vacf567c9c3d6cf7c6e2cc0ce108e0631 = umiObjectTypesCollection::getInstance()->getHierarchyTypeIdByObjectTypeId($this->getTypeId());
        $v89b0b9deff65f8b9cd1f71bc74ce36ba = umiHierarchyTypesCollection::getInstance()->getType($vacf567c9c3d6cf7c6e2cc0ce108e0631);
        if ($v89b0b9deff65f8b9cd1f71bc74ce36ba instanceof umiHierarchyType) {
            return $v89b0b9deff65f8b9cd1f71bc74ce36ba->getExt();
        } else {
            return false;
        }
    }

    public function getGUID() {
        return $this->guid;
    }

    public function setGUID($v1e0ca5b1252f1f6b1e0ac91be7e7219e) {
        $vb80bb7740288fda1f201890375a60c8f = umiObjectsCollection::getInstance()->getObjectIdByGUID($v1e0ca5b1252f1f6b1e0ac91be7e7219e);
        if ($vb80bb7740288fda1f201890375a60c8f && $vb80bb7740288fda1f201890375a60c8f != $this->id) {
            throw new coreException("GUID {$v1e0ca5b1252f1f6b1e0ac91be7e7219e} already in use");
        }if ($this->guid != $v1e0ca5b1252f1f6b1e0ac91be7e7219e) {
            $this->guid = $v1e0ca5b1252f1f6b1e0ac91be7e7219e;
            $this->setIsUpdated();
        }
    }

    public function __destruct() {
        parent::__destruct();
        umiObjectProperty::unloadPropData($this->id);
    }

    private function loadType() {
        $v599dcce2998a6b40b1e38e8c6006cb0a = umiObjectTypesCollection::getInstance()->getType($this->type_id);
        if (!$v599dcce2998a6b40b1e38e8c6006cb0a) {
            throw new coreException("Can't load type in object's init");
        }
        $this->type = $v599dcce2998a6b40b1e38e8c6006cb0a;
    }

    private function loadProperties() {
        $v599dcce2998a6b40b1e38e8c6006cb0a = $this->getType();
        $v041f36f9e13e5473c5b995506bad2aaa = $v599dcce2998a6b40b1e38e8c6006cb0a->getFieldsGroupsList();
        foreach ($v041f36f9e13e5473c5b995506bad2aaa as $vdb0f6f37ebeb6ea09489124345af2a45) {
            if ($vdb0f6f37ebeb6ea09489124345af2a45->getIsActive() == false) {
                continue;
            }
            $vd05b6ed7d2345020440df396d6da7f73 = $vdb0f6f37ebeb6ea09489124345af2a45->getFields();
            $this->prop_groups[$vdb0f6f37ebeb6ea09489124345af2a45->getId()] = Array();
            foreach ($vd05b6ed7d2345020440df396d6da7f73 as $v06e3d36fa30cea095545139854ad1fb9) {
                $this->properties[$v06e3d36fa30cea095545139854ad1fb9->getId()] = $v06e3d36fa30cea095545139854ad1fb9->getName();
                $this->prop_groups[$vdb0f6f37ebeb6ea09489124345af2a45->getId()][] = $v06e3d36fa30cea095545139854ad1fb9->getId();
            }
        }
    }


    /* CUSTOM ORDER SAVE LOG */
    private function log_order_change(){
        if($this->getTypeId() != 51){ // 51 - заказ
            return true;
        }
        $old_serialize_object = $this->create_object_snapshot(new umiObject($this->getId()));
        $new_serialize_object = $this->create_object_snapshot($this);
        $res = $this->create_diff_details($old_serialize_object, $new_serialize_object);

		$permissions = permissionsCollection::getInstance();
		$current_user_id = $permissions->getUserId();

        $this->save_orders_change_log($this->getId(), $current_user_id, $res['message'], $res['details']);
    }

    /**
     * Сохранение факта изменения данных пользователя
     * @param type $obj_id - идентификатор объекта пользователя
     * @param type $author - имя автора изменений, строка
     * @param type $details - текстовое описание события
     */
    private function save_orders_change_log($obj_id, $author, $message, $details) {
        $author = mysql_real_escape_string($author);
        $message = mysql_real_escape_string($message);
        $details = serialize($details);
        $connection = ConnectionPool::getInstance()->getConnection();
        $connection->query("INSERT INTO `orders_change_log`(`obj_id`, `author`, `message`, `details`) VALUES ({$obj_id}, '{$author}', '{$message}', '{$details}')");
    }

    /**
     * Превращаем объект в массив свойств
     * @param umiObject $object
     * @return массив со значениями свойств
     */
    private function create_object_snapshot(umiObject $object) {
        $result = array();
        $object_types_collection = umiObjectTypesCollection::getInstance();
        $object_type = $object_types_collection->getType($object->getTypeId());
        $object_fields = $object_type->getAllFields();
        foreach ($object_fields as $field) {
            $row = $this->create_property_snapshot($object->getPropByName($field->getName()));
            if ($row !== false) {
                $result = $result + $row;
            }
        }
        return $result;
    }

    /**
     * Превращаем одно свойство объекта в массив
     * @param umiObjectProperty $property
     * @return boolean false если значение пусто или массив из правильных значений
     */
    private function create_property_snapshot(umiObjectProperty $property) {
        try {
            $value = $property->getValue();
            if (!empty($value)) {
                $field = $property->getField();
                $res = array(
                    "id" => $field->getId(),
                    "type" => $field->getDataType(),
                    "name" => $property->getName(),
                    "title" => $property->getTitle()
                );
                if (is_array($value)) {
                    $res['value'] = $value;
                } else {
                    $res['value'] = mysql_real_escape_string($value);
                }
                return array($field->getId() => $res);
            }
        } catch (Exception $exc) {
        }
        return false;
    }

    /**
     * Получаем разницу между двумя сериализованными объектами и сообщение об этом с деталями
     * @param type $old_value
     * @param type $new_value
     * @return type
     */
    private function create_diff_details($old_value, $new_value){
        $message = array();
        $details = array();
        $diff_props = array();

        // все ключи
        $keys = array_unique(array_merge(array_keys($old_value), array_keys($new_value)));
        foreach($keys as $key){
            // пропускаем пустой пароль
            if(key_exists($key, $old_value) && !key_exists($key, $new_value) && $old_value[$key]['title'] == 'Пароль'){
                continue;
            }

            // Дата 1970-01-01 03:00:00 - пустая, так как это стандарт пустого значения редактора дат. Чистим значение
            if(key_exists($key, $new_value) && $new_value[$key]['type'] == 'date' && $new_value[$key]['value'] == '1970-01-01 03:00:00'){
                unset($new_value[$key]);
            }

            // Файл, не являющийся файлом (директория) - пустой, так как это стандарт пустого значения выбора файла. Чистим значение
            if(key_exists($key, $new_value) && $new_value[$key]['type'] == 'file' && !is_file($_SERVER['DOCUMENT_ROOT'].$new_value[$key]['value'])){
                unset($new_value[$key]);
            }

            if(key_exists($key, $old_value) && !key_exists($key, $new_value)){
                $diff_props[$key] = array(
                    "name" => $old_value[$key]['title'],
                    "old" => $old_value[$key],
                    "new" => false
                );
            }elseif(!key_exists($key, $old_value) && key_exists($key, $new_value)){
                $diff_props[$key] = array(
                    "name" => $new_value[$key]['title'],
                    "old" => false,
                    "new" => $new_value[$key]
                );
            }elseif(key_exists($key, $old_value) && key_exists($key, $new_value)){
                if($old_value[$key]['value'] != $new_value[$key]['value']){
                    $diff_props[$key] = array(
                        "name" => $old_value[$key]['title'],
                        "old" => $old_value[$key],
                        "new" => $new_value[$key]
                    );
                }
            }
        }

        foreach($diff_props as $key => $values){
            $message[] = 'Изменено свойство "' . $values['name'] . '". Старое значение: "' . ($values['old'] === false ? '' : $values['old']['value']) . '", новое значение: "' . ($values['new'] === false ? '' : $values['new']['value']) . '".';
            $details[$key] = $values;
        }

        return array(
            "message" => implode("<br>", $message),
            "details" => $details
        );
    }
}