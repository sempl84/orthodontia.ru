<?php
	/**
	 * Управляет правами доступа на страницы и ресурсы модулей.
	 * Синглтон. Экземпляр класса можно получить через статичесик метод getInstance.
	 */
	class permissionsCollection extends singleton implements iSingleton, iPermissionsCollection {
		protected $methodsPermissions = array(), $user_id = 0, $tempElementPermissions = array();
		protected $elementsCache = array();
		private $readablePages = array();
		private $readablePagesLoadingEnabled = false;
		private $currentModulesPermissions = array();
		private $currentMethodsPermissions = array();
		private $modulesPermissionsLoadingEnabled = false;
		private $umiTypesHelper;
		private $umiPropertiesHelper;

		// Some permissions constants
		const E_READ_ALLOWED   = 0;
		const E_EDIT_ALLOWED   = 1;
		const E_CREATE_ALLOWED = 2;
		const E_DELETE_ALLOWED = 3;
		const E_MOVE_ALLOWED   = 4;

		const E_READ_ALLOWED_BIT   = 1;
		const E_EDIT_ALLOWED_BIT   = 2;
		const E_CREATE_ALLOWED_BIT = 4;
		const E_DELETE_ALLOWED_BIT = 8;
		const E_MOVE_ALLOWED_BIT   = 16;

		/**
		 * Конструктор
		 */
		public function __construct() {
			if (is_null(getRequest('guest-mode')) == false) {
				$this->user_id = self::getGuestId();
			}

			$this->umiTypesHelper = umiTypesHelper::getInstance();
			$this->umiPropertiesHelper = umiPropertiesHelper::getInstance();
			$mainConfigs = mainConfiguration::getInstance();

			$this->readablePagesLoadingEnabled = (bool) $mainConfigs->get('kernel', 'load-readable-pages');

			if ($this->readablePagesLoadingEnabled) {
				$this->loadReadablePages();
			}

			$this->modulesPermissionsLoadingEnabled = (bool) $mainConfigs->get('kernel', 'load-modules-permissions');

			if ($this->modulesPermissionsLoadingEnabled) {
				$this->loadModulesPermissions();
			}
		}

		/**
		 * Получить экземпляр коллекци
		 * @return permissionsCollection экземпляр класса permissionsCollection
		 */
		public static function getInstance($c = NULL) {
			return parent::getInstance(__CLASS__);
		}
		/**
		 * Внутрисистемный метод, не является частью публичного API
		 * @param Integer $owner_id id пользователя или группы
		 * @return Integer|array
		 */
		public function getOwnerType($ownerId) {
			static $cache = array();

			$ownerId = intval($ownerId);

			if (isset($cache[$ownerId])) {
				return $cache[$ownerId];
			}

			$userTypeId = $this->umiTypesHelper->getObjectTypeIdByGuid('users-user');

			$groups = $this->umiPropertiesHelper->getProperty(
				$ownerId,
				'groups',
				$userTypeId
			);

			if ($groups instanceof umiObjectProperty) {
				$cache[$ownerId] = $groups->getValue();
				return $cache[$ownerId];
			} else {
				return $ownerId;
			}
		}

		/**
		 * Включена ли предварительная загрузка идентификаторов страниц,
		 * доступных для чтения текущему пользователю
		 * @return bool
		 */
		public function isReadablePagesLoaded() {
			return (bool) $this->readablePagesLoadingEnabled;
		}

		/**
		 * Возвращает массив идентификаторов страниц,
		 * доступных для чтения текущему пользователю
		 * @return array
		 */
		public function getReadablePagesIds() {
			return $this->readablePages;
		}

		/**
		 * Внутрисистемный метод, не является частью публичного API
		 * @param Integer $owner_id id пользователя или группы
		 * @return String фрагмент SQL-запроса
		 */
		public function makeSqlWhere($owner_id, $ignoreSelf = false) {
			static $cache = array();

			if(isset($cache[$owner_id])) return $cache[$owner_id];

			$owner = $this->getOwnerType($owner_id);

			if(is_numeric($owner)) {
				$owner = array();
			}

			if($owner_id) {
				$owner[] = $owner_id;
			}
			$owner[] = self::getGuestId();

			$owner = array_unique($owner);

			if(sizeof($owner) > 2) {
				foreach($owner as $i => $id) {
					if($id == $owner_id && $ignoreSelf) {
						unset($owner[$i]);
					}
				}
				$owner = array_unique($owner);
				sort($owner);
			}

			$sql = "";
			$sz = sizeof($owner);
			for($i = 0; $i < $sz; $i++) {
				$sql .= "cp.owner_id = '{$owner[$i]}'";
				if($i < ($sz - 1)) {
					$sql .= " OR ";
				}
			}
			$sql = "({$sql})";

			return $cache[$owner_id] = $sql;
		}


		/**
		 * Узнать, разрешен ли пользователю или группе $owner_id доступ к модулю $module
		 * @param Integer $owner_id id пользователя или группы пользователей
		 * @param String $module название модуля
		 * @return Boolean true если доступ разрешен
		 */
		public function isAllowedModule($owner_id, $module) {
			static $cache = array();

			if ($owner_id == false) {
				$owner_id = $this->getUserId();
			}

			if ($this->isSv($owner_id)) {
				return true;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$module = $connection->escape($module);

			if (substr($module, 0, 7) == "macros_") {
				return false;
			}

			if ($owner_id == $this->getUserId() && $this->modulesPermissionsLoadingEnabled) {
				return (isset($this->currentModulesPermissions[$module])) ? (bool) $this->currentModulesPermissions[$module] : false;
			}

			if (isset($cache[$owner_id][$module])) {
				return $cache[$owner_id][$module];
			}

			$sql_where = $this->makeSqlWhere($owner_id);
			$sql = "SELECT module, MAX(cp.allow) FROM cms_permissions cp WHERE method IS NULL AND {$sql_where} GROUP BY module";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			foreach ($result as $row) {
				list($m, $allow) = $row;
				$cache[$owner_id][$m] = $allow;
			}

			return isset($cache[$owner_id][$module]) ? (bool) $cache[$owner_id][$module] : false;
		}

		/**
		 * Узнать, разрешен ли пользователю или группе $owner_id доступ к методу $method модуля $module
		 * @param Integer $owner_id id пользователя или группы пользователей
		 * @param String $module название модуля
		 * @param String $method название метода
		 * @return Boolean true если доступ на метод разрешен
		 */
		public function isAllowedMethod($owner_id, $module, $method, $ignoreSelf = false) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$module = $connection->escape($module);
			$owner_id = (int) $owner_id;

			if ($module == "content" && !strlen($method)) {
				return 1;
			}
			if ($module == "config" && $method == "menu") {
				return 1;
			}
			if ($module == "eshop" && $method == "makeRealDivide") {
				return 1;
			}

			if ($this->isAdmin($owner_id)) {
				if ($this->isAdminAllowedMethod($module, $method)) {
					return 1;
				}
			}

			if ($this->isSv($owner_id)) {
				return true;
			}
			if (!$module) {
				return false;
			}

			$method = $this->getBaseMethodName($module, $method);

			if ($module == "backup" && $method == "rollback") {
				return true;
			}
			if ($module == "autoupdate" && $method == "service") {
				return true;
			}
			if ($module == "config" && ($method == "lang_list" || $method == "lang_phrases")) {
				return true;
			}
			if ($module == "users" && ($method == "auth" || $method == "login_do" || $method == "login")) {
				return true;
			}

			if ($owner_id == $this->getUserId() && $this->modulesPermissionsLoadingEnabled) {
				return (isset($this->currentMethodsPermissions[$module][$method])) ? (bool) $this->currentMethodsPermissions[$module][$method] : false;
			}

			$methodsPermissions = &$this->methodsPermissions;

			if(!isset($methodsPermissions[$owner_id]) || !is_array($methodsPermissions[$owner_id])) {
				$methodsPermissions[$owner_id] = array();
			}

			$cache = &$methodsPermissions[$owner_id];

			$sql_where = $this->makeSqlWhere($owner_id, $ignoreSelf);

			$cache_key = $module;

			if (!array_key_exists($cache_key, $cache)) {
				$cacheData = cacheFrontend::getInstance()->loadData('module_perms_' . $owner_id . '_' . $cache_key);
				if (is_array($cacheData)) {
					$cache[$module] = $cacheData;
				} else {
					$sql = "SELECT cp.method, MAX(cp.allow) FROM cms_permissions cp WHERE module = '{$module}' AND {$sql_where} GROUP BY module, method";
					$result = $connection->queryResult($sql);
					$result->setFetchType(IQueryResult::FETCH_ROW);

					$cache[$module] = array();

					foreach ($result as $row) {
						list($cmethod) = $row;
						$cache[$cache_key][] = $cmethod;
					}

					cacheFrontend::getInstance()->saveData('module_perms_' . $owner_id . '_' . $cache_key, $cache[$module], 3600);
				}
			}

			if (in_array($method, $cache[$cache_key]) || in_array(strtolower($method), $cache[$cache_key])) {
				return true;
			}

			return false;
		}

		/**
		 * Возвращает права пользователя или группы пользователей на страницу
		 * @param int $owner_id id пользователя или группы пользователей
		 * @param int $object_id id страницы, доступ к которой проверяется
		 * @param bool $resetCache игнорировать кеш
		 * @return array(
		 *	0 => bool права на просмотр страницы,
		 *	1 => bool права на редактирование страницы,
		 *	2 => bool права на создание дочерней страницы,
		 *	3 => bool права на удаление страницы,
		 *	4 => bool права на перемещение страницы
		 * )
		 */
		public function isAllowedObject($owner_id, $object_id, $resetCache = false) {
			$object_id = (int) $object_id;
			if ($object_id == 0) {
				return array(
					false,
					false,
					false,
					false,
					false
				);
			}

			if ($this->isSv($owner_id)) {
				return array(
					true,
					true,
					true,
					true,
					true
				);
			}

			if (array_key_exists($object_id, $this->tempElementPermissions)) {
				$level = $this->tempElementPermissions[$object_id];
				return array(
					(bool) ($level & 1),
					(bool) ($level & 2),
					(bool) ($level & 4),
					(bool) ($level & 8),
					(bool) ($level & 16)
				);
			}

			$cache = &$this->elementsCache;

			if (!$resetCache && isset($cache[$object_id]) && isset($cache[$object_id][$owner_id])) {
				return $cache[$object_id][$owner_id];
			}

			$sql_where = $this->makeSqlWhere($owner_id);
			$sql = "SELECT BIT_OR(cp.level) FROM cms3_permissions cp WHERE rel_id = '{$object_id}' AND {$sql_where}";
			$level = false;
			cacheFrontend::getInstance()->loadSql($sql);

			if (!$level || $resetCache) {
				$connection = ConnectionPool::getInstance()->getConnection();
				$result = $connection->queryResult($sql);
				$result->setFetchType(IQueryResult::FETCH_ROW);
				$level = 0;

				if ($result->length() > 0) {
					$fetchResult = $result->fetch();
					$level = (int) array_shift($fetchResult);
				}

				$level = array(
					(bool) ($level & 1),
					(bool) ($level & 2),
					(bool) ($level & 4),
					(bool) ($level & 8),
					(bool) ($level & 16)
				);
			}

			if ($level) {
				cacheFrontend::getInstance()->saveSql($sql, $level, 600);
			}

			if (!isset($cache[$object_id]))  {
				$cache[$object_id] = array();
			}

			$cache[$object_id][$owner_id] = $level;
			return $level;
		}

		public function isPageReadable($pageId, $resetCache = false) {

			if (!$this->readablePagesLoadingEnabled) {
				return $this->isAllowedObject($this->getUserId(), $pageId, $resetCache);
			}

			$pageId = (int) $pageId;

			if ($pageId == 0) {
				return array(false);
			}

			$userId = (int) $this->getUserId();

			if ($this->isSv($userId)) {
				return array(true);
			}

			if ($resetCache || !is_array($this->readablePages)) {
				$this->loadReadablePages();
			}

			return (isset($this->readablePages[$pageId])) ? array(true) : array(false);
		}

		/**
		 * Узнать, является ли пользователь или группа пользователей $user_id супервайзером
		 *
		 * @param bool|int $user_id id пользователя (по умолчанию используется id текущего пользователя)
		 *
		 * @return bool true, если пользователь является супервайзером
		 */
		public function isSv($userId = false) {
			static $isSv = array();

			$umiObjectsCollection = umiObjectsCollection::getInstance();
			$svGroupId = $umiObjectsCollection->getObjectIdByGUID('users-users-15');
			$userTypeId = $this->umiTypesHelper->getObjectTypeIdByGuid('users-user');

			if ($userId === false) {
				$userId = $this->getUserId();
				$userGroups =  $this->umiPropertiesHelper->getPropertyValue($userId, 'groups', $userTypeId);
				if ((is_array($userGroups) && in_array($svGroupId, $userGroups)) || $userId == $svGroupId) {
					return $isSv[$userId] = true;
				}
			}

			if (isset($isSv[$userId])) {
				return $isSv[$userId];
			}

			if(is_null(getRequest('guest-mode')) == false) {
				return $isSv[$userId] = false;
			}

			$userGroups =  $this->umiPropertiesHelper->getPropertyValue($userId, 'groups', $userTypeId);
			if((is_array($userGroups) && in_array($svGroupId, $userGroups)) || $userId == $svGroupId) {
				return $isSv[$userId] = true;
			}

			return $isSv[$userId] = false;
		}

		/**
		 * Узнать, является ли пользователь $user_id администратором, т.е. есть ли у него доступ
		 * к администрированию хотя бы одного модуля
		 * @param Integer|bool $user_id = false id пользователя (по умолчанию используется id текущего пользователя)
		 * @return Boolean true, если пользователь является администратором
		 */
		public function isAdmin($user_id = false, $ignoreCache = false) {
			static $is_admin = array();

			if ($user_id === false) {
				$user_id = $this->getUserId();
			}

			if (isset($is_admin[$user_id])) {
				return $is_admin[$user_id];
			}

			if ($this->isSv($user_id)) {
				return $is_admin[$user_id] = true;
			}

			if (!$ignoreCache && is_array(getSession('is_admin'))) {
				$is_admin = getSession('is_admin');
				if (isset($is_admin[$user_id])) {
					return $is_admin[$user_id];
				}
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql_where = $this->makeSqlWhere($user_id);
			$sql = <<<SQL
	SELECT COUNT(cp.allow)
		FROM cms_permissions cp
		WHERE method IS NULL AND {$sql_where} AND cp.allow IN (1, 2) GROUP BY module
SQL;
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$cnt = 0;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$cnt = (int) array_shift($fetchResult);
			}

			$is_admin[$user_id] = (bool) $cnt;
			$_SESSION['is_admin'] = $is_admin;

			return $is_admin[$user_id];
		}

		/**
		 * Узнать, является ли пользователь $user_id владельцем объекта (класс umiObject) $object_id
		 * @param Integer $object_id id объекта (класс umiObject)
		 * @param $user_id id пользователя
		 * @return Boolean true, если пользователь является владельцем
		 */
		public function isOwnerOfObject($object_id, $user_id = false) {
			if($user_id == false) {
				$user_id = $this->getUserId();
			}

			if($user_id == $object_id) {	//Objects == User, that's ok
				return true;
			} else {
				$object = umiObjectsCollection::getInstance()->getObject($object_id);
				if($object instanceof umiObject) {
					$owner_id = $object->getOwnerId();
				} else {
					$owner_id = false;
				}

				if($owner_id === false || $owner_id == $user_id) {
					return true;
				} else {
					$guestId = umiObjectsCollection::getInstance()->getObjectIdByGUID('system-guest');
					if($owner_id == $guestId && class_exists('customer')) {
						$customer = customer::get();
						if($customer && ($customer->id == $owner_id)) {
							return true;
						}
					}
					return false;
				}
			}
		}
		/**
		 * Сбросить настройки прав до дефолтных для страницы (класс umiHierarchyElement) $element_id
		 * @param Integer $element_id id страницы (класс umiHierarchyElement)
		 * @return Boolean false если произошла ошибка
		 */
		public function setDefaultPermissions($element_id) {
			$element_id = (int) $element_id;
			if (!umiHierarchy::getInstance()->isExists($element_id)) {
				return false;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$connection->query("START TRANSACTION");
			$sql = "DELETE FROM cms3_permissions WHERE rel_id = '{$element_id}'";
			$connection->query($sql);

			$element = umiHierarchy::getInstance()->getElement($element_id, true, true);

			if (!$element instanceof iUmiHierarchyElement) {
				return false;
			}

			$hierarchy_type_id = $element->getTypeId();
			$hierarchy_type = umiHierarchyTypesCollection::getInstance()->getType($hierarchy_type_id);

			$module = $hierarchy_type->getName();
			$method = $hierarchy_type->getExt();

			//Getting outgroup users
			$type_id = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName("users", "user");

			$sel = new umiSelection;
			$sel->setObjectTypeFilter();
			$sel->addObjectType($type_id);

			$group_field_id = umiObjectTypesCollection::getInstance()->getType($type_id)->getFieldId("groups");
			$sel->setPropertyFilter();
			$sel->addPropertyFilterIsNull($group_field_id);

			$users = umiSelectionsParser::runSelection($sel);

			// add system-guest if not in list
			$guestId = (int) umiObjectsCollection::getInstance()->getObjectIdByGUID('system-guest');
			if($guestId) {
				$users[] = $guestId;
				$users = array_unique($users);
			}

			//Getting groups list
			$object_type_id = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName("users", "users");

			$sel = new umiSelection;

			$sel->setObjectTypeFilter();
			$sel->addObjectType($object_type_id);
			$groups = umiSelectionsParser::runSelection($sel);

			$objects = array_merge($users, $groups);

			//Let's get element's ownerId and his groups (if user)
			$owner_id = $element->getObject()->getOwnerId();

			if ($owner = umiObjectsCollection::getInstance()->getObject($owner_id)) {
				if ($owner_groups = $owner->getValue("groups")) {
					$owner_arr = $owner_groups;
				} else {
					$owner_arr = array($owner_id);
				}
			} else {
				$owner_arr = array();
			}


			foreach ($objects as $ugid) {
				if ($ugid == SV_GROUP_ID) {
					continue;
				}
				if ($module === "content") {
					$method = "page";
				}

				if ($this->isAllowedMethod($ugid, $module, $method)) {
					if (in_array($ugid, $owner_arr) || $ugid == SV_GROUP_ID || $this->isAllowedMethod($ugid, $module, $method . ".edit")) {
						$level = permissionsCollection::E_READ_ALLOWED_BIT +
							permissionsCollection::E_EDIT_ALLOWED_BIT +
							permissionsCollection::E_CREATE_ALLOWED_BIT +
							permissionsCollection::E_DELETE_ALLOWED_BIT +
							permissionsCollection::E_MOVE_ALLOWED_BIT;
					} else {
						$level = permissionsCollection::E_READ_ALLOWED_BIT;
					}
					$sql = "INSERT INTO cms3_permissions (rel_id, owner_id, level) VALUES('{$element_id}', '{$ugid}', '{$level}')";
					$connection->query($sql);
				}
			}

			$connection->query("COMMIT");
			$connection->query("SET AUTOCOMMIT=1");

			$this->cleanupElementPermissions($element_id);

			if (isset($this->elementsCache[$element_id])) {
				unset($this->elementsCache[$element_id]);
			}

			$cache_key = $this->getUserId() . "." . $element_id;
			cacheFrontend::getInstance()->saveSql($cache_key, array(true, true));
			$this->loadReadablePages();

			return true;
		}

		/**
		 * Копирует права с родительского элемента
		 * @param Integer $elementId идентификатор элемента, на который устанавливаем права
		 * @return bool
		 */
		public function setInheritedPermissions($elementId) {
			$elementId = (int) $elementId;
			$hierarchy = umiHierarchy::getInstance();
			$parentId = false;

			if ($element = $hierarchy->getElement($elementId, true)) {
				$parentId = $element->getParentId();
			}

			if (!$parentId) {
				return $this->setDefaultPermissions($elementId);
			}

			$records = $this->getRecordedPermissions($parentId);
			$values  = array();

			foreach($records as $ownerId => $level) {
				$values[] = "('{$elementId}', '{$ownerId}', '{$level}')";
			}

			if (empty($values)) {
				return false;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$connection->query("START TRANSACTION");
			$sql = "DELETE FROM cms3_permissions WHERE rel_id = '{$elementId}'";
			$connection->query($sql);
			$sql = "INSERT INTO cms3_permissions (rel_id, owner_id, level) VALUES " . implode(", ", $values);
			$connection->query($sql);
			$connection->query("COMMIT");
			$connection->query("SET AUTOCOMMIT=1");
			return true;
		}

		/**
		 * Удалить все права на странциу $elementId для ползователя или группы $ownerId
		 * @param Integer $elementId id страницы (класс umiHierarchyElement)
		 * @param Integer $ownerId=false id пользователя или группы, чьи права сбрасываются. Если false, то права сбрасываются для всех пользователей
		 * @return bool
		 */
		public function resetElementPermissions($elementId, $ownerId = false) {
			$elementId = (int) $elementId;

			if ($ownerId === false) {
				$sql = "DELETE FROM cms3_permissions WHERE rel_id = '{$elementId}'";

				if (isset($this->elementsCache[$elementId])) {
					unset($this->elementsCache[$elementId]);
				}
			} else {
				$ownerId = (int) $ownerId;
				$sql = "DELETE FROM cms3_permissions WHERE owner_id = '{$ownerId}' AND rel_id = '{$elementId}'";

				if (isset($this->elementsCache[$elementId]) && isset($this->elementsCache[$elementId][$ownerId])) {
					unset($this->elementsCache[$elementId][$ownerId]);
				}
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$connection->query($sql);
			return true;
		}

		/**
		 * Удалить права на все страницы для пользователя или группы
		 * @param int $ownerId ид пользователя или группы
		 * @return void
		 */
		public function deleteElementsPermissionsByOwnerId($ownerId) {
			$ownerId = (int) $ownerId;
			$connection = ConnectionPool::getInstance()->getConnection();
			$deletion = "DELETE FROM `cms3_permissions` WHERE `owner_id` = $ownerId";
			$connection->query($deletion);
		}

		/**
		 * Сбросить все права на модули и методы для пользователя или группы $ownerId
		 * @param Integer $ownerId id пользователя или группы пользователей
		 * @param array $modules=NULL массив, который указывает модули, для которых сбросить права. По умолчанию, сбрасываются права на все модули
		 * @return bool
		 */
		public function resetModulesPermissions($ownerId, $modules = NULL) {
			$ownerId = (int) $ownerId;
			$sql = "DELETE FROM cms_permissions WHERE owner_id = '{$ownerId}'";
			$connection = ConnectionPool::getInstance()->getConnection();

			if (is_array($modules) && count($modules) > 0) {
				$modules = array_map(array($connection, 'escape'), $modules);
				$sql = "DELETE FROM cms_permissions WHERE owner_id = '{$ownerId}' AND module IN ('" . implode("', '", $modules) . "')";
			}

			$connection->query($sql);

			$cacheFrontend = cacheFrontend::getInstance();

			foreach ($modules as $module) {
				$cacheFrontend->deleteKey('module_perms_' . $ownerId . '_' . $module, true);
			}

			return true;
		}

		/**
		 * Установить определенные права на страница $elementId для пользователя или группы $ownerId
		 * @param Integer $ownerId id пользователя или группы пользователей
		 * @param Integer $elementId id страницы (класс umiHierarchyElement), для которой меняются права
		 * @param Integer $level уровень выставляемых прав от "0" до "31":
		 * ---------------------------------------------------------------------------
		 * | значение | чтение | редактирование | создание | удаление |  перемещение |
		 * |    0     |   -    |       -        |    -     |    -     |       -      |
		 * |    1     |   +    |       -        |    -     |    -     |       -      |
		 * |    3     |   +    |       +        |    -     |    -     |       -      |
		 * |    7     |   +    |       +        |    +     |    -     |       -      |
		 * |    15    |   +    |       +        |    +     |    +     |       -      |
		 * |    31    |   +    |       +        |    +     |    +     |       +      |
		 * ---------------------------------------------------------------------------
		 *
		 * @return Boolean true если не произошло ошибки
		 */
		public function setElementPermissions($ownerId, $elementId, $level) {
			$ownerId = (int) $ownerId;
			$elementId = (int) $elementId;
			$level = (int) $level;

			if ($elementId == 0 || $ownerId == 0) {
				return false;
			}

			if (isset($this->elementsCache[$elementId]) && isset($this->elementsCache[$elementId][$ownerId])) {
				unset($this->elementsCache[$elementId][$ownerId]);
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql_reset = "DELETE FROM cms3_permissions WHERE owner_id = '".$ownerId."' AND rel_id = '".$elementId."'";
			$connection->query($sql_reset);

			$sql = "INSERT INTO cms3_permissions (owner_id, rel_id, level) VALUES('{$ownerId}', '{$elementId}', '{$level}')";
			$connection->query($sql);

			$this->cleanupElementPermissions($elementId);

			if ($this->readablePagesLoadingEnabled) {
				$this->loadReadablePages();
			} else {
				$this->isAllowedObject($ownerId, $elementId, true);
			}

			return true;
		}


		/**
		 * Разрешить пользователю или группе $owner_id права на $module/$method
		 * @param Integer $ownerId id пользователя или группы пользователей
		 * @param String $module название модуля
		 * @param String|bool $method=false название метода
		 * @return bool
		 */
		public function setModulesPermissions($ownerId, $module, $method = false, $cleanupPermissions = true) {
			$ownerId = (int) $ownerId;
			$connection = ConnectionPool::getInstance()->getConnection();
			$module = $connection->escape($module);

			if ($method !== false) {
				return $this->setMethodPermissions($ownerId, $module, $method);
			}

			$sql = "INSERT INTO cms_permissions (owner_id, module, method, allow) VALUES('{$ownerId}', '{$module}', NULL, '1')";
			$connection->query($sql);

			if ($cleanupPermissions) {
				$this->cleanupBasePermissions();
			}

			return true;
		}

		protected function setMethodPermissions($ownerId, $module, $method, $cleanupPermissions = true) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$method = $connection->escape($method);
			$module = $connection->escape($module);
			$ownerId = (int) $ownerId;

			$sql = "INSERT INTO cms_permissions (owner_id, module, method, allow) VALUES('{$ownerId}', '{$module}', '{$method}', '1')";
			$connection->query($sql);

			$this->methodsPermissions[$ownerId][$module][] = $method;

			if ($cleanupPermissions) {
				$this->cleanupBasePermissions();
			}

			return true;
		}

		/**
		 * Узнать, имеет ли пользователь или группа в принципе права на какие-нибудь страницы
		 * @param Integer $ownerId id пользователя или группы
		 * @return Boolean false, если записей нет
		 */
		public function hasUserPermissions($ownerId) {
			$ownerId = (int) $ownerId;
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT COUNT(*) FROM cms3_permissions WHERE owner_id = '{$ownerId}'";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$count = 0;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$count = (int) array_shift($fetchResult);
			}

			return (bool) $count;
		}

		/**
		 * Узнать, имеет ли пользователь или группа права на какие-нибудь модули
		 * @param int $ownerId id пользователя или группы
		 * @return bool
		 */
		public function hasUserModulesPermissions($ownerId) {
			$ownerId = intval($ownerId);

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT `module` FROM `cms_permissions` WHERE `owner_id` = $ownerId AND `allow` = 1 LIMIT 0,1";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ASSOC);

			return ($result->length() == 1);
		}

		/**
		 * Скопировать права на все страницы из $fromUserId в $toUserId
		 * @param Integer $fromUserId id пользователя или группы пользователей, из которых копируются права
		 * @param Integer $fromUserId id пользователя или группы пользователей, в которые копируются права
		 * @return bool
		 */
		public function copyHierarchyPermissions($fromUserId, $toUserId) {
			if ($fromUserId == self::getGuestId()) {
				return false;		//No need in cloning guest permissions now
			}

			$fromUserId = (int) $fromUserId;
			$toUserId = (int) $toUserId;
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "INSERT INTO cms3_permissions (level, rel_id, owner_id) SELECT level, rel_id, '{$toUserId}' FROM cms3_permissions WHERE owner_id = '{$fromUserId}'";
			$connection->query($sql);

			return true;
		}

		/**
		 * Системный метод. Получить массив прав из permissions.php и permissions.custom.php
		 * @return array
		 */
		public function getStaticPermissions($module, $templater = false) {
			static $cache = array();

			if (isset($cache[$module]) && !$templater) {
				return $cache[$module];
			}

			$static_file = SYS_MODULES_PATH . $module . "/permissions.php";
			if(file_exists($static_file)) {
				require $static_file;
				if(isset($permissions)) {
					$static_permissions = $permissions;

					$static_file_custom = SYS_MODULES_PATH . $module . "/permissions.custom.php";
					if(file_exists($static_file_custom)) {
						unset($permissions);
						require $static_file_custom;
						if(isset($permissions)) {
							$static_permissions = array_merge_recursive($static_permissions, $permissions);
						}
					}

					// подключаем права из ресурсов шаблона
					// TODO: refactoring
					if ($resourcesDir = cmsController::getInstance()->getResourcesDirectory()) {
						$static_file_custom = $resourcesDir . '/classes/modules/' . $module . "/permissions.php";
						if (file_exists($static_file_custom)) {
							unset($permissions);
							require $static_file_custom;
							if(isset($permissions)) {
								$static_permissions = array_merge_recursive($static_permissions, $permissions);
							}
						}
					}

					// подключение прав из расширений
					$path = SYS_MODULES_PATH . $module . "/ext/permissions.*.php";

					$fileNames = glob($path);
					if (is_array($fileNames)) {
						foreach ($fileNames as $filename) {
							if (file_exists($filename)) {
								unset($permissions);
								require $filename;
								if (isset($permissions)) {
									$static_permissions = array_merge_recursive($static_permissions, $permissions);
								}
							}
						}
					}

					$cache[$module] = $static_permissions;
					unset($static_permissions);
					unset($permissions);
				}
				else $cache[$module] = array();
			}
			else $cache[$module] = array();

			return $cache[$module];
		}

		/**
		 * Получить название корневого метода в системе приритета прав для $module::$method
		 * @param String $module название модуля
		 * @param String $method название метода
		 * @return String название корневого метода
		 */
		protected function getBaseMethodName($module, $method) {
			$methods = $this->getStaticPermissions($module);

			if($method && is_array($methods)) {
				if(array_key_exists($method, $methods)) {
					return $method;
				} else {
					foreach($methods as $base_method => $sub_methods) {
						if(is_array($sub_methods)) {
							if(in_array($method, $sub_methods) || in_array(strtolower($method), $sub_methods)) {
								return $base_method;
							}
						}
					}
					return $method;
				}
			} else {
				return $method;
			}
		}

		/**
		 * Получить id текущего пользователя
		 * @return Integer id текущего пользователя
		 */
		public function getUserId() {
			if ($this->user_id === 0) {
				/**
				 * @var users $users
				 */
				$users = cmsController::getInstance()->getModule("users");
				if ($users instanceof def_module) {
					$this->user_id = $users->user_id;
				}
			}

			return $this->user_id;
		}

		/**
		 * Удалить все записи о правах на модули и методы для пользователей, если они ниже, чем у гостя
		 */
		public function cleanupBasePermissions() {
			$guestId = self::getGuestId();

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql    = "SELECT module, method FROM cms_permissions WHERE owner_id = '{$guestId}' AND allow = 1";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			$sql = array();

			foreach ($result as $row) {
				list($module, $method) = $row;
				$sql[] = ($method) ? "(module = '{$module}' AND method = '{$method}')" : "(module = '{$module}' AND method IS NULL)";
			}

			if (!empty($sql)) {
				$connection->query("DELETE FROM cms_permissions WHERE owner_id != '{$guestId}' AND (" . implode(' OR ', $sql) . ")");
			}
		}

		/**
		 * Удалить для страницы  с id $rel_id записи о правах пользователей, которые ниже, чем у гостя
		 * @param Integer $rel_id id страница (класс umiHierarchyElement)
		 */
		protected function cleanupElementPermissions($rel_id) {
			$rel_id = (int) $rel_id;
			$guestId = self::getGuestId();

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT level FROM cms3_permissions WHERE owner_id = '{$guestId}' AND rel_id = {$rel_id}";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			$maxLevel = 0;

			foreach ($result as $row) {
				$level = array_shift($row);

				if ($level > $maxLevel) {
					$maxLevel = $level;
				}
			}

			$connection->query("DELETE FROM cms3_permissions WHERE owner_id != '{$guestId}' AND level <= {$maxLevel} AND rel_id = {$rel_id}");
		}

		/**
		 * Узнать, разрешено ли пользователю или группе $owner_id администрировать домен $domain_id
		 * @param Integer $owner_id id пользователя или группы пользователей
		 * @param Integer $domain_id id домена (класс domain)
		 * @return Integer 1, если доступ разрешен, 0 если нет
		 */
		public function isAllowedDomain($owner_id, $domain_id) {
			$owner_id = (int) $owner_id;
			$domain_id = (int) $domain_id;

			if ($this->isSv($owner_id)) {
				return 1;
			}

			$sql_where_owners = $this->makeSqlWhere($owner_id);
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT MAX(cp.allow) FROM cms_permissions cp WHERE cp.module = 'domain' AND cp.method = '{$domain_id}' AND " . $sql_where_owners;
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			$isAllowed = 0;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$isAllowed = (int) array_shift($fetchResult);
			}

			return $isAllowed;
		}

		/**
		 * Установить права пользователю или группе $owner_id на администрирование домена $domain_id
		 * @param Integer $owner_id id пользователя или группы пользователей
		 * @param Integer $domain_id id домена (класс domain)
		 * @param Boolean $allow=true если true, то доступ разрешен
		 * @return bool
		 */
		public function setAllowedDomain($owner_id, $domain_id, $allow = 1) {
			$owner_id = (int) $owner_id;
			$domain_id = (int) $domain_id;
			$allow = (int) $allow;

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "DELETE FROM cms_permissions WHERE module = 'domain' AND method = '{$domain_id}' AND owner_id = '{$owner_id}'";
			$connection->query($sql);

			$sql = "INSERT INTO cms_permissions (module, method, owner_id, allow) VALUES('domain', '{$domain_id}', '{$owner_id}', '{$allow}')";
			$connection->query($sql);

			return true;
		}

		/**
		 * Установить права по умолчанию для страницы $element по отношению к пользователю $owner_id
		 * @param umiHierarchyElement $element экземпляр страницы
		 * @param Integer $owner_id id пользователя или группы пользователей
		 * @return Integer уровен доступа к странице, который был выбран системой
		 */
		public function setDefaultElementPermissions(iUmiHierarchyElement $element, $owner_id) {
			$module = $element->getModule();
			$method = $element->getMethod();

			$level = 0;
			if($this->isAllowedMethod($owner_id, $module, $method, true)) {
				$level = permissionsCollection::E_READ_ALLOWED_BIT;
			}

			if($this->isAllowedMethod($owner_id, $module, $method . ".edit", true)) {
				$level = permissionsCollection::E_READ_ALLOWED_BIT +
					permissionsCollection::E_EDIT_ALLOWED_BIT +
					permissionsCollection::E_CREATE_ALLOWED_BIT +
					permissionsCollection::E_DELETE_ALLOWED_BIT +
					permissionsCollection::E_MOVE_ALLOWED_BIT;
			}

			$this->setElementPermissions($owner_id, $element->getId(), $level);

			return $level;
		}

		/**
		 * Сбросить для пользователя или группы $owner_id права на все страницы на дефолтные
		 * @param Integer $owner_id id пользователя или группы пользователей
		 */
		public function setAllElementsDefaultPermissions($owner_id) {
			$owner_id = (int) $owner_id;
			$hierarchyTypes = umiHierarchyTypesCollection::getInstance();

			$this->elementsCache = array();

			$owner = $this->getOwnerType($owner_id);
			if (is_numeric($owner)) {
				$owner = array();
			}

			$owner[] = self::getGuestId();
			$owner = array_unique($owner);

			$connection = ConnectionPool::getInstance()->getConnection();
			$connection->query("START TRANSACTION");

			$read = array();
			$write = array();

			foreach ($hierarchyTypes->getTypesList() as $hierarchyType) {
				$module = $hierarchyType->getName();
				$method = $hierarchyType->getExt();

				if ($this->isAllowedMethod($owner_id, $module, $method . ".edit", true)) {
					foreach ($owner as $gid) {
						if ($this->isAllowedMethod($gid, $module, $method . ".edit", true)) {
							continue 2;
						}
					}
					$write[] = $hierarchyType->getId();
					$level = 2;
				} else if ($this->isAllowedMethod($owner_id, $module, $method, true)) {
					foreach ($owner as $gid) {
						if ($this->isAllowedMethod($gid, $module, $method, true)) {
							continue 2;
						}
					}

					$read[] = $hierarchyType->getId();
					$level = 1;
				} else {
					$level = 0;
				}
			}

			if (sizeof($read)) {
				$types = implode(", ", $read);

				$sql = <<<SQL
	INSERT INTO cms3_permissions (level, owner_id, rel_id)
		SELECT 1, '{$owner_id}', id FROM cms3_hierarchy WHERE type_id IN ({$types})
SQL;
				$connection->query($sql);
			}

			if (sizeof($write)) {
				$types = implode(", ", $write);

				$sql = <<<SQL
	INSERT INTO cms3_permissions (level, owner_id, rel_id)
		SELECT 31, '{$owner_id}', id FROM cms3_hierarchy WHERE type_id IN ({$types})
SQL;
				$connection->query($sql);
			}

			$connection->query("COMMIT");
			$this->loadReadablePages();
		}

		/**
		 * Получить список всех пользователей или групп, имеющих права на страницу $elementId
		 * @param Integer $elementId id страницы
		 * @param Integer $level = 1 искомый уровень прав
		 * @return array массив id пользователей или групп, имеющих права на страницу
		 */
		public function getUsersByElementPermissions($elementId, $level = 1) {
			$elementId = (int) $elementId;
			$level = (int) $level;

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT owner_id FROM cms3_permissions WHERE rel_id = '{$elementId}' AND level >= '{$level}'";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			$owners = array();

			foreach ($result as $row) {
				$owners[] = (int) array_shift($row);
			}

			return $owners;
		}

		/**
		 * Получить список сохраненных прав для страницы $elementId
		 * @param Integer $elementId
		 * @return array $ownerId => $permissionsLevel
		 */
		public function getRecordedPermissions($elementId) {
			$elementId = (int) $elementId;

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT owner_id, level FROM cms3_permissions WHERE rel_id = '{$elementId}'";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			$records = array();

			foreach ($result as $row) {
				list($ownerId, $level) = $row;
				$records[$ownerId] = (int) $level;
			}

			return $records;
		}

		/**
		 * Указать права на страницу. Влияет только на текущую сессию, данные в базе изменены не будут
		 * @param Integer $elementId id страницы
		 * @param Integer $level = 1 уровень прав доступа (0-3).
		 */
		public function pushElementPermissions($elementId, $level = 1) {
			$this->tempElementPermissions[$elementId] = (int) $level;
		}

		/**
		 * Узнать, авторизован ли текущий пользователь
		 * @return Boolean true, если авторизован
		 */
		public function isAuth() {
			return ($this->getUserId() != self::getGuestId());
		}

		/**
		 * Алиас для isAuth()
		 * @return bool
		 */
		public function is_auth() {
			return $this->isAuth();
		}

		/**
		 * Позволяет узнать id пользователя "Гостя"
		 * @return Integer $guestId id пользователя "Гость"
		 */
		public static function getGuestId() {
			static $guestId;
			if(!$guestId) {
				$guestId = (int) umiObjectsCollection::getInstance()->getObjectIdByGUID('system-guest');
			}
			return $guestId;
		}

		/**
		 * Авторизовать клиента как пользователя $userId
		 *
		 * @param int|umiObject id пользователя, либо объект пользователя
		 *
		 * @return bool успешность операции
		 */
		public function loginAsUser($userId) {
			if(is_null($userId)) {
				return false;
			}

			if(is_array($userId) && sizeof($userId)) {
				list($userId) = $userId;
			}

			if($userId instanceof iUmiObject) {
				$user = $userId;
				$userId = $user->id;
			} else {
				$user = selector::get('object')->id($userId);
			}

			$this->user_id = $userId;

			$login = $user->login;
			$passwordHash = $user->password;

			if(getRequest('u-login-store')) {
				$time = time() + 31536000;
				setcookie("u-login", $user->login, $time, "/");
				setcookie("u-password-md5", $passwordHash, $time, "/");
			}

			$session = session::recreateInstance();
			$session->set('cms_login', $login);
			$session->set('cms_pass', $passwordHash);
			$session->set('user_id', $userId);

			return true;
		}

		/**
		 * Проверить параметры авторизации
		 * @param String login логин
		 * @param String password пароль
		 * @return NULL|umiObject null, либо пользователь
		 */
		public function checkLogin($login, $password) {
			$sel = new selector('objects');
			$sel->types('object-type')->name('users', 'user');
			$sel->where('login')->equals($login);
			$sel->where('password')->equals(md5($password));
			$sel->where('is_activated')->equals(true);

			if ($sel->first) return $sel->first;

			$sel = new selector('objects');
			$sel->types('object-type')->name('users', 'user');
			$sel->where('e-mail')->equals($login);
            // добавлена проверка на sha256 и SALT с сайта ormco.ru из файла /classes/system/subsystems/models/auth/PasswordHash/iAlgorithm.php
			$sel->where('password')->equals(hash('sha256', $password . "o95j43hiwjrthpoiwj45ihwpriobneop;jfgp3408ghqpqh5gpqoi4hgp9q85h"));
			$sel->where('is_activated')->equals(true);

			return $sel->first;
		}

		public function getPrivileged($perms) {
			if (!sizeof($perms)) {
				return array();
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = 'SELECT owner_id FROM cms_permissions WHERE ';
			$sqls = array();

			foreach ($perms as $perm) {
				$module = $connection->escape(getArrayKey($perm, 0));
				$method = $connection->escape($this->getBaseMethodName($module, getArrayKey($perm, 1)));
				$sqls[] = "(module = '{$module}' AND method = '{$method}')";
			}

			$sql .= implode(' OR ', $sqls);
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			$owners = array();

			foreach ($result as $row) {
				$owners[] = array_shift($row);
			}

			$owners = array_unique($owners);
			return $owners;
		}

		/**
		 * Очищает внутренний кеш класса
		 * @return void
		 */
		public function clearCache() {
			$this->elementsCache = array();
			$this->methodsPermissions = array();
		}

		/**
		 * Загружает id страниц, доступных на чтение текущему пользователю
		 * @return bool
		 */
		public function loadReadablePages() {
			$currentUserId = $this->getUserId();
			$this->readablePages = array();

			if ($this->isSv($currentUserId)) {
				$this->readablePages = 'sv';
				return true;
			}

			$whereSqlPart = $this->makeSqlWhere($currentUserId);
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT cp.rel_id AS id FROM `cms3_permissions` AS cp WHERE {$whereSqlPart} AND cp.level&1 = 1;";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ASSOC);

			$pagesIds = array();

			foreach ($result as $row) {
				$pagesIds[array_shift($row)] = true;
			}

			$this->readablePages = $pagesIds;
			return true;
		}

		protected function isAdminAllowedMethod($module, $method) {
			$methods = array(
				'content' =>    array('json_mini_browser', 'old_json_load_files', 'json_load_files',
					'json_load_zip_folder', 'load_tree_node', 'get_editable_region',
					'save_editable_region', 'widget_create', 'widget_delete',
					'getObjectsByTypeList', 'getObjectsByBaseTypeList',
					'json_get_images_panel', 'json_create_imanager_object',
					'domainTemplates', 'json_unlock_page', 'tree_unlock_page'),
				'backup' =>     array('backup_panel'),
				'data'   =>     array('guide_items', 'guide_items_all', 'json_load_hierarchy_level'),
				'webo' => array('show'),
				'users'  => array('getFavourites', 'json_change_dock', 'saveUserSettings', 'loadUserSettings'),
				'*'		 =>		array('dataset_config')
			);

			if(isset($methods[$module])) {
				if(in_array($method, $methods[$module])) {
					return true;
				}
			}
			if(isset($methods['*'])) {
				if(in_array($method, $methods['*'])) {
					return true;
				}
			}
			return false;
		}

		/**
		 * Загружает информацию о правах на модули для текущего пользователя
		 * @return bool
		 */
		private function loadModulesPermissions() {
			$currentUserId = $this->getUserId();

			if ($this->isSv($currentUserId)) {
				$this->modulesPermissions = 'sv';
				return true;
			}

			$whereSqlPart = $this->makeSqlWhere($currentUserId);
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT cp.module as module, cp.method as method, cp.allow as allow FROM cms_permissions AS cp WHERE {$whereSqlPart}";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ASSOC);

			$modulesPermissions = array();
			$methodPermissions = array();

			foreach ($result as $row) {
				if (is_null($row['method'])) {
					$modulesPermissions[$row['module']] = $row['allow'];
				} else {
					$methodPermissions[$row['module']][$row['method']] = $row['allow'];
				}
			}

			$this->currentModulesPermissions = $modulesPermissions;
			$this->currentMethodsPermissions = $methodPermissions;
			return true;
		}

	};
?>
