<?php

class UniSenderApiContactModelWrapper extends translatorWrapper
{
    public function translate($v8d777f385d3dfec8815d20f7496026dc)
    {
        return $this->translateData($v8d777f385d3dfec8815d20f7496026dc);
    }
    
    protected function translateData(UniSenderApiContactModel $contactModel)
    {
        $return = array();
        
        $email = array(
            'node:value' => $contactModel->getEmail()
        );
        if($emailStatus = $contactModel->getEmailStatus()) {
            $email['attribute:status'] = $emailStatus;
        }
        $return['email'] = $email;
        
        if($emailListIds = $contactModel->getEmailListIds()) {
            $return['email_list_ids'] = array('nodes:item' => $emailListIds);
        }
    
        $phone = array(
            'node:value' => $contactModel->getPhone()
        );
        $return['phone'] = $phone;
        
        if($data = $contactModel->getData()) {
            $return['data'] = $data;
        }
        
        return $return;
    }
}