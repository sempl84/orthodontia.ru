<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

$classesDir = dirname(__FILE__) . '/site/Classes';
$helpersDir = dirname(__FILE__) . '/site/Helpers';
$modelsDir = dirname(__FILE__) . '/site/Models';

$classes = [
    'CatalogIndexFilters' => $classesDir . '/Catalog/IndexFilters.php',
    'OrthoUniSendersSyncContacts' => $classesDir . '/OrthoUniSender/SyncContacts.php',

    'OrthoSiteDataDadataHelper' => $helpersDir . '/OrthoSiteData/Dadata.php',
    'ObjectHelper' => $helpersDir . '/Object.php',
    
    'SiteCatalogObjectModel' => $modelsDir . '/Catalog/Object.php',
    
    'SiteContentPageSettingsModel' => $modelsDir . '/Content/Page/Settings.php',

    'SiteEmarketOrderModel' => $modelsDir . '/Emarket/Order.php',
    
    'SiteUsersUserModel' => $modelsDir . '/Users/User.php',
];