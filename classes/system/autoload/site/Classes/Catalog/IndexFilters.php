<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2020, Evgenii Ioffe
 */

class CatalogIndexFilters
{
    const action_prepare = 'prepare';
    const action_index = 'index';

    const param_has_errors = 'has_errors';

    private $id = '';
    private $action = '';
    private $params = array();
    /**
     * @var catalog
     */
    private $catalog;

    private $isDone = false;

    private $checkLock = true;

    private $start = 0;

    public function __construct()
    {
        outputBuffer::current(defined('CRON') == 'CLI' ? 'CLIOutputBuffer' : 'HTTPOutputBuffer');

        $this->catalog = cmsController::getInstance()->getModule('catalog');
        if (!$this->catalog instanceof catalog) {
            throw new publicException('Не найден модуль catalog');
        }

        $this->loadParams();
    }

    public function init()
    {
        $this->clearLog();

        $this->id = time();
        $this->action = self::action_prepare;
        $this->params = array();

        $this->saveParams();
        $this->addLog('Запущена индексация данных в каталоге');
    }

    public function process()
    {
        $this->start = microtime(true);

        if ($this->checkLock()) {
            $this->setLock();

            try {
                do {
                    if ($this->action == self::action_prepare) {
                        $this->doPrepare();
                    } elseif ($this->action == self::action_index) {
                        $this->doIndex();
                    }
                } while (!$this->isDone && $this->hasTimeForNextAction());
            } catch (Exception $e) {
                $this->addError('Ошибка: ' . $e->getMessage());
            }

            $this->removeLock();
        } else {
            $this->addLog('Скрипт уже запущен');
        }
    }

    public function isDone()
    {
        return $this->isDone;
    }

    private function doPrepare()
    {
        $this->addLog('Получаем список разделов для индексации');

        $sel = new selector('pages');
        $sel->types('hierarchy-type')->name('catalog', 'category');
        $sel->where('index_choose')->equals(true);
        $sel->where('is_active')->equals(array(0, 1));
        $sel->where('domain')->equals(1);
        $sel->where('lang')->equals(1);
        $sel->order('ord')->asc();
        $sel->option('no-length')->value(true);
        $sel->option('no-permissions')->value(true);
        $arCategory = $sel->result();
        if (!$arCategory) {
            $this->addError('Не найдены разделы для индексации');
            $this->finish();
            return false;
        }

        $arCategoryId = array();
        foreach ($arCategory as $category) {
            if (!$category instanceof umiHierarchyElement) {
                continue;
            }
            $arCategoryId[] = $category->getId();
        }

        $this->action = self::action_index;
        $this->params['category_id'] = $arCategoryId;
        $this->saveParams();

        $this->addLog('Получен список разделов для индексации');

        return true;
    }

    private function doIndex()
    {
        $arCategoryId = getArrayKey($this->params, 'category_id');
        if (!is_array($arCategoryId)) {
            $this->addError('Не найден параметр category_id');
            $this->finish();
            return false;
        }

        $categoryId = intval($arCategoryId[0]);
        $umiHierarchy = umiHierarchy::getInstance();
        $category = $umiHierarchy->getElement($categoryId, true);
        $catalogObjectHierarchyTypeId = $this->catalog->getProductHierarchyTypeId();
        $offset = intval(getArrayKey($this->params, 'offset'));

        $bIndex = false;
        if ($category instanceof umiHierarchyElement) {
            if(!$offset && !$umiHierarchy->getChildrenCount($categoryId, false, true, $category->getValue('index_level'), $catalogObjectHierarchyTypeId)) {
                $this->addLog('В разделе ' . $category->getName() . ' не найдены объекты');
            } else {
                $bIndex = true;
            }
        } else {
            $this->addError('Не найден раздел ' . $categoryId);
        }

        if ($bIndex) {
            if ($offset == 0) {
                $count = 0;
                $this->addLog('Запущена индексация раздела ' . $category->getName());
            } else {
                $count = $this->params['count'];
            }

            $limit = 25;
            $indexGenerator = new FilterIndexGenerator($catalogObjectHierarchyTypeId, 'pages');
            $indexGenerator->setHierarchyCondition($category->getId(), $category->getValue('index_level'));
            $indexGenerator->setLimit($limit);

            if ($offset == 0) {
                $indexGenerator->deleteStoredOffset();
            }

            try {
                $i = 1;

                do {
                    $i++;
                    $indexGenerator->run();
                    if ($indexGenerator->isDone()) {
                        break;
                    }

                    if ($i % 5 == 0) {
                        $this->addLog('Проиндексировано объектов ' . ($count + ($i * $limit)));
                    }
                } while ($this->hasTimeForNextAction());

                $count += ($i * $limit);

                $this->addLog('Проиндексировано объектов ' . $count);

                if (!$indexGenerator->isDone()) {
                    $this->params['offset'] = $offset + 1;
                    $this->params['count'] = $count;
                } else {
                    $this->params['offset'] = 0;
                    array_shift($arCategoryId);
                    $category->setValue(__filter_catalog::FILTER_INDEX_SOURCE_FIELD_NAME, $category->getId());
                    $category->setValue(__filter_catalog::FILTER_INDEX_INDEXATION_DATE, new umiDate());
                    $category->setValue(__filter_catalog::FILTER_INDEX_INDEXATION_STATE, 100);
                    $category->commit();

                    $this->catalog->markChildrenCategories($category->getId(), $category->getValue('index_level') - 1);
                    $indexGenerator->deleteStoredOffset();
                    $this->addLog('Завершена индексация раздела ' . $category->getName());
                }
            } catch (Exception $e) {
                array_shift($arCategoryId);
                $indexGenerator->deleteStoredOffset();
                $this->addError('Произошла ошибка ' . $e->getMessage());
            }

            $this->saveParams();
        } else {
            $this->params['offset'] = 0;
            array_shift($arCategoryId);
        }

        if (count($arCategoryId)) {
            $this->params['category_id'] = $arCategoryId;
            $this->saveParams();
        } else {
            $this->finish('Завершена индексация данных в каталоге');
            $this->isDone = true;
        }

        return true;
    }

    public function setCheckLock($checkLock)
    {
        $this->checkLock = $checkLock === true;
    }

    /**
     * @return bool
     */
    private function checkLock()
    {
        if (!$this->checkLock) {
            return true;
        }

        $file = $this->getLockFile();
        if (!file_exists($file)) {
            return true;
        }

        $attempt = intval(file_get_contents($file)) + 1;
        $maxAttempt = 3;
        if ($attempt == $maxAttempt) {
            unlink($file);
            $this->addLog('Индексация автоматически перезапущена после ' . $attempt . ' попыток');
            return true;
        }

        file_put_contents($file, $attempt);

        return false;
    }

    public function isActive()
    {
        return $this->id ? true : false;
    }

    private function finish($message = '')
    {
        $this->addLog($message);

        $file = $this->getParamsFile();
        if (file_exists($file)) {
            unlink($file);
        }

        unset($this->id);
    }

    private function loadParams()
    {
        $file = $this->getParamsFile();
        if (!file_exists($file)) {
            return false;
        }

        $params = json_decode(file_get_contents($file), true);
        if (!is_array($params)) {
            return false;
        }

        $this->id = getArrayKey($params, 'id');
        $this->action = getArrayKey($params, 'action');
        $this->params = getArrayKey($params, 'params');

        return true;
    }

    private function saveParams()
    {
        $file = $this->getParamsFile();
        if (!is_dir(dirname($file))) {
            mkdir(dirname($file), 0775, true);
        }

        file_put_contents($file, json_encode([
            'id' => $this->id,
            'action' => $this->action,
            'params' => $this->params
        ]));
    }

    private function getParamsFile()
    {
        return $this->getBaseDir() . '/params.json';
    }

    private $log = array();

    private function addLog($message = '')
    {
        if (!$this->id || !$message) {
            return false;
        }

        $this->log[] = $message;

        $file = $this->getLogDir() . '/' . $this->id . '.log';
        if (!is_dir(dirname($file))) {
            mkdir(dirname($file), 0775, true);
        }

        file_put_contents($file, date('H:i:s') . ': ' . $message . PHP_EOL, FILE_APPEND);

        return true;
    }

    private function addError($message)
    {
        $this->params[self::param_has_errors] = true;

        $this->addLog($message);
    }

    public function getLog()
    {
        return $this->log;
    }

    private function clearLog()
    {
        $dir = $this->getLogDir();
        if (!is_dir($dir)) {
            return false;
        }

        /* Логи хранятся 15 дней*/
        $time = time() - (60 * 60 * 24 * 15);

        $iterator = new DirectoryIterator($dir);
        foreach ($iterator as $file) {
            if (!$file->isFile()) {
                continue;
            }

            if ($file->getMTime() < $time) {
                unlink($file->getPath());
            }
        }

        return true;
    }

    private static function getBaseDir()
    {
        return CURRENT_WORKING_DIR . '/sys-temp/site/catalog/index_filters';
    }

    public static function getLogDir()
    {
        return self::getBaseDir() . '/log';
    }

    private function getLock()
    {
        return is_file($this->getLockFile());
    }

    private function removeLock()
    {
        $file = $this->getLockFile();
        if (file_exists($file)) {
            unlink($file);
        }
    }

    private function setLock()
    {
        file_put_contents($this->getLockFile(), 0);
    }

    private function getLockFile()
    {
        return $this->getBaseDir() . '/.lock';
    }

    private function hasTimeForNextAction()
    {
        return (microtime(true) - $this->start) <= 50;
    }

    public function testFinish()
    {
        $this->addError('Тестовая ошибка');
        $this->finish();
    }
}