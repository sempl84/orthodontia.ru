<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class SiteCatalogObjectModel
{
    const module = 'catalog';
    const method = 'object';
    
    const test_page_id = 2848;
    
    const field_price = 'price';
    
    const field_listeners_speakers = 'listeners_speakers';
    
    const group_reserve = 'reserv_group';
    const field_speakers_left = 'object_speakers_left';
    
    const group_system = 'object_system';
    const field_system_log = 'object_system_log';
    
    public static function addToLog(umiHierarchyElement $element, $message)
    {
        $log = trim($element->getValue(self::field_system_log));
        
        if ($log) {
            $log .= PHP_EOL;
        }
        
        $log .= date('d.m.Y H:i:s') . ' ' . $message;
        $element->setValue(self::field_system_log, $log);
        $element->commit();
    }
    
    public static function isSpeakersDiscountEvent(umiHierarchyElement $element)
    {
        if(!$element->getValue(self::field_listeners_speakers)) {
            return false;
        }
        
        if(mb_strtolower($element->getValue('nazvanie_skidki_1')) != 'выступающий') {
            return false;
        }
        
        return true;
    }
}