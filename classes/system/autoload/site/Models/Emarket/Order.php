<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class SiteEmarketOrderModel
{
    const module = 'emarket';
    const method = 'order';

    const field_need_export = 'need_export';

    const group_event = 'event_params';
    const field_event_participation = 'event_participation';

    const group_1c = 'order_1c';
    const field_1c_log = 'order_1c_log';

    public static function add1cLog($orderId, $text)
    {
        $order = umiObjectsCollection::getInstance()->getObject($orderId);
        if(!$order instanceof umiObject) {
            throw new publicException('Не найден объект заказа ' . $orderId);
        }

        $log = trim(date('d.m.Y H:i:s') . ' ' . $text . PHP_EOL . $order->getValue(self::field_1c_log));
        $order->setValue(self::field_1c_log, $log);
        $order->commit();
    }

    public static function canExportTo1C(order $order)
    {
        return true;
        return self::canExportTo1CByExchangeActive() && self::canExportTo1CByEventType($order);
    }

    protected static function canExportTo1CByEventType(order $order)
    {
        $return = false;

        foreach ($order->getItems() as $orderItem) {
            if (!$orderItem instanceof orderItem) {
                continue;
            }

            $element = $orderItem->getItemElement(true);
            if (!$element instanceof umiHierarchyElement) {
                continue;
            }

            if ($element->getValue('event_type') != 1280) {
                $return = true;
            }
        }

        return $return;
    }

    protected static function canExportTo1CByExchangeActive()
    {
        return strpos(getServer('REQUEST_URI'), 'admin/exchange/auto') === false;
    }
}