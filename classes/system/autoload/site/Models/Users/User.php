<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class SiteUsersUserModel
{
    const module = 'users';
    const method = 'user';
    
    const field_is_active = 'is_activated';
    const field_surname = 'lname';
    const field_name = 'fname';
    const field_father_name = 'father_name';
    const field_email = 'e-mail';
    const field_phone = 'phone';
    const field_prof_status = 'prof_status';
    const field_country = 'country';
    const field_region = 'region';
    const field_city = 'city';

    const group_ormco_stars = 'ormco_stars';
    const field_ormco_stars_notify_hash = 'ormco_stars_notify_hash';
    
    const group_uni_sender = 'user_uni_sender';
    const field_uni_sender_exported = 'user_uni_sender_exported';
    
    const group_address = 'user_address';
    const field_address_postal_code = 'user_address_postal_code';
    const field_address_country = 'user_address_country';
    const field_address_country_iso = 'user_address_country_iso';
    const field_address_region = 'user_address_region';
    const field_address_area = 'user_address_area';
    const field_address_city = 'user_address_city';
    const field_address_settlement = 'user_address_settlement';
    const field_address_street = 'user_address_street';
    const field_address_house = 'user_address_house';
    const field_address_corpus = 'user_address_corpus';
    const field_address_building = 'user_address_building';
    const field_address_liter = 'user_address_liter';
    const field_address_room = 'user_address_room';
    const field_address_flat = 'user_address_flat';
    const field_address_office = 'user_address_office';
    const field_address_raw = 'user_address_raw';
}