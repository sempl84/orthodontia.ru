<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class SiteContentPageSettingsModel
{
    const object_type_id = 162;
    
    const group_mail_templates = 'mail_templates';
    const field_mail_template_register_seminar_client = 'mail_register_seminar_client';
    
    public static function getSettingsObject()
    {
        return umiHierarchy::getInstance()->getElement(765);
    }
}