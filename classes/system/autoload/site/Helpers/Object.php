<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class ObjectHelper
{
    public static function isObjectOfType($objectId, $objectTypeGUID)
    {
        if (!is_numeric($objectId)) {
            return false;
        }

        $object = umiObjectsCollection::getInstance()->getObject($objectId);
        if (!$object instanceof umiObject) {
            return false;
        }

        return $object->getTypeGUID() == $objectTypeGUID;
    }

    public static function getObjectName($objectId = false)
    {
        if(!$objectId) {
            return '';
        }

        $object = umiObjectsCollection::getInstance()->getObject($objectId);
        if(!$object instanceof umiObject) {
            return '';
        }

        return $object->getName();
    }
}