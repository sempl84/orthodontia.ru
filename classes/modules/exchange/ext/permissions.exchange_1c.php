<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2023, Evgenii Ioffe
 */

$permissions = [
    'auto' => [
        'addOrdersCommerceMlLog'
    ],
];