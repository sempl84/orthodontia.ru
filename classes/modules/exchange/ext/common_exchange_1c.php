<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2023, Evgenii Ioffe
 */

class common_exchange_1c
{
    public function addOrdersCommerceMlLog($orderId = null, $type = null)
    {
        $orderId = intval($orderId);
        if ($orderId <= 0) {
            throw new publicException('Не передан id заказа');
        }

        switch ($type) {
            case 'prepared-old':
            {
                $message = 'Подготовлены данные для выгрузки commerceML';
                break;
            }
            case 'prepared-new':
            {
                $message = 'Подготовлены данные для выгрузки commerceMLNew';
                break;
            }
            default:
            {
                $message = false;
            }
        }

        if (!$message) {
            throw new publicException('Не указано сообщение');
        }

        SiteEmarketOrderModel::add1cLog($orderId, $message);
    }

    public function getExchangeOrderCommerceMlXml($orderId = null)
    {
        $orderId = intval($orderId);
        if($orderId <= 0) {
            throw new publicException('Не передан id заказа');
        }

        cmsController::getInstance()->getModule('emarket');

        $order = order::get($orderId);
        if(!$order instanceof order) {
            throw new publicException('Не найден заказ ' . $orderId);
        }

        $umiDump = new xmlExporter('CommerceML2');
        $umiDump->addObjects([$order->getObject()]);
        $umiDump->setIgnoreRelations();
        $umiDumpResult = $umiDump->execute();

        $domDocument = new DOMDocument("1.0", "utf-8");
        $domDocument->formatOutput = XML_FORMAT_OUTPUT;
        $domDocument->loadXML($umiDumpResult->saveXML());
        $template = umiTemplater::create('XSLT', './xsl/export/ordersCommerceML.xsl');
        $result = $template->parse($domDocument);

        $buffer = outputBuffer::current();
        $buffer->clear();
        $buffer->contentType('text/xml');
        $buffer->push($result);
        $buffer->send();
    }
}