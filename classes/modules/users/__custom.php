<?php
abstract class __custom_users {
    //Макрос используется для авторизации в мобильном приложении!
    public function login_do_my() {
        $res = "";
        $login = getRequest('login');
        $password = getRequest('password');
        $skin_sel = getRequest('skin_sel');

        $from_page = getRequest('from_page');

        if(strlen($login) == 0) {
            return 'NoLogin';
        }

        $permissions = permissionsCollection::getInstance();
        $cmsController = cmsController::getInstance();

        $user = $permissions->checkLogin($login, $password);

        if($user instanceof iUmiObject) {
            $permissions->loginAsUser($user);

            if ($permissions->isAdmin($user->id)) {
                $session = session::getInstance();
                $session->set('csrf_token', md5(rand() . microtime()));
                if ($permissions->isSv($user->id)) {
                    $session->set('user_is_sv', true);
                }
                $session->setValid();
            }

            $login = $user->getValue('login');

            session::commit();
            system_runSession();
            /*
                        $oEventPoint = new umiEventPoint("users_login_successfull");
                        $oEventPoint->setParam("user_id", $user->id);
                        $this->setEventPoint($oEventPoint); */

            if($cmsController->getCurrentMode() == "admin") {
                ulangStream::getLangPrefix();
                system_get_skinName();
                $this->chooseRedirect($from_page);
            } else {
                if(!$from_page) {
                    $from_page = getServer('HTTP_REFERER');
                }
                return 'Success';
            }

        } else {
            $users = new selector('objects');
            $users->types('object-type')->name('users', 'user');
            $users->where('login')->equals($login);
            $totaluser = $users->length;
            if ($totaluser > 0) {
                return 'ErrorPass';
            }
            else {
                return 'ErrorLogin';
            }

        }
    }


    //Макрос используется в мобильном приложении!
    public function registrate_do_my($template = "mobile") {
        if ($this->is_auth()) {
            return 'AuthAlredy'; //---------------------------------- Возвращаем Рег
        }
        if (!($template = getRequest('template'))) {
            $template = 'mobile';
        }
        $objectTypes = umiObjectTypesCollection::getInstance();
        $regedit = regedit::getInstance();

        $refererUrl = getServer('HTTP_REFERER');
        $without_act = (bool) $regedit->getVal("//modules/users/without_act");

        $objectTypeId = "52";
        if ($customObjectTypeId = getRequest('type-id')) {
            $childClasses = $objectTypes->getChildClasses($objectTypeId);
            if (in_array($customObjectTypeId, $childClasses)) {
                $objectTypeId = $customObjectTypeId;
            }
        }

        $login = getRequest('login');
        if (!$this->checkIsUniqueLogin($login, false)) {
            return 'LoginAlredy';
        }
    
        $this->sendCorsHeader();

        $password = getRequest('password');
        $email = getRequest('email');

        $oEventPoint = new umiEventPoint("users_registrate");
        $oEventPoint->setMode("before");
        $oEventPoint->setParam("login", $login);
        $oEventPoint->addRef("password", $password);
        $oEventPoint->addRef("email", $email);
        $this->setEventPoint($oEventPoint);

        //Creating user...
        $objectId = umiObjectsCollection::getInstance()->addObject($login, $objectTypeId);
        $activationCode = md5($login . time());

        $object = umiObjectsCollection::getInstance()->getObject($objectId);


        /*
                $userFirstName = getRequest('fname');
                $userLastName = getRequest('lname');
                $fatherName = getRequest('fathname');
                $phone = getRequest('tel'); */

        $object->setValue("login", $login);
        $object->setValue("password", md5($password));
        $object->setValue("e-mail", $email);
        /*
                $object->setValue("fname", $userFirstName);
                $object->setValue("lname", $userLastName);
                $object->setValue("father_name", $fatherName);
                $object->setValue("phone", $phone); */

        $object->setValue("is_activated", $without_act);
        $object->setValue("activate_code", $activationCode);
        $object->setValue("referer", urldecode(getSession("http_referer")));
        $object->setValue("target", urldecode(getSession("http_target")));
        $object->setValue("register_date", umiDate::getCurrentTimeStamp());
        $object->setOwnerId($objectId);

        if ($without_act) {
            $_SESSION['cms_login'] = $login;
            $_SESSION['cms_pass'] = md5($password);
            $_SESSION['user_id'] = $objectId;

            session_commit();
        }

        $group_id = regedit::getInstance()->getVal("//modules/users/def_group");
        $object->setValue("groups", Array($group_id));

        cmsController::getInstance()->getModule('data');
        $data_module = cmsController::getInstance()->getModule('data');
        $data_module->saveEditedObjectWithIgnorePermissions($objectId, true, true);

        $phone = trim(getRequest('phone'));
        $phone = str_replace("_", "", $phone);
        if($phone[0] != '7') {
            $phone = '7' . $phone;
        }
        $object->setValue("phone", '+'.$phone );
    
        if(OrthoSiteDataDadataHelper::isDadataEnabledForCountry(getArrayKey(getArrayKey(getRequest('data'), 'new'), 'country'))) {
            $data = json_decode(trim(getRequest('dadata')), true);
            
            OrthoSiteDataDadataHelper::validateData(trim(getRequest('address')), $data);
            OrthoSiteDataDadataHelper::setUserAddressData($object, $data);
        }
        
        $object->commit();



        //Forming mail...
        list(
            $template_mail, $template_mail_subject, $template_mail_noactivation, $template_mail_subject_noactivation
            ) = def_module::loadTemplatesForMail("users/register/".$template,
            "mail_registrated", "mail_registrated_subject", "mail_registrated_noactivation", "mail_registrated_subject_noactivation"
        );

        if ($without_act && $template_mail_noactivation && $template_mail_subject_noactivation) {
            $template_mail = $template_mail_noactivation;
            $template_mail_subject = $template_mail_subject_noactivation;
        }

        $mailData = array(
            'user_id' => $objectId,
            'domain' => $domain = cmsController::getInstance()->getCurrentDomain()->getCurrentHostName(),
            'activate_link' => "http://" . $domain . $this->pre_lang . "/users/activate/" . $activationCode . "/",
            'login' => $login,
            'password' => $password,
            'lname' => $object->getValue("lname"),
            'fname' => $object->getValue("fname"),
            'father_name' => $object->getValue("father_name"),
        );

        $mailContent = def_module::parseTemplateForMail($template_mail, $mailData, false, $objectId);
        $mailSubject = def_module::parseTemplateForMail($template_mail_subject, $mailData, false, $objectId);

        $fio = $object->getValue("lname") . " " . $object->getValue("fname") . " " . $object->getValue("father_name");

        $email_from = regedit::getInstance()->getVal("//settings/email_from");
        $fio_from = regedit::getInstance()->getVal("//settings/fio_from");


        $registrationMail = new umiMail();
        $registrationMail->addRecipient($email, $fio);
        $registrationMail->setFrom($email_from, $fio_from);
        $registrationMail->setSubject($mailSubject);
        $registrationMail->setContent($mailContent);
        $registrationMail->commit();
        $registrationMail->send();

        $oEventPoint = new umiEventPoint("users_registrate");
        $oEventPoint->setMode("after");
        $oEventPoint->setParam("user_id", $objectId);
        $oEventPoint->setParam("login", $login);
        $this->setEventPoint($oEventPoint);

        $line_arr = Array();
        $line_arr['attribute:result'] = "RegSuccess";
        $line_arr['attribute:id'] = $objectId;

        if ($without_act) {
            return $line_arr; //---------------------------------- Возвращаем Рег
        } else {
            return $line_arr; //---------------------------------- Возвращаем Рег
        }
    }

    public function registrate_do_code($currentUserId) {
        if (isset($currentUserId)) {
            $objects = umiObjectsCollection::getInstance();
            $userObject = $objects->getObject($currentUserId);
            $userObject->setValue("is_activated", true);
            $userObject->commit();

            // sync user to other site
            $this->exportNewUser($userObject);

            return 'Success';
        }
        else {
            return 'Error';
        }
    }

    public function settings_do_my($template = "default") {
        $this->sendCorsHeader();
        
        $object_id = getRequest('user');
        if (!$object_id) {
            return 'error';
        }
        $email = trim((string) getRequest('email'));

        $oEventPoint = new umiEventPoint("users_settings_do");
        $oEventPoint->setMode("before");
        $oEventPoint->setParam("user_id", $object_id);
        $this->setEventPoint($oEventPoint);

        $object = umiObjectsCollection::getInstance()->getObject($object_id);

        if ($email) {
            if (!preg_match("/^.+@\S+\.\S{2,}$/", $email)) {
                return 'ErrorMail';
            }

            if (!$this->checkIsUniqueEmail($email, $object_id)) {
                return 'ErrorMailExist';
            }

            $object->setValue("e-mail", $email);
        }

        $password = getRequest('password');
        if (isset($password)) {
            $object->setValue("password", md5($password));
        }

            /*
        $data_module = cmsController::getInstance()->getModule('data');
        $data_module->saveEditedObject($object_id);
            */
        $object->setValue("e-mail", $email);

        $data = 'data';
        $key = getRequest('user');
        $phone = trim(getRequest('phone'));
        $phone = str_replace("_", "", $phone);

        $object->setValue("lname", getRequest('lname'));
        $object->setValue("fname", getRequest('fname'));
        $object->setValue("father_name", getRequest('father_name'));
        $object->setValue("phone", '+'.$phone );
        $object->setValue("phone_office", getRequest('phone_office'));
        $object->setValue("region", getRequest('region'));
        $object->setValue("country", getRequest('country'));
        $object->setValue("prof_status", getRequest('prof_status'));
    
        if(OrthoSiteDataDadataHelper::isDadataEnabledForCountry($object->getValue('country'))) {
            $data = json_decode(trim(getRequest('dadata')), true);
        
            OrthoSiteDataDadataHelper::validateData(trim(getRequest('address')), $data);
            OrthoSiteDataDadataHelper::setUserAddressData($object, $data);
    
            $object->setValue("city", false);
        } else {
            $object->setValue("city", getRequest('city'));
            OrthoSiteDataDadataHelper::clearUserAddressData($object);
        }
        
        $object->commit();

        if ($eshop_module = cmsController::getInstance()->getModule('eshop')) {
            $eshop_module->discountCardSave($object_id);
        }

        $oEventPoint = new umiEventPoint("users_settings_do");
        $oEventPoint->setMode("after");
        $oEventPoint->setParam("user_id", $object_id);
        $this->setEventPoint($oEventPoint);


        return 'Success';
    }


    public function forget_do_mobile($template = "default") {
        static $macrosResult;
        if($macrosResult) return $macrosResult;

        $forget_login = (string) getRequest('forget_login');
        $forget_email = (string) getRequest('forget_email');

        $hasLogin = strlen($forget_login) != 0;
        $hasEmail = strlen($forget_email) != 0;

        $user_id = false;

        list($template_wrong_login_block, $template_forget_sended) = def_module::loadTemplates("users/forget/".$template, "wrong_login_block", "forget_sended");
        list($template_mail_verification, $template_mail_verification_subject) = def_module::loadTemplatesForMail("users/forget/".$template, "mail_verification", "mail_verification_subject");

        if ($hasLogin || $hasEmail) {
            $sel = new selector('objects');
            $sel->types('object-type')->name('users', 'user');
            if($hasLogin) $sel->where('login')->equals($forget_login);
            if($hasEmail) $sel->where('e-mail')->equals($forget_email);
            $sel->limit(0, 1);

            $user_id = ($sel->first) ? $sel->first->id : false;
        }
        else $user_id = false;

        if ($user_id) {
            $activate_code = md5(self::getRandomPassword());

            $object = umiObjectsCollection::getInstance()->getObject($user_id);

            $regedit = regedit::getInstance();
            $without_act = (bool) $regedit->getVal("//modules/users/without_act");
            if ($without_act || intval($object->getValue('is_activated'))) {
                $object->setValue("activate_code", $activate_code);
                $object->commit();

                $email = $object->getValue("e-mail");
                $fio = $object->getValue("lname") . " " . $object->getValue("fname") . " " . $object->getValue("father_name");

                $email_from = regedit::getInstance()->getVal("//settings/email_from");
                $fio_from = regedit::getInstance()->getVal("//settings/fio_from");

                $mail_arr = Array();
                $mail_arr['domain'] = $domain = $_SERVER['HTTP_HOST'];
                $mail_arr['restore_link'] = getSelectedServerProtocol() . "://" . $domain . $this->pre_lang . "/users/restore/" . $activate_code . "/";
                $mail_arr['login'] = $object->getValue('login');
                $mail_arr['email'] = $object->getValue('e-mail');

                $mail_subject = def_module::parseTemplateForMail($template_mail_verification_subject, $mail_arr, false, $user_id);
                $mail_content = def_module::parseTemplateForMail($template_mail_verification, $mail_arr, false, $user_id);

                $someMail = new umiMail();
                $someMail->addRecipient($email, $fio);
                $someMail->setFrom($email_from, $fio_from);
                $someMail->setSubject($mail_subject);
                $someMail->setPriorityLevel("highest");
                $someMail->setContent($mail_content);
                $someMail->commit();
                $someMail->send();

                $oEventPoint = new umiEventPoint("users_restore_password");
                $oEventPoint->setParam("user_id", $user_id);
                $this->setEventPoint($oEventPoint);

                $block_arr = Array();
                $block_arr['attribute:status'] = "success";
                return "success";
            } else {

                $block_arr = Array();
                $block_arr['attribute:status'] = "fail";
                $block_arr['forget_login'] = $forget_login;
                $block_arr['forget_email'] = $forget_email;
                return "nonactivated_login";
            }
        } else {
            $block_arr = Array();
            $block_arr['attribute:status'] = "fail";
            $block_arr['forget_login'] = $forget_login;
            $block_arr['forget_email'] = $forget_email;
            return "wrong_login";
        }

    }

    public static function getRandomPassword ($length = 12) {
        if(function_exists('openssl_random_pseudo_bytes')) {
            $password = base64_encode(openssl_random_pseudo_bytes($length, $strong));
            if($strong == TRUE)
                return substr($password, 0, $length);
        }
        $avLetters = "$#@^&!1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
        $size = strlen($avLetters);

        $npass = "";
        for($i = 0; $i < $length; $i++) {
            $c = rand(0, $size - 1);
            $npass .= $avLetters[$c];
        }
        return $npass;
    }
    
    public function sendCorsHeader() {
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');    // cache for 1 day
        }
    
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        
            exit(0);
        }
    }
};