<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2022, Evgenii Ioffe
 */

class site_users_personal extends def_module
{
    public function address()
    {
        if (!OrthoSiteDataDadataHelper::isDadataEnabled()) {
            $this->redirect('/users/settings/');
        }
        
        $action = getRequest('param0');
        if ($action) {
            switch ($action) {
                case 'do':
                {
                    try {
                        $this->_saveAddress();
                        $this->redirect('/users/settings/?address=ok');
                    } catch (Exception $e) {
                        $this->errorSetErrorPage('/users/address/');
                        $this->errorAddErrors($e);
                        $this->errorThrow('public');
                    }
                    break;
                }
                default:
                {
                    $this->redirect('/users/address/');
                }
            }
        }
    }
    
    public function _saveAddress()
    {
        $user = umiObjectsCollection::getInstance()->getObject(permissionsCollection::getInstance()->getUserId());
        if (!$user instanceof umiObject || $user->getId() == permissionsCollection::getGuestId()) {
            throw new publicException('Не найден объект пользователя');
        }
        
        $country = getRequest(SiteUsersUserModel::field_country);
        
        if (OrthoSiteDataDadataHelper::isDadataEnabledForCountry($country)) {
            $dadataData = json_decode(trim(getRequest('dadata')), true);
            
            OrthoSiteDataDadataHelper::validateData(trim(getRequest('address')), $dadataData);
            OrthoSiteDataDadataHelper::setUserAddressData($user, $dadataData);
            
            $user->setValue(SiteUsersUserModel::field_city, false);
        } else {
            $user->setValue(SiteUsersUserModel::field_city, getRequest(SiteUsersUserModel::field_city));
            OrthoSiteDataDadataHelper::clearUserAddressData($user);
        }
        
        $user->setValue(SiteUsersUserModel::field_country, $country);
        
        $user->commit();
        
        $this->exportModifyUser($user);
    }
    
    public function getUserAddress()
    {
        $userId = permissionsCollection::getInstance()->getUserId();
        
        if ($userId == permissionsCollection::getGuestId()) {
            return null;
        }
        
        return $this->_getUserAddress($userId);
    }
    
    public function _getUserAddress($userId = null)
    {
        $user = umiObjectsCollection::getInstance()->getObject($userId);
        if (!$user instanceof umiObject) {
            throw new publicException('Не найден объект пользователя ' . $userId);
        }
    
        $countryId = $user->getValue(SiteUsersUserModel::field_country);
    
        $address = '';
    
        if (OrthoSiteDataDadataHelper::isDadataEnabledForCountry($countryId)) {
            $address = $user->getValue(SiteUsersUserModel::field_address_raw);
        }
    
        if (!$address) {
            if ($country = ObjectHelper::getObjectName($countryId)) {
                $address = $country;
            }
        
            if ($city = trim($user->getValue(SiteUsersUserModel::field_city))) {
                $address = ($address) ? $address . ', ' . $city : $city;
            }
        }
        
        return $address;
    }
}