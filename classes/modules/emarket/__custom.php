<?php
abstract class __emarket_custom {

    public function eventsl($template = 'default', $sort = "asc", $old = false, $customerId = false) {
        list($tplBlock, $tplBlockEmpty, $tplItem) = def_module::loadTemplates(
            "emarket/" . $template,
            'orders_block',
            'orders_block_empty',
            'orders_item'
        );

        if ($customerId == false) {
            return ('error');
        }

        $domainId = cmsController::getInstance()->getCurrentDomain()->getId();

        $select = new selector('objects');
        $select->types('object-type')->name('emarket', 'order');
        $select->where('customer_id')->equals($customerId);
        $select->where('name')->isNull(false);
        $select->where('domain_id')->equals($domainId);
        $select->option('no-length')->value(true);
        $select->option('load-all-props')->value(true);
        $total = $select->length;

        if(in_array($sort, array("desc"))) {
            call_user_func(array($select->order('id'), $sort));
        }

        if(!$select->first) {
            $tplBlock = $tplBlockEmpty;
        }
        $umiHierarchy = umiHierarchy::getInstance();
        $umiLinksHelper = umiLinksHelper::getInstance();
        $umiObjects    = umiObjectsCollection::getInstance();

        $itemsArray = array();
        foreach($select->result as $order) {
            $items = array();

            $orderObject = order::get($order->id);
            //информация о мероприятии
            foreach ($orderObject->getItems() as $orderItem) {

                $orderItemElementId = $orderItem->getItemElement()->id;
                $element = $umiHierarchy -> getElement($orderItemElementId);
                if ($element instanceof umiHierarchyElement) {
                    $publish_date = ($element->publish_date) ? $element->publish_date->getFormattedDate("U") : '';

                    $itemDay = date('d', $publish_date);
                    $itemMonth = date('n', $publish_date);
                    $itemYear = date('Y', $publish_date);
                    $itemTime = strtotime("$itemDay-$itemMonth-$itemYear");

                    $currDay = date('d');
                    $currMonth = date('n');
                    $currYear = date('Y');
                    $currTime = strtotime("$currDay-$currMonth-$currYear");


                    if($old && $currTime < $itemTime){
                        continue;
                    }elseif(!$old && $currTime > $itemTime){
                        continue(2);
                    }

                    $item = array();
                    $elementId = $element->getId();
                    $item['attribute:id'] = $elementId;
                    $item['names'] = $element->getName();
                    $item['price'] = $element->getValue('price');
                    $item['price_origin'] = $element->getValue('price_origin');
                    $id = $element->id;
                    $item['pricedis'] = $this->showDiscountByDate($id);
                    $elh = $element->getValue('photo');
                    $item['names'] = $element->getName();
                    $date_begin = $element->getValue("publish_date")->getFormattedDate(U);
                    $date_end = $element->getValue("finish_date")->getFormattedDate(U);
                    $item['dateru'] = $this->dateruPeriod($date_begin , $date_end);
                    $max_reserv = $element->getValue("max_reserv");
                    $curr_reserv = $element->getValue("curr_reserv");
                    $item['reserv'] = $max_reserv - $curr_reserv;

                    $idauto = $element->getValue('speaker');
                    foreach($idauto as $Id) {
                        $objects = umiObjectsCollection::getInstance();
                        $autoObject = $objects->getObject($Id);
                        $item['speaker'] = $autoObject->name;
                        $item['speakerid'] = $autoObject->id;
                        $item['speakerdescr'] = $autoObject->getValue('full_desrc');
                        $item['speakerdescr2'] = $autoObject->getValue('full_descr2');
                        $elh = $autoObject->getValue("photo");
                    }

                    $gorod_in = $element->getValue('gorod_in');
                    $objects = umiObjectsCollection::getInstance();
                    $gorod_inObject = $objects->getObject($gorod_in);
                    $item['gorodin'] = $gorod_inObject->name;
                    $item['gorodinid'] = $gorod_in;
                    $item['mestoprovedeniya'] = $element->getValue("mesto_provedeniya");

                    $event_type = $element->getValue('event_type');
                    $objects = umiObjectsCollection::getInstance();
                    $event_typeObject = $objects->getObject($event_type);
                    $item['eventtype'] = $event_typeObject->name;
                    $item['eventtypeid'] = $event_type;

                    $organizer = $element->getValue('organizer');
                    $organizerObject = $objects->getObject($organizer);
                    $item['organizer'] = $organizerObject->name;
                    $item['organizerimg'] = $organizerObject->getValue("logo");
                    $item['organizerid'] = $organizer;

                    $level = $element->getValue('level');
                    $levelObject = $objects->getObject($level);
                    $item['level'] = $levelObject->name;
                    $item['levelid'] = $level;

                    $level_shoo = $element->getValue('level_shoo');
                    $level_shooObject = $objects->getObject($level_shoo);
                    $item['levelshoo'] = $level_shooObject->name;
                    $item['levelshooid'] = $level_shoo;

                    $item['description'] = $element->getValue("description");
                    $item['aboutreg'] = $element->getValue("about_reg");
                    $item['id'] = $element->id;

                    if ($elh) {
                        $imgsrc = $elh->getFilePath();
                        $sourcthmb = $this->makeThumbnailFullmy($imgsrc, '350', '350');
                        $item['thumb'] = $sourcthmb;
                    }
                    else {
                        $item['thumb'] = "/images/nofoto_260_auto_5_80.jpg";
                    }

                    $items[] = def_module::parseTemplate('', $item, $elementId);
                    def_module::pushEditable('catalog', 'object', $elementId);
                    $umiHierarchy->unloadElement($elementId);

                    $lines['nodes:item'][] = $item;
                }

            }

            try {



                $payment = payment::get($order->payment_id, $orderObject);

                if($payment){
                    $paymentName 	= $payment->name;
                    $paymentStatus  = order::getCodeByStatus($orderObject->getPaymentStatus());

                    $paymentObject = $payment->getObject();
                    $paymentTypeId = $paymentObject->getValue('payment_type_id');
                    $paymentTypeName = $umiObjects->getObject($paymentTypeId)->getValue('class_name');
                }


            } catch(coreException $e) {
                $paymentName 	= "";
                $paymentStatus 	= "";
                $paymentTypeName 	= "";
            }

            $order_item = array(
                'attribute:id' => $order->id,
                'attribute:name' => $order->name,
                'attribute:type-id' => $order->typeId,
                'attribute:guid' => $order->GUID,
                'attribute:type-guid' => $order->typeGUID,
                'attribute:ownerId' => $order->ownerId,
                'attribute:paymentName' => $paymentName,
                'attribute:paymentStatus' => $paymentStatus,
                'attribute:paymentTypeName' => $paymentTypeName,
                'xlink:href' => $order->xlink,
                'subnodes:lines' => $items
            );

            $block_arr['lines'] = $lines;
            $block_arr['total'] = $total;

            $itemsArray[] = def_module::parseTemplate($tplItem, $order_item, false, $order->id);
        }

        return $block_arr;
    }

    function makeThumbnailFullmy($path, $width, $height, $crop = true, $cropside = 5, $isLogo = false, $quality = 50) {

        $isSharpen=true;
        $thumbs_path="./images/cms/thumbs/";
        $image = new umiImageFile($path);
        $file_name = $image->getFileName();
        $file_ext = strtolower($image->getExt());
        $file_ext = ($file_ext=='bmp'?'jpg':$file_ext);

        $allowedExts = Array('gif', 'jpeg', 'jpg', 'png', 'bmp');
        if(!in_array($file_ext, $allowedExts)) return "";

        $file_name = substr($file_name, 0, (strlen($file_name) - (strlen($file_ext) + 1)) );

        $thumbPath = sha1($image->getDirName());

        if (!is_dir($thumbs_path . $thumbPath)) {
            mkdir($thumbs_path . $thumbPath, 0755, true);
        }

        $file_name_new = $file_name . '_' . $width . '_' . $height . '_' . $cropside . '_' . $quality . "." . $file_ext;
        $path_new = $thumbs_path . $thumbPath . '/' . $file_name_new;

        if(!file_exists($path_new) || filemtime($path_new) < filemtime($path)) {
            if(file_exists($path_new)) {
                unlink($path_new);
            }

            $width_src = $image->getWidth();
            $height_src = $image->getHeight();

            if(!($width_src && $height_src)) {
                throw new coreException(getLabel('error-image-corrupted', null, $path));
            }

            if($height == "auto") {
                $real_height = (int) round($height_src * ($width / $width_src));
                //change
                $height=$real_height;
                $real_width = (int) $width;
            } else {
                if($width == "auto") {
                    $real_width = (int) round($width_src * ($height / $height_src));
                    //change
                    $width=$real_width;
                } else {
                    $real_width = (int) $width;
                }

                $real_height = (int) $height;
            }

            $offset_h=0;
            $offset_w=0;

            // realloc: devision by zero fix
            if (!intval($width) || !intval($height)) {
                $crop = false;
            }

            if ($crop){
                $width_ratio = $width_src/$width;
                $height_ratio = $height_src/$height;

                if ($width_ratio > $height_ratio){
                    $offset_w = round(($width_src-$width*$height_ratio)/2);
                    $width_src = round($width*$height_ratio);
                } elseif ($width_ratio < $height_ratio){
                    $offset_h = round(($height_src-$height*$width_ratio)/2);
                    $height_src = round($height*$width_ratio);
                }


                if($cropside) {
                    //defore all it was cropside work like as - 5
                    //123
                    //456
                    //789
                    switch ($cropside):
                        case 1:
                            $offset_w = 0;
                            $offset_h = 0;
                            break;
                        case 2:
                            $offset_h = 0;
                            break;
                        case 3:
                            $offset_w += $offset_w;
                            $offset_h = 0;
                            break;
                        case 4:
                            $offset_w = 0;
                            break;
                        case 5:
                            break;
                        case 6:
                            $offset_w += $offset_w;
                            break;
                        case 7:
                            $offset_w = 0;
                            $offset_h += $offset_h;
                            break;
                        case 8:
                            $offset_h += $offset_h;
                            break;
                        case 9:
                            $offset_w += $offset_w;
                            $offset_h += $offset_h;
                            break;
                    endswitch;
                }
            }

            $thumb = imagecreatetruecolor($real_width, $real_height);

            $source_array = $image->createImage($path);
            $source = $source_array['im'];

            if ($width*4 < $width_src && $height*4 < $height_src) {
                $_TMP=array();
                $_TMP['width'] = round($width*4);
                $_TMP['height'] = round($height*4);

                $_TMP['image'] = imagecreatetruecolor($_TMP['width'], $_TMP['height']);

                if ($file_ext == 'gif') {
                    $_TMP['image_white'] = imagecolorallocate($_TMP['image'], 255, 255, 255);
                    imagefill($_TMP['image'], 0, 0, $_TMP['image_white']);
                    imagecolortransparent($_TMP['image'], $_TMP['image_white']);
                    imagealphablending($source, TRUE);
                    imagealphablending($_TMP['image'], TRUE);
                } else {
                    imagealphablending($_TMP['image'], false);
                    imagesavealpha($_TMP['image'], true);
                }
                imagecopyresampled($_TMP['image'], $source, 0, 0, $offset_w, $offset_h, $_TMP['width'], $_TMP['height'], $width_src, $height_src);

                imageDestroy($source);

                $source = $_TMP['image'];
                $width_src = $_TMP['width'];
                $height_src = $_TMP['height'];

                $offset_w = 0;
                $offset_h = 0;
                unset($_TMP);
            }

            if ($file_ext == 'gif') {
                $thumb_white_color = imagecolorallocate($thumb, 255, 255, 255);
                imagefill($thumb, 0, 0, $thumb_white_color);
                imagecolortransparent($thumb, $thumb_white_color);
                imagealphablending($source, TRUE);
                imagealphablending($thumb, TRUE);
            } else {
                imagealphablending($thumb, false);
                imagesavealpha($thumb, true);
            }

            imagecopyresampled($thumb, $source, 0, 0, $offset_w, $offset_h, $width, $height, $width_src, $height_src);
            if($isSharpen) $thumb = makeThumbnailFullUnsharpMask($thumb,80,.5,3);

            switch($file_ext) {
                case 'gif':
                    $res = imagegif($thumb, $path_new);
                    break;
                case 'png':
                    $res = imagepng($thumb, $path_new);
                    break;
                default:
                    $res = imagejpeg($thumb, $path_new, $quality);
            }
            if(!$res) {
                throw new coreException(getLabel('label-errors-16008'));
            }

            imageDestroy($source);
            imageDestroy($thumb);

            if($isLogo) {
                umiImageFile::addWatermark($path_new);
            }
        }

        $value = new umiImageFile($path_new);

        $arr = $value->getFilePath(true);
        return $arr;
    }

    public function getMonthRu($month, $year, $type=0)
    {
        // Проверка существования месяца
        if (!checkdate($month, 1, $year)) {
            throw new publicException("Проверьте порядок ввода даты.");
        }
        $months_ru = Array(1 => 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
        $months_ru1 = Array(1 => 'январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь');
        if($type==1){return $months_ru1[$month];}
        return $months_ru[$month];
    }

    public function dateru($time) {
        if(!$time) return;
        $day = date('j', $time);
        $month = date('n', $time);
        $year = date('Y', $time);
        $hour = date('g', $time);
        $minutes = date('i', $time);
        $seconds = date('s', $time);

        $temp = Array(
            'month' => $this->getMonthRu($month, $year),
            'day' => $day,
            'year' => $year,
            'hour' => $hour,
            'minutes' => $minutes,
            'seconds' => $seconds
        );
        return def_module::parseTemplate('', $temp);
    }
    // test data:
    // 1352204856 Tue, 06 Nov 2012 12:27:36 GMT
    // 1352377656 Thu, 08 Nov 2012 12:27:36 GMT
    // 1355660856 Sun, 16 Dec 2012 12:27:36 GMT
    // 1357475256 Sun, 06 Jan 2013 12:27:36 GMT
    // 1383740856 Wed, 06 Nov 2013 12:27:36 GMT
    public function dateruPeriod($timeBegin=NULL, $timeEnd=NULL) {
        if(!$timeBegin || !$timeEnd) return "-";
        if(!is_numeric($timeBegin) || !is_numeric($timeEnd)) return "=";
        if($timeEnd < $timeBegin) {
            $tmp = $timeEnd;
            $timeEnd = $timeBegin;
            $timeBegin = $tmp;
        }

        $dayBegin = date('d', $timeBegin);
        $monthBegin = date('n', $timeBegin);
        $yearBegin = date('Y', $timeBegin);
        $dayEnd = date('d', $timeEnd);
        $monthEnd = date('n', $timeEnd);
        $yearEnd = date('Y', $timeEnd);

        $timeEnd_forCalc = strtotime("$dayEnd-$monthEnd-$yearEnd ");
        $timeBegin_forCalc = strtotime("$dayBegin-$monthBegin-$yearBegin ");

        $yearCurrent = date('Y');

        if(($monthBegin != $monthEnd) || ($yearBegin != $yearEnd)) {
            $firstLine = $dayBegin.' '.$this->getMonthRu($monthBegin, $yearBegin).' ';
            $secondLine = $dayEnd.' '.$this->getMonthRu($monthEnd, $yearEnd);
            /* if($yearBegin != $yearEnd) {
                $secondLine .= ' '.$yearEnd.' г.';
            } */
            $firstLine .= '-';
        } else {
            $firstLine = ($dayBegin!=$dayEnd) ? $dayBegin.'-'.$dayEnd : $dayBegin;
            $secondLine = $this->getMonthRu($monthBegin, $yearBegin);
        }

        //if(($yearEnd != $yearCurrent) && ($yearBegin == $yearEnd)) {
        $secondLine .= ' '.$yearEnd.' г.';
        //}
        $days = (int)(($timeEnd_forCalc - $timeBegin_forCalc) / 86400) +1; // секунд в 24 часах
        $temp = Array(
            'firstline' => $firstLine,
            'secondline' => $secondLine,
            'days' => $days.' '.$this->wordDays($days)
        );
        return $temp;
    }

    public function wordDays($days) {
        $units = $days - (int)($days / 10) * 10;
        $tens = $days - (int)($days / 100) * 100 - $units;
        if($tens == 10) return 'дней'; // 11-19 дней

        if($units == 1) return 'день'; // 1 21 31... день
        if(($units > 1) && ($units < 5)) return 'дня'; // 2 3 4 22 23 24 дня
        return 'дней'; // 5 6 7 8 9 20 дней
    }

    public function legalMobileAdd($template = 'default') {
        $collection = umiObjectsCollection::getInstance();
        $types = umiObjectTypesCollection::getInstance();
        $typeId = $types -> getBaseType("emarket", "legal_person");
        $customerId = getRequest('customerId');
        $customer = customer::get(false, $customerId);

        $personId = getRequest('legalperson');
        //$personId = getRequest('param0');
        $isNew = ($personId == null || $personId == 'new');
        if ($isNew) {
            $typeId = $types -> getBaseType("emarket", "legal_person");
            $customerId = getRequest('customerId');
            $customer = customer::get(false, $customerId);
            $personId = $collection -> addObject("", $typeId);
            $customer -> legal_persons = array_merge($customer -> legal_persons, array($personId));
        }
        $controller = cmsController::getInstance();
        $data = getRequest('data');
        if ($data && $dataModule = $controller -> getModule("data")) {
            $key = $isNew ? 'new' : $personId;
            $person = $collection -> getObject($personId);

            $person -> setName($data[$key]['name']);
            $person -> setValue("fio_rukovoditelya", $data[$key]['fio_rukovoditelya']);
            $person -> setValue("dolzhnost", $data[$key]['dolzhnost']);
            $person -> setValue("contact_person", $data[$key]['contact_person']);
            $person -> setValue("email", $data[$key]['email']);
            $person -> setValue("phone_number", $data[$key]['phone_number']);
            $person -> setValue("fax", $data[$key]['fax']);
            $person -> setValue("inn", $data[$key]['inn']);
            $person -> setValue("kpp", $data[$key]['kpp']);
            $person -> setValue("account", $data[$key]['account']);
            $person -> setValue("bank_account", $data[$key]['bank_account']);
            $person -> setValue("ogrn", $data[$key]['ogrn']);
            $person -> setValue("legal_address", $data[$key]['legal_address']);
            $person -> setValue("defacto_address", $data[$key]['defacto_address']);
            $person -> setValue("post_address", $data[$key]['post_address']);
            $person->commit();

            //$dataModule -> saveEditedObject($personId, $isNew, false);

        }
        return 'Success';
    }


    public function legalMobilelList($customerId) {
        /* list($tpl_block, $tpl_item) = def_module::loadTemplates("./tpls/emarket/payment/invoice/{$template}.tpl",
         'legal_person_block', 'legal_person_item'); */
        $tpl_block = $tpl_item = '';
        $collection = umiObjectsCollection::getInstance();
        $types = umiObjectTypesCollection::getInstance();
        $typeId = $types -> getBaseType("emarket", "legal_person");
        $customer = customer::get(false, $customerId);

        //var_dump($customer);
        //exit('aa');

        $order = $this -> order;
        $mode = getRequest('param2');

        $items = array();
        $persons = $customer -> legal_persons;
        if (is_array($persons))
            foreach ($persons as $personId) {
                $person = $collection -> getObject($personId);
                $item_arr = array(
                    'attribute:id' => $personId,
                    'attribute:name' => $person -> name,
                    'attribute:fio_rukovoditelya' => $person -> fio_rukovoditelya,
                    'attribute:dolzhnost' => $person -> dolzhnost,
                    'attribute:contact_person' => $person -> contact_person,
                    'attribute:email' => $person -> email,
                    'attribute:phone_number' => $person -> phone_number,
                    'attribute:fax' => $person -> fax,
                    'attribute:inn' => $person -> inn,
                    'attribute:kpp' => $person -> kpp,
                    'attribute:account' => $person -> account,
                    'attribute:bik' => $person -> bik,
                    'attribute:ogrn' => $person -> ogrn,
                    'attribute:bank_account' => $person -> bank_account,
                    'attribute:legal_address' => $person -> legal_address,
                    'attribute:defacto_address' => $person -> defacto_address,
                    'attribute:post_address' => $person -> post_address,
                );
                $items[] = def_module::parseTemplate($tpl_item, $item_arr, false, $personId);
            }
        if ($tpl_block) {
            return def_module::parseTemplate($tpl_block, array('items' => $items, 'type_id' => $typeId));
        } else {
            return array('attribute:type-id' => $typeId, 'xlink:href' => 'udata://data/getCreateForm/' . $typeId, 'items' => array('nodes:item' => $items));
        }
    }

};
?>