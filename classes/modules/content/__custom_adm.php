<?php
	abstract class __content_custom_admin {
		//TODO: Write here your own macroses (admin mode)
		
		/*переопределяем вывод списка страниц в структуре указав большой per_page = 100*/
		public function load_tree_node() {
			$this->setDataType("list");
			$this->setActionType("view");

			$limit = 100;//getRequest('per_page_limit');
			$curr_page = getRequest('p');
			$offset = $curr_page * $limit;

			list($rel) = getRequest('rel');
			$sel = new selector('pages');
			if ($rel !== 0) {
				$sel->limit($offset, $limit);
			}
			selectorHelper::detectFilters($sel);

			$result = $sel->result;
			$length = $sel->length;
			$templatesData = getRequest('templates');

			if ($templatesData) {
				$templatesList = explode(',', $templatesData);
				$result = $this->getPagesByTemplatesIdList($templatesList, $limit, $offset);
				$length = $this->getTotalPagesByTemplates($templatesList);
			}

			$data = $this->prepareData($result, "pages");
			$this->setData($data, $length);
			$this->setDataRange($limit, $offset);

			if ($rel != 0) {
				$this->setDataRangeByPerPage($limit, $curr_page);
			}
			return $this->doData();
		}
	};
?>