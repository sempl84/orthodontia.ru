<?php

$INFO = Array();

$INFO['name'] = "speakers";
$INFO['filename'] = "modules/speakers/class.php";
$INFO['config'] = "1";
$INFO['default_method'] = "listElements";
$INFO['default_method_admin'] = "lists";
$INFO['is_indexed'] = "1";
$INFO['per_page'] = "10";

$INFO['func_perms'] = "";

$COMPONENTS = array();

$COMPONENTS[0] = "./classes/modules/speakers/__admin.php";
$COMPONENTS[1] = "./classes/modules/speakers/class.php";
$COMPONENTS[2] = "./classes/modules/speakers/i18n.php";
$COMPONENTS[3] = "./classes/modules/speakers/lang.php";
$COMPONENTS[4] = "./classes/modules/speakers/permissions.php";
?>