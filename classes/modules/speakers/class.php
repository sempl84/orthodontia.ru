<?php

class speakers extends def_module {
	public $per_page;

	public function __construct() {
		parent::__construct();

		if (cmsController::getInstance() -> getCurrentMode() == "admin") {

			$configTabs = $this -> getConfigTabs();
			if ($configTabs) {
				$configTabs -> add("config");
			}

			$this -> __loadLib("__admin.php");
			$this -> __implement("__speakers");

		} else {

			$this -> per_page = regedit::getInstance() -> getVal("//modules/speakers/per_page");
		}

	}

	public function groupelements($path = "", $template = "default") {
		if ($this -> breakMe())
			return;
		$element_id = cmsController::getInstance() -> getCurrentElementId();

		templater::pushEditable("speakers", "groupelements", $element_id);
		return $this -> group($element_id, $template) . $this -> listElements($element_id, $template);
	}

	public function item_element() {
		if ($this -> breakMe())
			return;
		$element_id = (int)cmsController::getInstance() -> getCurrentElementId();
		return $this -> view($element_id);
	}

	public function group($elementPath = "", $template = "default", $per_page = false) {
		if ($this -> breakMe())
			return;
		$hierarchy = umiHierarchy::getInstance();
		list($template_block) = def_module::loadTemplates("tpls/speakers/{$template}.tpl", "group");

		$elementId = $this -> analyzeRequiredPath($elementPath);

		$element = $hierarchy -> getElement($elementId);

		templater::pushEditable("speakers", "groupelements", $element -> id);
		return self::parseTemplate($template_block, array('id' => $element -> id), $element -> id);

	}

	public function view($elementPath = "", $template = "default") {
		if ($this -> breakMe())
			return;
		$hierarchy = umiHierarchy::getInstance();
		list($template_block) = def_module::loadTemplates("tpls/speakers/{$template}.tpl", "view");

		$elementId = $this -> analyzeRequiredPath($elementPath);
		$element = $hierarchy -> getElement($elementId);

		templater::pushEditable("speakers", "item_element", $element -> id);
		return self::parseTemplate($template_block, array('id' => $element -> id), $element -> id);
	}

	public function listGroup($elementPath, $template = "default", $per_page = false, $ignore_paging = false) {
		// Код метода
	}

    public function listElements()
    {
        $pages = new selector('pages');
        $pages->types('hierarchy-type')->name('speakers', 'item_element');
        $pages->where('hierarchy')->page(237)->childs(1);
        selectorHelper::detectFilters($pages);
        $pages->order('name')->asc();
        $pages->option('load-all-props')->value(true);

        $limit = intval($this->per_page);
        $pages->limit(intval(getRequest('p')) * $limit, $limit);

        $total = $pages->length();
        if(!$total) {
            return null;
        }

        $umiLinksHelper = umiLinksHelper::getInstance();
        $umiHierarchy = umiHierarchy::getInstance();

        $items = [];
        foreach ($pages as $element) {
            if (!$element instanceof umiHierarchyElement) {
                continue;
            }

            $item = [
                'attribute:id' => $element->getId(),
                'attribute:object-id' => $element->getObjectId(),
                'attribute:link' => $umiLinksHelper->getLinkByParts($element),
                'xlink:href' => 'upage://' . $element->getId(),
                'node:name' => $element->getName(),
            ];

            $items[] = self::parseTemplate('', $item, $element->getId());
            $umiHierarchy->unloadElement($element->getId());
        }

        return $this->parseTemplate('', [
            'subnodes:items' => $items,
            'total' => $total,
            'per_page' => $limit
        ]);
    }
	

	public function config() {
		return __speakers::config();
	}

	public function getEditLink($element_id, $element_type) {
		switch($element_type) {
			case "groupelements": {
				$link_add = $this->pre_lang . "/admin/speakers/add/{$element_id}/item_element/";
				$link_edit = $this->pre_lang . "/admin/speakers/edit/{$element_id}/";

				return Array($link_add, $link_edit);
				break;
			}

			case "item_element": {
				$link_edit = $this->pre_lang . "/admin/speakers/edit/{$element_id}/";

				return Array(false, $link_edit);
				break;
			}

			default: {
				return false;
			}
		}	
			
		
	}

};
?>