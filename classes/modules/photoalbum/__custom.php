<?php
	abstract class __custom_photoalbum {
		//TODO: Write here your own macroses
		
		/*клон с измененой сортировкой (сортировка по дате)*/
		public function albumsCust($template = "default", $limit = false, $ignore_paging = false, $parentElementId = false, $order = 'asc', $ignoreFilter = false) {
			//exit('hhh');	
			list(
				$template_block, $template_block_empty, $template_line
			) = def_module::loadTemplates("photoalbum/".$template,
				"albums_list_block", "albums_list_block_empty", "albums_list_block_line"
			);
			$block_arr = Array();

			$curr_page = (int) getRequest('p');
			if($ignore_paging) $curr_page = 0;
			$offset = $limit * $curr_page;

			$sel = new selector('pages');
			$sel->types('object-type')->name('photoalbum', 'album');
			$sel->where('permissions');

			$parentElementId = (int) $parentElementId;
			if($parentElementId) {
				$sel->where('hierarchy')->page($parentElementId)->childs(1);
			}
			
			if(!$ignoreFilter) selectorHelper::detectFilters($sel);
			$sel->order('publish_time')->desc();
			$sel->option('load-all-props')->value(true);
			$sel->limit($offset, $limit);
			$result = $sel->result();
			$total = $sel->length();

			$lines = Array();
			if($total > 0) {
				foreach($result as $element) {
					$line_arr = Array();
					if (!$element instanceof umiHierarchyElement) {
						continue;
					}
					$element_id = $element->getId();
					$line_arr['attribute:id'] = $element_id;
					$line_arr['attribute:link'] = umiLinksHelper::getInstance()->getLinkByParts($element);
					$line_arr['xlink:href'] = "upage://" . $element_id;
					$line_arr['node:name'] = $element->getName();

					$this->pushEditable("photoalbum", "album", $element_id);
					$lines[] = def_module::parseTemplate($template_line, $line_arr, $element_id);
				}
			} else {
				return def_module::parseTemplate($template_block_empty, $block_arr);
			}

			$block_arr['subnodes:items'] = $block_arr['void:lines'] = $lines;
			$block_arr['total'] = $total;
			$block_arr['per_page'] = $limit;
			return def_module::parseTemplate($template_block, $block_arr);
		}
	};
?>