<?php
 
$i18n = Array(
"label-add-list" => "Добавить группу",
"label-add-item" => "Добавить страницу",
 
"header-feedbacks-lists" => "Группы и страницы",
"header-feedbacks-config" => "Настройки модуля",
"header-feedbacks-add" => "Добавление",
"header-feedbacks-edit" => "Редактирование",
 
"header-feedbacks-add-groupelements"   => "Добавление группы",
"header-feedbacks-add-item_element"     => "Добавление страницы",
 
"header-feedbacks-edit-groupelements"   => "Редактирование группы",
"header-feedbacks-edit-item_element"     => "Редактирование страницы",
 
"header-feedbacks-activity" => "Изменение активности",
 
'perms-feedbacks-view' => 'Просмотр страниц',
'perms-feedbacks-lists' => 'Управление страницами',

'module-feedbacks' => 'Отзывы',
 
);
 
?>