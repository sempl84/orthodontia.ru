<?php

$INFO = Array();

$INFO['name'] = "feedbacks";
$INFO['filename'] = "modules/feedbacks/class.php";
$INFO['config'] = "1";
$INFO['default_method'] = "listElements";
$INFO['default_method_admin'] = "lists";
$INFO['is_indexed'] = "1";
$INFO['per_page'] = "10";

$INFO['func_perms'] = "";

$COMPONENTS = array();

$COMPONENTS[0] = "./classes/modules/feedbacks/__admin.php";
$COMPONENTS[1] = "./classes/modules/feedbacks/class.php";
$COMPONENTS[2] = "./classes/modules/feedbacks/i18n.php";
$COMPONENTS[3] = "./classes/modules/feedbacks/lang.php";
$COMPONENTS[4] = "./classes/modules/feedbacks/permissions.php";
?>