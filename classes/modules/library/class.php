<?php

class library extends def_module
{
    public $per_page;
    
    public function __construct()
    {
        parent::__construct();
        
        if (cmsController::getInstance()->getCurrentMode() == "admin") {
            
            $configTabs = $this->getConfigTabs();
            if ($configTabs) {
                $configTabs->add("config");
            }
            
            $this->__loadLib("__admin.php");
            $this->__implement("__library");
            
            $this->__loadLib("__events_handles.php");
            $this->__implement("__library_events");
            
        } else {
            
            $this->per_page = regedit::getInstance()->getVal("//modules/library/per_page");
        }
        
    }
    
    public function groupelements($path = "", $template = "default")
    {
        $elementId = cmsController::getInstance()->getCurrentElementId();
        
        templater::pushEditable("library", "groupelements", $elementId);
        return $this->group($elementId, $template) . $this->listElements($elementId, $template);
    }
    
    public function item_element()
    {
        $elementId = (int)cmsController::getInstance()->getCurrentElementId();
        return $this->view($elementId);
    }
    
    public function group($elementPath = "", $template = "default", $per_page = false)
    {
        $hierarchy = umiHierarchy::getInstance();
        list($template_block) = def_module::loadTemplates("tpls/library/{$template}.tpl", "group");
        
        $elementId = $this->analyzeRequiredPath($elementPath);
        
        $element = $hierarchy->getElement($elementId);
        
        templater::pushEditable("library", "groupelements", $element->getId());
        return self::parseTemplate($template_block, array('id' => $element->getId()), $element->getId());
    }
    
    public function view($elementPath = "", $template = "default")
    {
        $hierarchy = umiHierarchy::getInstance();
        list($template_block) = def_module::loadTemplates("tpls/library/{$template}.tpl", "view");
        
        $elementId = $this->analyzeRequiredPath($elementPath);
        $element = $hierarchy->getElement($elementId);
        
        templater::pushEditable("library", "item_element", $element->getId());
        return self::parseTemplate($template_block, array('id' => $element->getId()), $element->getId());
    }
    
    public function listGroup($elementPath, $template = "default", $per_page = false, $ignore_paging = false)
    {
    }
    
    public function listElements($path = "", $template = "default", $per_page = false, $ignore_paging = false, $ignoreFilter = false)
    {
        $extendedProperties = array('library_type', 'library_allow', 'short_desrc');
        $extendedGroups = array('files_arr');
        
        $dealers_user_group_id = 7694;
        $dealers_allow_item_id = 7695;
        $permissions = permissionsCollection::getInstance();
        $objects = umiObjectsCollection::getInstance();
        
        $group_ids = array();
        $user_id = $permissions->getUserId();
        $user = $objects->getObject($user_id);
        if ($user instanceof umiObject) {
            $group_ids = $user->groups;
        }
        
        
        if (!$per_page) $per_page = $this->per_page;
        $per_page = intval($per_page);
        
        //
        list($template_block, $template_block_empty, $template_line, $template_archive) = def_module::loadTemplates("library/" . $template, "lastlist_block", "lastlist_block_empty", "lastlist_item", "lastlist_archive");
        $curr_page = (int)getRequest('p');
        if ($ignore_paging) $curr_page = 0;
        
        
        $parent_id = $this->analyzeRequiredPath($path);
        
        if ($parent_id === false && $path != KEYWORD_GRAB_ALL) {
            throw new publicException(getLabel('error-page-does-not-exist', null, $path));
        }
        
        $this->loadElements($parent_id);
        
        $webinars = new selector('pages');
        $webinars->types('hierarchy-type')->name('library', 'item_element');
        if ($path != KEYWORD_GRAB_ALL) {
            if (is_array($parent_id)) {
                foreach ($parent_id as $parent) {
                    $webinars->where('hierarchy')->page($parent)->childs(1);
                }
            } else {
                $webinars->where('hierarchy')->page($parent_id)->childs(1);
            }
        }
        if (!in_array($dealers_user_group_id, $group_ids)) {
            $webinars->where('library_allow')->notequals($dealers_allow_item_id); // прячем материалы для дилеров, если пользователь не принадлежит к этой группе пользователей
        }
        
        
        if (!$ignoreFilter) selectorHelper::detectFilters($webinars);
        
        $webinars->option('no-permissions')->value(true);
        
        $webinars->option('load-all-props')->value(true);
        $webinars->limit($curr_page * $per_page, $per_page);
        
        $result = $webinars->result();
        
        if(!$result) {
            return $template_block_empty;
        }
        
        $total = $webinars->length();
        
        $umiLinksHelper = $this->umiLinksHelper;
        $umiHierarchy = umiHierarchy::getInstance();
        
        $isAuth = $permissions->isAuth();
        $guestUserId = permissionsCollection::getGuestId();
    
        $return = array();
        $items = array();
        foreach ($result as $element) {
            if (!$element instanceof umiHierarchyElement) {
                continue;
            }
            
            $object = $element->getObject();
        
            $item = array();
            $elementId = $element->getId();
        
            $item['attribute:id'] = $elementId;
            $item['node:name'] = $element->getName();
        
            $item['attribute:link'] = $umiHierarchy->getPathById($elementId);
            $item['xlink:href'] = "upage://" . $elementId;
            $item['void:header'] = $lines_arr['name'] = $element->getName();
            
            if(!$isAuth && !getArrayKey($permissions->isAllowedObject($guestUserId, $elementId), 0)) {
                $item['attribute:auth-required'] = true;
            }
        
            if (count($extendedProperties) > 0) {
                $item['extended']['properties'] = array();
                foreach ($extendedProperties as $extendedPropery) {
                    $property = $object->getPropByName($extendedPropery);
                    if (!$property instanceof umiObjectProperty) continue;
                    $propertyXml = translatorWrapper::get($property)->translate($property);
                    if ($propertyXml) {
                        $item['extended']['properties']['nodes:property'][] = $propertyXml;
                    }
                }
            }
            if (count($extendedGroups) > 0) {
                $item['extended']['groups'] = array();
                $item['extended']['groups']['nodes:group'] = array();
            
                $objectType = $object->getType();
            
                foreach ($extendedGroups as $extendedGroup) {
                    $group = $objectType->getFieldsGroupByName($extendedGroup);
                    if (!$group instanceof umiFieldsGroup) continue;
                
                    $item['extended']['groups']['nodes:group'][] = translatorWrapper::get($group)->translateProperties($group, $object);
                }
            }
            
            $items[] = self::parseTemplate($template_line, $item, $elementId);
            $this->pushEditable("news", "item", $elementId);
            $umiHierarchy->unloadElement($elementId);
        }
    
        if (is_array($parent_id)) {
            list($parent_id) = $parent_id;
        }
    
        $return['subnodes:items'] = $return['void:lines'] = $items;
        $return['archive'] = ($total > 0) ? $template_archive : "";
        $parent = $umiHierarchy->getElement($parent_id);
        if ($parent instanceof umiHierarchyElement) {
            $return['archive_link'] = $umiLinksHelper->getLinkByParts($parent);
        }
        
        $return['total'] = $total;
        $return['per_page'] = $per_page;
        $return['category_id'] = $parent_id;
    
        return $this->parseTemplate($template_block, $return, $parent_id);
    }
    
    public function countDownloads($pageId = NULL, $file_field_name = NULL)
    {
        $umiHierarchy = umiHierarchy::getInstance();
        
        $page = $umiHierarchy->getElement($pageId);
        if ($page instanceof umiHierarchyElement) {
            $field_count_name = $file_field_name . '_downloads';
            $field_count_new = $page->getValue($field_count_name) + 1;
            //$file_field_path = $page->getValue($file_field_name)->getFilePath(true);
            $page->setValue($field_count_name, $field_count_new);
        }
    }
    
    public function config()
    {
        return __library::config();
    }
    
    public function getEditLink($elementId, $elementType)
    {
        switch ($elementType) {
            case "groupelements":
            {
                $link_add = "/admin/library/add/{$elementId}/item_element/";
                $link_edit = "/admin/library/edit/{$elementId}/";
                
                return array($link_add, $link_edit);
            }
            
            case "item_element":
            {
                $link_edit = "/admin/library/edit/{$elementId}/";
                
                return array(false, $link_edit);
            }
            
            default:
            {
                return false;
            }
        }
    }
}