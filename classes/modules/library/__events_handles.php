<?php

abstract class __library_events
{
    
    /**
     * проверяем выставленное значение в поле "Доступ" ( name= library_allow)
     * выставляем соответствующее значение  в правах доступа к странице
     * @param iUmiEventPoint $eventPoint событие при сохранении изменений страницы в админке
     * @return bool
     * $level уровень выставляемых прав от "0" до "31":
     * ---------------------------------------------------------------------------
     * | значение | чтение | редактирование | создание | удаление |  перемещение |
     * |    0     |   -    |       -        |    -     |    -     |       -      |
     * |    1     |   +    |       -        |    -     |    -     |       -      |
     * |    3     |   +    |       +        |    -     |    -     |       -      |
     * |    7     |   +    |       +        |    +     |    -     |       -      |
     * |    15    |   +    |       +        |    +     |    +     |       -      |
     * |    31    |   +    |       +        |    +     |    +     |       +      |
     * ---------------------------------------------------------------------------
     */
    public function setPerm(iUmiEventPoint $eventPoint)
    {
        static $modifiedCache = array();
        
        $element = $eventPoint->getRef("element");
        if (!$element instanceof umiHierarchyElement || $element->getObjectTypeId() != 147) {
            return null;
        }
        
        $object = $element->getObject();
        
        // Отследить что изменилось нужное поле
        $field_name = 'library_allow';
        if ($eventPoint->getMode() == "before") {
            $data = getRequest("data");
            $id = $object->getId();
            $newValue = getArrayKey($data[$id], $field_name);
            if ($newValue != $object->getValue($field_name)) {
                $modifiedCache[$object->getId()] = $field_name;
            }
        } else {
            if (isset($modifiedCache[$object->getId()])) {
                if ($modifiedCache[$object->getId()] == $field_name) {
                    $this->_setLibraryItemPermissions($object, $object->getValue($field_name));
                }
            }
        }
        
        return null;
    }
    
    public function onCreateElementSetPerm(iUmiEventPoint $eventPoint)
    {
        if ($eventPoint->getMode() !== 'after') {
            return null;
        }
        
        $element = $eventPoint->getRef("element");
        if (!$element instanceof umiHierarchyElement || $element->getObjectTypeId() != 147) {
            return null;
        }
        
        $this->_setLibraryItemPermissions($element);
        return null;
    }
    
    public function setLibraryItemPermissions($elementId = null)
    {
        $elementId = intval($elementId) > 0 ? $elementId : intval(getRequest('param0'));
        if($elementId <= 0) {
            throw new publicException('Не передан id страницы');
        }
        
        $element = umiHierarchy::getInstance()->getElement($elementId);
        if (!$element instanceof umiHierarchyElement || $element->getObjectTypeId() != 147) {
            throw new publicException('Не найдена страница ' . $elementId);
        }
        
        $this->_setLibraryItemPermissions($element);
        $this->redirect('/admin/library/edit/' . $elementId . '/');
    }
    
    public function _setLibraryItemPermissions(umiHierarchyElement $element, $type = null)
    {
        if (is_null($type)) {
            $type = $element->getValue('library_allow');
        }
        
        $permissionsCollection = permissionsCollection::getInstance();
        $permissionsCollection->resetElementPermissions($element->getId());
        
        /* проверить кого выбрали
        1305 - Общего доступа
        1306 - Для зарегистрированных пользователей
        7695 - Для дилеров
        */
        switch ($type) {
            //Для зарегистрированных пользователей
            case 1306:
                $ownerId = 334; // получаем id группы пользователей "Зарегистрированные пользователи"
                break;
            //Для дилеров
            case 7695:
                $ownerId = 7694; // получаем id группы пользователей "Дилеры"
                break;
            //Общего доступа (если выставили id - 1305 или не выставили ничего)
            default:
                $ownerId = $permissionsCollection->getGuestId(); // получаем id гостя
                break;
        }
        
        $permissionsCollection->setElementPermissions($ownerId, $element->getId(), 1);
    }
}