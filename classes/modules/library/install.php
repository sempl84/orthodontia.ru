<?php

$INFO = Array();

$INFO['name'] = "library";
$INFO['filename'] = "modules/library/class.php";
$INFO['config'] = "1";
$INFO['default_method'] = "listElements";
$INFO['default_method_admin'] = "lists";
$INFO['is_indexed'] = "1";
$INFO['per_page'] = "10";

$INFO['func_perms'] = "";

$COMPONENTS = array();

$COMPONENTS[0] = "./classes/modules/library/__admin.php";
$COMPONENTS[1] = "./classes/modules/library/class.php";
$COMPONENTS[2] = "./classes/modules/library/i18n.php";
$COMPONENTS[3] = "./classes/modules/library/lang.php";
$COMPONENTS[4] = "./classes/modules/library/permissions.php";
?>