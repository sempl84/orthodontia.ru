<?php
class custom extends def_module {
    public function cms_callMethod($method_name, $args) {
        return call_user_func_array(Array($this, $method_name), $args);
    }

    public function __call($method, $args) {
        throw new publicException("Method " . get_class($this) . "::" . $method . " doesn't exist");
    }

    //TODO: Write your own macroses here

    // формирует новый номер для счета для бухгалтерии
    public function buhgalt_order_number($old_id = NULL) {
        if (!$old_id){
            $old_id = getRequest('param0');
        }
        $res = "СУ" . str_pad($old_id, 9, "0", STR_PAD_LEFT);
        return $res;
    }

    public function get1cRel($page_id = false) {
        if (!$page_id) {
            $page_id = getRequest('param0');
        }
        if (!$page_id) {
            return 0;
        }

        $relation = umiImportRelations::getInstance();
        $source_id = $relation->getSourceId('commerceML2');
        $old_1c_id = $relation->getOldIdRelation($source_id, $page_id);

        if ($old_1c_id !== false) {
            $element = umiHierarchy::getInstance()->getElement($page_id);
            if ($element instanceof umiHierarchyElement) {
                $element->setValue('1c_product_id', $old_1c_id);
                $element->commit();
            }
            return $old_1c_id;
        }
        return 0;
    }

//    public function get1cRelTest($page_id = false) {
//        if (!$page_id){
//            $page_id = getRequest('param0');
//        }
//        if (!$page_id){
//            return;
//        }
//        $hierarchy = umiHierarchy::getInstance();
//        $element = $hierarchy -> getElement($page_id);
//        if (!$element){
//            return 'no-page';
//        }
//
//        $relation = umiImportRelations::getInstance();
//        $source_id = $relation -> getSourceId('commerceML2');
//        $old_id_test = $relation -> getOldIdRelation($source_id, $page_id);
//
//        if ($old_id_test) {
//            //$element -> setValue('1c_product_id', $old_id_test);
//            //$element -> commit();
//            return $old_id_test;
//        }
//        return 'no-1C-id';
//    }

    // вывод скидки к мероприятию из полей внутри мероприятия
    public function outputEventDiscount($elementId)
    {
        $hierarchy = umiHierarchy::getInstance();
        $discountFields = array(
            'nazvanie_skidki_1' => 'stoimost_meropriyatiya_s_uchetom_skidki_1',
            'nazvanie_skidki_2' => 'stoimost_meropriyatiya_s_uchetom_skidki_2',
            'nazvanie_skidki_3' => 'stoimost_meropriyatiya_s_uchetom_skidki_3',
            'nazvanie_skidki_4' => 'stoimost_meropriyatiya_s_uchetom_skidki_4',
            'nazvanie_skidki_5' => 'stoimost_meropriyatiya_s_uchetom_skidki_5'
        );
        $element = $hierarchy->getElement($elementId);
        if (!$element) {
            return "";
        }

        $price = $element->getValue('price');
        $listeners_speakers = ($element->getValue('listeners_speakers') == 1) ? 1 : 0;
        $is_loyality_available = $element->getValue('add_loyalty') == 1;
        $discount_loyalty = ($element->getValue('discount_loyalty') == 1) ? 1 : 0;
        $discounts_content = array();
        $discounts_content[] = array(
            'attribute:pageId' => $elementId,
            'attribute:listeners_speakers' => $listeners_speakers,
            'attribute:input_name' => 'data[new][skidka]',
            'attribute:name' => 'без скидки',
            'attribute:price' => $price,
            'attribute:freez_ormco_star' => 0, // сколько надо баллов для скидки
            // 'attribute:loyalty_bonus' => ($discount_loyalty || $listeners_speakers) ? $this->getLoyaltyPotentialBonus($price) : 'false' // ($discount_loyalty) ? getLoyaltyPotentialBonus($value) : false
            //'attribute:loyalty_bonus'=> $this->getLoyaltyPotentialBonus($price)
            'attribute:loyalty_bonus' => $is_loyality_available ? $this->getLoyaltyPotentialBonus($price) : false
        );
        $is_discount = false;

        //Проверка скидки ординатора
        if ($element->getValue('discount_ordinator') == 1) {
            $name = 'Ординаторам (40%)';
            $value = ceil($price * 0.6);
            $discounts_content[] = array(
                'attribute:pageId' => $elementId,
                'attribute:listeners_speakers' => $listeners_speakers,
                'attribute:id' => 'discount_ordinator',
                'attribute:input_name' => 'data[new][skidka]',
                'attribute:name' => $name,
                'attribute:price' => $value,
                'attribute:freez_ormco_star' => 0, // сколько надо баллов для скидки
                //'attribute:loyalty_bonus'=> ($discount_loyalty) ? $this->getLoyaltyPotentialBonus($value) : 'false' // ($discount_loyalty) ? getLoyaltyPotentialBonus($value) : false
                'attribute:loyalty_bonus' => $is_loyality_available ? $this->getLoyaltyPotentialBonus($value) : false
            );
        }

        //Проверка скидки ординатора 30
        if ($element->getValue('discount_ordinator_30') == 1) {
            $name = 'Ординаторам (30%)';
            $value = ceil($price * 0.7);
            $discounts_content[] = array(
                'attribute:pageId' => $elementId,
                'attribute:listeners_speakers' => $listeners_speakers,
                'attribute:id' => 'discount_ordinator_30',
                'attribute:input_name' => 'data[new][skidka]',
                'attribute:name' => $name,
                'attribute:price' => $value,
                'attribute:freez_ormco_star' => 0, // сколько надо баллов для скидки
                'attribute:loyalty_bonus' => $is_loyality_available ? $this->getLoyaltyPotentialBonus($value) : false
            );
        }

        //Проверка скидки врачам 1-2 года
        if ($element->getValue('discount_doctor') == 1) {
            $name = 'Врачам 1-2 года (30%)';
            $value = ceil($price * 0.7);
            $discounts_content[] = array(
                'attribute:pageId' => $elementId,
                'attribute:listeners_speakers' => $listeners_speakers,
                'attribute:id' => 'discount_doctor',
                'attribute:input_name' => 'data[new][skidka]',
                'attribute:name' => $name,
                'attribute:price' => $value,
                'attribute:freez_ormco_star' => 0, // сколько надо баллов для скидки
                'attribute:loyalty_bonus' => $is_loyality_available ? $this->getLoyaltyPotentialBonus($value) : false
            );
        }

        //Проверка скидки преподавателям кафедры
        if ($element->getValue('discount_teacher') == 1) {
            $name = 'Преподавателям кафедры (50%)';
            $value = ceil($price * 0.5);
            $discounts_content[] = array(
                'attribute:pageId' => $elementId,
                'attribute:listeners_speakers' => $listeners_speakers,
                'attribute:id' => 'discount_teacher',
                'attribute:input_name' => 'data[new][skidka]',
                'attribute:name' => $name,
                'attribute:price' => $value,
                'attribute:freez_ormco_star' => 0, // сколько надо баллов для скидки
                'attribute:loyalty_bonus' => $is_loyality_available ? $this->getLoyaltyPotentialBonus($value) : false
            );
        }

        //Проверка скидки ПОО
        if ($element->getValue('discount_poo') == 1) {
            $name = 'Действующим членам ПОО (15%)';
            $value = ceil($price * 0.85);
            $discounts_content[] = array(
                'attribute:pageId' => $elementId,
                'attribute:listeners_speakers' => $listeners_speakers,
                'attribute:id' => 'discount_poo',
                'attribute:input_name' => 'data[new][skidka]',
                'attribute:name' => $name,
                'attribute:price' => $value,
                'attribute:freez_ormco_star' => 0, // сколько надо баллов для скидки
                //'attribute:loyalty_bonus'=> ($discount_loyalty) ? $this->getLoyaltyPotentialBonus($value) : 'false' // ($discount_loyalty) ? getLoyaltyPotentialBonus($value) : false
                'attribute:loyalty_bonus' => $is_loyality_available ? $this->getLoyaltyPotentialBonus($value) : false
            );
        }
        //Проверка скидки ШОО
        if ($element->getValue('discount_shoo') == 1) {
            $name = 'Действующим участникам Школы ортодонтии и выпускникам Онлайн Школы ортодонтии (50%)';
            $value = ceil($price * 0.5);
            $discounts_content[] = array(
                'attribute:pageId' => $elementId,
                'attribute:listeners_speakers' => $listeners_speakers,
                'attribute:id' => 'discount_shoo',
                'attribute:input_name' => 'data[new][skidka]',
                'attribute:name' => $name,
                'attribute:price' => $value,
                'attribute:freez_ormco_star' => 0, // сколько надо баллов для скидки
                'attribute:loyalty_bonus' => $is_loyality_available ? $this->getLoyaltyPotentialBonus($value) : false
            );
        }

        $isSpeakersDiscountEvent = SiteCatalogObjectModel::isSpeakersDiscountEvent($element);

        foreach ($discountFields as $discountName => $discountValue) {
            $name = $element->getValue($discountName);
            if(!$name) {
                continue;
            }

            $value = $element->getValue($discountValue);
            if($isSpeakersDiscountEvent) {
                if(is_null($value)) {
                    continue;
                }
            } elseif(!$value) {
                continue;
            }

            $discount = array(
                'attribute:pageId' => $elementId,
                'attribute:listeners_speakers' => $listeners_speakers,
                'attribute:id' => $discountName,
                'attribute:input_name' => 'data[new][skidka]',
                'attribute:name' => $name,
                'attribute:price' => $value,
                'attribute:freez_ormco_star' => 0, // сколько надо баллов для скидки
                'attribute:loyalty_bonus' => $is_loyality_available ? $this->getLoyaltyPotentialBonus($value) : false
            );

            if($isSpeakersDiscountEvent && $name == 'выступающий') {
                $discount['attribute:speakers-left'] = $element->getValue(SiteCatalogObjectModel::field_speakers_left);
            }

            $discounts_content[] = $discount;
        }

        //Проверка скидки по программе лояльности (50%)
        if ($discount_loyalty) {
            $value = ceil($price * 0.5);
            $loyaltyDiscount = $this->getLoyaltyDiscountSize($price);
            $isLoyaltyDiscountAllow = (isset($loyaltyDiscount['allow'])) ? $loyaltyDiscount['allow'] : 0;
            $loyaltyDiscountSize = (isset($loyaltyDiscount['loyalty_discount'])) ? $loyaltyDiscount['loyalty_discount'] : 0;
            $name = 'Cкидка 50% за ' . $loyaltyDiscountSize . ' звезд';

            $discounts_content[] = array(
                'attribute:pageId' => $elementId,
                'attribute:listeners_speakers' => $listeners_speakers,
                'attribute:id' => 'discount_loyalty',
                'attribute:input_name' => 'data[new][skidka]',
                'attribute:name' => $name,
                'attribute:price' => $value,
                'attribute:isallow' => $isLoyaltyDiscountAllow, // хватает ли на скидку
                'attribute:freez_ormco_star' => $loyaltyDiscountSize, // сколько надо баллов для скидки
                'attribute:loyalty_bonus' => ($discount_loyalty) ? 0 : 'false'
            );
        }

        $isPromocode = 0;
        $promocodes_str = $element->promokod_arr;
        $promocodes_arr = json_decode($promocodes_str);
        foreach ($promocodes_arr as $promocodeItem) {
            //[{"name":"testpromo","discount":"10","count":"1","active":"1"},{"name":"111","discount":"20","count":"15","active":"1"},{"name":"2222","discount":"50","count":"12","active":"0"}]
            $count = $promocodeItem->count;
            $active = $promocodeItem->active;
            if ($count > 0 && $active == 1) {
                $isPromocode = 1;
            }
        }

        return array('price' => $price, 'nodes:item' => $discounts_content, 'promocode' => array('attribute:active' => $isPromocode));
    }

    // вывод скидки к мероприятию из полей внутри мероприятия
    public function outputEventLoyalty($elementId) {
        $hierarchy = umiHierarchy::getInstance();

        $element = $hierarchy -> getElement($elementId);
        if (!$element){
            return "";
        }

        $price = $element -> price;
        $loyalty = array();

        //Проверка возможности получение баллов за покупку мероприятия
        if($element -> getValue('add_loyalty') == 1){
            $potential_bonus = $this->getLoyaltyPotentialBonus($price);
            $loyalty['add_loyalty'] = array(
                'attribute:potential_bonus'=>$potential_bonus
            );
        }

        //Проверка скидки по программе лояльности (50%)
        if($element -> getValue('discount_loyalty') == 1){
            $name = 'оплата баллами, скидка 50%';
            $value = ceil($price * 0.5);
            $loyalty['discount_loyalty'] = array(
                'attribute:name'=>$name,
                'attribute:price'=>$value,
                'attribute:isallow'=>1 //$this->getLoyaltyDiscountSize($value),
            );
        }
        return $loyalty;
    }

    /*узнать кол-во потенциальных баллов в зависимости от стоимости мероприятия*/
    public function getLoyaltyPotentialBonus($price = NULL) {
        if(!$price){
            $price = getRequest('param0');
        }
        $potentialBonus = 0;

        $loyaltyPotentialGUID = 198;
        $sel = new selector('objects');
        $sel -> types('object-type') -> id($loyaltyPotentialGUID);
        $sel -> where('event_price_from') -> eqless($price);
        $sel -> where('event_price_to') -> more($price);
        $sel -> limit(0, 1);

        if ($object = $sel -> first) {
            $potentialBonus = (int) $object -> name;
        }
        return $potentialBonus;
    }

    /*узнать кол-во потенциальных баллов в зависимости от стоимости мероприятия*/
    public function getLoyaltyDiscountSize($price = NULL) {
        if(!$price){
            $price = getRequest('param0');
        }
        $discountAllow = 0;
        $bonusForLoyaltyDiscount = 0;

        $loyaltyDiscountGUID = 199;
        $sel = new selector('objects');
        $sel -> types('object-type') -> id($loyaltyDiscountGUID);
        $sel -> where('event_price_from') -> eqless($price);
        $sel -> where('event_price_to') -> more($price);
        $sel -> limit(0, 1);
        if ($object = $sel -> first) {
            $bonusForLoyaltyDiscount = (int) $object -> name;
            if($bonusForLoyaltyDiscount > 0){
                $permissions = permissionsCollection::getInstance();
                $currentUserId = $permissions->getUserId();
                $currentUser = umiObjectsCollection::getInstance() -> getObject($currentUserId);
                if($currentUser->yet_ormco_star >= $bonusForLoyaltyDiscount){
                    $discountAllow = 1;
                }
            }
        }
        return array('allow'=>$discountAllow, 'loyalty_discount'=>$bonusForLoyaltyDiscount);
    }

    public function import_users() {
        return 'switch_off';

        $start = 1;
        $end = 6;
        $handle = fopen('./users.csv', "r");
        //$handle = fopen('./users_02.csv', "r");
        //$handle = fopen('./users_03.csv', "r");
        $row = 1;
        $group_id = regedit::getInstance() -> getVal("//modules/users/def_group");
        $fields_arr = array(
        //2=>'e-mail',
        //3=>'password',
        //'groups',
        4 => 'lname', 5 => 'fname', 6 => 'father_name', 7 => 'mobile_phone', 8 => 'gorod_from', 9 => 'nazvanie_organizacii', 10 => 'fio_rukovoditelya', 11 => 'dolzhnost', 12 => 'inn', 13 => 'kpp', 14 => 'raschetnyj_cchet', 15 => 'bik', 16 => 'korrschet', 17 => 'yuridicheskij_adres', 18 => 'fakticheskij_adres', 19 => 'telefon', 20 => 'faks', 21 => 'fio_kntaktnogo_lica', 22 => 'tel_kontaktnogo_lica', 23 => 'mejl_kontaktnogo_lica',

        //'want_to_buy',
        //'type_of_production',
        //'another_type_of_production',
        //'how_you_been_edu',
        );

        while (!feof($handle)) {
            $data_str = fgets($handle, 1000);

            $data = explode(";", $data_str);

            foreach ($data as $key => $data_item) {
                $data[$key] = str_replace('"', '', $data_item);
            }
            //var_dump('===============================');
            //var_dump($data);

            if (feof($handle)){
                continue;
            }

            $objectTypes = umiObjectTypesCollection::getInstance();
            $objectTypeId = $objectTypes -> getBaseType("users", "user");
            $id = (isset($data[0]) && $data[0] != '') ? $data[0] : '-';
            $login = (isset($data[2]) && $data[2] != '') ? $data[2] : 'un_user' . $row . '@test.ru';
            $password = $data[3];

            // search users
            $email = strtolower(trim($login));
            if (!umiMail::checkEmail($email)) {
                file_put_contents(CURRENT_WORKING_DIR . '/traceajax.txt', $data_str . "\n", FILE_APPEND);
                continue;
            }

            $sel = new selector('objects');
            $sel -> types('object-type') -> name('users', 'user');
            $sel -> where('e-mail') -> equals($email);
            $sel -> limit(0, 1);

            if ($sel -> first) {
                $object = $sel -> first;
                $object -> setValue("sync_merge", 1);
                $object -> setValue("is_activated", true);
            } else {
                //Creating user...
                $object_id = umiObjectsCollection::getInstance() -> addObject($login, $objectTypeId);
                $object = umiObjectsCollection::getInstance() -> getObject($object_id);

                $object -> setValue("login", $login);
                $object -> setValue("password", $password);
                $object -> setValue("e-mail", $email);
                $object -> setValue("is_activated", true);
                $object -> setValue("register_date", time());
                $object -> setValue("groups", Array($group_id));
                $object -> setValue("id_old", $id);
                $object -> setValue("sync_new", 1);
            }

            //$activate_code = md5($login . time());
            foreach ($fields_arr as $key => $field) {
                $oldValue = $object -> getValue($field);
                if (!$oldValue || $oldValue = '' || $oldValue = ' ') {
                    $value = (isset($data[$key]) && $data[$key] != '') ? $data[$key] : " ";
                    $object -> setValue($field, $value);
                }
            }

            //$object->setValue("activate_code", $activate_code);
            $object -> commit();

            /*//save addres
             $city = ereg_replace(" +", " ", iconv("CP1251", "UTF-8//IGNORE", $data[6]));
             $street = ereg_replace(" +", " ", iconv("CP1251", "UTF-8//IGNORE", $data[7]));
             $korpys = ereg_replace(" +", " ", iconv("CP1251", "UTF-8//IGNORE", $data[9]));
             $korpys = ($korpys) ? ', корп ' . $korpys : '';
             $stroenie = ereg_replace(" +", " ", iconv("CP1251", "UTF-8//IGNORE", $data[10]));
             $stroenie = ($stroenie) ? ', строение ' . $stroenie : '';
             $house = ereg_replace(" +", " ", iconv("CP1251", "UTF-8//IGNORE", $data[8])) . $korpys . $stroenie;
             $flat = ereg_replace(" +", " ", iconv("CP1251", "UTF-8//IGNORE", $data[11]));
             $comment = ereg_replace(" +", " ", iconv("CP1251", "UTF-8//IGNORE", $data[12]));

             if ($street && $house) {
             $collection = umiObjectsCollection::getInstance();
             $types = umiObjectTypesCollection::getInstance();
             $typeId = $types -> getBaseType("emarket", "delivery_address");
             //$customer   = customer::get();
             $addressId = $collection -> addObject("Address for customer #" . $object -> id, $typeId);
             $address = umiObjectsCollection::getInstance() -> getObject($addressId);

             $address -> setValue("city", $city);
             $address -> setValue("street", $street);
             $address -> setValue("house", $house);
             $address -> setValue("flat", $flat);

             $address -> commit();

             $object -> delivery_addresses = array_merge($object -> delivery_addresses, array($addressId));
             $object -> commit();
             }*/
        }
        fclose($handle);
        exit('aaa');
    }

    public function testpaw(){
        var_dump(!is_null(0));
        exit('aaa');
        /*
        MNT_ID=84211464
        MNT_TRANSACTION_ID=18112.1499842313
        MNT_OPERATION_ID=155396502
        MNT_AMOUNT=11.00
        MNT_CURRENCY_CODE=RUB
        MNT_TEST_MODE=0
        MNT_SIGNATURE=65028441d9004b881d51391281ff09d3
        paymentSystem.unitId=1686945
        MNT_CORRACCOUNT=303
        MNT_CUSTOM1=Rumyantsev+Pavel+Ivanovich
        MNT_CUSTOM2=%2B7+%28812%29+3244259
        MNT_CUSTOM3=TEST+%D1%81%D0%B5%D0%BC%D0%B8%D0%BD%D0%B0%D1%80
        MNT_CUSTOM4=admin%40umihelp.ru
        MNT_CUSTOM5=%D0%A1%D0%B0%D0%BD%D0%BA%D1%82-%D0%9F%D0%B5%D1%82%D0%B5%D1%80%D0%B1%D1%83%D1%80%D0%B3
        MNT_FEE=-0.30
        cardnumber=532130******8302';
         *
         */

        $params = '';
        //if (getRequest('MNT_COMMAND')) $params .= getRequest('MNT_COMMAND');
        $params .= '84211464';//getRequest('MNT_ID');
        $params .= '18112.1499842313';//getRequest('MNT_TRANSACTION_ID');
        $params .= '155396502';//getRequest('MNT_OPERATION_ID');
        $params .= '11.00';//getRequest('MNT_AMOUNT');
        $params .= 'RUB';//getRequest('MNT_CURRENCY_CODE');
        $params .= '';//getRequest('MNT_SUBSCRIBER_ID');
        $params .= '0';//getRequest('MNT_TEST_MODE');
        $params .= '124732142';//$this->object->mnt_data_integrity_code;
        $signature = md5($params);
        $s_xheck = '65028441d9004b881d51391281ff09d3';
        file_put_contents(CURRENT_WORKING_DIR .'/traceajaxpaw.txt', "\n\n" .print_r($signature,true) ."\n". date('H:i:s') ."\n", FILE_APPEND);
        file_put_contents(CURRENT_WORKING_DIR .'/traceajaxpaw.txt', "\n\n" .print_r($s_xheck,true) ."\n". date('H:i:s') ."\n", FILE_APPEND);
        file_put_contents(CURRENT_WORKING_DIR .'/traceajaxpaw.txt', "\n\n" .strcasecmp($signature, $s_xheck) ."\n". date('H:i:s') ."\n", FILE_APPEND);

        var_dump($signature);
        var_dump($s_xheck);
        var_dump(strcasecmp($signature, $s_xheck));
        exit('aaa');
    }

    public function testChangekafedraParam(){
        return 'switch off';
        $dealers_spec_programs_pid = 2259;
        $seminar_na_kafeder_da = 478455;
        $seminar_na_kafeder_net = 478454;
        $sel = new selector('pages');
        $sel -> types('object-type') -> name('catalog', 'object');
        $sel->where('hierarchy')->page($dealers_spec_programs_pid)->childs(8);
        //$sel->where('seminar_na_kafedre')->equals($seminar_na_kafeder_da);

        foreach ($sel as $page) {
            $page->seminar_na_kafedre = $seminar_na_kafeder_net;
        }
        return 'yes';
	}

	/*
	 * Авторизация под произвольным пользователем
	 */
//	public function login_as_user(){
//		$user_id = 1191773;
//		permissionsCollection::getInstance()->loginAsUser($user_id);
//	}

    /**
     * Починка иерархии для элементов каталога
     * @return int
     */
//    public function rebuild_hierarhy(){
//        $umiHierarchy = umiHierarchy::getInstance();
//
//        $sel = new selector('pages');
//        $sel->types('object-type')->name('catalog', 'object');
//        $sel->order('id')->asc();
//        $sel->limit(0, 1500);
//
//        $count = 0;
//        foreach ($sel as $page) {
//            $umiHierarchy->rebuildRelationNodes($page->getId());
//            $count++;
//        }
//        return $count;
//    }
}