<?php
 
$i18n = Array(
"label-add-list" => "Добавить группу",
"label-add-item" => "Добавить страницу",
 
"header-cliniccourse-lists" => "Группы и страницы",
"header-cliniccourse-config" => "Настройки модуля",
"header-cliniccourse-add" => "Добавление",
"header-cliniccourse-edit" => "Редактирование",
 
"header-cliniccourse-add-groupelements"   => "Добавление группы",
"header-cliniccourse-add-item_element"     => "Добавление страницы",
 
"header-cliniccourse-edit-groupelements"   => "Редактирование группы",
"header-cliniccourse-edit-item_element"     => "Редактирование страницы",
 
"header-cliniccourse-activity" => "Изменение активности",
 
'perms-cliniccourse-view' => 'Просмотр страниц',
'perms-cliniccourse-lists' => 'Управление страницами',

'module-cliniccourse' => 'Клинические офис-курсы',
 
);
 
?>