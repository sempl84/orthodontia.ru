<?php

$INFO = Array();

$INFO['name'] = "cliniccourse";
$INFO['filename'] = "modules/cliniccourse/class.php";
$INFO['config'] = "1";
$INFO['default_method'] = "listElements";
$INFO['default_method_admin'] = "lists";
$INFO['is_indexed'] = "1";
$INFO['per_page'] = "10";

$INFO['func_perms'] = "";

$COMPONENTS = array();

$COMPONENTS[0] = "./classes/modules/cliniccourse/__admin.php";
$COMPONENTS[1] = "./classes/modules/cliniccourse/class.php";
$COMPONENTS[2] = "./classes/modules/cliniccourse/i18n.php";
$COMPONENTS[3] = "./classes/modules/cliniccourse/lang.php";
$COMPONENTS[4] = "./classes/modules/cliniccourse/permissions.php";
?>