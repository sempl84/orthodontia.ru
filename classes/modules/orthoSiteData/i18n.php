<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2022, Evgenii Ioffe
 */

$i18n = Array(
    'module-orthoSiteData' => 'Данные сайта',

    'header-orthoSiteData-dadata' => 'Dadata',
    
    'header-orthoSiteData-dadataConfig' => 'Настройки Dadata',
    'group-orthoSiteData-dadataConfig-api' => 'API',
    'option-orthoSiteData-dadataConfig-api-token' => 'Токен',

    'perms-orthoSiteData-admin' => 'Управление модулем',
    'perms-orthoSiteData-auth' => 'Авторизованные пользователи',
    'perms-orthoSiteData-guest' => 'Гостевые права',
);