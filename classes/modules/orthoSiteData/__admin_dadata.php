<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2022, Evgenii Ioffe
 */

/**
 * Class __orthoSiteDataAdmin
 */
class __orthoSiteDataAdminDadata extends __orthoSiteDataAdmin
{
    public function dadata()
    {
        outputBuffer::current()->redirect('/admin/orthoSiteData/dadataConfig/');
    }
    
    public function dadataConfig()
    {
        $regedit = regedit::getInstance();
        
        $params = array(
            'orthoSiteData-dadataConfig-api' => array(
                'string:orthoSiteData-dadataConfig-api-token' => NULL,
            ),
        );
        
        $mode = getRequest('param0');
        
        if ($mode == 'do') {
            $params = $this->expectParams($params);
            $regedit->setVar(OrthoSiteDataDadataHelper::registry_param_token, $params['orthoSiteData-dadataConfig-api']['string:orthoSiteData-dadataConfig-api-token']);
            $this->chooseRedirect();
        }
        
        $params['orthoSiteData-dadataConfig-api']['string:orthoSiteData-dadataConfig-api-token'] = $regedit->getVal(OrthoSiteDataDadataHelper::registry_param_token);
        
        $this->setDataType('settings');
        $this->setActionType('modify');
        
        $data = $this->prepareData($params, 'settings');
        
        $this->setData($data);
        $this->doData();
    }
}