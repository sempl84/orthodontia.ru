<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2022, Evgenii Ioffe
 */
class orthoSiteData extends def_module
{
    const module = 'orthoSiteData';

    public function __construct()
    {
        parent::__construct();

        if (cmsController::getInstance()->getCurrentMode() == "admin") {
            $this->initTabs();
            $this->includeAdminClasses();
        }

        $this->includeCommonClasses();
    }

    /**
     * Создает вкладки административной панели модуля
     */
    protected function initTabs()
    {
        $commonTabs = $this->getCommonTabs();

        if ($commonTabs instanceof iAdminModuleTabs) {
            $commonTabs->add('dadata');
        }
        
        $configTabs = $this->getConfigTabs();
        if($configTabs instanceof iAdminModuleTabs) {
            $configTabs->add('dadataConfig');
        }
    }

    /**
     * Подключает классы функционала административной панели
     */
    protected function includeAdminClasses()
    {
        $this->__loadLib("__admin.php");
        $this->__implement("__orthoSiteDataAdmin");

        $this->__loadLib("__admin_dadata.php");
        $this->__implement("__orthoSiteDataAdminDadata");
    }

    /**
     * Подключает общие классы функционала
     */
    protected function includeCommonClasses()
    {
        $this->__loadLib("__macros_dadata.php");
        $this->__implement("OrthoSiteDataMacrosDadata");
    }
}