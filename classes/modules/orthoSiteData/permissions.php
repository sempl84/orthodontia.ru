<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2022, Evgenii Ioffe
 */

$permissions = Array(
    'admin' => array(
        'dadataConfig'
    ),
    'auth' => array(
        'getSiteDataIsUserDadataEmpty',
    ),
    'guest' => array(
        'getSiteDataDadataToken',
    )
);