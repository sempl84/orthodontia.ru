<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2022, Evgenii Ioffe
 */

class OrthoSiteDataMacrosDadata extends orthoSiteData
{
    public function getSiteDataDadataToken()
    {
        $token = regedit::getInstance()->getVal(OrthoSiteDataDadataHelper::registry_param_token);
        
        if (!$token) {
            return null;
        }
        
        return array(
            'token' => $token
        );
    }
    
    public function getSiteDataIsUserDadataEmpty()
    {
        if(!permissionsCollection::getInstance()->isAuth()) {
            throw new publicException('Пользователь неавторизован');
        }
        
        $user = umiObjectsCollection::getInstance()->getObject(permissionsCollection::getInstance()->getUserId());
        if(!$user instanceof umiObject) {
            throw new publicException('Не найден объект пользователя');
        }
        
        $isEmpty = false;
        
        if(isset($_REQUEST['test_dadata_alert'])) {
            $isEmpty = true;
        } elseif(OrthoSiteDataDadataHelper::isDadataEnabledForCountry($user->getValue(SiteUsersUserModel::field_country))) {
            $isEmpty = !$user->getValue(SiteUsersUserModel::field_address_raw);
        }
        
        return array('attribute:is-empty' => intval($isEmpty));
    }
}