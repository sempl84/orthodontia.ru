<?php
abstract class __webinars_events {

	/**
	 * проверяем выставленное значение в поле "Доступ" ( name= library_allow)
	 * выставляем соответствующее значение  в правах доступа к странице
	 * @param iUmiEventPoint $event событие при сохранении изменений страницы в админке
	 * @return bool
	 * $level уровень выставляемых прав от "0" до "31":
	 * ---------------------------------------------------------------------------
	 * | значение | чтение | редактирование | создание | удаление |  перемещение |
	 * |    0     |   -    |       -        |    -     |    -     |       -      |
	 * |    1     |   +    |       -        |    -     |    -     |       -      |
	 * |    3     |   +    |       +        |    -     |    -     |       -      |
	 * |    7     |   +    |       +        |    +     |    -     |       -      |
	 * |    15    |   +    |       +        |    +     |    +     |       -      |
	 * |    31    |   +    |       +        |    +     |    +     |       +      |
	 * ---------------------------------------------------------------------------	 
	 */
	public function setPerm(iUmiEventPoint $event) {		
		static $modifiedCache = array();
		$hierarchy = umiHierarchy::getInstance();
		$perm = permissionsCollection::getInstance();
		$level = 1;

		$element = $event -> getRef("element");
		$element_Id = $element -> getId();
		$object = $element -> getObject();
		$element_type_id = $object -> getTypeId();

		$webinar_tid = umiObjectTypesCollection::getInstance() -> getBaseType('webinars', 'item_element');

		// проверить что это изменение записи вебинара
		if (!($element_type_id == $webinar_tid)) return;
		
		
		// Отследить что изменилось нужное поле
		$field_name = 'library_allow';
		if ($event -> getMode() == "before") {
			$data = getRequest("data");
			$id = $object -> getId();
			$newValue = getArrayKey($data[$id], $field_name);
			if ($newValue != $object -> getValue($field_name)){
				$modifiedCache[$object -> getId()] = $field_name;
			}
		} else {
			if (isset($modifiedCache[$object -> getId()])) {
				if ($modifiedCache[$object -> getId()] == $field_name) {
					$newValue = $object -> getValue($field_name);
					
					// сбросить все права на страницу
					$perm->resetElementPermissions($element_Id);
					
					
					/* проверить кого выбрали
					1305 - Общего доступа
					1306 - Для зарегистрированных пользователей
					7695 - Для дилеров
					*/
					switch ($newValue) {
						//Для зарегистрированных пользователей
						case 1306:
							$ownerId = 334; // получаем id группы пользователей "Зарегистрированные пользователи"
							break;
						//Для дилеров
						case 7695:
							$ownerId = 7694; // получаем id группы пользователей "Дилеры"
							break;
						//Общего доступа (если выставили id - 1305 или не выставили ничего)
						default:
							$ownerId = $perm->getGuestId(); // получаем id гостя
							break;
					}
					$perm->setElementPermissions($ownerId,$element_Id,$level);
				}
			}
		}		
		
		
		return true;
		
	}

	public function onCreateElementSetPerm(iUmiEventPoint $event) {
		if ($event -> getMode() === 'after') {
			$perm = permissionsCollection::getInstance();
			$level = 1;
		
			$element = $event -> getRef("element");
			$element_Id = $element -> getId();
			$object = $element -> getObject();
			$element_type_id = $object -> getTypeId();
	
			$webinar_tid = umiObjectTypesCollection::getInstance() -> getBaseType('webinars', 'item_element');
	
			// проверить что это изменение записи вебинара
			if (!($element_type_id == $webinar_tid)) return;
			
			$field_name = 'library_allow';
			$newValue = $element -> getValue($field_name);
			
			// сбросить все права на страницу
			$perm->resetElementPermissions($element_Id);
			
			
			/* проверить кого выбрали
			1305 - Общего доступа
			1306 - Для зарегистрированных пользователей
			7695 - Для дилеров
			*/
			switch ($newValue) {
				//Для зарегистрированных пользователей
				case 1306:
					$ownerId = 334; // получаем id группы пользователей "Зарегистрированные пользователи"
					break;
				//Для дилеров
				case 7695:
					$ownerId = 7694; // получаем id группы пользователей "Дилеры"
					break;
				//Общего доступа (если выставили id - 1305 или не выставили ничего)
				default:
					$ownerId = $perm->getGuestId(); // получаем id гостя
					break;
			}
			$perm->setElementPermissions($ownerId,$element_Id,$level);
		}
		return true;
	}

}
?>