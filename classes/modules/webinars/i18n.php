<?php
 
$i18n = Array(
"label-add-list" => "Добавить группу",
"label-add-item" => "Добавить страницу",
 
"header-webinars-lists" => "Группы и страницы",
"header-webinars-config" => "Настройки модуля",
"header-webinars-add" => "Добавление",
"header-webinars-edit" => "Редактирование",
 
"header-webinars-add-groupelements"   => "Добавление группы",
"header-webinars-add-item_element"     => "Добавление страницы",
 
"header-webinars-edit-groupelements"   => "Редактирование группы",
"header-webinars-edit-item_element"     => "Редактирование страницы",
 
"header-webinars-activity" => "Изменение активности",
 
'perms-webinars-view' => 'Просмотр страниц',
'perms-webinars-lists' => 'Управление страницами',

'module-webinars' => 'Записи вебинаров',
 
);
 
?>