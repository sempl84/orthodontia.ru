<?php

class webinars extends def_module {
	public $per_page;

	public function __construct() {
		parent::__construct();

		if (cmsController::getInstance() -> getCurrentMode() == "admin") {

			$configTabs = $this -> getConfigTabs();
			if ($configTabs) {
				$configTabs -> add("config");
			}

			$this -> __loadLib("__admin.php");
			$this -> __implement("__webinars");

			$this -> __loadLib("__events_handles.php");
			$this -> __implement("__webinars_events");

		} else {

			$this -> per_page = regedit::getInstance() -> getVal("//modules/webinars/per_page");
		}

	}

	public function groupelements($path = "", $template = "default") {
		if ($this -> breakMe())
			return;
		$element_id = cmsController::getInstance() -> getCurrentElementId();

		templater::pushEditable("webinars", "groupelements", $element_id);
		return $this -> group($element_id, $template) . $this -> listElements($element_id, $template);
	}

	public function item_element() {
		if ($this -> breakMe())
			return;
		$element_id = (int)cmsController::getInstance() -> getCurrentElementId();
		return $this -> view($element_id);
	}

	public function group($elementPath = "", $template = "default", $per_page = false) {
		if ($this -> breakMe())
			return;
		$hierarchy = umiHierarchy::getInstance();
		list($template_block) = def_module::loadTemplates("tpls/webinars/{$template}.tpl", "group");

		$elementId = $this -> analyzeRequiredPath($elementPath);

		$element = $hierarchy -> getElement($elementId);

		templater::pushEditable("webinars", "groupelements", $element -> id);
		return self::parseTemplate($template_block, array('id' => $element -> id), $element -> id);

	}

	public function view($elementPath = "", $template = "default") {
		if ($this -> breakMe())
			return;
		$hierarchy = umiHierarchy::getInstance();
		list($template_block) = def_module::loadTemplates("tpls/webinars/{$template}.tpl", "view");

		$elementId = $this -> analyzeRequiredPath($elementPath);
		$element = $hierarchy -> getElement($elementId);

		templater::pushEditable("webinars", "item_element", $element -> id);
		return self::parseTemplate($template_block, array('id' => $element -> id), $element -> id);
	}

	public function listGroup($elementPath, $template = "default", $per_page = false, $ignore_paging = false) {
		// Код метода
	}

	public function listElements($path = "", $template = "default", $per_page = false, $ignore_paging = false, $ignoreFilter = false) {
		$extendedProperties = array('author_photo','author_info','library_allow','ssylka_na_spikera','video_thumb','anons','publish_time');

		$dealers_user_group_id = 7694;
		$dealers_allow_item_id = 7695;
		$permissions = permissionsCollection::getInstance();
        $objects = umiObjectsCollection::getInstance();

	  	$group_ids=array();
        $user_id = $permissions->getUserId();
        $user = $objects->getObject($user_id);
        if($user instanceof umiObject) {
            $group_ids = $user->groups;
        }

		if(!$per_page) {
			$per_page = $this->per_page;
		}
		$per_page = intval($per_page);

		$data_module = cmsController::getInstance()->getModule('data');
		$cache_path = $_SERVER['DOCUMENT_ROOT'] . '/sys-temp/webinars_list_elements/';

		$params = '' . getRequest('p') . getRequest('FilterByDate') . print_r(getRequest('fields_filter'), true) . $path . $template . $per_page . $ignore_paging . $ignoreFilter;

		$is_dealer = in_array($dealers_user_group_id, $group_ids) ? '_dealer' : '';
		$cache_file_name = 'cache_' . sha1($params) . $is_dealer . '.txt';

		$cache = $data_module->try_get_cache($cache_path, $cache_file_name, 3600);
		if($cache !== null){
			return $cache;
		}

		//
		list($template_block, $template_block_empty, $template_line, $template_archive) = def_module::loadTemplates("webinars/".$template, "lastlist_block", "lastlist_block_empty", "lastlist_item", "lastlist_archive");
		$curr_page = (int) getRequest('p');
		if($ignore_paging) {
			$curr_page = 0;
		}

		$parent_id = $this->analyzeRequiredPath($path);

		if ($parent_id === false && $path != KEYWORD_GRAB_ALL) {
			throw new publicException(getLabel('error-page-does-not-exist', null, $path));
		}

		$this->loadElements($parent_id);

		$webinars = new selector('pages');
		$webinars->types('hierarchy-type')->name('webinars', 'item_element');
		if ($path != KEYWORD_GRAB_ALL) {
			if (is_array($parent_id)) {
				foreach ($parent_id as $parent) {
					$webinars->where('hierarchy')->page($parent)->childs(1);
				}
			} else {
				$webinars->where('hierarchy')->page($parent_id)->childs(1);
			}
		}

		if(!in_array($dealers_user_group_id, $group_ids)){
			$webinars->where('library_allow')->notequals($dealers_allow_item_id); // прячем материалы для дилеров, если пользователь не принадлежит к этой группе пользователей
		}

		if(!empty(getRequest('FilterByDate'))){
			$webinars->where('publish_time')->equals(getRequest('FilterByDate'));
		}

		if(!$ignoreFilter) {
			selectorHelper::detectFilters($webinars);
		}

		$webinars->option('no-permissions')->value(true);

		$webinars->option('load-all-props')->value(true);
		$webinars->limit($curr_page * $per_page, $per_page);

		$result = $webinars->result();
		$total = $webinars->length();

		$umiLinksHelper = $this->umiLinksHelper;
		$umiHierarchy = umiHierarchy::getInstance();


		if(($sz = sizeof($result)) > 0) {
			$block_arr = Array();
			$lines = Array();
			foreach ($result as $element) {
				if (!$element instanceof umiHierarchyElement) {
					continue;
				}
				$object = $element->getObject();

				$line_arr = Array();
				$element_id = $element->getId();

				$line_arr['attribute:id'] = $element_id;
				$line_arr['node:name'] = $element->getName();

				$line_arr['attribute:link'] = $umiHierarchy->getPathById($element_id);
				$line_arr['xlink:href'] = "upage://" . $element_id;
				$line_arr['void:header'] = $lines_arr['name'] = $element->getName();

				if (count($extendedProperties)>0) {
                  	$line_arr['extended']['properties'] = array();
                 	foreach ($extendedProperties as $extendedPropery) {
                       	$property = $object->getPropByName($extendedPropery);
                       	if (!$property instanceof umiObjectProperty) {
							continue;
						}
					   	$propertyXml = translatorWrapper::get($property)->translate($property);
                       	if($propertyXml){
                       		$line_arr['extended']['properties']['nodes:property'][] = $propertyXml;
                  		}
					}
				}

				$lines[] = self::parseTemplate($template_line, $line_arr, $element_id);
				$this->pushEditable("news", "item", $element_id);
				umiHierarchy::getInstance()->unloadElement($element_id);
			}

			if (is_array($parent_id)) {
				list($parent_id) = $parent_id;
			}

			$block_arr['subnodes:items'] = $block_arr['void:lines'] = $lines;
			$block_arr['archive'] = ($total > 0) ? $template_archive : "";
			$parent = $umiHierarchy->getElement($parent_id);
			if ($parent instanceof umiHierarchyElement) {
				$block_arr['archive_link'] = $umiLinksHelper->getLinkByParts($parent);
			}
			$block_arr['total'] = $total;
			$block_arr['per_page'] = $per_page;
			$block_arr['category_id'] = $parent_id;

			$result = $this->parseTemplate($template_block, $block_arr, $parent_id);
		} else {
			$result = $template_block_empty;
		}

		$data_module->save_cache($cache_path, $cache_file_name, $result);
		return $result;
	}

	public function config() {
		return __webinars::config();
	}

	public function getEditLink($element_id, $element_type) {
		switch($element_type) {
			case "groupelements": {
				$link_add = $this->pre_lang . "/admin/webinars/add/{$element_id}/item_element/";
				$link_edit = $this->pre_lang . "/admin/webinars/edit/{$element_id}/";

				return Array($link_add, $link_edit);
				break;
			}

			case "item_element": {
				$link_edit = $this->pre_lang . "/admin/webinars/edit/{$element_id}/";

				return Array(false, $link_edit);
				break;
			}

			default: {
				return false;
			}
		}
	}
};
?>