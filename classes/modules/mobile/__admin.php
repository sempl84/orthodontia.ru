<?php

abstract class __mobile extends baseModuleAdmin {

	public function config() {
		$regedit = regedit::getInstance();
		$params = Array(
			"config" => Array(
				"string:ioslink" => NULL,
				"string:androidlink" => NULL,
			),
			'communi' => Array(
				'string:email' => NULL,
			),
			'promo' => Array(
				'int:regbal' => NULL,
				'int:regbalpromo' => NULL,
				'int:regbalpromoplus' => NULL,
				'int:vkgroup' => NULL,
				'int:report' => NULL,
			)
		);

		$mode = getRequest("param0");
		if ($mode == "do") {
			$params = $this->expectParams($params);
			$regedit->setVar("//modules/mobile/ioslink", $params["config"]["string:ioslink"]);
			$regedit->setVar("//modules/mobile/androidlink", $params["config"]["string:androidlink"]);

			$regedit->setVar("//modules/mobile/email", $params["communi"]["string:email"]);

			$regedit->setVar("//modules/mobile/regbal", $params["promo"]["int:regbal"]);
			$regedit->setVar("//modules/mobile/regbalpromo", $params["promo"]["int:regbalpromo"]);
			$regedit->setVar("//modules/mobile/regbalpromoplus", $params["promo"]["int:regbalpromoplus"]);
			$regedit->setVar("//modules/mobile/vkgroup", $params["promo"]["int:vkgroup"]);
			$regedit->setVar("//modules/mobile/report", $params["promo"]["int:report"]);

			$this->chooseRedirect();
		}
		$params["config"]["string:ioslink"] = (string) $regedit->getVal("//modules/mobile/ioslink");
		$params["config"]["string:androidlink"] = (string) $regedit->getVal("//modules/mobile/androidlink");

		$params["communi"]["string:email"] = (string) $regedit->getVal("//modules/mobile/email");

		$params["promo"]["int:regbal"] = (int) $regedit->getVal("//modules/mobile/regbal");
		$params["promo"]["int:regbalpromo"] = (int) $regedit->getVal("//modules/mobile/regbalpromo");
		$params["promo"]["int:regbalpromoplus"] = (int) $regedit->getVal("//modules/mobile/regbalpromoplus");
		$params["promo"]["int:vkgroup"] = (int) $regedit->getVal("//modules/mobile/vkgroup");
		$params["promo"]["int:report"] = (int) $regedit->getVal("//modules/mobile/report");

		$this->setDataType("settings");
		$this->setActionType("modify");

		$data = $this->prepareData($params, "settings");
		$this->setData($data);
		return $this->doData();
	}

	public function lists() {
		$this->setDataType("list");
		$this->setActionType("view");

		if ($this->ifNotXmlMode())
			return $this->doData();

		$limit = 20;
		$curr_page = getRequest('p');
		$offset = $curr_page * $limit;

		$sel = new selector('pages');
		$sel->types('hierarchy-type')->name('mobile', 'groupelements');
		$sel->types('hierarchy-type')->name('mobile', 'item_element');
		$sel->limit($offset, $limit);

		selectorHelper::detectFilters($sel);

		$data = $this->prepareData($sel->result, "pages");

		$this->setData($data, $sel->length);
		$this->setDataRangeByPerPage($limit, $curr_page);
		return $this->doData();
	}

	public function feedback() {
		$this->setDataType("list");
		$this->setActionType("view");

		if ($this->ifNotXmlMode()){
			return $this->doData();
		}

		$limit = 20;
		$curr_page = getRequest('p');
		$offset = $curr_page * $limit;

		$sel = new selector('pages');
		$sel->types('hierarchy-type')->name('mobile', 'group_feedback');
		$sel->types('hierarchy-type')->name('mobile', 'item_feedback');
		$sel->limit($offset, $limit);

		selectorHelper::detectFilters($sel);

		$data = $this->prepareData($sel->result, "pages");

		$this->setData($data, $sel->length);
		$this->setDataRangeByPerPage($limit, $curr_page);
		return $this->doData();
	}

	public function communication() {
		$this->setDataType("list");
		$this->setActionType("view");
		if ($this->ifNotXmlMode()){
			return $this->doData();
		}

		$limit = getRequest('per_page_limit');
		$curr_page = getRequest('p');
		$offset = $limit * $curr_page;

		$sel = new selector('objects');
		$sel->types('object-type')->name('mobile', 'communi');
		$sel->limit($offset, $limit);
		selectorHelper::detectFilters($sel);

		$this->setDataRange($limit, $offset);

		$data = $this->prepareData($sel->result, "objects");
		$this->setData($data, $sel->length);
		return $this->doData();
	}

	public function add() {
		$parent = $this->expectElement("param0");
		$type = (string) getRequest("param1");
		$mode = (string) getRequest("param2");

		$this->setHeaderLabel("header-mobile-add-" . $type);

		$inputData = Array("type" => $type,
			"parent" => $parent,
			'type-id' => getRequest('type-id'),
			"allowed-element-types" => Array('groupelements', 'item_element', 'item_feedback', 'group_feedback'));

		if ($mode == "do") {
			$element_id = $this->saveAddedElementData($inputData);
			if ($type == "item") {
				umiHierarchy::getInstance()->moveFirst($element_id, ($parent instanceof umiHierarchyElement) ? $parent->getId() : 0);
			}
			$this->chooseRedirect();
		}

		$this->setDataType("form");
		$this->setActionType("create");

		$data = $this->prepareData($inputData, "page");

		$this->setData($data);
		return $this->doData();
	}

	public function edit() {
		$element = $this->expectElement('param0', true);
		$mode = (string) getRequest('param1');

		$this->setHeaderLabel("header-mobile-edit-" . $this->getObjectTypeMethod($element->getObject()));

		$inputData = array(
			'element' => $element,
			'allowed-element-types' => array('groupelements', 'item_element', 'item_feedback', 'group_feedback')
		);

		if ($mode == "do") {
			$this->saveEditedElementData($inputData);
			$this->chooseRedirect();
		}

		$this->setDataType("form");
		$this->setActionType("modify");

		$data = $this->prepareData($inputData, "page");

		$this->setData($data);
		return $this->doData();
	}

	public function del() {
		$elements = getRequest('element');
		if (!is_array($elements)) {
			$elements = array($elements);
		}

		foreach ($elements as $elementId) {
			$element = $this->expectElement($elementId, false, true);

			$params = array(
				"element" => $element,
				"allowed-element-types" => Array('groupelements', 'item_element', 'item_feedback', 'group_feedback')
			);
			$this->deleteElement($params);
		}

		$this->setDataType("list");
		$this->setActionType("view");
		$data = $this->prepareData($elements, "pages");
		$this->setData($data);

		return $this->doData();
	}

	public function delobj() {
		$objects = getRequest('element');
		if (!is_array($objects)) {
			$objects = Array($objects);
		}

		foreach ($objects as $objectId) {
			$object = $this->expectObject($objectId, false, true);

			$params = Array(
				'object' => $object,
				'allowed-element-types' => Array('groupelements', 'item_element', 'communi')
			);

			$this->deleteObject($params);
		}

		$this->setDataType("list");
		$this->setActionType("view");
		$data = $this->prepareData($objects, "objects");
		$this->setData($data);

		return $this->doData();
	}

	public function activity() {
		$elements = getRequest('element');
		if (!is_array($elements)) {
			$elements = array($elements);
		}
		$is_active = getRequest('active');

		foreach ($elements as $elementId) {
			$element = $this->expectElement($elementId, false, true);

			$params = array(
				"element" => $element,
				"allowed-element-types" => Array('groupelements', 'item_element', 'item_feedback', 'group_feedback'),
				"activity" => $is_active
			);
			$this->switchActivity($params);
			$element->commit();
		}

		$this->setDataType("list");
		$this->setActionType("view");
		$data = $this->prepareData($elements, "pages");
		$this->setData($data);

		return $this->doData();
	}

	public function getDatasetConfiguration($param = '') {
		switch ($param) {
			case 'communication':
				$loadMethod = 'communication';
				$idtypes = 'communi';
				$deltype = 'delobj';
				break;
			case 'lists':
				$loadMethod = 'lists';
				$idtypes = 'item_element';
				$deltype = 'del';
				break;
			case 'feedback':
				$loadMethod = 'feedback';
				$idtypes = 'item_feedback';
				$deltype = 'del';
				break;
			default:
				$loadMethod = 'lists';
				$idtypes = 'item_element';
				$deltype = 'del';
		}
		return array(
			'methods' => array(
				array('title' => getLabel('smc-load'), 'forload' => true, 'module' => 'mobile', '#__name' => $loadMethod),
				array('title' => getLabel('smc-delete'), 'module' => 'mobile', '#__name' => $deltype, 'aliases' => 'tree_delete_element,delete'),
				array('title' => getLabel('smc-activity'), 'module' => 'mobile', '#__name' => 'activity', 'aliases' => 'tree_set_activity,activity'),
				array('title' => getLabel('smc-copy'), 'module' => 'content', '#__name' => 'tree_copy_element'),
				array('title' => getLabel('smc-move'), 'module' => 'content', '#__name' => 'tree_move_element'),
				array('title' => getLabel('smc-change-template'), 'module' => 'content', '#__name' => 'change_template'),
				array('title' => getLabel('smc-change-lang'), 'module' => 'content', '#__name' => 'move_to_lang')),
			'types' => array(
				array('common' => 'true', 'id' => $idtypes)
			),
			'stoplist' => array(),
			'default' => 'h1[140px]'
		);
	}
}
?>