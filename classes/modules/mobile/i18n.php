<?php

$i18n = Array(
    "header-mobile-communication" => "Заявки",
    "header-mobile-feedback" => "Отзывы",
    "option-ioslink" => "Ссылка на AppStore",
    "option-androidlink" => "Ссылка на Google Play",
    "option-email" => "Email Администратора",
    "group-communi" => "Настройки отправки заявок",
    "module-mobile" => "Приложение",

    "group-promo" => "Настройки бонусных баллов",
    "option-regbal" => "Баллы за регистрацию пользователя",
    "option-regbalpromo" => "Бонус, если друг зарегестрируется в приложении",
    "option-regbalpromoplus" => "Бонус, если друг воспользуется услугами",
    "option-vkgroup" => "Бонус, за вступление в группу",
    "option-report" => "Бонус, за написание отзыва",


    "label-add-list" => "Добавить группу",
"label-add-item" => "Добавить страницу",
 
"header-mobile-lists" => "Группы и страницы",
"header-mobile-config" => "Настройки модуля",
"header-mobile-add" => "Добавление",
"header-mobile-edit" => "Редактирование",
 
"header-mobile-add-groupelements"   => "Добавление группы",
"header-mobile-add-item_element"     => "Добавление страницы",
 
"header-mobile-edit-groupelements"   => "Редактирование группы",
"header-mobile-edit-item_element"     => "Редактирование страницы",
 
"header-mobile-activity" => "Изменение активности",
 
'perms-mobile-view' => 'Просмотр страниц',
'perms-mobile-lists' => 'Управление страницами',

);

?>