<?php
class mobile extends def_module {
	public $per_page;

	public function __construct() {
		parent::__construct();
		if (cmsController::getInstance()->getCurrentMode() == "admin") {
			$commonTabs = $this->getCommonTabs();
			if ($commonTabs) {
				$commonTabs->add('lists');
				$commonTabs->add('communication');
				$commonTabs->add('feedback');
			}

			$configTabs = $this->getConfigTabs();
			if ($configTabs) {
				$configTabs->add("config");
			}

			$this->__loadLib("__admin.php");
			$this->__implement("__mobile");
		}
	}

	/* ------------------  Информация о пользователе  ----------------- */

	public function userinfo($currentUserId) {
		//Получаем объект, который соответствует текущему пользователю
		$objects = umiObjectsCollection::getInstance();
		$userObject = $objects->getObject($currentUserId);

		//Проверяем, получился ли у нас объект
		if ($userObject instanceof umiObject) {
			$lines = Array();
			$line_arr = Array();
			$line_arr['attribute:email'] = $userObject->getValue("e-mail");
			$line_arr['attribute:lname'] = $userObject->getValue("lname");
			$line_arr['attribute:fname'] = $userObject->getValue("fname");
			$line_arr['attribute:father_name'] = $userObject->getValue("father_name");
			$line_arr['attribute:phone'] = $userObject->getValue("phone");
			$line_arr['attribute:phone_office'] = $userObject->getValue("phone_office");
			$line_arr['attribute:region'] = $userObject->getValue("region");
			$line_arr['attribute:allwho'] = $userObject->getValue("prof_status");
			$line_arr['attribute:country'] = $userObject->getValue("country");
			$line_arr['attribute:city'] = $userObject->getValue("city");
			$groups = $userObject->getValue("groups");
			$line_arr['attribute:groups'] = $groups;
			if (in_array("7694", $groups)) {
				$line_arr['attribute:dealer'] = true;
			} else {
				$line_arr['attribute:dealer'] = false;
			}
			
            $users = cmsController::getInstance()->getModule('users');
            if($users instanceof users) {
                /* @var $users users|site_users_personal*/
                $line_arr['attribute:address'] = $users->_getUserAddress($currentUserId);
            }
            
            return $line_arr;
		}
	}

	/* ------------------  Отображение отзыва  ----------------- */
	public function feedshow($parentElementId) {
		$hierarchyTypes = umiHierarchyTypesCollection::getInstance();
		$hierarchyType = $hierarchyTypes->getTypeByName("mobile", "item_feedback");
		$hierarchyTypeId = $hierarchyType->getId();

		$objectTypes = umiObjectTypesCollection::getInstance();
		$objectTypeId = $objectTypes->getTypeByHierarchyTypeId($hierarchyTypeId);

		$hierarchy = umiHierarchy::getInstance();

		//Создаем и подготавливаем выборку
		$sel = new umiSelection;
		$sel->addElementType($hierarchyTypeId); //Добавляет поиск по иерархическому типу
		$sel->addPermissions(); //Говорим, что обязательно нужно учитывать права доступа

		$sel->setOrderByObjectId(false);
		//Получаем результаты
		$result = umiSelectionsParser::runSelection($sel); //Массив id объектов
		$total = umiSelectionsParser::runSelectionCounts($sel); //Количество записей

		$datenow = date(U);

		//Выводим список  на экран
		if (($sz = sizeof($result)) > 0) {
			$block_arr = Array();
			$lines = Array();
			for ($i = 00; $i < $sz; $i++) {
				$element_id = $result[$i];
				$element = umiHierarchy::getInstance()->getElement($element_id);
				if (!$element){
					continue;
				}
				$line_arr = Array();
				$line_arr['attribute:id'] = $element_id;
				if ($i < 10) {
					$line_arr['i'] = '0' . $i;
					$impr = '0' . $i;
				} else {
					$line_arr['i'] = $i;
					$impr = $i;
				}
				$line_arr['name'] = $element->getName();
				$line_arr['otzyv'] = $element->getValue('otzyv');
				$line_arr['usluga'] = $element->getValue('usluga');
				$line_arr['imya_otpravitelya'] = $element->getValue('imya_otpravitelya');
				$ptime = $element->getValue("publish_time");
				$formatedptime = $ptime->getFormattedDate("d.m.Y");
				$line_arr['publish_time'] = $formatedptime;

				$lines['nodes:item'][$impr] = $line_arr;
			}
			$block_arr['lines'] = $lines;

			return $block_arr;
		}
		return $block_arr;
	}

	/* ------------------  Отправка отзыва  ----------------- */
	public function sendfeed($parentElementId) {
		//Данные о сообщении
		$title = getRequest('title');
		$imya = getRequest('imya_otpravitelya');
		$email = getRequest('author_email');
		$usluga = getRequest('usluga');
		$kommentarij = getRequest('comment');
		$permissions = permissionsCollection::getInstance();
		$currentUserId = "334";
		$nowtime = date("U");

		$hierarchyTypes = umiHierarchyTypesCollection::getInstance();
		$hierarchyType = $hierarchyTypes->getTypeByName("mobile", "item_feedback");
		$hierarchyTypeId = $hierarchyType->getId();

		$hierarchy = umiHierarchy::getInstance();

		$newElementId = $hierarchy->addElement($parentElementId, $hierarchyTypeId, $title, $newElementAltName);
		if ($newElementId === false) {
			return "Ne udalos sozdat stranicu";
		}

		//Установим права на страницу в состояние "по умолчанию"
		$permissions = permissionsCollection::getInstance();
		$permissions->setElementPermissions('334', $newElementId, '1');

		//Получим экземпляр страницы
		$newElement = $hierarchy->getElement($newElementId, true);

		if ($newElement instanceof umiHierarchyElement) {
			$newElement->setValue("imya_otpravitelya", $imya);
			$newElement->setValue("usluga", $usluga);
			$newElement->setValue("otzyv", $kommentarij);
			$newElement->setValue("publish_time", $nowtime);
			$newElement->setValue("polzovatel", $currentUserId);

			//Укажем, что страница является активной
			$newElement->setIsActive(true);

			//Подтвердим внесенные изменения
			$newElement->commit();

			//Покажем адрес новой страницы
			return "Success";
		} else {
			return "Ne udalos poluchit ekzempliar stranici #{$newElementId}.";
		}

		/*
		  //куда отправлять сообщение
		  $to = regedit::getInstance()->getVal("//modules/mobile/email");
		  $from = 'noreaply@mobileapps.your';

		  $headers = 'Content-type: text/html; charset=utf-8 ' . "\r\n";
		  $headers .= 'From: Отзывы Мобильного приложения <noreaply@mobileapps.your>  ' . "\r\n";

		  //Формируем сообщение

		  //Отправляем сообщение
		  mail($to, 'Оставлен новый отзыв с мобильного приложения ',
		  'С мобильного приложения поступил новый заказ, данные заказа следующие: <br/> Имя: ' . $imya . '<br/> Услуга: ' . $usluga . ' <br/> Телефон: ' . $telefon . ' <br/> Удобная дата: ' . $ydata . '<br/> Комментарий: ' . $kommentarij . '<br/> Прикрепленное Изображение: ' . $host . '/upload/' . $imgname . '<br/> Промо страница:' . $promoname ,
		  $headers);
		  return('Сообщение отправлено');
		 */
	}

	/* ------------------  Отправка сообщения в техподдержку  ----------------- */
	public function supportmessage() {
		//Данные о сообщении
		$lname = getRequest('lname');
		$email = getRequest('email');
		$namecli = getRequest('namecli');
		$adres = getRequest('adres');
		$phone2 = getRequest('phone2');
		$city = getRequest('city');
		$question = getRequest('question');

		//куда отправлять сообщение
        $to = 'natalia.postnikova@envistaco.com';
		$from = $email;

		$headers = 'Content-type: text/html; charset=utf-8 ' . "\r\n";
		$headers .= 'From: Mobile App ORMCO <noreaply@orthodontia.ru>  ' . "\r\n";

		//Формируем сообщение
		//Отправляем сообщение
		mail($to, 'Вопрос от пользователя мобильного приложения ', 'С мобильного приложения поступил новый вопрос, данные вопроса следующие:
            <br/> ФИО: ' . $lname .
				'<br/> Email: ' . $email .
				'<br/> Название клиники: ' . $namecli .
				'<br/> Адрес клиники: ' . $adres .
				'<br/> Телефон: ' . $phone2 .
				'<br/> Город: ' . $city .
				'<br/> Вопрос: ' . $question, $headers);
		return ('Success');
	}

	/* ------------------  Отправка сообщения в техподдержку  ----------------- */
	public function clinickursmessage() {
		//Данные о сообщении
		$lname = getRequest('lname');
		$email = getRequest('email');
		$namecli = getRequest('namecli');
		$phone2 = getRequest('phone2');

		//куда отправлять сообщение
		$to = 'pavel.rumyantsev@ormco.com';
		$from = $email;

		$headers = 'Content-type: text/html; charset=utf-8 ' . "\r\n";
		$headers .= 'From: Mobile Форма клинических курсов <noreaply@orthodontia.ru>  ' . "\r\n";

		//Формируем сообщение
		//Отправляем сообщение
		mail($to, 'Форма клинических курсов ', 'С мобильного приложения через форму клинических курсов отправлена заявка, данные  следующие:
            <br/> ФИО: ' . $lname .
				'<br/> Email: ' . $email .
				'<br/> Название курса: ' . $namecli .
				'<br/> Телефон: ' . $phone2, $headers);
		return ('Success');
	}

	/* ------------------  Отправка сообщения в техподдержку  ----------------- */

	public function supportmessagecode() {
		//Данные о сообщении
		$lname = getRequest('lname');
		$email = getRequest('email');
		$namecli = getRequest('namecli');
		$adres = getRequest('adres');
		$phone2 = getRequest('phone2');
		$city = getRequest('city');
		$question = getRequest('question');


		//куда отправлять сообщение
		$to = 'pavel.rumyantsev@ormco.com';
		$from = $email;

		$headers = 'Content-type: text/html; charset=utf-8 ' . "\r\n";
		$headers .= 'From: Mobile App ORMCO <noreaply@orthodontia.ru>  ' . "\r\n";

		//Формируем сообщение
		//Отправляем сообщение
		mail($to, 'Вопрос от пользователя мобильного приложения ', 'Не пришел код активации e-mail в приложении:
            <br/> ФИО: ' . $lname .
				'<br/> Email: ' . $email, $headers);
		return ('Success');
	}

	/* ------------------  Menu  ----------------- */

	public function catitems($parentElementId, $width = 'auto', $height = '400') {
		$hierarchyTypes = umiHierarchyTypesCollection::getInstance();
		$hierarchyType = $hierarchyTypes->getTypeByName("catalog", "category");
		$hierarchyTypeId = $hierarchyType->getId();

		//Получаем базовый тип данных для новостей
		$objectTypes = umiObjectTypesCollection::getInstance();
		$objectTypeId = $objectTypes->getTypeByHierarchyTypeId($hierarchyTypeId);
		$objectType = $objectTypes->getType($objectTypeId);

		$hierarchy = umiHierarchy::getInstance();

		//Создаем и подготавливаем выборку
		$sel = new umiSelection;
		$sel->addElementType($hierarchyTypeId); //Добавляет поиск по иерархическому типу
		$sel->addHierarchyFilter($parentElementId); //Устанавливаем поиск по разделу
		$sel->addPermissions(); //Говорим, что обязательно нужно учитывать права доступа
		//Получаем результаты
		$result = umiSelectionsParser::runSelection($sel); //Массив id объектов
		$total = umiSelectionsParser::runSelectionCounts($sel); //Количество записей

		$datenow = date(U);

		//Выводим список  на экран
		if (($sz = sizeof($result)) > 0) {
			$block_arr = Array();
			$lines = Array();
			for ($i = 0; $i < $sz; $i++) {
				$element_id = $result[$i];
				$element = umiHierarchy::getInstance()->getElement($element_id);
				if (!$element)
					continue;
				$line_arr = Array();
				$line_arr['attribute:id'] = $element_id;
				if ($i < 10) {
					$line_arr['i'] = '00' . $i;
					$impr = '00' . $i;
				} else {
					$line_arr['i'] = '0' . $i;
					$impr = '0' . $i;
				}
				$line_arr['name'] = $element->getName();
				$elh = $element->getValue("izobrazhenie");

				if ($elh) {
					$imgsrc = $elh->getFilePath();
					$sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
					$line_arr['thumb'] = $sourcthmb;
				}

				$pages = new selector('pages');
				$pages->types('hierarchy-type')->name('catalog', 'category');
				$pages->where('hierarchy')->page($element_id)->childs(1);
				$countcat = $pages->length;
				$resultp = $pages->result();
				$line_arr2 = Array();
				$line_arr3 = Array();
				if ($countcat > 0) {
					for ($k = 0; $k < $countcat; $k++) {
						$line_arr2['names'] = $resultp[$k]->getName();
						$newelid = $resultp[$k]->id;
						$line_arr2['id'] = $newelid;
						$pagesn = new selector('pages');
						$pagesn->types('hierarchy-type')->name('catalog', 'category');
						$pagesn->where('hierarchy')->page($newelid)->childs(1);
						$countcatn = $pagesn->length;
						if ($countcatn > 0) {
							$line_arr2['moremenu'] = 1;
						} else {
							$line_arr2['moremenu'] = 0;
						}

						if ($k < 10) {
							$nk = '00' . $k;
						} else {
							$nk = '0' . $k;
						}

						$line_arr3['nodes:itemmenu'][$nk] = $line_arr2;
					}
					$line_arr['nodes:menu'][$impr] = $line_arr3;
				} else {
					$line_arr['nodes:nomenu'][$impr] = $line_arr3;
				}

				$lines['nodes:item'][$impr] = $line_arr;
			}
			$block_arr['lines'] = $lines;
			$block_arr['numpages'] = umiPagenum::generateNumPage($total, $per_page);
			$block_arr['total'] = $total;
			$block_arr['per_page'] = $per_page;
			$block_arr['category_id'] = $category_id;

			if ($type_id) {
				$block_arr['type_id'] = $type_id;
			}
			return $block_arr;
		} else {
			$block_arr['numpages'] = umiPagenum::generateNumPage(0, 0);
			$block_arr['lines'] = "";
			$block_arr['total'] = 0;
			$block_arr['per_page'] = 0;
			$block_arr['category_id'] = $category_id;
		}
		return $block_arr;
	}

	/*
	  public function guide($id) {
	  $objectsCollection = umiObjectsCollection::getInstance();
	  $guideitm = $objectsCollection->getGuidedItems($id);
	  $block_arr = Array();
	  $block_arr['category_id'] = $guideitm;
	  return $block_arr;
	  }
	 */

	public function guide($id) {
		$objectsCollection = umiObjectsCollection::getInstance();
		$guideitms = $objectsCollection->getGuidedItems($id);
		$count = count($guideitms);
		$block_arr = Array();
		/*
		  for($k = 0; $k < $count; $k++) {
		  $block_arr['nodes:item'][$k] = $guideitms;
		  } */
		foreach ($guideitms as $Id => $key) {
			$line_arr2[$Id]['id'] = $Id;
			$line_arr2[$Id]['key'] = $key;
		}
		$lines['nodes:item'] = $line_arr2;
		return $lines;
	}

	/* ------------------  Работа со страницами  ----------------- */

	public function pageitems($parentElementId, $curpage = '0', $width = 'auto', $height = '400') {
		$perpage = 999;
		$pages = new selector('pages');
		$pages->types('hierarchy-type')->name('catalog', 'object');
		$pages->limit($curpage, $perpage);
		$pages->where('hierarchy')->page($parentElementId)->childs(3);
		$countcat = $pages->length;
		if ($countcat < $perpage) {
			$rescount = $countcat;
		} else if ($perpage * $curpage > $countcat) {
			$rescount = $countcat - ($perpage * ($curpage));
		} else {
			$rescount = $perpage;
		}
		$resultp = $pages->result();
		$line_arr2 = Array();
		for ($k = 0; $k < $rescount; $k++) {
			$elh = $resultp[$k]->getValue("photo");

			$line_arr2['names'] = $resultp[$k]->getName();
			$date_begin = $resultp[$k]->getValue("publish_date")->getFormattedDate(U);
			$date_end = $resultp[$k]->getValue("finish_date")->getFormattedDate(U);
			$line_arr2['dateru'] = $this->dateruPeriod($date_begin, $date_end);
			$max_reserv = $resultp[$k]->getValue("max_reserv");
			$curr_reserv = $resultp[$k]->getValue("curr_reserv");
			$line_arr2['reserv'] = $max_reserv - $curr_reserv;

			$idauto = $resultp[$k]->getValue('speaker');
			foreach ($idauto as $Id) {
				$objects = umiObjectsCollection::getInstance();
				$autoObject = $objects->getObject($Id);
				$line_arr2['speaker'] = $autoObject->name;
				$line_arr2['speakerdescr'] = $autoObject->getValue('full_desrc');
				$line_arr2['speakerdescr2'] = $autoObject->getValue('full_descr2');
				$elh = $autoObject->getValue("photo");
			}

			$gorod_in = $resultp[$k]->getValue('gorod_in');
			$objects = umiObjectsCollection::getInstance();
			$gorod_inObject = $objects->getObject($gorod_in);
			$line_arr2['gorodin'] = $gorod_inObject->name;
			$line_arr2['gorodinid'] = $gorod_in;

			$line_arr2['mestoprovedeniya'] = $resultp[$k]->getValue("mesto_provedeniya");

			$event_type = $resultp[$k]->getValue('event_type');
			$objects = umiObjectsCollection::getInstance();
			$event_typeObject = $objects->getObject($event_type);
			$line_arr2['eventtype'] = $event_typeObject->name;
			$line_arr2['eventtypeid'] = $event_type;

			$organizer = $resultp[$k]->getValue('organizer');
			$organizerObject = $objects->getObject($organizer);
			$line_arr2['organizer'] = $organizerObject->name;
			$line_arr2['organizerimg'] = $organizerObject->getValue("logo");
			$line_arr2['organizerid'] = $organizer;

			$level = $resultp[$k]->getValue('level');
			$levelObject = $objects->getObject($level);
			$line_arr2['level'] = $levelObject->name;
			$line_arr2['levelid'] = $level;

			$level_shoo = $resultp[$k]->getValue('level_shoo');
			$level_shooObject = $objects->getObject($level_shoo);
			$line_arr2['levelshoo'] = $level_shooObject->name;
			$line_arr2['levelshooid'] = $level_shoo;

			$line_arr2['description'] = $resultp[$k]->getValue("description");
			$line_arr2['aboutreg'] = $resultp[$k]->getValue("about_reg");

			$line_arr2['id'] = $resultp[$k]->id;
			$line_arr2['price'] = $resultp[$k]->getValue("price");

			if ($elh) {
				$imgsrc = $elh->getFilePath();
				$sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
				$line_arr2['thumb'] = $sourcthmb;
			} else {

				$line_arr2['thumb'] = "/images/nofoto_260_auto_5_80.jpg";
			}
			if ($k < 10) {
				//$nk = '0' . $k;
				$nk = $k;
			} else {
				$nk = $k;
			}

			$lines['nodes:item'][$nk] = $line_arr2;
		}

		$block_arr['lines'] = $lines;
		$block_arr['numpages'] = $curpage;
		$block_arr['total'] = $countcat;

		return $block_arr;
	}

	public function getcity($parentElementId, $curpage = '0', $width = 'auto', $height = '400') {
		$perpage = 999;
		$pages = new selector('pages');
		$pages->types('hierarchy-type')->name('catalog', 'object');
		$pages->limit($curpage, $perpage);
		$pages->where('hierarchy')->page($parentElementId)->childs(3);
		$countcat = $pages->length;
		$resultp = $pages->result();
		$line_arr2 = Array();
		for ($k = 0; $k < $countcat; $k++) {
			$gorod_in = $resultp[$k]->getValue('gorod_in');
			$objects = umiObjectsCollection::getInstance();
			$gorod_inObject = $objects->getObject($gorod_in);
			$line_arr2['gorodin'] = $gorod_inObject->name;
			$line_arr2['gorodinid'] = $gorod_in;
			$lines['nodes:item'][$k] = $line_arr2;
		}

		$result = array_unique($lines['nodes:item']);

		for ($k = 0; $k < 6; $k++) {
			$lines2['nodes:item2'][$k] = $result[$k];
		}

		$block_arr['lines'] = $lines2;

		return $block_arr;
	}

	public function getMonthRu($month, $year, $type = 0) {
		// Проверка существования месяца
		if (!checkdate($month, 1, $year)) {
			throw new publicException("Проверьте порядок ввода даты.");
		}
		$months_ru = Array(1 => 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
		$months_ru1 = Array(1 => 'январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь');
		if ($type == 1) {
			return $months_ru1[$month];
		}
		return $months_ru[$month];
	}

	public function dateru($time) {
		if (!$time)
			return;
		$day = date('j', $time);
		$month = date('n', $time);
		$year = date('Y', $time);
		$hour = date('g', $time);
		$minutes = date('i', $time);
		$seconds = date('s', $time);

		$temp = Array(
			'month' => $this->getMonthRu($month, $year),
			'day' => $day,
			'year' => $year,
			'hour' => $hour,
			'minutes' => $minutes,
			'seconds' => $seconds
		);
		return def_module::parseTemplate('', $temp);
	}

	// test data:
	// 1352204856 Tue, 06 Nov 2012 12:27:36 GMT
	// 1352377656 Thu, 08 Nov 2012 12:27:36 GMT
	// 1355660856 Sun, 16 Dec 2012 12:27:36 GMT
	// 1357475256 Sun, 06 Jan 2013 12:27:36 GMT
	// 1383740856 Wed, 06 Nov 2013 12:27:36 GMT
	public function dateruPeriod($timeBegin = NULL, $timeEnd = NULL) {
		if (!$timeBegin || !$timeEnd)
			return "-";
		if (!is_numeric($timeBegin) || !is_numeric($timeEnd))
			return "=";
		if ($timeEnd < $timeBegin) {
			$tmp = $timeEnd;
			$timeEnd = $timeBegin;
			$timeBegin = $tmp;
		}

		$dayBegin = date('d', $timeBegin);
		$monthBegin = date('n', $timeBegin);
		$yearBegin = date('Y', $timeBegin);
		$dayEnd = date('d', $timeEnd);
		$monthEnd = date('n', $timeEnd);
		$yearEnd = date('Y', $timeEnd);

		$timeEnd_forCalc = strtotime("$dayEnd-$monthEnd-$yearEnd ");
		$timeBegin_forCalc = strtotime("$dayBegin-$monthBegin-$yearBegin ");

		$yearCurrent = date('Y');

		if (($monthBegin != $monthEnd) || ($yearBegin != $yearEnd)) {
			$firstLine = $dayBegin . ' ' . $this->getMonthRu($monthBegin, $yearBegin) . ' ';
			$secondLine = $dayEnd . ' ' . $this->getMonthRu($monthEnd, $yearEnd);
			/* if($yearBegin != $yearEnd) {
			  $secondLine .= ' '.$yearEnd.' г.';
			  } */
			$firstLine .= '-';
		} else {
			$firstLine = ($dayBegin != $dayEnd) ? $dayBegin . '-' . $dayEnd : $dayBegin;
			$secondLine = $this->getMonthRu($monthBegin, $yearBegin);
		}

		//if(($yearEnd != $yearCurrent) && ($yearBegin == $yearEnd)) {
		$secondLine .= ' ' . $yearEnd . ' г.';
		//}
		$days = (int) (($timeEnd_forCalc - $timeBegin_forCalc) / 86400) + 1; // секунд в 24 часах
		$temp = Array(
			'firstline' => $firstLine,
			'secondline' => $secondLine,
			'days' => $days . ' ' . $this->wordDays($days)
		);
		return $temp;
	}

	public function wordDays($days) {
		$units = $days - (int) ($days / 10) * 10;
		$tens = $days - (int) ($days / 100) * 100 - $units;
		if ($tens == 10)
			return 'дней'; // 11-19 дней

		if ($units == 1)
			return 'день'; // 1 21 31... день
		if (($units > 1) && ($units < 5))
			return 'дня'; // 2 3 4 22 23 24 дня
		return 'дней'; // 5 6 7 8 9 20 дней
	}

	public function pageitemsmin($parentElementId, $curpage, $perpage = 20, $width = 'auto', $height = '400') {
		$pages = new selector('pages');
		$pages->types('hierarchy-type')->name('news', 'item');
		$pages->limit($curpage, $perpage);
		$pages->where('hierarchy')->page($parentElementId)->childs(3);
		$pages->order('ord');
		$countcat = $pages->length;
		if ($countcat < $perpage) {
			$rescount = $countcat;
		} else if ($perpage * $curpage > $countcat) {
			$rescount = $countcat - ($perpage * ($curpage));
		} else {
			$rescount = $perpage;
		}
		$resultp = $pages->result();
		$line_arr2 = Array();
		for ($k = 0; $k < $rescount; $k++) {
			$elh = $resultp[$k]->getValue("header_pic");

			$line_arr2['names'] = $resultp[$k]->getName();
			$line_arr2['id'] = $resultp[$k]->id;
			$line_arr2['link'] = $resultp[$k]->link;
			$line_arr2['anons'] = $resultp[$k]->getValue("anons");
			$line_arr2['content'] = $resultp[$k]->getValue("content");
			$line_arr2['contentmobile'] = $resultp[$k]->getValue("content_mobile");
			$ptime = $resultp[$k]->getValue("publish_time");
			$line_arr2['publishtime'] = $ptime->getFormattedDate("d.m.Y");

			if ($elh) {
				$imgsrc = $elh->getFilePath();
				$sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
				$line_arr2['thumb'] = $sourcthmb;
			} else {

				$line_arr2['thumb'] = "/images/nofoto_260_auto_5_80.jpg";
			}
			if ($k < 10) {
				$nk = '00' . $k;
			} else {
				$nk = '0' . $k;
			}

			$lines['nodes:item'][$nk] = $line_arr2;
		}

		$block_arr['lines'] = $lines;
		$block_arr['numpages'] = $curpage;
		$block_arr['total'] = $countcat;

		return $block_arr;
	}

	public function checknews($parentElementId, $curpage, $perpage = 20, $width = 'auto', $height = '400') {
		$timecreate = getRequest('create');
		if (isset($timecreate)) {
			$timecreate = getRequest('create') + 1;
		} else {
			$timecreate = '0';
		}
		$pages = new selector('pages');
		$pages->types('hierarchy-type')->name('news', 'item');
		$pages->limit($curpage, $perpage);
		$pages->where('hierarchy')->page($parentElementId)->childs(3);
		$pages->where('publish_time')->more($timecreate);
		$pages->order('publish_time')->desc();
		$countcat = $pages->length;
		if ($countcat < $perpage) {
			$rescount = $countcat;
		} else if ($perpage * $curpage > $countcat) {
			$rescount = $countcat - ($perpage * ($curpage));
		} else {
			$rescount = $perpage;
		}
		$resultp = $pages->result();
		$line_arr2 = Array();
		$line_arr3 = Array();
		for ($k = 0; $k < $rescount; $k++) {
			$ptime = $resultp[$k]->getValue("publish_time");
			$formatedptime = $ptime->getFormattedDate("U");
			$line_arr2['publish_time'] = $formatedptime;

			if ($k < 10) {
				$nk = $k;
			} else {
				$nk = $k;
			}

			$lines['nodes:item'][$nk] = $line_arr2;
		}

		$block_arr['lines'] = $lines;
		$block_arr['numpages'] = $curpage;
		$block_arr['total'] = $countcat;

		return $block_arr;
	}

	public function slider($parentElementId, $curpage, $perpage = 8, $width = '214', $height = 'auto') {

		$pages = new selector('pages');
		$pages->types('hierarchy-type')->name('slider', 'item_element');
		$pages->limit($curpage, $perpage);
		$pages->where('hierarchy')->page($parentElementId)->childs(3);
		$pages->order('ord');
		$countcat = $pages->length;
		if ($countcat < $perpage) {
			$rescount = $countcat;
		} else if ($perpage * $curpage > $countcat) {
			$rescount = $countcat - ($perpage * ($curpage));
		} else {
			$rescount = $perpage;
		}
		$resultp = $pages->result();
		$line_arr2 = Array();
		$line_arr3 = Array();
		for ($k = 0; $k < $rescount; $k++) {
			$elh = $resultp[$k]->getValue("right_img");

			$line_arr2['names'] = $resultp[$k]->getName();
			$line_arr2['id'] = $resultp[$k]->id;
			$line_arr2['content'] = $resultp[$k]->getValue("content");
			$line_arr2['link_for_slide'] = $resultp[$k]->getValue("link_for_slide");

			if ($elh) {
				$imgsrc = $elh->getFilePath();
				$sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
				$line_arr2['thumb'] = $sourcthmb;
			} else {

				$line_arr2['thumb'] = "/images/nofoto_260_auto_5_80.jpg";
			}
			if ($k < 10) {
				$nk = '00' . $k;
			} else {
				$nk = '0' . $k;
			}

			$lines['nodes:item'][$nk] = $line_arr2;
		}

		$block_arr['lines'] = $lines;
		$block_arr['numpages'] = $curpage;
		$block_arr['total'] = $countcat;

		return $block_arr;
	}

	public function listElements($path = "", $template = "default", $per_page = false, $ignore_paging = false) {
		if (!$per_page)
			$per_page = $this->per_page;
		$per_page = intval($per_page);

		//
		list($template_block, $template_block_empty, $template_line, $template_archive) = def_module::loadTemplates("library/" . $template, "lastlist_block", "lastlist_block_empty", "lastlist_item", "lastlist_archive");
		$curr_page = (int) getRequest('p');
		if ($ignore_paging)
			$curr_page = 0;


		$parent_id = $this->analyzeRequiredPath($path);

		if ($parent_id === false && $path != KEYWORD_GRAB_ALL) {
			throw new publicException(getLabel('error-page-does-not-exist', null, $path));
		}

		$this->loadElements($parent_id);

		$library = new selector('pages');
		$library->types('hierarchy-type')->name('library', 'item_element');
		$library->option('no-permissions')->value(true);
		if ($path != KEYWORD_GRAB_ALL) {
			if (is_array($parent_id)) {
				foreach ($parent_id as $parent) {
					$library->where('hierarchy')->page($parent)->childs(1);
				}
			} else {
				$library->where('hierarchy')->page($parent_id)->childs(1);
			}
		}

		// TODO UH
		selectorHelper::detectFilters($library);
		$library->option('load-all-props')->value(true);
		$library->limit($curr_page * $per_page, $per_page);

		$result = $library->result();
		$total = $library->length();

		$umiLinksHelper = $this->umiLinksHelper;
		$umiHierarchy = umiHierarchy::getInstance();

		if (($sz = sizeof($result)) > 0) {
			$block_arr = Array();
			$lines = Array();
			foreach ($result as $element) {
				if (!$element instanceof umiHierarchyElement) {
					continue;
				}
				$line_arr = Array();
				$element_id = $element->getId();

				$line_arr['attribute:id'] = $element_id;
				$line_arr['node:name'] = $element->getName();
				$line_arr['attribute:link'] = $umiLinksHelper->getLinkByParts($element);
				$line_arr['content'] = $element->getValue('content');
				$line_arr['shortdesrc'] = $element->getValue('short_desrc');

				$line_arr['file1'] = $element->getValue('file_1');
				$line_arr['file1name'] = $element->getValue('file_1_name');
				$line_arr['file2'] = $element->getValue('file_2');
				$line_arr['file2name'] = $element->getValue('file_2_name');
				$line_arr['file3'] = $element->getValue('file_3');
				$line_arr['file3name'] = $element->getValue('file_3_name');
				$line_arr['file4'] = $element->getValue('file_4');
				$line_arr['file4name'] = $element->getValue('file_4_name');
				$line_arr['file5'] = $element->getValue('file_5');
				$line_arr['file5name'] = $element->getValue('file_5_name');

				$line_arr['video'] = $element->getValue('video');
				$line_arr['void:header'] = $lines_arr['name'] = $element->getName();

				$line_arr['libraryallow'] = $element->getValue('library_allow');
				$line_arr['librarytype'] = $element->getValue('library_type');


				$lent_name = "";
				$lent_link = "";
				$lent_id = $element->getParentId();

				if ($lent_element = $umiHierarchy->getElement($lent_id)) {
					$lent_name = $lent_element->getName();
					$lent_link = $umiLinksHelper->getLinkByParts($lent_element);
				}

				$line_arr['attribute:lent_id'] = $lent_id;
				$line_arr['attribute:lent_name'] = $lent_name;
				$line_arr['attribute:lent_link'] = $lent_link;

				$lines[] = self::parseTemplate($template_line, $line_arr, $element_id);
				$this->pushEditable("news", "item", $element_id);
				umiHierarchy::getInstance()->unloadElement($element_id);
			}

			if (is_array($parent_id)) {
				list($parent_id) = $parent_id;
			}

			$block_arr['subnodes:items'] = $block_arr['void:lines'] = $lines;
			$block_arr['archive'] = ($total > 0) ? $template_archive : "";
			$parent = $umiHierarchy->getElement($parent_id);
			if ($parent instanceof umiHierarchyElement) {
				$block_arr['archive_link'] = $umiLinksHelper->getLinkByParts($parent);
			}
			$block_arr['total'] = $total;
			$block_arr['per_page'] = $per_page;
			$block_arr['category_id'] = $parent_id;

			return $this->parseTemplate($template_block, $block_arr, $parent_id);
		} else {
			return $template_block_empty;
		}
	}

	public function webinars($path = "", $template = "default", $per_page = false, $ignore_paging = false) {
		if (!$per_page){
			$per_page = $this->per_page;
        }

		$per_page = intval($per_page);

		//
		list($template_block, $template_block_empty, $template_line, $template_archive) = def_module::loadTemplates("library/" . $template, "lastlist_block", "lastlist_block_empty", "lastlist_item", "lastlist_archive");
		$curr_page = (int) getRequest('p');
		if ($ignore_paging){
			$curr_page = 0;
        }

		$parent_id = $this->analyzeRequiredPath($path);

		if ($parent_id === false && $path != KEYWORD_GRAB_ALL) {
			throw new publicException(getLabel('error-page-does-not-exist', null, $path));
		}

		$this->loadElements($parent_id);

		$library = new selector('pages');
		$library->types('hierarchy-type')->name('webinars', 'item_element');
		if ($path != KEYWORD_GRAB_ALL) {
			if (is_array($parent_id)) {
				foreach ($parent_id as $parent) {
					$library->where('hierarchy')->page($parent)->childs(1);
				}
			} else {
				$library->where('hierarchy')->page($parent_id)->childs(1);
			}
		}

		// TODO UH
		selectorHelper::detectFilters($library);
		$library->option('load-all-props')->value(true);
		$library->option('no-permissions')->value(true);
		$library->limit($curr_page * $per_page, $per_page);

		$result = $library->result();
		$total = $library->length();

		$umiLinksHelper = $this->umiLinksHelper;
		$umiHierarchy = umiHierarchy::getInstance();

		if (($sz = sizeof($result)) > 0) {
			$block_arr = Array();
			$lines = Array();
			$elh = '';
			foreach ($result as $element) {
				if (!$element instanceof umiHierarchyElement) {
					continue;
				}
				$line_arr = Array();
				$element_id = $element->getId();

				$line_arr['attribute:id'] = $element_id;
				$line_arr['node:name'] = $element->getName();
				$line_arr['attribute:link'] = $element->link;
				$line_arr['content'] = $element->getValue('content');
				$line_arr['anons'] = $element->getValue('anons');
				$ptime = $element->getValue("publish_time");
				$formatedptime = $ptime->getFormattedDate("d.m.Y");
				$line_arr['publish_time'] = $formatedptime;

				$idauto = $element->getValue('ssylka_na_spikera');
				$elh = '';
				if ($idauto) {
					$objects = umiObjectsCollection::getInstance();
					$autoObject = "";
					$autoObject = $objects->getObject($idauto);
					$line_arr['speaker'] = $autoObject->name;
					$line_arr['speakerdescr'] = $autoObject->getValue('full_desrc');
					$line_arr['speakerdescr2'] = $autoObject->getValue('full_descr2');
					$elh = $autoObject->getValue("photo");
				}

				if ($elh) {
					$imgsrc = $elh->getFilePath();
					$sourcthmb = $this->makeThumbnailFullmy($imgsrc, 'auto', 400);
					$line_arr['thumb'] = $sourcthmb;
				}

				$line_arr['author_photo'] = $element->getValue('author_photo');
				$line_arr['author_info'] = $element->getValue('author_info');
				$line_arr['video'] = preg_replace('/src=\"([^\"]*)\"/', 'src="$1?enablejsapi=1"', $element->getValue('video'));
				$line_arr['video_thumb'] = $element->getValue('video_thumb');

				$line_arr['libraryallow'] = $element->getValue('library_allow');
				$line_arr['librarytype'] = $element->getValue('library_type');


				$lent_name = "";
				$lent_link = "";
				$lent_id = $element->getParentId();

				if ($lent_element = $umiHierarchy->getElement($lent_id)) {
					$lent_name = $lent_element->getName();
					$lent_link = $lent_element->link;
				}

				$line_arr['attribute:lent_id'] = $lent_id;
				$line_arr['attribute:lent_name'] = $lent_name;
				$line_arr['attribute:lent_link'] = $lent_link;

				$lines[] = self::parseTemplate($template_line, $line_arr, $element_id);
				$this->pushEditable("news", "item", $element_id);
				umiHierarchy::getInstance()->unloadElement($element_id);
			}

			if (is_array($parent_id)) {
				list($parent_id) = $parent_id;
			}

			$block_arr['subnodes:items'] = $block_arr['void:lines'] = $lines;
			$block_arr['archive'] = ($total > 0) ? $template_archive : "";
			$parent = $umiHierarchy->getElement($parent_id);
			if ($parent instanceof umiHierarchyElement) {
				$block_arr['archive_link'] = $parent->link;
			}
			$block_arr['total'] = $total;
			$block_arr['per_page'] = $per_page;
			$block_arr['category_id'] = $parent_id;

			return $this->parseTemplate($template_block, $block_arr, $parent_id);
		} else {
			return $template_block_empty;
		}
	}

	public function cliniccourse($path = "", $template = "default", $per_page = false, $ignore_paging = false) {
		if (!$per_page)
			$per_page = $this->per_page;
		$per_page = intval($per_page);

		//
		list($template_block, $template_block_empty, $template_line, $template_archive) = def_module::loadTemplates("library/" . $template, "lastlist_block", "lastlist_block_empty", "lastlist_item", "lastlist_archive");
		$curr_page = (int) getRequest('p');
		if ($ignore_paging)
			$curr_page = 0;


		$parent_id = $this->analyzeRequiredPath($path);

		if ($parent_id === false && $path != KEYWORD_GRAB_ALL) {
			throw new publicException(getLabel('error-page-does-not-exist', null, $path));
		}

		$this->loadElements($parent_id);

		$library = new selector('pages');
		$library->types('hierarchy-type')->name('cliniccourse', 'item_element');
		if ($path != KEYWORD_GRAB_ALL) {
			if (is_array($parent_id)) {
				foreach ($parent_id as $parent) {
					$library->where('hierarchy')->page($parent)->childs(1);
				}
			} else {
				$library->where('hierarchy')->page($parent_id)->childs(1);
			}
		}

		// TODO UH
		selectorHelper::detectFilters($library);
		$library->option('load-all-props')->value(true);
		$library->option('no-permissions')->value(true);
		$library->limit($curr_page * $per_page, $per_page);

		$result = $library->result();
		$total = $library->length();

		$umiLinksHelper = $this->umiLinksHelper;
		$umiHierarchy = umiHierarchy::getInstance();

		if (($sz = sizeof($result)) > 0) {
			$block_arr = Array();
			$lines = Array();
			$elh = '';
			foreach ($result as $element) {
				if (!$element instanceof umiHierarchyElement) {
					continue;
				}
				$line_arr = Array();
				$element_id = $element->getId();

				$line_arr['attribute:id'] = $element_id;
				$line_arr['node:name'] = $element->getName();
				$line_arr['attribute:link'] = $umiLinksHelper->getLinkByParts($element);
				$line_arr['content'] = $element->getValue('content');
				$line_arr['anons'] = $element->getValue('anons');
				$elh = $element->getValue("anons_pic");

				if ($elh) {
					$imgsrc = $elh->getFilePath();
					$sourcthmb = $this->makeThumbnailFullmy($imgsrc, 400, auto);
					$line_arr['thumb'] = $sourcthmb;
				}


				$lines[] = self::parseTemplate($template_line, $line_arr, $element_id);
				$this->pushEditable("news", "item", $element_id);
				umiHierarchy::getInstance()->unloadElement($element_id);
			}

			if (is_array($parent_id)) {
				list($parent_id) = $parent_id;
			}

			$block_arr['subnodes:items'] = $block_arr['void:lines'] = $lines;
			$block_arr['archive'] = ($total > 0) ? $template_archive : "";
			$parent = $umiHierarchy->getElement($parent_id);
			if ($parent instanceof umiHierarchyElement) {
				$block_arr['archive_link'] = $umiLinksHelper->getLinkByParts($parent);
			}
			$block_arr['total'] = $total;
			$block_arr['per_page'] = $per_page;
			$block_arr['category_id'] = $parent_id;

			return $this->parseTemplate($template_block, $block_arr, $parent_id);
		} else {
			return $template_block_empty;
		}
	}

	public function partners($path = "", $template = "default", $per_page = false, $ignore_paging = false) {
		if (!$per_page){
			$per_page = $this->per_page;
		}
		$per_page = intval($per_page);

		//
		list($template_block, $template_block_empty, $template_line, $template_archive) = def_module::loadTemplates("library/" . $template, "lastlist_block", "lastlist_block_empty", "lastlist_item", "lastlist_archive");
		$curr_page = (int) getRequest('p');
		if ($ignore_paging){
			$curr_page = 0;
		}

		$parent_id = $this->analyzeRequiredPath($path);

		if ($parent_id === false && $path != KEYWORD_GRAB_ALL) {
			throw new publicException(getLabel('error-page-does-not-exist', null, $path));
		}

		$this->loadElements($parent_id);

		$library = new selector('pages');
		$library->types('hierarchy-type')->name('partners', 'item_element');
		if ($path != KEYWORD_GRAB_ALL) {
			if (is_array($parent_id)) {
				foreach ($parent_id as $parent) {
					$library->where('hierarchy')->page($parent)->childs(1);
				}
			} else {
				$library->where('hierarchy')->page($parent_id)->childs(1);
			}
		}

		// TODO UH
		selectorHelper::detectFilters($library);
		$library->option('load-all-props')->value(true);
		$library->limit($curr_page * $per_page, $per_page);

		$result = $library->result();
		$total = $library->length();

		$umiHierarchy = umiHierarchy::getInstance();
		if (($sz = sizeof($result)) > 0) {
			$block_arr = Array();
			$lines = Array();
			foreach ($result as $element) {
				if (!$element instanceof umiHierarchyElement) {
					continue;
				}
				$line_arr = Array();
				$element_id = $element->getId();

				$line_arr['attribute:id'] = $element_id;
				$line_arr['name'] = $element->getName();
				$line_arr['attribute:link'] = $element->link;
				$line_arr['diler'] = $element->getValue('diler');
				$line_arr['adres'] = $element->getValue('adres');
				$line_arr['partner_phone'] = $element->getValue('partner_phone');
				$line_arr['partner_email'] = $element->getValue('partner_email');
				$line_arr['partner_web'] = $element->getValue('partner_web');

				$line_arr['country_id'] = $element->getValue('country');
				$line_arr['city_id'] = $element->getValue('city');

				$country = $element->getValue('country');
				$objects = umiObjectsCollection::getInstance();
				$countryObject = $objects->getObject($country);
				$line_arr['country'] = $countryObject->name;

				$city = $element->getValue('city');
				$objects = umiObjectsCollection::getInstance();
				$cityObject = $objects->getObject($city);
				$line_arr['city'] = $cityObject->name;

				$lent_name = "";
				$lent_link = "";
				$lent_id = $element->getParentId();

				if ($lent_element = $umiHierarchy->getElement($lent_id)) {
					$lent_name = $lent_element->getName();
					$lent_link = $lent_element->link;
				}

				$line_arr['attribute:lent_id'] = $lent_id;
				$line_arr['attribute:lent_name'] = $lent_name;
				$line_arr['attribute:lent_link'] = $lent_link;

				$lines[] = self::parseTemplate($template_line, $line_arr, $element_id);
				$this->pushEditable("news", "item", $element_id);
				umiHierarchy::getInstance()->unloadElement($element_id);
			}

			if (is_array($parent_id)) {
				list($parent_id) = $parent_id;
			}

			$block_arr['subnodes:items'] = $block_arr['void:lines'] = $lines;
			$block_arr['archive'] = ($total > 0) ? $template_archive : "";
			$parent = $umiHierarchy->getElement($parent_id);
			if ($parent instanceof umiHierarchyElement) {
				$block_arr['archive_link'] = $parent->link;
			}
			$block_arr['total'] = $total;
			$block_arr['per_page'] = $per_page;
			$block_arr['category_id'] = $parent_id;

			return $this->parseTemplate($template_block, $block_arr, $parent_id);
		} else {
			return $template_block_empty;
		}
	}

	public function contsell($parentElementId, $width = 'auto', $height = '400') {
		$hierarchyTypes = umiHierarchyTypesCollection::getInstance();
		$hierarchyTypeId = 135;

		//Получаем базовый тип данных для новостей
		$objectTypes = umiObjectTypesCollection::getInstance();
		$objectTypeId = $objectTypes->getTypeByHierarchyTypeId($hierarchyTypeId);
		$objectType = $objectTypes->getType($objectTypeId);

		$hierarchy = umiHierarchy::getInstance();

		//Создаем и подготавливаем выборку
		$sel = new umiSelection;
		$sel->addElementType($hierarchyTypeId); //Добавляет поиск по иерархическому типу
		$sel->addHierarchyFilter($parentElementId); //Устанавливаем поиск по разделу
		$sel->addPermissions(); //Говорим, что обязательно нужно учитывать права доступа
		//Получаем результаты
		$result = umiSelectionsParser::runSelection($sel); //Массив id объектов
		$total = umiSelectionsParser::runSelectionCounts($sel); //Количество записей

		$datenow = date(U);

		//Выводим список  на экран
		if (($sz = sizeof($result)) > 0) {
			$block_arr = Array();
			$lines = Array();
			for ($i = 0; $i < $sz; $i++) {
				$element_id = $result[$i];
				$element = umiHierarchy::getInstance()->getElement($element_id);
				if (!$element)
					continue;
				$line_arr = Array();
				$line_arr['attribute:id'] = $element_id;
				if ($i < 10) {
					$line_arr['i'] = $i;
					$impr = $i;
				} else {
					$line_arr['i'] = $i;
					$impr = $i;
				}
				$line_arr['name'] = $element->getName();
				$elh = $element->getValue("izobrazhenie");

				if ($elh) {
					$imgsrc = $elh->getFilePath();
					$sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
					$line_arr['thumb'] = $sourcthmb;
				}

				$pages = new selector('pages');
				$pages->types('hierarchy-type')->name('content');
				$pages->where('hierarchy')->page($element_id)->childs(1);
				$countcat = $pages->length;
				$resultp = $pages->result();
				$line_arr2 = Array();
				$line_arr3 = Array();
				if ($countcat > 0) {
					for ($k = 0; $k < $countcat; $k++) {
						$line_arr2['names'] = $resultp[$k]->getName();
						$line_arr2['link'] = $resultp[$k]->link;
						$line_arr2['opisanie11'] = $resultp[$k]->getValue("opisanie11");
						$line_arr2['telefon11'] = $resultp[$k]->getValue("telefon11");
						$line_arr2['email11'] = $resultp[$k]->getValue("email11");
						$newelid = $resultp[$k]->id;
						$line_arr2['id'] = $newelid;
						$pagesn = new selector('pages');
						$pagesn->types('hierarchy-type')->name('catalog', 'category');
						$pagesn->where('hierarchy')->page($newelid)->childs(1);
						$countcatn = $pagesn->length;
						if ($countcatn > 0) {
							$line_arr2['moremenu'] = 1;
						} else {
							$line_arr2['moremenu'] = 0;
						}

						if ($k < 10) {
							$nk = $k;
						} else {
							$nk = $k;
						}

						$line_arr3['nodes:itemmenu'][$nk] = $line_arr2;
					}
					$line_arr['nodes:menu']['0'] = $line_arr3;
				} else {
					$line_arr['nodes:nomenu'][$impr] = $line_arr3;
				}

				$lines['nodes:item'][$impr] = $line_arr;
			}
			$block_arr['lines'] = $lines;
			$block_arr['numpages'] = umiPagenum::generateNumPage($total, $per_page);
			$block_arr['total'] = $total;
			$block_arr['per_page'] = $per_page;
			$block_arr['category_id'] = $category_id;

			if ($type_id) {
				$block_arr['type_id'] = $type_id;
			}
			return $block_arr;
		} else {
			$block_arr['numpages'] = umiPagenum::generateNumPage(0, 0);
			$block_arr['lines'] = "";
			$block_arr['total'] = 0;
			$block_arr['per_page'] = 0;
			$block_arr['category_id'] = $category_id;
		}
		return $block_arr;
	}

	public function newsitems($parentElementId, $width = '250', $height = '250') {
		$pages = new selector('pages');

		$pages->types('hierarchy-type')->name('faq', 'question');
		$pages->where('hierarchy')->page($parentElementId)->childs(3);
		$countcat = $pages->length;
		$resultp = $pages->result();
		$line_arr2 = Array();
		$line_arr3 = Array();
		for ($k = 0; $k < $countcat; $k++) {
			$line_arr2['names'] = $resultp[$k]->getName();
			$line_arr2['id'] = $resultp[$k]->id;
			/* $line_arr2['answer'] = $resultp[$k]->getValue('answer');

			  $elh = $resultp[$k]->getValue("anons_pic");
			  if ($elh) {
			  $imgsrc = $elh->getFilePath();
			  $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
			  $line_arr2['anons_pic'] = $sourcthmb;
			  }
			  else {
			  $elh2 = $resultp[$k]->getValue("publish_pic");
			  if ($elh2) {
			  $imgsrc = $elh2->getFilePath();
			  $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
			  $line_arr2['anons_pic'] = $sourcthmb;
			  }
			  }
			 */


			if ($k < 10) {
				$nk = '0' . $k;
			} else {
				$nk = $k;
			}

			$lines['nodes:item'][$nk] = $line_arr2;
		}

		$block_arr['lines'] = $lines;
		$block_arr['numpages'] = umiPagenum::generateNumPage($total, $per_page);
		$block_arr['total'] = $total;

		return $block_arr;
	}

	//Метод upage
	public function upage($elementId, $width = '800', $height = '600') {
		$hierarchy = umiHierarchy::getInstance();
		$element = $hierarchy->getElement($elementId);
		$line_arr = Array();
		$line_arr2 = Array();
		$line_arr['id'] = $elementId;
		$line_arr['name'] = $element->getName();
		$line_arr['link'] = $element->link;
		$line_arr['content'] = $element->getValue('content');
		$line_arr['contentmobile'] = $element->getValue('content_mobile');

		$elh = $element->getValue("header_pic");
		if ($elh) {
			$imgsrc = $elh->getFilePath();
			$sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
			$line_arr['thumbs'] = $sourcthmb;
		} else {
			$line_arr['thumbs'] = "/images/cms/thumbs/f8c15f3a9a4ad9fe97f7d162357d4696012859f3/nofoto_260_auto_5_80.jpg";
		}


//        $elh = $element->getValue("photo_2");
		//       if ($elh) {
		//           $imgsrc = $elh->getFilePath();
		//           $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
		//           $line_arr['nodes:thumbs'][] = $sourcthmb;
		//       }
		//       $elh = $element->getValue("photo_3");
		//       if ($elh) {
		//           $imgsrc = $elh->getFilePath();
		//           $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
		//           $line_arr['nodes:thumbs'][] = $sourcthmb;
		//       }
		//       $elh = $element->getValue("photo_4");
		//       if ($elh) {
		//           $imgsrc = $elh->getFilePath();
		//           $sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
		//          $line_arr['nodes:thumbs'][] = $sourcthmb;
		//       }

		return $line_arr;
	}

	public function npage($elementId, $width = '500', $height = '300') {
		$hierarchy = umiHierarchy::getInstance();
		$element = $hierarchy->getElement($elementId);
		$line_arr = Array();
		$line_arr['id'] = $elementId;
		$line_arr['name'] = $element->getName();
		$line_arr['content'] = $element->getValue('content');
		$elh = $element->getValue("publish_pic");
		if ($elh) {
			$imgsrc = $elh->getFilePath();
			$sourcthmb = $this->makeThumbnailFullmy($imgsrc, $width, $height);
			$line_arr['thumbs'] = $sourcthmb;
		}
		return $line_arr;
	}

	public function contacts($elementId, $width = '500', $height = '300') {
		$hierarchy = umiHierarchy::getInstance();
		$element = $hierarchy->getElement($elementId);
		$line_arr = Array();
		$line_arr['id'] = $elementId;
		$line_arr['name'] = $element->getName();
		$line_arr['adres1'] = $element->getValue('adres1');
		$line_arr['tel1'] = $element->getValue('tel1');
		$line_arr['faks1'] = $element->getValue('faks1');
		$line_arr['jobt1'] = $element->getValue('jobt1');
		$line_arr['map_mobile1'] = $element->getValue('map_mobile1');

		$line_arr['opisanie11'] = $element->getValue('opisanie11');
		$line_arr['telefon11'] = $element->getValue('telefon11');
		$line_arr['email11'] = $element->getValue('email11');

		$line_arr['opisanie12'] = $element->getValue('opisanie12');
		$line_arr['telefon12'] = $element->getValue('telefon12');
		$line_arr['email12'] = $element->getValue('email12');

		$line_arr['adres2'] = $element->getValue('adres2');
		$line_arr['tel2'] = $element->getValue('tel2');
		$line_arr['faks2'] = $element->getValue('faks2');
		$line_arr['jobt2'] = $element->getValue('jobt2');
		$line_arr['map_mobile2'] = $element->getValue('map_mobile2');

		$line_arr['opisanie21'] = $element->getValue('opisanie21');
		$line_arr['telefon21'] = $element->getValue('telefon21');
		$line_arr['email21'] = $element->getValue('email21');
		$lines['nodes:item'][] = $line_arr;
		return $lines;
	}

	public function custupage($elementId) {
		$hierarchy = umiHierarchy::getInstance();
		$element = $hierarchy->getElement($elementId);
		$line_arr = Array();
		$line_arr['id'] = $elementId;

		$kolvokup = $element->getValue('kolichestvo_kuponov');


		if ($kolvokup > 0) {
			if ($kolvokup == 0 or $kolvokup == '-1') {
				$line_arr['kypon'] = 'end';
			} else {
				$line_arr['kypon'] = $element->getValue('kolichestvo_kuponov');
			}
		} else {
			$line_arr['kypon'] = null;
		}

		if ($element->getValue('end_time')) {
			$datenow = date(U);
			$timeakcia = $element->getValue('end_time');
			$timeakciaUnix = strtotime($timeakcia);
			$timedokoncaUnix = ($timeakciaUnix - $datenow);
			$timedokonca = round($timedokoncaUnix / 86400);
			if ($timedokonca == '1') {
				$timeinfoo = $timedokonca . ' день';
			} else if ($timedokonca == '2' or $timedokonca == '3' or $timedokonca == '4') {
				$timeinfoo = $timedokonca . ' дня';
			} else if ($timedokonca < '0') {
				$timeinfoo = 'end';
			} else {
				$timeinfoo = $timedokonca . ' дней';
			}
			$line_arr['time'] = $timeinfoo;
		} else {
			$line_arr['time'] = null;
		}

		return $line_arr;
	}

	public function contitem($parentElementId, $width = '600', $height = '400') {
		$hierarchyTypes = umiHierarchyTypesCollection::getInstance();
		$hierarchyType = $hierarchyTypes->getTypeByName("content");
		$hierarchyTypeId = $hierarchyType->getId();

		//Получаем базовый тип данных для новостей
		$objectTypes = umiObjectTypesCollection::getInstance();
		$objectTypeId = $objectTypes->getTypeByHierarchyTypeId($hierarchyTypeId);
		$objectType = $objectTypes->getType($objectTypeId);

		$hierarchy = umiHierarchy::getInstance();

		//Создаем и подготавливаем выборку
		$sel = new umiSelection;
		$sel->addElementType($hierarchyTypeId); //Добавляет поиск по иерархическому типу
		$sel->addHierarchyFilter($parentElementId); //Устанавливаем поиск по разделу
		$sel->addPermissions(); //Говорим, что обязательно нужно учитывать права доступа
		//Получаем результаты
		$result = umiSelectionsParser::runSelection($sel); //Массив id объектов
		$total = umiSelectionsParser::runSelectionCounts($sel); //Количество записей

		$datenow = date(U);

		//Выводим список  на экран
		if (($sz = sizeof($result)) > 0) {
			$block_arr = Array();
			$lines = Array();
			for ($i = 0; $i < $sz; $i++) {
				$element_id = $result[$i];
				$element = umiHierarchy::getInstance()->getElement($element_id);
				if (!$element)
					continue;
				$line_arr = Array();
				$line_arr['attribute:id'] = $element_id;
				if ($i < 10) {
					$line_arr['i'] = '0' . $i;
					$impr = '0' . $i;
				} else {
					$line_arr['i'] = $i;
					$impr = $i;
				}
				$line_arr['name'] = $element->getName();
				$line_arr['h1'] = $element->getValue('h1');
				$line_arr['kontent'] = $element->getValue('kontent');
				$line_arr['cena'] = $element->getValue('cena');
				$elh = $element->getValue("izobrazhenie");
				$sourcthmb = $this->makeThumbnailFullmy('.' . $elh, $width, $height);
				$line_arr['thumb'] = $sourcthmb;

				$lines['nodes:item'][$impr] = $line_arr;
				templater::pushEditable("catalog", "object", $element_id);
				umiHierarchy::getInstance()->unloadElement($element_id);
			}
			$block_arr['lines'] = $lines;
			$block_arr['numpages'] = umiPagenum::generateNumPage($total, $per_page);
			$block_arr['total'] = $total;
			$block_arr['per_page'] = $per_page;
			$block_arr['category_id'] = $category_id;

			if ($type_id) {
				$block_arr['type_id'] = $type_id;
			}
			return $block_arr;
		} else {
			$block_arr['numpages'] = umiPagenum::generateNumPage(0, 0);
			$block_arr['lines'] = "";
			$block_arr['total'] = 0;
			$block_arr['per_page'] = 0;
			$block_arr['category_id'] = $category_id;
		}
		return $block_arr;
	}

	function makeThumbnailFullmy($path, $width, $height, $crop = true, $cropside = 5, $isLogo = false, $quality = 50) {
		$isSharpen = true;
		$thumbs_path = "./images/cms/thumbs/";
		$image = new umiImageFile($path);
		$file_name = $image->getFileName();
		$file_ext = strtolower($image->getExt());
		$file_ext = ($file_ext == 'bmp' ? 'jpg' : $file_ext);

		$allowedExts = Array('gif', 'jpeg', 'jpg', 'png', 'bmp');
		if (!in_array($file_ext, $allowedExts)){
			return "";
		}

		$file_name = substr($file_name, 0, (strlen($file_name) - (strlen($file_ext) + 1)));

		$thumbPath = sha1($image->getDirName());

		if (!is_dir($thumbs_path . $thumbPath)) {
			mkdir($thumbs_path . $thumbPath, 0755, true);
		}

		$file_name_new = $file_name . '_' . $width . '_' . $height . '_' . $cropside . '_' . $quality . "." . $file_ext;
		$path_new = $thumbs_path . $thumbPath . '/' . $file_name_new;

		if (!file_exists($path_new) || filemtime($path_new) < filemtime($path)) {
			if (file_exists($path_new)) {
				unlink($path_new);
			}

			$width_src = $image->getWidth();
			$height_src = $image->getHeight();

			if (!($width_src && $height_src)) {
				throw new coreException(getLabel('error-image-corrupted', null, $path));
			}

			if ($height == "auto") {
				$real_height = (int) round($height_src * ($width / $width_src));
				//change
				$height = $real_height;
				$real_width = (int) $width;
			} else {
				if ($width == "auto") {
					$real_width = (int) round($width_src * ($height / $height_src));
					//change
					$width = $real_width;
				} else {
					$real_width = (int) $width;
				}

				$real_height = (int) $height;
			}

			$offset_h = 0;
			$offset_w = 0;

			// realloc: devision by zero fix
			if (!intval($width) || !intval($height)) {
				$crop = false;
			}

			if ($crop) {
				$width_ratio = $width_src / $width;
				$height_ratio = $height_src / $height;

				if ($width_ratio > $height_ratio) {
					$offset_w = round(($width_src - $width * $height_ratio) / 2);
					$width_src = round($width * $height_ratio);
				} elseif ($width_ratio < $height_ratio) {
					$offset_h = round(($height_src - $height * $width_ratio) / 2);
					$height_src = round($height * $width_ratio);
				}

				if ($cropside) {
					//defore all it was cropside work like as - 5
					//123
					//456
					//789
					switch ($cropside):
						case 1:
							$offset_w = 0;
							$offset_h = 0;
							break;
						case 2:
							$offset_h = 0;
							break;
						case 3:
							$offset_w += $offset_w;
							$offset_h = 0;
							break;
						case 4:
							$offset_w = 0;
							break;
						case 5:
							break;
						case 6:
							$offset_w += $offset_w;
							break;
						case 7:
							$offset_w = 0;
							$offset_h += $offset_h;
							break;
						case 8:
							$offset_h += $offset_h;
							break;
						case 9:
							$offset_w += $offset_w;
							$offset_h += $offset_h;
							break;
					endswitch;
				}
			}

			$thumb = imagecreatetruecolor($real_width, $real_height);

			$source_array = $image->createImage($path);
			$source = $source_array['im'];

			if ($width * 4 < $width_src && $height * 4 < $height_src) {
				$_TMP = array();
				$_TMP['width'] = round($width * 4);
				$_TMP['height'] = round($height * 4);

				$_TMP['image'] = imagecreatetruecolor($_TMP['width'], $_TMP['height']);

				if ($file_ext == 'gif') {
					$_TMP['image_white'] = imagecolorallocate($_TMP['image'], 255, 255, 255);
					imagefill($_TMP['image'], 0, 0, $_TMP['image_white']);
					imagecolortransparent($_TMP['image'], $_TMP['image_white']);
					imagealphablending($source, TRUE);
					imagealphablending($_TMP['image'], TRUE);
				} else {
					imagealphablending($_TMP['image'], false);
					imagesavealpha($_TMP['image'], true);
				}
				imagecopyresampled($_TMP['image'], $source, 0, 0, $offset_w, $offset_h, $_TMP['width'], $_TMP['height'], $width_src, $height_src);

				imageDestroy($source);

				$source = $_TMP['image'];
				$width_src = $_TMP['width'];
				$height_src = $_TMP['height'];

				$offset_w = 0;
				$offset_h = 0;
				unset($_TMP);
			}

			if ($file_ext == 'gif') {
				$thumb_white_color = imagecolorallocate($thumb, 255, 255, 255);
				imagefill($thumb, 0, 0, $thumb_white_color);
				imagecolortransparent($thumb, $thumb_white_color);
				imagealphablending($source, TRUE);
				imagealphablending($thumb, TRUE);
			} else {
				imagealphablending($thumb, false);
				imagesavealpha($thumb, true);
			}

			imagecopyresampled($thumb, $source, 0, 0, $offset_w, $offset_h, $width, $height, $width_src, $height_src);
			if ($isSharpen)
				$thumb = makeThumbnailFullUnsharpMask($thumb, 80, .5, 3);

			switch ($file_ext) {
				case 'gif':
					$res = imagegif($thumb, $path_new);
					break;
				case 'png':
					$res = imagepng($thumb, $path_new);
					break;
				default:
					$res = imagejpeg($thumb, $path_new, $quality);
			}
			if (!$res) {
				throw new coreException(getLabel('label-errors-16008'));
			}

			imageDestroy($source);
			imageDestroy($thumb);

			if ($isLogo) {
				umiImageFile::addWatermark($path_new);
			}
		}

		$value = new umiImageFile($path_new);

		$arr = $value->getFilePath(true);
		return $arr;
	}

	public function userid() {
		$permissions = permissionsCollection::getInstance();
		return $permissions->isAuth() ? $permissions->getUserId() : null;
	}

	public function setnotify($currentUserId) {
		/* $permissions = permissionsCollection::getInstance();
		  $currentUserId = $permissions->getUserId(); */
		$objects = umiObjectsCollection::getInstance();
		$userObject = $objects->getObject($currentUserId);
		$userObject->setValue("ormco_star_notify", 0);
		$userObject->commit();
		return Array("result" => "Success");
	}

	public function usercatid() {
		$permissions = permissionsCollection::getInstance();
		$currentUserId = $permissions->getUserId();
		$objects = umiObjectsCollection::getInstance();
		$userObject = $objects->getObject($currentUserId);
		$result = $userObject->getValue("groups");
		return Array("result" => $result[0]);
	}

	public function ubalance() {
		$permissions = permissionsCollection::getInstance();
		$currentUserId = $permissions->getUserId();

		$objects = umiObjectsCollection::getInstance();
		$userObject = $objects->getObject($currentUserId);

		$ballance = $userObject->getValue("bonus");
		return $ballance;
	}

	/* ------------------   Начисление баллов ----------------- */
	public function userbonus() {
		$userId = getRequest('userid');
		$rate = getRequest('balli');

		$sRedirect = getRequest('ref_notsuccess');
		if (!$rate) {
			$this->redirect($sRedirect . '&errorbal=1');
		}

		//Получим объект пользователя
		$userObject = umiObjectsCollection::getInstance()->getObject($userId);
		// Получим текущий рейтинг, увеличим его на переданное значение и сохраним изменения
		$currentRateUser = $userObject->getValue("bonus");
		$currentvizity = $userObject->getValue("vizity"); //число визитов
		if ($currentvizity == 0) {
			$promo_code = $userObject->getValue("promo_kod");
			//Начисление баллов за рекомендацию
			if (strlen($promo_code) > 0) {
				$users_promo = new selector('objects');
				$users_promo->types('object-type')->name('users', 'user');
				$users_promo->where('id')->equals($promo_code);
				$totalusers = $users_promo->length;
				if ($totalusers > 0) {
					$bbal = regedit::getInstance()->getVal("//modules/mobile/regbalpromoplus");
					foreach ($users_promo as $user) {
						$userbonus = $user->bonus;
						$userbonus = intval($userbonus) + intval($bbal); //Начисляем 50 баллов за рекомендацию
						$user->setValue("bonus", $userbonus);
						$user->commit();
					}
					//Теперь Запишем данные о начислении в модуль
					$pointTypes = umiObjectTypesCollection::getInstance();
					$pointObjectTypeId = $pointTypes->getBaseType("points", "stat");

					$pointsName = "Баллы за регистрацию";
					$pontsData = date("U");
					$pointsAdmin = 42;
					$pointsUser = $promo_code;
					$pointsUsluga = "Посещение по рекомендации";
					$pointsSumm = $bbal;

					$objectsp = umiObjectsCollection::getInstance();
					$newPointsId = $objectsp->addObject($pointsName, $pointObjectTypeId);

					//Получаем объект Поинта
					$newPoint = $objectsp->getObject($newPointsId);
					if ($newPoint instanceof umiObject) {
						//Заполняем объект  новыми свойствами
						$newPoint->setValue("administrator", $pointsAdmin);
						$newPoint->setValue("polzovatel", $pointsUser);
						$newPoint->setValue("data", $pontsData);
						$newPoint->setValue("usluga", $pointsUsluga);
						$newPoint->setValue("kolichestvo_ballov", $pointsSumm);
						$newPoint->setValue("is_active", true);

						//Подтверждаем внесенные изменения
						$newPoint->commit(); //Хочу домой есть и спать
					}
					//Конец Запишем данные о начислении в модуль
				}
			}
		}
		//Конец начисления баллов за рекомендацию
		$currentvizity = $currentvizity + 1;
		$sumRateUser = $currentRateUser + round($rate / 10, 0);
		$sumRateUser = round($sumRateUser, 0);
		$time = time();
		//Проверяем не было ли начислений на эту сумму 5 мин назад
		$check = new selector('objects');
		$check->types('object-type')->name('points', 'stat');
		$check->where('kolichestvo_ballov')->equals(round($rate / 10, 0));
		$check->where('data')->between(time() - 300, time() + 300);
		$kolvocheck = $check->length;
		//КОнец проверяем начисления
		if ($kolvocheck > 0) {
			$this->redirect('/mobile/success/?usr=' . $userId . '&errorbal=5&sumbal=' . round($rate / 10, 0));
		} else {
			$userObject->setValue("vizity", $currentvizity);
			$userObject->setValue("bonus", $sumRateUser);
			$userObject->setValue("poslednij_vizit", $time);
			$userObject->commit();

			//Запишем данные о начислении в модуль
			$pointTypes = umiObjectTypesCollection::getInstance();
			$pointObjectTypeId = $pointTypes->getBaseType("points", "stat");

			$pemissios = permissionsCollection::getInstance();
			$cuetUseId = $pemissios->getUserId();

			$pointsName = "Начисление баллов";
			$pontsData = date("U");
			$pointsAdmin = $cuetUseId;
			$pointsUser = $userId;
			$pointsUsluga = "В салоне";
			$pointsSumm = $rate / 10;
			$pointsSumm = round($pointsSumm, 0);

			$objects = umiObjectsCollection::getInstance();
			$newPointsId = $objects->addObject($pointsName, $pointObjectTypeId);

			//Получаем объект Поинта
			$newPoint = $objects->getObject($newPointsId);
			if ($newPoint instanceof umiObject) {
				//Заполняем объект  новыми свойствами
				$newPoint->setValue("administrator", $pointsAdmin);
				$newPoint->setValue("polzovatel", $pointsUser);
				$newPoint->setValue("data", $pontsData);
				$newPoint->setValue("usluga", $pointsUsluga);
				$newPoint->setValue("kolichestvo_ballov", $pointsSumm);
				$newPoint->setValue("is_active", true);

				//Подтверждаем внесенные изменения
				$newPoint->commit();
			}
			$this->redirect('/mobile/success/?usr=' . $userId . '&sumbal=' . round($rate / 10, 0));
		}
	}

	public function userminus() {
		$userId = getRequest('userid');
		$rate = getRequest('balli');
		$uslugaid = getRequest('usluga');

		$sRedirect = getRequest('ref_notsuccess');

		//Получим объект пользователя
		$userObject = umiObjectsCollection::getInstance()->getObject($userId);
		// Получим текущий рейтинг, увеличим его на переданное значение и сохраним изменения
		$currentRateUser = $userObject->getValue("bonus");

		if ($currentRateUser < $rate) {
			$this->redirect($sRedirect . '&errorbal=1');
		}

		$sumRateUser = $currentRateUser - (int) $rate;
		$time = time();
		$userObject->setValue("bonus", $sumRateUser);
		$userObject->setValue("poslednij_vizit", $time);
		$userObject->commit();

		//Запишем данные о Списании в модуль
		$pointTypes = umiObjectTypesCollection::getInstance();
		$pointObjectTypeId = $pointTypes->getBaseType("points", "stat");

		$pemissios = permissionsCollection::getInstance();
		$cuetUseId = $pemissios->getUserId();

		$hierarchy = umiHierarchy::getInstance();
		$elementId = $uslugaid;

		$element = $hierarchy->getElement($elementId);
		if ($element instanceof umiHierarchyElement) {
			$pointsUsluga = $element->getName();
		} else {
			$pointsUsluga = "Подарок";
		}

		$pointsName = "Списание баллов";
		$pontsData = date("U");
		$pointsAdmin = $cuetUseId;
		$pointsUser = $userId;
		$pointsSumm = "-" . $rate;

		$objects = umiObjectsCollection::getInstance();
		$newPointsId = $objects->addObject($pointsName, $pointObjectTypeId);

		//Получаем объект Поинта
		$newPoint = $objects->getObject($newPointsId);
		if ($newPoint instanceof umiObject) {
			//Заполняем объект  новыми свойствами
			$newPoint->setValue("administrator", $pointsAdmin);
			$newPoint->setValue("polzovatel", $pointsUser);
			$newPoint->setValue("data", $pontsData);
			$newPoint->setValue("usluga", $pointsUsluga);
			$newPoint->setValue("kolichestvo_ballov", $pointsSumm);
			$newPoint->setValue("is_active", true);

			//Подтверждаем внесенные изменения
			$newPoint->commit();
		}
		$this->redirect('/mobile/success/?usr=' . $userId);
	}

	/* ----------------- Конец начисления баллов ------------------- */
	public function show() {
		require_once 'Mobile_Detect.php';
		$detect = new Mobile_Detect;
		if ($detect->isiOS()) {
			$redirect_url = regedit::getInstance()->getVal("//modules/mobile/ioslink");
		} else if ($detect->isAndroidOS()) {
			$redirect_url = regedit::getInstance()->getVal("//modules/mobile/androidlink");
		} else {
			$redirect_url = regedit::getInstance()->getVal("//modules/mobile/ioslink");
		}
		$this->redirect($redirect_url);
	}

	public function znachsprv($typeId) {
		$objectsCollection = umiObjectsCollection::getInstance();
		$guidepr = $objectsCollection->getGuidedItems($typeId);

		$items = Array();
		$firstb = Array();
		foreach ($guidepr as $itid => $item) {
			$item_arr = Array();
			$item_arr['attribute:value'] = $item;
			$item_arr['attribute:id'] = $itid;
			$items[] = $item_arr;
		}
		$itogarray = Array("items" => Array('nodes:item' => $items));
		return $itogarray;
	}

	public function selectCompany($value) {
		$objects = umiObjectsCollection::getInstance();
		$companyObject = $objects->getObject($value);

		if ($companyObject instanceof umiObject) {
			$listCompanies = $companyObject->transp_company;
			$count = count($listCompanies);
			$i = 1;
			foreach ($listCompanies as $company) {
				if ($i == $count) {
					$result .= $company;
				} else {
					$result .= $company . ', ';
				}
				$i++;
			}
		}
		return $result;
	}

	public function payments() {
		$sel = new selector('objects');
		$sel->types('hierarchy-type')->name('emarket', 'payment');
		$sel->option('load-all-props')->value(true);
		$paymentsList = $sel->result;

		$itogarray = Array("items" => Array('nodes:item' => $paymentsList));
		return $itogarray;
	}

	public function config() {
		return __mobile::config();
	}

	public function loyalty($userId) {
		$user = umiObjectsCollection::getInstance()->getObject($userId);
		if ($user instanceof umiObject) {
			$yet_ormco_star = $user->yet_ormco_star;
			$potential_ormco_star = $user->potential_ormco_star;
			$freez_ormco_star = $user->freez_ormco_star;
			$ormco_star_notify = $user->ormco_star_notify;
			$result = array(
				'yet_ormco_star' => $yet_ormco_star,
				'potential_ormco_star' => $potential_ormco_star,
				'freez_ormco_star' => $freez_ormco_star,
				'ormco_star_notify' => $ormco_star_notify
			);
			if ($start_ormco_star = $user->start_ormco_star) {
				$start_ormco_star = $start_ormco_star->getFormattedDate("U");
				$finish_ormco_star = strtotime(date("Y-m-d", $start_ormco_star) . " + 1 year");
				$day_form = '';
				$num = date("d", $start_ormco_star);
				if ($num[strlen($num) - 1] == '1') {
					$day_form = 'день';
				} elseif (in_array($num[strlen($num) - 1], array(2, 3))) {
					$day_form = 'дня';
				} else {
					$day_form = 'дней';
				}
				$day_left = $finish_ormco_star - time();
				$result['start_ormco_star'] = date("d.m.Y", $start_ormco_star);
				$result['finish_ormco_star'] = date("d.m.Y", $finish_ormco_star);
				$result['day_left'] = floor($day_left / (60 * 60 * 24)) . ' ' . $day_form;
			}
			return $result;
		}
		return;
	}

	public function getEditLink($element_id, $element_type) {
		$element = umiHierarchy::getInstance()->getElement($element_id);
		$parent_id = $element->getParentId();

		switch ($element_type) {
			case "groupelements": {
					$link_add = $this->pre_lang . "/admin/mobile/add/{$element_id}/item_element/";
					$link_edit = $this->pre_lang . "/admin/mobile/edit/{$element_id}/";

					return Array($link_add, $link_edit);
					break;
				}

			case "group_feedback": {
					$link_add = $this->pre_lang . "/admin/mobile/add/{$element_id}/item_feedback/";
					$link_edit = $this->pre_lang . "/admin/mobile/edit/{$element_id}/";

					return Array($link_add, $link_edit);
					break;
				}

			case "item_element": {
					$link_edit = $this->pre_lang . "/admin/mobile/edit/{$element_id}/";

					return Array(false, $link_edit);
					break;
				}

			case "item_feedback": {
					$link_edit = $this->pre_lang . "/admin/mobile/edit/{$element_id}/";

					return Array(false, $link_edit);
					break;
				}

			default: {
					return false;
				}
		}
	}
}
?>