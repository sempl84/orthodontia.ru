<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class __orthoUniSenderAdmin extends baseModuleAdmin
{
    public function dashboard()
    {
        $this->redirect('/admin/orthoUniSender/syncContactsLogList/');
    }
    
    public function config() {
        $regedit = regedit::getInstance();
        
        $params = array(
            'uniSender-config-api' => array(
                'string:uniSender-config-api-key' => NULL,
                'string:uniSender-config-test-email' => NULL,
            ),
            'uniSender-sender-contacts' => array(
                'select:uniSender-sender-contacts-list' => array(
                    '0' => 'Не выбран'
                )
            )
        );
        
        if($regedit->getVal(orthoUniSender::registry_param_api_key)) {
            $api = $this->getApi();
            if($api instanceof UniSenderApi) {
                try {
                    $senderLists = $api->getLists();
                    if(is_array($senderLists) && count($senderLists)) {
                        foreach($senderLists as $senderList) {
                            if(!$senderList instanceof UniSenderApiListModel) {
                                continue;
                            }
    
                            $params['uniSender-sender-contacts']['select:uniSender-sender-contacts-list'][$senderList->getId()] = $senderList->getTitle();
                        }
                    }
                }  catch (Exception $e) {}
            }
        }
        
        $mode = getRequest('param0');
        
        if($mode == 'do') {
            $params = $this->expectParams($params);
            $regedit->setVar(orthoUniSender::registry_param_api_key, $params['uniSender-config-api']['string:uniSender-config-api-key']);
            $regedit->setVar(orthoUniSender::registry_param_test_email, $params['uniSender-config-api']['string:uniSender-config-test-email']);
            $regedit->setVar(orthoUniSender::registry_param_contacts_list_id, $params['uniSender-sender-contacts']['select:uniSender-sender-contacts-list']);
            $this->chooseRedirect();
        }
    
        $params['uniSender-config-api']['string:uniSender-config-api-key'] = $regedit->getVal(orthoUniSender::registry_param_api_key);
        $params['uniSender-config-api']['string:uniSender-config-test-email'] = $regedit->getVal(orthoUniSender::registry_param_test_email);
        $params['uniSender-sender-contacts']['select:uniSender-sender-contacts-list']['value'] = $regedit->getVal(orthoUniSender::registry_param_contacts_list_id);
        
        $this->setDataType('settings');
        $this->setActionType('modify');
        
        $data = $this->prepareData($params, 'settings');
        
        $this->setData($data);
        $this->doData();
    }
    
    public function getDatasetConfiguration($param = '')
    {
        $arParams = array();
        
        if (strpos($param, '|') !== false) {
            $data = explode('|', $param);
            $param = $data[0];
            $arParams[] = $data[1];
        }
        
        $module = $this;
        /* @var $module __orthoUniSenderAdmin */
        
        switch ($param) {
            default:
            {
                throw new publicException('Неизвестный параметр');
            }
        }
    }
}