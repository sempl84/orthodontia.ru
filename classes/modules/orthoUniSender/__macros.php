<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class OrthoUniSenderMacros extends orthoUniSender
{
    public function getOrthoUniSenderLists()
    {
        $lists = $this->getApi()->getLists();
        if (!$lists) {
            return null;
        }
        
        $items = array();
        foreach ($lists as $list) {
            $items[] = array(
                'attribute:id' => $list->getId(),
                'node:title' => $list->getTitle()
            );
        }
        
        if (!count($items)) {
            return null;
        }
        
        return array('items' => array('nodes:item' => $items));
    }
    
    public function getContactFromUser(umiObject $user)
    {
        $email = $user->getValue(SiteUsersUserModel::field_email);
        if (!$email) {
            throw new publicException('У пользователя не указан email');
        }
        
        $contact = new UniSenderApiContactModel($email);
        $contact->setPhone($user->getValue(SiteUsersUserModel::field_phone));
        $contact->addData('surname', $user->getValue(SiteUsersUserModel::field_surname));
        $contact->addData('Name', $user->getValue(SiteUsersUserModel::field_name));
        $contact->addData('fathername', $user->getValue(SiteUsersUserModel::field_father_name));
        $contact->addData('status', ObjectHelper::getObjectName($user->getValue(SiteUsersUserModel::field_prof_status)));
        $contact->addData('region', ObjectHelper::getObjectName($user->getValue(SiteUsersUserModel::field_region)));
        
        return $contact;
    }
    
    public function testGetUserContactModel($userId = null)
    {
        return array('full:contact' => $this->getContactFromUser($this->_getUserObject($userId)));
    }
    
    public function testSendUserContact($userId = null)
    {
        $contactsListId = regedit::getInstance()->getVal(orthoUniSender::registry_param_contacts_list_id);
        if(!$contactsListId) {
            throw new publicException('Не выбран список для импорта контакта');
        }
        
        $contact = $this->getContactFromUser($this->_getUserObject($userId));
        $contact->addEmailListId($contactsListId);
        
        return $this->getApi()->importContacts([$contact]);
    }
    
    public function _getUserObject($userId = null)
    {
        if (!$userId) {
            throw new publicException('Не передан id пользователя');
        }
    
        $user = umiObjectsCollection::getInstance()->getObject($userId);
        if (!$user instanceof umiObject) {
            throw new publicException('Не найден пользователь ' . $userId);
        }
        
        return $user;
    }
    
    public function testGetContact($email = null)
    {
        if(!$email) {
            throw new publicException('Не передан email');
        }
        
        return array('full:contact' => $this->getApi()->getContact($email));
    }
}