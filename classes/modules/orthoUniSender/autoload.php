<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2020, Evgenii Ioffe
 */

$classes = [
    'UniSenderApi' => __DIR__ . '/Classes/Api.php',
    'UniSenderApiContactModel' => __DIR__ . '/Classes/Models/Contact.php',
    'UniSenderApiListModel' => __DIR__ . '/Classes/Models/List.php',
];