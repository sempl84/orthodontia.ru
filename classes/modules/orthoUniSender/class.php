<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class orthoUniSender extends def_module
{
    const registry_param_api_key = '//modules/orthoUniSender/api_key';
    const registry_param_test_email = '//modules/orthoUniSender/test_email';
    const registry_param_contacts_list_id = '//modules/orthoUniSender/contacts_list_id';
    
    public function __construct()
    {
        parent::__construct();
        
        $cmsController = cmsController::getInstance();

        if ($cmsController->getCurrentMode() == "admin") {
            $this->__loadLib("__admin.php");
            $this->__implement("__orthoUniSenderAdmin");
    
            $this->__loadLib("__admin_sync_contacts.php");
            $this->__implement("__orthoUniSenderAdminSyncContacts");
    
            $commonTabs = $this->getCommonTabs();
            if ($commonTabs instanceof iAdminModuleTabs) {
                $commonTabs->add('dashboard');
            }
            
            $configTabs = $this->getConfigTabs();
            if($configTabs instanceof iAdminModuleTabs) {
                $configTabs->add('config');
            }
        } else {
            $this->autoDetectAttributes();
        }
    
        $this->__loadLib("__macros.php");
        $this->__implement("OrthoUniSenderMacros");
    }
    
    /**
     * @return UniSenderApi
     * @throws publicException
     */
    public function getApi()
    {
        $apiKey = regedit::getInstance()->getVal(self::registry_param_api_key);
        if(!$apiKey) {
            throw new publicException('Не перед apikey');
        }
        
        return new UniSenderApi($apiKey, CURRENT_WORKING_DIR . '/sys-temp/orthoUniSender/api/log');
    }
}