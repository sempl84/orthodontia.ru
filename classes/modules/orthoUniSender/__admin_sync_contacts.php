<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
class __orthoUniSenderAdminSyncContacts extends __orthoUniSenderAdmin
{
    public function syncContactsLogList()
    {
        $this->setDataType("settings");
        $this->setActionType("view");
        
        $data = array();
        
        $dir = OrthoUniSendersSyncContacts::getLogDir();
        if (is_dir($dir)) {
            $items = array();
            
            $iterator = new DirectoryIterator($dir);
            
            foreach ($iterator as $file) {
                if (!$file->isFile()) {
                    continue;
                }
                
                if ($file->getExtension() != 'log') {
                    continue;
                }
    
                $id = str_replace('.log', '', $file->getFilename());
                if(strlen($id) > 8) {
                    $date = $id;
                    $text = date('d.m.Y H:i:s', $date);
                } else {
                    $date = strtotime($id);
                    $text = date('d.m.Y', $date);
                }
                
                $items[$date] = array(
                    'attribute:link' => '/admin/orthoUniSender/syncContactsLogView/' . $id . '/',
                    'node:text' => $text
                );
            }
            
            if (count($items)) {
                arsort($items);
                
                $data['items'] = array('nodes:item' => $items);
            }
        }
        
        $this->setData($data);
        $this->doData();
    }
    
    public function syncContactsLogView()
    {
        $this->setDataType("settings");
        $this->setActionType("view");
        
        $data = array();
        
        $dir = OrthoUniSendersSyncContacts::getLogDir();
        if (is_dir($dir)) {
            $id = getRequest('param0');
            
            $file = $dir . '/' . $id . '.log';
            
            if (file_exists($file)) {
                $reverse = (bool)getRequest('reverse');
    
                $fileContents = trim(file_get_contents($file));
    
                if($reverse) {
                    $data['attribute:reverse'] = true;
                    $log = implode('<br />', array_reverse(explode(PHP_EOL, $fileContents)));
                } else {
                    $log = nl2br($fileContents);
                }
                
                $data['attribute:id'] = $id;
                $data['log'] = $log;
            }
        }
        
        $this->setData($data);
        $this->doData();
    }
}