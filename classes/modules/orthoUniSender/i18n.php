<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */
$i18n = Array(
    'module-orthoUniSender' => 'UniSender',
    
    'header-orthoUniSender-dashboard' => 'Панель управления',
    
    'header-orthoUniSender-syncContacts' => 'Синхронизация контактов',
    'header-orthoUniSender-syncContactsLogList' => 'Логи',
    'header-orthoUniSender-syncContactsLogView' => 'Просмотр лога',
    
    'header-orthoUniSender-config' => 'Базовые настройки',
    'group-uniSender-config-api' => 'Интеграция по API',
    'option-uniSender-config-api-key' => 'Api Key',
    'option-uniSender-config-test-email' => 'Тестовый e-mail',
    'group-uniSender-sender-contacts' => 'Передача контактов',
    'option-uniSender-sender-contacts-list' => 'Список контактов',
    
    'perms-orthoUniSender-admin' => 'Управление модулем',
);