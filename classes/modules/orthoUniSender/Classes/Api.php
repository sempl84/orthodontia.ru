<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class UniSenderApi
{
    const server = 'https://api.unisender.com/';
    
    private $apiKey;
    private $logDir;
    
    /**
     * UniSenderApi constructor.
     * @param $apiKey
     * @param $logDir
     */
    public function __construct($apiKey, $logDir)
    {
        $this->apiKey = $apiKey;
        $this->logDir = rtrim($logDir, '/');
    }
    
    /**
     * @return array
     * @throws publicException
     */
    public function getLists()
    {
        $lists = getArrayKey($this->request('getLists'), 'result');
        if (!$lists) {
            throw new publicException('Ошибка при выполении запроса');
        }
        
        $return = array();
        foreach ($lists as $row) {
            $return[] = UniSenderApiListModel::loadFromArray($row);
        }
        
        return $return;
    }
    
    public function importContacts($contacts)
    {
        $fieldNames = array(
            'email',
            'email_list_ids',
            'phone'
        );
        
        $data = array();
        
        foreach ($contacts as $contact) {
            if (!$contact instanceof UniSenderApiContactModel) {
                continue;
            }
            
            $email = $contact->getEmail();
            if (!$email) {
                continue;
            }
            
            $contactData = array();
            $contactData[] = $email;
            $contactData[] = implode(',', $contact->getEmailListIds());
            $contactData[] = $contact->getPhone();
            
            $additionalData = $contact->getData();
            if ($additionalData) {
                $fieldNames = array_merge($fieldNames, array_keys($additionalData));
                $contactData = array_merge($contactData, array_values($additionalData));
            }
            
            $data[] = $contactData;
        }
        
        return $this->request('importContacts', array(
            'field_names' => $fieldNames,
            'data' => $data
        ));
    }
    
    public function getContact($email)
    {
        $data = getArrayKey(
            getArrayKey(
                $this->request(
                    'getContact',
                    array(
                        'email' => $email
                    )
                ), 'result'
            ), 'email'
        );
        if (!is_array($data) || !isset($data['email'])) {
            return false;
        }
    
        return new UniSenderApiContactModel($data['email']);
    }
    
    private function request($url, $data = array())
    {
        if (!is_array($data)) {
            $data = [];
        }
        
        $data['format'] = 'json';
        $data['api_key'] = $this->apiKey;
        
        $url = rtrim(self::server, '/') . '/ru/api/' . ltrim($url, '/');
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $response = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);
        
        $result = json_decode($response, true);
        if (!is_array($result)) {
            if ($this->logDir) {
                if (!is_dir($this->logDir)) {
                    mkdir($this->logDir, 0775, true);
                }
                
                $logFileName = $this->logDir . '/' . time() . '_' . rand(100, 999);
                
                file_put_contents($logFileName . '_request.json', json_encode(array(
                    'url' => $url,
                    'data' => $data
                )));
                
                file_put_contents($logFileName . '_response.json', json_encode(array(
                    'response' => $response,
                    'curl_error' => $error
                )));
            }
            throw new publicException('Неверный формат ответа');
        }
        
        return $result;
    }
}