<?php

class doclocator extends def_module {
	public $per_page;

	public function __construct() {
		parent::__construct();

		if (cmsController::getInstance() -> getCurrentMode() == "admin") {

			$configTabs = $this -> getConfigTabs();
			if ($configTabs) {
				$configTabs -> add("config");
			}

			$this -> __loadLib("__admin.php");
			$this -> __implement("__doclocator");

		} else {

			$this -> per_page = regedit::getInstance() -> getVal("//modules/doclocator/per_page");
		}

	}

	public function groupelements($path = "", $template = "default") {
		if ($this -> breakMe())
			return;
		$element_id = cmsController::getInstance() -> getCurrentElementId();

		templater::pushEditable("doclocator", "groupelements", $element_id);
		return $this -> group($element_id, $template) . $this -> listElements($element_id, $template);
	}

	public function item_element() {
		if ($this -> breakMe())
			return;
		$element_id = (int)cmsController::getInstance() -> getCurrentElementId();
		return $this -> view($element_id);
	}

	public function group($elementPath = "", $template = "default", $per_page = false) {
		if ($this -> breakMe())
			return;
		$hierarchy = umiHierarchy::getInstance();
		list($template_block) = def_module::loadTemplates("tpls/doclocator/{$template}.tpl", "group");

		$elementId = $this -> analyzeRequiredPath($elementPath);

		$element = $hierarchy -> getElement($elementId);

		templater::pushEditable("doclocator", "groupelements", $element -> id);
		return self::parseTemplate($template_block, array('id' => $element -> id), $element -> id);

	}

	public function view($elementPath = "", $template = "default") {
		if ($this -> breakMe())
			return;
		$hierarchy = umiHierarchy::getInstance();
		list($template_block) = def_module::loadTemplates("tpls/doclocator/{$template}.tpl", "view");

		$elementId = $this -> analyzeRequiredPath($elementPath);
		$element = $hierarchy -> getElement($elementId);

		templater::pushEditable("doclocator", "item_element", $element -> id);
		return self::parseTemplate($template_block, array('id' => $element -> id), $element -> id);
	}

	public function listGroup($elementPath, $template = "default", $per_page = false, $ignore_paging = false) {
		// Код метода
	}

	public function listElements($path = "", $template = "default", $per_page = false, $ignore_paging = false) {
            if (!$per_page){
                $per_page = $this->per_page;
            }
            $per_page = intval($per_page);

            //
            list($template_block, $template_block_empty, $template_line, $template_archive) = def_module::loadTemplates("doclocator/" . $template, "lastlist_block", "lastlist_block_empty", "lastlist_item", "lastlist_archive");
            $curr_page = (int) getRequest('p');
            if ($ignore_paging){
                $curr_page = 0;
            }

            $parent_id = $this->analyzeRequiredPath($path);

            if ($parent_id === false && $path != KEYWORD_GRAB_ALL) {
                throw new publicException(getLabel('error-page-does-not-exist', null, $path));
            }

            $this->loadElements($parent_id);

            $doclocator = new selector('pages');
            $doclocator->types('hierarchy-type')->name('doclocator', 'item_element');
            if ($path != KEYWORD_GRAB_ALL) {
                if (is_array($parent_id)) {
                    foreach ($parent_id as $parent) {
                        $doclocator->where('hierarchy')->page($parent)->childs(1);
                    }
                } else {
                    $doclocator->where('hierarchy')->page($parent_id)->childs(1);
                }
            }

            selectorHelper::detectFilters($doclocator);
            $doclocator->option('load-all-props')->value(true);
            $doclocator->limit($curr_page * $per_page, $per_page);

            $result = $doclocator->result();
            $total = $doclocator->length();

            $umiLinksHelper = $this->umiLinksHelper;
            $umiHierarchy = umiHierarchy::getInstance();

            if (($sz = sizeof($result)) > 0) {
                $block_arr = Array();
                $lines = $regions = $cities = $regionsFlip = $citiesFlip = Array();
                foreach ($result as $element) {
                    if (!$element instanceof umiHierarchyElement) {
                        continue;
                    }
                    $line_arr = Array();
                    $element_id = $element->getId();

                    $line_arr['attribute:id'] = $element_id;
                    $line_arr['node:name'] = $element->getName();
                    $line_arr['attribute:link'] = $umiLinksHelper->getLinkByParts($element);
                    $line_arr['xlink:href'] = "upage://" . $element_id;
                    $line_arr['void:header'] = $lines_arr['name'] = $element->getName();

                    $lent_name = "";
                    $lent_link = "";
                    $lent_id = $element->getParentId();

                    if ($lent_element = $umiHierarchy->getElement($lent_id)) {
                        $lent_name = $lent_element->getName();
                        $lent_link = $umiLinksHelper->getLinkByParts($lent_element);
                    }

                    $line_arr['attribute:lent_id'] = $lent_id;
                    $line_arr['attribute:lent_name'] = $lent_name;
                    $line_arr['attribute:lent_link'] = $lent_link;

                    $regionClinicId = '';
                    $regionClinic = $this->umiObjectsCollection->getObject($element->region_clinic);
                    if ($regionClinic instanceof umiObject) {
                        $regionClinicName = $regionClinic->name;
                        $regionClinicId = $regionClinic->id;
                        $regions[$regionClinicName] = $regionClinicId;

                        $line_arr['attribute:region_id'] = $regionClinicId;
                    }

                    $cityClinic = $this->umiObjectsCollection->getObject($element->city_clinic);
                    if ($cityClinic instanceof umiObject) {
                        $cityClinicName = $cityClinic->name;
                        $cities[$cityClinicName] = array($cityClinic->id, $regionClinicId);

                        $line_arr['attribute:city_id'] = $cityClinic->id;
                        $line_arr['attribute:city_name'] = $cityClinic->name;
                    }

                    //$element->region_clinic = 'Северо-Западный регион';
                    //$element->commit();

                    if ($element->lng == '') {
                        $adres = $cityClinicName . ' ' . $element->adres_clinic;
                        $coords_item = json_decode(umiRemoteFileGetter::get("https://geocode-maps.yandex.ru/1.x/?format=json&geocode=" . urlencode($adres)));
                        if (isset($coords_item->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos)) {
                            $pos = $coords_item->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
                            $pos_arr = explode(" ", $pos);
                            if (isset($pos_arr[1]) && isset($pos_arr[0])) {
                                $element->lng = $pos_arr[0];
                                $element->lat = $pos_arr[1];
                                $element->commit();
                            }
                        }
                    }

                    $lines[] = self::parseTemplate($template_line, $line_arr, $element_id);
                    $this->pushEditable("news", "item", $element_id);
                    umiHierarchy::getInstance()->unloadElement($element_id);
                }

                if (is_array($parent_id)) {
                    list($parent_id) = $parent_id;
                }

                foreach ($regions as $key => $value) {
                    $regionsFlip[] = array('attribute:id' => $value, 'node:name' => $key);
                }

                foreach ($cities as $key => $value) {
                    $citiesFlip[] = array('attribute:id' => $value[0], 'attribute:region_id' => $value[1], 'node:name' => $key);
                }
                //asort($regionsFlip);
                $block_arr['subnodes:regions'] = $regionsFlip;
                $block_arr['subnodes:cities'] = $citiesFlip;
                $block_arr['subnodes:items'] = $block_arr['void:lines'] = $lines;
                $block_arr['archive'] = ($total > 0) ? $template_archive : "";
                $parent = $umiHierarchy->getElement($parent_id);
                if ($parent instanceof umiHierarchyElement) {
                    $block_arr['archive_link'] = $umiLinksHelper->getLinkByParts($parent);
                }
                $block_arr['total'] = $total;
                $block_arr['per_page'] = $per_page;
                $block_arr['category_id'] = $parent_id;

                return $this->parseTemplate($template_block, $block_arr, $parent_id);
            } else {
                return $template_block_empty;
            }
        }

        public function getCategoryList($template = "default", $category_id = false, $limit = false, $ignore_paging = false, $i_need_deep = 0) {
		if(!$template) $template = "default";
		list($template_block, $template_block_empty, $template_line) = def_module::loadTemplates("catalog/".$template, "category_block", "category_block_empty", "category_block_line");

		if (!$i_need_deep) $i_need_deep = intval(getRequest('param4'));
		if (!$i_need_deep) $i_need_deep = 0;
		$i_need_deep = intval($i_need_deep);
		if ($i_need_deep === -1) $i_need_deep = 100;

		if((string) $category_id != '0') $category_id = $this->analyzeRequiredPath($category_id);

		$social = false;


		if ($category_id === false) {
			throw new publicException(getLabel('error-page-does-not-exist', null, $category_id));
		}

		$per_page = ($limit) ? $limit : $this->per_page;
		$curr_page = (int) getRequest('p');
		if ($ignore_paging) $curr_page = 0;

		$categories = new selector('pages');
		$categories->types('object-type')->name('doclocator', 'groupelements');
		if (is_array($category_id)) {
			foreach ($category_id as $category) {
				$categories->where('hierarchy')->page($category)->childs($i_need_deep + 1);
			}
		} else {
			$categories->where('hierarchy')->page($category_id)->childs($i_need_deep + 1);
		}
		$categories->limit($curr_page * $per_page, $per_page);
		$result = $categories->result();
		$total = $categories->length();

		if(($sz = sizeof($result)) > 0) {
			$block_arr = array();
			$lines = array();

			foreach ($result as $element) {
				if (!$element instanceof umiHierarchyElement) {
					continue;
				}

				$element_id = $element->getId();

				if($social && !$social->isHierarchyAllowed($element_id)) {
					continue;
				}

				$line_arr = Array();
				$line_arr['attribute:id'] = $element_id;
				$line_arr['void:alt_name'] = $element->getAltName();
				$line_arr['attribute:link'] = $this->umiLinksHelper->getLinkByParts($element);
				$line_arr['xlink:href'] = "upage://" . $element_id;
				$line_arr['node:text'] = $element->getName();
				$lines[] = self::parseTemplate($template_line, $line_arr, $element_id);
			}

			if (is_array($category_id)) {
				list($category_id) = $category_id;
			}

			$block_arr['attribute:category-id'] = $block_arr['void:category_id'] = $category_id;
			$block_arr['subnodes:items'] = $block_arr['void:lines'] = $lines;
			$block_arr['total'] = $total;
			$block_arr['per_page'] = $per_page;
			return self::parseTemplate($template_block, $block_arr, $category_id);
		} else {
			$block_arr = array();
			$block_arr['attribute:category-id'] = $block_arr['void:category_id'] = $category_id;
			return self::parseTemplate($template_block_empty, $block_arr, $category_id);
		}
	}

	public function config() {
		return __doclocator::config();
	}

	public function getEditLink($element_id, $element_type) {
		switch($element_type) {
			case "groupelements": {
				$link_add = $this->pre_lang . "/admin/doclocator/add/{$element_id}/item_element/";
				$link_edit = $this->pre_lang . "/admin/doclocator/edit/{$element_id}/";

				return Array($link_add, $link_edit);
				break;
			}

			case "item_element": {
				$link_edit = $this->pre_lang . "/admin/doclocator/edit/{$element_id}/";

				return Array(false, $link_edit);
				break;
			}

			default: {
				return false;
			}
		}


	}

};
?>