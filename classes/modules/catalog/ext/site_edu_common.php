<?php
    class site_edu_common {
        public function getLinkByObject($objId = NULL) {
			if(!$objId) return ;
				$h = umiHierarchy::getInstance();
				$arr_id = $h->getObjectInstances($objId);
				if (empty($arr_id)) return;
				$element_id = $arr_id[0];
				if($element_id) return $h->getPathById($element_id);
			return;
		}

    public function getPageIdByObject($objId = NULL) {
      if(!$objId) return ;
        $h = umiHierarchy::getInstance();
        $arr_id = $h->getObjectInstances($objId);
        if (empty($arr_id)) return;
        $element_id = $arr_id[0];
        if($element_id) return $element_id;
      return;
    }

		/*
		возвращает дату в необходимом формате, имеет возможность расширяться для вывода русских дат
		Пример вывода <xsl:value-of select="document(concat('udata://catalog/dateru/',.//property[@name = 'publish_time']/value/@unix-timestamp))/udata"/>
		В данный момент использование параметра @format, не работает
		*/
		public function dateru($time=NULL,$format=NULL) {
			$allowDateFormat = array('j','d','n','Y','months_ru');
			$months_ru = array(1 => 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');

			if(!$time) $time=time();
			if(!$format) $format="%j% %months_ru% %Y%";

			$sTemplate  = str_replace(array("&#037;", "&#37;"), "%", $format);
			preg_match_all("/%[A-z0-9_]+%/", $sTemplate, $aMarks);
			//var_dump($aMarks);
			foreach($aMarks[0] as $sMark){
				$param=str_replace("%", "", $sMark);
				if(in_array($param,$allowDateFormat)){
					if($param=='months_ru'){
						$month = date('n', $time);
						$sTemplate = str_replace($sMark,  $months_ru[$month], $sTemplate);
					}/* elseif($param=='Y'){
						$year = date('Y', $time);
						if($year == date('Y')) $year_val = '';
						else $year_val = $year.' года.';
						$sTemplate = str_replace($sMark,  $year_val, $sTemplate);
					} */else {
						$sTemplate = str_replace($sMark,  date($param, $time), $sTemplate);
					}
				}
			}
			return $sTemplate;
		}

		public function guide($typeId=NULL,$orderByField=NULL, $orderAsc=NULL) {
			if(!$typeId) return;
			$sel = new selector('objects');
			$sel->types('object-type')->id($typeId);
			if($orderByField){
				/* if($orderAsc!==) */
				$sel->order($orderByField)->asc();
			}

			$lines_arr = array();
			foreach($sel as $object) {
				$line=array();

				$line['attribute:id']=$object->id;
				$line['attribute:name']=$object->name;

				$lines_arr[] = $line;
			}

			return array('nodes:item'=>$lines_arr);
		}

		/* доработанная функция вывода каталога
		 * Есть возможность отфильтровать по спикеру
		 * TODO Есть возможность выводить свежие мероприятия или архивные
		 * */
		public function getSmartCatalogPro($template = 'default', $categoryId, $limit, $ignorePaging = false, $level = 1, $fieldName = false, $isAsc = true,$speaker = NULL) {
			/* @var catalog|__filter_catalog $this*/
			//var_dump(getRequest('filter'));
			//var_dump(getRequest('param7'));
			//var_dump($speaker);
			//	exit('gg');
			if (!is_string($template)) {
				$template = 'default';
			}

			list(
				$itemsTemplate,
				$emptyItemsTemplate,
				$emptySearchTemplates,
				$itemTemplate
			) = def_module::loadTemplates(
				'catalog/' . $template,
				'objects_block',
				'objects_block_empty',
				'objects_block_search_empty',
				'objects_block_line'
			);

			$umiHierarchy = umiHierarchy::getInstance();
			//@var iUmiHierarchyElement $category
			$category = $umiHierarchy->getElement($categoryId);

			if (!$category instanceof iUmiHierarchyElement) {
				throw new publicException(__METHOD__ . ': cant get page by id = '. $categoryId);
			}

			$limit = ($limit) ? $limit : $this->per_page;
			$currentPage = ($ignorePaging) ? 0 : (int) getRequest('p');
			$offset = $currentPage * $limit;

			if (!is_numeric($level)) {
				$level = 1;
			}

			$filteredProductsIds = null;
			$queriesMaker = null;
			if (is_array(getRequest('filter'))) {
				$emptyItemsTemplate = $emptySearchTemplates;
				$queriesMaker = $this->getCatalogQueriesMaker($category, $level);

				if (!$queriesMaker instanceof FilterQueriesMaker) {
					return $this->makeEmptyCatalogResponse($emptyItemsTemplate, $categoryId);
				}

				$filteredProductsIds = $queriesMaker->getFilteredEntitiesIds();

				if (count($filteredProductsIds) == 0) {
					return $this->makeEmptyCatalogResponse($emptyItemsTemplate, $categoryId);
				}
			}

			$products = new selector('pages');
			$products->types('hierarchy-type')->name('catalog', 'object');

			if (is_null($filteredProductsIds)) {
				$products->where('hierarchy')->page($categoryId)->childs($level);
			} else {
				$products->where('id')->equals($filteredProductsIds);
			}

			// add speaker filter
			if (!$speaker)$speaker = getRequest('param7');
			if ($speaker) {
				$products->where('speaker')->equals($speaker);
			}

			if ($fieldName) {
				if ($isAsc) {
					$products->order($fieldName)->asc();
				} else {
					$products->order($fieldName)->desc();
				}
			} else {
				$products->order('publish_date')->desc();
			}

			if ($queriesMaker instanceof FilterQueriesMaker) {
				if (!$queriesMaker->isPermissionsIgnored()) {
					$products->option('no-permissions')->value(true);
				}
			}

			$products->option('load-all-props')->value(true);
			$products->limit($offset, $limit);
			$pages = $products->result();
			$total = $products->length();

			if ($total == 0) {
				return $this->makeEmptyCatalogResponse($emptyItemsTemplate, $categoryId);
			}

			$result = array();
			$items = array();
			$umiLinksHelper = umiLinksHelper::getInstance();
			//@var iUmiHierarchyElement|umiEntinty $page
			foreach ($pages as $page) {
				$item = array();
				$pageId = $page->getId();
				$item['attribute:id'] = $pageId;
				$item['attribute:alt_name'] = $page->getAltName();
				$item['attribute:link'] = $umiLinksHelper->getLinkByParts($page);
				if ($publish_time = $page->getValue('publish_date')) {
					$item['attribute:publish_date'] = $publish_time->getFormattedDate("U");
				}
				$item['xlink:href'] ='upage://' . $pageId;
				$item['node:text'] = $page->getName();
				$items[] = def_module::parseTemplate($itemTemplate, $item, $pageId);
				def_module::pushEditable('catalog', 'object', $pageId);
				$umiHierarchy->unloadElement($pageId);
			}

			$result['subnodes:lines'] = $items;
			$result['numpages'] = umiPagenum::generateNumPage($total, $limit);
			$result['total'] = $total;
			$result['per_page'] = $limit;
			$result['category_id'] = $categoryId;

			return def_module::parseTemplate($itemsTemplate, $result, $categoryId);
		}


    }
?>