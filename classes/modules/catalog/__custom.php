<?php
abstract class __custom_catalog {
    public function getObjectsListNew($template = "default", $path = false, $limit = false, $ignore_paging = false, $i_need_deep = 0) {

        if(def_module::breakMe()) return;
        if(!$template) $template = "default";
        if (!$i_need_deep) $i_need_deep = intval(getRequest('param4'));
        if (!$i_need_deep) $i_need_deep = 0;
        $i_need_deep = intval($i_need_deep);
        if ($i_need_deep === -1) $i_need_deep = 100;

        $hierarchy = umiHierarchy::getInstance();
        list($template_block, $template_block_empty, $template_block_search_empty, $template_line) = def_module::loadTemplates("tpls/catalog/{$template}.tpl", "objects_block", "objects_block_empty", "objects_block_search_empty", "objects_block_line");
        $hierarchy_type_id = umiHierarchyTypesCollection::getInstance()->getTypeByName("catalog", "object")->getId();
        $category_id = def_module::analyzeRequiredPath($path);

        if($category_id === false && $path != KEYWORD_GRAB_ALL) {
            throw new publicException("Element not found: \"{$path}\"");
        }
        $hierarchy_type_id = umiHierarchyTypesCollection::getInstance()->getTypeByName("catalog", "object")->getId();
        $hierarchy_type = umiHierarchyTypesCollection::getInstance()->getType($hierarchy_type_id);
        $type_id = umiObjectTypesCollection::getInstance()->getBaseType($hierarchy_type->getName(), $hierarchy_type->getExt());
        if(!$type_id) {
            $type_id = umiObjectTypesCollection::getInstance()->getBaseType($hierarchy_type->getName(), $hierarchy_type->getExt());
        }
//Берем $per_page (кол-во элементов на странице) из атрибута макроса либо из реестра каталога (настройка модуля каталог)
        $per_page_r = regedit::getInstance()->getVal("//modules/catalog/per_page");
        $per_page = ($limit) ? $limit : $per_page_r;
// $curr_page - текущая страница
        $curr_page = getRequest('p');
        if($ignore_paging) $curr_page = 0;

        $sel = new umiSelection;
        $sel->setElementTypeFilter();
        $sel->addElementType($hierarchy_type_id);
        $sel->setOrderByProperty(468, true);

        if($path != KEYWORD_GRAB_ALL) {
            $sel->setHierarchyFilter();
            $sel->addHierarchyFilter($category_id, $i_need_deep);
        }

        $sel->setPermissionsFilter();
        $sel->addPermissions();

        if($path === KEYWORD_GRAB_ALL) {
            $curr_category_id = cmsController::getInstance()->getCurrentElementId();
        } else {
            $curr_category_id = $category_id;
        }


        if($path != KEYWORD_GRAB_ALL) {
            $type_id = $hierarchy->getDominantTypeId($curr_category_id, $i_need_deep);
        }

        if($type_id) {
            def_module::autoDetectOrders($sel, $type_id);
            def_module::autoDetectFilters($sel, $type_id);

        } else {
            $sel->setOrderFilter();
            $sel->setOrderByName();
        }

        if($curr_page !== "all") {
            $curr_page = (int) $curr_page;
            $sel->setLimitFilter();
            $sel->addLimit($per_page, $curr_page);
        }

        $result = umiSelectionsParser::runSelection($sel);
        $total = umiSelectionsParser::runSelectionCounts($sel);
        $umiLinksHelper = umiLinksHelper::getInstance();


        if(($sz = sizeof($result)) > 0) {
            $block_arr = Array();
            $lines = Array();
            for($i = 0; $i < $sz; $i++) {
                $element_id = $result[$i];
                $element = umiHierarchy::getInstance()->getElement($element_id);
                if(!$element) continue;
                $line_arr = Array();
                // Дополнительный параметр
                $line_arr['names'] = $element->getName();
                $line_arr['id'] = $element_id;
                $line_arr['attribute:link'] = $umiLinksHelper->getLink($element);
                $line_arr['price'] = $element->getValue('price');
                $line_arr['price_origin'] = $element->getValue('price_origin');
                $id = $element->id;
                $line_arr['pricedis'] = $this->showDiscountByDate($id);
                $elh = $element->getValue('photo');
                $line_arr['names'] = $element->getName();
                $date_begin = $element->getValue("publish_date")->getFormattedDate(U);
                $date_end = $element->getValue("finish_date")->getFormattedDate(U);
                $line_arr['dateru'] = $this->dateruPeriod($date_begin , $date_end);
                $line_arr['datebegin'] = $date_begin;
                $line_arr['datepubl'] = $element->getValue("publish_date");
                $max_reserv = $element->getValue("max_reserv");
                $curr_reserv = $element->getValue("curr_reserv");
                $line_arr['curr_reserv'] = $curr_reserv;
                $line_arr['discount_loyalty'] = $element->getValue('discount_loyalty');
                $add_loyalty = $element->getValue('add_loyalty');
                $line_arr['add_loyalty'] = $add_loyalty;

                if ($add_loyalty > 0) {
                    /*Расчет кол-ва звезд*/
                    $price = $element->getValue('price');
                    $loyaltyPotentialGUID = 198;
                    $sel = new selector('objects');
                    $sel->types('object-type')->id($loyaltyPotentialGUID);
                    $sel->where('event_price_from')->eqless($price);
                    $sel->where('event_price_to')->more($price);
                    $sel->limit(0, 1);
                    if ($object = $sel->first) {
                        $potentialBonus = (int)$object->name;
                    }
                    $line_arr['potentialBonus'] = $potentialBonus;
                }

                if($max_reserv > 0) {
                    $line_arr['reserv'] = $max_reserv - $curr_reserv;
                    $line_arr['max_reserv'] = $max_reserv;
                }
                else {
                    $line_arr['reserv'] = 25;
                    $line_arr['max_reserv'] = 0;
                }

                $idauto = $element->getValue('speaker');
                foreach($idauto as $Id) {
                    $objects = umiObjectsCollection::getInstance();
                    $autoObject = $objects->getObject($Id);
                    $line_arr['speaker'] = $autoObject->name;
                    $line_arr['speakerid'] = $autoObject->id;
                    $line_arr['speakerdescr'] = $autoObject->getValue('full_desrc');
                    $line_arr['speakerdescr2'] = $autoObject->getValue('full_descr2');
                    $elh = $autoObject->getValue("photo");
                }

                $idautos = $element->getValue('speaker');
                $line_arr2 = array();
                foreach($idautos as $Ids => $key) {
                    $objects = umiObjectsCollection::getInstance();
                    $autoObject = $objects->getObject($key);
                    $line_arr2[$Ids]['speaker'] = $autoObject->name;
                    $line_arr2[$Ids]['speakerid'] = $autoObject->id;
                    $line_arr2[$Ids]['speakerdescr'] = $autoObject->getValue('full_desrc');
                    $line_arr2[$Ids]['speakerdescr2'] = $autoObject->getValue('full_descr2');
                    $elh2 = $autoObject->getValue("photo");
                    if ($elh2) {
                        $imgsrc = $elh2->getFilePath();
                        $sourcthmb = $this->makeThumbnailFullmy($imgsrc, '350', '350');
                        $line_arr2[$Ids]['thumb'] = $sourcthmb;
                    }
                    else {
                        $line_arr2[$Ids]['thumb'] = "/images/nofoto_260_auto_5_80.jpg";
                    }
                }
                $line_arr['nodes:speakitems'] = $line_arr2;

                $gorod_in = $element->getValue('gorod_in');
                $objects = umiObjectsCollection::getInstance();
                $gorod_inObject = $objects->getObject($gorod_in);
                $line_arr['gorodin'] = $gorod_inObject->name;
                $line_arr['gorodinid'] = $gorod_in;

                $line_arr['mestoprovedeniya'] = $element->getValue("mesto_provedeniya");

                $event_type = $element->getValue('event_type');
                $objects = umiObjectsCollection::getInstance();
                $event_typeObject = $objects->getObject($event_type);
                $line_arr['eventtype'] = $event_typeObject->name;
                $line_arr['eventtypeid'] = $event_type;

                $organizer = $element->getValue('organizer');
				if($organizer) {
					$organizerObject = $objects->getObject($organizer);
					$line_arr['organizer'] = $organizerObject->name;
					$line_arr['organizerimg'] = $organizerObject->getValue("logo");
					$line_arr['organizerid'] = $organizer;
				}

                $level = $element->getValue('level');
                $levelObject = $objects->getObject($level);
                $line_arr['level'] = $levelObject->name;
                $line_arr['levelid'] = $level;

                $level_shoo = $element->getValue('level_shoo');
                $level_shooObject = $objects->getObject($level_shoo);
                $line_arr['levelshoo'] = $level_shooObject->name;
                $line_arr['levelshooid'] = $level_shoo;

                $line_arr['description'] = $element->getValue("description");
                $line_arr['aboutreg'] = $element->getValue("about_reg");

                $line_arr['hide_reg_button'] = $element->getValue("hide_reg_button");
                $line_arr['event_url_redirect'] = $element->getValue("event_url_redirect");

                $line_arr['id'] = $element->id;

                if ($elh) {
                    $imgsrc = $elh->getFilePath();
                    $sourcthmb = $this->makeThumbnailFullmy($imgsrc, '350', '350');
                    $line_arr['thumb'] = $sourcthmb;
                }
                else {
                    $line_arr['thumb'] = "/images/nofoto_260_auto_5_80.jpg";
                }

                $lines[] = def_module::parseTemplate($template_line, $line_arr, $element_id);
                templater::pushEditable("catalog", "object", $element_id);
                umiHierarchy::getInstance()->unloadElement($element_id);
            }
            $block_arr['subnodes:lines'] = $lines;
            $block_arr['numpages'] = umiPagenum::generateNumPage($total, $per_page);
            $block_arr['total'] = $total;
            $block_arr['per_page'] = $per_page;
            $block_arr['category_id'] = $category_id;

            if($type_id) {
                $block_arr['type_id'] = $type_id;
            }
            return def_module::parseTemplate($template_block, $block_arr, $category_id);
        } else {
            $block_arr['numpages'] = umiPagenum::generateNumPage(0, 0);
            $block_arr['lines'] = "";
            $block_arr['total'] = 0;
            $block_arr['per_page'] = 0;
            $block_arr['category_id'] = $category_id;

            return def_module::parseTemplate($template_block_empty, $block_arr, $category_id);
        }
    }

    public function checkevents($template = "default", $path = false, $limit = false, $ignore_paging = false, $i_need_deep = 0) {

        if(def_module::breakMe()) return;
        if(!$template) $template = "default";
        if (!$i_need_deep) $i_need_deep = intval(getRequest('param4'));
        if (!$i_need_deep) $i_need_deep = 0;
        $i_need_deep = intval($i_need_deep);
        if ($i_need_deep === -1) $i_need_deep = 100;

        $hierarchy = umiHierarchy::getInstance();
        list($template_block, $template_block_empty, $template_block_search_empty, $template_line) = def_module::loadTemplates("tpls/catalog/{$template}.tpl", "objects_block", "objects_block_empty", "objects_block_search_empty", "objects_block_line");
        $hierarchy_type_id = umiHierarchyTypesCollection::getInstance()->getTypeByName("catalog", "object")->getId();
        $category_id = def_module::analyzeRequiredPath($path);

        if($category_id === false && $path != KEYWORD_GRAB_ALL) {
            throw new publicException("Element not found: \"{$path}\"");
        }
        $hierarchy_type_id = umiHierarchyTypesCollection::getInstance()->getTypeByName("catalog", "object")->getId();
        $hierarchy_type = umiHierarchyTypesCollection::getInstance()->getType($hierarchy_type_id);
        $type_id = umiObjectTypesCollection::getInstance()->getBaseType($hierarchy_type->getName(), $hierarchy_type->getExt());
        if(!$type_id) {
            $type_id = umiObjectTypesCollection::getInstance()->getBaseType($hierarchy_type->getName(), $hierarchy_type->getExt());
        }
//Берем $per_page (кол-во элементов на странице) из атрибута макроса либо из реестра каталога (настройка модуля каталог)
        $per_page_r = regedit::getInstance()->getVal("//modules/catalog/per_page");
        $per_page = ($limit) ? $limit : $per_page_r;
// $curr_page - текущая страница
        $curr_page = getRequest('p');
        if($ignore_paging) $curr_page = 0;

        $sel = new umiSelection;
        $sel->setElementTypeFilter();
        $sel->addElementType($hierarchy_type_id);

        if($path != KEYWORD_GRAB_ALL) {
            $sel->setHierarchyFilter();
            $sel->addHierarchyFilter($category_id, $i_need_deep);
        }

        $sel->setPermissionsFilter();
        $sel->addPermissions();

        $timecreate = getRequest('create');
        if (isset($timecreate)) {
            $timecreate = getRequest('create')+1;
        }
        else {
            $timecreate = '0';
        }
        $sel->addPropertyFilterMore(289, $timecreate);
        $sel->setOrderByProperty(289, false);

        if($path === KEYWORD_GRAB_ALL) {
            $curr_category_id = cmsController::getInstance()->getCurrentElementId();
        } else {
            $curr_category_id = $category_id;
        }


        if($path != KEYWORD_GRAB_ALL) {
            $type_id = $hierarchy->getDominantTypeId($curr_category_id, $i_need_deep);
        }

        if($type_id) {
            def_module::autoDetectOrders($sel, $type_id);
            def_module::autoDetectFilters($sel, $type_id);

        } else {
            $sel->setOrderFilter();
            $sel->setOrderByName();
        }

        if($curr_page !== "all") {
            $curr_page = (int) $curr_page;
            $sel->setLimitFilter();
            $sel->addLimit($per_page, $curr_page);
        }

        $result = umiSelectionsParser::runSelection($sel);
        $total = umiSelectionsParser::runSelectionCounts($sel);


        if(($sz = sizeof($result)) > 0) {
            $block_arr = Array();
            $lines = Array();
            for($i = 0; $i < $sz; $i++) {
                $element_id = $result[$i];
                $element = umiHierarchy::getInstance()->getElement($element_id);
                if(!$element) continue;
                $line_arr = Array();

                $ptime = $element->getValue("date_create_object");
                $formatedptime = $ptime->getFormattedDate("U");
                $line_arr['date_create_object'] = $formatedptime;


                $lines[] = def_module::parseTemplate($template_line, $line_arr, $element_id);
                templater::pushEditable("catalog", "object", $element_id);
                umiHierarchy::getInstance()->unloadElement($element_id);
            }
            $block_arr['subnodes:lines'] = $lines;
            $block_arr['numpages'] = umiPagenum::generateNumPage($total, $per_page);
            $block_arr['total'] = $total;
            $block_arr['per_page'] = $per_page;
            $block_arr['category_id'] = $category_id;

            if($type_id) {
                $block_arr['type_id'] = $type_id;
            }
            return def_module::parseTemplate($template_block, $block_arr, $category_id);
        } else {
            $block_arr['numpages'] = umiPagenum::generateNumPage(0, 0);
            $block_arr['lines'] = "";
            $block_arr['total'] = 0;
            $block_arr['per_page'] = 0;
            $block_arr['category_id'] = $category_id;

            return def_module::parseTemplate($template_block_empty, $block_arr, $category_id);
        }
    }


    public function getMonthRu($month, $year, $type=0)
    {
        // Проверка существования месяца
        if (!checkdate($month, 1, $year)) {
            throw new publicException("Проверьте порядок ввода даты.");
        }
        $months_ru = Array(1 => 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
        $months_ru1 = Array(1 => 'январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь');
        if($type==1){return $months_ru1[$month];}
        return $months_ru[$month];
    }

    public function dateru($time) {
        if(!$time) return;
        $day = date('j', $time);
        $month = date('n', $time);
        $year = date('Y', $time);
        $hour = date('g', $time);
        $minutes = date('i', $time);
        $seconds = date('s', $time);

        $temp = Array(
            'month' => $this->getMonthRu($month, $year),
            'day' => $day,
            'year' => $year,
            'hour' => $hour,
            'minutes' => $minutes,
            'seconds' => $seconds
        );
        return def_module::parseTemplate('', $temp);
    }
    // test data:
    // 1352204856 Tue, 06 Nov 2012 12:27:36 GMT
    // 1352377656 Thu, 08 Nov 2012 12:27:36 GMT
    // 1355660856 Sun, 16 Dec 2012 12:27:36 GMT
    // 1357475256 Sun, 06 Jan 2013 12:27:36 GMT
    // 1383740856 Wed, 06 Nov 2013 12:27:36 GMT
    public function dateruPeriod($timeBegin=NULL, $timeEnd=NULL) {
        if(!$timeBegin || !$timeEnd) return "-";
        if(!is_numeric($timeBegin) || !is_numeric($timeEnd)) return "=";
        if($timeEnd < $timeBegin) {
            $tmp = $timeEnd;
            $timeEnd = $timeBegin;
            $timeBegin = $tmp;
        }

        $dayBegin = date('d', $timeBegin);
        $monthBegin = date('n', $timeBegin);
        $yearBegin = date('Y', $timeBegin);
        $dayEnd = date('d', $timeEnd);
        $monthEnd = date('n', $timeEnd);
        $yearEnd = date('Y', $timeEnd);

        $timeEnd_forCalc = strtotime("$dayEnd-$monthEnd-$yearEnd ");
        $timeBegin_forCalc = strtotime("$dayBegin-$monthBegin-$yearBegin ");

        $yearCurrent = date('Y');

        if(($monthBegin != $monthEnd) || ($yearBegin != $yearEnd)) {
            $firstLine = $dayBegin.' '.$this->getMonthRu($monthBegin, $yearBegin).' ';
            $secondLine = $dayEnd.' '.$this->getMonthRu($monthEnd, $yearEnd);
            /* if($yearBegin != $yearEnd) {
                $secondLine .= ' '.$yearEnd.' г.';
            } */
            $firstLine .= '-';
        } else {
            $firstLine = ($dayBegin!=$dayEnd) ? $dayBegin.'-'.$dayEnd : $dayBegin;
            $secondLine = $this->getMonthRu($monthBegin, $yearBegin);
        }

        //if(($yearEnd != $yearCurrent) && ($yearBegin == $yearEnd)) {
        $secondLine .= ' '.$yearEnd.' г.';
        //}
        $days = (int)(($timeEnd_forCalc - $timeBegin_forCalc) / 86400) +1; // секунд в 24 часах
        $temp = Array(
            'firstline' => $firstLine,
            'secondline' => $secondLine,
            'days' => $days.' '.$this->wordDays($days)
        );
        return $temp;
    }

    public function wordDays($days) {
        $units = $days - (int)($days / 10) * 10;
        $tens = $days - (int)($days / 100) * 100 - $units;
        if($tens == 10) return 'дней'; // 11-19 дней

        if($units == 1) return 'день'; // 1 21 31... день
        if(($units > 1) && ($units < 5)) return 'дня'; // 2 3 4 22 23 24 дня
        return 'дней'; // 5 6 7 8 9 20 дней
    }

    function makeThumbnailFullmy($path, $width, $height, $crop = true, $cropside = 5, $isLogo = false, $quality = 50) {

        $isSharpen=true;
        $thumbs_path="./images/cms/thumbs/";
        $image = new umiImageFile($path);
        $file_name = $image->getFileName();
        $file_ext = strtolower($image->getExt());
        $file_ext = ($file_ext=='bmp'?'jpg':$file_ext);

        $allowedExts = Array('gif', 'jpeg', 'jpg', 'png', 'bmp');
        if(!in_array($file_ext, $allowedExts)) return "";

        $file_name = substr($file_name, 0, (strlen($file_name) - (strlen($file_ext) + 1)) );

        $thumbPath = sha1($image->getDirName());

        if (!is_dir($thumbs_path . $thumbPath)) {
            mkdir($thumbs_path . $thumbPath, 0755, true);
        }

        $file_name_new = $file_name . '_' . $width . '_' . $height . '_' . $cropside . '_' . $quality . "." . $file_ext;
        $path_new = $thumbs_path . $thumbPath . '/' . $file_name_new;

        if(!file_exists($path_new) || filemtime($path_new) < filemtime($path)) {
            if(file_exists($path_new)) {
                unlink($path_new);
            }

            $width_src = $image->getWidth();
            $height_src = $image->getHeight();

            if(!($width_src && $height_src)) {
                throw new coreException(getLabel('error-image-corrupted', null, $path));
            }

            if($height == "auto") {
                $real_height = (int) round($height_src * ($width / $width_src));
                //change
                $height=$real_height;
                $real_width = (int) $width;
            } else {
                if($width == "auto") {
                    $real_width = (int) round($width_src * ($height / $height_src));
                    //change
                    $width=$real_width;
                } else {
                    $real_width = (int) $width;
                }

                $real_height = (int) $height;
            }

            $offset_h=0;
            $offset_w=0;

            // realloc: devision by zero fix
            if (!intval($width) || !intval($height)) {
                $crop = false;
            }

            if ($crop){
                $width_ratio = $width_src/$width;
                $height_ratio = $height_src/$height;

                if ($width_ratio > $height_ratio){
                    $offset_w = round(($width_src-$width*$height_ratio)/2);
                    $width_src = round($width*$height_ratio);
                } elseif ($width_ratio < $height_ratio){
                    $offset_h = round(($height_src-$height*$width_ratio)/2);
                    $height_src = round($height*$width_ratio);
                }


                if($cropside) {
                    //defore all it was cropside work like as - 5
                    //123
                    //456
                    //789
                    switch ($cropside):
                        case 1:
                            $offset_w = 0;
                            $offset_h = 0;
                            break;
                        case 2:
                            $offset_h = 0;
                            break;
                        case 3:
                            $offset_w += $offset_w;
                            $offset_h = 0;
                            break;
                        case 4:
                            $offset_w = 0;
                            break;
                        case 5:
                            break;
                        case 6:
                            $offset_w += $offset_w;
                            break;
                        case 7:
                            $offset_w = 0;
                            $offset_h += $offset_h;
                            break;
                        case 8:
                            $offset_h += $offset_h;
                            break;
                        case 9:
                            $offset_w += $offset_w;
                            $offset_h += $offset_h;
                            break;
                    endswitch;
                }
            }

            $thumb = imagecreatetruecolor($real_width, $real_height);

            $source_array = $image->createImage($path);
            $source = $source_array['im'];

            if ($width*4 < $width_src && $height*4 < $height_src) {
                $_TMP=array();
                $_TMP['width'] = round($width*4);
                $_TMP['height'] = round($height*4);

                $_TMP['image'] = imagecreatetruecolor($_TMP['width'], $_TMP['height']);

                if ($file_ext == 'gif') {
                    $_TMP['image_white'] = imagecolorallocate($_TMP['image'], 255, 255, 255);
                    imagefill($_TMP['image'], 0, 0, $_TMP['image_white']);
                    imagecolortransparent($_TMP['image'], $_TMP['image_white']);
                    imagealphablending($source, TRUE);
                    imagealphablending($_TMP['image'], TRUE);
                } else {
                    imagealphablending($_TMP['image'], false);
                    imagesavealpha($_TMP['image'], true);
                }
                imagecopyresampled($_TMP['image'], $source, 0, 0, $offset_w, $offset_h, $_TMP['width'], $_TMP['height'], $width_src, $height_src);

                imageDestroy($source);

                $source = $_TMP['image'];
                $width_src = $_TMP['width'];
                $height_src = $_TMP['height'];

                $offset_w = 0;
                $offset_h = 0;
                unset($_TMP);
            }

            if ($file_ext == 'gif') {
                $thumb_white_color = imagecolorallocate($thumb, 255, 255, 255);
                imagefill($thumb, 0, 0, $thumb_white_color);
                imagecolortransparent($thumb, $thumb_white_color);
                imagealphablending($source, TRUE);
                imagealphablending($thumb, TRUE);
            } else {
                imagealphablending($thumb, false);
                imagesavealpha($thumb, true);
            }

            imagecopyresampled($thumb, $source, 0, 0, $offset_w, $offset_h, $width, $height, $width_src, $height_src);
            if($isSharpen) $thumb = makeThumbnailFullUnsharpMask($thumb,80,.5,3);

            switch($file_ext) {
                case 'gif':
                    $res = imagegif($thumb, $path_new);
                    break;
                case 'png':
                    $res = imagepng($thumb, $path_new);
                    break;
                default:
                    $res = imagejpeg($thumb, $path_new, $quality);
            }
            if(!$res) {
                throw new coreException(getLabel('label-errors-16008'));
            }

            imageDestroy($source);
            imageDestroy($thumb);

            if($isLogo) {
                umiImageFile::addWatermark($path_new);
            }
        }

        $value = new umiImageFile($path_new);

        $arr = $value->getFilePath(true);
        return $arr;
    }

    public function custom_search($category_id = false, $group_names = "", $template = "default", $type_id = false) {
        /**
         * @var __search_catalog|self|def_module $this
         */
        if(!$template) $template = "default";

        if (!$category_id) {
            $category_id = $this->analyzeRequiredPath($category_id);
            if(!$category_id) return "";
        }


        list($template_block, $template_block_empty, $template_block_line, $template_block_line_text,
            $template_block_line_relation, $template_block_line_item_relation, $template_block_line_item_relation_separator,
            $template_block_line_price, $template_block_line_boolean, $template_block_line_symlink) =

            def_module::loadTemplates("catalog/".$template, "search_block", "search_block_empty",
                "search_block_line", "search_block_line_text", "search_block_line_relation",
                "search_block_line_item_relation", "search_block_line_item_relation_separator",
                "search_block_line_price", "search_block_line_boolean", "search_block_line_symlink");

        $block_arr = Array();

        if($type_id === false) {
            $type_id = umiHierarchy::getInstance()->getDominantTypeId($category_id);
        }

        if(is_null($type_id)) return "";

        if(!($type = umiObjectTypesCollection::getInstance()->getType($type_id))) {
            trigger_error("Failed to load type", E_USER_WARNING);
            return "";
        }

        $fields = array();
        /**
         * @var umiFieldsGroup[] $groups
         */
        $groups = array();
        $lines = array();

        $group_names = trim($group_names);
        if($group_names) {
            $group_names_arr = explode(" ", $group_names);
            foreach($group_names_arr as $group_name) {
                if(!($fields_group = $type->getFieldsGroupByName($group_name))) {
                } else {
                    $groups[] = $fields_group;
                }
            }
        } else {
            $groups = $type->getFieldsGroupsList();
        }


        $lines_all = Array();
        $groups_arr = Array();

        foreach($groups as $fields_group) {
            $fields = $fields_group->getFields();
            if($fields_group->getName() == 'opisanie_tovara') continue;

            $group_block = Array();
            $group_block['attribute:name'] = $fields_group->getName();
            $group_block['attribute:title'] = $fields_group->getTitle();

            $lines = Array();


            foreach($fields as $field_id => $field) {
                if(!$field->getIsVisible()) continue;
                if(!$field->getIsInFilter()) continue;

                $line_arr = Array();

                $field_type_id = $field->getFieldTypeId();
                $field_name = $field->getName();
                $field_title = $field->getTitle();
                $field_type = umiFieldTypesCollection::getInstance()->getFieldType($field_type_id);

                $data_type = $field_type->getDataType();

                if($data_type == 'text' or $data_type == 'date' or $data_type == 'string'or $data_type == 'wysiwyg' or $data_type == 'float' or $data_type == 'boolean' or $data_type == 'symlink') continue;

                $line = Array();
                $item_arr = Array();
                switch($data_type) {
                    case "relation": {
                        $sql_guide_id = "SELECT guide_id FROM `cms3_object_fields` WHERE `id` = '{$field_id}' LIMIT 1";
                        $result1 = l_mysql_query($sql_guide_id);
                        list($guide_id) = mysql_fetch_row($result1);
                        $o = umiObjectsCollection::getInstance();
                        $items = $o ->getGuidedItems($guide_id);
                        $line['attribute:unfilter_link'] = '?scheme=udata';
                        $line['attribute:id'] = $field_id;
                        $line['attribute:name'] = $field_name;
                        $line['attribute:title'] = $field_title;
                        $line['attribute:data-type'] = $data_type;

                        foreach($items as $prop_val_id=>$name){
                            $sql4 = "SELECT COUNT(*)
								FROM cms3_hierarchy h, cms3_object_content oc
								WHERE h.obj_id = oc.obj_id
								AND h.rel = {$category_id}
								AND h.is_deleted = '0'
								AND h.is_active = '1'
								AND oc.rel_val = {$prop_val_id}";
                            $result4 = l_mysql_query($sql4);
                            list($items_props) = mysql_fetch_row($result4);

                            $item = array(
                                'attribute:id'      => $prop_val_id,
                                'attribute:filter_link'       => '?scheme=udata&amp;fields_filter%5B'.$field_name.'%5D='.$prop_val_id,
                                'attribute:unfilter_link'    => '?scheme=udata',
                                'attribute:cnt'         => $items_props,
                                'node:name'         => $name
                            );
                            if ($items_props > 0) {
                                $item_arr[] = $item;}
                        }

                        $line['values']['nodes:item'] = $item_arr;

                        break;
                    }

                    case "text": {
                        $line = $this->parseSearchText($field, $template_block_line_text);
                        break;
                    }

                    case "date": {
                        $line = $this->parseSearchDate($field, $template_block_line_text);
                        break;
                    }

                    case "string": {
                        $line = $this->parseSearchText($field, $template_block_line_text);
                        break;
                    }

                    case "wysiwyg": {
                        $line = $this->parseSearchText($field, $template_block_line_text);
                        break;
                    }

                    case "float":
                    case "price": {
                        $line = $this->parseSearchPrice($field, $template_block_line_price);
                        break;
                    }

                    case "int": {
                        $line['attribute:name'] = $field_name;
                        $line['attribute:title'] = $field_title;
                        $line['attribute:data-type'] = $data_type;

                        $sql = "SELECT MAX( oc.int_val ), MIN( oc.int_val )
							FROM cms3_hierarchy h, cms3_object_content oc
							WHERE h.obj_id = oc.obj_id
							AND h.rel = {$category_id}
							AND h.is_deleted = '0'
							AND h.is_active = '1'
							AND oc.field_id ={$field_id}";
                        $result3 = l_mysql_query($sql);
                        list($max, $min)= mysql_fetch_row($result3);
                        if($max != 0){
                            $line['value_from'] = $min;
                            $line['value_to'] = $max;
                        }
                        break;
                    }

                    case "boolean": {
                        $line = $this->parseSearchBoolean($field, $template_block_line_boolean);
                        break;
                    }

                    case "symlink": {
                        $line = $this->parseSearchSymlink($field, $template_block_line_symlink, $category_id);
                        break;
                    }

                    default: {
                        $line = "[search filter for \"{$data_type}\" not specified]";
                        break;
                    }
                }

                if (def_module::isXSLTResultMode()) {
                    if (is_array($line)) {
                        $line['attribute:data-type'] = $data_type;
                    }
                }

                $line_arr['void:selector'] = $line;

                if (def_module::isXSLTResultMode()) {
                    $lines[] = $line;
                } else {
                    $lines[] = $tmp = def_module::parseTemplate($template_block_line, $line_arr);
                    $lines_all[] = $tmp;
                }
            }


            if(empty($lines)) {
                continue;
            }

            $group_block['nodes:field'] = $lines;
            $groups_arr = $group_block;

        }

        $block_arr['void:items'] = $block_arr['void:lines'] = $lines_all;
        $block_arr = $groups_arr;

        return $block_arr;
    }

    public function showDiscountByDate($elementId) {
        $hierarchy = umiHierarchy::getInstance();

        $element = $hierarchy -> getElement($elementId);
        if (!$element)
            return "0";


        $discountFieldsCheck = array(
            'data_okonchaniya_skidki_1' => 'skidka_ogranichena_datoj_1',
            'data_okonchaniya_skidki_2' => 'skidka_ogranichena_datoj_2',
            'data_okonchaniya_skidki_3' => 'skidka_ogranichena_datoj_3',
            'data_okonchaniya_skidki_4' => 'skidka_ogranichena_datoj_4',
            'data_okonchaniya_skidki_5' => 'skidka_ogranichena_datoj_5'
        );
        $i=0;
        foreach ($discountFieldsCheck as $discountDate => $discountCheck) {
            $i=$i+1;
            $check = $element -> getValue($discountCheck);
            if ($check) {
                $date = $element -> getValue($discountDate);
                if(!$date) continue;
                $dateU = $date->getFormattedDate("U");


                if($time<$dateU){
                    $name = $element -> getValue('nazvanie_skidki_'.$i);
                    $value = $element -> getValue('stoimost_meropriyatiya_s_uchetom_skidki_'.$i);


                    return $value;
                }
            }
        }

        return 0;
    }



};
?>