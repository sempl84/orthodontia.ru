<?php
	/**
	 * Языковые константы для английской версии
	 */
	$i18n = [
		'header-appointment-orders' => 'Appointment orders',
		"module-appointment"	=> "Appointments",
		"use-compatible-modules" => "This module works only in <a target=\"_blank\" href=\"http://dev.docs.umi-cms.ru/nastrojka_sistemy/dostupnye_sekcii/sekciya_system/#sel=103:1,103:3\">new modules mode</a>"
	];
?>
