<?php
	/**
	 * Группы прав на функционал модуля
	 */
	$permissions = [
		/**
		 * Права на запись
		 */
		'enroll' => [
		],
		/**
		 * Права на администрирование модуля
		 */
		'manage' => [
			'orders'
		]
	];
?>
