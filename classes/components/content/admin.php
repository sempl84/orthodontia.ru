<?php
	/**
	 * Класс функционала административной панели
	 */
	class ContentAdmin {

		use baseModuleAdmin;
		/**
		 * @var content $module
		 */
		public $module;

		/**
		 * Возвращает список объектов по идентификатору объектного типа
		 * @param int $type_id идентификатор объектного типа
		 * @return array
		 * @throws coreException
		 */
		public function getObjectsByTypeList($type_id) {
			$objectsCollection = umiObjectsCollection::getInstance();
			$objects = $objectsCollection->getGuidedItems($type_id);

			$items = array();

			foreach($objects as $item_id => $item_name) {
				$items[] = array(
					'attribute:id'	=> $item_id,
					'node:name'		=> $item_name
				);
			}

			return array(
				'items' => array(
					'nodes:item' => $items
				)
			);
		}

		/**
		 * Возвращает список объектов по имени и расширению
		 * иерархического типа
		 * @param string $module имя иерархического типа
		 * @param string $method расширение иерархического типа
		 * @return array
		 * @throws coreException
		 */
		public function getObjectsByBaseTypeList($module, $method) {
			$objectTypesCollection = umiObjectTypesCollection::getInstance();
			$objectsCollection = umiObjectsCollection::getInstance();

			$type_id = $objectTypesCollection->getTypeIdByHierarchyTypeName($module, $method);

			$objects = $objectsCollection->getGuidedItems($type_id);

			$items = array();
			foreach($objects as $item_id => $item_name) {
				$items[] = array(
					'attribute:id'	=> $item_id,
					'node:name'		=> $item_name
				);
			}
			return array('items' => array('nodes:item' => $items));
		}

		/**
		 * Возвращает список страниц по имени и расширению
		 * иерархического типа
		 * @param string $module имя иерархического типа
		 * @param string $method расширение иерархического типа
		 * @return array
		 * @throws publicException
		 */
		public function getPagesByBaseTypeList($module, $method) {
			$hierarchyTypesCollection = umiHierarchyTypesCollection::getInstance();
			$hierarchy = umiHierarchy::getInstance();

			$type = $hierarchyTypesCollection->getTypeByName($module, $method);
			/* @var iUmiHierarchyType|iUmiEntinty $type */
			if ($type instanceof iUmiHierarchyType) {
				$type_id = $type->getId();
			} else {
				throw new publicException("Hierarchy type {$module}::{$method} doesn't exist");
			}

			$sel = new umiSelection;
			$sel->addElementType($type_id);
			$sel->addPermissions();

			$result = umiSelectionsParser::runSelection($sel);
			$pages = array();
			foreach ($result as $element_id) {
				$element = $hierarchy->getElement($element_id);
				if ($element instanceof umiHierarchyElement) {
					$pages[] = $element;
				}
			}
			return Array("pages" => Array("nodes:page" => $pages));
		}

		/**
		 * Возвращает список шаблонов сайта
		 */
		public function domainTemplates() {
			$domains = domainsCollection::getInstance();
			$langs = langsCollection::getInstance();
			$templates = templatesCollection::getInstance();

			$data = Array();
			foreach ($domains->getList() as $domain) {
				$domainId = $domain->getId();

				foreach ($langs->getList() as $lang) {
					$langId = $lang->getId();

					foreach ($templates->getTemplatesList($domainId, $langId) as $template) {
						$data['templates']['nodes:template'][] = $template;
					}
				}
			}

			foreach ($domains->getList() as $domain) {
				$data['domains']['nodes:domain'][] = $domain;
			}

			foreach ($langs->getList() as $lang) {
				$data['langs']['nodes:lang'][] = $lang;
			}

			$this->setDataType("list");
			$this->setActionType("view");

			$this->setData($data);
			return $this->doData();
		}

		/**
		 * Возвращает список доменов системы для построения деревьев
		 * @throws coreException
		 */
		public function sitetree() {
			$domains = domainsCollection::getInstance()->getList();
			$permissions = permissionsCollection::getInstance();
			$user_id = $permissions->getUserId();

			$this->setDataType("list");
			$this->setActionType("view");

			foreach($domains as $i => $domain) {
				$domain_id = $domain->getId();

				if (!$permissions->isAllowedDomain($user_id, $domain_id)) {
					unset($domains[$i]);
				}
			}

			$data = $this->prepareData($domains, "domains");

			$this->setData($data, sizeof($domains));
			$this->doData();
		}

		/**
		 * Возвращает данные списка страниц контента для административной панели
		 * @return bool
		 * @throws coreException
		 * @throws selectorException
		 */
		public function tree() {
			$this->setDataType("list");
			$this->setActionType("view");

			if ($this->module->ifNotXmlMode()) {
				$this->setDirectCallError();
				$this->doData();
				return true;
			}

			$limit = getRequest('per_page_limit');
			$currentPage = getRequest('p');
			$offset = $currentPage * $limit;

			$sel = new selector('pages');
			$sel->types('hierarchy-type')->name('content', 'page');
			$sel->limit($offset, $limit);

			selectorHelper::detectHierarchyFilters($sel);
			selectorHelper::detectWhereFilters($sel);
			selectorHelper::detectOrderFilters($sel);

			$data = $this->prepareData($sel->result, "pages");

			$this->setData($data, $sel->length);
			$this->setDataRangeByPerPage($limit, $currentPage);
			$this->doData();

			return true;
		}

		/**
		 * Возвращает форму создания страницы.
		 * Если передан ключевой параметр $_REQUEST['param2'] = do,
		 * то метод запустит добавление страницы
		 * @throws coreException
		 * @throws expectElementException
		 * @throws wrongElementTypeAdminException
		 */
		public function add() {
			$parent = $this->expectElement("param0");
			$type = (string) getRequest("param1");
			$mode = (string) getRequest("param2");

			$inputData = array(
				'type' => $type,
				'parent' => $parent,
				'type-id' => getRequest('type-id'),
				'allowed-element-types' => array('page', '')
			);

			if ($mode == "do") {
				$this->saveAddedElementData($inputData);
				$this->chooseRedirect();
			}

			$this->setDataType("form");
			$this->setActionType("create");

			$data = $this->prepareData($inputData, "page");

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает форму редактирования страницы.
		 * Если передан ключевой параметр $_REQUEST['param1'] = do,
		 * то метод запустит сохранение страницы
		 * @throws coreException
		 * @throws expectElementException
		 * @throws wrongElementTypeAdminException
		 */
		public function edit() {
			$element = $this->expectElement("param0");
			$mode = (string) getRequest('param1');

			$inputData = array(	"element" => $element,
				"allowed-element-types" => array('page', '')
			);

			if ($mode == "do") {
				$this->saveEditedElementData($inputData);
				$this->chooseRedirect();
			}

			$this->setDataType("form");
			$this->setActionType("modify");

			$data = $this->prepareData($inputData, "page");

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает настройки системы.
		 * Если передан ключевой параметр $_REQUEST['param0'] = do,
		 * то метод запустит сохранение настроек
		 * @throws coreException
		 */
		public function config() {
			$domains = domainsCollection::getInstance()->getList();
			$lang_id = cmsController::getInstance()->getCurrentLang()->getId();

			$mode = (string) getRequest('param0');
			$result = Array();

			/**
			 * @var domain|umiEntinty $domain
			 */
			foreach($domains as $domain) {
				$host = $domain->getHost();
				$domain_id = $domain->getId();

				$result[$host] = Array();

				$templates = templatesCollection::getInstance()->getTemplatesList($domain_id, $lang_id);
				foreach ($templates as $template) {
					$result[$host][] = $template;
				}
			}

			if ($mode == "do") {
				$this->saveEditedList("templates", $result);
				$this->chooseRedirect();
			}


			$this->setDataType("list");
			$this->setActionType("modify");

			$data = $this->prepareData($result, "templates");

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Удаляет страницу
		 * @throws coreException
		 * @throws expectElementException
		 * @throws wrongElementTypeAdminException
		 */
		public function del() {
			$element = $this->expectElement('param0');

			$params = Array(
				"element" => $element,
				"allowed-element-types" => Array('page', '')
			);

			$this->deleteElement($params);
			$this->chooseRedirect();
		}

		/**
		 * Возвращает форму редактирования шаблона.
		 * Если передан ключевой параметр $_REQUEST['param1'] = do,
		 * то метод запустит сохранение шаблона
		 * @throws coreException
		 */
		public function tpl_edit() {
			$tpl_id = (int) getRequest('param0');
			$template = templatesCollection::getInstance()->getTemplate($tpl_id);

			$mode = (string) getRequest('param1');

			if ($mode == "do") {
				$this->saveEditedTemplateData($template);
				$this->chooseRedirect();
			}

			$this->setDataType('form');
			$this->setActionType('modify');

			$data = $this->prepareData($template, 'template');

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Валидирует текущего пользователя и отключает блокировку у страницы
		 * @throws publicAdminException
		 */
		public function unlock_page() {
			$pageId = getRequest("param0");

			if (permissionsCollection::getInstance()->isSv()) {
				throw new publicAdminException(getLabel('error-can-unlock-not-sv'));
			}

			$this->module->unlockPage($pageId);
		}

		/**
		 * Возвращает настройки управления контентом.
		 * Если передан ключевой параметр $_REQUEST['param0'] = do,
		 * то метод запустит сохранение настроек.
		 * @throws coreException
		 */
		public function content_control() {
			$mode = getRequest("param0");
			$regedit = regedit::getInstance();

			$params = array (
				"content_config" => array (
					'bool:lock_pages' => false,
					'int:lock_duration' => 0,
					'bool:expiration_control' => false
				),
				'output_options' => array (
					'int:elements_count_per_page' => null
				)
			);

			if ($mode == "do") {
				$params = $this->expectParams($params);
				$regedit->setVar("//settings/lock_pages", $params['content_config']['bool:lock_pages']);
				$regedit->setVar("//settings/lock_duration", $params['content_config']['int:lock_duration']);
				$regedit->setVar("//settings/expiration_control", $params['content_config']['bool:expiration_control']);
				$regedit->setVar("//settings/elements_count_per_page", $params['output_options']['int:elements_count_per_page']);

				$this->switchGroupsActivity('svojstva_publikacii', (bool) $params['content_config']['bool:expiration_control']);

				$this->chooseRedirect();
			}

			$params['content_config']['bool:lock_pages'] = $regedit->getVal("//settings/lock_pages");
			$params['content_config']['int:lock_duration'] = $regedit->getVal("//settings/lock_duration");
			$params['content_config']['bool:expiration_control'] = $regedit->getVal("//settings/expiration_control");
			$params['output_options']['int:elements_count_per_page'] = $regedit->getVal("//settings/elements_count_per_page");

			$this->setDataType("settings");
			$this->setActionType("modify");

			$data = $this->prepareData($params, "settings");
			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает настройки табличного контрола
		 * @param string $param контрольный параметр
		 * @return array
		 */
		public function getDatasetConfiguration($param = '') {
			$loadMethod = 'load_tree_node';
			$deleteMethod = 'tree_delete_element';
			$activityMethod = 'tree_set_activity';

			$types = array();
			if ($param == 'tree') {
				$types = array(
					'types' => array(
						array(
							'common' => 'true',
							'id' => 'page'
						)
					)
				);
				$loadMethod = $param;
			}

			$result = array(
				'methods' => array(
					array(
						'title' => getLabel('smc-load'),
						'forload' => true,
						'module' => 'content',
						'#__name' => $loadMethod
					),
					array(
						'title' => getLabel('smc-delete'),
						'module' => 'content',
						'#__name' => $deleteMethod,
						'aliases' => 'tree_delete_element,delete,del'
					),
					array(
						'title' => getLabel('smc-activity'),
						'module' => 'content',
						'#__name' => $activityMethod,
						'aliases' => 'tree_set_activity,activity'
					),
					array(
						'title' => getLabel('smc-copy'),
						'module' => 'content',
						'#__name' => 'tree_copy_element'
					),
					array(
						'title' => getLabel('smc-move'),
						'module' => 'content',
						'#__name' => 'move'
					),
					array(
						'title' => getLabel('smc-change-template'),
						'module' => 'content',
						'#__name' => 'change_template'
					),
					array(
						'title' => getLabel('smc-change-lang'),
						'module' => 'content',
						'#__name' => 'move_to_lang'
					),
					array(
						'title' => getLabel('smc-change-lang'),
						'module' => 'content',
						'#__name' => 'copy_to_lang_old'
					)
				),
				'default' => 'name[400px]'
			);

			if (!empty($types)) {
				$result += $types;
			}

			return $result;
		}

		/**
		 * Возвращает ссылку на редактирование объекта
		 * @param $objectId
		 * @param bool $type
		 * @return bool
		 */
		public function getObjectEditLink($objectId, $type = false) {
			return false;
		}

		/**
		 * Возвращает данные для включения быстрого
		 * редактирования поля в табличном контроле
		 * @throws coreException
		 * @throws publicAdminException
		 */
		public function get_editable_region() {
			$itemId = getRequest('param0');
			$propName = getRequest('param1');
			$isObject = (bool) getRequest('is_object');

			$objects = umiObjectsCollection::getInstance();
			$hierarchy = umiHierarchy::getInstance();
			$oEntity = ($isObject) ? $objects->getObject($itemId) : $hierarchy->getElement($itemId);

			// Checking rights
			$bDisallowed = false;
			$mainConfiguration = mainConfiguration::getInstance();
			$objectEditionAllowed = (bool) $mainConfiguration->get('system', 'allow-object-editing');
			$permissions = permissionsCollection::getInstance();
			$userId = $permissions->getUserId();
			$groupIds = $objects->getObject($userId)->getValue('groups');
			$svGroupId = $objects->getObjectIdByGUID('users-users-15');
			$svId = $objects->getObjectIdByGUID('system-supervisor');

			if ($userId != $svId && !in_array($svGroupId, $groupIds)) {
				if ($isObject) {
					$bDisallowed = !($oEntity->getOwnerId() == $userId);
					if ($bDisallowed) {
						$module = $oEntity->getModule();
						$method = $oEntity->getMethod();
						switch (true) {
							case ($module && $method): {
								$bDisallowed = !$permissions->isAllowedMethod($userId, $module, $method);
								break;
							}
							case $objectEditionAllowed: {
								$bDisallowed = false;
								break;
							}
							default: {
								throw new publicAdminException(getLabel('error-no-permissions'));
							}
						}
					}
				} else {
					list ($r, $w) = $permissions->isAllowedObject($userId, $itemId);
					if (!$w) $bDisallowed = true;
				}
			}

			if ($bDisallowed) {
				throw new publicAdminException(getLabel('error-no-permissions'));
			}

			$result = false;
			if ($oEntity) {
				switch($propName) {
					case "name":
						$result = array('name' => $oEntity->name);
						break;

					default:
						$oObject = (!$isObject)? $oEntity->getObject() : $oEntity;
						$prop = $oObject->getPropByName($propName);
						if (!$prop instanceof umiObjectProperty) {
							throw new publicAdminException(getLabel('error-property-not-exists'));
						}
						$result = array('property' => $prop);
						translatorWrapper::get($oObject->getPropByName($propName));
						umiObjectPropertyWrapper::$showEmptyFields = true;
				}
			}

			if (!is_array($result)) {
				throw new publicAdminException(getLabel('error-entity-not-exists'));
			}

			$this->setData($result);
			$this->doData();
		}

		/**
		 * Сохраняет значения поля,
		 * используется в быстром редактирования полей
		 * в табличном контроле
		 * @throws coreException
		 * @throws publicAdminException
		 */
		public function save_editable_region() {
			$iEntityId = getRequest('param0');
			$sPropName = getRequest('param1');
			$content = getRequest('data');
			$bIsObject = (bool) getRequest('is_object');

			if (is_array($content) && count($content) == 1) {
				$content = $content[0];
			} else if(is_array($content) && isset($content[0])) {
				$temp = array();
				foreach ($content as $item) {
					$temp[] = is_array($item) ? $item[0] : $item;
				}
				$content = $temp;
			}

			$oEntity = ($bIsObject) ? umiObjectsCollection::getInstance()->getObject($iEntityId) : umiHierarchy::getInstance()->getElement($iEntityId);

			// Checking rights
			$bDisallowed = false;
			$mainConfiguration = mainConfiguration::getInstance();
			$objectEditionAllowed = (bool) $mainConfiguration->get('system', 'allow-object-editing');
			$permissions = permissionsCollection::getInstance();
			$userId = $permissions->getUserId();

			if (!$permissions->isSv($userId)) {
				if($bIsObject) {
					$bDisallowed = !($oEntity->getOwnerId() == $userId);
					if($bDisallowed) {
						//Check module permissions
						$module = $oEntity->getModule();
						$method = $oEntity->getMethod();

						switch (true) {
							case ($module && $method): {
								$bDisallowed = !$permissions->isAllowedMethod($userId, $module, $method);
								break;
							}
							case $objectEditionAllowed: {
								$bDisallowed = false;
								break;
							}
							default: {
								throw new publicAdminException(getLabel('error-no-permissions'));
							}
						}
					}
				} else {
					list($r, $w) = $permissions->isAllowedObject($userId, $iEntityId);
					if(!$w) $bDisallowed = true;
				}
			}

			if ($bDisallowed) {
				throw new publicAdminException(getLabel('error-no-permissions'));
			}

			$event = new umiEventPoint("systemModifyPropertyValue");
			$event->addRef("entity", $oEntity);
			$event->setParam("property", $sPropName);
			$event->addRef("newValue", $content);
			$event->setMode("before");

			try {
				$event->call();
			} catch (wrongValueException $e) {
				throw new publicAdminException($e->getMessage());
			}

			/**
			 * @var iUmiEntinty|umiHierarchyElement|iUmiObject $oEntity
			 */
			if ($oEntity instanceof iUmiHierarchyElement) {
				$backupModel = backupModel::getInstance();
				$backupModel->addLogMessage($oEntity->getId());
			}

			if ($bIsObject && !$this->module->checkAllowedColumn($oEntity, $sPropName)) {
				throw new publicAdminException(getLabel('error-no-permissions'));
			}

			if ($bIsObject && $sPropName == 'is_activated') {

				$guestId = $permissions->getGuestId();

				if ($iEntityId == SV_USER_ID) {
					throw new publicAdminException(getLabel('error-users-swtich-activity-sv'));
				}

				if ($iEntityId == $guestId) {
					throw new publicAdminException(getLabel('error-users-swtich-activity-guest'));
				}

				if ($iEntityId == $userId) {
					throw new publicAdminException(getLabel('error-users-swtich-activity-self'));
				}
			}

			$sPropValue = "";
			if ($oEntity) {
				$bOldVal = umiObjectProperty::$IGNORE_FILTER_INPUT_STRING;
				umiObjectProperty::$IGNORE_FILTER_INPUT_STRING = true;
				/**
				 * @var iUmiEntinty|umiObject $oObject
				 */
				$oObject = (!$bIsObject)? $oEntity->getObject() : $oEntity;
				$oldValue = null;

				try {
					if($sPropName == 'name') {
						if (is_string($content) && strlen($content)) {
							$oldValue = $oEntity->name;
							$oEntity->name = $content;
							if ($oEntity instanceof iUmiHierarchyElement) {
								$oEntity->h1 = $content;
							}
						}
						$result = array('name' => $content);
					} else {
						/**
						 * @var iUmiObjectProperty $property
						 */
						$property = $oObject->getPropByName($sPropName);
						if ($property->getDataType() == 'date') {
							$date = new umiDate();
							$date->setDateByString($content);
							$content = $date;
						}

						$oldValue = $oObject->getValue($sPropName);
						$oObject->setValue($sPropName, $content);

						if ($oObject->getIsUpdated() && $oObject->getId() != $oEntity->getId()) {
							$oEntity->setIsUpdated(true, true);
						}

						if ($oEntity instanceof iUmiHierarchyElement && $sPropName == 'h1') {
							$oEntity->name = $content;
						}
						$result = array('property' => $property);

						translatorWrapper::get($property);
						umiObjectPropertyWrapper::$showEmptyFields = true;
					}
				} catch (fieldRestrictionException $e) {
					throw new publicAdminException($e->getMessage());
				}
				$oEntity->commit();
				umiObjectProperty::$IGNORE_FILTER_INPUT_STRING = $bOldVal;

				$oObject->update();
				$oEntity->update();

				if ($oEntity instanceof umiEntinty) {
					$oEntity->commit();
				}

				$event->setParam("oldValue", $oldValue);
				$event->setParam("newValue", $content);
				$event->setMode("after");
				$event->call();

				$this->setData($result);
				$this->doData();
			}
		}

		/**
		 * Возвращает ветвь контрола типа "Дерево" или "Таблица"
		 * @throws coreException
		 */
		public function load_tree_node() {
			$this->setDataType("list");
			$this->setActionType("view");

			$limit = getRequest('per_page_limit');
			$curr_page = getRequest('p');
			$offset = $curr_page * $limit;

			list($rel) = getRequest('rel');
			$sel = new selector('pages');
			if ($rel !== 0) {
				$sel->limit($offset, $limit);
			}
			selectorHelper::detectFilters($sel);

			$result = $sel->result;
			$length = $sel->length;
			$templatesData = getRequest('templates');

			if ($templatesData) {
				$templatesList = explode(',', $templatesData);
				$result = $this->module->getPagesByTemplatesIdList($templatesList, $limit, $offset);
				$length = $this->module->getTotalPagesByTemplates($templatesList);
			}

			$data = $this->prepareData($result, "pages");
			$this->setData($data, $length);
			$this->setDataRange($limit, $offset);

			if ($rel != 0) {
				$this->setDataRangeByPerPage($limit, $curr_page);
			}

			$this->doData();
		}

		/**
		 * Переключает активность у страниц
		 * @throws coreException
		 * @throws expectElementException
		 * @throws publicAdminException
		 * @throws requreMoreAdminPermissionsException
		 * @throws wrongElementTypeAdminException
		 */
		public function tree_set_activity() {
			$elements = getRequest('element');
			if (!is_array($elements)) {
				$elements = Array($elements);
			}

			$active = getRequest('active');

			if (!is_null($active)) {
				foreach ($elements as $elementId) {
					$element = $this->expectElement($elementId, false, true);

					if ($element instanceof umiHierarchyElement) {
						$active = intval($active) > 0 ? true : false;

						$params = Array(
							"element" => $element,
							"activity" => $active
						);

						$oEventPoint = new umiEventPoint("systemSwitchElementActivity");
						$oEventPoint->setMode("before");

						$oEventPoint->addRef("element", $element);
						$this->module->setEventPoint($oEventPoint);

						$this->switchActivity($params);

						// after del event
						$oEventPoint->setMode("after");
						$this->module->setEventPoint($oEventPoint);

					} else {
						throw new publicAdminException(getLabel('error-expect-element'));
					}
				}

				$this->setDataType("list");
				$this->setActionType("view");
				$data = $this->prepareData($elements, "pages");
				$this->setData($data);
				$this->doData();
			} else {
				throw new publicAdminException(getLabel('error-expect-action'));
			}
		}

		/**
		 * Перемещает страницу и|или объект в административной панели
		 * @throws expectElementException
		 * @throws expectObjectException
		 * @throws publicAdminException
		 */
		public function move() {
			$element =  $this->expectElement("element");
			$elementParent =  $this->expectElement("rel");

			if ($element instanceof iUmiHierarchyElement && ($elementParent instanceof iUmiHierarchyElement || getRequest("rel") == 0)) {
				return $this->tree_move_element();
			}

			$object = $this->expectObject("element");
			$objectParent =  $this->expectObject("rel");

			if ($object instanceof iUmiObject && $objectParent instanceof iUmiObject) {
				return $this->table_move_object($object, $objectParent);
			}

			if (($element instanceof iUmiHierarchyElement || $object instanceof iUmiObject) && ($elementParent instanceof iUmiHierarchyElement || $objectParent instanceof iUmiObject)) {
				return $this->table_mixed_move();
			}

			$this->setDataType("list");
			$this->setActionType("view");

			$this->setData(array());
			return $this->doData();
		}

		/**
		 * Метод-заглушка для смешанного перемещения страниц и объектов
		 */
		public function table_mixed_move() {
			$this->setDataType("list");
			$this->setActionType("view");
			$this->setData(array('node' => 'mixed'));
			$this->doData();
		}

		/**
		 * Перемещает объект
		 * @param iUmiObject $object объект который перемещают
		 * @param iUmiObject $objectParent объект в который перемещают
		 */
		public function table_move_object(iUmiObject $object, iUmiObject $objectParent) {
			$this->setDataType("list");
			$this->setActionType("view");

			$moveMode = getRequest('moveMode');

			$umiObjects = umiObjectsCollection::getInstance();
			$orderChanged = $umiObjects->changeOrder($objectParent, $object, $moveMode);

			if ($orderChanged) {
				$this->setDataRange(2, 0);
				$data = $this->prepareData(array($object, $objectParent), 'objects');
				$this->setData($data, 2);
			} else {
				$this->setDataRange(0, 0);
				$data = $this->prepareData(array(), 'objects');
				$this->setData($data, 0);
			}

			$this->doData();
		}

		/**
		 * Перемещает выбранные страницы
		 */
		public function tree_move_element() {
			$selectedItems = getRequest('selected_list');
			$newParentId = (int) getRequest("rel");
			$domain = getRequest('domain');
			$asSibling = (int) getRequest('as-sibling');
			$beforeId = getRequest('before');

			$umiHierarchy = umiHierarchy::getInstance();
			$newParentParentsIds = $umiHierarchy->getAllParents($newParentId);
			$page = null;
			$movedPages = [];

			if (count($selectedItems) == 0 && isset($_REQUEST['element']) && getRequest('return_copies')) {
				$selectedItems[] = getRequest('element');
			}

			foreach ($selectedItems as $pageId) {
				if (in_array($pageId, $newParentParentsIds)) {
					continue;
				}

				/**
				 * @var iUmiHierarchyElement|iUmiEntinty $page
				 */
				$page = $this->expectElement($pageId, false, true);

				if (!$page instanceof iUmiHierarchyElement) {
					throw new publicAdminException(getLabel('error-expect-element'));
				}

				$oldParentId = $page->getParentId();

				if ($newParentId == $oldParentId || $pageId == $newParentId) {
					continue;
				}

				$movingParams = [
					'element'	=> $page,
					'parent-id'	=> $newParentId,
					'domain'	=> $domain,
					'as-sibling'=> $asSibling,
					'before-id'	=> $beforeId
				];

				if ($this->moveElement($movingParams)) {
					$movedPages[] = $page->getId();
				}
			}

			if (getRequest('return_copies')) {
				$this->setDataType("form");
				$this->setActionType("modify");
				$data = $this->prepareData(['element' => $page], "page");
			} else {
				$this->setDataType("list");
				$this->setActionType("view");
				$data = $this->prepareData($movedPages, "pages");
			}

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Удаляет страницы в корзину
		 * @throws coreException
		 * @throws expectElementException
		 * @throws publicAdminException
		 * @throws wrongElementTypeAdminException
		 */
		public function tree_delete_element() {
			$elements = getRequest('element');
			if (!is_array($elements)) {
				$elements = Array($elements);
			}

			$parentIds = Array();

			foreach ($elements as $elementId) {
				$element = $this->expectElement($elementId, false, true, true);

				if ($element instanceof umiHierarchyElement) {
					// before del event
					$element_id = $element->getId();
					$parentIds[] = $element->getParentId();
					$oEventPoint = new umiEventPoint("content_del_element");
					$oEventPoint->setMode("before");
					$oEventPoint->setParam("element_id", $element_id);
					$this->module->setEventPoint($oEventPoint);

					// try delete
					$params = Array(
						"element" => $element
					);

					$this->deleteElement($params);

					// after del event
					$oEventPoint->setMode("after");
					$this->module->setEventPoint($oEventPoint);
				} else {
					throw new publicAdminException(getLabel('error-expect-element'));
				}
			}

			$parentIds = array_unique($parentIds);

			// retrun parent element for update
			$this->setDataType("list");
			$this->setActionType("view");
			$data = $this->prepareData($parentIds, "pages");

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Копирует страницу
		 * @throws Exception
		 * @throws coreException
		 * @throws expectElementException
		 * @throws publicAdminException
		 */
		public function tree_copy_element() {
			$element =  $this->expectElement('element');
			$cloneMode = (bool) getRequest('clone_mode');
			$copyAll = (bool) getRequest('copy_all');
			$parentId = (int) getRequest('rel');
			$connection = ConnectionPool::getInstance()->getConnection();
			$new_element_id = false;

			if ($element instanceof umiHierarchyElement) {
				$element_id = $element->getId();
				if (!($parentId && umiHierarchy::getInstance()->isExists($parentId))) {
					$parentId = umiHierarchy::getInstance()->getParent($element_id);
				}

				$connection->query("START TRANSACTION");

				if ($cloneMode) {
					// create real copy
					$clone_allowed = true;

					if ($clone_allowed) {
						$event = new umiEventPoint("systemCloneElement");
						$event->addRef("element", $element);
						$event->setParam("elementId", $element_id);
						$event->setParam("parentId", $parentId);
						$event->setMode("before");
						$event->call();

						$new_element_id = umiHierarchy::getInstance()->cloneElement($element_id, $parentId, $copyAll);

						$event->setParam("newElementId", $new_element_id);
						$event->setMode("after");
						$event->call();

						$new_element = umiHierarchy::getInstance()->getElement((int) $new_element_id, false, false);

						$event = new umiEventPoint("systemCreateElementAfter");
						$event->addRef("element", $new_element);
						$event->setParam("elementId", $new_element_id);
						$event->setParam("parentId", $parentId);
						$event->setMode("after");
						$event->call();

					}
				} else {
					// create virtual copy
					$event = new umiEventPoint("systemVirtualCopyElement");
					$event->setParam("elementId", $element_id);
					$event->setParam("parentId", $parentId);
					$event->addRef("element", $element);
					$event->setMode("before");
					$event->call();

					$new_element_id = umiHierarchy::getInstance()->copyElement($element_id, $parentId, $copyAll);

					$event->setParam("newElementId", $new_element_id);
					$event->setMode("after");
					$event->call();

					$new_element = umiHierarchy::getInstance()->getElement((int) $new_element_id, false, false);

					$event = new umiEventPoint("systemCreateElementAfter");
					$event->addRef("element", $new_element);
					$event->setParam("elementId", $new_element_id);
					$event->setParam("parentId", $parentId);
					$event->setMode("after");
					$event->call();
				}

				if ($new_element_id) {
					if ((bool) getRequest('return_copies')) {
						$this->setDataType("form");
						$this->setActionType("modify");
						$data = $this->prepareData(array('element' => $new_element), "page");
						$this->setData($data);
					} else {
						$this->setDataType("list");
						$this->setActionType("view");
						$data = $this->prepareData(array($new_element_id), "pages");
						$this->setData($data);
					}

					$connection->query("COMMIT");
					$this->doData();
				} else {
					throw new publicAdminException(getLabel('error-copy-element'));
				}

			} else {
				throw new publicAdminException(getLabel('error-expect-element'));
			}
		}

		/**
		 * Копирует страницу в другую языковую версию и/или другой домен
		 * @throws coreException
		 * @throws expectElementException
		 */
		public function copy_to_lang_old() {
			$langId = (int) getRequest('lang-id');
			$domainId = (int) getRequest('domain-id');
			$alias_new = (array) getRequest('alias');
			$move_old = (array) getRequest('move');
			$force = (int) getRequest('force');
			$mode = (string) getRequest('mode');

			$elements = getRequest('element');
			if (!is_array($elements)) {
				$elements = Array($elements);
			}

			foreach ($alias_new as $k=>$v) {
				$alias_new[$k] = umiHierarchy::convertAltName($v);
			}

			if (!is_null($langId)) {
				$hierarchy = umiHierarchy::getInstance();

				if (!$force) {
					$aliases_old = array();

					foreach ($elements as $elementId) {

						if (!empty($move_old[$elementId])) {
							continue;
						}

						$element = $this->expectElement($elementId, false, true);
						$alt_name = $element->getAltName();

						if (!empty($alias_new[$element->getId()])) {
							$alt_name = $alias_new[$element->getId()];
						}

						$errors = array();
						$element_dst =  umiHierarchy::getInstance()->getIdByPath( $alt_name , false, $errors, $domainId ,$langId);
						$element_dst = $this->expectElement($element_dst, false, true);

						if ($element_dst && $element_dst->getAltName() == $alt_name) {
							$alt_name_normal = $hierarchy->getRightAltName($alt_name, $element_dst, false, true);
							$aliases_old[$element->getId()] = array($alt_name, $alt_name_normal);
						}
					}

					if (count($aliases_old) ) {
						$this->setDataType("list");
						$this->setActionType("view");
						$data = array('error'=>array());

						$data['error']['nodes:item'] = array();
						$data['error']['type'] = '__alias__';

						$path = getSelectedServerProtocol() . "://".domainsCollection::getInstance()->getDomain($domainId)->getHost() ."/";

						if (!langsCollection::getInstance()->getLang($langId)->getIsDefault()) {
							$path .= langsCollection::getInstance()->getLang($langId)->getPrefix() . '/';
						}

						foreach ($aliases_old as $k=>$v) {
							$data['error']['nodes:item'][] = array('attribute:id'=>$k, 'attribute:path'=>$path , 'attribute:alias'=>$v[0], 'attribute:alt_name_normal'=>$v[1]);
						}

						$this->setData($data);
						$this->doData();
						return;
					}
				}

				$templatesCollection = templatescollection::getInstance();

				$templates = $templatesCollection->getTemplatesList($domainId, $langId);

				$template_error = false;
				if (empty($templates)) {
					$template_error = true;
				}

				if ($template_error) {
					$this->setDataType("list");
					$this->setActionType("view");
					$data = $this->prepareData(array(), "pages");

					$dstLang = langsCollection::getInstance()->getLang($langId);
					$lang = '';
					if (!$dstLang->getIsDefault()) {
						$lang .= $dstLang->getPrefix() . '/';
					}

					$data['error'] = array();
					$data['error']['type'] = "__template_not_exists__";
					$data['error']['text'] = sprintf(getLabel('error-no-template-in-domain'), $lang);
					$this->setData($data);
					$this->doData();
				}

				$template_def = $templatesCollection->getDefaultTemplate($domainId, $langId);;

				foreach ($elements as $elementId) {
					$element = $this->expectElement($elementId, false, true);
					$element_template = $templatesCollection->getTemplate($element->getTplId());

					$template_has = false;
					/**
					 * @var template $v
					 */
					foreach ($templates as $v) {
						if ($v->getFilename() == $element_template->getFilename()) {
							$template_has = $v;
						}
					}

					if (!$template_has)
						$template_has = $template_def;

					if (!$template_has)
						$template_has = reset($templates);

					if ($mode == 'move') {
						$copyElement = $element;
						$copyElementId = $element->getId();
					} else {
						$copyElementId = $hierarchy->cloneElement($element->getId(), 0, true, false);
						$copyElement = $hierarchy->getElement($copyElementId);
					}

					if ($copyElement instanceof umiHierarchyElement) {
						$alt_name = $element->getAltName();

						if (!empty($alias_new[$element->getId()])) {
							$alt_name = $alias_new[$element->getId()];
						}

						if (!empty($move_old[$element->getId()])) {
							$element_dst =  umiHierarchy::getInstance()->getIdByPath($alt_name , false, $errors,$domainId , $langId);
							$element_dst = $this->expectElement($element_dst, false, true);

							if($element_dst && $element_dst->getAltName() == $alt_name) {
								$hierarchy->delElement($element_dst->getId());
							}
						}

						$copyElement->setLangId($langId);

						if ($domainId) {
							$copyElement->setDomainId($domainId);
						}

						$copyElement->setAltName( $alt_name );

						if ($template_has) {
							$copyElement->setTplId($template_has->getId());
						}

						$copyElement->commit();

						$childs = $hierarchy->getChildrenTree($copyElementId);
						$this->module->changeChildsLang($childs, $langId, $domainId);
					}
				}
			}

			$this->setDataType("list");
			$this->setActionType("view");
			$data = $this->prepareData(array(), "pages");

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Копирует страницы в другю языковую версию
		 * @throws coreException
		 * @throws expectElementException
		 */
		public function copy_to_lang() {
			$langId = (int) getRequest('lang-id');
			$elements = getRequest('element');
			if (!is_array($elements)) {
				$elements = Array($elements);
			}

			if (!is_null($langId)) {
				$hierarchy = umiHierarchy::getInstance();

				foreach ($elements as $elementId) {
					$element = $this->expectElement($elementId, false, true);
					if ($element->getLangId() != $langId || true) {
						$copyElementId = $hierarchy->cloneElement($element->getId(), 0, true);
						$copyElement = $hierarchy->getElement($copyElementId);
						if ($copyElement instanceof umiHierarchyElement) {
							$copyElement->setLangId($langId);
							$copyElement->commit();

							$childs = $hierarchy->getChildrenTree($copyElementId);
							$this->module->changeChildsLang($childs, $langId);
						}
					}
				}
			}

			$this->setDataType("list");
			$this->setActionType("view");
			$data = $this->prepareData(array(), "pages");
			$this->setData($data);
			$this->doData();
		}

		/**
		 * Алиас для copy_to_lang()
		 */
		public function move_to_lang() {
			$_REQUEST['mode'] = 'move';
			return $this->copy_to_lang();
		}

		/**
		 * Устанавливает шаблону флаг "основной"
		 * @param int $templateId ид шаблона, который требуется изменить
		 * @param bool|int $domainId ид домена, к которому относится шаблон. Если не передать - возьмет текущий.
		 * @param bool|int $languageId ид языка, к которому относится шаблон. Если не передать - возьмет текущий.
		 * @return true
		 * @throws publicAdminException если $templateId не является числом
		 * @throws publicAdminException если не удалось получить шаблон по id
		 * @throws publicAdminException если не удалось получить текущий домен
		 * @throws publicAdminException если не удалось получить домен по id
		 * @throws publicAdminException если не удалось получить текущий язык
		 * @throws publicAdminException если не удалось получить язык по id
		 * @throws publicAdminException если не удалось сделать шаблон основным
		 */
		public function setBaseTemplate($templateId = null, $domainId = false, $languageId = false) {
			$templateId = (is_null($templateId)) ? getRequest('param0') : $templateId;

			if (!is_numeric($templateId)) {
				throw new publicAdminException(__METHOD__ . ': wrong template id given: ' . $templateId);
			}

			$templateCollection = templatesCollection::getInstance();
			$template = $templateCollection->getTemplate($templateId);

			if (!$template instanceof template) {
				throw new publicAdminException(__METHOD__ . ': template with id = ' . $templateId . ' was not found');
			}

			$cmsController = cmsController::getInstance();
			$domainId = (is_bool($domainId)) ? getRequest('param1') : $domainId;

			if (!is_numeric($domainId)) {
				$currentDomain = $cmsController->getCurrentDomain();

				if (!$currentDomain instanceof domain) {
					throw new publicAdminException(__METHOD__ . ':  cant get current domain');
				}

				$domainId = $currentDomain->getId();
			}

			$domainsCollection = domainsCollection::getInstance();
			$domain = $domainsCollection->getDomain($domainId);

			if (!$domain instanceof domain) {
				throw new publicAdminException(__METHOD__ . ':  cant get domain by id: ' . $domainId);
			}

			$languageId = (is_bool($languageId)) ? getRequest('param2') : $languageId;

			if (!is_numeric($languageId)) {
				$currentLang = $cmsController->getCurrentLang();

				if (!$currentLang instanceof lang) {
					throw new publicAdminException(__METHOD__ . ':  cant get current language');
				}

				$languageId = $currentLang->getId();
			}

			$languagesCollection = langsCollection::getInstance();
			$language = $languagesCollection->getLang($languageId);

			if (!$language instanceof lang) {
				throw new publicAdminException(__METHOD__ . ':  cant get language by id: ' . $languageId);
			}

			$baseTemplateChanged = $templateCollection->setDefaultTemplate($templateId, $domainId, $languageId);

			if (!$baseTemplateChanged) {
				throw new publicAdminException(__METHOD__ . ':  cant change base template');
			}

			return true;
		}
	}
?>