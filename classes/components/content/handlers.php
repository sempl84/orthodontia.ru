<?php
	/**
	 * Класс обработчиков событий
	 */
	class ContentHandlers {
		/**
		 * @var content $module
		 */
		public $module;

		/**
		 * Обработчик событий редактирования и перемещения страницы.
		 * Проверяет не изменился ли адрес страницы, если изменился
		 * - добавляет редирект со старого адрес на новый
		 * @param umiEventPoint $e
		 * @return bool
		 */
		public function onModifyPageWatchRedirects(umiEventPoint $e) {
			static $links = array();
			$serviceContainer = umiServiceContainers::getContainer();
			/**
			 * @var umiRedirectsCollection $redirects
			 */
			$redirects = $serviceContainer->get('redirects');
			$hierarchy = umiHierarchy::getInstance();

			$element = $e->getRef('element');
			/**
			 * @var umiHierarchyElement|iUmiEntinty $element
			 */
			if ($element instanceof umiHierarchyElement == false) {
				return false;
			}

			$elementId = $element->getId();
			$link = $hierarchy->getPathById($elementId, false, false, true);

			if ($e->getMode() == 'before') {
				$links[$elementId] = $link;
				return true;
			}

			if ($links[$elementId] != $link) {
				$redirects->add($links[$elementId], $link, 301);
			}

			return true;
		}

		/**
		 * Проверяет тестовое сообщение
		 * @param iUmiEventPoint $event
		 */
		public function testMessages(iUmiEventPoint $event) {
			$userId = $event->getParam('user_id');
			$umiPermissions = permissionsCollection::getInstance();

			if (!$umiPermissions->isAdmin($userId)) {
				return;
			}

			$umiRegistry = regedit::getInstance();
			$lastTestTime = (int) $umiRegistry->getVal('//settings/last_mess_time');

			if (time() < $lastTestTime + 2592000) {
				return;
			}

			$umiMessages = umiMessages::getInstance();
			$umiMessages->testMessages();
		}

		/**
		 * Обработчик события начала редактирования страницы.
		 * Блокирует редактирования страницы для других пользователей
		 * @param iUmiEventPoint $eEvent события начала редактирования
		 */
		public function systemLockPage(iUmiEventPoint $eEvent){
			/**
			 * @var iUmiHierarchyElement|iUmiEntinty $ePage
			 */
			if ($ePage = $eEvent->getRef("element")){
				$userId = $eEvent->getParam("user_id");
				$lockTime = $eEvent->getParam("lock_time");
				/**
				 * @var iUmiObject|iUmiEntinty $oPage
				 */
				$oPage = &$ePage->getObject();
				$oPage->setValue("locktime", $lockTime);
				$oPage->setValue("lockuser", $userId);
				$oPage->commit();
			}
		}

		/**
		 * Обработчик события сохранения отредактированной страницы
		 * Снимает блокировку редактирования страницы для других пользователей
		 * @param iUmiEventPoint $eEvent событие сохранения
		 */
		public function systemUnlockPage(iUmiEventPoint $eEvent){
			/**
			 * @var iUmiHierarchyElement|iUmiEntinty $ePage
			 */
			if ($ePage = $eEvent->getRef("element")){
				/**
				 * @var iUmiObject|iUmiEntinty $oPage
				 */
				$oPage = $ePage->getObject();
				$oPage->setValue("locktime", null);
				$oPage->setValue("lockuser", null);
				$oPage->commit();
			}
		}

		/**
		 * Обработчик события срабатывания системного крона.
		 * Получает список страниц, которые скоро будут отключены
		 * по истечению времени актуальности контента.
		 * Устанавливает им статус предварительного отключения.
		 * Уведомляет их авторов об этом.
		 * @param iUmiEventPoint $oEvent событие срабатывания крона
		 */
		public function cronSendNotification(iUmiEventPoint $oEvent){
			$object_type = umiObjectTypesCollection::getInstance()->getTypeByGUID('root-pages-type');
			$field_id = $object_type->getFieldId("notification_date");
			$field_id_expiration = $object_type->getFieldId("expiration_date");
			$sel = new umiSelection();
			$sel->addPropertyFilterLess($field_id, time());
			$sel->addPropertyFilterMore($field_id_expiration, time());
			$sel->addPropertyFilterNotEqual($field_id, 0);
			$sel->addActiveFilter(true);
			$sel->forceHierarchyTable(true);
			$result = umiSelectionsParser::runSelection($sel);

			$umiHierarchy = umiHierarchy::getInstance();
			$umiRegistry = regedit::getInstance();
			$umiObjectsCollection = umiObjectsCollection::getInstance();
			$lastCheckDate = (int) $umiRegistry->getVal("//modules/content/last-notification-date");
			$domainsCollection = domainsCollection::getInstance();

			foreach ($result as $key => $page_id){
				$ePage = $umiHierarchy->getElement($page_id, true, true);

				if ($ePage instanceof umiHierarchyElement == false) {
					continue;
				}

				$notificationDateObject = $ePage->getValue('notification_date');

				if (!$notificationDateObject instanceof umiDate) {
					continue;
				}

				$notificationDate = $notificationDateObject->getDateTimeStamp();
				if ($lastCheckDate && ($lastCheckDate - (3600 * 24) < time()) && ($lastCheckDate > $notificationDate)) {
					continue;
				}

				$oPage = $ePage->getObject();
				$ePage->setValue("publish_status", $this->module->getPageStatusIdByFieldGUID("page_status_preunpublish"));
				$ePage->commit();

				if (!$publishComments = $ePage->getValue("publish_comments")) {
					$publishComments = getLabel('no-publish-comments');
				}

				$user_id = $oPage->getOwnerId();
				$oUser = $umiObjectsCollection->getObject($user_id);

				if (!$oUser instanceof umiObject) {
					continue;
				}

				$user_email = $oUser->getValue("e-mail");

				if (!umiMail::checkEmail($user_email)) {
					continue;
				}

				$mail_message = new umiMail();
				$from = $umiRegistry->getVal("//settings/email_from");
				$mail_message->setFrom($from);
				$mail_message->setPriorityLevel("high");
				$mail_message->setSubject(getLabel('label-notification-mail-header'));

				list ($body) = content::loadTemplatesForMail("mail/notify", "body");

				$block['notify_header']	= getLabel('label-notification-mail-header');
				$block['page_header'] =  $ePage->getName();
				$block['publish_comments'] = $publishComments;
				$domain = $domainsCollection->getDomain($ePage->getDomainId());
				$page_host = getSelectedServerProtocol() . "://" . $domain->getHost() . $umiHierarchy->getPathById($page_id);
				$block['page_link'] = $page_host;

				$mail_html = content::parseTemplateForMail($body, $block, $page_id);

				$mail_message->addRecipient($user_email);
				$mail_message->setContent($mail_html);
				$mail_message->commit();
				$mail_message->send();
			}

			$umiRegistry->setVal("//modules/content/last-notification-date", time());
		}

		/**
		 * Обработчик события срабатывания системного крона.
		 * Снимает с публикации страницы, у которых истекло время актуальности
		 * и уведомляет их авторов об этом
		 * @param iUmiEventPoint $oEvent события срабатывания системного крона
		 */
		public function cronUnpublishPage(iUmiEventPoint $oEvent){
			$object_type = umiObjectTypesCollection::getInstance()->getTypeByGUID('root-pages-type');
			$field_id = $object_type->getFieldId("expiration_date");
			$sel = new umiSelection();
			$sel->addPropertyFilterLess($field_id, time());
			$sel->addPropertyFilterNotEqual($field_id, 0);
			$sel->addActiveFilter(true);
			$sel->forceHierarchyTable(true);
			$result = umiSelectionsParser::runSelection($sel);
			$umiHierarchy = umiHierarchy::getInstance();
			$umiObjectsCollection = umiObjectsCollection::getInstance();
			$umiRegistry = regedit::getInstance();
			$domainsCollection = domainsCollection::getInstance();

			foreach ($result as $key => $page_id){
				/**
				 * @var iUmiHierarchyElement|iUmiEntinty $ePage
				 */
				$ePage = $umiHierarchy->getElement($page_id, true);

				if (!$ePage instanceof iUmiHierarchyElement) {
					continue;
				}

				$ePage->setIsActive(false);
				$ePage->setValue("publish_status", $this->module->getPageStatusIdByFieldGUID("page_status_unpublish"));
				$ePage->commit();

				if (!$publishComments = $ePage->getValue("publish_comments")) {
					$publishComments = getLabel('no-publish-comments');
				}

				/**
				 * @var iUmiObject $pageObject
				 */
				$pageObject = $ePage->getObject();
				$user_id = $pageObject->getOwnerId();
				$oUser = $umiObjectsCollection->getObject($user_id);

				if (!$oUser instanceof umiObject) {
					continue;
				}

				$user_email = $oUser->getValue("e-mail");

				if (!umiMail::checkEmail($user_email)) {
					continue;
				}

				$mail_message = new umiMail();
				$from = $umiRegistry->getVal("//settings/email_from");
				$mail_message->setFrom($from);
				$mail_message->setPriorityLevel("high");
				$mail_message->setSubject(getLabel('label-notification-expired-mail-header'));

				list($body) = content::loadTemplatesForMail("mail/expired", "body");

				$block['notify_header']	= getLabel('label-notification-expired-mail-header');
				$block['page_header'] =  $ePage->getName();
				$block['publish_comments'] = $publishComments;
				$domain = $domainsCollection->getDomain($ePage->getDomainId());
				$page_host = getSelectedServerProtocol() . "://" . $domain->getHost() . $umiHierarchy->getPathById($page_id);
				$block['page_link'] = $page_host;

				$mail_html = content::parseTemplateForMail($body, $block, $page_id);

				$mail_message->addRecipient($user_email);
				$mail_message->setContent($mail_html);
				$mail_message->commit();
				$mail_message->send();
			}
		}

		/**
		 * Обработчик события сохранения изменений страницы.
		 * Запускает переключение активности страницы, в зависимости от актуальности контента
		 * @param iUmiEventPoint $event события сохранения изменений
		 */
		public function pageCheckExpiration(iUmiEventPoint $event) {
			if ($inputData = $event->getRef("inputData")) {
				$page = getArrayKey($inputData, "element");
				$this->module->saveExpiration($page);
			}
		}

		/**
		 * Обработчик события создания страницы.
		 * Запускает переключение активности страницы, в зависимости от актуальности контента
		 * @param iUmiEventPoint $event событие создания страницы
		 */
		public function pageCheckExpirationAdd(iUmiEventPoint $event) {
			if ($page = $event->getRef("element")) {
				$this->module->saveExpiration($page);
			}
		}

		/**
		 * Обработчик события сохранения изменений поля сущности.
		 * Проверяет сущность на предмет содержания спама
		 * @param iUmiEventPoint $event событие сохранения изменений поля сущности
		 */
		public function onModifyPropertyAntiSpam(iUmiEventPoint $event) {
			/**
			 * @var iUmiEntinty|iUmiHierarchyElement $entity
			 */
			$entity = $event->getRef("entity");
			if (($entity instanceof iUmiHierarchyElement) && ($event->getParam("property") == "is_spam")) {
				$type = umiHierarchyTypesCollection::getInstance()->getTypeByName("faq", "question");
				$contentField = ($type->getId() == $entity->getTypeId()) ? 'question' : 'content';
				antiSpamHelper::report($entity->getId(), $contentField);
			}
		}

		/**
		 * Обработчик события сохранения изменений страницы.
		 * Проверяет страницу на предмет содержания спама
		 * @param iUmiEventPoint $event событие сохранения изменений страницы
		 */
		public function onModifyElementAntiSpam(iUmiEventPoint $event) {
			static $cache = array();
			/**
			 * @var iUmiEntinty|iUmiHierarchyElement $element
			 */
			$element  = $event->getRef("element");

			if (!$element) {
				return;
			}

			if ($event->getMode() == "before") {
				$data = getRequest("data");
				if(isset($data[ $element->getId() ])) {
					$oldValue = getArrayKey($data[ $element->getId() ], 'is_spam');
					if($oldValue != $element->getValue("is_spam")) {
						$cache[ $element->getId() ] = true;
					}
				}
			} else if(isset($cache[ $element->getId() ])) {
				$type = umiHierarchyTypesCollection::getInstance()->getTypeByName("faq", "question");
				$contentField = ($type->getId() == $element->getTypeId()) ? 'question' : 'content';
				antiSpamHelper::report($element->getId(), $contentField);
			}
		}
	}
?>