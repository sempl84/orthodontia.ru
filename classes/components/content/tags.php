<?php
	/**
	 * Класс для получения списка тегов и страниц по тегам
	 */
	class ContentTags {
		/**
		 * @var content $module
		 */
		public $module;

		/**
		 * Возвращает страницы, которым назначен один из указанных тегов, ищет среди страниц всех доменов
		 * @param null $s_tags теги
		 * @param string $s_template имя шаблона для tpl шаблонизатора
		 * @param null $s_base_types список иерархических типов, среди которых искать страницу (пример: catalog.object+content.page)
		 * @param bool $i_per_page количество страниц к выводу
		 * @param bool $b_ignore_paging не учитывать пагинацию
		 * @return mixed
		 */
		public function pagesByAccountTags($s_tags = NULL, $s_template = "tags", $s_base_types = NULL, $i_per_page = false, $b_ignore_paging = false) {
			if (is_null($s_tags)) {
				$s_tags = getRequest('param0');
			}
			if (!$s_template) {
				$s_template = getRequest('param1');
			}
			if (is_null($s_base_types)) {
				$s_base_types = getRequest('param2');
			}
			return $this->pages_mklist_by_tags($s_tags, NULL, $s_template, $i_per_page, $b_ignore_paging, $s_base_types);
		}

		/**
		 * Возвращает страницы, которым назначен один из указанных тегов, ищет среди страниц текущего домена
		 * @param null $s_tags теги
		 * @param string $s_template имя шаблона для tpl шаблонизатора
		 * @param null $s_base_types список иерархических типов, среди которых искать страницу (пример: catalog.object+content.page)
		 * @param bool $i_per_page количество страниц к выводу
		 * @param bool $b_ignore_paging не учитывать пагинацию
		 * @return mixed
		 */
		public function pagesByDomainTags($s_tags = NULL, $s_template = "tags", $s_base_types = NULL, $i_per_page = false, $b_ignore_paging = false) {
			if (is_null($s_tags)) {
				$s_tags = getRequest('param0');
			}
			if (!$s_template) {
				$s_template = getRequest('param1');
			}
			if (is_null($s_base_types)) {
				$s_base_types = getRequest('param2');
			}
			return $this->pages_mklist_by_tags($s_tags, cmsController::getInstance()->getCurrentDomain()->getId(), $s_template, $i_per_page, $b_ignore_paging, $s_base_types);
		}

		/**
		 * Возвращает облако тегов страниц всех доменов
		 * @param string $s_template имя шаблона для tpl шаблонизатора
		 * @param bool|int $b_curr_user идентификатор пользователя
		 * @param int $i_per_page количество страниц к выводу
		 * @param bool $b_ignore_paging не учитывать пагинацию
		 * @return mixed
		 */
		public function tagsAccountCloud($s_template = "tags", $b_curr_user = false, $i_per_page = -1, $b_ignore_paging = true) {
			return $this->tags_mk_cloud(null, $s_template, $i_per_page, $b_ignore_paging, false, ($b_curr_user ? permissionsCollection::getInstance()->getUserId() : array()));
		}

		/**
		 * Возвращает облако эффективности тегов страниц всех доменов
		 * @param string $s_template имя шаблона для tpl шаблонизатора
		 * @param bool $b_curr_user идентификатор пользователя
		 * @param int $i_per_page количество страниц к выводу
		 * @param bool $b_ignore_paging не учитывать пагинацию
		 * @return mixed
		 */
		public function tagsAccountEfficiencyCloud($s_template = "tags", $b_curr_user = false, $i_per_page = -1, $b_ignore_paging = true) {
			return $this->tags_mk_eff_cloud(null, $s_template, $i_per_page, $b_ignore_paging, ($b_curr_user ? permissionsCollection::getInstance()->getUserId() : array()));
		}

		/**
		 * Возвращает облако используемых тегов страниц всех доменов
		 * @param string $s_template имя шаблона для tpl шаблонизатора
		 * @param int $i_per_page количество страниц к выводу
		 * @param bool $b_ignore_paging не учитывать пагинацию
		 * @return mixed
		 */
		public function tagsAccountUsageCloud($s_template = "tags", $i_per_page = -1, $b_ignore_paging = true) {
			return $this->tags_mk_cloud(null, $s_template, $i_per_page, $b_ignore_paging, true, array());
		}

		/**
		 * Возвращает облако тегов страниц текущего домена
		 * @param string $s_template имя шаблона для tpl шаблонизатора
		 * @param bool|int $b_curr_user идентификатор пользователя
		 * @param int $i_per_page количество страниц к выводу
		 * @param bool $b_ignore_paging не учитывать пагинацию
		 * @return mixed
		 */
		public function tagsDomainCloud($s_template = "tags", $b_curr_user = false, $i_per_page = -1, $b_ignore_paging = true) {
			return $this->tags_mk_cloud(cmsController::getInstance()->getCurrentDomain()->getId(), $s_template, $i_per_page, $b_ignore_paging, false, ($b_curr_user ? permissionsCollection::getInstance()->getUserId() : array()));
		}

		/**
		 * Возвращает облако эффективности тегов страниц текущего домена
		 * @param string $s_template имя шаблона для tpl шаблонизатора
		 * @param bool $b_curr_user идентификатор пользователя
		 * @param int $i_per_page количество страниц к выводу
		 * @param bool $b_ignore_paging не учитывать пагинацию
		 * @return mixed
		 */
		public function tagsDomainEfficiencyCloud($s_template = "tags", $b_curr_user = false, $i_per_page = -1, $b_ignore_paging = true) {
			return $this->tags_mk_eff_cloud(cmsController::getInstance()->getCurrentDomain()->getId(), $s_template, $i_per_page, $b_ignore_paging, ($b_curr_user ? permissionsCollection::getInstance()->getUserId() : array()));
		}

		/**
		 * Возвращает облако используемых тегов страниц текущего домена
		 * @param string $s_template имя шаблона для tpl шаблонизатора
		 * @param int $i_per_page количество страниц к выводу
		 * @param bool $b_ignore_paging не учитывать пагинацию
		 * @return mixed
		 */
		public function tagsDomainUsageCloud($s_template = "tags", $i_per_page = -1, $b_ignore_paging = true) {
			return $this->tags_mk_cloud(cmsController::getInstance()->getCurrentDomain()->getId(), $s_template, $i_per_page, $b_ignore_paging, false, array());
		}

		/**
		 * Возващает список страниц, которым назначены определенные теги
		 * @param string $s_tags теги
		 * @param null $i_domain_id искать только в текущем домене
		 * @param string $s_template имя шаблона для tpl шаблонизатора
		 * @param bool|int $i_per_page количество страниц к выводу
		 * @param bool $b_ignore_paging не учитывать пагинацию
		 * @param string $s_base_types список иерархических типов, среди которых искать страницу (пример: catalog.object+content.page)
		 * @return mixed
		 */
		private function pages_mklist_by_tags($s_tags, $i_domain_id = NULL, $s_template = "tags", $i_per_page = false, $b_ignore_paging = false, $s_base_types = '') {
			// init and context :
			$s_tpl_pages = "pages";
			$s_tpl_page = "page";
			$s_tpl_pages_empty = "pages_empty";

			// validate input :

			$i_per_page = intval($i_per_page);

			if (!$i_per_page) {
				$i_per_page = 10;
			}

			if ($i_per_page === -1) {
				$b_ignore_paging = true;
			}

			$s_template = strval($s_template);
			if (!strlen($s_template)) {
				$s_template = "tags";
			}

			$i_curr_page = intval(getRequest('p'));
			if ($b_ignore_paging) {
				$i_curr_page = 0;
			}

			$s_base_types = strval($s_base_types);

			// load templates :

			list(
				$tpl_pages,
				$tpl_page,
				$tpl_pages_empty
				) = content::loadTemplates("content/".$s_template,
				$s_tpl_pages,
				$s_tpl_page,
				$s_tpl_pages_empty
			);

			// process :

			$o_sel = new umiSelection();

			if ($i_domain_id) {
				$o_sel->setIsDomainIgnored(false);
			} else {
				$o_sel->setIsDomainIgnored(true);
			}

			if (strlen($s_base_types)) {
				$o_sel->setElementTypeFilter();
				$arr_base_types = preg_split("/\s+/is", $s_base_types);
				foreach ($arr_base_types as $s_next_type) {
					$arr_next_type = explode('.', $s_next_type);
					if (count($arr_next_type) === 2) {
						$o_hierarchy_type = umiHierarchyTypesCollection::getInstance()->getTypeByName($arr_next_type[0], $arr_next_type[1]);
						if ($o_hierarchy_type instanceof umiHierarchyType) {
							$i_hierarchy_type_id = $o_hierarchy_type->getId();
							$o_sel->addElementType($i_hierarchy_type_id);
						}
					}
				}
			}

			$o_sel->forceHierarchyTable();

			$o_object_type = umiObjectTypesCollection::getInstance()->getTypeByGUID('root-pages-type');
			$i_tags_field_id = $o_object_type->getFieldId('tags');
			$arr_tags = preg_split("/\s*,\s*/is", $s_tags);
			$o_sel->setPropertyFilter();
			$o_sel->addPropertyFilterEqual($i_tags_field_id, $arr_tags);

			$o_sel->setPermissionsFilter();
			$o_sel->addPermissions();

			if ($i_per_page !== -1) {
				$o_sel->setLimitFilter();
				$o_sel->addLimit($i_per_page, $i_curr_page);
			}

			$result = umiSelectionsParser::runSelection($o_sel);
			$total = umiSelectionsParser::runSelectionCounts($o_sel);
			$block_arr = array();
			$sz = sizeof($result);

			if ($sz == 0) {
				return content::parseTemplate($tpl_pages_empty, $block_arr);
			}

			$arr_items = array();

			for ($i = 0; $i < $sz; $i++) {
				$line_arr = Array();
				$element_id = intval($result[$i]);

				$element = umiHierarchy::getInstance()->getElement($element_id);

				if (!$element) {
					continue;
				}

				$line_arr['attribute:id'] = $element_id;
				$line_arr['node:name'] = $element->getName();
				$line_arr['attribute:link'] = umiHierarchy::getInstance()->getPathById($element_id);
				$line_arr['void:header'] = $lines_arr['name'] = $element->getName();

				if ($publish_time = $element->getValue('publish_time')) {
					$line_arr['attribute:publish_time'] = $publish_time->getFormattedDate("U");
				}

				$arr_items[] = content::parseTemplate($tpl_page, $line_arr, $element_id);
				umiHierarchy::getInstance()->unloadElement($element_id);
			}

			$block_arr['subnodes:items'] = $arr_items;
			$block_arr['tags'] = $s_tags;
			$block_arr['total'] = $total;
			$block_arr['per_page'] = $i_per_page;

			return content::parseTemplate($tpl_pages, $block_arr);
		}

		/**
		 * Возвращает данные для формирования облака тегов
		 * @param null $i_domain_id искать только в текущем домене
		 * @param string $s_template имя шаблона для tpl шаблонизатора
		 * @param int $i_per_page количество тегов к выводу
		 * @param bool $b_ignore_paging не учитывать пагинацию
		 * @param bool $b_by_usage выводить только используемые теги (назначенные страницам)
		 * @param array $arr_users массив идентификаторов пользователей
		 * @return mixed
		 * @throws coreException
		 */
		private function tags_mk_cloud($i_domain_id = NULL, $s_template = "tags", $i_per_page = -1, $b_ignore_paging = true, $b_by_usage = false, $arr_users = array()) {
			// init and context :
			$s_tpl_tags = "cloud_tags";
			$s_tpl_tag = "cloud_tag";
			$s_tpl_tag_sep = "cloud_tagseparator";
			$s_tpl_tags_empty = "cloud_tags_empty";

			// validate input :

			if (!$arr_users || intval($arr_users) === -1 || strval($arr_users) === getLabel('page_status_all')) {
				$arr_users = array();
			}
			if (is_int($arr_users)) {
				$arr_users = array(intval($arr_users));
			} elseif (is_array($arr_users)) {
				$arr_users = array_map('intval', $arr_users);
			} else {
				$arr_users = array(intval(strval($arr_users)));
			}

			$i_per_page = intval($i_per_page);
			if (!$i_per_page) {
				$i_per_page = 10;
			}
			if ($i_per_page === -1) {
				$b_ignore_paging = true;
			}

			$s_template = strval($s_template);
			if (!strlen($s_template)) {
				$s_template = "tags";
			}

			$i_curr_page = intval(getRequest('p'));
			if ($b_ignore_paging) {
				$i_curr_page = 0;
			}

			// load templates :
			list(
				$tpl_tags, $tpl_tag, $tpl_tag_sep, $tpl_tags_empty
				) = content::loadTemplates("content/".$s_template,
				$s_tpl_tags, $s_tpl_tag, $s_tpl_tag_sep, $s_tpl_tags_empty
			);
			// process :

			$max_font_size = 32;
			$min_font_size = 10;
			//
			$s_prefix = '';
			//
			if ($b_by_usage) {
				$o_object_type = umiObjectTypesCollection::getInstance()->getTypeByGUID('root-pages-type');
				$i_tags_field_id = $o_object_type->getFieldId('tags');
				$result = umiObjectProperty::objectsByValue($i_tags_field_id, 'all', true, true, ($i_domain_id ? $i_domain_id : -1));

			} else {
				cmsController::getInstance()->getModule('stat');
				$sStatIncPath = dirname(dirname(__FILE__)) . '/stat/classes/reports/';

				if (class_exists("statisticFactory") == false) {
					return;
				}

				$factory = new statisticFactory($sStatIncPath);

				$factory->isValid('allTags');
				/**
				 * @var allTags $report
				 */
				$report = $factory->get('allTags');

				if ($i_domain_id) {
					$s_prefix = 'Domain';
					$report->setDomain($i_domain_id);
				} else {
					$s_prefix = 'Account';
					$report->setDomain(-1);
				}

				if (is_array($arr_users) && count($arr_users)) {
					$report->setUserIDs($arr_users);
				}

				$result = $report->get();
			}

			if (isset($result['values']) && is_array($result['values'])) {
				natsort2d($result['values'], "cnt");
				$result['values'] = array_slice($result['values'], -$i_per_page, $i_per_page);
				natsort2d($result['values'], "value");
			}

			$max = intval($result['max']);
			$sum = intval($result['sum']);

			$arrTags = array();

			$s_values_label = ($b_by_usage ? 'values' : 'labels');
			$s_value_label = ($b_by_usage ? 'value' : 'tag');
			$s_value_cnt = 'cnt';

			$sz = sizeof($result[$s_values_label]);
			for ($i = 0; $i < $sz; $i++) {
				$label = $result[$s_values_label][$i];
				$tag = $label[$s_value_label];
				if (is_null($tag)) continue; //$tag = '[nontagged]';
				$cnt = intval($label[$s_value_cnt]);
				$f_weight = round($cnt * 100 / $sum, 1);
				$font_size = round(((($max_font_size - $min_font_size)/100) * $f_weight) + $min_font_size);
				$arrTags[$tag] = array('weight' => $f_weight, 'font' => $font_size);
			}
			//
			$summ_weight = 0;
			if (count($arrTags)) {
				$arrTagsTplteds = array();
				foreach ($arrTags as $sTag => $arrTagStat) {
					$summ_weight += $arrTagStat['weight'];
					$params = array(
						'tag'=>$sTag,
						'tag_urlencoded'=>rawurlencode($sTag),
						'attribute:weight' => $arrTagStat['weight'],
						'attribute:font' => $arrTagStat['font'],
						'attribute:context' => $s_prefix
					);
					$arrTagsTplteds[] = content::parseTemplate($tpl_tag, $params);
				}

				if (isset($arrTagsTplteds[0]) && is_array($arrTagsTplteds[0])) { // udata
					$arrForTags = array('subnodes:items'=>$arrTagsTplteds);
				} else { // not udata
					$arrForTags = array('items'=>implode($tpl_tag_sep, $arrTagsTplteds));
				}
				//
				$arrForTags['attribute:summ_weight'] = ceil($summ_weight);
				$arrForTags['attribute:context'] = $s_prefix;
				// RETURN
				return content::parseTemplate($tpl_tags, $arrForTags);
			} else {
				$arrForTags = array();
				// RETURN
				return content::parseTemplate($tpl_tags_empty, $arrForTags);
			}
		}

		/**
		 * Возвращает данные для формирования облака эффективности тегов
		 * @param null $i_domain_id искать только в текущем домене
		 * @param string $s_template мя шаблона для tpl шаблонизатора
		 * @param int $i_per_page количество тегов к выводу
		 * @param bool $b_ignore_paging не учитывать пагинацию
		 * @param array $arr_users массив идентификаторов пользователей
		 * @return mixed
		 * @throws coreException
		 */
		private function tags_mk_eff_cloud($i_domain_id = NULL, $s_template = "tags", $i_per_page = -1, $b_ignore_paging = true, $arr_users = array()) {
			if (!$arr_users || intval($arr_users) === -1 || strval($arr_users) === getLabel('page_status_all')) {
				$arr_users = array();
			}
			if (is_int($arr_users)) {
				$arr_users = array(intval($arr_users));
			} elseif (is_array($arr_users)) {
				$arr_users = array_map('intval', $arr_users);
			} else {
				$arr_users = array(intval(strval($arr_users)));
			}

			$i_per_page = intval($i_per_page);
			if (!$i_per_page) $i_per_page = 10;
			if ($i_per_page === -1) $b_ignore_paging = true;

			$s_template = strval($s_template);
			if (!strlen($s_template)) $s_template = "tags";

			$i_curr_page = intval(getRequest('p'));
			if ($b_ignore_paging) $i_curr_page = 0;

			// load templates :
			list(
				$tpl_tags, $tpl_tag, $tpl_tag_sep, $tpl_tags_empty
				) = content::loadTemplates("content/".$s_template,
				"cloud_tags", "cloud_tag", "cloud_tagseparator", "cloud_tags_empty"
			);
			// process :

			$max_font_size = 32;
			$min_font_size = 10;

			$s_prefix = 'Account';
			if ($i_domain_id) $s_prefix = 'Domain';

			// by usage :
			$o_object_type = umiObjectTypesCollection::getInstance()->getTypeByGUID('root-pages-type');
			$i_tags_field_id = $o_object_type->getFieldId('tags');
			//
			$result_u = umiObjectProperty::objectsByValue($i_tags_field_id, 'all', true, true, ($i_domain_id ? $i_domain_id : -1));

			// by popularity
			cmsController::getInstance()->getModule('stat');
			$sStatIncPath = dirname(dirname(__FILE__)) . '/stat/classes/reports/';
			$factory = new statisticFactory($sStatIncPath);
			$factory->isValid('allTags');
			/**
			 * @var allTags $report
			 */
			$report = $factory->get('allTags');
			if ($i_domain_id) {
				$report->setDomain($i_domain_id);
			} else {
				$report->setDomain(-1);
			}
			if (is_array($arr_users) && count($arr_users)) {
				$report->setUserIDs($arr_users);
			}
			$result_p = $report->get();

			$arrTags = array();

			$i_sum_u = intval($result_u['sum']);
			$i_sum_p = intval($result_p['sum']);
			$arr_usage_tags = $result_u['values'];
			$arr_popular_tags = $result_p['labels'];
			$arr_u_tags = array();
			$arr_p_tags = array();
			$arr_eff_tags = array();

			foreach ($arr_usage_tags as $arr_next_tag) {
				$s_tag = $arr_next_tag['value'];
				$i_tag = intval($arr_next_tag['cnt']);
				$arr_u_tags[$s_tag] = round($i_tag * 100 / $i_sum_u, 1);
				if (!isset($arr_eff_tags[$s_tag])) $arr_eff_tags[$s_tag] = 0;
			}
			foreach ($arr_popular_tags as $arr_next_tag) {
				$s_tag = $arr_next_tag['tag'];
				$i_tag = intval($arr_next_tag['cnt']);
				$arr_p_tags[$s_tag] = round($i_tag * 100 / $i_sum_p, 1);
				if (!isset($arr_eff_tags[$s_tag])) $arr_eff_tags[$s_tag] = 0;
			}

			foreach ($arr_eff_tags as $s_tag => $i_efficiency) {
				if (isset($arr_u_tags[$s_tag]) && isset($arr_p_tags[$s_tag])) {
					$arr_eff_tags[$s_tag] = round($arr_p_tags[$s_tag] / $arr_u_tags[$s_tag], 1);
				} elseif (isset($arr_u_tags[$s_tag])) {
					$arr_eff_tags[$s_tag] = 0; // 0/100
				} elseif (isset($arr_p_tags[$s_tag])) {
					$arr_eff_tags[$s_tag] = 1000; // 100/0.1 (0.1 - round(x/y, 1))
				}
			}

			$arrTags = array();

			foreach ($arr_eff_tags as $s_tag => $i_efficiency) {
				if (is_null($s_tag)) $s_tag = '[nontagged]';

				$f_weight = round($i_efficiency / 10, 1);

				$i_font = round(((($max_font_size - $min_font_size)/100) * $f_weight) + $min_font_size);

				$arrTags[$s_tag] = array('weight' => $f_weight, 'font' => $i_font);
			}

			$summ_weight = 0;
			if (count($arrTags)) {
				$arrTagsTplteds = array();
				foreach ($arrTags as $sTag => $arrTagStat) {
					$summ_weight += $arrTagStat['weight'];
					$params = array(
						'tag'=>$sTag,
						'tag_urlencoded'=>rawurlencode($sTag),
						'attribute:weight' => $arrTagStat['weight'],
						'attribute:font' => $arrTagStat['font'],
						'attribute:context' => $s_prefix
					);
					$arrTagsTplteds[] = content::parseTemplate($tpl_tag, $params);
				}

				if (isset($arrTagsTplteds[0]) && is_array($arrTagsTplteds[0])) { // udata
					$arrForTags = array('subnodes:items'=>$arrTagsTplteds);
				} else { // not udata
					$arrForTags = array('items'=>implode($tpl_tag_sep, $arrTagsTplteds));
				}
				//
				$arrForTags['attribute:summ_weight'] = $summ_weight;
				$arrForTags['attribute:context'] = $s_prefix;
				// RETURN
				return content::parseTemplate($tpl_tags, $arrForTags);
			} else {
				$arrForTags = array();
				// RETURN
				return content::parseTemplate($tpl_tags_empty, $arrForTags);
			}

		}
	}
?>