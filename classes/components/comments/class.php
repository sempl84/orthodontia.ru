<?php
	/**
	 * Базовый класс модуля "Комментарии".
	 *
	 * Модуль управляет сущностью "Комментарий" и
	 * содержит настройки для вставки виджетов комментариев
	 * Facebook и Вконтакте.
	 * @link http://help.docs.umi-cms.ru/rabota_s_modulyami/modul_kommentarii/
	 */
	class comments extends def_module {

		/**
		 * Конструктор
		 */
		public function __construct() {
			parent::__construct();

			$umiRegistry = regedit::getInstance();

			if ($umiRegistry->getVal('//modules/comments/default_comments') == NULL) {
				$umiRegistry->setVar('//modules/comments/default_comments', 1);
			}

			$this->per_page = (int) $umiRegistry->getVal("//modules/comments/per_page");
			$this->moderated = (int) $umiRegistry->getVal("//modules/comments/moderated");
			$cmsController = cmsController::getInstance();

			if ($cmsController->getCurrentMode() == "admin") {
				$commonTabs = $this->getCommonTabs();

				if ($commonTabs) {
					$commonTabs->add('view_comments');
					$commonTabs->add('view_noactive_comments');
				}

				$this->__loadLib("admin.php");
				$this->__implement("CommentsAdmin");

				$this->loadAdminExtension();

				$this->__loadLib("customAdmin.php");
				$this->__implement("CommentsCustomAdmin", true);
			} else {
				$this->__loadLib("macros.php");
				$this->__implement("CommentsMacros");

				$this->loadSiteExtension();

				$this->__loadLib("customAdmin.php");
				$this->__implement("CommentsCustomAdmin", true);
			}

			$this->__loadLib("handlers.php");
			$this->__implement("CommentsHandlers");

			$this->loadCommonExtension();
			$this->loadTemplateCustoms();

			$this->__loadLib("customCommon.php");
			$this->__implement("CommentsCustomCommon", true);
		}

		/**
		 * Возвращает ссылки на форму редактирования страницы модуля и
		 * на форму добавления дочернего элемента к странице.
		 * @param int $element_id идентификатор страницы модуля
		 * @param string $element_type тип страницы модуля
		 * @return array
		 */
		public function getEditLink($element_id, $element_type) {
			switch($element_type) {
				case "comment": {
					$link_edit = $this->pre_lang . "/admin/comments/edit/{$element_id}/";
					return Array(false, $link_edit);
				}
				default: {
					return false;
				}
			}
		}
	};
?>