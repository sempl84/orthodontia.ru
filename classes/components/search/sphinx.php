<?php
	/**
	 * Класс работы с Sphinx API
	 */
	class SphinxSearch {
		/**
		 * @var search $module
		 */
		public $module;

		/**
		 * Генерирует базовый View для контента
		 */
		public function generateView() {
			$contentIndex = new SphinxIndexGenerator('sphinx_content_index');
			$this->setIndexType($contentIndex);

			$sql = $contentIndex->generateViewQuery();
			$connection = ConnectionPool::getInstance()->getConnection();
			$connection->query($sql);

			$config = mainConfiguration::getInstance();
			$pathToSphinx = $config->get('sphinx', 'sphinx.dir');
			$dir = new umiDirectory($pathToSphinx);

			if (empty($pathToSphinx)) {
				$pathToSphinx = SYS_TEMP_PATH . DIRECTORY_SEPARATOR . 'sphinx';
			}

			$pathToConfig = $pathToSphinx . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR;
			$dir->requireFolder($pathToConfig);

			if (file_exists($pathToConfig)) {
				$fileName = $pathToConfig . 'view.sql';
				file_put_contents($fileName, $sql);
			}
			if ($connection->errorNumber() == 0) {
				$this->sendJson(array(
					'status'  => 'ok',
					'message' => getLabel('build-view-finish')
				));
			} else {
				$this->sendJson(array(
					'status'  => 'fail',
					'message' => getLabel('build-view-finish-error')
				));
			}
		}

		/**
		 * Генерирует базовый конфиг для Sphinx
		 */
		public function generateSphinxConfig() {
			$config = mainConfiguration::getInstance();

			$mySqlPort = $config->get('connections', 'core.port');
			if (empty($mySqlPort)) {
				$mySqlPort = 3306;
			}

			$pathToSphinx = $config->get('sphinx', 'sphinx.dir');
			$dir = new umiDirectory($pathToSphinx);
			if (empty($pathToSphinx)) {
				$pathToSphinx = SYS_TEMP_PATH . DIRECTORY_SEPARATOR . 'sphinx';
			}
			$pathToIndex = $pathToSphinx . DIRECTORY_SEPARATOR . 'index' . DIRECTORY_SEPARATOR;
			$dir->requireFolder($pathToIndex);
			$binlog = $pathToSphinx . DIRECTORY_SEPARATOR . 'log';
			$pathToLog = $pathToSphinx . DIRECTORY_SEPARATOR . 'log' . DIRECTORY_SEPARATOR;
			$dir->requireFolder($pathToLog);
			$pathToConfig = $pathToSphinx . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR;
			$dir->requireFolder($pathToConfig);

			$contentIndex = new SphinxIndexGenerator('sphinx_content_index');
			$this->setIndexType($contentIndex);

			$configSphinx = $contentIndex->generateSphinxConfig(array(
				'{mySqlHost}' => $config->get('connections', 'core.host'),
				'{mySqlUser}' => $config->get('connections', 'core.login'),
				'{mySqlPass}' => $config->get('connections', 'core.password'),
				'{mySqlDB}' => $config->get('connections', 'core.dbname'),
				'{mySqlPort}' => $mySqlPort,
				'{pathToIndex}' => $pathToIndex,
				'{listen}' => $config->get('sphinx', 'sphinx.port'),
				'{pathToLog}' => $pathToLog,
				'{binlog}' => $binlog,
			));

			if (file_exists($pathToConfig)) {
				$fileName = $pathToConfig . 'sphinx.conf';
				file_put_contents($fileName, $configSphinx);
				$this->sendJson(array(
					'status'  => 'ok',
					'message' => getLabel('build-config-sphinx-finish')
				));
			} else {
				$this->sendJson(array(
					'status'  => 'fail',
					'message' => getLabel('build-config-sphinx-finish-error')
				));
			}
		}

		/**
		 * Создан ли конфиг для Sphinx
		 */
		public function isExistsConfig() {
			$config = mainConfiguration::getInstance();
			$pathToSphinx = $config->get('sphinx', 'sphinx.dir');
			if (empty($pathToSphinx)) {
				$pathToSphinx = SYS_TEMP_PATH . DIRECTORY_SEPARATOR . 'sphinx';
			}
			$pathToConfig = $pathToSphinx . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'sphinx.conf';
			$this->sendJson(array(
				'response' => file_exists($pathToConfig)
			));
		}

		/**
		 * Отправляет на Sphinx запрос для поиска данных и возвращает результат
		 * @param string $phrase поисковая фраза
		 * @param int $limit ограничение на количество результатов
		 * @param string $index тип выборки
		 * @param SphinxClient $sphinx клиент для работы со Sphinx
		 * @internal param SphinxClient $MAX_MATCHES Максимальное кол-во документов в результате, которое сфинкс держит в памяти
		 * @internal param $fieldWeights Веса полей в ранжировании результатов
		 * @return array
		 */
		public function findResult($phrase, $limit, $index, $sphinx) {
			$config = mainConfiguration::getInstance();
			define('MAX_MATCHES' , 1000);
			define('HIGHLIGHT_INDEX' , 'content');

			$fieldWeights = array(
				'title' => (int) ($config->get('sphinx', 'sphinx.title') ? $config->get('sphinx', 'sphinx.title') : 10),
				'h1' => (int) ($config->get('sphinx', 'sphinx.h1') ? $config->get('sphinx', 'sphinx.h1') : 10),
				'meta_keywords' => (int) ($config->get('sphinx', 'sphinx.meta_keywords') ? $config->get('sphinx', 'sphinx.meta_keywords') : 5),
				'meta_descriptions' => (int) ($config->get('sphinx', 'sphinx.meta_descriptions') ? $config->get('sphinx', 'sphinx.meta_descriptions') : 3),
				'content' => (int) ($config->get('sphinx', 'sphinx.field_content') ? $config->get('sphinx', 'sphinx.field_content') : 1),
				'tags' => (int) ($config->get('sphinx', 'sphinx.tags') ? $config->get('sphinx', 'sphinx.tags') : 50)
			);

			if ($phrase) {
				$sphinx->open();

				$sphinx->SetSortMode(SPH_SORT_RELEVANCE);
				$sphinx->setFieldWeights($fieldWeights);

				$sphinx->setLimits(0, $limit, 1000);

				$event = new umiEventPoint("sphinxExecute");
				$event->setParam("sphinx", $sphinx);
				$event->setMode("before");
				$event->call();

				$sphinx->ResetGroupBy();
				$sphinx->SetFilter(
					'domain_id',
					array(
						cmsController::getInstance()->getCurrentDomain()->getId()
					)
				);

				$results = $sphinx->query($sphinx->escapeString($phrase), $index);

				$pages = umiHierarchy::getInstance();
				if (is_array($results) && array_key_exists('matches', $results)) {
					$ids = array();
					foreach ($results['matches'] as $id => $document) {
						$ids[] = $id;
					}
					$this->module->loadElements($ids);
					foreach ($results['matches'] as $id => $document) {
						if ($page = $pages->getElement($id)) {
							$results['matches'][$id]['page'] = $page;
						} else {
							unset($results['matches'][$id]);
						}
					}
				}

				return $results;
			}
		}

		/**
		 * Помечает в тексте подсветку вхождение поисковой фразы
		 * @param array $var массив текстов для подсветки
		 * @param string $phrase поисковая фраза
		 * @param SphinxClient $sphinx
		 * @return mixed
		 */
		public function highlighter($var, $phrase, $sphinx) {
			$res = $sphinx->buildExcerpts($var, HIGHLIGHT_INDEX, $phrase, array(
				'before_match' => '<strong>',
				'after_match' => '</strong>'
			));
			return $res[0];
		}

		/**
		 * Добавляет поля во View
		 * @param SphinxIndexGenerator $contentIndex
		 */
		protected function setIndexType($contentIndex) {
			$types = umiObjectTypesCollection::getInstance();

			$pagesType = $types->getSubTypesList($types->getType('root-pages-type')->getId());

			$indexFields = array(
				'title',
				'h1',
				'meta_keywords',
				'meta_descriptions',
				'content',
				'tags',
				'is_unindexed',
				'readme',
				'anons',
				'description',
				'descr',
				'message',
				'question',
				'answers',
			);

			$contentIndex->addPagesList($pagesType, $types, $indexFields);

			$event = new umiEventPoint("sphinxCreateView");
			$event->addRef("contentIndex", $contentIndex);
			$event->setMode("before");
			$event->call();
		}

		/**
		 * Преобразует данные в json и выводит в буффер
		 * @param mixed $data данные
		 * @throws coreException
		 */
		protected function sendJson($data) {
			/**
			 * @var HTTPOutputBuffer $buffer
			 */
			$buffer = outputBuffer::current();
			$buffer->option('generation-time', false);
			$buffer->clear();
			$buffer->push(
				json_encode(array(
						'result' => $data
					)
				)
			);
			$buffer->end();
		}
	}
?>