<?php
	/**
	 * Класс макросов, то есть методов, доступных в шаблоне
	 */
	class SearchMacros {
		/**
		 * @var search $module
		 */
		public $module;

		/**
		 * Возвращает результат работы поиска
		 * @param string $template имя шаблона (для tpl)
		 * @param string $search_string поисковая строка
		 * @param string $search_types список идентификаторов иерархических типов для поиска
		 * (указываются через пробел)
		 * @param string $search_branches Список разделов в которых будет осуществляться поиск
		 * (указываются через пробел).
		 * @param int $per_page количество выводимых результатов в рамках пагинации
		 * @return mixed
		 */
		public function search_do($template = "default", $search_string = "", $search_types = "", $search_branches = "", $per_page = 0) {
			// поисковая фраза :
			if (!$search_string) {
				$search_string = (string) getRequest('search_string');
			}

			$p = (int) getRequest('p');
			// если запрошена нетипичная постраничка
			if (!$per_page) {
				$per_page = intval(getRequest('per_page'));
			}
			if (!$per_page) {
				$per_page = $this->module->per_page;
			}

			$config = mainConfiguration::getInstance();
			$searchEngine = $config->get('modules', 'search.using-sphinx');
			if ($searchEngine){
				return $this->sphinxSearch($template, $search_string, $per_page, $p);
			}

			list(
				$template_block, $template_line, $template_empty_result, $template_line_quant
				) = search::loadTemplates("search/".$template,
				"search_block", "search_block_line", "search_empty_result", "search_block_line_quant"
			);

			$block_arr = Array();
			$block_arr['last_search_string'] = htmlspecialchars($search_string);

			$search_string = urldecode($search_string);
			$search_string = htmlspecialchars($search_string);
			$search_string = str_replace(". ", " ", $search_string);
			$search_string = trim($search_string, " \t\r\n%");
			$search_string = str_replace(array('"', "'"), "", $search_string);

			$orMode = (bool) getRequest('search-or-mode');

			if (!$search_string) {
				return $this->insert_form($template);
			}

			// если запрошен поиск только по определенным веткам :
			$arr_search_by_rels = array();
			if (!$search_branches) $search_branches = (string) getRequest('search_branches');
			$search_branches = trim(rawurldecode($search_branches));
			if (strlen($search_branches)) {
				$arr_branches = preg_split("/[\s,]+/", $search_branches);
				foreach ($arr_branches as $i_branch => $v_branch) {
					$arr_branches[$i_branch] = $this->module->analyzeRequiredPath($v_branch);
				}
				$arr_branches = array_map('intval', $arr_branches);
				$arr_search_by_rels = array_merge($arr_search_by_rels, $arr_branches);
				$o_selection = new umiSelection;
				$o_selection->addHierarchyFilter($arr_branches, 100, true);
				$o_result = umiSelectionsParser::runSelection($o_selection);
				$sz = sizeof($o_result);
				for ($i = 0; $i < $sz; $i++) {
					$arr_search_by_rels[] = intval($o_result[$i]);
				}
			}

			$search_types = $this->module->getSearchTypes($search_types);

			$lines = Array();
			$result = searchModel::getInstance()->runSearch($search_string, $search_types, $arr_search_by_rels, $orMode);
			$total = sizeof($result);

			$result = array_slice($result, $per_page * $p, $per_page);
			$this->module->loadElements($result);
			$i = $per_page * $p;
			$umiHierarchy = umiHierarchy::getInstance();
			$umiLinksHelper = umiLinksHelper::getInstance();

			foreach ($result as $num => $element_id) {
				$line_arr = Array();

				$element = $umiHierarchy->getElement($element_id);

				if (!$element) {
					continue;
				}

				$line_arr['type'] = $this->module->getTypeInfo($element);
				$line_arr['void:num'] = ++$i;
				$line_arr['attribute:id'] = $element_id;
				$line_arr['attribute:name'] = $element->getName();
				$line_arr['attribute:link'] = $umiLinksHelper->getLink($element);
				$line_arr['xlink:href'] = "upage://" . $element_id;
				$line_arr['node:context'] = searchModel::getInstance()->getContext($element_id, $search_string);
				$line_arr['void:quant'] = ($num < count($result)-1? search::parseTemplate($template_line_quant, array()) : "");
				$lines[] = search::parseTemplate($template_line, $line_arr, $element_id);

				search::pushEditable(false, false, $element_id);
				$umiHierarchy->unloadElement($element_id);
			}

			$block_arr['subnodes:items'] = $block_arr['void:lines'] = $lines;
			$block_arr['total'] = $total;
			$block_arr['per_page'] = $per_page;

			return search::parseTemplate(($total > 0 ? $template_block : $template_empty_result), $block_arr);
		}

		/**
		 * Возвращает данные для построения формы поиска
		 * @param string $template имя шаблона (для tpl)
		 * @return mixed
		 */
		public function insert_form($template = "default") {
			list($template_block) = search::loadTemplates("search/".$template, "search_form");

			$search_string = (string) getRequest('search_string');
			$search_string = strip_tags($search_string);
			$search_string = trim($search_string, " \t\r\n%");
			$search_string = htmlspecialchars(urldecode($search_string));
			$search_string = str_replace(array('"', "'"), "", $search_string);

			$orMode = (bool) getRequest('search-or-mode');

			$block_arr = Array();
			$block_arr['last_search_string'] = ($search_string) ? $search_string : "%search_input_text%";

			if ($orMode) {
				$block_arr['void:search_mode_and_checked'] = "";
				$block_arr['void:search_mode_or_checked'] = " checked";
			} else {
				$block_arr['void:search_mode_and_checked'] = " checked";
				$block_arr['void:search_mode_or_checked'] = "";
			}

			return search::parseTemplate($template_block, $block_arr);
		}

		/**
		 * Возвращает подсказки для поисковой фразы
		 * @param string $template имя шаблона (для tpl)
		 * @param bool|string $string поисковая строка
		 * @param int $limit ограничение на количество
		 * @return mixed
		 */
		public function suggestions($template = 'default', $string = false, $limit = 10) {
			if ($string == false) {
				$string = getRequest('suggest-string');
			}

			list($template_block, $template_line, $template_block_empty) = search::loadTemplates(
				"tpls/search/".$template, "suggestion_block", "suggestion_block_line", "suggestion_block_empty"
			);

			$search = searchModel::getInstance();
			$words = $search->suggestions($string, $limit);
			$total = sizeof($words);

			if ($total == 0) {
				return search::parseTemplate($template_block_empty, array());
			}

			$items_arr = array();
			foreach ($words as $word) {
				$item_arr = array(
					'attribute:count'	=> $word['cnt'],
					'node:word'			=> $word['word']
				);

				$items_arr[] = search::parseTemplate($template_line, $item_arr);
			}

			$block_arr = array(
				'words'	=> array('nodes:word' => $items_arr),
				'total'	=> $total
			);

			return search::parseTemplate($template_block, $block_arr);
		}

		/**
		 * Возвращает результат работы поиска с помощью Sphinx
		 * @param string $template имя шаблона (для tpl)
		 * @param string $searchString поисковая фраза
		 * @param int $perPage количество элементов на странице
		 * @param int $p номер страницы
		 * @return mixed|string
		 */
		public function sphinxSearch($template = 'default', $searchString = '', $perPage = 0, $p) {
			list(
				$template_block, $template_line, $template_empty_result, $template_line_quant
				) = search::loadTemplates("search/".$template,
				"search_block", "search_block_line", "search_empty_result", "search_block_line_quant"
			);

			if (!$searchString) {
				return $this->insert_form($template);
			}

			$result = Array();
			$items = Array();

			$index = '*';
			$limitResult = 1000;
			$config = mainConfiguration::getInstance();
			$sphinxHost = $config->get('sphinx', 'sphinx.host');
			$sphinxPort = (int) $config->get('sphinx', 'sphinx.port');


			$sphinx = new SphinxClient;
			$sphinx->SetServer($sphinxHost, $sphinxPort);
			if (!$sphinx->open()) {
				return;
			}

			/**
			 * @var search|SphinxSearch $search
			 */
			$search = $this->module;
			$resultSphinx = $search->findResult($searchString, $limitResult, $index, $sphinx);
			if (empty($resultSphinx) || !array_key_exists('matches', $resultSphinx)) {
				return;
			}
			$resultMatches = $resultSphinx['matches'];
			$total = sizeof($resultMatches);
			$resultMatches = array_slice($resultMatches, $perPage * $p, $perPage);
			$umiLinksHelper = umiLinksHelper::getInstance();
			$i = $perPage * $p;

			foreach ($resultMatches as $num => $element) {
				$item = Array();
				/** @var umiHierarchyElement $element */
				$page_weight = $element['weight'];
				$element = $element['page'];

				if(!$element) {
					continue;
				}

				$content = $element->getValue('content');
				$pattern = '/%[^\s](.*?)[^\s]%/i';
				$content = preg_replace($pattern, '', $content);

				$item['type'] = $search->getTypeInfo($element);
				$item['void:num'] = ++$i;
				$item['attribute:id'] = $element->getId();
				$item['attribute:name'] = $element->getName();
				$item['attribute:weight'] = $page_weight;
				$item['attribute:link'] = $umiLinksHelper->getLinkByParts($element);
				$item['xlink:href'] = "upage://" . $element->getId();
				$item['node:context'] = '<p>' . $search->highlighter(array($content), $searchString, $sphinx) . '</p>';
				$item['void:quant'] = ($num < count($resultMatches)-1? search::parseTemplate($template_line_quant, array()) : "");
				$items[] = search::parseTemplate($template_line, $item, $element->getId());

				search::pushEditable(false, false, $element->getId());
				umiHierarchy::getInstance()->unloadElement($element->getId());
			}

			$result['subnodes:items'] = $result['void:lines'] = $items;
			$result['total'] = $total;
			$result['per_page'] = $perPage;
			$result['last_search_string'] = "";

			return search::parseTemplate(($total > 0 ? $template_block : $template_empty_result), $result);
		}
	}
?>