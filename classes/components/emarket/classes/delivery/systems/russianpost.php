<?php
	/**
	 * Способ доставки типа "Почта России".
	 * Подходит ко всем заказам.
	 * Стоимость доставки расчитывается за счет интеграции
	 * с ресурсами:
	 *
	 * 1) http://www.russianpost.ru/autotarif/Autotarif.aspx;
	 * 2) http://emspost.ru/api/rest/;
	 */
	class russianpostDelivery extends delivery {

		/**
		 * {@inheritdoc}
		 */
		public function validate(order $order) {
			return true;
		}

		/**
		 * {@inheritdoc}
		 */
		public function getDeliveryPrice(order $order) {
			try {
				$umiObjectsCollection = umiObjectsCollection::getInstance();
				$address = $this->getOrderAddress($order);
				$weight = $this->getOrderWeight($order);
				$viewPost = $umiObjectsCollection->getObject($this->viewpost);
				$typePost = $umiObjectsCollection->getObject($this->typepost);

				if ($this->isEmsViewpost($viewPost)) {
					$price = $this->getEmsPrice($address, $weight);
				} else {
					$price = $this->getMailPrice($address, $weight, $order, $typePost->identifier, $typePost->identifier);
				}

				return $price;

			} catch (privateException $e) {
				return $e->getMessage();
			}
		}

		/**
		 * Возвращает адрес заказа.
		 * @param order $order заказ
		 * @return object $address адрес
		 * @throws privateException если в заказе не указан адрес
		 */
		private function getOrderAddress($order) {
			$address = umiObjectsCollection::getInstance()->getObject($order->getValue('delivery_address'));

			if (!$address) {
				throw new privateException(getLabel("error-russianpost-no-address"));
			}

			return $address;
		}

		/**
		 * Возвращает вес заказа.
		 * @param order $order заказ
		 * @return int вес
		 * @throws privateException если в заказе нет товаров
		 * @throws privateException если у товара в заказе не указан вес
		 */
		private function getOrderWeight($order) {
			$items = $order->getItems();

			if (!$items) {
				throw new privateException(getLabel("error-russianpost-empty-order"));
			}

			return $this->calculateOrderItemsWeight($items);
		}

		/**
		 * Возвращает вес товаров.
		 * @throws privateException если у товара не указан вес
		 * @param orderItem[] $items товары в заказе
		 * @return int $weight вес
		 */
		private function calculateOrderItemsWeight($items) {
			$weight = 0;

			foreach ($items as $item) {
				$itemWeight = (int) $item->getItemElement()->getValue("weight");

				if ($itemWeight === 0) {
					throw new privateException(getLabel("error-russianpost-no-weight"));
				}

				$weight += $itemWeight * $item->getAmount();
			}

			return $weight;
		}

		/**
		 * Относится ли вид отправления к ЕМС-доставке?
		 * @param object $viewpost вид отправления
		 * @return boolean
		 */
		private function isEmsViewpost($viewpost) {
			$emsNames = array(getLabel("object-ems_standart"), getLabel("object-ems_declared_value"));
			return in_array($viewpost->name, $emsNames);
		}

		/**
		 * Рассчитывает стоимость ЕМС-доставки и возвращает ее.
		 * @param object $address адрес
		 * @param int $weight вес
		 * @return string информация о стоимости и сроках доставки
		 * @throws privateException см. calculateEmsPrice()
		 */
		private function getEmsPrice($address, $weight) {
			$defaultCity = "Москва";
			$kilogram = 1000;

			$weight = $weight / $kilogram;
			$fromCity = umiObjectsCollection::getInstance()->getObject($this->departure_city);

			if ($fromCity instanceof umiObject) {
				$fromCityName = $fromCity->getName();
			} else {
				$fromCityName = $defaultCity;
			}

			$toCityName = $address->city;
			$response = $this->calculateEmsPrice($fromCityName, $toCityName, $weight);

			$price = $response->price;
			$min = $response->term->min;
			$max = $response->term->max;

			return "{$price} руб. (займет от {$min} до {$max} дней)";
		}

		/**
		 * Возвращает стоимость ЕМС-доставки на сайте http://emspost.ru.
		 * @param string $fromCityName название города отправления
		 * @param string $toCityName название города получения
		 * @param int $weight вес
		 * @return object ЕМС-ответ
		 * @throws privateException если превышен вес заказа
		 * @throws privateException если указан несуществующий город отправления или получения
		 * @throws privateException если в ответ на запрос получена ошибка
		 */
		private function calculateEmsPrice($fromCityName, $toCityName, $weight) {
			$response = json_decode(umiRemoteFileGetter::get('http://emspost.ru/api/rest/?method=ems.get.max.weight'));
			$maxWeight = $response->rsp->max_weight;

			if (($weight <= 0) || ($weight > $maxWeight)) {
				throw new privateException(getLabel("error-russianpost-max-weight", false, $maxWeight));
			}

			$response = json_decode(umiRemoteFileGetter::get('http://emspost.ru/api/rest?method=ems.get.locations&type=cities&plain=true'));
			$cities = $response->rsp->locations;

			$fromCityEms = $this->getCityEms(wa_strtoupper($fromCityName), $cities, 'from');
			$toCityEms = $this->getCityEms(wa_strtoupper($toCityName), $cities, 'to');

			$params = array(
				'method' => 'ems.calculate',
				'from'   => $fromCityEms,
				'to'     => $toCityEms,
				'weight' => $weight
			);

			$query = http_build_query($params);
			$response = json_decode(umiRemoteFileGetter::get("http://emspost.ru/api/rest?{$query}"));
			$flag = $response->rsp->stat;

			if ($flag != 'ok') {
				throw new privateException(getLabel("error-russianpost-no-to-city"));
			}

			return $response->rsp;
		}

		/**
		 * Возвращает идентификатор города по его названию из ЕМС-массива городов
		 * @param string $cityName название города
		 * @param array $cities массив с городами
		 * @param string 'from'|'to' $mode что ищем - город отправления или город получения
		 * @return string ЕМС-идентификатор города
		 * @throws privateException если искомого города нет в массиве
		 */
		private function getCityEms($cityName, $cities, $mode) {
			foreach ($cities as $city) {
				if ($city->name == $cityName) {
					$cityEms = $city->value;
					break;
				}
			}

			if (!isset($cityEms)) {
				$fromError = getLabel("error-russianpost-no-from-city");
				$toError = getLabel("error-russianpost-no-to-city");

				$msg = ($mode == 'from') ? $fromError : $toError;
				throw new privateException($msg);
			}

			return $cityEms;
		}

		/**
		 * Возвращает стоимость почтовой доставки на сайте http://www.russianpost.ru.
		 * @param object $address адрес
		 * @param int $weight вес
		 * @param order $order название города отправления
		 * @param int $viewpostIdentifier ид вида отправления
		 * @param int $typepostIdentifier ид способа пересылки
		 * @return int|string цена доставки или сообщение об ошибке
		 * @throws privateException если сервер вернул неожиданный ответ
		 */
		private function getMailPrice($address, $weight, $order, $viewpostIdentifier, $typepostIdentifier) {
			$value = $this->object->setpostvalue ? ceil($order->getActualPrice()) : 0;
			$zipcode = $address->getValue("index");

			$params = array(
				'viewPost'     => $viewpostIdentifier,
				'typePost'     => $typepostIdentifier,
				'countryCode'  => 643,
				'weight'       => $weight,
				'value1'       => $value,
				'postOfficeId' => $zipcode
			);

			$query = http_build_query($params);
			$url = "http://www.russianpost.ru/autotarif/Autotarif.aspx?{$query}";
			$content = umiRemoteFileGetter::get($url);

			if (preg_match("/<input id=\"key\" name=\"key\" value=\"(\d+)\"\/>/i", $content, $match)) {
				$key = trim($match[1]);
				$headers = array('Content-type' => 'application/x-www-form-urlencoded');
				$postVars = array('key' => $key);

				$content = umiRemoteFileGetter::get($url, false, $headers, $postVars);
				$content = umiRemoteFileGetter::get($url);
			}

			if (preg_match("/span\s+id=\"TarifValue\">([^<]+)<\/span/i", $content, $match)) {
				$price = floatval(str_replace(",", ".", trim($match[1])));

				if ($price > 0) {
					return $price;
				} elseif (preg_match("/span\s+id=\"lblErrStr\">([^<]+)<\/span/i", $content, $match)) {
					return $match[1];
				}
			}

			throw new privateException(getLabel("error-russianpost-undefined"));
		}
	}
?>
