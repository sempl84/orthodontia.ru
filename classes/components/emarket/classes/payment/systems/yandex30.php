<?php
	/**
	 * Способ оплаты через платежную систему "Яндекс.Касса"
	 */
	class yandex30Payment extends payment {
		/**
		 * Коды статуса ответа от сервиса (см. документацию сервиса)
		 * @const int STATUS_SUCCESS
		 * @const int STATUS_AUTHERROR
		 * @const int STATUS_SUCCESS_WITH_CHANGES
		 * @const int STATUS_DECLINE
		 * @const int STATUS_REQUESTERROR
		 * @const int STATUS_INTERNALERROR
		 */
		const STATUS_SUCCESS = 0;
		const STATUS_AUTHERROR = 1;
		const STATUS_SUCCESS_WITH_CHANGES = 2;
		const STATUS_DECLINE = 100;
		const STATUS_REQUESTERROR = 200;
		const STATUS_INTERNALERROR = 1000;

		/**
		 * {@inheritdoc}
		 */
		public function validate() {
			return true;
		}

		/**
		 * {@inheritdoc}
		 * Устанавливает заказу статус оплаты "Инициализирована"
		 */
		public function process($template = null) {
			$this->order->order();
			$shopId = $this->object->getValue('shop_id');
			$bankId = $this->object->getValue('bank_id');
			$scid   = $this->object->getValue('scid');

			if (!strlen($shopId) || !strlen($scid)) {
				throw new publicException(getLabel('error-payment-wrong-settings'));
			}

			$productPrice = (float) $this->order->getActualPrice();

			list($templateString, $modeItem) = emarket::loadTemplates(
				"emarket/payment/yandex30/" . $template,
				'form_block',
				'mode_type_item'
			);

			$modeTypeItems = array();

			foreach ($this->getAvailablePaymentTypes() as $payment) {
				$modeTypeItems[] = emarket::parseTemplate($modeItem, $payment);
			}

			$param = array();
			$param['shopId']	= $shopId;
			$param['Sum']		= $productPrice;
			$param['BankId']	= $bankId;
			$param['scid']		= $scid;
			$param['CustomerNumber']	= $this->order->getId();
			$param['formAction']		= $this->object->getValue('demo_mode') ? 'https://demomoney.yandex.ru/eshop.xml' : 'https://money.yandex.ru/eshop.xml';
			$param['orderId']			= $this->order->getId();
			$param['subnodes:items']	= $param['void:mode_type_list'] = $modeTypeItems;

			$this->order->setPaymentStatus('initialized');

			return emarket::parseTemplate($templateString, $param);
		}

		/**
		 * Возвращает список поддерживаемых типов оплаты
		 * @return array
		 */
		public function getAvailablePaymentTypes() {
			$payments = array();

			if ($this->object->getValue('yandex_pc')) {
				array_push($payments, array(
					'id' => 0,
					'type' => 'PC',
					'subtype' => '',
					'label' => getLabel('label-yandex-payment-pc')
				));
			}

			if ($this->object->getValue('yandex_ac')) {
				array_push($payments, array(
					'id' => 1,
					'type' => 'AC',
					'subtype' => '',
					'label' => getLabel('label-yandex-payment-ac')
				));
			}

			if ($this->object->getValue('yandex_mc')) {
				array_push($payments, array(
					'id' => 2,
					'type' => 'MC',
					'subtype' => '',
					'label' => getLabel('label-yandex-payment-mc')
				));
			}

			if ($this->object->getValue('yandex_gp_svzny')) {
				array_push($payments, array(
					'id' => 3,
					'type' => 'GP',
					'subtype' => 'SVZNY',
					'label' => getLabel('label-yandex-payment-gp-svzny')
				));
			}

			if ($this->object->getValue('yandex_gp_eurst')) {
				array_push($payments, array(
					'id' => 4,
					'type' => 'GP',
					'subtype' => 'EURST',
					'label' => getLabel('label-yandex-payment-gp-eurst')
				));
			}

			if ($this->object->getValue('yandex_gp_other')) {
				array_push($payments, array(
					'id' => 5,
					'type' => 'GP',
					'subtype' => 'OTHER',
					'label' => getLabel('label-yandex-payment-gp-other')
				));
			}

			if ($this->object->getValue('yandex_wm')) {
				array_push($payments, array(
					'id' => 6,
					'type' => 'WM',
					'subtype' => '',
					'label' => getLabel('label-yandex-payment-wm')
				));
			}

			if ($this->object->getValue('yandex_sb')) {
				array_push($payments, array(
					'id' => 7,
					'type' => 'SB',
					'subtype' => '',
					'label' => getLabel('label-yandex-payment-sb')
				));
			}

			if ($this->object->getValue('yandex_mp')) {
				array_push($payments, array(
					'id' => 8,
					'type' => 'MP',
					'subtype' => '',
					'label' => getLabel('label-yandex-payment-mp')
				));
			}

			return $payments;
		}

		/**
		 * {@inheritdoc}
		 *  В зависимости от типа запроса либо валидирует заказ с установлением соответствующего статуса оплаты
		 * (Проверена/Отклонена), либо переводит оплату заказа в статус "Принята".
		 */
		public function poll() {
			/**
			 * @var HTTPOutputBuffer $buffer
			 */
			$buffer = outputBuffer::current();
			$buffer->clear();
			$buffer->contentType('text/xml');
			$action    = getRequest('action');
			$shopId	   = getRequest('shopId');
			$invoiceId = getRequest('invoiceId');

			if (!$this->checkSignature()) {
				$responseCode = yandex30Payment::STATUS_AUTHERROR;
			} elseif(is_null($shopId) || is_null($invoiceId)) {
				$responseCode = yandex30Payment::STATUS_REQUESTERROR;
			} else {
				switch (strtolower($action)) {
					case 'checkorder'	: {
						$responseCode = $this->checkDetails();
						break;
					}
					case 'paymentaviso' : {
						$responseCode = $this->acceptPaymentResult();
						break;
					}
					default	: {
						$responseCode = yandex30Payment::STATUS_REQUESTERROR;
					}
				}
			}

			$this->order->payment_document_num = $invoiceId;
			$this->order->commit();

			$buffer->push($this->getResponseXML($action, $responseCode, $shopId, $invoiceId) );
			$buffer->end();
		}

		/**
		 * Проверяет подпись в запросе
		 * @return bool
		 */
		public function checkSignature() {
			$password = (string) $this->object->getValue('shop_password');

			if (!strlen($password)) {
				return false;
			}

			$hashPieces   = array();
			$hashPieces[] = getRequest('action');
			$hashPieces[] = getRequest('orderSumAmount');
			$hashPieces[] = getRequest('orderSumCurrencyPaycash');
			$hashPieces[] = getRequest('orderSumBankPaycash');
			$hashPieces[] = getRequest('shopId');
			$hashPieces[] = getRequest('invoiceId');
			$hashPieces[] = getRequest('customerNumber');
			$hashPieces[] = $password;
			$hashString   = md5(implode(';', $hashPieces));

			if (strcasecmp($hashString, getRequest('md5') ) == 0) {
				return true;
			}

			return false;
		}

		/**
		 * Формирует и возвращает xml для ответа на сервер Яндекс денег
		 * @param string $action код запроса, на которое выполняется ответ
		 * @param int $code код результата
		 * @param int $shopId идентификатор магазина
		 * @param int $invoiceId идентификатор транзакции
		 * @return int
		 */
		public function getResponseXML($action, $code, $shopId, $invoiceId) {
			$dateTime = date('c');
			$result   = "<"."?xml version=\"1.0\" encoding=\"windows-1251\" ?".">" . <<<XML
<{$action}Response performedDatetime="{$dateTime}" code="{$code}" shopId="{$shopId}" invoiceId="{$invoiceId}"/>
XML;
			return $result;
		}

		/**
		 * Валидирует данные заказа платежной системы и возвращает код
		 * статуса ответа для нее.
		 * Устанавливает соответствующий статус оплаты заказа (Проверена/Отклонена)
		 * @return int
		 */
		private function checkDetails() {
			$orderSumAmount = (float) getRequest('orderSumAmount');
			try {
				$actualPrice = (float) $this->order->getActualPrice();

				if ($orderSumAmount != $actualPrice) {
					$this->order->setPaymentStatus('declined');
					$resultCode = yandex30Payment::STATUS_DECLINE;
				} else {
					$this->order->setPaymentStatus('validated');
					$resultCode = yandex30Payment::STATUS_SUCCESS;
				}

			} catch (Exception $e) {
				$resultCode = yandex30Payment::STATUS_INTERNALERROR;
			}

			return $resultCode;
		}

		/**
		 * Принимает результат платежной транзакции и код
		 * статуса ответа для платежной системы.
		 * Устанавливает статус оплаты заказа "Принята".
		 * @return int
		 */
		private function acceptPaymentResult() {
			$resultCode = yandex30Payment::STATUS_SUCCESS;

			try {
				$this->order->setPaymentStatus('accepted');
			} catch(Exception $e) {
				$resultCode = yandex30Payment::STATUS_INTERNALERROR;
			}

			return $resultCode;
		}
	};
?>