<?php
	/**
	 * Класс уведомлений об изменении статуса заказа, доставки или оплаты
	 */
	class EmarketNotification {
		/**
		 * @var emarket $module
		 */
		public $module;

		/**
		 * Запускает отправку уведомления об изменении статуса заказа, доставки или оплаты.
		 * Устанавливает флаг необходимости экспорта в 1С и дату изменения статуса заказа.
		 * @param order $order заказ
		 * @param string $changedProperty строковой идентификатор поля заказа, значение которого изменилось
		 * @throws selectorException
		 */
		public function notifyOrderStatusChange(order $order, $changedProperty) {
			$order->need_export = true;

			if ($changedProperty == "status_id") {
				$order->status_change_date = new umiDate();
			}

			if (order::getCodeByStatus($order->getPaymentStatus()) == "accepted" && !$order->delivery_allow_date) {
				$sel = new selector('objects');
				$sel->types('hierarchy-type')->name('emarket', 'delivery');
				$sel->option('no-length')->value(true);
				if ($sel->first) {
					$order->delivery_allow_date = new umiDate();
				}
			}

			$statusId = $order->getValue($changedProperty);
			$codeName = order::getCodeByStatus($statusId);

			if ($changedProperty == 'status_id' && (!$statusId || $codeName == 'payment')) {
				return;
			}

			$this->sendCustomerNotification($order, $changedProperty, $codeName);

			if ($changedProperty == 'status_id' && $codeName == 'waiting') {
				$this->sendManagerNotification($order);
				$this->sendManagerPushNotification($order);
			}

			$order->commit();
		}

		/**
		 * Отправляет информацию о заказе на сервер PUSH уведомлений
		 * @param order $order заказ
		 * @throws Exception
		 * @throws selectorException
		 * @throws coreException
		 * @throws umiRemoteFileGetterException
		 */
		public function sendManagerPushNotification(order $order) {
			$currentDomain = cmsController::getInstance()->getCurrentDomain();

			if (!$currentDomain instanceof domain) {
				throw new coreException('Cannot detect current domain');
			}

			$currentLanguage = cmsController::getInstance()->getCurrentLang();

			if (!$currentLanguage instanceof lang) {
				throw new coreException('Cannot detect current language');
			}

			$selector = new selector("objects");
			$selector->types('object-type')->id('emarket-mobile-devices');
			$selector->where('domain_id')->equals($currentDomain->getId());
			$selector->where('active')->equals(1);
			$selector->option('no-length')->value(true);
			$selector->option('load-all-props')->value(true);

			if (!$selector->first) {
				return;
			}

			$tokens = array();
			/**
			 * @var iUmiObject $deviceObject
			 */
			foreach($selector->result as $deviceObject) {
				$token = $deviceObject->getValue('token');
				if (!$token) {
					continue;
				}
				$tokens[] = $token;
			}

			if (count($tokens) == 0) {
				return;
			}

			$request = array(
				'requestType' => 'sendPushMessage',
				'domain' => $currentDomain->getHost(),
				'lang_prefix' => $currentLanguage->getPrefix(),
				'tokens' => $tokens,
				'orderId' => (string) $order->getId(),
				'orderNumber' => (string) $order->getValue('number'),
				'orderStatusId' => (string) $order->getValue('status_id')
			);

			umiRemoteFileGetter::get(PUSH_SERVER, false, false, $request, false, 'POST', 10);
		}


		/**
		 * Отправляет почтовое уведомление об изменении статуса заказа, доставки
		 * или оплаты покупателю, оформившему заказ
		 * @param order $order заказ
		 * @param string $changedStatus строковой идентификатор поля заказа, которое было изменено
		 * @param string $codeName код нового статуса
		 * @return bool было ли письмо отправлено
		 */
		public function sendCustomerNotification(order $order, $changedStatus, $codeName) {
			$umiObjectsCollection = umiObjectsCollection::getInstance();
			$customer = $umiObjectsCollection->getObject($order->getCustomerId());
			$buyerOneClick = $umiObjectsCollection->getObject($order->getValue('purchaser_one_click'));
			$emailOneClick = false;

			if ($buyerOneClick instanceof umiObject) {
				$emailOneClick = $buyerOneClick->getValue('email') ? $buyerOneClick->getValue('email') : $buyerOneClick->getValue("e-mail");
			}

			if ($emailOneClick) {
				$email = $emailOneClick;
			} else {
				$email = $customer->getValue('email') ? $customer->getValue('email') : $customer->getValue("e-mail");
			}

			if (!umiMail::checkEmail($email)) {
				return false;
			}

			$name  = $customer->getValue('lname') . " " .$customer->getValue('fname') . " " . $customer->getValue('father_name');
			$cmsController = cmsController::getInstance();
			$languages = $cmsController->langs;
			$statusString = "";
			$subjectString = $languages['notification-status-subject'];
			$umiRegistry = regedit::getInstance();

			switch ($changedStatus) {
				case 'status_id' : {
					if ($umiRegistry->getVal('//modules/emarket/no-order-status-notification')) {
						return false;
					}

					if ($codeName == 'waiting') {
						$paymentStatusCodeName = order::getCodeByStatus($order->getPaymentStatus());
						$pkey = 'notification-status-payment-' . $paymentStatusCodeName;
						$okey = 'notification-status-' . $codeName;

						$statusString = ($paymentStatusCodeName == 'initialized') ?
							(
								(
									isset($languages[$okey]) ? ($languages[$okey] . " " . $languages['notification-and']) : ""
								) . (
									isset($languages[$pkey]) ? (" " . $languages[$pkey]) : ""
								)
							) : (
								(
									isset($languages[$pkey]) ? ($languages[$pkey] . " " . $languages['notification-and']) : ""
								) . (
									isset($languages[$okey]) ? (" " . $languages[$okey]) : ""
								)
							);

						$subjectString = $languages['notification-client-neworder-subject'];
					} else {
						$key = 'notification-status-' . $codeName;
						$statusString = isset($languages[$key]) ? $languages[$key] : "_";
					}
					break;
				}
				case 'payment_status_id': {
					if ($umiRegistry->getVal('//modules/emarket/no-payment-status-notification')) {
						return false;
					}

					$key = 'notification-status-payment-' . $codeName;
					$statusString = isset($languages[$key]) ? $languages[$key] : "_";
					break;
				}
				case 'delivery_status_id': {
					if ($umiRegistry->getVal('//modules/emarket/no-delivery-status-notification')) {
						return false;
					}

					$key = 'notification-status-delivery-' . $codeName;
					$statusString = isset($languages[$key]) ? $languages[$key] : "_";
					break;
				}
			}

			$paymentObject = $umiObjectsCollection->getObject($order->payment_id);
			$paymentClassName = null;

			if ($paymentObject instanceof iUmiObject) {
				$paymentType = $umiObjectsCollection->getObject($paymentObject->getValue('payment_type_id'));
				$paymentClassName = ($paymentType instanceof iUmiObject) ? $paymentType->getValue('class_name') : null;
			}

			$templateName  = ($paymentClassName == "receipt") ? "status_notification_receipt" : "status_notification";

			list($template) = emarket::loadTemplatesForMail(
				"emarket/mail/default",
				$templateName
			);

			$param = array();
			$param["order_id"]        = $order->id;
			$param["order_name"]      = $order->name;
			$param["order_number"]    = $order->number;
			$param["status"]          = $statusString;
			$param["personal_params"] = $this->module->getPersonalLinkParams($customer->getId());

			$domain = $cmsController->getCurrentDomain();
			$param["domain"] = ($domain instanceof domain) ? $domain->getCurrentHostName() : getServer('HTTP_HOST');

			if ($paymentClassName == "receipt") {
				$param["receipt_signature"] = sha1("{$customer->getId()}:{$customer->getValue('email')}:{$order->getValue('order_date')}");
			}

			$content = emarket::parseTemplateForMail($template, $param);
			$letter = new umiMail();
			$letter->addRecipient($email, $name);

			$domains = domainsCollection::getInstance();
			$domainId = ($domain instanceof domain) ? $domain->getId() : null;
			$defaultDomain = $domains->getDefaultDomain();
			$defaultDomainId = ($defaultDomain instanceof domain) ? $defaultDomain->getId() : null;

			if ($umiRegistry->getVal("//modules/emarket/from-email/{$domainId}")) {
				$fromMail = $umiRegistry->getVal("//modules/emarket/from-email/{$domainId}");
				$fromName = $umiRegistry->getVal("//modules/emarket/from-name/{$domainId}");
			} elseif ($umiRegistry->getVal("//modules/emarket/from-email/{$defaultDomainId}")) {
				$fromMail = $umiRegistry->getVal("//modules/emarket/from-email/{$defaultDomainId}");
				$fromName = $umiRegistry->getVal("//modules/emarket/from-name/{$defaultDomainId}");
			} else {
				$fromMail = $umiRegistry->getVal("//modules/emarket/from-email");
				$fromName = $umiRegistry->getVal("//modules/emarket/from-name");
			}

			$letter->setFrom($fromMail, $fromName);
			$letter->setSubject($subjectString);
			$letter->setContent($content);
			$letter->commit();
			$letter->send();

			return true;
		}

		/**
		 * Отправляет почтовое уведомление об изменении статуса заказа, доставки
		 * или оплаты менеджеру магазина
		 * @param order $order изменившийся заказа
		 * @return bool
		 */
		public function sendManagerNotification(order $order) {
			$umiRegistry  = regedit::getInstance();
			$cmsController = cmsController::getInstance();
			$domains = domainsCollection::getInstance();
			$currentDomain = $cmsController->getCurrentDomain();
			$defaultDomain = $domains->getDefaultDomain();
			$currentDomainId = ($currentDomain instanceof domain) ? $currentDomain->getId() : null;
			$defaultDomainId = ($defaultDomain instanceof domain) ? $defaultDomain->getId() : null;

			if ($umiRegistry->getVal("//modules/emarket/manager-email/{$currentDomainId}")) {
				$emails	= $umiRegistry->getVal("//modules/emarket/manager-email/{$currentDomainId}");
				$fromMail = $umiRegistry->getVal("//modules/emarket/from-email/{$currentDomainId}");
				$fromName = $umiRegistry->getVal("//modules/emarket/from-name/{$currentDomainId}");
			} elseif ($umiRegistry->getVal("//modules/emarket/manager-email/{$defaultDomainId}")) {
				$emails	= $umiRegistry->getVal("//modules/emarket/manager-email/{$defaultDomainId}");
				$fromMail = $umiRegistry->getVal("//modules/emarket/from-email/{$defaultDomainId}");
				$fromName = $umiRegistry->getVal("//modules/emarket/from-name/{$defaultDomainId}");
			} else {
				$emails	  = $umiRegistry->getVal('//modules/emarket/manager-email');
				$fromMail = $umiRegistry->getVal("//modules/emarket/from-email");
				$fromName = $umiRegistry->getVal("//modules/emarket/from-name");
			}

			$letter = new umiMail();
			$recipientsCount = 0;

			foreach (explode(',' , $emails) as $recipient) {
				$recipient = trim($recipient);
				if (strlen($recipient) && umiMail::checkEmail($recipient)) {
					$letter->addRecipient($recipient);
					$recipientsCount++;
				}
			}

			if ($recipientsCount == 0) {
				return false;
			}

			list($template) = emarket::loadTemplatesForMail(
				"emarket/mail/default",
				"neworder_notification"
			);

			try {
				/**
				 * @var payment $payment
				 */
				$payment = payment::get($order->payment_id, $order);
				$paymentName = $payment ? $payment->name : '';
				$paymentStatus = order::getCodeByStatus($order->getPaymentStatus());
			} catch(coreException $e) {
				$paymentName = "";
				$paymentStatus	= "";
			}

			$param = array();
			$param["order_id"] = $order->id;
			$param["order_name"] = $order->name;
			$param["order_number"] = $order->number;
			$param["payment_type"] = $paymentName;
			$param["payment_status"] = $paymentStatus;
			$param["price"] = $order->getActualPrice();
			$param["domain"] = ($currentDomain instanceof domain) ? $currentDomain->getCurrentHostName() : getServer('HTTP_HOST');
			$content = emarket::parseTemplateForMail($template, $param);
			$languages = $cmsController->langs;

			$letter->setFrom($fromMail, $fromName);
			$letter->setSubject($languages['notification-neworder-subject'] . " (#{$order->number})");
			$letter->setContent($content);
			$letter->commit();
			$letter->send();

			return true;
		}
	};
?>
