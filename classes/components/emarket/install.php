<?php
	/**
	 * Установщик модуля
	 */

	/**
	 * @var array $INFO реестр модуля
	 */
	$INFO = Array();
	$INFO['name'] = "emarket";
	$INFO['config'] = "1";
	$INFO['ico'] = "ico_eshop";
	$INFO['default_method_admin'] = "orders";
	$INFO['func_perms'] = "Группы прав на функционал модуля";
	$INFO['func_perms/purchasing'] = "Права на оформление заказа";
	$INFO['func_perms/personal'] = "Права на личный кабинет покупателя";
	$INFO['func_perms/compare'] = "Права на сравнение товаров";
	$INFO['func_perms/control'] = "Права на администрирование модуля";
	$INFO['func_perms/order_editing'] = "Права на оформление и изменение заказа от имени покупателя";
	$INFO['func_perms/mobile_application_get_data'] = "Права на api для мобильного приложения";
	$INFO['enable-discounts'] = "1";
	$INFO['enable-currency'] = "1";
	$INFO['enable-stores'] = "1";
	$INFO['enable-payment'] = "1";
	$INFO['enable-delivery'] = "1";
	$INFO['delivery-with-address'] = "0";
	$INFO['purchasing-one-step'] = "0";

	/**
	 * @var array $COMPONENTS файлы модуля
	 */
	$COMPONENTS = array();
	$COMPONENTS[] = "./classes/components/emarket/admin.php";
	$COMPONENTS[] = "./classes/components/emarket/class.php";
	$COMPONENTS[] = "./classes/components/emarket/common.php";
	$COMPONENTS[] = "./classes/components/emarket/customAdmin.php";
	$COMPONENTS[] = "./classes/components/emarket/customCommon.php";
	$COMPONENTS[] = "./classes/components/emarket/customMacros.php";
	$COMPONENTS[] = "./classes/components/emarket/events.php";
	$COMPONENTS[] = "./classes/components/emarket/handlers.php";
	$COMPONENTS[] = "./classes/components/emarket/i18n.en.php";
	$COMPONENTS[] = "./classes/components/emarket/i18n.php";
	$COMPONENTS[] = "./classes/components/emarket/includes.php";
	$COMPONENTS[] = "./classes/components/emarket/install.php";
	$COMPONENTS[] = "./classes/components/emarket/lang.en.php";
	$COMPONENTS[] = "./classes/components/emarket/lang.php";
	$COMPONENTS[] = "./classes/components/emarket/macros.php";
	$COMPONENTS[] = "./classes/components/emarket/notification.php";
	$COMPONENTS[] = "./classes/components/emarket/permissions.php";
	$COMPONENTS[] = "./classes/components/emarket/printInvoice.php";
	$COMPONENTS[] = "./classes/components/emarket/purchasingOneClick.php";
	$COMPONENTS[] = "./classes/components/emarket/purchasingOneStep.php";
	$COMPONENTS[] = "./classes/components/emarket/purchasingStages.php";
	$COMPONENTS[] = "./classes/components/emarket/purchasingStagesSteps.php";
	$COMPONENTS[] = "./classes/components/emarket/statReports.php";
	$COMPONENTS[] = "./classes/components/emarket/umiManagerAPI.php";
	$COMPONENTS[] = "./classes/components/emarket/yandexMarketClient.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/customer/customer.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/delivery/delivery.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/delivery/systems/courier.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/delivery/systems/russianpost.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/delivery/systems/self.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/discounts/discount.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/discounts/discountModificator.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/discounts/discountRule.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/discounts/iItemDiscountRule.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/discounts/iOrderDiscountRule.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/discounts/discounts/bonusDiscount.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/discounts/discounts/itemDiscount.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/discounts/discounts/orderDiscount.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/discounts/modificators/absolute.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/discounts/modificators/proc.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/discounts/rules/allOrdersPrices.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/discounts/rules/dataRange.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/discounts/rules/items.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/discounts/rules/orderPrice.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/discounts/rules/relatedItems.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/discounts/rules/userGroups.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/discounts/rules/users.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/orders/order.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/orders/orderItem.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/orders/items/custom.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/orders/items/digital.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/orders/items/optioned.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/orders/number/default.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/orders/number/iOrderNumber.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/payment/payment.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/payment/api/kupivkredit.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/payment/systems/acquiropay.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/payment/systems/courier.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/payment/systems/dengionline.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/payment/systems/invoice.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/payment/systems/kupivkredit.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/payment/systems/payanyway.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/payment/systems/payonline.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/payment/systems/paypal.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/payment/systems/rbk.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/payment/systems/receipt.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/payment/systems/robox.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/payment/systems/yandex30.php";
	$COMPONENTS[] = "./classes/components/emarket/classes/stat/emarketTop.php";
?>
