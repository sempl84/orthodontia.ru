<?php
	/**
	 * Адрес сервера push уведомлений
	 */
	define('PUSH_SERVER', 'http://push.umi-cms.ru/');
	/**
	 * Базовый класс скидки
	 */
	require_once dirname(__FILE__) . '/classes/discounts/discount.php';
	/**
	 * Базовый класс модификатора скидки
	 */
	require_once dirname(__FILE__) . '/classes/discounts/discountModificator.php';
	/**
	 * Интерфейс правила скидки на товар
	 */
	require_once dirname(__FILE__) . '/classes/discounts/iItemDiscountRule.php';
	/**
	 * Интерфейс правила скидки на заказ
	 */
	require_once dirname(__FILE__) . '/classes/discounts/iOrderDiscountRule.php';
	/**
	 * Базовый правила скидки
	 */
	require_once dirname(__FILE__) . '/classes/discounts/discountRule.php';
	/**
	 * Подключение реализаций скидок
	 */
	discount::init();
	/**
	 * Класс заказа или корзины интернет-магазина
	 */
	require_once dirname(__FILE__) . '/classes/orders/order.php';
	/**
	 * Базовый класс товарного наименования
	 */
	require_once dirname(__FILE__) . '/classes/orders/orderItem.php';
	/**
	 * Интерфейс генератора номера заказа
	 */
	require_once dirname(__FILE__) . '/classes/orders/number/iOrderNumber.php';
	$umiConfig = mainConfiguration::getInstance();
	/**
	 * Используемый генератор номера заказа
	 */
	$selectedNumbersGeneratorPath = dirname(__FILE__) . '/classes/orders/number/' . $umiConfig->get('modules', 'emarket.numbers') . '.php';
	if (is_file($selectedNumbersGeneratorPath)) {
		/**
		 * @noinspection PhpIncludeInspection
		 */
		require_once $selectedNumbersGeneratorPath;
	} else {
		require_once dirname(__FILE__) . '/classes/orders/number/default.php';
	}
	/**
	 * Базовый класс способа доставки
	 */
	require_once dirname(__FILE__) . '/classes/delivery/delivery.php';
	/**
	 * Базовый класс способа оплаты
	 */
	require_once dirname(__FILE__) . '/classes/payment/payment.php';
	/**
	 * Класс покупателя интернет-магазина
	 */
	require_once dirname(__FILE__) . '/classes/customer/customer.php';
	/**
	 * Класс сбора статистики интернет-магазина
	 */
	require_once dirname(__FILE__) . '/classes/stat/emarketTop.php';
	/**
	 * Автозагрузчик классов для интеграции с Яндекс.Маркет
	 */
	require_once dirname(__FILE__) . '/../../system/utils/yandex_market/vendor/autoload.php';
?>