<?php
	/**
	 * Класс функционала оформления заказа в 1 клик
	 */
	class EmarketPurchasingOneClick {
		/**
		 * @var emarket $module
		 */
		public $module;

		/**
		 * Выводит данные для построения формы создания заказа в клик
		 * @param int $object_type идентификатор типа данных заказа в клик
		 * @return mixed
		 */
		public function createForm($object_type) {
			$languages = langsCollection::getInstance();
			$lang = getRequest('lang');
			$lang = $languages->getLangId($lang);

			if (!$lang) {
				$lang = $languages->getDefaultLang()->getId();
			}

			$cmsController = cmsController::getInstance();
			$cmsController->setLang($languages->getLang($lang));

			$data = $cmsController->getModule('data');
			/**
			 * @var DataForms $data
			 */
			$form = $data->getCreateForm($object_type);

			if (array_key_exists('nodes:group', $form) && count($form['nodes:group']) > 0) {
				$form['nodes:group'][0]['attribute:lang'] = $languages->getLang($lang)->getPrefix();
			}

			return $form;
		}

		/**
		 * Принимает данные формы создания заказа в 1 клик и формирует на их основе заказ.
		 * Возвращает данные созданного заказа.
		 * При необходимо добавляет товар в корзину.
		 * @param bool|string $itemType тип товара (element)
		 * @param bool|int $elementId идентификатор товара (объекта каталога)
		 * @return array
		 * @throws publicException
		 */
		public function getOneClickOrder($itemType = false, $elementId = false) {
			/**
			 * @var emarket|EmarketPurchasingOneClick|EmarketMacros $module
			 */
			$module = $this->module;
			$params = array();
			$types = umiObjectTypesCollection::getInstance();
			$form = $types->getTypeByGUID('emarket-purchase-oneclick');

			$dataModule = cmsController::getInstance()->getModule('data');
			/**
			 * @var data $dataModule
			 */
			$errors = $dataModule->checkRequiredFields($form->getId());

			if ($errors !== true) {
				throw new publicException(getLabel('error-required_one_click_list') . $dataModule->assembleErrorFields($errors));
			}

			$errors = $this->validOneClickInfo();

			if (count($errors) > 0) {
				return $errors;
			}

			if ($itemType && $elementId) {
				$_REQUEST['no-redirect'] = 1;
				$module->basket('put', 'element', $elementId);
			}

			$order = $module->getBasketOrder();
			$this->saveOneClickInfo($order);

			if ($order->getTotalAmount() < 1) {
				throw new publicException('%error-market-empty-basket%');
			}

			$order->setValue('order_date', time());
			$numOrder = $order->generateNumber();
			$order->setOrderStatus('waiting');
			$order->commit();
			$params['orderId'] = $numOrder;

			return $params;
		}

		/**
		 * Инициирует валидацию данных формы создание заказа в 1 клик и
		 * возвращает полученные ошибки
		 * @return array
		 */
		public function validOneClickInfo() {
			$dataForm = getRequest('data');
			$emarketOneClick = umiObjectTypesCollection::getInstance()->getTypeByGUID('emarket-purchase-oneclick');
			$errors = array();

			/**
			 * @var iUmiField $field
			 */
			foreach ($emarketOneClick->getAllFields() as $field) {
				$value = $dataForm['new'][$field->getName()];

				if ($restrictionId = $field->getRestrictionId()) {
					$restriction = baseRestriction::get($restrictionId);

					if ($restriction instanceof baseRestriction) {

						if ($restriction instanceof iNormalizeInRestriction) {
							$value = $restriction->normalizeIn($value);
						}

						if ($restriction->validate($value) == false) {
							$fieldTitle = $field->getTitle();
							$errorMessage = getLabel('error-wrong-field-value');
							$errorMessage .=  " \"{$fieldTitle}\" - " . $restriction->getErrorMessage();
							$errors['nodes:error'][] = $errorMessage;
						}
					}
				}

				if (count($errors) > 0) {
					return $errors;
				}
			}

			return $errors;
		}

		/**
		 * Создает заказ в 1 клик, заполняет его
		 * и покупателя данными из формы
		 * @param order $order текущая корзина
		 * @throws coreException
		 */
		public function saveOneClickInfo(order $order) {
			$dataForm = getRequest('data');
			$objects = umiObjectsCollection::getInstance();

			$emarketOneClick = umiObjectTypesCollection::getInstance()->getTypeByGUID('emarket-purchase-oneclick');
			$objectId = $objects->addObject($order->getName(), $emarketOneClick->getId());
			$object = $objects->getObject($objectId);

			/**
			 * @var iUmiField $field
			 */
			foreach ($emarketOneClick->getAllFields() as $field) {
				$value = $dataForm['new'][$field->getName()];
				$object->setValue($field->getName(), $value);
			}

			$object->commit();
			$customer = customer::get();

			if (!$customer->isFilled()) {
				foreach ($emarketOneClick->getAllFields() as $field) {
					$value = $dataForm['new'][$field->getName()];
					$customer->setValue($field->getName(), $value);
				}
			}

			$order->setValue('purchaser_one_click', $objectId);
			$order->commit();
			$customer->commit();

			return;
		}
	}
?>
