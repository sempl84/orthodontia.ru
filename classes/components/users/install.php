<?php
	/**
	 * Установщик модуля
	 */
	
	/**
	 * @var array $INFO реестр модуля
	 */
	$INFO = Array();
	$INFO['name'] = "users";
	$INFO['config'] = "1";
	$INFO['default_method'] = "auth";
	$INFO['default_method_admin'] = "users_list";
	$INFO['func_perms'] = "Группы прав на функционал модуля";
	$INFO['func_perms/login'] = "Права на авторизацию";
	$INFO['func_perms/registrate'] = "Права на регистрацию";
	$INFO['func_perms/settings'] = "Права на редактирование настроек";
	$INFO['func_perms/users_list'] = "Права на администрирование модуля";
	$INFO['func_perms/profile'] = "Права на просмотр профиля пользователей";

	/**
	 * @var array $COMPONENTS файлы модуля
	 */
	$COMPONENTS = array();
	$COMPONENTS[] = "./classes/components/users/admin.php";
	$COMPONENTS[] = "./classes/components/users/class.php";
	$COMPONENTS[] = "./classes/components/users/common.php";
	$COMPONENTS[] = "./classes/components/users/customAdmin.php";
	$COMPONENTS[] = "./classes/components/users/customCommon.php";
	$COMPONENTS[] = "./classes/components/users/customMacros.php";
	$COMPONENTS[] = "./classes/components/users/events.php";
	$COMPONENTS[] = "./classes/components/users/handlers.php";
	$COMPONENTS[] = "./classes/components/users/i18n.en.php";
	$COMPONENTS[] = "./classes/components/users/i18n.php";
	$COMPONENTS[] = "./classes/components/users/includes.php";
	$COMPONENTS[] = "./classes/components/users/lang.en.php";
	$COMPONENTS[] = "./classes/components/users/lang.php";
	$COMPONENTS[] = "./classes/components/users/macros.php";
	$COMPONENTS[] = "./classes/components/users/permissions.php";
?>