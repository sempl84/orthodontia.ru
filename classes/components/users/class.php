<?php
	/**
	 * Класс пользователей и их групп
	 *
	 * Модуль отвечает за:
	 *
	 * 1) Авторизацию;
	 * 2) Регистрацию;
	 * 3) Подтверждение регистрации;
	 * 3) Восставновление пароля;
	 *
	 * @link http://help.docs.umi-cms.ru/rabota_s_modulyami/modul_polzovateli/
	 */
	class users extends def_module {
		/**
		 * @var string $user_login логин текущего пользователя
		 */
		public $user_login = "%users_anonymous_login%";
		/**
		 * @var int $user_id идентификатор текущего пользователя
		 */
		public $user_id;
		/**
		 * @var string $user_fullname полное имя текущего пользователя
		 */
		public $user_fullname = "%users_anonymous_fullname%";
		/**
		 * @var array|string $groups группы текущего пользователя
		 */
		public $groups = "";
		/**
		 * Включена перезагрузка прав пользователя на страницы при изменении прав пользователя на методы модулей
		 * @var bool
		 */
		protected $changePagesPermissionsOnEdit = false;
		/**
		 * Включена установка прав пользователя на страницы при создании пользователя
		 * @var bool
		 */
		protected $changePagesPermissionsOnAdd = false;

		/**
		 * Конструктор
		 * @throws coreException
		 */
		public function __construct() {
			parent::__construct();
			$umiObjectsCollection = umiObjectsCollection::getInstance();

			if (!defined('SV_GROUP_ID')) {
				define('SV_GROUP_ID', $umiObjectsCollection->getObjectIdByGUID('users-users-15'));
			}

			if (!defined('SV_USER_ID')) {
				define('SV_USER_ID', $umiObjectsCollection->getObjectIdByGUID('system-supervisor'));
			}

			$commonTabs = $this->getCommonTabs();

			if ($commonTabs) {
				$commonTabs->add('users_list_all', array('users_list'));
				$commonTabs->add('groups_list');
			}

			$umiRegistry = regedit::getInstance();
			$this->changePagesPermissionsOnEdit = (bool) $umiRegistry->getVal('//modules/users/pages_permissions_changing_enabled_on_edit');
			$this->changePagesPermissionsOnAdd = (bool) $umiRegistry->getVal('//modules/users/pages_permissions_changing_enabled_on_add');
			$cmsController = cmsController::getInstance();

			if ($cmsController->getCurrentMode() == "admin") {
				$this->__loadLib("admin.php");
				$this->__implement("UsersAdmin");

				$this->loadAdminExtension();

				$this->__loadLib("customAdmin.php");
				$this->__implement("UsersCustomAdmin", true);
			} else {
				$this->__loadLib("macros.php");
				$this->__implement("UsersMacros");

				$this->loadSiteExtension();

				$this->__loadLib("customMacros.php");
				$this->__implement("UsersCustomMacros", true);
			}

			$this->__loadLib("common.php");
			$this->__implement("UsersCommon");

			$this->__loadLib("handlers.php");
			$this->__implement("UsersHandlers");

			$this->loadCommonExtension();
			$this->loadTemplateCustoms();

			$this->__loadLib("customCommon.php");
			$this->__implement("UsersCustomCommon", true);

			$this->is_auth();
		}

		/**
		 * Проверяет авторизован ли пользователь
		 * @return bool
		 * @throws coreException
		 */
		public function is_auth() {
			static $isAuth;

			if (is_bool($isAuth)) {
				return $isAuth;
			}

			$umiObjectsCollection = umiObjectsCollection::getInstance();
			$guestId = $umiObjectsCollection->getObjectIdByGUID('system-guest');
			$userId = getSession('user_id');

			$this->user_login = "%users_anonymous_login%";
			$this->user_fullname = "%users_anonymous_fullname%";
			$umiTypesHelper = umiTypesHelper::getInstance();
			$userTypeId = $umiTypesHelper->getObjectTypeIdByGuid('users-user');

			if ($userId == $guestId) {
				$this->user_id = $userId;
				return $isAuth = false;
			}

			if (!is_null($userId) && $umiObjectsCollection->isExists($userId)) {
				$umiPropertiesHelper = umiPropertiesHelper::getInstance();
				$password = $umiPropertiesHelper->getPropertyValue($userId, 'password', $userTypeId);

				if (getSession('cms_pass') != $password) {
					unset($_SESSION['user_id']);
					$this->user_id = $guestId;
					return $isAuth = false;
				}

				return $this->initUser($userId, $userTypeId);
			}

			$existUser = $this->getUserIdBySessionData();
			if (sizeof($existUser) == 1) {
				$userId = $existUser[0]['id'];
				return $this->initUser($userId, $userTypeId);
			}

			$this->user_id = $guestId;
			$_SESSION['user_id'] = $guestId;
			return $isAuth = false;
		}

		/**
		 * Экранирует строку и возвращает результат
		 * @param string $stringVariable строка
		 * @return string
		 */
		public static function protectStringVariable($stringVariable = "") {
			$stringVariable = htmlspecialchars($stringVariable);
			return $stringVariable;
		}

		/**
		 * Обновляет время и дату последнего посещения авторизованного пользователя
		 * @param $user_id
		 * @return bool
		 * @throws coreException
		 */
		public function updateUserLastRequestTime($user_id) {

			$config = mainConfiguration::getInstance();

			$calculateUserLastRequestTime = intval($config->get('modules', 'users.calculate-last-request-time'));

			if ($calculateUserLastRequestTime === 0) {
				return false;
			}

			$umiObjectsCollection = umiObjectsCollection::getInstance();

			if ($user_id == $umiObjectsCollection->getObjectIdByGUID('system-guest')) {
				return false;
			}

			$user_object = $umiObjectsCollection->getObject($user_id);

			if ($user_object instanceof iUmiObject == false) {
				return false;
			}

			$cmsController = cmsController::getInstance();

			if ($cmsController->getCurrentMode() != "admin") {
				$time = time();

				if (($user_object->last_request_time + 60) < $time) {
					$user_object->last_request_time = $time;
					$user_object->commit();
				}
			}

			return true;
		}

		/**
		 * Возвращает ссылку на страницу редактирования объекта
		 * @param int $objectId идентификатор объекта
		 * @param bool $type контрольный параметр
		 * @return string
		 */
		public function getObjectEditLink($objectId, $type = false) {
			/* @var UsersMacros|UsersAdmin $this */
			return $this->getEditLink($objectId, $type = false);
		}

		/**
		 * Проверяет e-mail на уникальность
		 * @param string $email - проверяемый e-mail
		 * @param integer|bool $userId - id редактируемого пользователя
		 * @return boolean true если e-mail уникален или используется этим пользователем или не задан, false если e-mail используется другим пользователем
		 */
		public function checkIsUniqueEmail($email, $userId = false) {
			if (!$email) {
				return true;
			}

			$sel = new selector('objects');
			$sel->types('object-type')->name('users', 'user');
			$sel->where('e-mail')->equals($email);
			$sel->limit(0, 1);

			if ($sel->first) {
				return ($userId !== false) ? ($sel->first->id == $userId) : false;
			} else {
				return true;
			}
		}

		/**
		 * Проверяет логин на уникальность
		 * @param string $login - проверяемый логин
		 * @param integer|bool $userId - id редактируемого пользователя
		 * @return boolean true если логин уникален или используется этим пользователем или не задан, false если логин используется другим пользователем
		 */
		public function checkIsUniqueLogin($login, $userId = false) {
			if (!$login) {
				return true;
			}

			$sel = new selector('objects');
			$sel->types('object-type')->name('users', 'user');
			$sel->where('login')->equals($login);
			$sel->limit(0, 1);

			if($sel->first) {
				return ($userId !== false) ? ($sel->first->id == $userId) : false;
			} else {
				return true;
			}
		}

		/**
		 * Фильтрует значение логина и проверяет его
		 * @param string $login Проверяемое имя пользлвателя
		 * @param integer|bool $userId - id редактируемого пользователя
		 * @param boolean $public Режим проверки (из публички или из админки)
		 * @return string | false $valid отфильтрованный логин или false если логин не валиден
		 */
		public function validateLogin($login, $userId = false, $public = false) {
			$valid = false;
			$login = trim($login);
			$valid = $login ? $login : (bool) $login;

			$minLength = 1;
			if (!preg_match("/^\S+$/", $login) && $login) {
				$this->errorAddErrors('error-login-wrong-format');
				$valid = false;
			}

			if ($public) {
				$minLength = 3;
				if (mb_strlen($login, 'utf-8') > 40) {
					$this->errorAddErrors('error-login-long');
					$valid = false;
				}
			}

			if (mb_strlen($login, 'utf-8') < $minLength) {
				$this->errorAddErrors('error-login-short');
				$valid = false;
			}

			if (!$this->checkIsUniqueLogin($login, $userId)) {
				$this->errorAddErrors('error-login-exists');
				$valid = false;
			}

			return $valid;
		}

		/**
		 * Фильтрует значение пароля и проверяет его, сравнивает при необходимости с подтверждением и логином
		 * @param string $password пароль
		 * @param string $login логин
		 * @param string $passwordConfirmation подтверждение пароля
		 * @param boolean $public Режим проверки (из публички или из админки)
		 * @return string | false $valid отфильтрованный пароль или false если пароль не валиден
		 */
		public function validatePassword($password, $passwordConfirmation = null, $login = false, $public = false) {
			$valid = false;

			$password = trim($password);
			$valid = $password ? $password : (bool) $password;

			$minLength = 1;

			if (!preg_match("/^\S+$/", $password) && $password) {
				$this->errorAddErrors('error-password-wrong-format');
				$valid = false;
			}

			if ($login && ($password == trim($login))) {
				$this->errorAddErrors('error-password-equal-login');
				$valid = false;
			}

			if ($public) {
				$minLength = 3;
				if (!is_null($passwordConfirmation)) {
					if ($password != $passwordConfirmation) {
						$this->errorAddErrors('error-password-wrong-confirm');
						$valid = false;
					}
				}
			}

			if (mb_strlen($password, 'utf-8') < $minLength) {
				$this->errorAddErrors('error-password-short');
				$valid = false;
			}

			return $valid;
		}

		/**
		 * Фильтрует значение e-mail'а и проверяет его
		 * @param string $email
		 * @param integer $userId - id редактируемого пользователя
		 * @param boolean $requireActivation
		 * @return string | boolean $valid отфильтрованный e-mail, false если e-mail не валиден, true если e-mail не указан, а активация не требуется
		 */
		public function validateEmail($email, $userId = false, $requireActivation = true) {
			$valid = false;

			$email = strtolower(trim($email));
			$valid = $email ? $email : (bool) $email;

			if ($email) {
				if (!umiMail::checkEmail($email)) {
					$this->errorAddErrors('error-email-wrong-format');
					$valid = false;
				}
				if (!$this->checkIsUniqueEmail($email, $userId)) {
					$this->errorAddErrors('error-email-exists');
					$valid = false;
				}
			} elseif ($requireActivation) {
				$this->errorAddErrors('error-email-required');
				$valid = false;
			} else {
				return $valid = '';
			}

			return $valid;
		}

		/**
		 * Восстанавливает и авторизует пользователя на основе данных сессиии.
		 * Используется в связке с __emarket_admin::editOrderAsUser().
		 * @param bool $noRedirect не производить редирект при успешном выполнении
		 * @return bool
		 */
		public function restoreUser($noRedirect = false) {
			$userId = getSession('old_user_id');

			$umiObjectsCollection = umiObjectsCollection::getInstance();
			$user = $umiObjectsCollection->getObject($userId);

			if (!$user instanceof umiObject) {
				return false;
			}

			session::destroy();
			setcookie('u-login', "", time() - 3600, "/");
			setcookie('u-password', "", time() - 3600, "/");
			setcookie('u-password-md5', "", time() - 3600, "/");
			setcookie ('eip-panel-state-first', "", time() - 3600, "/");
			setcookie('customer-id', "", time() - 3600, "/");

			if (isset($_COOKIE['customer-id'])) {
				unset($_COOKIE['customer-id']);
			}

			$permissions = permissionsCollection::getInstance();
			$permissions->loginAsUser($userId);

			if ($permissions->isAdmin($userId)) {
				$session = session::getInstance();
				$session->set('csrf_token', md5(rand() . microtime()));
				if ($permissions->isSv($userId)) {
					$session->set('user_is_sv', true);
				}
				$session->setValid();
			}

			if ($noRedirect) {
				return true;
			}

			$this->redirect("/");
		}

		/**
		 * Получает идентификатор пользователя из сессии
		 * @return array
		 * @throws selectorException
		 */
		private function getUserIdBySessionData() {

			$login = getSession('cms_login');
			$pass = getSession('cms_pass');
			$existUser = array();

			if (!$login || !$pass) {
				return $existUser;
			}

			$existUser = new selector('objects');
			$existUser->types('object-type')->name('users', 'user');
			$existUser->where('login')->equals($login);
			$existUser->where('password')->equals($pass);
			$existUser->option('no-length')->value(true);
			$existUser->option('return')->value('id');
			$existUser->limit(0, 1);

			return $existUser->result();
		}

		/**
		 * Инициализирует пользователя
		 * @param int $userId идентификатор пользователя
		 * @param int $userTypeId идентификатор объектного типа данных пользователя
		 * @return bool
		 */
		private function initUser($userId, $userTypeId) {
			$userId = intval($userId);
			$userTypeId = intval($userTypeId);
			$umiPropertiesHelper = umiPropertiesHelper::getInstance();

			$login = $umiPropertiesHelper->getPropertyValue($userId, 'login', $userTypeId);
			$fname = $umiPropertiesHelper->getPropertyValue($userId, 'fname', $userTypeId);
			$lname = $umiPropertiesHelper->getPropertyValue($userId, 'lname', $userTypeId);
			$groups = $umiPropertiesHelper->getPropertyValue($userId, 'groups', $userTypeId);

			$this->updateUserLastRequestTime($userId);

			$this->groups = $groups;
			$this->user_id = $userId;
			$this->user_login = $login;
			$this->user_fullname = "{$fname} {$lname}";

			$_SESSION['user_id'] = $userId;
			system_runSession();
			return $is_auth = true;
		}

		/**
		 * Возвращает пользователя с заданным кодом активации
		 * @param string $activateCode код активации
		 * @return Array
		 * @throws coreException
		 */
		public function getUserByActivationCode($activateCode) {
			$typesCollection = umiObjectTypesCollection::getInstance();
			$object_type_id = $typesCollection->getTypeIdByHierarchyTypeName("users", "user");
			$object_type = $typesCollection->getType($object_type_id);
			$childTypes = $typesCollection->getChildTypeIds($object_type_id);
			$activate_code_field_id = $object_type->getFieldId("activate_code");

			$sel = new umiSelection;
			$sel->addLimit(1);
			$sel->addObjectType($object_type_id);
			$sel->addObjectType($childTypes);
			$sel->addPropertyFilterEqual($activate_code_field_id, $activateCode);

			return umiSelectionsParser::runSelection($sel);
		}

		/**
		 * Активирует пользователя
		 * @param int $userId идентификатор пользователя
		 * @throws coreException
		 */
		public function activateUser($userId) {
			$user = umiObjectsCollection::getInstance()->getObject($userId);
			$user->setValue("is_activated", 1);
			$user->setValue("activate_code", md5(uniqid(rand(), true)));
			$user->commit();

			permissionsCollection::getInstance()->loginAsUser($userId);

			$oEventPoint = new umiEventPoint("users_activate");
			$oEventPoint->setMode("after");
			$oEventPoint->setParam("user_id", $userId);
			$this->setEventPoint($oEventPoint);
		}

		/**
		 * Возвращает адрес виджета Loginza
		 * @return array|string
		 */
		public function getLoginzaProvider() {
			$loginzaAPI = new loginzaAPI();

			$result = Array();
			foreach ($loginzaAPI->getProvider() as $k=>$v) {
				$result['providers']['nodes:provider'][] = array('attribute:name'=>$k, 'attribute:title'=>$v);
			}

			$result ['widget_url'] = $loginzaAPI->getWidgetUrl() . "&providers_set=google,yandex,mailru,vkontakte,facebook,twitter,loginza,rambler,lastfm,myopenid,openid,mailruapi";

			if (self::isXSLTResultMode()) {
				return $result;
			} else {
				return $result['widget_url'];
			}
		}


		/**
		 * На сайте ли сейчас заданный пользователь
		 * @param bool|int $userId идентификатор пользователя
		 * @param int $onlineTimeout сколько секунд пользователь будет онлайн
		 * @return bool
		 * @throws publicException
		 */
		public function isUserOnline($userId = false, $onlineTimeout = 900) {
			if ($userId === false) {
				throw new publicException("This macros need user id given.");
			}

			if ($user = umiObjectsCollection::getInstance()->getObject($userId)) {
				$last_request_time = $user->getValue("last_request_time");

				$is_online = (bool) (($last_request_time + $onlineTimeout) >= time());

				$user->setValue("is_online", $is_online);
				$user->commit();

				return $is_online;
			}

			throw new publicException("User #{$userId} doesn't exist.");
		}

		/**
		 * Сохраняет права пользователя на страницу
		 * @param int $element_id id страницы
		 * @return void
		 */
		public function setPerms($element_id) {
			$permissions = permissionsCollection::getInstance();

			if (!getRequest('perms_read') && !getRequest('perms_edit') && !getRequest('perms_create') &&
				!getRequest('perms_delete') && !getRequest('perms_move') &&
				/* Note this argument. It's important' */
				getRequest('default-permissions-set')) {

				$permissions->setDefaultPermissions($element_id);
				return;
			} elseif (!getRequest('perms_read') && !getRequest('perms_edit') && !getRequest('perms_create') &&
				!getRequest('perms_delete') && !getRequest('permissions-sent')) {
				return;
			}

			$perms_read   = ($t = getRequest('perms_read'))   ? $t : array();
			$perms_edit   = ($t = getRequest('perms_edit'))   ? $t : array();
			$perms_create = ($t = getRequest('perms_create')) ? $t : array();
			$perms_delete = ($t = getRequest('perms_delete')) ? $t : array();
			$perms_move   = ($t = getRequest('perms_move'))	  ? $t : array();

			$permissions->resetElementPermissions($element_id);

			$owners = array_keys($perms_read);
			$owners = array_merge($owners, array_keys($perms_edit));
			$owners = array_merge($owners, array_keys($perms_create));
			$owners = array_merge($owners, array_keys($perms_delete));
			$owners = array_merge($owners, array_keys($perms_move));
			$owners = array_unique($owners);

			foreach ($owners as $owner) {
				$level = 0;
				if (isset($perms_read[$owner]))   $level |= 1;
				if (isset($perms_edit[$owner]))   $level |= 2;
				if (isset($perms_create[$owner])) $level |= 4;
				if (isset($perms_delete[$owner])) $level |= 8;
				if (isset($perms_move[$owner]))   $level |= 16;

				if (is_string($owner)) $owner = umiObjectsCollection::getInstance()->getObjectIdByGUID($owner);

				$permissions->setElementPermissions($owner, $element_id, $level);
			}
		}

		/**
		 * Сохраняет права пользователя|группы при его|ее редактировании или добавлении
		 * через административную панель
		 * @param int $ownerId id пользователя|группы
		 * @param string $mode режим запуска модуля (edit/add)
		 * @return void
		 * @throws publicAdminException если передан некорректный $ownerId
		 */
		public function save_perms($ownerId, $mode = 'add') {
			$owner = umiObjectsCollection::getInstance()->getObject($ownerId);

			if (!$owner instanceof umiObject) {
				throw new publicAdminException(__METHOD__ . ': object id expected');
			}

			$ownerGUID = $owner->getTypeGUID();

			if ($ownerGUID !== 'users-user' && $ownerGUID !== 'users-users') {
				throw new publicAdminException(__METHOD__ . ': user or group id expected');
			}

			if (!is_string($mode)) {
				throw new publicAdminException(__METHOD__ . ': wrong mode given');
			}

			$umiPermissions = permissionsCollection::getInstance();
			$umiRegistry = regedit::getInstance();
			$guestId = (int) $umiRegistry->getVal("//modules/users/guest_id");
			$defGroupId = (int) $umiRegistry->getVal("//modules/users/def_group");

			if (is_array(getRequest('ps_m_perms'))) {
				$umiPermissions->resetModulesPermissions($ownerId, array_keys(getRequest('ps_m_perms')));
			} else {
				$umiPermissions->resetModulesPermissions($ownerId);
			}

			$groups = $owner->getValue("groups");

			if (is_array($groups)) {
				if (sizeof($groups)) {
					list($nl) = $groups;
				} else {
					$nl = false;
				}
				if (!$nl) {
					$groups = false;
				}
			}

			if (!$groups) {
				$cnt = $umiPermissions->hasUserPermissions($ownerId);

				if (!$cnt) {
					$umiPermissions->copyHierarchyPermissions($guestId, $ownerId);
				}
			}

			$isOwnerGuestOrDefGroup = ($ownerId != $guestId && $ownerId != $defGroupId);

			foreach (getRequest('ps_m_perms') as $module => $value) {
				if (is_array(getRequest('m_perms')) && $isOwnerGuestOrDefGroup) {
					if (in_array($module, getRequest('m_perms'))) {
						$umiPermissions->setModulesPermissions($ownerId, $module, false, true);
					}
				}

				if (is_array($domains = getRequest('domain'))) {
					foreach ($domains as $id => $level) {
						$umiPermissions->setAllowedDomain($ownerId, $id, $level);
					}
				}

				if (!is_array(getRequest($module))) {
					continue;
				}

				foreach (getRequest($module) as $method => $is_allowed) {
					$umiPermissions->setModulesPermissions($ownerId, $module, $method, true);
					$methodsPermissions = $umiRegistry->getList("//modules/{$module}/func_perms/{$method}");
					if (is_array($methodsPermissions)) {
						foreach ($methodsPermissions as $method) {
							list($methodName) = $method;

							if (!$methodName || $methodName == 'NULL') {
								continue;
							}

							$umiPermissions->setModulesPermissions($ownerId, $module, $methodName, true);
						}
					}
				}
			}
			$umiPermissions->cleanupBasePermissions();
			switch (true) {
				case $mode == 'edit' && $this->changePagesPermissionsOnEdit: {
					$umiPermissions->deleteElementsPermissionsByOwnerId($ownerId);
					$umiPermissions->setAllElementsDefaultPermissions($ownerId);
					break;
				}
				case $mode == 'add' && $this->changePagesPermissionsOnAdd: {
					$umiPermissions->setAllElementsDefaultPermissions($ownerId);
					break;
				}
			}
		}

		/**
		 * Генерирует случайный пароль
		 * @param int $length длина пароля
		 * @return string
		 */
		public static function getRandomPassword ($length = 12) {
			if (function_exists('openssl_random_pseudo_bytes')) {
				$password = base64_encode(openssl_random_pseudo_bytes($length, $strong));
				if ($strong == TRUE) {
					return substr($password, 0, $length);
				}
			}

			$avLetters = "$#@^&!1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
			$size = strlen($avLetters);

			$npass = "";
			for ($i = 0; $i < $length; $i++) {
				$c = rand(0, $size - 1);
				$npass .= $avLetters[$c];
			}
			return $npass;
		}

		/**
		 * Создает автора на основе данных зарегистрированного пользователя
		 * и возвращает его идентификатор
		 * @param int $user_id идентификатор пользователя
		 * @return bool|int
		 * @throws coreException
		 * @throws selectorException
		 */
		public function createAuthorUser($user_id) {
			$objects = umiObjectsCollection::getInstance();
			$objectTypes = umiObjectTypesCollection::getInstance();

			if ($objects->isExists($user_id) === false) {
				return false;
			}

			$sel = new selector('objects');
			$sel->types('object-type')->name('users', 'author');
			$sel->where('user_id')->equals($user_id);
			$sel->limit(0, 1);

			if ($sel->first) {
				return $sel->first->id;
			}

			$user_object = $objects->getObject($user_id);
			$user_name = $user_object->getName();

			$object_type_id = $objectTypes->getTypeIdByHierarchyTypeName("users", "author");
			$author_id = $objects->addObject($user_name, $object_type_id);
			$author = $objects->getObject($author_id);
			$author->is_registrated = true;
			$author->user_id = $user_id;
			$author->commit();

			return $author_id;
		}

		/**
		 * Создает автора на основе данных незарегистрированного пользователя
		 * и возвращает его идентификатор
		 * @param string $nick псевдоним пользователя
		 * @param string $email почтовый ящик пользователя
		 * @param string $ip ip адрес с которого зашел пользователь
		 * @return int
		 * @throws coreException
		 * @throws selectorException
		 */
		public function createAuthorGuest($nick, $email, $ip) {
			$objects = umiObjectsCollection::getInstance();
			$objectTypes = umiObjectTypesCollection::getInstance();

			$nick = trim($nick);
			$email = trim($email);

			if (!$nick) {
				$nick = getLabel('author-anonymous');
			}
			if (!$email) {
				$email = getServer('REMOTE_ADDR');
			}

			$sel = new selector('objects');
			$sel->types('object-type')->name('users', 'author');
			$sel->where('email')->equals($email);
			$sel->where('nickname')->equals($nick);
			$sel->where('ip')->equals($ip);
			$sel->limit(0, 1);

			if ($sel->first) {
				return $sel->first->id;
			}

			$user_name = $nick . " ({$email})";

			$object_type_id = $objectTypes->getTypeIdByHierarchyTypeName("users", "author");
			$author_id = $objects->addObject($user_name, $object_type_id);
			$author = $objects->getObject($author_id);
			$author->setName($user_name);
			$author->is_registrated = false;
			$author->nickname = $nick;
			$author->email = $email;
			$author->ip = $ip;
			$author->commit();

			return $author_id;
		}

		/**
		 * Возвращает настройки пользователя
		 * @return array
		 * @throws coreException
		 */
		public function loadUserSettings() {
			$permissions = permissionsCollection::getInstance();
			$objects = umiObjectsCollection::getInstance();

			$user_id = $permissions->getUserId();
			$user = $objects->getObject($user_id);

			if ($user instanceof umiObject == false) {
				throw new coreException("Can't get current user with id #{$user_id}");
			}

			$settings_data = $user->user_settings_data;
			$settings_data_arr = unserialize($settings_data);

			if (!is_array($settings_data_arr)) {
				$settings_data_arr = array();
			}

			$block_arr = array();
			$items = array();

			foreach ($settings_data_arr as $key => $data) {
				$item_arr = array();
				$item_arr['attribute:key'] = (string) $key;

				$values_arr = array();
				foreach ($data as $tag => $value) {
					$value_arr = array();
					$value_arr['attribute:tag'] = (string) $tag;

					if ($key == 'dockItems' && $tag == 'common') {
						$value = $this->filterModulesList($value);
					}

					$value_arr['node:value'] = (string) $value;
					$values_arr[] = $value_arr;
				}
				$item_arr['nodes:value'] = $values_arr;
				$items[] = $item_arr;
			}
			$block_arr['items']['nodes:item'] = $items;
			return $block_arr;
		}

		/**
		 * Отфильтровывает несуществующие модули
		 * @param string $modules список названий модулей, разделенных ";"
		 * @return null|string
		 */
		public function filterModulesList($modules) {
			if (!is_string($modules)) {
				return null;
			}

			$dockModules = explode(";", $modules);
			$cmsController = cmsController::getInstance();
			$systemModules = $cmsController->getModulesList();

			$result = array();
			foreach ($dockModules as $moduleName) {
				if (in_array($moduleName, $systemModules) || $moduleName == 'trash') {
					$result[] = $moduleName;
				}
			}

			return implode(";", $result);
		}

		/**
		 * Сохраняет настройки пользователя
		 * @throws coreException
		 * @throws publicException
		 */
		public function saveUserSettings() {
			$this->flushAsXML("saveUserSettings");

			$permissions = permissionsCollection::getInstance();
			$objects = umiObjectsCollection::getInstance();

			$user_id = $permissions->getUserId();
			$user = $objects->getObject($user_id);

			if ($user instanceof umiObject == false) {
				throw new coreException("Can't get current user with id #{$user_id}");
			}

			$settings_data = $user->getValue("user_settings_data");
			$settings_data = unserialize($settings_data);
			if (!is_array($settings_data)) {
				$settings_data = Array();
			}

			$key = getRequest('key');
			$value = getRequest('value');
			$tags = (Array) getRequest('tags');

			if (!$key) {
				throw new publicException("You should pass \"key\" parameter to this resourse");
			}

			if(sizeof($tags) == 0) {
				$tags[] = 'common';
			}

			foreach ($tags as $tag) {
				if (!$value) {
					if (isset($settings_data[$key][$tag])) {
						unset($settings_data[$key][$tag]);

						if (sizeof($settings_data[$key]) == 0) {
							unset($settings_data[$key]);
						}
					}
				} else {
					$settings_data[$key][$tag] = $value;
				}
			}

			$user->setValue("user_settings_data", serialize($settings_data));
			$user->commit();
		}

		/**
		 * Деавторизует пользователя
		 * @param bool $makeRedirect нужно ли произвести редирект при успешной деавторизации
		 */
		public function logout($makeRedirect = true) {
			$session = session::getInstance();

			$session->del(array(
				'cms_login', 'cms_pass', 'user_id',
				'u-login', 'u-password', 'u-password-md5', 'u-session-id'
			));

			setcookie('u-login', "", time() - 3600, "/");
			setcookie('u-password', "", time() - 3600, "/");
			setcookie('u-password-md5', "", time() - 3600, "/");

			setcookie(ini_get('session.name'), "", time() - 3600, "/");
			system_removeSession();

			setcookie ('eip-panel-state-first', "", time() - 3600, "/");

			session::destroy();

			if ($makeRedirect) {
				$redirect_url = getRequest('redirect_url');
				$this->redirect($redirect_url);
			}
		}

		/**
		 * Возвращает ссылку на страницу редактирования объекта
		 * @param int $objectId идентификатор объекта
		 * @param bool $type контрольный параметр
		 * @return string
		 */
		public function getEditLink($objectId, $type = false) {
			if ($type == 'author') return $this->pre_lang . "/admin/data/guide_item_edit/" . $objectId . "/";
			return $this->pre_lang . "/admin/users/edit/" . $objectId . "/";
		}
	};
?>