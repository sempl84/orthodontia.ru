<?php
	/**
	 * Класс макросов, то есть методов, доступных в шаблоне
	 */
	class UsersMacros {
		/**
		 * @var users $module
		 */
		public $module;

		/**
		 * Выводит информацию об авторизованном пользователе
		 * @param string $template имя шаблона для tpl шаблонизатора
		 * @return string
		 */
		public function welcome($template = "default") {
			if (!$template) {
				$template = "default";
			}

			if ($this->module->is_auth()) {
				return $this->auth($template);
			} else {
				return "";
			}
		}

		/**
		 * Выводит форму изменения настроек пользователя.
		 * @param string $template имя шаблона для tpl шаблонизатора
		 * @return mixed
		 */
		public function settings($template = "default") {
			if (!$template) {
				$template = "default";
			}

			list($template_block) = users::loadTemplates("users/register/" . $template, "settings_block");

			$block_arr = Array();
			$block_arr['xlink:href'] = "udata://data/getEditForm/" . $this->module->user_id;
			$block_arr['user_id'] = $this->module->user_id;


			return users::parseTemplate($template_block, $block_arr, false, $this->module->user_id);
		}

		/**
		 * Выводит форму регистрации пользователя на сайте.
		 * @param string $template имя шаблона для tpl шаблонизатора
		 * @return mixed
		 */
		public function registrate($template = "default") {
			if (!$template) {
				$template = "default";
			}

			if ($this->module->is_auth()) {
				$this->module->redirect($this->module->pre_lang . "/users/settings/");
			}

			$objectTypeId = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName("users", "user");

			list($template_block) = users::loadTemplates("users/register/" . $template, "registrate_block");

			$block_arr = Array();
			$block_arr['xlink:href'] = "udata://data/getCreateForm/" . $objectTypeId;
			$block_arr['type_id'] = $objectTypeId;

			return users::parseTemplate($template_block, $block_arr);
		}

		/**
		 * Выводит результат регистрации
		 * @param string $template имя шаблона для tpl шаблонизатора
		 * @return mixed
		 */
		public function registrate_done($template = "default") {
			if (!$template) {
				$template = "default";
			}

			$suffix = '';
			switch (getRequest('result')) {
				case 'without_activation': $suffix = '_without_activation'; break;
				case 'error'             : $suffix = '_error'; break;
				case 'error_user_exists' : $suffix = '_user_exists'; break;
			}

			list($template_block) = users::loadTemplates("users/register/" . $template, "registrate_done_block" . $suffix);
			$block_arr = array(
				'result' => getRequest('result')
			);

			return users::parseTemplate($template_block, $block_arr);
		}

		/**
		 * Возвращает результат активации
		 * @param string $template имя шаблона для tpl шаблонизатора
		 * @param bool $isCorrect удачно ли прошла активация
		 * @return mixed
		 */
		public function getActivateResult($template = "default", $isCorrect = true) {
			if (!$template) {
				$template = "default";
			}

			list($template_block, $template_block_failed) = users::loadTemplates(
				"users/register/" . $template,
				"activate_block",
				"activate_block_failed"
			);
			$template = ($isCorrect) ? $template_block : $template_block_failed;

			return users::parseTemplate($template, array());
		}

		/**
		 * Выводит профиль пользователя.
		 * @param string $template имя шаблона для tpl шаблонизатора
		 * @param bool $user_id идентификатор пользователя
		 * @return mixed
		 * @throws coreException
		 * @throws publicException
		 */
		public function profile($template = "default", $user_id = false) {
			if (!$template) {
				$template = "default";
			}

			list($template_block, $template_bad_user_block) = users::loadTemplates(
				"users/profile/" . $template,
				"profile_block",
				"bad_user_block"
			);
			$block_arr = Array();

			if (!$user_id) {
				$user_id = (int) getRequest('param0');
			}

			if (!$user_id) {
				$permissions = permissionsCollection::getInstance();
				if ($permissions->isAuth()) {
					$user_id = $permissions->getUserId();
				}
			}

			/**
			 * @var iUmiObject $user
			 */
			if ($user = selector::get('object')->id($user_id)) {
				$this->module->validateEntityByTypes($user, array('module' => 'users', 'method' => 'user'));
				$block_arr['xlink:href'] = "uobject://" . $user_id;
				$userTypeId = $user->getTypeId();

				if ($userType = umiObjectTypesCollection::getInstance()->getType($userTypeId)) {
					$userHierarchyTypeId = $userType->getHierarchyTypeId();
					if ($userHierarchyType = umiHierarchyTypesCollection::getInstance()->getType($userHierarchyTypeId)) {
						if ($userHierarchyType->getName() == "users" && $userHierarchyType->getExt() == "user") {
							$block_arr['id'] = $user_id;
							return users::parseTemplate($template_block, $block_arr, false, $user_id);
						}
					}
				}
			} else {
				throw new publicException(getLabel('error-object-does-not-exist', null, $user_id));
			}

			return users::parseTemplate($template_bad_user_block, $block_arr);
		}

		/**
		 * Выводит список зарегистрированных и активированных пользователей.
		 * @param string $template имя шаблона для tpl шаблонизатора
		 * @param int $per_page количество выводимых пользователей на одну страницу
		 * @return mixed
		 * @throws coreException
		 */
		public function list_users($template = "default", $per_page = 10) {
			list($template_block, $template_block_item) = users::loadTemplates(
				"users/list_users/" . $template,
				"block",
				"block_item"
			);
			$block_arr = Array();

			$curr_page = (int) getRequest('p');

			$type_id = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName("users", "user");
			$type = umiObjectTypesCollection::getInstance()->getType($type_id);
			$is_active_field_id = $type->getFieldId('is_activated');

			$sel = new umiSelection;
			$sel->addLimit($per_page, $curr_page);
			$sel->addObjectType($type_id);
			$sel->addPropertyFilterEqual($is_active_field_id, true);
			$this->module->autoDetectOrders($sel, $type_id);
			$this->module->autoDetectFilters($sel, $type_id);

			$result = umiSelectionsParser::runSelection($sel);
			$total = umiSelectionsParser::runSelectionCounts($sel);

			$items = Array();

			foreach ($result as $user_id) {
				$item_arr = Array();
				$item_arr['void:user_id'] = $user_id;
				$item_arr['attribute:id'] = $user_id;
				$item_arr['xlink:href'] = "uobject://" . $user_id;
				$items[] = users::parseTemplate($template_block_item, $item_arr, false, $user_id);
			}

			$block_arr['subnodes:items'] = $items;
			$block_arr['per_page'] = $per_page;
			$block_arr['total'] = $total;
			return users::parseTemplate($template_block, $block_arr);
		}

		/**
		 * Выводит общее количество зарегистрированных и активированных пользователей.
		 * @return Int
		 * @throws coreException
		 */
		public function count_users() {
			$typeId = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName("users", "user");
			$type = umiObjectTypesCollection::getInstance()->getType($typeId);
			$isActiveField = $type->getFieldId('is_activated');

			$sel = new umiSelection;
			$sel->addObjectType($typeId);
			$sel->addPropertyFilterEqual($isActiveField, true);
			return umiSelectionsParser::runSelectionCounts($sel);
		}

		/**
		 * Выводит форму восстановления пароля
		 * @param string $template имя шаблона для tpl шаблонизатора
		 * @return mixed
		 */
		public function forget($template = "default") {
			list($template_block) = def_module::loadTemplates("users/forget/" . $template, "forget_block");
			return users::parseTemplate($template_block, array());
		}

		/**
		 * Возвращает результат отправки письма для восстановления пароля
		 * @param string $template имя шаблона для tpl шаблонизатора
		 * @param bool $isCorrect была отправка успешной
		 * @param string|bool $login некорректный логин, который был введен
		 * @param string|bool $email некорректный почтовый ящик, который был введен
		 * @return mixed
		 */
		public function getForgetResult($template = "default", $isCorrect = true, $login = false, $email = false) {
			list($template_wrong_login_block, $template_forget_sended) = users::loadTemplates(
				"users/forget/" . $template,
				"wrong_login_block",
				"forget_sended"
			);
			$block_arr = Array();
			$template = null;

			if ($isCorrect) {
				$template = $template_forget_sended;
				$block_arr['attribute:status'] = "success";
			} else {
				$template = $template_wrong_login_block;
				$block_arr['attribute:status'] = "fail";
				$block_arr['forget_login'] = $login;
				$block_arr['forget_email'] = $email;
			}

			return users::parseTemplate($template, $block_arr);
		}

		/**
		 * Возвращает результат восстановления пароля
		 * @param string $template имя шаблона для tpl шаблонизатора
		 * @param bool $isCorrect было ли восстановление успешным
		 * @param bool $login корректный логин, восстановленного пользователя
		 * @param bool $password корректный пароль, восстановленного пользователя
		 * @param bool $userId корректный идентификатор, восстановленного пользователя
		 * @return mixed
		 */
		public function getRestoreResult($template = "default", $isCorrect = true, $login = false, $password = false, $userId = false) {
			list(
				$template_restore_failed_block, $template_restore_ok_block
				) = def_module::loadTemplatesForMail(
				"users/forget/" . $template,
				"restore_failed_block",
				"restore_ok_block"
			);

			$block_arr = Array();
			$template = null;
			$pageId = false;
			$objectId = false;

			if ($isCorrect) {
				$template = $template_restore_ok_block;
				$block_arr['attribute:status'] = "success";
				$block_arr['login'] = $login;
				$block_arr['password'] = $password;
				$objectId = $userId;
			} else {
				$block_arr['attribute:status'] = "fail";
				$template = $template_restore_failed_block;
			}

			return def_module::parseTemplate($template, $block_arr, $pageId, $objectId);
		}

		/**
		 * Возвращает информацию об авторе
		 * @param bool|int $author_id идентификатор автора
		 * @param string $template имя шаблона для tpl шаблонизатора
		 * @return mixed
		 * @throws publicException
		 */
		public function viewAuthor($author_id = false, $template = "default") {
			if ($author_id === false) {
				throw new publicException(getLabel('error-object-does-not-exist', null, $author_id));
			}

			if (!($author = umiObjectsCollection::getInstance()->getObject($author_id))) {
				throw new publicException(getLabel('error-object-does-not-exist', null, $author_id));
			}

			if (!$template) {
				$template = "default";
			}

			list($template_user, $template_guest, $template_sv) = users::loadTemplates(
				"users/author/{$template}",
				"user_block",
				"guest_block",
				"sv_block"
			);

			$block_arr = Array();

			if ($author->getTypeId() == umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName('users', 'user')) {
				$template = $template_user;
				$block_arr['user_id'] = $author_id;

				$user = $author;

				$block_arr['nickname'] = $user->getValue("login");
				$block_arr['email']    = $user->getValue("e-mail");
				$block_arr['fname']    = $user->getValue("fname");
				$block_arr['lname']    = $user->getValue("lname");

				$block_arr['subnodes:groups'] = $groups = $user->getValue("groups");

				if (in_array(SV_GROUP_ID, $groups)) {
					if ($template_sv) {
						$template = $template_sv;
					}
				}
			} else if($author->getValue("is_registrated")) {
				$template = $template_user;
				$block_arr['user_id'] = $user_id = $author->getValue("user_id");

				$user = umiObjectsCollection::getInstance()->getObject($user_id);

				if (!$user instanceof umiObject) {
					$block_arr['user_id'] = $user_id = intval(regedit::getInstance()->getVal("//modules/users/guest_id"));
					$user = umiObjectsCollection::getInstance()->getObject($user_id);
				}

				if (!$user instanceof umiObject) {
					return false;
				}

				$block_arr['nickname'] = $user->getValue("login");
				$block_arr['login'] = $user->getValue("login");
				$block_arr['email'] = $user->getValue("e-mail");
				$block_arr['fname'] = $user->getValue("fname");
				$block_arr['lname'] = $user->getValue("lname");

				$block_arr['subnodes:groups'] = $groups = $user->getValue("groups");
				if (in_array(SV_GROUP_ID, $groups)) {
					if ($template_sv) {
						$template = $template_sv;
					}
				}
			} else {
				$template = $template_guest;
				$block_arr['user_id'] = $author_id;
				$block_arr['nickname'] = $author->getValue("nickname");
				$block_arr['email'] = $author->getValue("email");
			}
			return users::parseTemplate($template, $block_arr, false, $author_id);
		}



		/**
		 * Сохраняет настройки пользователя
		 * @throws coreException
		 * @throws errorPanicException
		 * @throws privateException
		 */
		public function settings_do() {
			if (!users::checkHTTPReferer()) {
				throw new coreException(getLabel('error-users-non-referer'));
			}

			$module = $this->module;

			if (strtolower($_SERVER['REQUEST_METHOD']) != 'post') {
				$module->errorNewMessage("%errors_bad_request_method%");
				$module->errorPanic();
			}

			$object_id = $module->user_id;

			$password = trim((string) getRequest('password'));
			$email = trim((string) getRequest('email'));

			$oEventPoint = new umiEventPoint("users_settings_do");
			$oEventPoint->setMode("before");
			$oEventPoint->setParam("user_id", $object_id);
			users::setEventPoint($oEventPoint);

			$object = umiObjectsCollection::getInstance()->getObject($object_id);
			$login = $object->getValue('login');

			if ($password) {
				if (strlen($password) < 3) {
					$module->errorNewMessage("%errors_too_short_password%");
					$module->errorPanic();
				}

				if ($login == $password) {
					$module->errorNewMessage("%errors_login_equals_password%");
					$module->errorPanic();
				}

				if (!is_null($password_confirm = getRequest('password_confirm'))) {
					if ($password != $password_confirm) {
						$module->errorNewMessage("%errors_wrong_password_confirm%");
						$module->errorPanic();
					}
				}

				$object->setValue("password", md5($password));
				$_SESSION['cms_pass'] = md5($password);
			}

			if ($email) {
				if (!preg_match("/^.+@\S+\.\S{2,}$/", $email)) {
					$module->errorNewMessage("%errors_wrong_email_format%");
					$module->errorPanic();
				}

				if (!$module->checkIsUniqueEmail($email, $object_id)) {
					$module->errorNewMessage("%error_users_non_unique_email%");
					$module->errorPanic();
				}

				$object->setValue("e-mail", $email);
			}

			/**
			 * @var data|DataForms $data_module
			 */
			$data_module = cmsController::getInstance()->getModule('data');
			$data_module->saveEditedObject($object_id);

			$object->commit();

			$oEventPoint = new umiEventPoint("users_settings_do");
			$oEventPoint->setMode("after");
			$oEventPoint->setParam("user_id", $object_id);
			users::setEventPoint($oEventPoint);

			$url = getRequest("from_page");

			if (!$url) {
				$url = ($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $this->pre_lang . "/users/settings/";
			}

			$module->redirect($url);
		}

		/**
		 * Регистрирует пользователя
		 * @throws coreException
		 * @throws errorPanicException
		 * @throws wrongValueException
		 */
		public function registrate_do() {
			$module = $this->module;

			if ($module->is_auth()) {
				$module->redirect($module->pre_lang . "/");
			}

			if (!($template = getRequest('template'))) {
				$template = 'default';
			}

			$objectTypes = umiObjectTypesCollection::getInstance();
			$regedit = regedit::getInstance();

			$refererUrl = getServer('HTTP_REFERER');
			$without_act = (bool) $regedit->getVal("//modules/users/without_act");

			$objectTypeId = $objectTypes->getTypeIdByHierarchyTypeName("users", "user");
			if ($customObjectTypeId = getRequest('type-id')) {
				$childClasses = $objectTypes->getChildTypeIds($objectTypeId);
				if (in_array($customObjectTypeId, $childClasses)) {
					$objectTypeId = $customObjectTypeId;
				}
			}

			$module->errorSetErrorPage($refererUrl);

			$login = $module->validateLogin(getRequest('login'), false, true);
			$password = $module->validatePassword(getRequest('password'), getRequest('password_confirm'), getRequest('login'), true);
			$email = $module->validateEmail(getRequest('email'), false, !$without_act);

			if (!umiCaptcha::checkCaptcha()) {
				$module->errorAddErrors('errors_wrong_captcha');
			}

			$module->errorThrow('public');

			$oEventPoint = new umiEventPoint("users_registrate");
			$oEventPoint->setMode("before");
			$oEventPoint->setParam("login", $login);
			$oEventPoint->addRef("password", $password);
			$oEventPoint->addRef("email", $email);
			users::setEventPoint($oEventPoint);

			$objectId = umiObjectsCollection::getInstance()->addObject($login, $objectTypeId);
			$activationCode = md5($login . time());

			$object = umiObjectsCollection::getInstance()->getObject($objectId);

			$object->setValue("login", $login);
			$object->setValue("password", md5($password));
			$object->setValue("e-mail", $email);

			$object->setValue("is_activated", $without_act);
			$object->setValue("activate_code", $activationCode);
			$object->setValue("referer", urldecode(getSession("http_referer")));
			$object->setValue("target", urldecode(getSession("http_target")));
			$date =  new umiDate();
			$object->setValue("register_date", $date->getCurrentTimeStamp());
			$object->setOwnerId($objectId);

			if ($without_act) {
				$_SESSION['cms_login'] = $login;
				$_SESSION['cms_pass'] = md5($password);
				$_SESSION['user_id'] = $objectId;
				session_commit();
			}

			$group_id = $regedit->getVal("//modules/users/def_group");
			$object->setValue("groups", Array($group_id));

			/**
			 * @var data|DataForms $data_module
			 */
			$data_module = cmsController::getInstance()->getModule('data');
			$data_module->saveEditedObjectWithIgnorePermissions($objectId, true, true);

			$object->commit();
			//Forming mail...
			list(
				$template_mail, $template_mail_subject, $template_mail_noactivation, $template_mail_subject_noactivation
				) = def_module::loadTemplatesForMail(
				"users/register/" . $template,
				"mail_registrated",
				"mail_registrated_subject",
				"mail_registrated_noactivation",
				"mail_registrated_subject_noactivation"
			);

			if ($without_act && $template_mail_noactivation && $template_mail_subject_noactivation) {
				$template_mail = $template_mail_noactivation;
				$template_mail_subject = $template_mail_subject_noactivation;
			}

			$mailData = array(
				'user_id' => $objectId,
				'domain' => $domain = cmsController::getInstance()->getCurrentDomain()->getCurrentHostName(),
				'activate_link' => getSelectedServerProtocol() . "://" . $domain . $module->pre_lang . "/users/activate/" . $activationCode . "/",
				'login' => $login,
				'password' => $password,
				'lname' => $object->getValue("lname"),
				'fname' => $object->getValue("fname"),
				'father_name' => $object->getValue("father_name"),
			);

			$mailContent = users::parseTemplateForMail($template_mail, $mailData, false, $objectId);
			$mailSubject = users::parseTemplateForMail($template_mail_subject, $mailData, false, $objectId);

			$fio = $object->getValue("lname") . " " . $object->getValue("fname") . " " . $object->getValue("father_name");

			$email_from = $regedit->getVal("//settings/email_from");
			$fio_from = $regedit->getVal("//settings/fio_from");

			$registrationMail = new umiMail();
			$registrationMail->addRecipient($email, $fio);
			$registrationMail->setFrom($email_from, $fio_from);
			$registrationMail->setSubject($mailSubject);
			$registrationMail->setContent($mailContent);
			$registrationMail->commit();
			$registrationMail->send();

			$oEventPoint = new umiEventPoint("users_registrate");
			$oEventPoint->setMode("after");
			$oEventPoint->setParam("user_id", $objectId);
			$oEventPoint->setParam("login", $login);
			users::setEventPoint($oEventPoint);

			if ($without_act) {
				$module->redirect($module->pre_lang . "/users/registrate_done/?result=without_activation");
			} else {
				$module->redirect($module->pre_lang . "/users/registrate_done/");
			}
		}

		/**
		 * Запускает активацию пользователя и возвращает ее результат
		 * @param string $template имя шаблона для tpl шаблонизатора
		 * @return mixed
		 */
		public function activate($template = "default") {
			$module = $this->module;

			if ($module->is_auth()) {
				$module->redirect("/");
			}

			$activate_code = (string) getRequest('param0');
			$isCorrect = false;

			if (!$activate_code || strlen($activate_code) != 32) {
				return $this->getActivateResult($template, $isCorrect);
			}

			$result = $module->getUserByActivationCode($activate_code);

			if ($result) {
				list($user_id) = $result;
				$module->activateUser($user_id);
				$isCorrect = true;
			}

			return $this->getActivateResult($template, $isCorrect);
		}

		/**
		 * Отправляет письмо с кодом активации для восстановления пароля
		 * @param string $template имя шаблона для tpl шаблонизатора
		 * @return mixed
		 * @throws coreException
		 * @throws errorPanicException
		 * @throws privateException
		 * @throws publicException
		 * @throws selectorException
		 */
		public function forget_do($template = "default") {
			$module = $this->module;

			static $macrosResult;

			if ($macrosResult) {
				return $macrosResult;
			}

			$forget_login = (string) getRequest('forget_login');
			$forget_email = (string) getRequest('forget_email');

			$hasLogin = strlen($forget_login) != 0;
			$hasEmail = strlen($forget_email) != 0;

			$user_id = false;

			list($template_mail_verification, $template_mail_verification_subject) = users::loadTemplatesForMail(
				"users/forget/" . $template,
				"mail_verification",
				"mail_verification_subject"
			);

			if ($hasLogin || $hasEmail) {
				$sel = new selector('objects');
				$sel->types('object-type')->name('users', 'user');
				if($hasLogin) $sel->where('login')->equals($forget_login);
				if($hasEmail) $sel->where('e-mail')->equals($forget_email);
				$sel->limit(0, 1);

				$user_id = ($sel->first) ? $sel->first->id : false;
			}

			if ($user_id) {
				$activate_code = md5(users::getRandomPassword());

				$object = umiObjectsCollection::getInstance()->getObject($user_id);

				$regedit = regedit::getInstance();
				$without_act = (bool) $regedit->getVal("//modules/users/without_act");

				if ($without_act || intval($object->getValue('is_activated'))) {
					$object->setValue("activate_code", $activate_code);
					$object->commit();

					$email = $object->getValue("e-mail");
					$fio = $object->getValue("lname") . " " . $object->getValue("fname") . " " . $object->getValue("father_name");

					$email_from = $regedit->getVal("//settings/email_from");
					$fio_from = $regedit->getVal("//settings/fio_from");

					$mail_arr = Array();
					$mail_arr['domain'] = $domain = $_SERVER['HTTP_HOST'];
					$mail_arr['restore_link'] = getSelectedServerProtocol() . "://" .
						$domain . $module->pre_lang . "/users/restore/" . $activate_code . "/";

					$mail_arr['login'] = $object->getValue('login');
					$mail_arr['email'] = $object->getValue('e-mail');

					$mail_subject = users::parseTemplateForMail($template_mail_verification_subject, $mail_arr, false, $user_id);
					$mail_content = users::parseTemplateForMail($template_mail_verification, $mail_arr, false, $user_id);

					$someMail = new umiMail();
					$someMail->addRecipient($email, $fio);
					$someMail->setFrom($email_from, $fio_from);
					$someMail->setSubject($mail_subject);
					$someMail->setPriorityLevel("highest");
					$someMail->setContent($mail_content);
					$someMail->commit();
					$someMail->send();

					$oEventPoint = new umiEventPoint("users_restore_password");
					$oEventPoint->setParam("user_id", $user_id);
					users::setEventPoint($oEventPoint);

					return $this->getForgetResult($template, true);
				} else {
					$referer_url = getServer('HTTP_REFERER');
					if (!strlen($referer_url)) {
						$referer_url = $module->pre_lang . "/users/forget/";
					}
					$module->errorRegisterFailPage($referer_url);
					$module->errorNewMessage("%errors_forget_nonactivated_login%");
					$module->errorPanic();

					return $this->getForgetResult($template, false, $forget_login, $forget_email);
				}
			} else {
				$referer_url = getServer('HTTP_REFERER');
				if (!strlen($referer_url)) {
					$referer_url = $module->pre_lang . "/users/forget/";
				}
				$module->errorRegisterFailPage($referer_url);

				if ($hasLogin && !$hasEmail) {
					$module->errorNewMessage("%errors_forget_wrong_login%");
				}
				if ($hasEmail && !$hasLogin) {
					$module->errorNewMessage("%errors_forget_wrong_email%");
				}
				if (($hasEmail && $hasLogin) || (!$hasEmail && !$hasLogin)) {
					$module->errorNewMessage("%errors_forget_wrong_person%");
				}
				$module->errorPanic();
				return $macrosResult = $this->getForgetResult($template, false, $forget_login, $forget_email);
			}
		}

		/**
		 * Восстанавливает доступ пользователя и отправляет ему
		 * письмо с данными для доступа
		 * @param bool|string $activate_code код активации
		 * @param string $template имя шаблона для tpl шаблонизатора
		 * @return mixed
		 * @throws coreException
		 * @throws selectorException
		 */
		public function restore($activate_code = false, $template = "default") {
			static $result = Array();

			if (isset($result[$template])) {
				return $result[$template];
			}

			list(
				$template_restore_failed_block,
				$template_restore_ok_block,
				$template_mail_password,
				$template_mail_password_subject
				) = users::loadTemplatesForMail(
				"users/forget/" . $template,
				"restore_failed_block",
				"restore_ok_block",
				"mail_password",
				"mail_password_subject"
			);

			if (!$activate_code) {
				$activate_code = (string) getRequest('param0');
				$activate_code = trim($activate_code);
			}

			$sel = new selector('objects');
			$sel->types('object-type')->name('users', 'user');
			$sel->where('activate_code')->equals($activate_code);
			$sel->limit(0, 1);

			if ($sel->first instanceof iUmiObject) {
				/**
				 * @var iUmiObject|iUmiEntinty $object
				 */
				$object = $sel->first;
				$user_id = $object->getId();
			} else {
				$object = false;
				$user_id = false;
			}

			if ($user_id && $activate_code) {
				$password = users::getRandomPassword();

				$login = $object->getValue("login");
				$email = $object->getValue("e-mail");
				$fio  = $object->getValue("lname") . " " . $object->getValue("fname") . " " . $object->getValue("father_name");

				$object->setValue("password", md5($password));
				$object->setValue("activate_code", "");
				$object->commit();

				$umiRegistry = regedit::getInstance();
				$email_from = $umiRegistry->getVal("//settings/email_from");
				$fio_from = $umiRegistry->getVal("//settings/fio_from");

				$mail_arr = Array();
				$mail_arr['domain'] = $domain = $_SERVER['HTTP_HOST'];
				$mail_arr['password'] = $password;
				$mail_arr['login'] = $login;

				$mail_subject = users::parseTemplateForMail($template_mail_password_subject, $mail_arr, false, $user_id);
				$mail_content = users::parseTemplateForMail($template_mail_password, $mail_arr, false, $user_id);

				$someMail = new umiMail();
				$someMail->addRecipient($email, $fio);
				$someMail->setFrom($email_from, $fio_from);
				$someMail->setSubject($mail_subject);
				$someMail->setContent($mail_content);
				$someMail->commit();
				$someMail->send();

				$eventPoint = new umiEventPoint('successfulPasswordRestoring');
				$eventPoint->setMode('after');
				$eventPoint->setParam('userId', $user_id);
				$eventPoint->call();

				return $result[$template] = $this->getRestoreResult($template, true, $login, $password, $user_id);
			}

			$block_arr['attribute:status'] = "fail";
			return $result[$template] = $this->getRestoreResult($template, false);
		}

	}
?>