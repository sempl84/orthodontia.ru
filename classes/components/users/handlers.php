<?php
	/**
	 * Класс обработчиков событий
	 */
	class UsersHandlers {
		/**
		 * @var users $module
		 */
		public $module;

		/**
		 * Обработчик события создания объекта через форму создания
		 * Запускает валидацию логина, пароля и почтового ящика
		 * @param iUmiEventPoint $e событие создания
		 * @throws coreException
		 */
		public function onCreateObject(iUmiEventPoint $e) {
			/**
			 * @var iUmiObject|iUmiEntinty $object
			 */
			$object = $e->getRef('object');
			$objectType = umiObjectTypesCollection::getInstance()->getType($object->getTypeId());
			if ($objectType->getModule() != 'users' || $objectType->getMethod() != 'user') {
				return;
			}

			if ($e->getMode() == "before") {

				if (!isset($_REQUEST['data']['new']['login'])) {
					$_REQUEST['data']['new']['login'] = $_REQUEST['name'];
				}

				$this->module->errorSetErrorPage($this->module->pre_lang . '/admin/users/add/user/');

				$_REQUEST['data']['new']['login'] = $this->module->validateLogin($_REQUEST['data']['new']['login']);
				$_REQUEST['data']['new']['password'][0] = $this->module->validatePassword($_REQUEST['data']['new']['password'][0], null, $_REQUEST['data']['new']['login']);
				$_REQUEST['data']['new']['e-mail'] = $this->module->validateEmail($_REQUEST['data']['new']['e-mail']);
				$object->setName($_REQUEST['data']['new']['login']);

				if ($this->module->errorHasErrors()) {
					if ($object instanceof umiObject) {
						umiObjectsCollection::getInstance()->delObject($object->getId());
					}
				}
				$this->module->errorThrow('admin');
			}
		}

		/**
		 * Обработчик события редактирования объекта через форму редактирования
		 * Запускает валидацию логина, пароля и почтового ящика
		 * @param umiEventPoint $e событие редактирования
		 * @throws coreException
		 */
		public function onModifyObject(umiEventPoint $e) {
			static $orig_groups = Array();
			/**
			 * @var iUmiObject|iUmiEntinty $object
			 */
			$object = $e->getRef('object');
			$objectId = $object->getId();
			$objectType = umiObjectTypesCollection::getInstance()->getType($object->getTypeId());

			if ($objectType->getModule() != 'users' || $objectType->getMethod() != 'user') {
				return;
			}

			if ($e->getMode() == "before") {
				$orig_groups[$objectId] = $object->getValue('groups');

				if (!isset($_REQUEST['data'][$objectId]['login'])) {
					$_REQUEST['data'][$objectId]['login'] = $_REQUEST['name'];
				}

				$this->module->errorSetErrorPage($this->module->pre_lang . "/admin/users/edit/{$objectId}/");

				$_REQUEST['data'][$objectId]['login'] = $this->module->validateLogin($_REQUEST['data'][$objectId]['login'], $objectId);
				if (isset($_REQUEST['data'][$objectId]['password'][0]) && trim($_REQUEST['data'][$objectId]['password'][0])) {
					$_REQUEST['data'][$objectId]['password'][0] = $this->module->validatePassword($_REQUEST['data'][$objectId]['password'][0], null, $_REQUEST['data'][$objectId]['login']);
				}
				$_REQUEST['data'][$objectId]['e-mail'] = $this->module->validateEmail($_REQUEST['data'][$objectId]['e-mail'], $objectId);
				$object->setName($_REQUEST['data'][$objectId]['login']);

				$this->module->errorThrow('admin');
			}

			if ($e->getMode() == 'after') {
				$permissions = permissionsCollection::getInstance();
				$is_sv = $permissions->isSv($permissions->getUserId());

				if ($objectId == SV_USER_ID) {
					$object->setValue('groups', Array(SV_GROUP_ID));
				} else {
					$groups = $object->getValue('groups');
					if (!$is_sv) {
						if (in_array(SV_GROUP_ID, $groups) && !in_array(SV_GROUP_ID, $orig_groups[$objectId])) {
							unset($groups[array_search(SV_GROUP_ID, $groups)]);
							$object->setValue('groups', $groups);
						} else if (!in_array(SV_GROUP_ID, $groups) && in_array(SV_GROUP_ID, $orig_groups[$objectId])){
							$groups[] = SV_GROUP_ID;
							$object->setValue('groups', $groups);
						}
					}
				}
				$object->commit();
			}
		}

		/**
		 * Обработчик события редактирования поля объекта через табличный контрол
		 * Запускает валидацию логина или почтового ящика
		 * @param umiEventPoint $e событие редактирования
		 * @throws coreException
		 */
		public function onModifyPropertyValue(umiEventPoint $e) {
			$object = $e->getRef('entity');

			if ($object instanceof umiHierarchyElement) {
				/**
				 * @var iUmiObject|iUmiEntinty
				 */
				$object = $object->getObject();
			}

			$objectId = $object->getId();
			$objectType = umiObjectTypesCollection::getInstance()->getType($object->getTypeId());

			if (!$objectType || $objectType->getModule() != 'users' || $objectType->getMethod() != 'user') {
				return;
			}

			if ($e->getMode() == "before") {
				$newValue = &$e->getRef('newValue');

				switch ((string) $e->getParam('property')) {
					case 'name' : {
						$newValue = $this->module->validateLogin($newValue, $objectId);
						break;
					}

					case 'e-mail' : {
						$newValue = $this->module->validateEmail($newValue, $objectId);
						break;
					}

					default:
						return;
				}

				$this->module->errorThrow('xml');
			}

			if ($e->getMode() == "after") {
				switch ((string) $e->getParam('property')) {
					case 'login' : {
						$object->setName((string) $e->getParam('newValue'));
						$object->commit();
						break;
					}

					case 'name' : {
						$object->setValue('login', (string) $e->getParam('newValue'));
						$object->commit();
						break;
					}

					default:
						return;
				}
			}
		}


		/**
		 * Обработчик регистрации пользователя и изменения его настроек
		 * Запускает загрузку изображения для аватара, создание аватара и назначение
		 * его пользователю
		 * @param iUmiEventPoint $oEventPoint событие регистрации или изменения настроек
		 * @return bool|void
		 * @throws coreException
		 */
		public function onAutoCreateAvatar(iUmiEventPoint $oEventPoint) {
			$user_id = $oEventPoint->getParam("user_id");
			$avatar_type_id = $object_type_id = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName("users", "avatar");

			if ($oEventPoint->getMode() != "after") {
				return;
			}

			if ($image = umiImageFile::upload("avatar", "user_avatar_file", "./images/cms/data/picture/")) {
				$avatar_id = umiObjectsCollection::getInstance()->addObject("Avatar for user {$user_id}", $avatar_type_id);

				$avatar = umiObjectsCollection::getInstance()->getObject($avatar_id);
				$avatar->setValue("picture", $image);
				$avatar->setValue("is_hidden", true);
				$avatar->commit();

				$user = umiObjectsCollection::getInstance()->getObject($user_id);
				$user->setValue("avatar", $avatar_id);
				$user->commit();

				return true;
			}

			return false;
		}

		/**
		 * Обработчик регистрации пользователя
		 * Отправлят письмо-уведомление о регистрации администратору
		 * @param iUmiEventPoint $oEventPoint событие регистрации
		 */
		public function onRegisterAdminMail(iUmiEventPoint $oEventPoint) {
			$regedit = regedit::getInstance();
			$template = "default";

			if ($oEventPoint->getMode() == "after") {
				list($template_mail, $template_mail_subject) = users::loadTemplatesForMail("users/register/".$template, "mail_admin_registrated", "mail_admin_registrated_subject");

				$email_to = $regedit->getVal("//settings/admin_email");
				$email_from = $regedit->getVal("//settings/email_from");
				$fio_from = $regedit->getVal("//settings/fio_from");

				$object_id = $oEventPoint->getParam('user_id');
				$login = $oEventPoint->getParam('login');

				$mail_arr = Array();
				$mail_arr['user_id'] = $object_id;
				if ($login) {
					$mail_arr['login'] = $login;
				}
				$mail_subject = users::parseTemplateForMail($template_mail_subject, $mail_arr, false, $object_id);
				$mail_content = users::parseTemplateForMail($template_mail, $mail_arr, false, $object_id);

				$someMail = new umiMail();
				$someMail->addRecipient($email_to, $fio_from);
				$someMail->setFrom($email_from, $fio_from);
				$someMail->setSubject($mail_subject);
				$someMail->setContent($mail_content);
				$someMail->commit();
				$someMail->send();
			}
		}

		/**
		 * Обработчик события создания поста или сообщения модуля "Форум"
		 * Изменяет страницы, на которые подписан пользователь
		 * @param umiEventPoint $e события создания поста или сообщения
		 * @return bool
		 */
		public function onSubscribeChanges(umiEventPoint $e) {
			static $is_called;

			if ($is_called === true) {
				return true;
			}

			$mode = (bool) getRequest('subscribe_changes');

			if ($user_id = $this->module->user_id) {
				$user = umiObjectsCollection::getInstance()->getObject($user_id);
				if ($user instanceof umiObject) {
					$topic_id = $e->getParam("topic_id");
					$subscribed_pages = $user->getValue("subscribed_pages");

					if ($mode) {
						$topic = umiHierarchy::getInstance()->getElement($topic_id);
						if ($topic instanceof umiHierarchyElement) {
							if (!in_array($topic, $subscribed_pages)) {
								$subscribed_pages[] = $topic_id;
							}
						}
					} else {
						$tmp = Array();

						if(!is_array($subscribed_pages)) {
							$subscribed_pages = Array();
						}

						/**
						 * @var iUmiEntinty $page
						 */
						foreach($subscribed_pages as $page) {
							if ($page->getId() != $topic_id) {
								$tmp[] = $page;
							}
						}
						$subscribed_pages = $tmp;
						unset($tmp);
					}

					$user->setValue("subscribed_pages", $subscribed_pages);
					$user->commit();

					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}

		/**
		 * Проверяет тестовое событие
		 * @param iUmiEventPoint $event тестовое событие
		 */
		public function checkMessage(iUmiEventPoint $event) {
			$dummyId = $event->getRef('id');
			$dummy = new umiMessage($dummyId);
			$dummy = $dummy->getContent();
			$umiRegistry = regedit::getInstance();
			$umiRegistry->doTesting($dummy);
			$umiMessages = umiMessages::getInstance();
			$umiMessages->dropTestMessages();
		}
	}
?>