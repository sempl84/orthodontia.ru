<?php
	/**
	 * Класс интеграции с Google Picasa desktop client.
	 */
	class GooglePicasaPhotoAlbum {
		/**
		 * @var photoalbum $module
		 */
		public $module;

		/**
		 * Возвращает rss от Google Picasa
		 * @return bool|mixed|null
		 */
		public function getRSS() {
			$rss = getRequest('rss');
			$_SESSION['picasa_rss'] = getRequest('rss');

			if (!$rss) {
				$cacheDirectory = mainConfiguration::getInstance()->includeParam('system.runtime-cache');
				if (is_file($cacheDirectory . "picasa")) {
					$rss = unserialize(file_get_contents($cacheDirectory . "picasa"));
				}
			}

			return $rss;
		}

		/**
		 * Возвращает путь до директории, куда будут импортироваться изображения.
		 * @return array
		 */
		public function getFolder() {
			$targetFolder = (string) getRequest('folder-name');
			$targetFolder = USER_IMAGES_PATH . "/cms/data/" . $targetFolder;
			umiDirectory::requireFolder($targetFolder, USER_IMAGES_PATH . "/cms/data/");

			return Array(
				'target-folder' => $targetFolder
			);
		}

		/**
		 * Обрабатывает запросы от Google Picasa.
		 * Создает фотоальбомы и фотографии.
		 * @throws publicAdminException
		 */
		public function import() {
			global $_FILES;
			header("Content-type: text/html");
			$targetFolder = USER_IMAGES_PATH . "/cms/data/picasa/";

			switch ($action = getRequest('action-mode')) {
				case "new": {
					$title = (string) getRequest('new-title');
					$body = (string) getRequest('new-body');
					$elementId = (int) $this->addNewPicasaPhotoalbum($title, $body);
					break;
				}
				case "add": {
					$elementId = (int) getRequest('photoalbum-id');
					break;
				}
				case "put": {
					$elementId = false;
					$targetFolder = (string) getRequest('folder-name');
					break;
				}
				default: {
					throw new publicAdminException("Unknown action \"{$action}\"");
				}
			}
			if ($elementId) {
				/**
				 * @var iUmiHierarchyElement|iUmiEntinty $element
				 */
				$element = selector::get('page')->id($elementId);
				$targetFolder = $this->module->_checkFolder($element);
			}

			if (!is_dir($targetFolder)) {
				throw new publicAdminException("Folder \"{$targetFolder}\" doesn't exist");
			}

			$titles = getRequest('title');

			if (defined('CURRENT_VERSION_LINE') && CURRENT_VERSION_LINE == 'demo') {
				throw new publicAdminException("Forbidden in demo-mode");
			}

			$i = 0;

			foreach($_FILES as $key => $info) {

				if ($info['error']) {
					continue;
				}

				$name = $info['name'];
				$title = $titles[$i++];
				$file = umiFile::manualUpload($name, $info['tmp_name'], $info['size'], $targetFolder);

				if ($elementId && $file instanceof umiFile) {
					if ($file->getIsBroken()) {
						throw new publicAdminException("Image is broken");
					}

					$this->addPicasaPhoto($elementId, $info['name'], $title, $file);
				}
			}

			switch($action) {
				case "put": {
					$this->putFile();
					break;
				}
				default: {
					$this->defaultAction($elementId);
				}
			}
		}

		/**
		 * Выводит служебные адрес в буффер
		 */
		public function putFile() {
			header("Content-type: text/plain");
			$folderName = (string) getRequest('folder-name');
			echo getSelectedServerProtocol() . "://" . getServer('HTTP_HOST') . "/admin/photoalbum/picasa/files/?folder-name=" . $folderName;
			exit();
		}

		/**
		 * Если задан $elementId, то выводит путь до страницы в буффер,
		 * иначе прерывает выполенение скрипта.
		 * @param bool|int $elementId идентификатор страницы
		 */
		public function defaultAction($elementId) {
			header("Content-type: text/plain");

			if ($elementId) {
				$umiHierarchy = umiHierarchy::getInstance();
				$force = $umiHierarchy->forceAbsolutePath(true);
				$link = $umiHierarchy->getPathById($elementId);
				echo $link;
				$umiHierarchy->forceAbsolutePath($force);
			}

			exit();
		}

		/**
		 * Создает фотографию
		 * @param int $parentId идентификатор фотоальбома
		 * @param string $name имя файла
		 * @param string $title имя фотографии
		 * @param umiFile $file изображение
		 * @throws coreException
		 */
		public function addPicasaPhoto($parentId, $name, $title, umiFile $file) {
			$hierarchy = umiHierarchy::getInstance();
			$hierarchyTypes = umiHierarchyTypesCollection::getInstance();
			$objectTypes = umiObjectTypesCollection::getInstance();
			
			$object_type_id = (int) getRequest('param1');
			$title = htmlspecialchars(trim($title));
			
			$parentElement = $hierarchy->getElement($parentId);
			$tpl_id = $parentElement->getTplId();
			$domain_id = $parentElement->getDomainId();
			$lang_id = $parentElement->getLangId();
			
			$hierarchy_type_id = $hierarchyTypes->getTypeByName("photoalbum", "photo")->getId();
			if (!$object_type_id) {
				$object_type_id = $objectTypes->getTypeIdByHierarchyTypeName("photoalbum", "photo");
			}
			
			$object_type = $objectTypes->getType($object_type_id);

			if ($object_type->getHierarchyTypeId() != $hierarchy_type_id) {
				$this->module->errorNewMessage("Object type and hierarchy type doesn't match");
				$this->module->errorPanic();
			}

			$element_id = $hierarchy->addElement(
				$parentId,
				$hierarchy_type_id,
				$title,
				$title,
				$object_type_id,
				$domain_id,
				$lang_id,
				$tpl_id
			);

			permissionsCollection::getInstance()->setDefaultPermissions($element_id);
			$element = $hierarchy->getElement($element_id, true);
			
			$element->setIsActive(true);
			$element->setIsVisible(false);
			$element->setName($title);
			$element->setValue("h1", $title);
			$element->setValue("photo", $file);
			$element->setValue("create_time", time());
			
			$element->commit();
			$parentElement->setUpdateTime(time());
			$parentElement->commit();
		}

		/**
		 * Создает фотоальбомы и возвращает его идентификатор
		 * @param string $title имя фотоальбома
		 * @param string $body описание фотоальбома
		 * @return int
		 * @throws coreException
		 */
		public function addNewPicasaPhotoalbum($title, $body) {
			$hierarchy = umiHierarchy::getInstance();
			$hierarchyTypes = umiHierarchyTypesCollection::getInstance();
			$hierarchy_type_id = $hierarchyTypes->getTypeByName("photoalbum", "album")->getId();

			$element_id = $hierarchy->addElement(0, $hierarchy_type_id, $title, $title);
			permissionsCollection::getInstance()->setDefaultPermissions($element_id);
			$element = $hierarchy->getElement($element_id, true);
			
			$element->setIsActive(true);
			$element->setIsVisible(false);
			$element->setName($title);
			$element->setValue("h1", $title);
			$element->setValue("descr", $body);
			$element->setValue("create_time", time());
			$element->commit();
			
			return $element_id;
		}

		/**
		 * Возвращает ссылку на файл с конфигом для плагина UMI.CMS под Google Picasa
		 * @return string
		 */
		public function getPicasaLink() {
			$cmsController = cmsController::getInstance();
			$domain = $cmsController->getCurrentDomain()->getHost();
			return getSelectedServerProtocol() . '://' . $domain . '/classes/modules/photoalbum/picasa-button/umicms-' . sha1($domain) . '.pbz';
		}

		/**
		 * Возвращает zip архив с плагином UMI.CMS под Google Picasa
		 * @return array
		 */
		public function generatePicasaButton() {
			$config = mainConfiguration::getInstance();
			$sourceDir = SYS_MODULES_PATH . 'photoalbum/picasa-button/';
			$tempDir = $config->includeParam('system.runtime-cache') . 'picasa-button/';
			
			$cmsController = cmsController::getInstance();
			$domain = $cmsController->getCurrentDomain()->getHost();
			
			$rand = sha1($domain);
			$pbzFilename = 'umicms-' . $rand;
			$pbzPath = $sourceDir . $pbzFilename . '.pbz';
			
			if (is_file($pbzPath)) {
				return true;
			}
			
			if (!is_writable($sourceDir) || !is_writable($config->includeParam('system.runtime-cache'))) {
				return false;
			}
			
			$uuid = uuid();
			
			if (!is_dir($tempDir)) {
				mkdir($tempDir, 0777);
			}
			
			$pbfPath = $tempDir . '{' . $uuid . '}.pbf';
			$psdPath = $tempDir . '{' . $uuid . '}.psd';
			copy($sourceDir . 'pbf.orign', $pbfPath);
			copy($sourceDir . 'icon.psd', $psdPath);
			
			$cont = file_get_contents($pbfPath);
			$cont = str_replace('{domain}', $domain, $cont);
			$cont = str_replace('{uuid}', $uuid, $cont);
			$cont = str_replace('{filename}', $pbzFilename, $cont);
			file_put_contents($pbfPath, $cont);
			
			$files = array($pbfPath, $psdPath);
			$zip = new UmiZipArchive($pbzPath);
			$result = $zip->create($files, $tempDir);
			
			unlink($pbfPath);
			unlink($psdPath);
			rmdir($tempDir);
			
			return $result;
		}
	};
?>