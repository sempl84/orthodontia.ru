<?php
	/**
	 * Базовый класс модуля "Меню".
	 *
	 * Модуль отвечает за:
	 *
	 * 1) Предоставление интерфейса для создания и редактирования меню из произвольных элементов;
	 * 2) Актуализацию содержимого меню;
	 * 3) Вывод данных для шаблонизации меню;
	 * @link http://help.docs.umi-cms.ru/rabota_s_modulyami/modul_menu/
	 */
	class menu extends def_module {

		/**
		 * Конструктор
		 */
		public function __construct() {
			parent::__construct();
			$cmsController = cmsController::getInstance();

			if ($cmsController->getCurrentMode() == "admin") {
				$configTabs = $this->getConfigTabs();

				if ($configTabs) {
					$configTabs->add("config");
				}

				$this->__loadLib("admin.php");
				$this->__implement("MenuAdmin");

				$this->loadAdminExtension();

				$this->__loadLib("customAdmin.php");
				$this->__implement("MenuCustomAdmin", true);
			} else {
				$this->__loadLib("macros.php");
				$this->__implement("MenuMacros");

				$this->loadSiteExtension();

				$this->__loadLib("customMacros.php");
				$this->__implement("MenuCustomMacros", true);
			}

			$this->__loadLib("handlers.php");
			$this->__implement("MenuHandlers");

			$this->loadCommonExtension();
			$this->loadTemplateCustoms();

			$this->__loadLib("customCommon.php");
			$this->__implement("MenuCustomCommon", true);
		}

		/**
		 * Возвращает ссылку на страницу редактирования меню
		 * @param int $objectId идентификатор меню
		 * @param bool $type контрольный параметр
		 * @return string
		 */
		public function getObjectEditLink($objectId, $type = false) {
			/* @var MenuMacros|MenuAdmin $this */
			return $this->getEditLink($objectId, $type = false);
		}

		/**
		 * Возращает суффикс для адресов системных страниц
		 * @return string
		 */
		public function getUrlSuffix() {
			$suffix = '';
			$config = mainConfiguration::getInstance();

			if ($config->get('seo', 'url-suffix.add')) {
				$suffix = (string) $config->get('seo', 'url-suffix');
			}

			return $suffix;
		}

		/**
		 * Возвращает список меню
		 * @return array|bool
		 * @throws selectorException
		 */
		public function getListMenu(){
			static $cache = null;

			if (!is_null($cache)) {
				return $cache;
			}

			$sel = new selector('objects');
			$sel->types('object-type')->name('menu', 'item_element');

			if (!$sel->length()) {
				return $cache = false;
			}

			return $cache = $sel->result();
		}

		/**
		 * Актуализует меню
		 * @param bool|array|object $values меню в json
		 * @return array|bool
		 */
		public function editLinkMenu($values = false){

			if (!is_array($values) && !is_object($values)) {
				return $values;
			}

			$values = (is_array($values)) ? $values : (array) $values;
			$hierarchy = umiHierarchy::getInstance();

			foreach ($values as $key => $value){
				$rel = $value->rel;
				$children = (!empty($value->children)) ? $value->children : false;

				if ($rel!='custom' && $rel!='system'){
					$link = $hierarchy->getPathById($rel);
					$element = $hierarchy->getElement($rel);

					if (!$link || !($element instanceof iUmiHierarchyElement)) {
						unset($values[$key]);
						continue;
					}

					$value->link = $link;
					$element = $hierarchy->getElement($rel);
					$value->isactive = (int) $element->getIsActive();
					$value->isdeleted = (int) $element->getIsDeleted();
					$hierarchy->unloadElement($rel);
				}

				if ($children){
					$this->editLinkMenu($children);
				}
			}

			return $values;
		}

		/**
		 * {@inheritdoc}
		 */
		protected function loadElements($menuHierarchy, $needProps = false, $hierarchyTypeId = false) {
			$elementIds = array();

			foreach ($menuHierarchy as $element) {
				$ids = $this->collectIds($element);
				if (is_array($ids)) {
					$elementIds = array_merge($elementIds, $ids);
				}
			}

			$elementIds = array_unique($elementIds);

			if (count($elementIds) == 0) {
				return false;
			}

			parent::loadElements($elementIds);

			return true;
		}

		/**
		 * Возвращает идентификаторы
		 * страниц из ветви меню в json формате
		 * @param stdClass $element ветвь меню в json формате
		 * @return array|bool
		 */
		private function collectIds($element) {
			$elementIds = array();

			if (isset($element->rel) && is_numeric($element->rel)) {
				$elementIds[] = $element->rel;
			}

			if (isset($element->children)) {
				foreach ($element->children as $children) {
					$ids = $this->collectIds($children);
					if (is_array($ids)) {
						$elementIds = array_merge($elementIds, $ids);
					}
				}
			}

			if (count($elementIds) > 0) {
				return $elementIds;
			}

			return false;
		}

		/**
		 * Возвращает ссылку на страницу с формой редактирования меню
		 * @param int $objectId идентификатор меню
		 * @return string
		 */
		public function getEditLink($objectId) {
			return $this->pre_lang . "/admin/menu/edit/" . $objectId . "/";
		}
	}
?>