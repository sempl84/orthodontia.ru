<?php
	/**
	 * Класс макросов, то есть методов, доступных в шаблоне
	 */
	class MenuMacros {
		/**
		 * @var menu $module
		 */
		public $module;

		/**
		 * Возвращает данные меню
		 * @param null|int $menuId идентификатор меню
		 * @param string $template имя шаблона (для tpl)
		 * @return mixed
		 * @throws publicException
		 * @throws selectorException
		 */
		public function draw($menuId = NULL, $template = "default") {

			if ($menuId === false) {
				throw new publicException(getLabel('error-object-does-not-exist', null, $menuId));
			}

			$menu = null;
			$umiObjectsCollection = umiObjectsCollection::getInstance();

			if (!is_numeric($menuId)) {
				$selector = new selector('objects');
				$selector->types('object-type')->name('menu', 'item_element');
				$selector->where("menu_id")->equals($menuId);
				$selector->option('no-length')->value(true);
				$selector->limit(0, 1);

				if (!$selector->first) {
					throw new publicException(getLabel('error-object-does-not-exist', null, $menuId));
				}

				$menu = $selector->first;
			} else {
				$menu = $umiObjectsCollection->getObject($menuId);
			}

			if (!$menu instanceof umiObject) {
				throw new publicException(getLabel('error-object-does-not-exist', null, $menuId));
			}

			if (!$template) {
				$template = "default";
			}

			$templates = menu::loadTemplates("menu/" . $template);
			$menuHierarchy = $menu->getValue('menuhierarchy');

			if (!$menuHierarchy) {
				throw new publicException(getLabel('error-prop-not-found', null, 'menuHierarchy'));
			}

			$menuHierarchyArray = json_decode($menuHierarchy);
			$currentElementId = cmsController::getInstance()->getCurrentElementId();
			$allParents = umiHierarchy::getInstance()->getAllParents($currentElementId, true);
			$this->module->loadElements((is_array($menuHierarchyArray)) ? $menuHierarchyArray : (array) $menuHierarchyArray);

			return $this->drawLevel($menuHierarchyArray, $templates, 0, $allParents);
		}

		/**
		 * Возвращает данные одного уровня меню.
		 * Вызывается рекурсивно.
		 * @param null|stdClass[] $arr меню в формате json
		 * @param mixed $templates блоки шаблона (для tpl)
		 * @param int $curr_depth текущий уровень вложенности меню
		 * @param array $allParents массив идентификаторов страниц, родительских для текущей
		 * @return mixed|void
		 */
		public function drawLevel($arr = NULL, $templates, $curr_depth = 0, $allParents) {
			if (!$arr) {
				return;
			}
			list(
				$template_block,
				$template_line,
				$template_line_a,
				$template_line_in,
				$separator,
				$separator_last,
				$class,
				$class_last
				) = $this->getMenuTemplates(
				$templates,
				($curr_depth + 1)
			);
			$lines = Array();
			$c = 0;
			$sz = sizeof($arr);
			$umiHierarchy = umiHierarchy::getInstance();
			$umiLinksHelper = umiLinksHelper::getInstance();

			/**
			 * @var stdClass $menuItem
			 */
			foreach ($arr as $menuItem){
				$id = null;
				$rel = $menuItem->rel;
				$is_active = $is_page = false;
				$line_arr = Array();
				$line_arr['attribute:rel'] = $rel;
				$line_arr['attribute:id'] = $line_arr['void:sub_menu'] = '';

				if (is_numeric($rel)){
					$is_page = true;
					$page = $umiHierarchy->getElement($rel);

					if (!$page instanceof umiHierarchyElement) {
						continue;
					}

					$link = $umiLinksHelper->getLinkByParts($page);
					$is_active = (in_array($rel, $allParents) !== false);
					$line_arr['attribute:rel'] = 'page';
					$line_arr['attribute:id'] = $id = $rel;
					$line_arr['attribute:is-active'] = isset($menuItem->isactive) ? $menuItem->isactive : 0;
					$line_arr['attribute:is-deleted'] = isset($menuItem->isdeleted) ? $menuItem->isdeleted : 0;
				} else {
					$link = $menuItem->link;
				}

				$line_arr['attribute:link'] = $link;
				$line_arr['attribute:name'] = $menuItem->name;
				$line_arr['node:text'] = $menuItem->name;

				$line_arr['void:num'] = ($c+1);
				$line_arr['void:separator'] = (($sz == ($c + 1)) && $separator_last) ? $separator_last : $separator;
				$line_arr['void:class'] = ($sz > ($c + 1)) ? $class : $class_last;

				if ($is_active) {
					$line_arr['attribute:status'] = "active";
				}

				$line = ($is_active) ? $template_line_a : $template_line;
				$childsArr = NULL;

				if (isset($menuItem->children)) {
					$childsArr = $menuItem->children;
				}

				if ($childsArr) {
					$line_arr['items'] = $line_arr['void:sub_menu'] = $this->drawLevel($childsArr, $templates, $curr_depth + 1, $allParents);
				}

				$c++;

				$lines[] = ($is_page) ? menu::parseTemplate($line, $line_arr, $id) : menu::parseTemplate($line, $line_arr);
			}

			$block_arr = array(
				'nodes:item'	=> $lines,
				'void:lines'	=> $lines
			);

			return menu::parseTemplate($template_block, $block_arr);
		}

		/**
		 * Алиас метода ContentMenu::getMenuTemplates()
		 * @param $templates
		 * @param $curr_depth
		 * @return array
		 */
		public function getMenuTemplates($templates, $curr_depth) {
			static $content;

			if (!$content instanceof content) {
				/**
				 * @var content|ContentMenu $content
				 */
				$content = cmsController::getInstance()->getModule('content');
			}

			return $content->getMenuTemplates($templates, $curr_depth);
		}
	}
?>