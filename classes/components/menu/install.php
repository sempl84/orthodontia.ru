<?php
	/**
	 * Установщик модуля
	 */

	/**
	 * @var array $INFO реестр модуля
	 */
	$INFO = Array();
	$INFO['name'] = "menu";
	$INFO['config'] = "0";
	$INFO['default_method'] = "empty";
	$INFO['default_method_admin'] = "lists";
	$INFO['per_page'] = "10";
	$INFO['func_perms'] = "Группы прав на функционал модуля";
	$INFO['func_perms/view'] = "Права на просмотр меню";
	$INFO['func_perms/lists'] = "Права на администрирование модуля";

	/**
	 * @var array $COMPONENTS файлы модуля
	 */
	$COMPONENTS = array();
	$COMPONENTS[] = "./classes/components/menu/admin.php";
	$COMPONENTS[] = "./classes/components/menu/class.php";
	$COMPONENTS[] = "./classes/components/menu/customAdmin.php";
	$COMPONENTS[] = "./classes/components/menu/customCommon.php";
	$COMPONENTS[] = "./classes/components/menu/customMacros.php";
	$COMPONENTS[] = "./classes/components/menu/events.php";
	$COMPONENTS[] = "./classes/components/menu/handlers.php";
	$COMPONENTS[] = "./classes/components/menu/i18n.en.php";
	$COMPONENTS[] = "./classes/components/menu/i18n.php";
	$COMPONENTS[] = "./classes/components/menu/includes.php";
	$COMPONENTS[] = "./classes/components/menu/install.php";
	$COMPONENTS[] = "./classes/components/menu/lang.en.php";
	$COMPONENTS[] = "./classes/components/menu/lang.php";
	$COMPONENTS[] = "./classes/components/menu/macros.php";
	$COMPONENTS[] = "./classes/components/menu/permissions.php";
?>