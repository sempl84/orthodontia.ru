<?php
	/**
	 * Класс функционала административной панели модуля
	 */
	class BackupAdmin {

		use baseModuleAdmin;
		/**
		 * @var backup $module
		 */
		public $module;

		/**
		 * Возвращает список резервных копий файлов системы
		 * @throws coreException
		 */
		public function backup_copies() {
			$backupDir = str_replace(CURRENT_WORKING_DIR, '', SYS_MANIFEST_PATH) . 'backup/';

			$params = Array(
				"snapshots" => Array(
					"status:backup-directory" => $backupDir
				)
			);

			$ent = getRequest('ent');

			if (!$ent) {
				$ent = time();
				$this->module->redirect($this->module->pre_lang . '/admin/backup/backup_copies/?ent=' . $ent);
			}

			$this->setDataType("settings");
			$this->setActionType("modify");
			$data = $this->prepareData($params, "settings");
			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает данные вкладки "История изменений"
		 */
		public function snapshots(){
			$this->setDataType("list");
			$this->setActionType("view");
			$this->setData(array());
			$this->doData();
		}

		/**
		 * Возвращает настройки модуля.
		 * Если передан ключевой параметр $_REQUEST['param0'] = do,
		 * то метод запустит сохранение настроек.
		 * @throws coreException
		 */
		public function config(){
			$regedit = regedit::getInstance();

			$params = Array(
				"backup" => Array(
					"boolean:enabled"		=> null,
					"int:max_timelimit"		=> null,
					"int:max_save_actions"	=> null
				)
			);

			$mode = getRequest("param0");

			if ($mode == "do") {
				if (!is_demo()) {
					$params = $this->expectParams($params);
					$regedit->setVar("//modules/backup/enabled", $params['backup']['boolean:enabled']);
					$regedit->setVar("//modules/backup/max_timelimit", $params['backup']['int:max_timelimit']);
					$regedit->setVar("//modules/backup/max_save_actions", $params['backup']['int:max_save_actions']);
					$this->chooseRedirect();
				}
			}

			$params['backup']['boolean:enabled'] = $regedit->getVal("//modules/backup/enabled");
			$params['backup']['int:max_timelimit'] = $regedit->getVal("//modules/backup/max_timelimit");
			$params['backup']['int:max_save_actions'] = $regedit->getVal("//modules/backup/max_save_actions");

			$this->setDataType("settings");
			$this->setActionType("modify");
			$data = $this->prepareData($params, "settings");
			$this->setData($data);
			$this->doData();
		}
	}
?>