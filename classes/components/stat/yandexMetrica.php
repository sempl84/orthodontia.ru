<?php
	/**
	 * Клиент для сервиса Яндекс.Метрика
	 */
	class StatYandexMetrica {
		/**
		 * @var stat $module
		 */
		public $module;

		/**
		 * Делает запрос к сервису и возвращает результат
		 * @param string $request uri запроса
		 * @param string $method метод запроса (GET|POST|PUT etc)
		 * @param array $params переменные, передаваемые методом POST
		 * @param array $headers дополнительные HTTP-заголовки
		 * @return mixed
		 * @throws Exception
		 * @throws publicAdminException
		 * @throws umiRemoteFileGetterException
		 */
		public function get_response($request, $method = "GET", $params = array(), $headers = array()) {
			$apiUrl = 'https://api-metrika.yandex.ru/';
			$regedit = regedit::getInstance();
			$token = (string) trim ($regedit->getVal("//modules/stat/yandex-token"));

			if (!$token) {
				throw new publicAdminException(getLabel('label-error-no-token'));
			}

			$url = $apiUrl . $request . '?oauth_token=' . $token;

			if (count($params) && is_array($params)) {
				$url .= '&' . http_build_query($params);
				$params = array();
			}

			$response = umiRemoteFileGetter::get($url, false, $headers, $params, true, $method);
			$result = preg_split("|(\r\n\r\n)|", $response);
			$headers = $result[0];
			$xml = $result[1];

			if (strpos($headers, '200 OK') === false) {
				if (strpos($headers, '401 Unauthorized') !== false) {
					throw new publicAdminException(getLabel('label-error-no-token'));
				} else {
					throw new publicAdminException($xml);
				}
			}

			$xml = str_replace(' xmlns="http://api.yandex.ru/metrika/"', ' id="' . $request . '"', $xml);
			return $xml;
		}

		/**
		 * Возвращает данные счетчика в формате json
		 * @throws coreException
		 */
		public function view_counter_json() {
			$section = (string) getRequest('section');
			$report = (string) getRequest('report');
			$counterId = (int) getRequest('counter');
			$order = (string) getRequest('order');
			$date1 = (string) getRequest('date1');
			$date2 = (string) getRequest('date2');
			$section = trim($section, "_");

			$cacheDir = CURRENT_WORKING_DIR . '/sys-temp/metrika/';
			if (!is_dir($cacheDir)) {
				mkdir($cacheDir, 0777, true);
			}

			$key = md5($section . $report . $counterId . $date1 . $date2);
			$fileKey = md5($section . $report . $counterId);
			$cacheFile = $cacheDir . $fileKey;
			$filteredResponse = null;

			if (file_exists($cacheFile) && getSession('metrikaKey') == $key) {
				$filteredResponse = file_get_contents($cacheFile);
			} elseif(file_exists($cacheFile)) {
				unlink($cacheFile);
			}

			if (is_null($filteredResponse)) {
				$headers = array (
					"Content-type" => 'application/json',
					"Accept" => 'application/x-yametrika+json'
				);

				if ($section == "summary" && $report == "common") {
					return;
				}

				/**
				 * @var system $system
				 */
				$system = system_buildin_load('system');

				$request = 'stat/' . $section . '/' . $report . '.json';
				if ($section == 'geo') {
					$request = 'stat/' . $section . '.json';
				}

				$params = array(
					'id' => $counterId,
					'pretty' => 1
				);

				if ($date1) {
					$params['date1'] = $system->convertDate("", 'Ymd', $date1);
				}
				if ($date2) {
					$params['date2'] = $system->convertDate("", 'Ymd', $date2);
				}

				try {
					$response = $this->get_response($request, "GET", $params, $headers);
				} catch (Exception $e) {
					$error = $e->getMessage();
					$result = array(
						'errors' => array(
							0 => array(
								'text' => $error
							)
						)
					);

					$filteredResponse = json_encode($result);
					file_put_contents($cacheFile, $filteredResponse);
					$response = null;
				}

				if (is_null($filteredResponse)) {
					$jsonDecoded = json_decode($response, true);

					if (isset($jsonDecoded['date1'])) {
						$jsonDecoded['date1'] = $system->convertDate("", 'd.m.Y', $jsonDecoded['date1']);
					}
					if (isset($jsonDecoded['date2'])) {
						$jsonDecoded['date2'] = $system->convertDate("", 'd.m.Y', $jsonDecoded['date2']);
					}

					$mainArray = 'data';
					if ($report == 'deepness') {
						$mainArray = 'data_time' ;
					}

					if (isset($jsonDecoded[$mainArray])) {
						if ($order == 'date') {
							$jsonDecodedData = array();
							foreach ($jsonDecoded['data'] as $data) {
								$date = $data['date'];
								unset($data['date']);
								$jsonDecodedData[$date] = array_merge(array('date' => $system->convertDate("", 'd.m.Y', $date)), $data);
							}

							ksort($jsonDecodedData);
							$jsonDecoded['data'] = array_values($jsonDecodedData);
						} elseif ($section == 'geo') {
							$jsonDecodedData = array();
							$controller = cmsController::getInstance();
							$lang = $controller->getLang();
							$langPrefix = $lang->getPrefix();
							$countries = $this->getCountriesList();

							foreach ($jsonDecoded['data'] as $data) {
								$orderValue = $data['name'];
								if ($langPrefix == 'ru' && isset($countries[$data['name']])) {
									$orderValue =  $countries[$data['name']];
								}
								$jsonDecodedData[] = array_merge(array($order => $orderValue), $data);
							}

							$jsonDecoded['data'] = $jsonDecodedData;

						} else {

							$jsonDecodedData = array();
							foreach ($jsonDecoded[$mainArray] as $data) {
								$orderValue = $data[$order];
								if ($report == 'browsers') {
									$orderValue .= ' ' . $data['version'];
								}
								unset($data[$order]);
								$jsonDecodedData[] = array_merge(array($order => $orderValue), $data);
							}

							$jsonDecoded['data'] = $jsonDecodedData;
						}
					}
					$filteredResponse = json_encode($jsonDecoded);
					file_put_contents($cacheFile, $filteredResponse);
				}
			}
			$_SESSION['metrikaKey'] = $key;
			/**
			 * @var HTTPOutputBuffer $buffer
			 */
			$buffer = outputBuffer::current();
			$buffer->contentType('text/javascript');
			$buffer->option('generation-time', false);
			$buffer->clear();
			$buffer->push($filteredResponse);
			$buffer->end();
		}

		/**
		 * Возвращает список имен и кодов стран
		 * @return array
		 */
		public function getCountriesList() {
			return array(
				'Австралия' => 'AU',
				'Австрия' => 'AT',
				'Азербайджан' => 'AZ',
				'Албания' => 'AL',
				'Алжир' => 'DZ',
				'Ангилья о. (GB)' => 'AI',
				'Ангола' => 'AO',
				'Андорра' => 'AD',
				'Антарктика' => 'AQ',
				'Антигуа и Барбуда' => 'AG',
				'Антильские о-ва (NL)' => 'AN',
				'Аргентина' => 'AR',
				'Армения' => 'AM',
				'Аруба' => 'AW',
				'Афганистан' => 'AF',
				'Багамы' => 'BS',
				'Бангладеш' => 'BD',
				'Барбадос' => 'BB',
				'Бахрейн' => 'BH',
				'Беларусь' => 'BY',
				'Белиз' => 'BZ',
				'Бельгия' => 'BE',
				'Бенин' => 'BJ',
				'Бермуды' => 'BM',
				'Бове о. (NO)' => 'BV',
				'Болгария' => 'BG',
				'Боливия' => 'BO',
				'Босния и Герцеговина' => 'BA',
				'Ботсвана' => 'BW',
				'Бразилия' => 'BR',
				'Бруней Дарассалам' => 'BN',
				'Буркина-Фасо' => 'BF',
				'Бурунди' => 'BI',
				'Бутан' => 'BT',
				'Вануату' => 'VU',
				'Ватикан' => 'VA',
				'Великобритания' => 'GB',
				'Венгрия' => 'HU',
				'Венесуэла' => 'VE',
				'Виргинские о-ва (GB)' => 'VG',
				'Виргинские о-ва (US)' => 'VI',
				'Восточное Самоа (US)' => 'AS',
				'Восточный Тимор' => 'TP',
				'Вьетнам' => 'VN',
				'Габон' => 'GA',
				'Гаити' => 'HT',
				'Гайана' => 'GY',
				'Гамбия' => 'GM',
				'Гана' => 'GH',
				'Гваделупа' => 'GP',
				'Гватемала' => 'GT',
				'Гвинея' => 'GN',
				'Гвинея-Бисау' => 'GW',
				'Германия' => 'DE',
				'Гибралтар' => 'GI',
				'Гондурас' => 'HN',
				'Гонконг (CN)' => 'HK',
				'Гренада' => 'GD',
				'Гренландия (DK)' => 'GL',
				'Греция' => 'GR',
				'Грузия' => 'GE',
				'Гуам' => 'GU',
				'Дания' => 'DK',
				'Демократическая Республика Конго' => 'CD',
				'Джибути' => 'DJ',
				'Доминика' => 'DM',
				'Доминиканская Республика' => 'DO',
				'Египет' => 'EG',
				'Замбия' => 'ZM',
				'Западная Сахара' => 'EH',
				'Зимбабве' => 'ZW',
				'Израиль' => 'IL',
				'Индия' => 'IN',
				'Индонезия' => 'ID',
				'Иордания' => 'JO',
				'Ирак' => 'IQ',
				'Иран' => 'IR',
				'Ирландия' => 'IE',
				'Исландия' => 'IS',
				'Испания' => 'ES',
				'Италия' => 'IT',
				'Йемен' => 'YE',
				'Кабо-Верде' => 'CV',
				'Казахстан' => 'KZ',
				'Каймановы о-ва (GB)' => 'KY',
				'Камбоджа' => 'KH',
				'Камерун' => 'CM',
				'Канада' => 'CA',
				'Катар' => 'QA',
				'Кения' => 'KE',
				'Кипр' => 'CY',
				'Киргизия' => 'KG',
				'Кирибати' => 'KI',
				'Китай' => 'CN',
				'Кокосовые (Киилинг) о-ва (AU)' => 'CC',
				'Колумбия' => 'CO',
				'Коморские о-ва' => 'KM',
				'Республика Конго' => 'CG',
				'Коста-Рика' => 'CR',
				"Кот-д’Ивуар" => 'CI',
				'Куба' => 'CU',
				'Кувейт' => 'KW',
				'Кука о-ва (NZ)' => 'CK',
				'Лаос' => 'LA',
				'Латвия' => 'LV',
				'Лесото' => 'LS',
				'Либерия' => 'LR',
				'Ливан' => 'LB',
				'Ливия' => 'LY',
				'Литва' => 'LT',
				'Лихтенштейн' => 'LI',
				'Люксембург' => 'LU',
				'Маврикий' => 'MU',
				'Мавритания' => 'MR',
				'Мадагаскар' => 'MG',
				'Майотта о. (KM)' => 'YT',
				'Макао (PT)' => 'MO',
				'Македония' => 'MK',
				'Малави' => 'MW',
				'Малайзия' => 'MY',
				'Мали' => 'ML',
				'Мальдивы' => 'MV',
				'Мальта' => 'MT',
				'Марокко' => 'MA',
				'Мартиника' => 'MQ',
				'Маршалловы о-ва' => 'MH',
				'Мексика' => 'MX',
				'Микронезия (US)' => 'FM',
				'Мозамбик' => 'MZ',
				'Молдова' => 'MD',
				'Монако' => 'MC',
				'Монголия' => 'MN',
				'Монсеррат о. (GB)' => 'MS',
				'Мьянма' => 'MM',
				'Намибия' => 'NA',
				'Науру' => 'NR',
				'Непал' => 'NP',
				'Нигер' => 'NE',
				'Нигерия' => 'NG',
				'Нидерланды' => 'NL',
				'Никарагуа' => 'NI',
				'Ниуэ о. (NZ)' => 'NU',
				'Новая Зеландия' => 'NZ',
				'Новая Каледония о. (FR)' => 'NC',
				'Норвегия' => 'NO',
				'Норфолк о. (AU)' => 'NF',
				'Объединенные Арабские Эмираты' => 'AE',
				'Оман' => 'OM',
				'Пакистан' => 'PK',
				'Палау (US)' => 'PW',
				'Палестинская автономия' => 'PS',
				'Панама' => 'PA',
				'Папуа-Новая Гвинея' => 'PG',
				'Парагвай' => 'PY',
				'Перу' => 'PE',
				'Питкэрн о-ва (GB)' => 'PN',
				'Польша' => 'PL',
				'Португалия' => 'PT',
				'Пуэрто-Рико (US)' => 'PR',
				'Реюньон о. (FR)' => 'RE',
				'Рождества о. (AU)' => 'CX',
				'Россия' => 'RU',
				'Руанда' => 'RW',
				'Румыния' => 'RO',
				'Сальвадор' => 'SV',
				'Самоа' => 'WS',
				'Сан Марино' => 'SM',
				'Сан-Томе и Принсипи' => 'ST',
				'Саудовская Аравия' => 'SA',
				'Свазиленд' => 'SZ',
				'Свалбард и Ян Мейен о-ва (NO)' => 'SJ',
				'Святой Елены о. (GB)' => 'SH',
				'Северная Корея (КНДР)' => 'KP',
				'Северные Марианские о-ва (US)' => 'MP',
				'Сейшелы' => 'SC',
				'Сен-Винсент и Гренадины' => 'VC',
				'Сен-Пьер и Микелон (FR)' => 'PM',
				'Сенегал' => 'SN',
				'Сент-Кристофер и Невис' => 'KN',
				'Сент-Люсия' => 'LC',
				'Сербия' => 'RS',
				'Сингапур' => 'SG',
				'Сирия' => 'SY',
				'Словакия' => 'SK',
				'Словения' => 'SI',
				'США' => 'US',
				'Соломоновы о-ва' => 'SB',
				'Сомали' => 'SO',
				'Судан' => 'SD',
				'Суринам' => 'SR',
				'Сьерра-Леоне' => 'SL',
				'Таджикистан' => 'TJ',
				'Таиланд' => 'TH',
				'Тайвань' => 'TW',
				'Танзания' => 'TZ',
				'Теркс и Кайкос о-ва (GB)' => 'TC',
				'Того' => 'TG',
				'Токелау о-ва (NZ)' => 'TK',
				'Тонга' => 'TO',
				'Тринидад и Тобаго' => 'TT',
				'Тувалу' => 'TV',
				'Тунис' => 'TN',
				'Туркмения' => 'TM',
				'Турция' => 'TR',
				'Уганда' => 'UG',
				'Узбекистан' => 'UZ',
				'Украина' => 'UA',
				'Уоллис и Футуна о-ва (FR)' => 'WF',
				'Уругвай' => 'UY',
				'Фарерские о-ва (DK)' => 'FO',
				'Фиджи' => 'FJ',
				'Филиппины' => 'PH',
				'Финляндия' => 'FI',
				'Фолклендские (Мальвинские) о-ва (GB/AR)' => 'FK',
				'Франция' => 'FR',
				'Французская Гвиана (FR)' => 'GF',
				'Французская Полинезия' => 'PF',
				'Херд и Макдональд о-ва (AU)' => 'HM',
				'Хорватия' => 'HR',
				'Центрально-африканская Республика' => 'CF',
				'Чад' => 'TD',
				'Чехия' => 'CZ',
				'Черногория' => 'ME',
				'Чили' => 'CL',
				'Швейцария' => 'CH',
				'Швеция' => 'SE',
				'Шри-Ланка' => 'LK',
				'Эквадор' => 'EC',
				'Экваториальная Гвинея' => 'GQ',
				'Эритрия' => 'ER',
				'Эстония' => 'EE',
				'Эфиопия' => 'ET',
				'Южная Африка' => 'ZA',
				'Южная Георгия и Южные Сандвичевы о-ва' => 'GS',
				'Южная Корея' => 'KR',
				'ЮАР' => 'ZA',
				'Ямайка' => 'JM',
				'Япония' => 'JP'
			);
		}
	}
?>