<?php
	/**
	 * Абстрактный класс отчета по статистике
	 */
	require_once dirname(__FILE__) . '/classes/simpleStat.php';
	/**
	 * Класс сбора статистики
	 */
	require_once dirname(__FILE__) . '/classes/statistic.php';
	/**
	 * Фабрика отчетов по статистике
	 */
	require_once dirname(__FILE__) . '/classes/statisticFactory.php';
	/**
	 * Абстрактный декоратор результата отчета
	 */
	require_once dirname(__FILE__) . '/classes/xmlDecorator.php';
	/**
	 * Класс вычисления числа будних и выходных дней
	 */
	require_once dirname(__FILE__) . '/classes/holidayRoutineCounter.php';
	/**
	 *  Класс разбора query_string на параметры OpenStat
	 */
	require_once dirname(__FILE__) . '/classes/openstat.php';
?>