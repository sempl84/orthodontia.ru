<?php
	/**
	 * Класс служебного функционала
	 */
	class AutoUpdateService {

		/**
		 * @var autoupdate $module
		 */
		public $module;

		/**
		 * Выводит в буффер служебную информацию о системе
		 */
		public function service () {
			$event = strtoupper(getRequest('param0'));
			switch ($event) {
				case "STATUS": {
					$result = $this->returnStatus();
					break;
				}
				case "VERSION": {
					$result = $this->returnVersions();
					break;
				}
				case "LAST_UPDATED": {
					$result = $this->getLastUpdated();
					break;
				}
				case "MODULES": {
					$result = $this->getModules();
					break;
				}
				case "DOMAINS": {
					$result = $this->getDomains();
					break;
				}
				case 'SUPPORT':
					$supportEndDate = $this->module->getSupportEndDate();
					$result = '';

					if (isset($supportEndDate['date']) && isset($supportEndDate['date']['@timestamp'])) {
						$result = date('d.m.Y H:i:s', $supportEndDate['date']['@timestamp']);
					}

					break;
				default: {
					$result = "UNHANDLED_EVENT";
				}
			}

			$this->module->flush($result, "text/plain");
		}

		/**
		 * Возвращает список модулей, которые не должны быть установлены на системе
		 * @return array
		 * @throws coreException
		 */
		public function getIllegalModules() {
			$regedit = regedit::getInstance();

			$info = array(
				'type'     => 'get-modules-list',
				'revision' => $regedit->getVal("//modules/autoupdate/system_build"),
				'key'      => $regedit->getVal("//settings/keycode"),
				'host'     => getServer('HTTP_HOST'),
				'ip'       => getServer('SERVER_ADDR')
			);

			$url = base64_decode('aHR0cDovL3Vkb2QudW1paG9zdC5ydS91cGRhdGVzZXJ2ZXIv') . "?" . http_build_query($info, '', '&');

			try {
				$result = $this->get_file($url);
			} catch (umiRemoteFileGetterException $e) {
				throw new coreException(getLabel('error-list-of-unsupported-modules-not-loaded'));
			}

			$xml = new DOMDocument();
			$xml->loadXML($result);

			$xpath = new DOMXPath($xml);
			/**
			 * @var DOMNodeList $no_active
			 */
			$no_active = $xpath->query("//module[not(@active)]");

			$illegal_modules = array();

			/**
			 * @var DOMElement $module
			 */
			foreach ($no_active as $module) {
				$illegal_modules[] = $module->getAttribute("name");
			}

			unset($regedit, $info, $url, $result, $xml, $xpath, $no_active, $module);
			return $illegal_modules;
		}

		/**
		 * Запускает удаление модулей, которые не должны быть
		 * установлены
		 * @throws coreException
		 */
		public function deleteIllegalModules() {
			$modules = $this->getIllegalModules();
			foreach ($modules as $module) {
				$this->deleteIllegalModule($module);
			}
		}

		/**
		 * Возвращает статус обновления
		 * @return string
		 */
		protected function returnStatus () {
			return (string) regedit::getInstance()->getVal("//modules/autoupdate/status");
		}

		/**
		 * Возвращает версию системы и сборки
		 * @return string
		 */
		protected function returnVersions() {
			$regedit = regedit::getInstance();
			return (string) $regedit->getVal("//modules/autoupdate/system_version") . "\n" . $regedit->getVal("//modules/autoupdate/system_build");
		}

		/**
		 * Возвращает timestamp времени последнего обновления
		 * @return string
		 */
		protected function getLastUpdated () {
			return (string) (int) regedit::getInstance()->getVal("//modules/autoupdate/last_updated");
		}

		/**
		 * Возвращает перечень установленных модулей
		 * @return string
		 */
		protected function getModules() {
			$regedit = regedit::getInstance();
			$ml = $regedit->getList("//modules");

			$res = "";

			foreach($ml as $m) {
				$res .= $m[0] . "\n";
			}

			return $res;
		}

		/**
		 * Возвращает перечень доменов системы
		 * @return string
		 */
		protected function getDomains() {
			$domainsCollection = domainsCollection::getInstance();
			$domains = $domainsCollection->getList();

			$res = "";
			/**
			 * @var domain $domain
			 */
			foreach($domains as $domain) {
				$res .= $domain->getHost() . "\n";
			}
			return $res;
		}

		/**
		 * Получает содержимое адреса
		 * @param string $url адрес
		 * @return string|umiFile
		 * @throws Exception
		 * @throws umiRemoteFileGetterException
		 */
		public function get_file($url) {
			return umiRemoteFileGetter::get($url);
		}

		/**
		 * Удаляет модуль
		 * @param string $module_name имя модуля
		 */
		protected function deleteIllegalModule($module_name) {
			if (!trim($module_name, " \r\n\t\/")) {
				return;
			}

			$regedit = regedit::getInstance();
			$regedit->delVar("//modules/{$module_name}");
		}
	}
?>