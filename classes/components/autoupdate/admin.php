<?php
	/**
	 * Класс функционала административной панели
	 */
	class AutoupdateAdmin{

		use baseModuleAdmin;
		/**
		 * @var autoupdate $module
		 */
		public $module;

		/**
		 * Возвращает информацию о состоянии обновлений системы
		 * @throws coreException
		 */
		public function versions() {
			$regedit = regedit::getInstance();
			$systemEdition = $regedit->getVal("//modules/autoupdate/system_edition");
			$systemEditionStatus = "%autoupdate_edition_" . $systemEdition . "%";

			if (
				$systemEdition == "commerce_trial" &&
				$_SERVER['HTTP_HOST'] != 'localhost' &&
				$_SERVER['HTTP_HOST'] != 'subdomain.localhost' &&
				$_SERVER['SERVER_ADDR'] != '127.0.0.1'
			) {
				$daysLeft = $regedit->getDaysLeft();
				$systemEditionStatus .= " ({$daysLeft} " . getLabel('label-days-left') . ")";
			}

			$systemEditionStatus = autoupdate::parseTPLMacroses($systemEditionStatus);

			$params = Array(
				"autoupdate" => Array(
					"status:system-edition"		=> NULL,
					"status:last-updated"		=> NULL,
					"status:system-version"		=> NULL,
					"status:system-build"		=> NULL,
					"status:db-driver"			=> NULL,
					"boolean:disabled"			=> NULL,
					"boolean:patches-disabled"	=> NULL
				)
			);


			$params['autoupdate']['status:system-version'] = $regedit->getVal("//modules/autoupdate/system_version");
			$params['autoupdate']['status:system-build'] = $regedit->getVal("//modules/autoupdate/system_build");
			$params['autoupdate']['boolean:patches-disabled'] = (int) $patches_disabled = $regedit->getVal("//modules/autoupdate/disable_patches");
			$params['autoupdate']['status:system-edition'] = $systemEditionStatus;
			$params['autoupdate']['status:last-updated'] = date("Y-m-d H:i:s", $regedit->getVal("//modules/autoupdate/last_updated"));

			$db_driver = "mysql";

			if (defined("DB_DRIVER")) {
				$db_driver = DB_DRIVER;
			}

			$params['autoupdate']['status:db-driver'] = $db_driver;

			$autoupdates_disabled = false;

			if (defined("CURRENT_VERSION_LINE")) {
				if (in_array(CURRENT_VERSION_LINE, array("start", "demo"))) {
					$autoupdates_disabled = true;
				}
			}

			$params['autoupdate']['boolean:disabled'] = (int) $autoupdates_disabled;

			$domainsCollection = domainsCollection::getInstance();

			if (!$domainsCollection->isDefaultDomain()) {
				$params['autoupdate']['check:disabled-by-host'] = $domainsCollection->getDefaultDomain()->getHost();
			}

			$this->setDataType("settings");
			$this->setActionType("view");
			$data = $this->prepareData($params, "settings");
			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает список патчей, доступных для установки
		 * @param bool|string $mode режим работы
		 * @throws Exception
		 * @throws umiRemoteFileGetterException
		 */
		public function patches($mode = false) {
			if (isset($_GET['mode'])) {
				$mode = $_GET['mode'];
			}

			$regedit = regedit::getInstance();
			$patches_disabled = $regedit->getVal("//modules/autoupdate/disable_patches");
			$version = $regedit->getVal("//modules/autoupdate/system_version");
			$build = $regedit->getVal("//modules/autoupdate/system_build");
			$applied = $regedit->getList("//modules/autoupdate/applied_patches//");

			if (!$patches_disabled == true) {
				$array = array();
				$a = '';

				if ($applied!==false) {
					foreach ($applied as &$value) {
						array_push($array, $value[0]);
					}
					$a = "<item>";
					$a .= implode('</item><item>', $array);
					$a .= "</item>";
				}				$data['xml:applied'] = "<applied>{$a}</applied>";

				$url = null;

				if ($mode === "all") {
					$url = "http://hub.umi-cms.ru/patches/xml/";
					$data['xml:caution'] = "<caution>all</caution>";
				} else {
					$data['xml:caution'] = "<caution>normal</caution>";
				}

				if ($version && $build) {
					if ($mode === false) {
						$url = "http://hub.umi-cms.ru/patches/xml/?version=" . $version . "&revision=" . $build;
					}
					$data['xml:info'] = umiRemoteFileGetter::get($url);
				}

				$this->setDataType("settings");
				$this->setActionType("view");
				$this->setData($data);
				$this->doData();
			}
		}

		/**
		 * Возвращает сообщение о статусе
		 * операции применения или отката патча
		 * @param string $message сообщение
		 * @param string|int $code код статуса
		 * @return bool
		 */
		public function getDiffMessage($message, $code) {
			$data['xml:response'] = '<response message="'.$message.'" code="'.$code.'"/>';
			$this->setDataType("settings");
			$this->setActionType("view");
			$this->setData($data);
			$this->doData();
			return true;
		}
	}
?>