<?php
	/**
	 * Класс функционала административной панели
	 */
	class SeoAdmin {

		use baseModuleAdmin;
		/**
		 * @var seo $module
		 */
		public $module;

		/**
		 * Возвращает данные для вкладки "Анализ позиций"
		 * @return bool
		 * @throws coreException
		 */
		public function seo() {
			/**
			 * @var seo|SeoMegaIndex $module
			 */
			$module = $this->module;
			$regedit = regedit::getInstance();
			$login = trim($regedit->getVal("//modules/seo/megaindex-login"));
			$password = trim($regedit->getVal("//modules/seo/megaindex-password"));

			if (CURRENT_VERSION_LINE === 'demo' && getRequest("host") == '') {
				$host = 'umi-cms.ru';
			} else {
				$host = (string) (strlen(getRequest ("host"))) ? getRequest ("host") : getServer('HTTP_HOST');
			}

			$date = date('Y-m-d');
			$this->setDataType("settings");
			$this->setActionType("view");

			$preParams = Array(
				"config" => Array(
					"url:http_host" => $host
				)
			);

			$data = $this->prepareData($preParams, 'settings');

			if ($module->ifNotXmlMode()) {
				$this->setData($data);
				$this->doData();
				return true;
			}

			if ($password && $login) {
				/** @var stdClass $params */
				$params = $module->siteAnalyzeJson($login, $password, $host, $date);
			}

			if (isset($params)) {
				$items = array();
				foreach ($params->data as $k => $param) {
					$item = array(
						'@word' => $param['0'],
						'@pos_y' => $param['1'],
						'@pos_g' => $param['3'],
						'@show_month' => $param['5'],
						'@wordstat' => $param['7'],
					);
					$items[] = $item;
				}
				unset($items[0]);
				$data['items'] = array(
					'nodes:item' => $items
				);
			} else {
				$data['error'] = getLabel('error-need-megaindex-registration');
			}

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает данные для вкладки "Анализ ссылок"
		 * @throws Exception
		 * @throws coreException
		 * @throws umiRemoteFileGetterException
		 */
		public function links() {
			/**
			 * @var seo|SeoMegaIndex $module
			 */
			$module = $this->module;
			$regedit = regedit::getInstance();
			$login = trim($regedit->getVal("//modules/seo/megaindex-login"));
			$password = trim($regedit->getVal("//modules/seo/megaindex-password"));

			if (CURRENT_VERSION_LINE === 'demo' && getRequest("host") == '') {
				$host = 'umi-cms.ru';
			} else {
				$host = (string) (strlen(getRequest ("host"))) ? getRequest ("host") : getServer('HTTP_HOST');
			}

			$result = $module->getBackLinks($login, $password, $host);

			$this->setDataType("settings");
			$this->setActionType("view");

			$preParams = Array(
				"config" => Array(
					"url:http_host" => $host
				)
			);

			$links = array('nodes:link' => array());
			$errors = array('nodes:error' => array());

			if (!is_array($result)) {
				$result = array($result);
			}

			foreach ($result as $link) {
				if (!$link instanceof stdClass || !empty($link->error)) {
					$error = $link->error;
					if ($error == "Сайт не проиндексирован! Добавьте пожалуйста на индексацию.") {
						$error = ulangStream::getLabelSimple('label-seo-noindex', array($host));
					}
					$errors['nodes:error'][] = array(
						'node:value' => $error
					);
				} else {
					$links['nodes:link'][] = array(
						'attribute:vs_from' => $link->vs_from,
						'attribute:vs_to' => $link->vs_to,
						'attribute:tic_from' => $link->tic_from,
						'attribute:tic_to' => $link->tic_to,
						'attribute:text' => $link->text,
						'attribute:noi' => $link->noi,
						'attribute:nof' => $link->nof
					);
				}
			}

			$data = $this->prepareData($preParams, 'settings');
			$data['links'] = $links;
			$data['errors'] = $errors;

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает данные для вкладки "Яндекс.Вебмастер"
		 * @throws coreException
		 */
		public function webmaster() {
			/**
			 * @var seo|SeoYandexWebMaster $module
			 */
			$module = $this->module;
			$this->setDataType("settings");
			$this->setActionType("view");

			$preParams = array();
			$hostsData = $module->getDomainsList();
			$data = $this->prepareData($preParams, 'settings');
			$data['hosts']['nodes:host'] = $hostsData;

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Запускает добавление сайта в Яндекс.Вебмастер
		 * и возвращает результат операции
		 * @throws coreException
		 */
		public function add_site() {
			/**
			 * @var seo|SeoYandexWebMaster $module
			 */
			$module = $this->module;
			$this->setDataType("settings");
			$this->setActionType("view");

			$preParams = array();
			$data = $this->prepareData($preParams, 'settings');
			$data['hosts']['host'] = $module->addSite();

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Запускает подтверждение сайта в Яндекс.Вебмастер
		 * и возвращает результат операции
		 * @throws coreException
		 */
		public function verify_site() {
			/**
			 * @var seo|SeoYandexWebMaster $module
			 */
			$module = $this->module;
			$this->setDataType("settings");
			$this->setActionType("view");

			$preParams = array();
			$data = $this->prepareData($preParams, 'settings');
			$data['hosts']['host'] = $module->verifySite();

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает информацию о сайте с Яндекс.Вебмастер
		 * @throws coreException
		 */
		public function refresh_site() {
			/**
			 * @var seo|SeoYandexWebMaster $module
			 */
			$module = $this->module;
			$this->setDataType("settings");
			$this->setActionType("view");

			$preParams = array();
			$data = $this->prepareData($preParams, 'settings');

			$hostLink = getRequest('hostLink');
			$data['hosts']['host'] = $module->getHostData($hostLink);

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Запускает отправления запроса к Яндекс.Вебмастер
		 * @param bool|string $url uri запроса
		 * @throws coreException
		 * @throws publicAdminException
		 */
		public function handle_url($url=false) {
			if (!$url) {
				$url = getRequest('url');
			}
			/**
			 * @var seo|SeoYandexWebMaster $module
			 */
			$module = $this->module;
			$preParams = array();
			$hostsData = $module->getYandexData($url);
			$this->setDataType("settings");
			$this->setActionType("view");
			$data = $this->prepareData($preParams, 'settings');
			$data['xml:yandex'] = $hostsData;

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает основные настройки модуля.
		 * Если передан ключевой параметр $_REQUEST['param0'] = do,
		 * то сохраняет настройки.
		 * @throws coreException
		 */
		public function config() {
			$regedit = regedit::getInstance();
			$domains = domainsCollection::getInstance()->getList();
			$lang_id = cmsController::getInstance()->getCurrentLang()->getId();

			$params = Array();

			/**
			 * @var domain $domain
			 */
			foreach ($domains as $domain) {
				$domain_id = $domain->getId();
				$domain_name = $domain->getHost();

				$seo_info = Array();
				$seo_info['status:domain'] = $domain_name;
				$seo_info['string:title-' . $domain_id] = $regedit->getVal("//settings/title_prefix/{$lang_id}/{$domain_id}");
				$seo_info['string:keywords-' . $domain_id] = $regedit->getVal("//settings/meta_keywords/{$lang_id}/{$domain_id}");
				$seo_info['string:description-' . $domain_id] = $regedit->getVal("//settings/meta_description/{$lang_id}/{$domain_id}");
				$params[$domain_name] = $seo_info;
			}

			$mode = (string) getRequest('param0');

			if ($mode == "do") {
				$params = $this->expectParams($params);

				foreach ($domains as $domain) {
					$domain_id = $domain->getId();
					$domain_name = $domain->getHost();

					$title = $params[$domain_name]['string:title-' . $domain_id];
					$keywords = $params[$domain_name]['string:keywords-' . $domain_id];
					$description = $params[$domain_name]['string:description-' . $domain_id];

					$regedit->setVal("//settings/title_prefix/{$lang_id}/{$domain_id}", $title);
					$regedit->setVal("//settings/meta_keywords/{$lang_id}/{$domain_id}", $keywords);
					$regedit->setVal("//settings/meta_description/{$lang_id}/{$domain_id}", $description);
				}

				$this->chooseRedirect();
			}

			$this->setDataType('settings');
			$this->setActionType('modify');
			$data = $this->prepareData($params, 'settings');
			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает настройки для подключения к сервису Megaindex.
		 * Если передан ключевой параметр $_REQUEST['param0'] = do,
		 * то сохраняет настройки.
		 * @throws coreException
		 */
		public function megaindex() {
			$regedit = regedit::getInstance();

			$params = Array (
				"config" => Array (
					"string:megaindex-login" => null,
					"string:megaindex-password" => null
				)
			);

			$mode = getRequest("param0");

			if ($mode == "do"){
				$params = $this->expectParams($params);
				$regedit->setVar("//modules/seo/megaindex-login", $params["config"]["string:megaindex-login"]);
				$regedit->setVar("//modules/seo/megaindex-password", $params["config"]["string:megaindex-password"]);
				$this->chooseRedirect();
			}

			$params["config"]["string:megaindex-login"] = $regedit->getVal("//modules/seo/megaindex-login");
			$params["config"]["string:megaindex-password"] = $regedit->getVal("//modules/seo/megaindex-password");

			$this->setDataType("settings");
			$this->setActionType("modify");

			$data = $this->prepareData($params, "settings");
			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает настройки для подключения к сервису Яндекс.Вебмастер.
		 * Если передан ключевой параметр $_REQUEST['param0'] = do,
		 * то сохраняет настройки.
		 * @throws coreException
		 */
		public function yandex() {
			/**
			 * @var seo|SeoYandexWebMaster $module
			 */
			$module = $this->module;
			$regedit = regedit::getInstance();
			$params = Array();

			if ($regedit->getVal("//modules/seo/yandex-token")) {
				$params['yandex']['string:token'] = $regedit->getVal("//modules/seo/yandex-token");
			} else {
				$params['yandex']['string:code'] = '';
			}

			$mode = (string) getRequest('param0');

			if ($mode == "do") {
				$params = $this->expectParams($params);

				if (isset($params['yandex']['string:code']) && strlen($params['yandex']['string:code'])){
					$token = $module->getAuthToken($params['yandex']['string:code']);
					$regedit->setVal("//modules/seo/yandex-token", $token);
				} else {
					$regedit->setVal("//modules/seo/yandex-token", $params['yandex']['string:token']);
				}

				$this->chooseRedirect();
			}

			$this->setDataType("settings");
			$this->setActionType("modify");

			$data = $this->prepareData($params, 'settings');
			$this->setData($data);
			$this->doData();
		}
	}
?>