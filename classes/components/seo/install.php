<?php
	/**
	 * Установщик модуля
	 */

	/**
	 * @var array $INFO реестр модуля
	 */
	$INFO = Array();
	$INFO['name'] = "seo";
	$INFO['config'] = "1";
	$INFO['default_method'] = "show";
	$INFO['default_method_admin'] = "seo";
	$INFO['func_perms'] = "Группы прав на функционал модуля";
	$INFO['func_perms/seo'] = "Права на администрирование модуля";

	/**
	 * @var array $COMPONENTS файлы модуля
	 */
	$COMPONENTS = array();
	$COMPONENTS[] = "./classes/components/seo/admin.php";
	$COMPONENTS[] = "./classes/components/seo/class.php";
	$COMPONENTS[] = "./classes/components/seo/customAdmin.php";
	$COMPONENTS[] = "./classes/components/seo/customCommon.php";
	$COMPONENTS[] = "./classes/components/seo/customMacros.php";
	$COMPONENTS[] = "./classes/components/seo/i18n.en.php";
	$COMPONENTS[] = "./classes/components/seo/i18n.php";
	$COMPONENTS[] = "./classes/components/seo/includes.php";
	$COMPONENTS[] = "./classes/components/seo/install.php";
	$COMPONENTS[] = "./classes/components/seo/lang.en.php";
	$COMPONENTS[] = "./classes/components/seo/lang.php";
	$COMPONENTS[] = "./classes/components/seo/megaIndex.php";
	$COMPONENTS[] = "./classes/components/seo/permissions.php";
	$COMPONENTS[] = "./classes/components/seo/yandexWebMaster.php";
?>