<?php
	/**
	 * Класс функционала административной панели
	 */
	class Social_networksAdmin {

		use baseModuleAdmin;
		/**
		 * @var social_network $module
		 */
		public $module;

		/**
		 * Возвращает настройки приложений социальных сетей.
		 * Если передан ключевой параметр $_REQUEST['param0'] = do,
		 * то сохраняет настройки.
		 * @param array $networks приложения социальные сетей
		 * @throws coreException
		 */
		public function network_settings($networks) {
			$mode = getRequest("param0");
			/**
			 * @var social_network $network
			 */
			$network = $networks[0];
			/**
			 * @var iUmiEntinty|iUmiObject $networkObject
			 */
			$networkObject = $network->getObject();
			$this->setHeaderLabel(getLabel("header-social_networks-settings") . $network->getName());

			$this->setDataType("form");
			$this->setActionType("modify");

			if ($mode == "do") {

				foreach ($networks as $network) {
					$this->saveEditedObjectData(array(
						'object' => $networkObject,
						'type' => $network->getCodeName()
					));
				}

				$this->chooseRedirect($this->module->pre_lang . '/admin/social_networks/' . $network->getCodeName() . '/');
			}

			$objects = array();
			$domainsCollection = domainsCollection::getInstance();

			foreach ($networks as $network) {

				$object = $this->prepareData(
					array(
						'object' => $network->getObject(),
						'type' => $network->getCodeName()
					),
					"object"
				);

				$domain = $domainsCollection->getDomain($networkObject->getValue("domain_id"));

				if (!$domain instanceof domain) {
					continue;
				}

				$object = $object['object'];
				$object['@domain'] = $domain->getHost();
				$object['@template-id'] = $networkObject->getValue("template_id");
				$objects[] = $object;
			}

			$this->setData(array(
				'nodes:object' => $objects
			));
			$this->doData();
		}
	}
?>