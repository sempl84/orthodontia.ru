<?php
	/**
	 * Класс функционала административной панели
	 */
	class ConfigAdmin {

		use baseModuleAdmin;
		/**
		 * @var config $module
		 */
		public $module;

		/**
		 * Возвращает главные настройки системы.
		 * Если передан ключевой параметр $_REQUEST['param0'] = do,
		 * то метод запустит сохранение настроек.
		 * @throws coreException
		 */
		public function main() {
			$regedit = regedit::getInstance();
			$config = mainConfiguration::getInstance();

			$timezones = $this->module->getTimeZones();
			$timezones['value'] = $config->get("system", "time-zone");

			$modules = Array();
			foreach ($regedit->getList("//modules") as $module) {
				list($module) = $module;
				$modules[$module] = getLabel('module-' . $module);
			}

			if ($regedit->getVal("//modules/events/") && !$regedit->getVal("//settings/default_module_admin_changed")) {
				$modules['value'] = 'events';
			} else {
				$modules['value'] = $regedit->getVal("//settings/default_module_admin");
			}

			$params = array(
				"globals" => array(
					"string:keycode" => NULL,
					"boolean:chache_browser" => NULL,
					"boolean:disable_url_autocorrection" => NULL,
					"boolean:disable_captcha" => NULL,
					"int:max_img_filesize" => NULL,
					"status:upload_max_filesize" => NULL,
					"boolean:allow-alt-name-with-module-collision" => NULL,
					"boolean:allow-redirects-watch" => NULL,
					"int:session_lifetime" => NULL,
					"status:busy_quota_files_and_images" => NULL,
					"int:quota_files_and_images" => NULL,
					"status:busy_quota_uploads" => NULL,
					"int:quota_uploads" => NULL,
					"boolean:disable_too_many_childs_notification" => NULL,
					'select:timezones' => NULL,
					'select:modules' => NULL
				)
			);

			/**
			 * @var data $moduleData
			 */
			$moduleData = cmsController::getInstance()->getModule('data');
			$maxUploadFileSize = $moduleData->getAllowedMaxFileSize();

			$mode = getRequest("param0");

			if ($mode == "do") {
				$params = $this->expectParams($params);

				$regedit->setVar("//settings/chache_browser", $params['globals']['boolean:chache_browser']);
				$regedit->setVar("//settings/keycode", $params['globals']['string:keycode']);
				$regedit->setVar("//settings/disable_url_autocorrection", $params['globals']['boolean:disable_url_autocorrection']);
				$config->set('anti-spam', 'captcha.enabled', !$params['globals']['boolean:disable_captcha']);

				$maxImgFileSize = $params['globals']['int:max_img_filesize'];
				if ($maxUploadFileSize != -1 && ($maxImgFileSize <= 0 || $maxImgFileSize > $maxUploadFileSize)) {
					$maxImgFileSize = $maxUploadFileSize;
				}
				$regedit->setVar("//settings/max_img_filesize", $maxImgFileSize);

				$config->set('kernel', 'ignore-module-names-overwrite', $params['globals']['boolean:allow-alt-name-with-module-collision']);
				$config->set('seo', 'watch-redirects-history', $params['globals']['boolean:allow-redirects-watch']);
				$config->set("system", "session-lifetime", $params['globals']['int:session_lifetime']);

				$quota = (int) $params['globals']['int:quota_files_and_images'];
				if ($quota < 0) {
					$quota = 0;
				}
				$config->set("system", "quota-files-and-images", $quota * 1024 * 1024);

				$quotaUploads = (int) $params['globals']['int:quota_uploads'];
				if ($quotaUploads < 0) {
					$quotaUploads = 0;
				}
				$config->set("system", "quota-uploads", $quotaUploads * 1024 * 1024);

				$config->set("system", "disable-too-many-childs-notification", $params['globals']['boolean:disable_too_many_childs_notification']);
				$config->set("system", "time-zone", $params['globals']['select:timezones']);
				$regedit->setVar("//settings/default_module_admin", $params['globals']['select:modules']);
				$regedit->setVar("//settings/default_module_admin_changed", 1);
				$this->chooseRedirect();
			}

			$params['globals']['boolean:chache_browser'] = $regedit->getVal("//settings/chache_browser");
			$params['globals']['string:keycode'] = $regedit->getVal("//settings/keycode");
			$params['globals']['boolean:disable_url_autocorrection'] = $regedit->getVal("//settings/disable_url_autocorrection");
			$params['globals']['boolean:disable_captcha'] = !$config->get('anti-spam', 'captcha.enabled');
			$params['globals']['status:upload_max_filesize'] = $maxUploadFileSize;

			$maxImgFileSize = $regedit->getVal("//settings/max_img_filesize");

			$params['globals']['int:max_img_filesize'] = $maxImgFileSize ? $maxImgFileSize : $maxUploadFileSize;
			$params['globals']['boolean:allow-alt-name-with-module-collision'] = $config->get('kernel', 'ignore-module-names-overwrite');
			$params['globals']['boolean:allow-redirects-watch'] = $config->get('seo', 'watch-redirects-history');

			$quotaByte = getBytesFromString(mainConfiguration::getInstance()->get('system', 'quota-files-and-images'));
			$params['globals']['status:busy_quota_files_and_images'] = ceil(getBusyDiskSize(getResourcesDirs()) / (1024*1024));

			if ($quotaByte > 0) {
				$params['globals']['status:busy_quota_files_and_images'] .=" ( ".getBusyDiskPercent()."% )";
			}

			$params['globals']['int:quota_files_and_images'] = (int) (getBytesFromString($config->get('system', 'quota-files-and-images')) / (1024*1024));
			$quotaUploadsBytes = getBytesFromString(mainConfiguration::getInstance()->get('system', 'quota-uploads'));
			$params['globals']['status:busy_quota_uploads'] = ceil(getBusyDiskSize(getUploadsDir()) / (1024*1024));

			if ($quotaUploadsBytes > 0) {
				$params['globals']['status:busy_quota_uploads'] .=" ( ".getOccupiedDiskPercent(getUploadsDir(), $quotaUploadsBytes)."% )";
			}

			$params['globals']['int:quota_uploads'] = (int) (getBytesFromString($config->get('system', 'quota-uploads')) / (1024*1024));
			$params['globals']['int:session_lifetime'] = $config->get('system', 'session-lifetime');
			$params['globals']['boolean:disable_too_many_childs_notification'] = $config->get('system', 'disable-too-many-childs-notification');
			$params['globals']['select:timezones'] = $timezones;
			$params['globals']['select:modules'] = $modules;

			$this->setDataType("settings");
			$this->setActionType("modify");

			if (is_demo()) {
				unset($params["globals"]['string:keycode']);
			}

			$data = $this->prepareData($params, "settings");

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает список модулей для одноименной
		 * вкладки модуля.
		 * @throws coreException
		 */
		public function modules() {
			$modules = Array();
			$regedit = regedit::getInstance();
			$modules_list = $regedit->getList("//modules");

			foreach ($modules_list as $module_name) {
				list($module_name) = $module_name;
				$modules[] = $module_name;
			}

			$this->setDataType("list");
			$this->setActionType("view");

			$data = $this->prepareData($modules, "modules");

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Оптимизирует хранение контента объектов, сгруппированых по
		 * иерархическому типу.
		 * Если объектов иерархического типа больше 3500 и отдельной таблицы для хранения нет,
		 * то их контент переносится в отдельную таблицу, если объектов меньше и отдельная
		 * таблица есть, то контент переносится в общую таблицу.
		 * @throws coreException
		 */
		public function reviewDatabase() {
			/**
			 * @var HTTPOutputBuffer $buffer
			 */
			$buffer = outputBuffer::current();
			$buffer->contentType('text/javascript');
			$buffer->charset('utf-8');

			$maxItemsPerType = 3500;
			$minItemsPerType = round($maxItemsPerType / 2);

			$status = umiBranch::getDatabaseStatus();
			foreach ($status as $item) {
				if ($item['isBranched'] == false) {
					if ($item['count'] > $maxItemsPerType) {
						$hierarchyTypeId = $item['id'];
						$this->module->branchTable($hierarchyTypeId);
					}
				} else {
					if ($item['count'] < $minItemsPerType) {
						$hierarchyTypeId = $item['id'];
						$this->module->mergeTable($hierarchyTypeId);
					}
				}
			}
			$buffer->push("\nwindow.location = window.location;\n");
			$buffer->end();
		}

		/**
		 * Возвращает настройки кеширования.
		 * Если передан ключевой параметр $_REQUEST['param0'] = do,
		 * то метод запустит сохранение настроек.
		 * @throws coreException
		 */
		public function cache() {
			$settings = $this->module->getStaticCacheSettings();
			$streamsSettings =$this->module->getStreamsCacheSettings();

			$enginesList = cacheFrontend::getPriorityEnginesList(true);
			$cacheFrontend = cacheFrontend::getInstance();
			$currentEngineName = $cacheFrontend->getCurrentCacheEngineName();

			$engines = array(getLabel('cache-engine-none'));
			foreach ($enginesList as $engineName) {
				$engines[$engineName] = getLabel("cache-engine-" . $engineName);
			}

			$engines['value'] = $currentEngineName;
			$cacheEngineLabel = $currentEngineName ? getLabel("cache-engine-" . $currentEngineName) : getLabel('cache-engine-none');
			$cacheEnginesWithSize = array('database', 'fs');
			$cacheSize = getLabel('cache-size-off');

			if (in_array($currentEngineName, $cacheEnginesWithSize)) {
				$cacheSize = bytesToString($cacheFrontend->getCacheSize());
			}

			$cacheStatus = ($cacheFrontend->isCacheEnabled()) ? getLabel('cache-engine-on') : getLabel('cache-engine-off');
			$umiConfigs = mainConfiguration::getInstance();
			$cacheLimit = $umiConfigs->get('cache', 'cache-size-limit');
			$cacheLimit = (is_numeric($cacheLimit)) ? bytesToString($cacheLimit) : getLabel('cache-size-limit-off');

			$params = array(
				'engine' => array(
					'status:current-engine' => $cacheEngineLabel,
					'status:cache-status' => $cacheStatus,
					'status:cache-size' => $cacheSize,
					'status:cache-size-limit' => $cacheLimit,
					'select:engines' => $engines
				),
				'streamscache' => array(
					'boolean:cache-enabled'	=> NULL,
					"int:cache-lifetime"	=> NULL,
				),
				'static' => array(
					'boolean:enabled'	=> NULL,
					'select:expire'		=> Array(
						'short'		=> getLabel('cache-static-short'),
						'normal'	=> getLabel('cache-static-normal'),
						'long'		=> getLabel('cache-static-long')
					),
					'boolean:ignore-stat' => NULL
				),
				'test' => array(

				),
			);

			if (isset($_REQUEST['show-something'])) {
				$dbReport = $this->module->getDatabaseReport();
				if ($dbReport) {
					$params['branching']['status:branch'] = $dbReport;
				}
			}

			if ($settings['expire'] == false) {
				unset($params['static']['select:expire']);
				unset($params['static']['boolean:ignore-stat']);
			}

			if ($currentEngineName) {
				$params['engine']['status:reset'] = true;
			}

			if (!$streamsSettings['cache-enabled']) {
				unset($params['streamscache']['int:cache-lifetime']);
			}

			if (!$currentEngineName) {
				unset($params['streamscache']);
			}

			$mode = (string) getRequest('param0');
			$is_demo = defined('CURRENT_VERSION_LINE') && CURRENT_VERSION_LINE == 'demo';

			if ($mode == 'do' and !$is_demo) {
				$params = $this->expectParams($params);

				if (!isset($params['static']['select:expire'])) {
					$params['static']['select:expire'] = "normal";
					$params['static']['boolean:ignore-stat'] = false;
				}

				$settings = Array(
					'enabled'		=> $params['static']['boolean:enabled'],
					'expire'		=> $params['static']['select:expire'],
					'ignore-stat' 	=> $params['static']['boolean:ignore-stat']
				);

				if (isset($params['streamscache']['boolean:cache-enabled'])) {
					$streamsSettings['cache-enabled'] = $params['streamscache']['boolean:cache-enabled'];
				}

				if (isset($params['streamscache']['int:cache-lifetime'])) {
					$streamsSettings['cache-lifetime'] = $params['streamscache']['int:cache-lifetime'];
				}

				$this->module->setStaticCacheSettings($settings);
				$this->module->setStreamsCacheSettings($streamsSettings);
				cacheFrontend::getInstance()->switchCacheEngine($params['engine']['select:engines']);
				$this->chooseRedirect($this->module->pre_lang . "/admin/config/cache/");

			} elseif ($mode == "reset" ) {
				if (!$is_demo) {
					cacheFrontend::getInstance()->flush();
				}
				$this->chooseRedirect($this->module->pre_lang . "/admin/config/cache/");
			}

			$settings = $this->module->getStaticCacheSettings();
			$params['static']['boolean:enabled'] = $settings['enabled'];
			$params['static']['select:expire']['value'] = $settings['expire'];
			$params['static']['boolean:ignore-stat'] = $settings['ignore-stat'];

			if ($settings['expire'] == false) {
				unset($params['static']['select:expire']);
				unset($params['static']['boolean:ignore-stat']);
			}

			$streamsSettings = $this->module->getStreamsCacheSettings();
			$params['streamscache']['boolean:cache-enabled'] = $streamsSettings['cache-enabled'];
			$params['streamscache']['int:cache-lifetime'] = $streamsSettings['cache-lifetime'];

			if (!$params['streamscache']['boolean:cache-enabled']) {
				unset($params['streamscache']['int:cache-lifetime']);
			}

			if (!$currentEngineName) {
				unset($params['streamscache']);
			}

			$this->setDataType("settings");
			$this->setActionType("modify");
			$data = $this->prepareData($params, "settings");
			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает список доменов для одноименной
		 * вкладки модуля.
		 * Если передан ключевой параметр $_REQUEST['param0'] = do,
		 * то метод запустит сохранение списка.
		 * @throws coreException
		 */
		public function domains() {
			$mode = getRequest("param0");

			if ($mode == "do") {
				if (!is_demo()) {
					$this->saveEditedList("domains");
				}
				$this->chooseRedirect($this->module->pre_lang . '/admin/config/domains/');
			}

			$domains = domainsCollection::getInstance()->getList();

			$this->setDataType("list");
			$this->setActionType("modify");
			$data = $this->prepareData($domains, "domains");
			$this->setData($data, sizeof($domains));
			$this->doData();
		}

		/**
		 * Возвращает список зеркал домена и его seo настройки.
		 * Если передан ключевой параметр $_REQUEST['param1'] = do,
		 * то метод запустит сохранение списка и настроек.
		 * @throws coreException
		 */
		public function domain_mirrows() {
			$domain_id = getRequest('param0');
			$mode = getRequest("param1");
			$regedit = regedit::getInstance();
			$lang_id = cmsController::getInstance()->getCurrentLang()->getId();

			$seo_info = array();
			$additional_info = array();
			$seo_info['string:seo-title'] = $regedit->getVal("//settings/title_prefix/{$lang_id}/{$domain_id}");
			$seo_info['string:seo-keywords'] = $regedit->getVal("//settings/meta_keywords/{$lang_id}/{$domain_id}");
			$seo_info['string:seo-description'] = $regedit->getVal("//settings/meta_description/{$lang_id}/{$domain_id}");
			$seo_info['string:ga-id'] = $regedit->getVal("//settings/ga-id/{$domain_id}");
			$additional_info['string:site_name'] = $regedit->getVal("//settings/site_name/{$domain_id}/{$lang_id}/") ?
				$regedit->getVal("//settings/site_name/{$domain_id}/{$lang_id}") : $regedit->getVal("//settings/site_name");

			$params = Array(
				'seo'			=> $seo_info,
				'additional' 	=> $additional_info,
			);

			if ($mode == "do") {
				if (!is_demo())  {
					$this->saveEditedList("domain_mirrows");
					$params = $this->expectParams($params);

					$title = $params['seo']['string:seo-title'];
					$keywords = $params['seo']['string:seo-keywords'];
					$description = $params['seo']['string:seo-description'];
					$ga_id = $params['seo']['string:ga-id'];
					$siteName = $params['additional']['string:site_name'];

					$regedit->setVal("//settings/title_prefix/{$lang_id}/{$domain_id}", $title);
					$regedit->setVal("//settings/meta_keywords/{$lang_id}/{$domain_id}", $keywords);
					$regedit->setVal("//settings/meta_description/{$lang_id}/{$domain_id}", $description);
					$regedit->setVal("//settings/ga-id/{$domain_id}", $ga_id);
					$regedit->setVal("//settings/site_name/{$domain_id}/{$lang_id}", $siteName);
				}

				$this->chooseRedirect($this->module->pre_lang . '/admin/config/domain_mirrows/' . $domain_id . '/');
			}

			$domains = domainsCollection::getInstance()->getDomain($domain_id);
			$mirrows = $domains->getMirrowsList();

			$this->setDataType("list");
			$this->setActionType("modify");
			$seoData = $this->prepareData($params, 'settings');
			$mirrorsData = $this->prepareData($mirrows, "domain_mirrows");
			$data = $seoData + $mirrorsData;
			$this->setData($data, sizeof($domains));
			$this->doData();
		}

		/**
		 * Обновляет данные для построения sitemap.xml.
		 * Обходит страницы всех доменов и языков, используется
		 * для итеративно.
		 */
		public function update_sitemap() {
			$domainId = (int) getRequest('param0');
			$complete = false;
			$hierarchy = umiHierarchy::getInstance();
			$dirName = CURRENT_WORKING_DIR . "/sys-temp/sitemap/{$domainId}/";

			if (!is_dir($dirName)) {
				mkdir($dirName, 0777, true);
			}

			$filePath = $dirName . "domain";

			if (!file_exists($filePath)) {
				$elements = array();
				$langsCollection = langsCollection::getInstance();
				$langs = $langsCollection->getList();
				/**
				 * @var lang|iUmiEntinty $lang
				 */
				foreach ($langs as $lang) {
					$elements = array_merge($elements, $hierarchy->getChildrenList(0, false, true, false, $domainId, false, $lang->getId()));
				}
				sort($elements);
				file_put_contents($filePath, serialize($elements));
			}

			$offset = (int) getSession("sitemap_offset_" . $domainId);
			$blockSize = mainConfiguration::getInstance()->get("modules", "exchange.splitter.limit") ?
				mainConfiguration::getInstance()->get("modules", "exchange.splitter.limit") : 25;
			$elements = unserialize(file_get_contents($filePath));

			for ($i = $offset; $i <= $offset + $blockSize -1; $i++) {
				if (!array_key_exists($i, $elements)) {
					$complete = true;
					break;
				}
				$element = $hierarchy->getElement($elements[$i], true, true);

				if ($element instanceof umiHierarchyElement) {
					$element->updateSiteMap(true);
				}
			}

			$_SESSION["sitemap_offset_" . $domainId] = $offset + $blockSize;
			if ($complete) {
				unset($_SESSION["sitemap_offset_" . $domainId]);
				unlink($filePath);
			}

			$data = array(
				"attribute:complete" => (int) $complete
			);

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает список языков для одноименной
		 * вкладки модуля.
		 * Если передан ключевой параметр $_REQUEST['param0'] = do,
		 * то метод запустит сохранение списка.
		 * @throws coreException
		 */
		public function langs() {
			$mode = getRequest("param0");

			if ($mode == "do" && !is_demo()) {
				$this->saveEditedList("langs");
				$this->chooseRedirect();
			}

			$langs = langsCollection::getInstance()->getList();

			$this->setDataType("list");
			$this->setActionType("modify");
			$data = $this->prepareData($langs, "langs");
			$this->setData($data, sizeof($langs));
			$this->doData();
		}

		/**
		 * Возвращает настройки отправляемых писем для вкладки "Почта".
		 * Если передан ключевой параметр $_REQUEST['param0'] = do,
		 * то метод запустит сохранение настроек.
		 * @throws coreException
		 */
		public function mails() {
			$regedit = regedit::getInstance();

			$params = Array(
				"mails" => Array(
					"email:admin_email"	=> NULL,
					"string:email_from"	=> NULL,
					"string:fio_from"	=> NULL
				)
			);

			$mode = getRequest("param0");

			if ($mode == "do") {
				$params = $this->expectParams($params);

				if (!is_demo()) {
					$regedit->setVar("//settings/admin_email", $params['mails']['email:admin_email']);
					$regedit->setVar("//settings/email_from", $params['mails']['string:email_from']);
					$regedit->setVar("//settings/fio_from", $params['mails']['string:fio_from']);
				}

				$this->chooseRedirect();
			}

			$params['mails']['email:admin_email']	= $regedit->getVal("//settings/admin_email");
			$params['mails']['string:email_from'] = $regedit->getVal("//settings/email_from");
			$params['mails']['string:fio_from'] = $regedit->getVal("//settings/fio_from");

			$this->setDataType("settings");
			$this->setActionType("modify");
			$data = $this->prepareData($params, "settings");
			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает результаты тестов безопасности
		 * для вкладки "Безопасность"
		 * @throws coreException
		 */
		public function security() {
			$params = array(
				"security-audit"=>array()
			);

			/**
			 * @var config|ConfigTest $module
			 */
			$module = $this->module;
			$allowedTestNames = $module->getSecurityTestNames();

			foreach ($allowedTestNames as $test) {
				$params["security-audit"][$test.":security-".$test] = NULL;
			}

			$this->setDataType("settings");
			$this->setActionType("modify");

			$data = $this->prepareData($params, "settings");

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает наложения водяного знака для вкладки "Водяной знак".
		 * Если передан ключевой параметр $_REQUEST['param0'] = do,
		 * то метод запустит сохранение настроек.
		 * @throws coreException
		 */
		public function watermark() {
			$regedit = regedit::getInstance();

			$params = Array(
				"watermark" => Array(
					"string:image"  => NULL,
					"int:alpha"     => NULL,
					"select:valign" => array (
						"top"   => getLabel ("watermark-valign-top"),
						"bottom"=> getLabel ("watermark-valign-bottom"),
						"center"=> getLabel ("watermark-valign-center")
					),
					"select:halign" => array (
						"left"  => getLabel ("watermark-halign-left"),
						"right" => getLabel ("watermark-halign-right"),
						"center" => getLabel ("watermark-halign-center")
					)
				)
			);

			$mode = getRequest("param0");

			if ($mode == "do") {
				$params = $this->expectParams($params);

				if ($regedit->getKey("//settings/watermark") === false) {
					$regedit->setVar("//settings/watermark", "");
				}

				$imagePath = trim ($params['watermark']['string:image']);
				$imagePath = str_replace ("./", "", $imagePath);

				if (substr ($imagePath, 0, 1) == "/") {
					$imagePath = substr ($imagePath, 1);
				}

				if (!empty($imagePath) && file_exists ("./".$imagePath)  ) {
					$imagePath = ("./".$imagePath);

				}
				if (intval($params['watermark']['int:alpha']) > 0 && intval($params['watermark']['int:alpha']) <= 100) {
					$regedit->setVar("//settings/watermark/alpha", $params['watermark']['int:alpha']);
				}

				$regedit->setVar("//settings/watermark/image", $imagePath);
				$regedit->setVar("//settings/watermark/valign", $params['watermark']['select:valign']);
				$regedit->setVar("//settings/watermark/halign", $params['watermark']['select:halign']);

				$this->chooseRedirect();
			}

			$params['watermark']['string:image'] = $regedit->getVal("//settings/watermark/image");
			$params['watermark']['int:alpha'] = $regedit->getVal("//settings/watermark/alpha");

			$params['watermark']['select:valign'] = array (
				"top"		=> getLabel ("watermark-valign-top"),
				"bottom"	=> getLabel ("watermark-valign-bottom"),
				"center"	=> getLabel ("watermark-valign-center"),
				"value"		=> $regedit->getVal("//settings/watermark/valign")
			);
			$params['watermark']['select:halign'] = array (
				"left"		=> getLabel ("watermark-halign-left"),
				"right"		=> getLabel ("watermark-halign-right"),
				"center"	=> getLabel ("watermark-valign-center"),
				"value"		=> $regedit->getVal("//settings/watermark/halign")
			);

			$this->setDataType("settings");
			$this->setActionType("modify");
			$data = $this->prepareData($params, "settings");
			$this->setData($data);
			$this->doData();
		}
	}
?>