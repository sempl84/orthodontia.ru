<?php
	/**
	 * Класс обработчиков событий
	 */
	class ConfigHandlers {
		/**
		 * @var config $module
		 */
		public $module;

		/**
		 * Обработчик события запуска системного cron'а.
		 * Запускает системный сборщик мусора
		 * @param iUmiEventPoint $e события запуска системного cron'а
		 */
		public function runGarbageCollector(iUmiEventPoint $e) {
			if ($e->getMode() == "process") {
				try {
					$gc = new garbageCollector();
					$gc->run();
				} catch (maxIterationsExeededException $e) {}
			}
		}

		/**
		 * Обработчик событий сохранения изменений объектов и страниц
		 * через административную панель.
		 * Ведет лог изменений.
		 * @param iUmiEventPoint $event событие сохранения изменений объекта|страницы
		 * @throws coreException
		 */
		public function systemEventsNotify(iUmiEventPoint $event) {
			$eventId = $event->getEventId();

			$titleLabel = $titleLabel = 'event-' . $eventId . '-title';
			$contentLabel = 'event-' . $eventId . '-content';

			$title = getLabel($titleLabel, 'common/content/config');
			$content = getLabel($contentLabel, 'common/content/config');

			if ($titleLabel == $title) {
				return;
			}

			/**
			 * @var iUmiEntinty|iUmiHierarchyElement $element
			 */
			if ($element = $event->getRef('element')) {
				$hierarchy = umiHierarchy::getInstance();
				$oldbForce = $hierarchy->forceAbsolutePath(true);

				$params = array(
					'%page-name%' => $element->getName(),
					'%page-link%' => $hierarchy->getPathById($element->getId())
				);

				$hierarchy->forceAbsolutePath($oldbForce);
			} else {
				$params = array();
			}

			/**
			 * @var iUmiEntinty|iUmiObject $object
			 */
			if ($object = $event->getRef('object')) {
				$params['%object-name%'] = $object->getName();
				$objectTypes = umiObjectTypesCollection::getInstance();
				$objectType = $objectTypes->getType($object->getTypeId());

				if ($hierarchyTypeId = $objectType->getHierarchyTypeId()) {
					$hierarchyTypes = umiHierarchyTypesCollection::getInstance();
					$hierarchyType = $hierarchyTypes->getType($hierarchyTypeId);
					$params['%object-type%'] = $hierarchyType->getTitle();
				}
			}

			$title = str_replace(array_keys($params), array_values($params), $title);
			$content = str_replace(array_keys($params), array_values($params), $content);
			$this->module->dispatchSystemEvent($title, $content);
		}
	}
?>