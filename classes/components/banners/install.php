<?php
	/**
	 * Установщик модуля
	 */

	/**
	 * @var array $INFO реестр модуля
	 */
	$INFO = Array();
	$INFO['name'] = "banners";
	$INFO['config'] = "1";
	$INFO['default_method'] = "insert";
	$INFO['default_method_admin'] = "lists";
	$INFO['func_perms'] = "Группы прав на функционал модуля";
	$INFO['func_perms/banners_list'] = "Права на администрирование модуля";
	$INFO['func_perms/insert'] = "Права на просмотр баннеров";

	/**
	 * @var array $COMPONENTS файлы модуля
	 */
	$COMPONENTS = array();
	$COMPONENTS[] = "./classes/components/banners/admin.php";
	$COMPONENTS[] = "./classes/components/banners/class.php";
	$COMPONENTS[] = "./classes/components/banners/customAdmin.php";
	$COMPONENTS[] = "./classes/components/banners/customCommon.php";
	$COMPONENTS[] = "./classes/components/banners/customMacros.php";
	$COMPONENTS[] = "./classes/components/banners/i18n.en.php";
	$COMPONENTS[] = "./classes/components/banners/i18n.php";
	$COMPONENTS[] = "./classes/components/banners/includes.php";
	$COMPONENTS[] = "./classes/components/banners/install.php";
	$COMPONENTS[] = "./classes/components/banners/lang.en.php";
	$COMPONENTS[] = "./classes/components/banners/lang.php";
	$COMPONENTS[] = "./classes/components/banners/macros.php";
	$COMPONENTS[] = "./classes/components/banners/permissions.php";
?>