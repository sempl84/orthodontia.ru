<?php

	/**
	 * Class def_module
	 * Базовый класс модулей системы
	 */
	abstract class def_module {
		/**
		 * @var bool, если true то при возникновении паники
		 * будет выброшено исключение вместо выполнения редиректа
		 */
		public static $noRedirectOnPanic = false;
		/** @var string имя шаблона по учмолчанию */
		public static $defaultTemplateName = 'default';
		/** @var null|bool является ли текущий режим режимом xslt */
		public static $xsltResultMode = null;

		/**
		 * Список макросов с выводом расширенных полей для XSLT
		 * @var array
		 */
		protected static $macrosesXsltExtendedResult = array(array(), array());
		/** @var int количество элементов, отображаемых на странице */
		public $per_page = 20;
		/** @var string префикс языковой версии */
		public $pre_lang;
		/** @var mixed объект основных вкладок модуля */
		public $common_tabs = null;
		/** @var mixed объект вкладок настроек модуля */
		public $config_tabs = null;
		/** @var array список возникших ошибок */
		protected $errors = array();
		/** @var string адрес страницы, на которой возникла ошибка */
		protected $errorPage = '';
		/**
		 * @var array $methodsTemplates установленные шаблоны системных методов [method => template_id]
		 */
		private static $methodsTemplates = [];
		/**
		 * @var array $libs загруженные файлы классов
		 */
		private $libs = array();
		/**
		 * @var array $classes подключенные классы
		 */
		private $classes = array();
		/**
		 * @var array $methods подключенные методы
		 */
		private $methods = array();

		/**
		 * Подключает класс к модулю
		 * @param string $className имя подключаемого класса
		 * @param bool $allowOverriding поддерживается ли перегрузка методов
		 * @return bool
		 */
		protected function __implement($className, $allowOverriding = false) {
			try {
				$classReflection = new ReflectionClass($className);

				if (!$classReflection->isInstantiable()) {
					return false;
				}

				$methods = $classReflection->getMethods(ReflectionMethod::IS_PUBLIC);

				if (count($methods) == 0) {
					return false;
				}

				$classInstance = new $className($this);
				$classInstance->module = $this;
				$this->classes[$className] = $classInstance;
				/**
				 * @var ReflectionMethod $method
				 */
				foreach ($methods as $method) {
					if (isset($this->methods[$method->getName()]) && !$allowOverriding) {
						continue;
					}

					if ($method->isStatic()) {
						$this->methods[$method->getName()] = $className;
					} else {
						$this->methods[$method->getName()] = $this->classes[$className];
					}
				}
			} catch (ReflectionException $e) {
				return false;
			}

			return true;
		}

		/**
		 * Возвращает экземпляр подключенного класса
		 * @param string $class имя класса
		 * @return object
		 * @throws coreException
		 */
		public function getImplementedInstance($class) {
			if (!is_string($class) || !$this->isClassImplemented($class)) {
				throw new coreException('Class ' . $class . ' not implemented');
			}

			return $this->classes[$class];
		}

		/**
		 * Возвращает экземпляр основного класса административной панели
		 * @return object
		 * @throws coreException
		 */
		public function getAdminInstance() {
			return $this->getImplementedInstance($this->getAdminClassName());
		}

		/**
		 * Подключен ли класс
		 * @param string $class имя класса
		 * @return bool
		 */
		public function isClassImplemented($class) {
			return isset($this->classes[$class]);
		}

		/**
		 * Устанавливает список дополнительных полей и групп для результатов парсинга макросов
		 * @param array $extProps имена полей
		 * @param array $extGroups имена групп
		 */
		public static function setMacrosExtendedResult($extProps = array(), $extGroups = array()) {
			self::$macrosesXsltExtendedResult = array($extProps, $extGroups);
		}

		/**
		 * Возвращает список доп полей для результата макроса
		 * @return array массив имен полей
		 */
		public static function getMacrosExtendedProps() {
			return self::$macrosesXsltExtendedResult[0];
		}

		/**
		 * Возвращает список доп групп для результата макроса
		 * @return array массив имен групп
		 */
		public static function getMacrosExtendedGroups() {
			return self::$macrosesXsltExtendedResult[1];
		}

		/**
		 * Алиас def_module::setupTemplate();
		 */
		public function getMethodTemplateId($method) {
			return self::setupTemplate($method);
		}

		/**
		 * Удаляет идентификатор шаблона, по которому нужно отрисовать страницу,
		 * данные которой возвращает указанный метод
		 * @param string $method имя метода
		 */
		public function flushMethodTemplateId($method) {
			self::$methodsTemplates[$method] = null;
		}

		/**
		 * Устанавливает идентификатор шаблона, по которому нужно отрисовать страницу,
		 * данные которой возвращает указанный метод.
		 * Возвращает результат операции
		 * @param string $method имя метода
		 * @param int $templateId идентификатор шаблона
		 * @return bool
		 */
		public function setMethodTemplateId($method, $templateId) {
			if (!$this->isMethodExists($method)) {
				return false;
			}

			$umiTemplates = templatesCollection::getInstance();

			if (!$umiTemplates->isExists($templateId)) {
				return false;
			}

			self::$methodsTemplates[$method] = $templateId;

			return true;
		}

		/**
		 * Магический метод, пытается найти переданный метод
		 * среди методов подключенных классов,
		 * если метод найден - вызывает его
		 * @param string $method имя метода
		 * @param array $args аргументы вызова
		 * @return mixed|string
		 */
		public function __call($method, $args) {
			$args = (!is_array($args)) ? array() : $args;

			if (isset($this->methods[$method])) {
				$class = $this->methods[$method];
				return call_user_func_array(array($class, $method), $args);
			}

			$cmsController = cmsController::getInstance();
			$cmsController->langs[get_class($this)][$method] = getLabel('label-error');

			if ($cmsController->getCurrentMode() === 'admin') {
				return getLabel('error-call-non-existent-method');
			}

			if ($cmsController->getCurrentModule() === get_class($this) && $cmsController->getCurrentMethod() === $method) {
				/**
				 * @var content $contentModule
				 */
				$contentModule = $cmsController->getModule('content');

				if (!$contentModule instanceof def_module) {
					return '';
				}

				return $contentModule->gen404();
			}

			return '';
		}


		/**
		 * Возвращает идентификатор шаблона, по которому нужно отрисовать страницу,
		 * данные которой возвращает указанный метод
		 * @param string $method имя метода
		 * @return int|null
		 */
		public static function setupTemplate($method) {
			if (isset(self::$methodsTemplates[$method])) {
				return self::$methodsTemplates[$method];
			}

			return null;
		}

		/** Конструктор */
		public function __construct() {
			$cmsController = cmsController::getInstance();
			$this->lang = $cmsController->getCurrentLang()->getPrefix();
			$includesFile = SYS_MODULES_PATH . get_class($this) . '/includes.php';

			if (file_exists($includesFile) && !defined('SKIP_MODULES_INCLUDES')) {
				require_once $includesFile;
			}
		}

		/**
		 * Возвращает список имен модулей системы
		 * с соответствующими значениями индексами их сортировки
		 * @return array
		 */
		public function getSortedModulesList() {
			$priorityList = Array(
				// user modules (priority < 100)
				'events'		=> 1,
				'content'		=> 2,
				'news'			=> 3,
				'menu'		=> 4,
				'forum'			=> 5,
				'blogs20'		=> 6,
				'vote'			=> 7,
				'comments'		=> 8,
				'photoalbum'	=> 9,
				'webforms'		=> 10,
				'dispatches'	=> 11,
				'faq'			=> 12,
				'eshop'			=> 13,
				'emarket'		=> 14,
				'catalog'		=> 15,
				'users'			=> 16,
				'banners'		=> 17,
				'seo'			=> 18,
				'stat'			=> 19,
				'social_networks'	=> 20,
				'exchange'		=> 21,
				// administrative modules (priority > 100)
				'data'			=> 101,
				'config'		=> 102,
				'backup'		=> 103,
				'autoupdate'	=> 104,
				'webo'			=> 105,
				'search'		=> 106,
				'filemanager'	=> 107
			);

			$sysModules = array('config', 'trash' ,'search', 'autoupdate');
			$utilModules = array('data','backup', 'webo', 'filemanager');

			$modulesList = regedit::getInstance()->getList('//modules');
			$permissions = permissionsCollection::getInstance();

			$result = array();
			foreach($modulesList as $module) {
				list( $module ) = $module;
				if ( $permissions->isAllowedModule(false, $module) == false ) {
					continue;
				}
				$priority = isset($priorityList[$module]) ? $priorityList[$module] : 99;
				$result[$module] = $priority;
			}

			$isTrashAllowed = $permissions->isAllowedMethod( $permissions->getUserId(), "data", "trash" );
			if ( system_get_skinName() == "mac" && $isTrashAllowed != false ) {
				$result['trash'] = 999;
			}
			natsort($result);

			foreach($result as $module => $priority) {
				if(in_array($module, $sysModules))
					$type = 'system';
				else if(in_array($module, $utilModules))
					$type = 'util';
				else $type = null;

				$moduleInfo = array();
				$moduleInfo['name'] = $module;
				$moduleInfo['label'] = getLabel("module-" . $module);
				$moduleInfo['type'] = $type;

				$result[$module] = $moduleInfo;
			}

			return $result;
		}

		/**
		 * Возвращает объект основных вкладок модуля
		 * @return adminModuleTabs|bool|mixed
		 */
		public function getCommonTabs() {
			$cmsController = cmsController::getInstance();
			$currentModule = $cmsController->getCurrentModule();
			$selfModule = get_class($this);

			if (($currentModule != $selfModule) && ($currentModule != false && $selfModule != 'users')) return false;
			if (!$this->common_tabs instanceof adminModuleTabs) {
				$this->common_tabs = new adminModuleTabs("common");
			}
			return $this->common_tabs;
		}

		/**
		 * Возвращает объект вкладок настроек модуля
		 * @return adminModuleTabs|bool|mixed
		 */
		public function getConfigTabs() {
			$cmsController = cmsController::getInstance();

			if ($cmsController->getCurrentModule() != get_class($this)) {
				return false;
			}

			if (!$this->config_tabs instanceof adminModuleTabs) {
				$this->config_tabs = new adminModuleTabs("config");
			}

			return $this->config_tabs;
		}

		/**
		 * Вызывает метод модуля
		 * @param string $method_name имя метода
		 * @param array $args аргументы для вызываемого метода
		 * @return mixed|void
		 */
		public function cms_callMethod($method_name, $args) {
			if(!$method_name) return;

			$aArguments = array();
			if(USE_REFLECTION_EXT && class_exists('ReflectionMethod')) {
				try {
					$oReflection   = new ReflectionMethod($this, $method_name);
					$iNeedArgCount = max($oReflection->getNumberOfRequiredParameters(), count($args));
					if($iNeedArgCount) $aArguments = array_fill(0, $iNeedArgCount, 0);
				} catch(Exception $e) {}
			}

			for($i=0; $i<count($args); $i++) $aArguments[$i] = $args[$i];

			if(count($aArguments) && !(empty($args[0]) && count($args) === 1)) {
				return call_user_func_array(array($this, $method_name), $aArguments);
			} else {
				return $this->$method_name();
			}
		}

		/** Подключает кастомы из шаблонов */
		public function loadTemplateCustoms() {
			$cmsController = cmsController::getInstance();

			if ($resourcesDir = $cmsController->getResourcesDirectory()) {
				$extraCustomsDir = $resourcesDir . '/classes/modules';

				if (is_dir($extraCustomsDir)) {
					$includesFile = realpath($extraCustomsDir) . '/' . get_class($this) . '/class.php';

					if (file_exists($includesFile)) {
						require_once $includesFile;
						$className = get_class($this) . '_custom';
						if (!in_array($className, $this->methods)) {
							$this->__implement($className, true);
						}
					}
				}
			}
		}

		/**
		 * Производит загрузку общих файлов расширений.
		 * Загрузка производится из директории [имя_модуля]/ext с учётом префикса файлов.
		 */
		public function loadCommonExtension() {
			$extensionFiles = SYS_MODULES_PATH . get_class($this) . '/ext/';
			if (file_exists($extensionFiles)) {
				$files = glob($extensionFiles . '/includes_*.php');
				if (is_array($files)) {
					foreach ($files as $filename) {
						require_once $filename;
					}
				}
				$files = glob($extensionFiles . '/common_*.php');
				if (is_array($files)) {
					foreach ($files as $filename) {
						$fileName = str_replace($extensionFiles . '/', '', $filename);
						$className = str_replace('.php', '', $fileName);
						$this->__loadLib($fileName, $extensionFiles);
						$this->__implement($className, true);
					}
				}
			}
		}

		/**
		 * Производит загрузку админских файлов расширений.
		 * Загрузка производится из директории [имя_модуля]/ext с учётом префикса файлов.
		 */
		public function loadAdminExtension() {
			$extensionFiles = SYS_MODULES_PATH . get_class($this) . '/ext/';
			if (file_exists($extensionFiles)) {
				$cmsController = cmsController::getInstance();
				if ($cmsController->getCurrentMode() == "admin") {
					$files = glob($extensionFiles . '/admin_*.php');
					if (is_array($files)) {
						foreach ($files as $filename) {
							$fileName = str_replace($extensionFiles . '/', '', $filename);
							$className = str_replace('.php', '', $fileName);
							$this->__loadLib($fileName, $extensionFiles);
							$this->__implement($className, true);
						}
					}
				}
			}
		}

		/**
		 * Производит загрузку сайтовых файлов расширений и подключение событий.
		 * Загрузка производится из директории [имя_модуля]/ext с учётом префикса файлов.
		 */
		public function loadSiteExtension() {
			$extensionFiles = SYS_MODULES_PATH . get_class($this) . '/ext/';

			if (file_exists($extensionFiles)) {
				$cmsController = cmsController::getInstance();
				if ($cmsController->getCurrentMode() != "admin") {
					$files = glob($extensionFiles . '/site_*.php');
					if (is_array($files)) {
						foreach ($files as $filename) {
							$fileName = str_replace($extensionFiles . '/', '', $filename);
							$className = str_replace('.php', '', $fileName);
							$this->__loadLib($fileName, $extensionFiles);
							$this->__implement($className, true);
						}
					}
					$files = glob($extensionFiles . '/__events_*.php');
					if (is_array($files)) {
						foreach ($files as $filename) {
							$fileName = str_replace($extensionFiles . '/', '', $filename);
							$className = str_replace('.php', '', $fileName);
							$this->__loadLib($fileName, $extensionFiles);
							$this->__implement($className, true);
						}
					}
				}
			}
		}

		/**
		 * Проверяет валидность HTTP_REFERER
		 * @return bool
		 */
		public static function checkHTTPReferer() {
			preg_match('|^http(?:s)?:\/\/(?:www\.)?([^\/]+)|ui', getServer('HTTP_REFERER'), $matches);

			if (!isset($matches[1]) || count($matches[1])!=1) {
				return false;
			}

			$originalDomain = $matches[1];

			$domainNames = array();
			$domainNames[] = $originalDomain;
			$domainNames[] = 'www.' . $originalDomain;
			$domainNames[] = (string) preg_replace('/(:\d+)/', '', $originalDomain);

			$domainsCollection = domainsCollection::getInstance();

			foreach ($domainNames as $domainName) {
				if (is_numeric($domainsCollection->getDomainId($domainName))) {
					return true;
				}
			}

			return false;
		}

		/**
		 * Устанавливает модуль
		 * @param array $INFO параметры установки модуля
		 */
		public static function install($INFO) {
			$xpath = '//modules/' . $INFO['name'];
			$regedit = regedit::getInstance();

			$regedit->setVar($xpath, $INFO['name']);

			if(is_array($INFO)) {
				foreach($INFO as $var => $module_param) {
					$val = $module_param;
					$regedit->setVar($xpath . "/" . $var, $val);
				}
			}
		}

		/** Производит удаление текущего модуля */
		public function uninstall() {
			$className = get_class($this);
			$regedit = regedit::getInstance();
			$defaultModuleAdmin = $regedit->getVal('//settings/default_module_admin');

			if ($defaultModuleAdmin == $className) {
				$regedit->setVal('//settings/default_module_admin', 'content');
			}

			$regedit->getKey('//modules/' . $className);
			$regedit->delVar('//modules/' . $className);

		}

		/**
		 * Алиас simpleRedirect()
		 * @param string $url адрес
		 * @param bool $ignoreErrorParam нужно ли удалить параметр с отсылкой на ошибку из адреса
		 * @throws coreException
		 */
		public function redirect($url, $ignoreErrorParam = true) {
			self::simpleRedirect($url, $ignoreErrorParam);
		}

		/**
		 * Производит перенаправление на указанный адрес
		 * @param string $url адрес
		 * @param bool $ignoreErrorParam нужно ли удалить параметр с отсылкой на ошибку из адреса
		 * @throws coreException
		 */
		public static function simpleRedirect($url, $ignoreErrorParam = true) {
			if (getRequest('redirect_disallow')) {
				return;
			}

			if (!$url) {
				$url = cmsController::getInstance()->pre_lang . '/';
			}

			if ($ignoreErrorParam) {
				$url = self::removeErrorCodeFromUrl($url);
			}

			umiHierarchy::getInstance()->__destruct();
			outputBuffer::current()->redirect($url);
		}

		/**
		 * При необъходимости добавляет суффикс или слэш к url-адресу
		 * и производит редирект
		 */
		public static function requireSlashEnding() {
			if(getRequest('is_app_user') !== null) {
				return;
			}

			if(getRequest('xmlMode') === 'force' || count($_POST) > 0) {
				return;
			}

			if (getRequest('jsonMode') === 'force' || count($_POST) > 0) {
				return;
			}

			$uri = getServer('REQUEST_URI');

			if($uri == '/') {
				return;
			}
			$uriInfo = parse_url($uri);

			if (cmsController::getInstance()->getCurrentMode() === 'admin') {
				if(substr($uriInfo['path'], -1, 1) != "/") {
					$uri = $uriInfo['path'] . "/";
					if(isset($uriInfo['query']) && $uriInfo['query']) {
						$uri .= "?" . $uriInfo['query'];
					}
					self::simpleRedirect($uri);
				}
				return;
			}

			if ($urlSuffix = mainConfiguration::getInstance()->get('seo', 'url-suffix')){
				$pos = strrpos($uriInfo['path'], $urlSuffix);
				if ($pos === false || !($pos + strlen($urlSuffix) == strlen($uriInfo['path']))) {
					if ($uriInfo['path'] == '/') {
						return;
					} else {
						$uri = rtrim($uriInfo['path'], '/') . $urlSuffix;
					}
					if(isset($uriInfo['query']) && $uriInfo['query']) {
						$uri .= "?" . $uriInfo['query'];
					}
					self::simpleRedirect($uri);
				}
			}
		}

		/**
		 * Загружает файла класса для последующей имлементации его в модуле
		 * @param string $file имя подключаемого файла
		 * @param string $path путь до файла
		 * @param string|null $parentClassName имя классса, в рамках которого загружается файл класса
		 * @return bool
		 */
		protected function __loadLib($file, $path = "", $parentClassName = null) {
			$parentClassName = (is_null($parentClassName)) ? get_class($this) : $parentClassName;
			$filePath = ($path) ? $path . $file : SYS_MODULES_PATH . $parentClassName . "/" . $file;

			if (!file_exists($filePath)) {
				return false;
			}

			if (!isset($this->libs[$filePath])) {
				require_once $filePath;
				$this->libs[$filePath] = true;
			}

			return true;
		}

		/**
		 * Изменяет текущий хедер модуля
		 * @param string $header новый хедер
		 */
		public function setHeader($header) {
			$cmsControllerInstance = cmsController::getInstance();
			$cmsControllerInstance->currentHeader = $header;
		}

		/**
		 * Изменяет текущий заголовок модуля
		 * @param string $title новый заголовок
		 * @param int $mode, если не 0, то значение заголовка будет получено из реестра
		 */
		protected function setTitle($title = "", $mode = 0) {
			$cmsControllerInstance = cmsController::getInstance();
			$umiRegistry = regedit::getInstance();

			if ($title) {
				if ($mode) {
					$cmsControllerInstance->currentTitle = $umiRegistry->getVal('//domains/' . $_REQUEST['domain'] . '/title_pref_' . $_REQUEST['lang']) . $title;
				} else {
					$cmsControllerInstance->currentTitle = $title;
				}
			} else {
				$cmsControllerInstance->currentTitle = $cmsControllerInstance->currentHeader;
			}
		}

		/**
		 * Алиас setHeader()
		 * @param string $h1 новый хедер
		 */
		protected function setH1($h1) {
			$this->setHeader($h1);
		}

		/**
		 * Выводит сообщение и завершает выполнение скрипта
		 * @param string $output сообщение для вывода
		 * @param bool|string $ctype значение для заголовка Content-type
		 */
		public function flush($output = "", $ctype = false) {
			if($ctype !== false) {
				header("Content-type: " . $ctype);
			}

			echo $output;
			exit();
		}

		/**
		 * @deprecated
		 * Больше не успользуется
		 */
		public static function loadTemplatesMeta($filepath = "") {
			$arguments = func_get_args();
			$templates = call_user_func_array(array('def_module', "loadTemplates"), $arguments);

			for($i=1; $i < count($arguments); $i++) {
				$templates[$i-1] = $templates[$i-1] ? array("#template" => $templates[$i-1], "#meta" => array("name" => $arguments[$i], "file" => $filepath)) : $templates[$i-1];
			}

			return $templates;
		}

		/**
		 * @static
		 * Загружает шаблоны, используя шаблонизатор в зависимости от режима работы макросов,
		 * возвращает запрошенные блоки
		 * @param string $filePath - путь к источнику шаблонов
		 * @return array
		 */
		public static function loadTemplates($filePath = "") {
			$args = func_get_args();

			$templater = self::isXSLTResultMode() ? 'umiTemplaterXSLT' : 'umiTemplaterTPL';

			if (!self::isXSLTResultMode() && !is_file($filePath)) {
				$cmsController = cmsController::getInstance();
				// получаем полный путь к tpl-шаблону
				$defaultLang = langsCollection::getInstance()->getDefaultLang();
				$currentLang = $cmsController->getCurrentLang();
				$resourcesDir = $cmsController->getResourcesDirectory();

				$langPrefix = '';
				if ($defaultLang && $currentLang && ($defaultLang->getId() != $currentLang->getId())) {
					$langPrefix = $currentLang->getPrefix();
				}

				if (substr($filePath, -4) === '.tpl') {
					$filePath = substr($filePath, 0, -4);
				}

				$files = array();
				if ( system_is_mobile() ) {
					$pathArray = explode('/', $filePath);
					$mobileFilePath = '/mobile/' . array_pop($pathArray);
					$mobileFilePath = implode('/', $pathArray) . $mobileFilePath;
					if ( strlen($langPrefix) ) {
						$files[] = $mobileFilePath . '.' . $langPrefix;
					}
					$files[] = $mobileFilePath;
				}

				if ( strlen($langPrefix) ) {
					$files[] = $filePath . '.' . $langPrefix;
				}
				$files[] = $filePath;

				$dir = rtrim(($resourcesDir ? $resourcesDir : CURRENT_WORKING_DIR), '/') . '/tpls/';

				foreach($files as $filePath) {
					$filePath = $dir . $filePath. '.tpl';
					if ( is_file($filePath) ) {
						break;
					}
				}

				$args[0] = $filePath;
			}

			$result = call_user_func_array(array(
				$templater, 'getTemplates'
			), $args);

			return $result;
		}

		/**
		 * @static
		 * Загружает шаблоны для формирования писем
		 * Сначала пытаемся загрузить XSLT-шаблон, если шаблон не найден, пытаемся загрузить TPL-шаблон
		 *
		 * @param string $filePath путь до шаблона
		 * @return array - массив шаблонов
		 * @throws coreException
		 */
		public static function loadTemplatesForMail($filePath = "") {
			if (substr($filePath, -4) === '.tpl') {
				$filePath = substr($filePath, 0, -4);
			}
			// fix for mail / mails paths for xslt
			$xslFilePath = $filePath;
			if (strpos($xslFilePath, "mail") !== false) {
				$xslFilePath = str_replace(array("mail/", "mails/"), array('', ''), $xslFilePath);
			}

			if ($resourcesDir = cmsController::getInstance()->getResourcesDirectory()) {
				$xslSourcePath = $resourcesDir . "/xslt/mail/" . $xslFilePath . ".xsl";
				$tplSourcePath = $resourcesDir . "/tpls/" . $filePath . ".tpl";
			} else {
				$xslSourcePath = CURRENT_WORKING_DIR . "/xsltTpls/mail/" . $xslFilePath . ".xsl";
				$tplSourcePath = CURRENT_WORKING_DIR . "/tpls/" . $filePath . ".tpl";
			}

			$templaterClass = null;

			if (is_file($xslSourcePath)) {
				$templaterClass = 'umiTemplaterXSLT';
				$sourcePath = $xslSourcePath;
			} elseif (is_file($tplSourcePath)) {
				$templaterClass = 'umiTemplaterTPL';
				$sourcePath = $tplSourcePath;
			} else {
				throw new coreException(getLabel('error-cannot-connect-mail-template') . $filePath, 2);
			}

			$args = func_get_args();
			$args[0] = $sourcePath;

			$result = call_user_func_array(array(
				$templaterClass, 'getTemplates'
			), $args);

			return $result;
		}

		/**
		 * @static
		 * Обрабатывает TPL - макросы в контенте, используя TPL-шаблонизатор
		 *
		 * @param string $content
		 * @param mixed $scopeElementId - id страницы в качестве области видимости блока
		 * @param mixed $scopeObjectId - id объекта в качестве области видимости блока
		 * @param array $parseVariables - переменные, для парсинга контента
		 * @return string
		 */
		public static function parseTPLMacroses($content, $scopeElementId = false, $scopeObjectId = false, $parseVariables = array()) {
			if (strpos($content, '%') === false) return $content;

			$tplTemplater = umiTemplater::create('TPL');
			$tplTemplater->setScope($scopeElementId, $scopeObjectId);
			return $tplTemplater->parse($parseVariables, $content);
		}

		/**
		 * @static
		 * Выполняет разбор шаблона, используя необходимый шаблонизатор в зависимости от режима работы макросов
		 *
		 * @param mixed $template - шаблон для разбора
		 * @param array $arr - массив переменнх
		 * @param bool|int $parseElementPropsId - установить id страницы в качестве области видимости блока
		 * @param bool|int $parseObjectPropsId  - установить id объекта в качестве области видимости блока
		 * @param null|bool $xsltResultMode - принудительно устанавливает режим работы макросов перед разбором
		 * и восстанавливает предыдущий режим работы в конце работы
		 * @return mixed - результат разбора шаблона
		 */
		public static function parseTemplate($template, $arr, $parseElementPropsId = false, $parseObjectPropsId = false, $xsltResultMode = null) {
			if (!is_array($arr)) $arr = array();

			$oldResultMode = null;
			if (is_bool($xsltResultMode)) {
				$oldResultMode = self::isXSLTResultMode($xsltResultMode);
			}
			if (self::isXSLTResultMode()) {
				$result = array();
				$extProps = self::getMacrosExtendedProps();
				$extGroups = self::getMacrosExtendedGroups();
				if ((!empty($extProps) || !empty($extGroups)) && ($parseElementPropsId || $parseObjectPropsId)) {
					if ($parseElementPropsId) {
						$entity = umiHierarchy::getInstance()->getElement($parseElementPropsId);
						if ($entity) $entity = $entity->getObject();
					} else {
						$entity = umiObjectsCollection::getInstance()->getObject($parseObjectPropsId);
					}
					/**
					 * @var umiObject $entity
					 */
					if ($entity) {
						$extPropsInfo = array();
						foreach ($extProps as $fieldName) {
							if ($fieldName == 'name' && !isset($arr['attribute:name'], $arr['@name'])) {
								$arr['@name'] = $entity->getName();
							} elseif ($extProp = $entity->getPropByName($fieldName)) {
								$extPropsInfo[] = $extProp;
							}
						}
						if (count($extPropsInfo)) {
							if (!isset($arr['extended'])) $arr['extended'] = array();
							$arr['extended']['properties'] = array('+property' => $extPropsInfo);
						}

						$extGroupsInfo = array();
						foreach ($extGroups as $groupName) {
							if ($group = $entity->getType()->getFieldsGroupByName($groupName)) {
								$groupWrapper = translatorWrapper::get($group);
								$extGroupsInfo[] = $groupWrapper->translateProperties($group, $entity);
							}
						}

						if (count($extGroupsInfo)) {
							if (!isset($arr['extended'])) $arr['extended'] = array();
							$arr['extended']['groups'] = array('+group' => $extGroupsInfo);
						}
					}
				}
				$keysCache = &xmlTranslator::$keysCache;
				foreach($arr as $key => $val) {
					if ($val === null || $val === false || $val === '') {
						continue;
					}
					if (is_array($val)) {
						$val = self::parseTemplate($template, $val);
					}
					if (!isset($keysCache[$key])) {
						$keysCache[$key] = xmlTranslator::getKey($key);
					}
					list($subKey, $realKey) = $keysCache[$key];
					if($subKey === 'subnodes') {
						$result[$realKey] = array(
							'nodes:item' => $val
						);
						continue;
					}

					$result[$key] = $val;
				}
				return $result;
			} else {
				$templater = umiTemplater::create('TPL');
				$variables = array();
				foreach($arr as $m => $v) {
					$m = self::getRealKey($m);

					if(is_array($v)) {
						$res = "";
						$v = array_values($v);
						$sz = count($v);
						for($i = 0; $i < $sz; $i++) {
							$str = $v[$i];

							$listClassFirst = ($i == 0) ? "first" : "";
							$listClassLast = ($i == $sz-1) ? "last" : "";
							$listClassOdd = (($i+1) % 2 == 0) ? "odd" : "";
							$listClassEven = $listClassOdd ? "" : "even";
							$listPosition = ($i + 1);
							$listComma = $listClassLast ? '' : ', ';

							$from = Array(
								'%list-class-first%', '%list-class-last%', '%list-class-odd%', '%list-class-even%', '%list-position%',
								'%list-comma%'
							);
							$to = Array(
								$listClassFirst, $listClassLast, $listClassOdd, $listClassEven, $listPosition, $listComma
							);
							$t_res = str_replace($from, $to, $str);
							$res .= is_array($t_res) ? implode('',$t_res) : $t_res;
						}
						$v = $res;
					}
					if(!is_object($v)) {
						$variables[$m] = $v;
					}
				}
				$arr = $variables;
			}
			$templater->setScope($parseElementPropsId, $parseObjectPropsId);

			$result = $templater->parse($arr, $template);
			$result = $templater->replaceCommentsAfterParse($result);

			if ($oldResultMode !== null) {
				self::isXSLTResultMode($oldResultMode);
			}

			return $result;
		}

		/**
		 * Сокращенная запись loadTemplate/parseTemplate
		 *
		 * @param string $template Файл шаблона
		 * @param string $block Блок вывода
		 * @param array $blockArray Значения
		 * @param bool|int $elementId Элемент
		 *
		 * @return mixed
		 */
		public static function renderTemplate($template, $block, $blockArray = array(), $elementId = false) {
			list($tpl) = def_module::loadTemplates($template, $block);
			return def_module::parseTemplate($tpl, $blockArray, $elementId);
		}

		/**
		 * @static
		 * Выполняет разбор шаблона для отправки письма
		 * Если в template пришел URI шаблона, для обработки используется umiTemplaterXSTL
		 * @param string $template - шаблон для разбора
		 * @param array $arr - массив переменнх
		 * @param bool|int $parseElementPropsId - установить id страницы в качестве области видимости блока
		 * @param bool|int $parseObjectPropsId  - установить id объекта в качестве области видимости блока
		 * @return mixed - результат разбора шаблона
		 * @throws publicException
		 */
		public static function parseTemplateForMail($template, $arr, $parseElementPropsId = false, $parseObjectPropsId = false) {
			if (strpos($template, 'file://') === 0) {
				// Используем xslt-шаблонизатор
				$templateURL = @parse_url($template);

				if (!is_array($templateURL)) {
					throw new publicException(getLabel('error-cannot-process-template') . $template);
				}

				$templateSource = $templateURL['path'];
				$templateFragment = (isset($templateURL['fragment']) && strlen($templateURL['fragment'])) ? $templateURL['fragment'] : 'result';

				$templater = umiTemplater::create('XSLT', $templateSource);
				return $templater->parse(array(
					$templateFragment => $arr
				));
			} else {
				// Используем tpl-шаблонизатор
				return def_module::parseTemplate($template, $arr, $parseElementPropsId, $parseObjectPropsId, false);
			}
		}
		/**
		 * @deprecated
		 * Используйте def_module::parseTemplateForMail
		 */
		public static function parseContent($template, $arr, $parseElementPropsId = false, $parseObjectPropsId = false) {
			return self::parseTemplateForMail($template, $arr, $parseElementPropsId, $parseObjectPropsId);
		}

		/**
		 * Возвращает часть строки до или после разделителя
		 * @param string $key исходная строка
		 * @param bool $reverse, если true, то будет возвращена строка перед разделителем,
		 * если false, то после резделителя
		 * @return string
		 */
		static public function getRealKey($key, $reverse = false) {
			$shortKeys = array('@', '#', '+', '%', '*');

			if(in_array(substr($key, 0, 1), $shortKeys)) {
				return substr($key, 1);
			}

			if($pos = strpos($key, ":")) {
				++$pos;
			} else {
				$pos = 0;
			}

			return $reverse ? substr($key, 0, $pos - 1) : substr($key, $pos);
		}

		/**
		 * Форматирует сообщение форума
		 * @param string $message исходное сообщение
		 * @param int $b_split_long_mode, если 0, то слишком длинные слова в сообщении
		 * будут разделены
		 * @return mixed|string
		 */
		public function formatMessage($message, $b_split_long_mode = 0) {
			static $bb_from;
			static $bb_to;

			$oldResultTMode = $this->isXSLTResultMode(false);

			try {
				list($quote_begin, $quote_end) = $this->loadTemplates('quote/default', 'quote_begin', 'quote_end');
			} catch (publicException $e) {
				$quote_begin = "<div class='quote'>";
				$quote_end = "</div>";
			}

			if (self::isXSLTResultMode()) {
				$quote_begin = "<div class='quote'>";
				$quote_end = "</div>";
			}

			if (!(is_array($bb_from) && is_array($bb_to) && count($bb_from) === count($bb_to))) {
				try {
					list($bb_from, $bb_to) = $this->loadTemplates('bb/default', 'bb_from', 'bb_to');
					if (!(is_array($bb_from) && is_array($bb_to) && count($bb_from) === count($bb_to) && count($bb_to))) {
						$bb_from = Array("[b]", "[i]", "[/b]", "[/i]",
							"[quote]", "[/quote]", "[u]", "[/u]", "\r\n"
						);

						$bb_to   = Array("<strong>", "<em>", "</strong>", "</em>",
							$quote_begin, $quote_end, "<u>", "</u>", "<br />"
						);
					}
				} catch (publicException $e) {
					$bb_from = Array("[b]", "[i]", "[/b]", "[/i]",
						"[quote]", "[/quote]", "[u]", "[/u]", "\r\n"
					);

					$bb_to   = Array("<strong>", "<em>", "</strong>", "</em>",
						$quote_begin, $quote_end, "<u>", "</u>", "<br />"
					);
				}
			}

			$openQuoteCount = substr_count(wa_strtolower($message), "[quote]");
			$closeQuoteCount = substr_count(wa_strtolower($message), "[/quote]");

			if($openQuoteCount > $closeQuoteCount) {
				$message .= str_repeat("[/quote]", $openQuoteCount - $closeQuoteCount);
			}
			if($openQuoteCount < $closeQuoteCount) {
				$message = str_repeat("[quote]", $closeQuoteCount - $openQuoteCount) . $message;
			}

			$message = preg_replace("`((http)+(s)?:(//)|(www\.))((\w|\.|\-|_)+)(/)?([/|#|?|&|=|\w|\.|\-|_]+)?`i", "[url]http\\3://\\5\\6\\8\\9[/url]", $message);

			$message = str_ireplace($bb_from, $bb_to, $message);
			$message = str_ireplace("</h4>", "</h4><p>", $message);
			$message = str_ireplace("</div>", "</p></div>", $message);

			$message = str_replace(".[/url]", "[/url].", $message);
			$message = str_replace(",[/url]", "[/url],", $message);

			$message = str_replace(Array("[url][url]", "[/url][/url]"), Array("[url]", "[/url]"), $message);

			// split long words
			if ($b_split_long_mode === 0) { // default
				$arr_matches = array();
				$b_succ = preg_match_all("/[^\s^<^>]{70,}/u", $message, $arr_matches);
				if ($b_succ && isset($arr_matches[0]) && is_array($arr_matches[0])) {
					foreach ($arr_matches[0] as $str) {
						$s = "";
						if (strpos($str, "[url]") === false) {
							for ($i = 0; $i<wa_strlen($str); $i++) $s .= wa_substr($str, $i, 1).(($i % 30) === 0 ? " " : "");
							$message = str_replace($str, $s, $message);
						}
					}
				}
			} elseif ($b_split_long_mode === 1) {
				// TODU abcdef...asdf
			}

			if (preg_match_all("/\[url\]([^А-я^\r^\n^\t]*)\[\/url\]/U", $message, $matches, PREG_SET_ORDER)) {
				for ($i=0; $i<count($matches); $i++) {
					$s_url = $matches[$i][1];
					$i_length = strlen($s_url);
					if ($i_length>40) {
						$i_cutpart = ceil(($i_length-40)/2);
						$i_center = ceil($i_length/2);

						$s_url = substr_replace($s_url, "...", $i_center-$i_cutpart, $i_cutpart*2);
					}
					$message = str_replace($matches[$i][0], "<a href='/go-out.php?url=".$matches[$i][1]."' target='_blank' title='" . getLabel('link-will-open-in-new-window') . "'>".$s_url."</a>", $message);
				}
			}

			$message = str_replace("&", "&amp;", $message);

			$message = str_ireplace("[QUOTE][QUOTE]", "", $message);

			if(preg_match_all("/\[smile:([^\]]+)\]/im", $message, $out)) {
				foreach($out[1] as $smile_path) {
					$s = $smile_path;
					$smile_path = "images/forum/smiles/" . $smile_path . ".gif";
					if(file_exists($smile_path)) {
						$message = str_replace("[smile:" . $s . "]", "<img src='/{$smile_path}' />", $message);
					}
				}
			}

			$message = preg_replace("/<p>(<br \/>)+/", "<p>", $message);
			$message = nl2br($message);
			$message = str_replace("<<br />br /><br />", "", $message);
			$message = str_replace("<p<br />>", "<p>", $message);

			$message = str_replace("&amp;quot;", "\"", $message);
			$message = str_replace("&amp;quote;", "\"", $message);
			$message = html_entity_decode($message);
			$message = str_replace("%", "&#37;", $message);

			$message = $this->parseTPLMacroses($message);

			$this->isXSLTResultMode($oldResultTMode);
			return $message;
		}

		/**
		 * Устанавливает заголовок и хедер в соответствии с параметрами страницы
		 * @return bool
		 */
		public function autoDetectAttributes() {
			$cmsController = cmsController::getInstance();
			if($element_id = $cmsController->getCurrentElementId()) {
				$element = umiHierarchy::getInstance()->getElement($element_id);

				if(!$element) return false;

				if($h1 = $element->getValue("h1")) {
					$this->setHeader($h1);
				} else {
					$this->setHeader($element->getName());
				}

				if($title = $element->getValue("title")) {
					$this->setTitle($title);
				}

			}
		}

		/**
		 * Производит определение параметров сортировки и применяет их к переданной выборке
		 * @param umiSelection $sel выборка, к которой будет применена сортировка
		 * @param int $object_type_id ID типа данных,
		 * в котором находится поле по которому будет произведена сортировка
		 * @return bool
		 */
		public function autoDetectOrders(umiSelection $sel, $object_type_id) {
			if(array_key_exists("order_filter", $_REQUEST)) {
				$sel->setOrderFilter();

				$type = umiObjectTypesCollection::getInstance()->getType($object_type_id);

				$order_filter = getRequest('order_filter');
				foreach($order_filter as $field_name => $direction) {
					if($direction === "asc") $direction = true;
					if($direction === "desc") $direction = false;

					if($field_name == "name") {
						$sel->setOrderByName((bool) $direction);
						continue;
					}

					if($field_name == "ord") {
						$sel->setOrderByOrd((bool) $direction);
						continue;
					}

					if($type) {
						if($field_id = $type->getFieldId($field_name)) {
							$sel->setOrderByProperty($field_id, (bool) $direction);
						} else {
							continue;
						}
					}
				}
			} else {
				return false;
			}
		}

		/**
		 * Производит определение параметров фильтрации
		 * @param umiSelection $sel выборка, к которой будет применена фильтрация
		 * @param int $object_type_id ID типа данных, в котором находятся поля, по которым будет
		 * произведена фильтрация
		 * @return bool
		 * @throws coreException
		 * @throws publicException
		 */
		public function autoDetectFilters(umiSelection $sel, $object_type_id) {
			if(getRequest('search-all-text') !== null) {
				$searchStrings = getRequest('search-all-text');
				if(is_array($searchStrings)) {
					foreach($searchStrings as $searchString) {
						if($searchString) {
							$sel->searchText($searchString);
						}
					}
				}
			}

			if(array_key_exists("fields_filter", $_REQUEST)) {
				$cmsController = cmsController::getInstance();

				/**
				 * @var data|DataFilters $data_module
				 */
				$data_module = $cmsController->getModule("data");

				if (!$data_module) {
					throw new publicException("Need data module installed to use dynamic filters");
				}

				$type = umiObjectTypesCollection::getInstance()->getType($object_type_id);

				$order_filter = getRequest('fields_filter');
				if(!is_array($order_filter)) {
					return false;
				}

				$umiFieldsCollection = umiFieldsCollection::getInstance();
				$umiFieldTypesCollection = umiFieldTypesCollection::getInstance();

				foreach($order_filter as $field_name => $value) {
					if($field_name == "name") {
						$data_module->applyFilterName($sel, $value);
						continue;
					}

					if($field_id = $type->getFieldId($field_name)) {
						$field = $umiFieldsCollection->getField($field_id);

						$field_type_id = $field->getFieldTypeId();
						$field_type = $umiFieldTypesCollection->getFieldType($field_type_id);

						$data_type = $field_type->getDataType();

						switch($data_type) {
							case "text": {
								$data_module->applyFilterText($sel, $field, $value);
								break;
							}

							case "wysiwyg": {
								$data_module->applyFilterText($sel, $field, $value);
								break;
							}

							case "string": {
								$data_module->applyFilterText($sel, $field, $value);
								break;
							}

							case "tags": {
								$tmp = array_extract_values($value);
								if(empty($tmp)) {
									break;
								}
							}
							case "boolean": {
								$data_module->applyFilterBoolean($sel, $field, $value);
								break;
							}

							case "int": {
								$data_module->applyFilterInt($sel, $field, $value);
								break;
							}

							case "symlink":
							case "relation": {
								$data_module->applyFilterRelation($sel, $field, $value);
								break;
							}

							case "float": {
								$data_module->applyFilterFloat($sel, $field, $value);
								break;
							}

							case "price": {
								/**
								 * @var emarket|EmarketCommon $emarket
								 */
								$emarket = $cmsController->getModule('emarket');

								if($emarket instanceof def_module) {
									$defaultCurrency = $emarket->getDefaultCurrency();
									$currentCurrency = $emarket->getCurrentCurrency();
									$prices = $emarket->formatCurrencyPrice($value, $defaultCurrency, $currentCurrency);
									foreach($value as $index => &$void) {
										$void = getArrayKey($prices, $index);
									}
									unset($void);
								}

								$data_module->applyFilterPrice($sel, $field, $value);
								break;
							}

							case "file":
							case "img_file":
							case "swf_file":
							case "video_file": {
								$data_module->applyFilterInt($sel, $field, $value);
								break;
							}

							case "date": {
								$data_module->applyFilterDate($sel, $field, $value);
								break;
							}

							default: {
								break;
							}
						}
					} else {
						continue;
					}
				}
			} else {
				return false;
			}
		}

		/**
		 * Производит анализ переданного пути и возвращает ID соответствующей страницы,
		 * если такая существует
		 * @param int|string $pathOrId ID страницы или путь до нее
		 * @param bool|true $returnCurrentIfVoid, если true и не передан первый параметр,
		 * то будет возвращен ID текущей страницы
		 * @return bool|false|int|string
		 */
		public function analyzeRequiredPath($pathOrId, $returnCurrentIfVoid = true) {

			$umiHierarchy = umiHierarchy::getInstance();

			if (is_numeric($pathOrId)) {
				return ($umiHierarchy->isExists((int) $pathOrId) || $pathOrId == 0) ? (int) $pathOrId : false;
			}

			$pathOrId = trim($pathOrId);

			if ($pathOrId) {
				if (strpos($pathOrId, " ") === false) {
					return $umiHierarchy->getIdByPath($pathOrId);
				}

				$paths_arr = explode(" ", $pathOrId);

				$ids = Array();

				foreach ($paths_arr as $subpath) {
					$id = $this->analyzeRequiredPath($subpath, false);

					if ($id === false) {
						continue;
					}

					$ids[] = $id;
				}

				if (count($ids) > 0) {
					return $ids;
				}

				return false;
			}

			if ($returnCurrentIfVoid) {
				$cmsController = cmsController::getInstance();
				return $cmsController->getCurrentElementId();
			}

			return false;
		}

		/**
		 * Проверяет переданы ли POST-параметры
		 * @param bool|true $bRedirect, еслп true, то в случае успешной проверки
		 * будет произведен редирект в административную панель
		 * @return bool результат проверки
		 */
		public function checkPostIsEmpty($bRedirect = true) {
			$bResult = !is_array($_POST) || (is_array($_POST) && !count($_POST));
			if ($bResult && $bRedirect) {
				$url = preg_replace("/(\r)|(\n)/", "", $_REQUEST['pre_lang'])."/admin/";
				header("Location: ".$url);
				exit();
			} else {
				return $bResult;
			}
		}

		/**
		 * @param umiEventPoint $eventPoint
		 * @throws Exception
		 * @throws baseException
		 */
		public static function setEventPoint(umiEventPoint $eventPoint) {
			umiEventsController::getInstance()->callEvent($eventPoint);
		}

		/**
		 * @deprecated
		 * @return bool
		 */
		public function breakMe() {
			return false;
		}

		/**
		 * Записывает адрес страницы, на которой произошла ошибка
		 * @param string $errorUrl url адрес страницы
		 */
		public function errorRegisterFailPage($errorUrl) {
			cmsController::getInstance()->errorUrl = $errorUrl;
		}

		/**
		 * Записывает сообщение об ошибке
		 * @param string $errorMessage сообщение об ошибке
		 * @param bool|true $causePanic, если true, то будет произведен редирект на текущую страницу,
		 * но с передачей параметра, сигнализируещего об ошибке
		 * @param bool|int $errorCode числовое представление кода ошибки
		 * @param bool|string $errorStrCode строковое представление кода ошибки
		 * @throws coreException
		 * @throws errorPanicException
		 * @throws privateException
		 */
		public function errorNewMessage($errorMessage, $causePanic = true, $errorCode = false, $errorStrCode = false) {
			$controller = cmsController::getInstance();
			$requestId = 'errors_' . $controller->getRequestId();
			if(!isset($_SESSION[$requestId])) {
				$_SESSION[$requestId] = Array();
			}

			$errorMessage = $controller->getCurrentTemplater()->putLangs($errorMessage);

			$_SESSION[$requestId][] = Array("message" => $errorMessage,
				"code" => $errorCode,
				"strcode" => $errorStrCode);

			if($causePanic) {
				$this->errorPanic();
			}
		}

		/**
		 * Выполняет редирект, если ранее было записано хотя бы одно сообщение об ошибке.
		 * Редирект производится на текущую страницу с передачей параметра, сигнализируещего об ошибке.
		 * @return bool
		 * @throws errorPanicException
		 * @throws privateException
		 */
		public function errorPanic() {
			if(getRequest('_err') !== null) {
				return false;
			}

			$cmsController = cmsController::getInstance();

			if(self::$noRedirectOnPanic) {
				$requestId = 'errors_' . $cmsController->getRequestId();
				if(!isset($_SESSION[$requestId])) {
					$_SESSION[$requestId] = Array();
				}
				$errorMessage = "";
				foreach($_SESSION[$requestId] as $i => $errorInfo) {
					unset($_SESSION[$requestId][$i]);
					$errorMessage .= $errorInfo['message'];
				}
				throw new errorPanicException($errorMessage);
			}

			if($errorUrl = $cmsController->errorUrl) {
				// validate url
				$errorUrl = preg_replace("/_err=\d+/is", '', $errorUrl);
				while (strpos($errorUrl, '&&') !== false || strpos($errorUrl, '??') !== false || strpos($errorUrl, '?&') !== false) {
					$errorUrl = str_replace('&&', '&', $errorUrl);
					$errorUrl = str_replace('??', '?', $errorUrl);
					$errorUrl = str_replace('?&', '?', $errorUrl);
				}
				if (strlen($errorUrl) && (substr($errorUrl, -1) === '?' || substr($errorUrl, -1) === '&')) $errorUrl = substr($errorUrl, 0, strlen($errorUrl)-1);
				// detect param concat
				$sUrlConcat = (strpos($errorUrl, '?') === false ? '?' : '&');
				//
				$errorUrl .= $sUrlConcat . "_err=" . $cmsController->getRequestId();
				$this->redirect($errorUrl, false);
			} else {
				throw new privateException("Can't find error redirect string");
			}
		}

		/**
		 * Пытается определить текущий домен
		 * @return bool|string возвращает хост домена в случае успеха или false в случае неудачи
		 */
		public function guessDomain() {
			$res = false;

			for($i = 0; ($param = getRequest("param" . $i)) || $i <= 3; $i++) {
				if(is_numeric($param)) {
					$element = umiHierarchy::getInstance()->getElement($param);
					if($element instanceof umiHierarchyElement) {
						$domain_id = $element->getDomainId();
						if($domain_id) $res = $domain_id;
					} else {
						continue;
					}
				} else {
					continue;
				}
			}

			$domainsCollection = domainsCollection::getInstance();
			$domain = $domainsCollection->getDomain($res);

			if ($domain instanceof domain) {
				return $domain->getHost();
			}

			return false;
		}

		/**
		 * Записывает данные редактируемой (с помощью EiP) страницы
		 * @param string $module имя модуля типа страницы
		 * @param string $method имя метода типа страницы
		 * @param int $id ID редактируемой страницы
		 */
		public static function pushEditable($module, $method, $id) {
			umiTemplater::pushEditable($module, $method, $id);
		}

		/**
		 * Проверяет существует ли метод текущего класса модуля
		 * @param string $method имя метода
		 * @return bool результат проверки
		 */
		public function isMethodExists($method) {
			if (isset($this->methods[$method])) {
				return true;
			}

			$methods = get_class_methods($this);

			if (in_array($method, $methods)) {
				return true;
			}

			return false;
		}

		/**
		 * Выполняет макрос текущего класса модуля, передает результат выполнения на буфер вывода и
		 * прекращает работу скрипта
		 * @param string $methodName имя метода макроса
		 * @throws coreException
		 */
		public function flushAsXML($methodName) {
			static $c = 0;
			if($c++ == 0) {
				$xml = true;
				if (getRequest('jsonMode') == 'force') {
					$xml = false;
				}
				/** @var HTTPOutputBuffer $buffer */
				$buffer = outputBuffer::current();
				$buffer->contentType('text/' . ($xml ? 'xml' : 'javascript'));

				if (!$xml) {
					$buffer->option('generation-time', false);
				}

				$buffer->charset('utf-8');
				$buffer->clear();
				$cmsController = cmsController::getInstance();
				$data = $cmsController->executeStream("udata://" . get_class($this) . "/" . $methodName . ($xml ? '' : '.json'));
				$buffer->push($data);
				$buffer->end();
			}
		}

		/**
		 * Проверяет не является ли текущий режим XML-режимом
		 * @return bool возвращает true, если текущий режим не является XML-режимом
		 * и false в обратном случае
		 */
		public function ifNotXmlMode() {
			return (getRequest('xmlMode') != 'force');
		}

		/**
		 * Проверяет не является ли текущий режим JSON-режимом
		 * @return bool возвращает true, если текущий режим не является JSON-режимом
		 * и false в обратном случае
		 */
		public function ifNotJsonMode() {
			return (getRequest('jsonMode') != 'force');
		}

		/**
		 * Алиас метода removeErrorCodeFromUrl()
		 * @param string $url url адрес
		 * @return mixed
		 */
		public function removeErrorParam($url) {
			return self::removeErrorCodeFromUrl($url);
		}

		/**
		 * Возвращает url адрес без GET-параметра, сигнализируещго о наличии ошибки
		 * @param string $url url адрес
		 * @return mixed
		 */
		public static function removeErrorCodeFromUrl($url) {
			return preg_replace("/_err=\d+/", "", $url);
		}

		/**
		 * Возвращает ссылку на редактирование объекта
		 * @param int $objectId ID объектв
		 * @param bool|false $type
		 * @return bool всегда false
		 */
		public function getObjectEditLink($objectId, $type = false) {
			return false;
		}

		/**
		 * Производит проверку шаблона. В текущей реализации ничего не делает.
		 * @param string $templateName имя шаблона
		 */
		public static function validateTemplate(&$templateName) {
			if(!$templateName && $templateName == 'default' && self::$defaultTemplateName != 'default') {
				$templateName = self::$defaultTemplateName;
			}
		}

		/**
		 * Проверяет текущий режим
		 * @param string $mode проверяемый режим
		 * @throws tplOnlyException, если проверяемый режим tpl, но текущий режим отличен
		 * @throws xsltOnlyException, если проверяемый режим xslt, но текущий режим отличен
		 */
		public function templatesMode($mode) {
			$isXslt = self::isXSLTResultMode();
			if($mode == 'xslt' && !$isXslt) {
				throw new xsltOnlyException;
			}

			if($mode == 'tpl' && $isXslt) {
				throw new tplOnlyException;
			}
		}

		/**
		 * Устанавливает/возвращает режим работы макросов
		 * @param bool|null $newValue - если передан, то переопределяет режим работы
		 * @static
		 * @return bool - возвращает режим работы, если передан новый режим, возвращает прошлый режим работы макросов
		 */
		public static function isXSLTResultMode($newValue = null) {
			if (self::$xsltResultMode === null) {
				self::$xsltResultMode = cmsController::getInstance()->getCurrentTemplater() instanceof IFullResult;
			}

			if ($newValue !== null) {
				$oldValue = self::$xsltResultMode;
				self::$xsltResultMode = (bool) $newValue;
				return $oldValue;
			}

			return self::$xsltResultMode;
		}

		/**
		 * Проверяет соответствие типа сущности переданным типам
		 * @param iUmiHierarchyElement|iUmiObject $entity
		 * @param array $types список типов
		 * в формате ['module' => 'some_module', 'method' => 'some_method']
		 * @param bool|false $checkParentType, если true,
		 * то проверка будет производиться на родительском типе типа сущности
		 * @return bool|void
		 * @throws publicException
		 */
		public function validateEntityByTypes($entity, $types, $checkParentType = false) {
			if($entity instanceof iUmiHierarchyElement) {
				$module = $entity->getModule();
				$method = $entity->getMethod();
			} else if($entity instanceof iUmiObject) {
				/**
				 * @var umiObjectType
				 */
				$objectType = selector::get('object-type')->id($entity->getTypeId());
				if($checkParentType) {
					$objectType = selector::get('object-type')->id($objectType->getParentId());
				}
				if($hierarchyTypeId = $objectType->getHierarchyTypeId()) {
					$hierarchyType = selector::get('hierarchy-type')->id($hierarchyTypeId);
					$module = $hierarchyType->getModule();
					$method = $hierarchyType->getMethod();
				} else {
					$module = null;
					$method = null;
				}
			} else {
				throw new publicException("Page or object must be given");
			}

			if($module === null && $method === null && $types === null) {
				return true;
			}

			if($module == 'content' && $method == '') {
				$method = 'page';
			}

			if(getArrayKey($types, 'module')) {
				$types = array($types);
			}

			foreach($types as $type) {
				$typeModule = getArrayKey($type, 'module');
				$typeMethod = getArrayKey($type, 'method');

				if($typeModule == 'content' && $typeMethod == '') {
					$typeMethod = 'page';
				}

				if($typeModule === $module && ($typeMethod === null || $typeMethod === $method)) {
					return;
				}
			}
			throw new publicException(getLabel('error-common-type-mismatch'));
		}

		/**
		 * Проверяет является ли текущий режим демонстрационным режимом
		 * @return bool результат проверки
		 */
		public function is_demo() {
			return defined('CURRENT_VERSION_LINE') && CURRENT_VERSION_LINE == 'demo';
		}


		/**
		 * Добавляет данные об возникших ошибках
		 * @param string|array|Exception $errors данные ошибок
		 * @return array|bool|int
		 */
		public function errorAddErrors($errors) {
			$result = array();
			if ($errors instanceof Exception) {
				$error = array(
					'message' => $errors->getMessage(),
					'code' => $errors->getCode(),
				);
				return array_push($this->errors, $error);
			} elseif (is_array($errors)) {
				if (array_key_exists('message', $errors)) {
					$error = array_intersect_key($errors, array('message'=>'', 'code'=>''));
					return array_push($this->errors, $error);
				} else {
					foreach ($errors as $error) {
						$result[] = $this->errorAddErrors($error);
					}
					return $result;
				}
			} elseif (is_string($errors)) {
				return array_push($this->errors, array('message' => $errors));
			}
			return false;
		}

		/** Устанавливет ошибки. В текущей реализации ничего не делает. */
		protected function errorSetErrors () {

		}

		/**
		 * Возвращает данные об ошибках
		 * @return array
		 */
		public function errorGetErrors() {
			return $this->errors;
		}

		/**
		 * Проверяет присутствуют ли ошибки
		 * @return bool результат проверки
		 */
		public function errorHasErrors() {
			return (!empty($this->errors));
		}

		/**
		 * Устанавливает адрес страницы, на которой произошла ошибка
		 * @param string $errorPage url адрес страницы
		 * @return bool
		 */
		public function errorSetErrorPage($errorPage) {
			$errorPage = preg_replace('#http://[^/]+#', '', trim($errorPage));
			// validate url
			$errorPage = preg_replace("/_err=\d+/is", '', $errorPage);
			while (strpos($errorPage, '&&') !== false || strpos($errorPage, '??') !== false || strpos($errorPage, '?&') !== false) {
				$errorPage = str_replace('&&', '&', $errorPage);
				$errorPage = str_replace('??', '?', $errorPage);
				$errorPage = str_replace('?&', '?', $errorPage);
			}
			if (strlen($errorPage) && (substr($errorPage, -1) === '?' || substr($errorPage, -1) === '&')) {
				$errorPage = substr($errorPage, 0, strlen($errorPage)-1);
			}
			$this->errorPage = $errorPage;
			return true;
		}

		/**
		 * Возвращает адрес страницы, на которой произошла ошибка
		 * @return string
		 */
		public function errorGetErrorPage() {
			return $this->errorPage;
		}

		/**
		 * Выбрасывает исключение в соответствии с записанными ошибками
		 * @param bool|string $mode тип выбрасываемого исключения
		 * @return bool
		 * @throws errorPanicException
		 * @throws wrongValueException
		 */
		public function errorThrow($mode = false) {
			if (!$this->errorHasErrors()) {
				return false;
			}

			if(self::$noRedirectOnPanic) {
				$errorMessage = '';
				foreach ($this->errors as $error) {
					$errorMessage .= getLabel($error['message']) . ' ';
				}
				$this->errors = array();
				throw new errorPanicException($errorMessage);
			}

			switch ($mode) {
				case 'public' : {
					$this->errorThrowPublic();
					break;
				}

				case 'admin' : {
					$this->errorThrowAdmin();
					break;
				}

				case 'xml' : {
					$errors = array();
					foreach ($this->errors as $error) {
						$errors[] = getLabel($error['message']);
					}
					$this->errors = array();
					throw new wrongValueException('<br/>' . implode("<br/><br/>", $errors));
				}
			}
		}

		/**
		 * Сортирует массив с объектам, по порядку идентификаторов
		 * в массиве с идентификаторами объектов.
		 * Возращает результат сортировки.
		 * @param array $sortedIds массив с идентификаторами объектов
		 * @param array $objects массив с объектами
		 * @return array
		 */
		public static function sortObjects($sortedIds, $objects) {
			if (count($sortedIds) == 0 || count($objects) == 0) {
				$objects;
			}

			$flippedSortedIds = array_flip($sortedIds);
			$sortedObjects = array();

			foreach ($objects as $object) {
				$objectId = null;

				switch ($object) {
					case ($object instanceof umiEntinty): {
						$objectId = $object->getId();
						break;
					}
					case (is_array($object) && isset($object['id'])): {
						$objectId = $object['id'];
						break;
					}
				}
				switch ($objectId) {
					case is_null($objectId): {
						$sortedObjects[] = $object;
						break;
					}
					case isset($flippedSortedIds[$objectId]): {
						$sortedObjects[$flippedSortedIds[$objectId]] = $object;
						break;
					}
					default: {
						$sortedObjects[] = $object;
					}
				}
			}
			ksort($sortedObjects);
			return $sortedObjects;
		}

		/**
		 * Геттер необъявленных свойств
		 * @param string $name имя свойства
		 * @return mixed
		 * @throws coreException
		 */
		public function __get($name) {
			$adminClassName = $this->getAdminClassName();

			if (in_array($name, $this->getAdminProperties()) && $this->isClassImplemented($adminClassName)) {
				return $this->getImplementedInstance($adminClassName)->$name;
			}
		}

		/**
		 * Проверяет существование необяъвленных свойств
		 * @param string $name свойства
		 * @return bool
		 */
		public function __isset($name) {
			return (in_array($name, $this->getAdminProperties()) && $this->isClassImplemented($this->getAdminClassName()));
		}

		/**
		 * Возвращает список атрибутов, присущих методам административной панели
		 * @return array
		 */
		protected function getAdminProperties() {
			return array(
				'limit',
				'offset',
				'dataType',
				'actionType',
				'total',
				'data'
			);
		}

		/**
		 * Возвращает имя класса, содержащего административный функционал
		 * @return string
		 */
		protected function getAdminClassName() {
			return ucfirst(get_class($this)) . 'Admin';
		}

		/**
		 * Формирует объекты страниц (umiHierarchyElement) по идентификаторам
		 * @param array $elementsIds массив с идентификаторами страниц
		 * @param bool $needProps нужно ли дополнительно загрузить свойства страниц
		 * @param bool|int $hierarchyTypeId ид иерархического типа данных, к которому принадлежат страницы
		 * @return bool
		 */
		protected function loadElements($elementsIds, $needProps = false, $hierarchyTypeId = false) {
			if (!is_array($elementsIds)) {
				$elementsIds = array($elementsIds);
			}

			$umiLinksHelper = umiLinksHelper::getInstance();
			$umiLinksHelper->loadLinkPartForPages($elementsIds);
			$hierarchy = umiHierarchy::getInstance();
			$elements = $hierarchy->loadElements($elementsIds);
			$objectsIds = array();

			/** @var iUmiHierarchyElement $element */
			foreach ($elements as $element) {
				if ($element instanceof umiHierarchyElement) {
					$objectsIds[] = $element->getObjectId();
				}
			}

			if ($needProps && $hierarchyTypeId && count($objectsIds) > 0) {
				umiObjectProperty::loadPropsData($objectsIds, $hierarchyTypeId);
			}

			return true;
		}

		/**
		 * Выбрасывает публичное исключение с сообщением записанных ошибок
		 * @throws privateException
		 */
		private function errorThrowPublic() {
			foreach ($this->errors as &$error) {
				$error['message'] = '%' . $error['message'] . '%';
			}
			$this->errorRedirect();
		}

		/**
		 * Выбрасывает исключение уровня административной панели с сообщением записанных ошибок
		 * @throws privateException
		 */
		private function errorThrowAdmin () {
			foreach ($this->errors as &$error) {
				$error['message'] = getLabel($error['message']);
			}
			$this->errorRedirect();
		}

		/**
		 * Производит редирект на страницу с ошибкой
		 * @throws privateException
		 */
		private function errorRedirect () {
			$cmsController = cmsController::getInstance();
			$requestId = 'errors_' . $cmsController->getRequestId();
			$_SESSION[$requestId] = $this->errors;
			if($errorUrl = $this->errorPage) {
				// detect param concat
				$sUrlConcat = (strpos($errorUrl, '?') === false ? '?' : '&');
				//
				$errorUrl .= $sUrlConcat . "_err=" . $cmsController->getRequestId();
				$this->errors = array();
				$this->redirect($errorUrl, false);
			} else {
				$this->errors = array();
				throw new privateException("Can't find error redirect string");
			}
		}

	};
?>
