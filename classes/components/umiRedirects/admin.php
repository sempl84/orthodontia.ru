<?php
	/**
	 * Класс функционала административной панели
	 */
	class UmiRedirectsAdmin {

		use baseModuleAdmin;
		use tUmiMapWorker;

		/**
		 * @var umiRedirects $module
		 */
		public $module;

		/**
		 * Конструктор
		 */
		public function __construct() {
			$this->setMap(new tableControlMap());
		}

		/**
		 * Возвращает список редиректов
		 * @return bool
		 * @throws Exception
		 */
		public function lists() {
			$this->setDataType("list");
			$this->setActionType("view");

			if ($this->module->ifNotJsonMode()) {
				$this->setDirectCallError();
				$this->doData();
				return true;
			}

			$map = $this->getMap();
			$limit = (int) getRequest($map->get('LIMIT_REQUEST_KEY'));
			$limit = ($limit === 0) ? 25 : $limit;
			$currentPage = (int) getRequest($map->get('PAGE_NUM_REQUEST_KEY'));
			$offset = $currentPage * $limit;

			/**
			 * @var umiRedirectsCollection $umiRedirectsCollection
			 */
			$umiRedirectsCollection = umiRedirectsCollection::getInstance();
			$redirectsMap = $umiRedirectsCollection->getMap();
			$dataResultKey = $map->get('DATA_RESULT_KEY');
			$result = [];
			$total = 0;

			try {
				$queryParams = [
					$redirectsMap->get('LIKE_MODE_KEY') => true,
					$redirectsMap->get('OFFSET_KEY') => $offset,
					$redirectsMap->get('LIMIT_KEY') => $limit,
					$redirectsMap->get('COUNT_KEY') => true
				];

				$filtersKey = $map->get('FILTERS_REQUEST_KEY');
				$filtersKeys = (isset($_REQUEST[$filtersKey]) && is_array($_REQUEST[$filtersKey])) ? array_keys($_REQUEST[$filtersKey]) : [];

				$fieldsKeys = [
					$redirectsMap->get('TARGET_FIELD_NAME'),
					$redirectsMap->get('SOURCE_FIELD_NAME'),
					$redirectsMap->get('STATUS_FIELD_NAME'),
					$redirectsMap->get('MADE_BY_USER_FIELD_NAME')
				];

				foreach ($filtersKeys as $fieldKey) {
					if (!in_array($fieldKey, $fieldsKeys)) {
						continue;
					}

					$fieldValue = $_REQUEST[$filtersKey][$fieldKey];
					$fieldValue = (is_array($fieldValue)) ? array_shift($fieldValue) : null;

					if ($fieldValue === null || $fieldValue === '') {
						continue;
					}

					$queryParams[$fieldKey] = $fieldValue;
				}

				$ordersKey = $map->get('ORDERS_REQUEST_KEY');
				$orders = (isset($_REQUEST[$ordersKey]) && is_array($_REQUEST[$ordersKey])) ? $_REQUEST[$ordersKey] : [];

				if (count($orders) > 0) {
					$queryParams[$redirectsMap->get('ORDER_KEY')] = $orders;
				}

				$redirects = $umiRedirectsCollection->export($queryParams);
				$result[$dataResultKey] = $redirects;
				$total = $umiRedirectsCollection->count([]);
			} catch (Exception $e) {
				$result[$dataResultKey][$map->get('ERROR_RESULT_KEY')] = $e->getMessage();
			}

			$result[$dataResultKey][$map->get('OFFSET_RESULT_KEY')] = $offset;
			$result[$dataResultKey][$map->get('LIMIT_REQUEST_KEY')] = $limit;
			$result[$dataResultKey][$map->get('TOTAL_RESULT_KEY')] = $total;
			/**
			 * @var HTTPOutputBuffer $buffer
			 */
			$buffer = $umiRedirectsCollection->getBuffer();
			$buffer->calltime();
			$buffer->contentType('text/javascript');
			$buffer->charset('utf-8');
			$buffer->option('generation-time', false);
			$buffer->push(json_encode($result));
			$buffer->end();
		}

		/**
		 * Возвращает данные для создания формы добавления редиректа.
		 * Если передан $_REQUEST['param0'] = do,
		 * то добавляет редирект и перенаправляет на страницу, где можно отредактировать
		 * редирект
		 * @throws Exception
		 */
		public function add() {
			$mode = (string) getRequest("param0");
			$this->setHeaderLabel("header-umiRedirects-add-redirect");
			$requestData = isset($_REQUEST['data']['new']) ? $_REQUEST['data']['new'] : [];
			/**
			 * @var umiRedirectsCollection $umiRedirectsCollection
			 */
			$umiRedirectsCollection = umiRedirectsCollection::getInstance();
			$redirectsMap = $umiRedirectsCollection->getMap();
			$source = $redirectsMap->get('SOURCE_FIELD_NAME');
			$target = $redirectsMap->get('TARGET_FIELD_NAME');
			$status = $redirectsMap->get('STATUS_FIELD_NAME');
			$madeByUser = $redirectsMap->get('MADE_BY_USER_FIELD_NAME');

			$formData = [
				$source => (isset($requestData[$source])) ? $requestData[$source] : null,
				$target => (isset($requestData[$target])) ? $requestData[$target] : null,
				$status => (isset($requestData[$status])) ? (int) $requestData[$status] : null,
				$madeByUser => (isset($requestData[$madeByUser])) ? (bool) $requestData[$status] : null,
				$this->getMap()->get('FORM_FIELD_NAME_PREFIX') => 'data[new]'
			];

			if ($mode == "do") {
				$madeByUserValue = (isset($requestData[$madeByUser])) ? (bool) $requestData[$madeByUser] : false;
				$formData[$madeByUser] = $madeByUserValue;

				$this->module->validateRedirectParams($formData);
				/**
				 * @var umiRedirect $redirect
				 */
				$redirect = $umiRedirectsCollection->create($formData);

				switch (getRequest('save-mode')) {
					case getLabel('label-save-add-exit'): {
						$this->chooseRedirect();
						break;
					}
					case getLabel('label-save-add'): {
						$this->module->redirect($this->module->pre_lang . '/admin/umiRedirects/edit/' . $redirect->getId());
						break;
					}
				}
			}

			$this->setDataType("form");
			$this->setActionType("create");
			$this->setData($formData);
			$this->doData();
		}

		/**
		 * Возвращает данные для создания формы редактирования редиректа.
		 * Если передан $_REQUEST['param1'] = do,
		 * то сохраняет изменения редиректа и производит перенаправление.
		 * Адрес перенаправление зависит от режима кнопки "Сохранить".
		 * @throws publicAdminException
		 */
		public function edit() {
			$redirectId = (string) getRequest("param0");
			$mode = (string) getRequest("param1");
			$this->setHeaderLabel("header-umiRedirects-edit-redirect");
			/**
			 * @var umiRedirectsCollection $umiRedirectsCollection
			 */
			$umiRedirectsCollection = umiRedirectsCollection::getInstance();
			$redirectsMap = $umiRedirectsCollection->getMap();
			$id = $redirectsMap->get('ID_FIELD_NAME');
			$source = $redirectsMap->get('SOURCE_FIELD_NAME');
			$target = $redirectsMap->get('TARGET_FIELD_NAME');
			$status = $redirectsMap->get('STATUS_FIELD_NAME');
			$madeByUser = $redirectsMap->get('MADE_BY_USER_FIELD_NAME');

			$redirects = $umiRedirectsCollection->get(
				[
					$id => $redirectId
				]
			);

			if (count($redirects) == 0) {
				throw new publicAdminException(getLabel('error-redirect-not-found'));
			}

			/**
			 * @var umiRedirect $redirect
			 */
			$redirect = array_shift($redirects);
			$requestData = isset($_REQUEST['data'][$redirectId]) ? $_REQUEST['data'][$redirectId] : [];

			$formData = [
				$source => (isset($requestData[$source])) ? $requestData[$source] : $redirect->getSource(),
				$target => (isset($requestData[$target])) ? $requestData[$target] : $redirect->getTarget(),
				$status => (isset($requestData[$status])) ? (int) $requestData[$status] : $redirect->getStatus(),
				$madeByUser => (isset($requestData[$madeByUser])) ? (bool) $requestData[$madeByUser] : $redirect->isMadeByUser()
			];

			if ($mode == "do") {
				$madeByUserValue = (isset($requestData[$madeByUser])) ? (bool) $requestData[$madeByUser] : false;
				$formData[$madeByUser] = $madeByUserValue;

				if ($requestData[$source] != $redirect->getSource()) {
					$this->module->validateRedirectParams($formData);
				} else {
					$this->module->checkCircles($formData);
				}

				$redirect->import($formData);
				$redirect->commit();
				$this->chooseRedirect();
			}

			$formData[$this->getMap()->get('FORM_FIELD_NAME_PREFIX')] = 'data[' . $redirectId . ']';
			$formData[$id] = $redirectId;

			$this->setDataType("form");
			$this->setActionType("modify");
			$this->setData($formData);
			$this->doData();
		}

		/**
		 * Удаляет редиректы
		 */
		public function del() {
			$redirectsIds = getRequest('element');

			if (!is_array($redirectsIds)) {
				$redirectsIds = [$redirectsIds];
			}

			/**
			 * @var umiRedirectsCollection $umiRedirectsCollection
			 */
			$umiRedirectsCollection = umiRedirectsCollection::getInstance();
			$map = $this->getMap();
			$dataResultKey = $map->get('DATA_RESULT_KEY');
			$result = [];

			try {
				$umiRedirectsCollection->delete(
					[
						$umiRedirectsCollection->getMap()->get('ID_FIELD_NAME') => $redirectsIds
					]
				);
				$result[$dataResultKey][$map->get('SUCCESS_RESULT_KEY')] = true;
			} catch (Exception $e) {
				$result[$dataResultKey][$map->get('ERROR_RESULT_KEY')] = $e->getMessage();
			}

			$this->setDataType("list");
			$this->setActionType("view");
			$this->setData($result);
			$this->doData();
		}

		/**
		 * Сохраняет изменения поля редиректа
		 * @throws Exception
		 */
		public function saveValue() {
			$redirectId = (string) getRequest("param0");
			$fieldKey = getRequest('field');
			$fieldValue = getRequest('value');

			/**
			 * @var umiRedirectsCollection $umiRedirectsCollection
			 */
			$umiRedirectsCollection = umiRedirectsCollection::getInstance();
			$redirectsMap = $umiRedirectsCollection->getMap();
			$map = $this->getMap();
			$dataResultKey = $map->get('DATA_RESULT_KEY');
			$result = [];

			try {
				$redirects = $umiRedirectsCollection->get(
					[
						$redirectsMap->get('ID_FIELD_NAME') => $redirectId
					]
				);

				if (count($redirects) == 0) {
					throw new Exception(getLabel('error-redirect-not-found'));
				}

				/**
				 * @var umiRedirect $redirect
				 */
				$redirect = array_shift($redirects);
				$redirect->setValue($fieldKey, $fieldValue);

				if ($fieldKey == $map->get('SOURCE_FIELD_NAME')) {
					$this->module->validateRedirectParams($redirect->export());
				} else {
					$this->module->checkCircles($redirect->export());
				}

				$redirect->commit();

				$result[$dataResultKey][$map->get('SUCCESS_RESULT_KEY')] = true;
			} catch (Exception $e) {
				$result[$dataResultKey][$map->get('ERROR_RESULT_KEY')] = $e->getMessage();
			}

			/**
			 * @var HTTPOutputBuffer $buffer
			 */
			$buffer = $umiRedirectsCollection->getBuffer();
			$buffer->calltime();
			$buffer->contentType('text/javascript');
			$buffer->charset('utf-8');
			$buffer->option('generation-time', false);
			$buffer->push(json_encode($result));
			$buffer->end();
		}

		/**
		 * Возвращает настройки для формирования табличного контрола
		 * @param string $param контрольный параметр
		 * @return array
		 */
		public function getDatasetConfiguration($param = '') {
			return [
				'methods' => [
					[
						'title'   => getLabel('smc-load'),
						'forload' => true,
						'module'  =>'umiRedirects',
						'#__name' =>'lists'
					],
					[
						'title'   => getLabel('js-permissions-edit'),
						'module'  => 'umiRedirects',
						'#__name' => 'edit'
					],
					[
						'title'   => getLabel('js-confirm-unrecoverable-yes'),
						'module'  => 'umiRedirects',
						'#__name' => 'del'
					],
				],
				'default' => 'source[400px]|target[400px]|status[250px]|made_by_user[250px]',
				'save_field_method' => 'saveValue',
				'fields' => [
					[
						'name' => 'source',
						'title' => getLabel('label-source-field'),
						'type' => 'string',
					],
					[
						'name' => 'target',
						'title' => getLabel('label-target-field'),
						'type' => 'string',
					],
					[
						'name' => 'status',
						'title' => getLabel('label-status-field'),
						'type' => 'relation',
						'multiple' => 'false',
						'options' => '301,302,303,307'
					],
					[
						'name' => 'made_by_user',
						'title' => getLabel('label-made-by-user-field'),
						'type' => 'bool',
					]
				]
			];
		}
	}
?>