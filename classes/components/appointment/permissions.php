<?php
	/**
	 * Группы прав на функционал модуля
	 */
	$permissions = [
		/**
		 * Права на запись
		 */
		'enroll' => [
			'getAppointmentsData',
			'postAppointment',
			'employees',
			'services',
			'statuses',
			'serviceGroups',
			'employeeSchedules',
			'employeeServicesIds',
			'employeesByServiceId',
			'getDefaultSchedule',
			'page'
		],
		/**
		 * Права на администрирование модуля
		 */
		'manage' => [
			'pages',
			'addPage',
			'editPage',
			'del',
			'activity',
			'config',
			'getDatasetConfiguration',
			'page.edit',
			'serviceGroups',
			'services',
			'employees',
			'orders',
			'employeesList',
			'servicesList',
			'statusesList',
			'employeesListByServiceId',
			'saveOrderField',
			'editService',
			'addService',
			'deleteServices',
			'serviceGroups',
			'saveServiceField',
			'deleteServiceGroups',
			'editServiceGroup',
			'addServiceGroup',
			'saveGroupField',
			'addEmployee',
			'editEmployee',
			'deleteEmployees',
			'employeeSchedulesList',
			'getScheduleWorkTimes',
			'employeeServicesIdsList',
			'serviceWorkingTime',
			'config'
		]
	];
?>
