<?php
	/**
	 * Класс методов для клиентского и административного режимов
	 */
	class AppointmentCommon {
		/**
		 * @var appointment $module
		 */
		public $module;

		/**
		 * Возвращает список сотрудник
		 * @param string $template имя шаблона (для tpl)
		 * @param int $limit ограничение на количество выводимых сотрудников
		 * @param null|int $selectedId идентификатор выбранного сотрудника (он будет помечен в списке)
		 * @return mixed
		 * @throws Exception
		 */
		public function employeesList($template = 'default', $limit = 25, $selectedId = null) {
			list($employeesTemplate, $employeeTemplate, $emptyTemplate) = appointment::loadTemplates(
				'appointment/' . $template,
				'employees_block',
				'employee_line',
				'employees_empty'
			);

			$serviceContainer = umiServiceContainers::getContainer();
			/**
			 * @var AppointmentEmployeesCollection $employeesCollection
			 */
			$employeesCollection = $serviceContainer->get('AppointmentEmployees');
			$collectionMap = $employeesCollection->getMap();

			$params = [];
			$params[$collectionMap->get('LIMIT_KEY')] = (is_numeric($limit)) ? $limit : 25;
			$params[$collectionMap->get('OFFSET_KEY')] = getRequest('p') * $limit;

			$employees = $this->module->getEmployees($params);

			if (count($employees) == 0) {
				return appointment::parseTemplate($emptyTemplate, $employees);
			}

			$total  = $employeesCollection->count([]);
			$items = [];
			$result = [];

			/**
			 * @var AppointmentEmployee $employee
			 */
			foreach ($employees as $employee) {
				$item = [];
				$item['attribute:id'] = $employee->getId();
				$item['attribute:name'] = $employee->getName();
				$item['attribute:photo'] = $employee->getPhoto();
				$item['attribute:description'] = $employee->getDescription();

				if ($employee->getId() == $selectedId) {
					$item['attribute:selected'] = 'selected';
				}

				$items[] = appointment::parseTemplate($employeeTemplate, $item);
			}

			$result['subnodes:items'] = $items;
			$result['total'] = $total;
			$result['selected'] = $selectedId;

			return appointment::parseTemplate($employeesTemplate, $result);
		}

		/**
		 * Возвращает список сотрудников, оказывающих заданную услугу
		 * @param string $template имя шаблона (для tpl)
		 * @param int $serviceId идентификатор услуги
		 * @param int $limit ограничение на количество выводимых сотрудников
		 * @param null $selectedId идентификатор выбранного сотрудника (он будет помечен в списке)
		 * @return mixed
		 * @throws Exception
		 */
		public function employeesListByServiceId($template = 'default', $serviceId, $limit = 25, $selectedId = null) {
			list($employeesTemplate, $employeeTemplate, $emptyTemplate) = appointment::loadTemplates(
				'appointment/' . $template,
				'employees_block',
				'employee_line',
				'employees_empty'
			);

			if (!is_numeric($serviceId)) {
				throw new publicAdminException(getLabel('error-service-not-found', 'appointment'));
			}

			$serviceContainer = umiServiceContainers::getContainer();
			/**
			 * @var AppointmentEmployeesServicesCollection $employeesServicesCollection
			 */
			$employeesServicesCollection = $serviceContainer->get('AppointmentEmployeesServices');
			$collectionMap = $employeesServicesCollection->getMap();

			$params = [];
			$params[$collectionMap->get('SERVICE_ID_FIELD_NAME')] = $serviceId;
			$params[$collectionMap->get('LIMIT_KEY')] = (is_numeric($limit)) ? $limit : 25;
			$params[$collectionMap->get('OFFSET_KEY')] = getRequest('p') * $limit;

			$employeesWithService = $this->module->getEmployeesServices($params);
			$employeesIds = [];

			/**
			 * @var AppointmentEmployeeService $employeeWithService
			 */
			foreach ($employeesWithService as $employeeWithService) {
				$employeesIds[] = $employeeWithService->getEmployeeId();
			}

			if (count($employeesIds) == 0) {
				return appointment::parseTemplate($emptyTemplate, $employeesIds);
			}

			$total  = $employeesServicesCollection->count([]);

			$params = [];
			$params[$collectionMap->get('ID_FIELD_NAME')] = $employeesIds;

			$employees = $this->module->getEmployees($params);

			$items = [];
			$result = [];

			/**
			 * @var AppointmentEmployee $employee
			 */
			foreach ($employees as $employee) {
				$item = [];
				$item['attribute:id'] = $employee->getId();
				$item['attribute:name'] = $employee->getName();
				$item['attribute:photo'] = $employee->getPhoto();
				$item['attribute:description'] = $employee->getDescription();

				if ($employee->getId() == $selectedId) {
					$item['attribute:selected'] = 'selected';
				}

				$items[] = appointment::parseTemplate($employeeTemplate, $item);
			}

			$result['subnodes:items'] = $items;
			$result['total'] = $total;
			$result['selected'] = $selectedId;

			return appointment::parseTemplate($employeesTemplate, $result);
		}

		/**
		 * Возвращает список услуг
		 * @param string $template имя шаблона (для tpl)
		 * @param int $limit ограничение на количество выводимых услуг
		 * @param null|int $selectedId идентификатор выбранной услуги (она будет помечен в списке)
		 * @return mixed
		 * @throws Exception
		 */
		public function servicesList($template = 'default', $limit = 25, $selectedId = null) {
			list($servicesTemplate, $serviceTemplate, $emptyTemplate) = appointment::loadTemplates(
				'appointment/' . $template,
				'services_block',
				'service_line',
				'service_empty'
			);

			$serviceContainer = umiServiceContainers::getContainer();
			/**
			 * @var AppointmentServicesCollection $servicesCollection
			 */
			$servicesCollection = $serviceContainer->get('AppointmentServices');
			$collectionMap = $servicesCollection->getMap();

			$params = [];
			$params[$collectionMap->get('LIMIT_KEY')] = (is_numeric($limit)) ? $limit : 25;
			$params[$collectionMap->get('OFFSET_KEY')] = getRequest('p') * $limit;

			$services = $this->module->getServices($params);

			if (count($services) == 0) {
				return appointment::parseTemplate($emptyTemplate, $services);
			}

			$total  = $servicesCollection->count([]);
			$items = [];
			$result = [];
			$timePregReplacePattern = $this->module->timePregReplacePattern;
			$groupsNames = $this->module->getServicesGroupsNames();

			/**
			 * @var AppointmentService $service
			 */
			foreach ($services as $service) {
				$item = [];
				$item['attribute:id'] = $service->getId();
				$item['attribute:name'] = $service->getName();
				$item['attribute:price'] = $service->getPrice();
				$item['attribute:time'] = preg_replace($timePregReplacePattern, '', $service->getTime());

				if (isset($groupsNames[$service->getGroupId()])) {
					$item['attribute:group'] = $groupsNames[$service->getGroupId()];
				}

				if ($service->getId() == $selectedId) {
					$item['attribute:selected'] = 'selected';
				}

				$items[] = appointment::parseTemplate($serviceTemplate, $item);
			}

			$result['subnodes:items'] = $items;
			$result['total'] = $total;
			$result['selected'] = $selectedId;

			return appointment::parseTemplate($servicesTemplate, $result);
		}

		/**
		 * Возвращает список статусов заявок на запись
		 * @param string $template имя шаблона (для tpl)
		 * @param null|int $selectedCode код выбранного статуса (он будет помечен в списке)
		 * @return mixed
		 */
		public function statusesList($template = 'default', $selectedCode = null) {
			list($block, $status) = appointment::loadTemplates(
				'appointment/' . $template,
				'statuses_block',
				'status_line'
			);

			$statuses = $this->module->getStatuses();
			$total = count($statuses);
			$items = [];
			$result = [];

			foreach ($statuses as $statusCode => $statusName) {
				$item = [];
				$item['attribute:code'] = $statusCode;
				$item['attribute:name'] = getLabel($statusName);

				if ($statusCode == $selectedCode) {
					$item['attribute:selected'] = 'selected';
				}

				$items[] = appointment::parseTemplate($status, $item);
			}

			$result['subnodes:items'] = $items;
			$result['total'] = $total;
			$result['selected'] = $selectedCode;

			return appointment::parseTemplate($block, $result);
		}

		/**
		 * Возвращает список групп услуг
		 * @param string $template имя шаблона (для tpl)
		 * @param int $limit ограничение на количество выводимых групп
		 * @param null $selectedId идентификатор выбранной группы услуг (она будет помечена в списке)
		 * @return mixed
		 * @throws Exception
		 */
		public function serviceGroupsList($template = 'default', $limit = 25, $selectedId = null) {
			list($serviceGroupsTemplate, $serviceGroupTemplate, $emptyTemplate) = appointment::loadTemplates(
				'appointment/' . $template,
				'services_groups_block',
				'services_group_line',
				'services_groups_empty'
			);

			$serviceContainer = umiServiceContainers::getContainer();
			/**
			 * @var AppointmentServiceGroupsCollection $serviceGroupsCollection
			 */
			$serviceGroupsCollection = $serviceContainer->get('AppointmentServiceGroups');
			$collectionMap = $serviceGroupsCollection->getMap();

			$params = [];
			$params[$collectionMap->get('LIMIT_KEY')] = (is_numeric($limit)) ? $limit : 25;
			$params[$collectionMap->get('OFFSET_KEY')] = getRequest('p') * $limit;

			$servicesGroups = $this->module->getServiceGroups($params);

			if (count($servicesGroups) == 0) {
				return appointment::parseTemplate($emptyTemplate, $servicesGroups);
			}

			$total  = $serviceGroupsCollection->count([]);
			$items = [];
			$result = [];

			/**
			 * @var AppointmentServiceGroup $serviceGroup
			 */
			foreach ($servicesGroups as $serviceGroup) {
				$item = [];
				$item['attribute:id'] = $serviceGroup->getId();
				$item['attribute:name'] = $serviceGroup->getName();

				if ($serviceGroup->getId() == $selectedId) {
					$item['attribute:selected'] = 'selected';
				}

				$items[] = appointment::parseTemplate($serviceGroupTemplate, $item);
			}

			$result['subnodes:items'] = $items;
			$result['total'] = $total;
			$result['selected'] = $selectedId;

			return appointment::parseTemplate($serviceGroupsTemplate, $result);
		}

		/**
		 * Возвращает список рабочих дней с часами работы сотрудника
		 * @param string $template имя шаблона (для tpl)
		 * @param int $employeeId идентификатор сотрудника
		 * @return mixed
		 * @throws publicAdminException
		 */
		public function employeeSchedulesList($template = 'default', $employeeId) {
			list($schedulesTemplate, $scheduleTemplate, $emptyTemplate) = appointment::loadTemplates(
				'appointment/' . $template,
				'employee_schedules_block',
				'employee_schedule_line',
				'employee_schedules_empty'
			);

			if (!is_numeric($employeeId)) {
				throw new publicAdminException(getLabel('error-employee-not-found', 'appointment'));
			}

			$serviceContainer = umiServiceContainers::getContainer();
			/**
			 * @var AppointmentEmployeesSchedulesCollection $employeesSchedulesCollection
			 */
			$employeesSchedulesCollection = $serviceContainer->get('AppointmentEmployeesSchedules');
			$collectionMap = $employeesSchedulesCollection->getMap();

			$params = [];
			$params[$collectionMap->get('EMPLOYEE_ID_FIELD_NAME')] = $employeeId;
			$employeeSchedules = $this->module->getEmployeesSchedules($params);
			$total = count($employeeSchedules);

			if ($total == 0) {
				return appointment::parseTemplate($emptyTemplate, $employeeSchedules);
			}

			$items = [];
			$result = [];
			$timePregReplacePattern = $this->module->timePregReplacePattern;

			/**
			 * @var AppointmentEmployeeSchedule $employeeSchedule
			 */
			foreach ($employeeSchedules as $employeeSchedule) {
				$item = [];
				$item['attribute:id'] = $employeeSchedule->getId();
				$item['attribute:number'] = $employeeSchedule->getDayNumber();
				$item['attribute:name'] = getLabel('label-day-' . $employeeSchedule->getDayNumber(), 'appointment');
				$item['attribute:time_start'] = preg_replace($timePregReplacePattern, '', $employeeSchedule->getTimeStart());
				$item['attribute:time_end'] = preg_replace($timePregReplacePattern, '', $employeeSchedule->getTimeEnd());
				$items[] = appointment::parseTemplate($scheduleTemplate, $item);
			}

			$result['subnodes:items'] = $items;
			$result['total'] = $total;
			$result['employee_id'] = $employeeId;
			return appointment::parseTemplate($schedulesTemplate, $result);
		}

		/**
		 * Возвращает список возможных значений для времени начала или конца работы сотрудника
		 * @param string $template имя шаблона (для tpl)
		 * @param null|string $selectedTime выбранное значение (будет помечено в списке)
		 * @return mixed
		 */
		public function getScheduleWorkTimes($template = 'default', $selectedTime = null) {
			list($workTimesTemplate, $workTimeTemplate) = appointment::loadTemplates(
				'appointment/' . $template,
				'work_times_block',
				'work_time_line'
			);

			$items = [];
			$result = [];
			$hoursCounter = 0;

			for ($counter = 0; $counter < 48; $counter++) {
				$isOddCounter = ($counter % 2) == 1;
				$minutes = ($isOddCounter) ? '30' : '00';
				$hoursCounter = (strlen($hoursCounter) == 1)  ? '0' . $hoursCounter : (string) $hoursCounter;

				$item = [];
				$item['attribute:number'] = $counter + 1;
				$item['attribute:value'] = $hoursCounter . ':' . $minutes;

				if ($selectedTime == $item['attribute:value']) {
					$item['attribute:selected'] = 'selected';
				}

				$items[] = appointment::parseTemplate($workTimeTemplate, $item);

				if ($isOddCounter) {
					$hoursCounter++;
				}
			}

			$result['subnodes:items'] = $items;
			return appointment::parseTemplate($workTimesTemplate, $result);
		}

		/**
		 * Возвращает список идентификатор услуг, которые умеет оказывать заданные сотрудник
		 * @param string $template имя шаблона (для tpl)
		 * @param int $employeeId идентификатор сотрудника
		 * @return mixed
		 * @throws publicAdminException
		 */
		public function employeeServicesIdsList($template = 'default', $employeeId) {
			list($servicesIdsTemplate, $serviceIdTemplate, $emptyTemplate) = appointment::loadTemplates(
				'appointment/' . $template,
				'services_ids_block',
				'service_id_line',
				'services_ids_empty'
			);

			if (!is_numeric($employeeId)) {
				throw new publicAdminException(getLabel('error-employee-not-found', 'appointment'));
			}

			$serviceContainer = umiServiceContainers::getContainer();
			/**
			 * @var AppointmentEmployeesServicesCollection $employeesServicesCollection
			 */
			$employeesServicesCollection = $serviceContainer->get('AppointmentEmployeesServices');
			$collectionMap = $employeesServicesCollection->getMap();

			$params = [];
			$params[$collectionMap->get('EMPLOYEE_ID_FIELD_NAME')] = $employeeId;
			$employeesServicesIds = $this->module->getEmployeesServices($params);
			$total = count($employeesServicesIds);

			if ($total == 0) {
				return appointment::parseTemplate($emptyTemplate, $employeesServicesIds);
			}

			$items = [];
			$result = [];
			/**
			 * @var AppointmentEmployeeService $employeeServiceId
			 */
			foreach ($employeesServicesIds as $employeeServiceId) {
				$item = [];
				$item['attribute:id'] = $employeeServiceId->getId();
				$item['attribute:service_id'] = $employeeServiceId->getServiceId();
				$items[] = appointment::parseTemplate($serviceIdTemplate, $item);
			}

			$result['subnodes:items'] = $items;
			$result['total'] = $total;
			$result['employee_id'] = $employeeId;
			return appointment::parseTemplate($servicesIdsTemplate, $result);
		}
	}
?>