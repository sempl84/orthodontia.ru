<?php
	/**
	 * @deprecated Класс для работы с фильтрами.
	 * Класс умеет применять фильтры для генератора выборок umiSelection.
	 */
	class DataFilters {
		/**
		 * @var data $module
		 */
		public $module;

		/**
		 * Примеряет фильтр по значению свойства "Name" объектов выборки
		 * @param umiSelection $sel выборка
		 * @param array|string $value значение для фильтрации
		 * @return bool|void
		 */
		public function applyFilterName(umiSelection $sel, $value) {
			if (empty($value)) {
				return false;
			}

			if (is_array($value)) {
				foreach($value as $key => $val) {
					if ($key == "eq") {
						$sel->addNameFilterEquals($val);
					}

					if ($key == "like") {
						$sel->addNameFilterLike($val);
					}
				}
				return;
			}

			$sel->addNameFilterLike($value);
		}

		/**
		 * Примеряет фильтр по значению поля типов "Строка", "Простой текст" и "HTML текст" объектов выборки
		 * @param umiSelection $sel выборка
		 * @param umiField $field объект поля
		 * @param array|string $value значение для фильтрации
		 * @return bool|void
		 */
		public function applyFilterText(umiSelection $sel, umiField $field, $value) {
			$value = trim($value);

			if (mb_strlen($value) == 0) {
				return false;
			}

			if ($this->applyKeyedFilters($sel, $field, $value)) {
				return;
			}

			if (is_array($value)) {
				return;
			}

			$sel->addPropertyFilterLike($field->getId(), $value);
		}

		/**
		 * Примеряет фильтр по значению поля типов "Число", "Счетчик" объектов выборки
		 * @param umiSelection $sel выборка
		 * @param umiField $field объект поля
		 * @param array|string $value значение для фильтрации
		 * @return bool|void
		 */
		public function applyFilterInt(umiSelection $sel, umiField $field, $value) {
			if (empty($value)) {
				return false;
			}

			if ($this->applyKeyedFilters($sel, $field, $value)) {
				return;
			}

			$tmp = array_extract_values($value);
			if (empty($tmp)) {
				return false;
			}

			if (!empty($value[1])) {
				$sel->addPropertyFilterBetween($field->getId(), $value[0], $value[1]);
			} else {
				if (!empty($value[0])) {
					$sel->addPropertyFilterMore($field->getId(), $value[0]);
				}
			}
		}

		/**
		 * Примеряет фильтр по значению полям типа "Выпадающие список"
		 * и "Выпадающий список со множественным выбором" объектов выборки
		 * @param umiSelection $sel выборка
		 * @param umiField $field объект поля
		 * @param array|string $value значение для фильтрации
		 * @return bool|void
		 */
		public function applyFilterRelation(umiSelection $sel, umiField $field, $value) {
			if (empty($value)) {
				return false;
			}

			if ($this->applyKeyedFilters($sel, $field, $value)) {
				return;
			}

			$value = $this->searchRelationValues($field, $value);

			$sel->addPropertyFilterEqual($field->getId(), $value);
		}

		/**
		 * Примеряет фильтр по значению поля типа "Цена" объектов выборки
		 * @param umiSelection $sel выборка
		 * @param umiField $field объект поля
		 * @param array|string $value значение для фильтрации
		 * @return bool|void
		 */
		public function applyFilterPrice(umiSelection $sel, umiField $field, $value) {
			if (empty($value)) {
				return false;
			}

			if ($this->applyKeyedFilters($sel, $field, $value)) {
				return;
			}

			$tmp = array_extract_values($value);
			if (empty($tmp)) {
				return false;
			}

			if (!empty($value[1])) {
				if ($value[0] <= $value[1]) {
					$minValue = $value[0];
					$maxValue = $value[1];
				} else {
					$minValue = $value[1];
					$maxValue = $value[0];
				}

				$sel->addPropertyFilterBetween($field->getId(), $minValue, $maxValue);
			} else {
				if (isset($value[0])) {
					$sel->addPropertyFilterMore($field->getId(), $value[0]);
				}
			}
		}

		/**
		 * Примеряет фильтр по значению поля типа "Дата" объектов выборки
		 * @param umiSelection $sel выборка
		 * @param umiField $field объект поля
		 * @param array|string $value значение для фильтрации
		 * @return bool|void
		 */
		public function applyFilterDate(umiSelection $sel, umiField $field, $value) {
			if (empty($value)) {
				return false;
			}
			$valueArray = (array) $value;

			foreach ($valueArray as $i => $val) {
				$valueArray[$i] = umiDate::getTimeStamp($val);
			}

			if ($this->applyKeyedFilters($sel, $field, $valueArray)) {
				return;
			}

			if (!empty($valueArray[1])) {
				$sel->addPropertyFilterBetween($field->getId(), $valueArray[0], $valueArray[1]);
			} else {
				if (!empty($valueArray[0])) {
					$sel->addPropertyFilterMore($field->getId(), $valueArray[0]);
				}
			}
		}

		/**
		 * Примеряет фильтр по значению поля типа "Число с точкой" объектов выборки
		 * @param umiSelection $sel выборка
		 * @param umiField $field объект поля
		 * @param array|string $value значение для фильтрации
		 * @return bool|void
		 */
		public function applyFilterFloat(umiSelection $sel, umiField $field, $value) {
			if (empty($value)) {
				return false;
			}

			if ($this->applyKeyedFilters($sel, $field, $value)) {
				return;
			}

			$tmp = array_extract_values($value);
			if (empty($tmp)) {
				return false;
			}

			if (!empty($value[1])) {
				$sel->addPropertyFilterBetween($field->getId(), $value[0], $value[1]);
			} else {
				if (!empty($value[0])) {
					$sel->addPropertyFilterMore($field->getId(), $value[0]);
				}
			}
		}

		/**
		 * Примеряет фильтр по значению поля типа "Кнопка-флажок" объектов выборки
		 * @param umiSelection $sel выборка
		 * @param umiField $field объект поля
		 * @param array|string $value значение для фильтрации
		 * @return bool|void
		 */
		public function applyFilterBoolean(umiSelection $sel, umiField $field, $value) {
			if (empty($value)) {
				return false;
			}

			if ($this->applyKeyedFilters($sel, $field, $value)) {
				return;
			}

			if ($value) {
				$sel->addPropertyFilterEqual($field->getId(), $value);
			}
		}


		/**
		 * Применяет значения фильтра к полю объектов выборки
		 * @param umiSelection $sel выборка
		 * @param umiField $field поле
		 * @param mixed $values значение фильтра
		 * @return bool
		 */
		private function applyKeyedFilters(umiSelection $sel, umiField $field, $values) {
			if (is_array($values) == false) {
				return false;
			}

			foreach($values as $key => $value) {
				if (is_numeric($key) || $value === "") {
					return false;
				}

				$dataType = $field->getFieldType()->getDataType();

				switch ($key) {
					case "eq": {
						if (is_array($value)) {
							foreach ($value as $v) {
								$this->applyKeyedFilters($sel, $field, Array($key => $v));
							}
							break;
						}

						$value = $this->searchRelationValues($field, $value);

						if ($dataType == "date") {
							$value = strtotime(date("Y-m-d", $value));
							$sel->addPropertyFilterBetween($field->getId(), $value, ($value + 3600 * 24));
							break;
						}
						if ($dataType == "file" || $dataType == "img_file" || $dataType == "swf_file") {
							if ($value > 0) {
								$sel->addPropertyFilterIsNotNull($field->getId());
							} else {
								$sel->addPropertyFilterIsNull($field->getId());
							}
						} else {
							$sel->addPropertyFilterEqual($field->getId(), $value);
						}
						break;
					}
					case "ne": {
						$sel->addPropertyFilterNotEqual($field->getId(), $value);
						break;
					}
					case "lt": {
						$sel->addPropertyFilterLess($field->getId(), $value);
						break;
					}
					case "gt": {
						$sel->addPropertyFilterMore($field->getId(), $value);
						break;
					}
					case "like": {
						$value = $this->searchRelationValues($field, $value);
						if (is_array($value)) {
							foreach ($value as $val) {
								if ($val) {
									$sel->addPropertyFilterLike($field->getId(), $val);
								}
							}
						} else {
							$sel->addPropertyFilterLike($field->getId(), $value);
						}
						break;
					}
					default: {
						return false;
					}
				}
			}
			return true;
		}

		/**
		 * Подготавливает значения фильтра по полям типов "Выпадающий список" и
		 * "Выпадающий список со множественным выбором" для применения к выборке
		 * @param iUmiField $field поле
		 * @param mixed $value значения фильтра поля
		 * @return Array
		 */
		private function searchRelationValues(iUmiField $field, $value) {
			if (is_array($value)) {
				$result = Array();
				foreach ($value as $sval) {
					$result[] = $this->searchRelationValues($field, $sval);
				}
				return $result;
			}

			$guideId = $field->getGuideId();

			if ($guideId) {
				if (is_numeric($value)) {
					return $value;
				} else {
					$sel = new umiSelection;
					$sel->addObjectType($guideId);
					$sel->searchText($value);
					$result = umiSelectionsParser::runSelection($sel);
					return sizeof($result) ? $result : array(-1);
				}
			} else {
				return $value;
			}
		}
	};
?>