<?php
	/**
	 * Класс функционала административной панели модуля
	 */
	class DispatchesAdmin {

		use baseModuleAdmin;
		/**
		 * @var dispatches $module
		 */
		public $module;

		/**
		 * Возвращает список рассылок
		 * @return bool|void
		 * @throws coreException
		 * @throws selectorException
		 */
		public function lists() {
			$this->setDataType("list");
			$this->setActionType("view");

			if ($this->module->ifNotXmlMode()) {
				$this->setDirectCallError();
				$this->doData();
				return true;
			}

			$limit = getRequest('per_page_limit');
			$curr_page = (int) getRequest('p');
			$offset = $limit * $curr_page;

			$sel = new selector('objects');
			$sel->types('object-type')->name('dispatches', 'dispatch');
			$sel->limit($offset, $limit);
			selectorHelper::detectFilters($sel);

			$this->setDataRange($limit, $offset);
			$data = $this->prepareData($sel->result, "objects");
			$this->setData($data, $sel->length);
			$this->doData();
		}

		/**
		 * Возвращает список подписчиков на рассылку
		 * @return bool|void
		 * @throws coreException
		 * @throws selectorException
		 */
		public function subscribers() {
			$this->setDataType("list");
			$this->setActionType("view");

			if ($this->module->ifNotXmlMode()) {
				$this->setDirectCallError();
				$this->doData();
				return true;
			}

			$limit = getRequest('per_page_limit');
			$curr_page = (int) getRequest('p');
			$offset = $limit * $curr_page;

			$dispatchId = getRequest("param0") ? getRequest("param0") : getRequest('id');

			if (is_array($dispatchId)) {
				$dispatchId = isset($dispatchId[0]) ? $dispatchId[0] : null;
			}

			$sel = new selector('objects');
			$sel->types('object-type')->name('dispatches', 'subscriber');

			if ($dispatchId) {
				$sel->where('subscriber_dispatches')->equals($dispatchId);
			}

			$sel->limit($offset, $limit);
			selectorHelper::detectFilters($sel);

			$this->setDataRange($limit, $offset);
			$data = $this->prepareData($sel->result, "objects");
			$this->setData($data, $sel->length);
			$this->doData();
		}

		/**
		 * Возвращает список выпусков рассылки
		 * @return bool
		 * @throws coreException
		 * @throws selectorException
		 */
		public function releases() {
			$this->setDataType("list");
			$this->setActionType("view");

			if ($this->module->ifNotXmlMode()) {
				$this->setDirectCallError();
				$this->doData();
				return true;
			}

			$limit = getRequest('per_page_limit');
			$curr_page = (int) getRequest('p');
			$offset = $limit * $curr_page;

			$dispatchId = getRequest("param0") ? getRequest("param0") : getRequest('id');
			if (is_array($dispatchId)) {
				$dispatchId = isset($dispatchId[0]) ? $dispatchId[0] : null;
			}

			$sel = new selector('objects');
			$sel->types('object-type')->name('dispatches', 'release');

			if ($dispatchId) {
				$sel->where('disp_reference')->equals($dispatchId);
			}

			$sel->limit($offset, $limit);
			selectorHelper::detectFilters($sel);

			$this->setDataRange($limit, $offset);
			$data = $this->prepareData($sel->result, "objects");
			$this->setData($data, $sel->length);
			$this->doData();
		}

		/**
		 * Возвращает список сообщений рассылки
		 * @throws coreException
		 */
		public function messages() {
			$dispatch_id = getRequest("param0");

			$o_dispatch = umiObjectsCollection::getInstance()->getObject($dispatch_id);
			$release_id = false;

			if ($o_dispatch instanceof umiObject) {
				$release_id = $this->module->getNewReleaseInstanceId($dispatch_id);
			}

			$result = $this->module->getReleaseMessages($release_id);
			$this->setDataType("list");
			$this->setActionType("view");
			$data = $this->prepareData($result, "objects");
			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает данные для создания формы добавления рассылки,
		 * если передан $_REQUEST['param1'] = do, то создает рассылку
		 * и перенаправляет страницу, где ее можно отредактировать.
		 * @throws coreException
		 * @throws publicAdminException
		 * @throws wrongElementTypeAdminException
		 */
		public function add() {
			$type = (string) getRequest("param0");
			$mode = (string) getRequest("param1");
			$this->setHeaderLabel("header-dispatches-add-" . $type);

			$inputData = array(
				"type" => $type
			);

			if ($mode == "do") {
				$object = $this->saveAddedObjectData($inputData);
				$added = umiObjectsCollection::getInstance()->getObject($object->getId());
				$added->setValue("subscribe_date", time());
				$added->commit();
				$this->chooseRedirect($this->module->pre_lang . '/admin/dispatches/edit/' . $object->getId() . "/");
			}

			$this->setDataType("form");
			$this->setActionType("create");
			$data = $this->prepareData($inputData, "object");
			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает данные для создания формы редактирования рассылки.
		 * Если передан ключевой параметр $_REQUEST['param1'] = do,
		 * то сохраняет изменения рассылки и производит перенаправление.
		 * Адрес перенаправление зависит от режима кнопки "Сохранить".
		 * @throws coreException
		 * @throws expectObjectException
		 */
		public function edit() {
			$object = $this->expectObject("param0");
			$mode = (string) getRequest('param1');
			$this->setHeaderLabel("header-dispatches-edit-" . $this->getObjectTypeMethod($object));

			if ($mode == "do") {
				$this->saveEditedObjectData($object);
				$this->chooseRedirect();
			}

			$this->setDataType("form");
			$this->setActionType("modify");
			$data = $this->prepareData($object, "object");
			$iTypeId = $object->getTypeId();

			$iHTypeId = umiObjectTypesCollection::getInstance()->getType($iTypeId)->getHierarchyTypeId();
			$oHType = umiHierarchyTypesCollection::getInstance()->getType($iHTypeId);

			if ($oHType->getExt() == 'dispatch') {
				$iReleaseId = $this->module->getNewReleaseInstanceId($object->getId());
				$arrMess = $this->module->getReleaseMessages($iReleaseId);
				$data['object']['release'] = array();
				$data['object']['release']['nodes:message'] = $arrMess;

			}

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Изменяет активность рассылок
		 * @throws coreException
		 * @throws expectObjectException
		 */
		public function activity() {
			$objects = getRequest('object');

			if (!is_array($objects)) {
				$objects = Array($objects);
			}

			$is_active = (bool) getRequest('active');

			foreach ($objects as $objectId) {
				$object = $this->expectObject($objectId, false, true);
				$object->setValue("is_active", $is_active);
				$object->commit();
			}

			$this->setDataType("list");
			$this->setActionType("view");
			$data = $this->prepareData($objects, "objects");
			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает данные для создания формы добавления сообщения рассылки,
		 * если передан $_REQUEST['param1'] = do, то создает сообщение
		 * и перенаправляет страницу, где его можно отредактировать.
		 * @throws coreException
		 * @throws publicAdminException
		 * @throws wrongElementTypeAdminException
		 */
		public function add_message() {
			$type = "message";
			$dispatch_rel = (int) getRequest("param0");
			$mode = (string) getRequest("param1");

			$inputData = Array(
				"type" => $type
			);

			if ($mode == "do") {
				$object = $this->saveAddedObjectData($inputData);
				$object->setValue('release_reference', $this->module->getNewReleaseInstanceId($dispatch_rel));
				$this->chooseRedirect($this->module->pre_lang . '/admin/dispatches/edit/' . $object->getId() . "/");
			}

			$this->setDataType("form");
			$this->setActionType("create");
			$data = $this->prepareData($inputData, "object");
			$this->setData($data);
			$this->doData();
		}

		/**
		 * Удаляет список объектов модуля
		 * @throws coreException
		 * @throws expectObjectException
		 * @throws wrongElementTypeAdminException
		 */
		public function del() {
			$objects = getRequest('element');

			if (!is_array($objects)) {
				$objects = Array($objects);
			}

			if (getRequest('param0')) {
				$objectId = getRequest('param0');
				$object = $this->expectObject($objectId, false, true);

				$params = Array(
					'object' => $object,
					'allowed-element-types' => Array(
						'dispatch',
						"subscriber",
						"release",
						"message"
					)
				);

				$this->deleteObject($params);
				$this->chooseRedirect();
			}

			foreach ($objects as $objectId) {
				$object = $this->expectObject($objectId, false, true);

				$params = Array(
					'object' => $object,
					'allowed-element-types' => Array(
						'dispatch',
						"subscriber",
						"release",
						"message"
					)
				);

				$this->deleteObject($params);
			}

			$this->setDataType("list");
			$this->setActionType("view");
			$data = $this->prepareData($objects, "objects");
			$this->setData($data);
			$this->doData();
		}

		/**
		 * Отправляет выпуск рассылки части подписчиков.
		 * Используется итерационно.
		 * @throws coreException
		 * @throws selectorException
		 */
		public function release_send() {
			/**
			 * @var HTTPOutputBuffer $buffer
			 */
			$buffer = outputBuffer::current('HTTPOutputBuffer');
			$buffer->charset('utf-8');
			$buffer->contentType('text/xml');
			$buffer->push('<?xml version="1.0" encoding="utf-8"?>');

			$iDispId = (int) getRequest('param0');
			$iReleaseId = $this->module->getNewReleaseInstanceId($iDispId);
			$objectsColl = umiObjectsCollection::getinstance();
			$controller = cmsController::getInstance();

			$oDispatch = $objectsColl->getObject($iDispId);
			$oRelease = $objectsColl->getObject($iReleaseId);

			if (!$oDispatch instanceof umiObject || !$oRelease instanceof umiObject) {
				$buffer->push("<error>" . getLabel('error-empty-dispatch-id') . "</error>");
				$buffer->end();
			}

			if ($oRelease->getValue('status')) {
				$buffer->push("<error>" . getLabel('error-already-released') . "</error>");
				$buffer->end();
			}

			$arrRecipients = array();
			if (!getSession('umi_send_list_' . $iReleaseId)) {
				$sel = new selector('objects');
				$sel->types("hierarchy-type")->name("dispatches", "subscriber");
				$sel->where("subscriber_dispatches")->equals($iDispId);
				$sel->option('return')->value('id');
				$sel->group("name");

				foreach ($sel->result() as $recipient) {
					$arrRecipients[] = $recipient['id'];
				}

				$_SESSION['umi_send_list_' . $iReleaseId] = $arrRecipients;
				$_SESSION['umi_send_list_' . $iReleaseId . '_count'] = count($arrRecipients);
			} else {
				$arrRecipients = getSession('umi_send_list_' . $iReleaseId);
			}

			$delay = getSession('umi_send_list_' . $iReleaseId . '_delay');
			$iTotal = (int) getSession('umi_send_list_' . $iReleaseId . '_count');

			if ($delay and  time() < $delay) {
				$iSended = $iTotal - count($arrRecipients);
				$sResult = <<<END
<release dispatch="{$iDispId}">
	<total>{$iTotal}</total>
	<sended>{$iSended}</sended>
</release>
END;
				$buffer->push($sResult);
				$buffer->end();
			}

			$oMailer = new umiMail();

			$arrMailBlocks = array();
			$arrMailBlocks['header'] = $oDispatch->getName();
			$arrMailBlocks['messages'] = "";

			list($sReleaseFrm, $sMessageFrm) = dispatches::loadTemplatesForMail(
				"dispatches/release",
				"release_body",
				"release_message"
			);

			$sel = new selector('objects');
			$sel->types("hierarchy-type")->name("dispatches", "message");
			$sel->where("release_reference")->equals($iReleaseId);

			if ($sel->length()) {
				foreach($sel->result() as $oNextMsg) {
					if ($oNextMsg instanceof umiObject) {
						$arrMsgBlocks = array();
						$arrMsgBlocks['body'] = $oNextMsg->getValue('body');
						$arrMsgBlocks['header'] = $oNextMsg->getValue('header');
						$arrMsgBlocks['id'] = $oNextMsg->getId();

						$arrMailBlocks['messages'] .= dispatches::parseTemplateForMail(
							$sMessageFrm,
							$arrMsgBlocks,
							false,
							$oNextMsg->getId()
						);

						$oNextAttach = $oNextMsg->getValue('attach_file');

						if ($oNextAttach instanceof umiFile && !$oNextAttach->getIsBroken()) {
							$oMailer->attachFile($oNextAttach);
						}
					}
				}
			} else {
				unset($_SESSION[$iDispId . '_new_templater']);
				$buffer->push("<error>" . getLabel('label-release-empty') . "</error>");
				$buffer->end();
			}

			$umiRegistry = regedit::getInstance();
			$oMailer->setFrom($umiRegistry->getVal("//settings/email_from"), $umiRegistry->getVal("//settings/fio_from"));
			$oMailer->setSubject($arrMailBlocks['header']);

			$delay = 0;
			$max_messages = (int) mainConfiguration::getinstance()->get('modules', 'dispatches.max_messages_in_hour');

			if ($max_messages && $iTotal >= $max_messages) {
				$delay = floor(3600 / $max_messages);
			}

			$aSended = array();
			$iPacketSize = 5;
			foreach ($arrRecipients as $recipient_id) {
				--$iPacketSize;
				$oNextMailer = clone $oMailer;
				$recipient = $objectsColl->getObject($recipient_id);
				$oNextSbs = new umiSubscriber($recipient->getId());
				$sRecipientName =  $oNextSbs->getValue('lname') . " " . $oNextSbs->getValue('fname') . " " . $oNextSbs->getValue('father_name');
				$mail = $oNextSbs->getValue('email');

				if (!strlen($mail)) {
					$mail = $oNextSbs->getName();
				}

				$arrMailBlocks['unsubscribe_link'] = $this->module->getUnSubscribeLink($recipient, $mail);

				$sMailBody = dispatches::parseTemplateForMail(
					$sReleaseFrm,
					$arrMailBlocks,
					false,
					$oNextSbs->getId()
				);

				$oNextMailer->setContent($sMailBody);

				$oNextMailer->addRecipient($mail, $sRecipientName);

				if (!(defined('CURRENT_VERSION_LINE') && CURRENT_VERSION_LINE == 'demo' )) {
					$oNextMailer->commit();
					$oNextMailer->send();
				}

				$aSended[] = $recipient_id;
				unset($oNextMailer);
				if ($iPacketSize === 0) {
					break;
				}

				$objectsColl->unloadObject($recipient_id);

				if ($delay) {
					$_SESSION['umi_send_list_' . $iReleaseId . '_delay'] = $delay + time();
					$_SESSION['umi_send_list_' . $iReleaseId] = array_diff($arrRecipients, $aSended);
					$iTotal = (int) getSession('umi_send_list_' . $iReleaseId . '_count');
					$iSended = $iTotal - (count($arrRecipients) - count($aSended));
					$sResult = <<<END
<release dispatch="{$iDispId}">
	<total>{$iTotal}</total>
	<sended>{$iSended}</sended>
</release>
END;
					$buffer->push($sResult);
					$buffer->end();
				}

			}

			umiMail::clearFilesCache();
			$_SESSION['umi_send_list_' . $iReleaseId] = array_diff($arrRecipients, $aSended);

			if (!count(getSession('umi_send_list_' . $iReleaseId))) {
				$oRelease->setValue('status', true);
				$oDate = new umiDate(time());
				$oRelease->setValue('date', $oDate);
				$oRelease->setName($oDate->getFormattedDate('d-m-Y H:i'));
				$oRelease->commit();

				$oDispatch->setValue('disp_last_release', $oDate);
				$oDispatch->commit();
			}

			$iTotal = (int) getSession('umi_send_list_' . $iReleaseId . '_count');
			$iSended = $iTotal - (count($arrRecipients) - count($aSended));

			$sResult = <<<END
<release dispatch="{$iDispId}">
	<total>{$iTotal}</total>
	<sended>{$iSended}</sended>
</release>
END;

			unset($_SESSION[$iDispId . '_new_templater']);
			$buffer->push($sResult);
			$buffer->end();
		}

		/**
		 * Возвращает настройки для формирования табличного контрола
		 * @param string $param контрольный параметр
		 * @return array
		 */
		public function getDatasetConfiguration($param = '') {
			switch ($param) {
				case 'lists': {
					$loadMethod = 'lists';
					$method = 'dispatch';
					$defaults	= 'name[400px]|disp_description[250px]|disp_last_release[250px]';
					break;
				}
				case 'subscribers' : {
					$loadMethod = 'subscribers';
					$method = 'subscriber';
					$defaults	= 'name[400px]|subscriber_dispatches[250px]';
					break;
				}
				case 'releases' : {
					$loadMethod = 'releases';
					$method = 'release';
					$defaults	= 'name[400px]|date[250px]';
					break;
				}
				default: {
					$loadMethod = 'messages';
					$method = 'message';
					$defaults	= 'name[400px]|msg_date[250px]';
				}
			}

			$umiObjectTypesCollection = umiObjectTypesCollection::getInstance();
			$typeId = $umiObjectTypesCollection->getTypeIdByHierarchyTypeName('dispatches', $method);

			return array(
				'methods' => array(
					array(
						'title'		=> getLabel('smc-load'),
						'forload'	=> true,
						'module'	=> 'dispatches',
						'#__name'	=> $loadMethod
					),
					array(
						'title'		=> getLabel('smc-delete'),
						'module'	=> 'dispatches',
						'#__name'	=> 'del',
						'aliases'	=> 'tree_delete_element,delete,del'
					),
					array(
						'title'		=> getLabel('smc-activity'),
						'module'	=> 'dispatches',
						'#__name'	=> 'activity',
						'aliases'	=> 'tree_set_activity,activity'
					),
				),
				'types' => array(
					array(
						'common' => 'true',
						'id' => $typeId
					)
				),
				'stoplist' => array(
					'disp_reference',
					'new_relation',
					'release_reference'
				),
				'default' => $defaults
			);
		}
	}
?>