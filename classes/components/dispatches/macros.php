<?php
	/**
	 * Класс макросов, то есть методов, доступных в шаблоне
	 */
	class DispatchesMacros {
		/**
		 * @var dispatches $module
		 */
		public $module;

		/**
		 * Подписывает на рассылку.
		 * @return array|mixed|string
		 * @throws publicException
		 */
		public function subscribe_do() {
			$requestData = $this->getSubscriptionRequest();
			$email = $this->getSubscriptionEmail($requestData['email']);

			if (!umiMail::checkEmail($email)) {
				return $this->getSubscriptionError('%subscribe_incorrect_email%');
			}

			$data = $this->getInitialData($requestData);
			$permissions = permissionsCollection::getInstance();
			$dispatches = $this->getActualDispatches($requestData['dispatches']);
			$subscriber = $this->getExistingSubscriber($requestData['email']);

			if ($this->module->isSubscriber($subscriber)) {
				if ($permissions->isAuth()) {
					$this->updateSubscriber($subscriber, $data);
				} else {
					$this->subscribeDispatches($subscriber, $dispatches);

					list($templateBlock) = dispatches::loadTemplates(
						"dispatches/default",
						"subscribe_guest_alredy_subscribed"
					);

					$result = array();
					$result['unsubscribe_link'] = $this->module->getUnSubscribeLink($subscriber, $email);
					return dispatches::parseTemplate($templateBlock, $result);
				}

			} else {
				$subscriber = $this->createSubscriber($data);
			}

			$this->sendSubscribingLetter($subscriber, $email);
			$this->subscribeDispatches($subscriber, $dispatches);

			return $this->getSubscriptionResult($dispatches);
		}

		/**
		 * Возвращает данные для создания формы подписки на рассылку.
		 * @param string $sTemplate имя шаблона (для tpl)
		 * @return mixed
		 * @throws coreException
		 */
		public function subscribe($sTemplate = "default") {
			if (!$sTemplate) {
				$sTemplate = "default";
			}

			list(
				$sUnregistredForm,
				$sRegistredForm,
				$sDispatchesForm,
				$sDispatchRowForm
				) = dispatches::loadTemplates(
				"dispatches/" . $sTemplate,
				"subscribe_unregistred_user",
				"subscribe_registred_user",
				"subscriber_dispatches",
				"subscriber_dispatch_row"
			);

			$permissions = permissionsCollection::getInstance();
			$isAuth = $permissions->isAuth();

			if ($isAuth) {
				$arrRegBlock = array();
				$arrSbsDispatches = array();

				$iUserId = (int) $permissions->getUserId();
				$oSubscriber = $this->module->getSubscriberByUserId($iUserId);

				if ($oSubscriber instanceof umiObject) {
					$arrSbsDispatches = $oSubscriber->getValue('subscriber_dispatches');
				}

				$arrRegBlock['subscriber_dispatches'] = $this->parseDispatches($sDispatchesForm, $sDispatchRowForm, $arrSbsDispatches);
				return dispatches::parseTemplate($sRegistredForm, $arrRegBlock);
			}

			$arrUnregBlock = array();
			$umiTypesHelper = umiTypesHelper::getInstance();
			$subscriberFields = $umiTypesHelper->getFieldsByObjectTypeGuid('dispatches-subscriber');
			$subscriberTypeId = $umiTypesHelper->getObjectTypeIdByGuid('dispatches-subscriber');

			if (isset($subscriberFields[$subscriberTypeId]['gender'])) {
				$oSbsGenderFld = umiFieldsCollection::getInstance()->getField($subscriberFields[$subscriberTypeId]['gender']);
				$arrGenders = umiObjectsCollection::getInstance()->getGuidedItems($oSbsGenderFld->getGuideId());

				$sGenders = Array();

				foreach ($arrGenders as $iGenderId => $sGenderName) {
					$sGenders[] = "<option value=\"" . $iGenderId . "\">" . $sGenderName . "</option>";
				}

				$arrUnregBlock['void:sbs_genders'] = $sGenders;
			}

			return dispatches::parseTemplate($sUnregistredForm, $arrUnregBlock);
		}

		/**
		 * Отписывает подписчика от рассылки
		 * @return string
		 */
		public function unsubscribe() {
			$subscriberId = (int) getRequest('id');
			$subscriberEmail = (string) getRequest('email');
			$subscriber = umiObjectsCollection::getInstance()->getObject($subscriberId);

			if ($this->module->isSubscriber($subscriber) && $subscriber->getName() == $subscriberEmail) {
				$subscriber->setValue('subscriber_dispatches', null);
				$subscriber->commit();
				return dispatches::parseTPLMacroses('%subscribe_unsubscribed_ok%');
			}

			return dispatches::parseTPLMacroses('%subscribe_unsubscribed_failed%');
		}

		/**
		 * Возвращает список рассылок
		 * @param mixed $sDispatchesForm общий блок шаблона (для tpl)
		 * @param mixed $sDispatchRowForm блок шаблона отдельной рассылки (для tpl)
		 * @param array $arrChecked список выбранных рассылок
		 * @param bool $bOnlyChecked выводить только выбранные рассылки
		 * @return mixed
		 */
		public function parseDispatches($sDispatchesForm, $sDispatchRowForm, $arrChecked = array(), $bOnlyChecked = false) {
			$arrDispSelResults = $this->module->getAllDispatches();
			$arrDispsBlock = array();
			$arrDispsBlock['void:rows'] = Array();

			if (is_array($arrDispSelResults) && count($arrDispSelResults)) {
				foreach ($arrDispSelResults as $dispatch) {

					if (!$dispatch instanceof umiObject) {
						continue;
					}

					$iNextDispId = $dispatch->getId();
					$arrDispRowBlock = "";
					$arrDispRowBlock['attribute:id'] = $arrDispRowBlock['void:disp_id'] = $dispatch->getId();
					$arrDispRowBlock['node:disp_name'] = $dispatch->getName();
					$arrDispRowBlock['attribute:is_checked'] = (in_array($iNextDispId, $arrChecked)? 1: 0);
					$arrDispRowBlock['void:checked'] = ($arrDispRowBlock['attribute:is_checked']? "checked": "");

					if ($arrDispRowBlock['attribute:is_checked']  || !$bOnlyChecked) {
						$arrDispsBlock['void:rows'][] = dispatches::parseTemplate($sDispatchRowForm, $arrDispRowBlock, false, $iNextDispId);
					}
				}
			}

			$arrDispsBlock['nodes:items'] = $arrDispsBlock['void:rows'];
			return dispatches::parseTemplate($sDispatchesForm, $arrDispsBlock);
		}

		/**
		 * Возвращает сообщение об ошибки подписки на рассылку
		 * @param string $message сообщение об ошибке
		 * @return array
		 */
		public function getSubscriptionError($message) {
			if (!dispatches::isXSLTResultMode()) {
				return $message;
			}

			return array(
				'result' => array(
					'@class' => 'error',
					'node'	 => $message
				)
			);
		}

		/**
		 * Возращает результат подписки на рассылку(и)
		 * @param array $dispatches список ID рассылок
		 * @return array|mixed|string
		 */
		public function getSubscriptionResult(array $dispatches) {
			$permissions = permissionsCollection::getInstance();

			$result = '%subscribe_subscribe%';

			if ($permissions->isAuth()) {
				$blockTemplate = '%subscribe_subscribe_user%:<br /><ul>%rows%</ul>';
				$itemTemplate = '<li>%disp_name%</li>';
				$result = $this->parseDispatches($blockTemplate, $itemTemplate, $dispatches, true);
			}

			return (!dispatches::isXSLTResultMode()) ? $result : array('result' => $result);
		}

		/**
		 * Возвращает массив с данными запроса для подписки на рассылки
		 * @return array
		 */
		private function getSubscriptionRequest() {
			return array (
				'email' => trim(getRequest('sbs_mail')),
				'name' => getRequest('sbs_fname'),
				'lastName' => getRequest('sbs_lname'),
				'surname' => getRequest('sbs_father_name'),
				'gender' => (int) getRequest('sbs_gender'),
				'dispatches' => getRequest('subscriber_dispatches')
			);
		}

		/**
		 * Возвращает e-mail подписки
		 * @param mixed $email запрошенный e-mail
		 * @return mixed
		 */
		private function getSubscriptionEmail($email) {
			$permissions = permissionsCollection::getInstance();

			if ($permissions->isAuth()) {
				$user = umiObjectsCollection::getInstance()->getObject($permissions->getUserId());

				if ($user instanceof iUmiObject) {
					return $user->getValue('e-mail');
				}
			}

			return $email;
		}

		/**
		 * Возвращает данные подписки
		 * @param array $data запрошенные данные
		 * @return array
		 */
		private function getInitialData($data) {
			$permissions = permissionsCollection::getInstance();

			if ($permissions->isAuth()) {
				$user = umiObjectsCollection::getInstance()->getObject($permissions->getUserId());

				if ($user instanceof iUmiObject) {
					return array(
						'email' => $user->getValue('e-mail'),
						'name' => $user->getValue('fname'),
						'lastName' => $user->getValue('lname'),
						'surname' => $user->getValue('father_name'),
						'gender' => $user->getValue('gender'),
					);
				}
			}

			return $data;
		}

		/**
		 * Возвращает существующего подписчика, если таковой существует
		 * @param mixed $email подписчика
		 * @return bool|null|umiObject
		 */
		private function getExistingSubscriber($email) {
			$permissions = permissionsCollection::getInstance();
			if ($permissions->isAuth()) {
				return $this->module->getSubscriberByUserId($permissions->getUserId());
			}

			return $this->module->getSubscriberByMail($email);
		}

		/**
		 * Создает нового подписчика
		 * @param array $data данные подписчика
		 * @return bool|umiObject
		 * @throws coreException
		 * @throws publicException
		 */
		private function createSubscriber(array $data) {
			$objectTypes = umiObjectTypesCollection::getInstance();
			$subscriberTypeId = $objectTypes->getTypeIdByHierarchyTypeName('dispatches', 'subscriber');
			$objects = umiObjectsCollection::getInstance();

			$subscriberId = $objects->addObject($data['email'], $subscriberTypeId);
			$subscriber = $objects->getObject($subscriberId);

			if ($this->module->isSubscriber($subscriber)) {
				$subscriber->setValue('subscribe_date', new umiDate());
				$this->updateSubscriber($subscriber, $data);

				return $subscriber;
			}

			throw new publicException(getLabel('error-cant-create-subscriber'));
		}

		/**
		 * Возвращает список рассылок для подписки
		 * @param array $dispatches список запрошенных рассылок
		 * @return array
		 */
		private function getActualDispatches($dispatches) {
			$result = array();

			$allDispatches = $this->getDispatchesList($this->module->getAllDispatches());
			if (!is_array($dispatches) || count($dispatches) === 0) {
				return $allDispatches;
			}

			foreach ($dispatches as $dispatchId) {
				$dispatch = umiObjectsCollection::getInstance()->getObject($dispatchId);
				if (!$this->module->isDispatch($dispatch)) {
					continue;
				}
				$result[] = $dispatchId;
			}

			sort($result);

			return (count($result) > 0 ? $result : $allDispatches);
		}

		/**
		 * Возвращает список ID действительных рассылок
		 * @param array $dispatches список объектов рассылок
		 * @return array
		 */
		private function getDispatchesList($dispatches) {
			$list = array();

			if (!is_array($dispatches) || count($dispatches) === 0) {
				return $list;
			}

			/** @var iUmiObject|iUmiEntinty $dispatch */
			foreach ($dispatches as $dispatch) {
				if (!$this->module->isDispatch($dispatch)) {
					continue;
				}

				$list[] = $dispatch->getId();
			}
			return $list;
		}

		/**
		 * Обновляет данные объекта подписчика
		 * @param iUmiObject $subscriber объект подписчика
		 * @param array $data новые данные подписчика
		 */
		private function updateSubscriber(iUmiObject $subscriber, array $data) {
			/**
			 * @var iUmiObject|iUmiEntinty $subscriber
			 */
			$subscriber->setName($data['email']);
			$subscriber->setValue('fname', $data['name']);
			$subscriber->setValue('lname', $data['lastName']);
			$subscriber->setValue('father_name', $data['surname']);
			$subscriber->setValue('gender', $data['gender']);

			$permissions = permissionsCollection::getInstance();
			if ($permissions->isAuth()) {
				$subscriber->setValue('uid', $permissions->getUserId());
			}

			$subscriber->commit();
		}

		/**
		 * Отправляет письмо подписчику с информацией о подписке
		 * @param iUmiObject $subscriber объект подписчика
		 * @param string $subscriberEmail e-mail подписчика
		 * @param string $template имя шаблона письма
		 * @throws coreException
		 * @throws publicException
		 */
		private function sendSubscribingLetter(iUmiObject $subscriber, $subscriberEmail, $template = 'default') {
			$mailData = array();
			$mailData['domain'] = cmsController::getInstance()->getCurrentDomain()->getHost();
			$mailData['unsubscribe_link'] = $this->module->getUnSubscribeLink($subscriber, $subscriberEmail);

			list($templateMail, $templateSubject) = dispatches::loadTemplatesForMail(
				'dispatches/' . $template,
				'subscribe_confirm',
				'subscribe_confirm_subject'
			);

			$content = dispatches::parseTemplateForMail($templateMail, $mailData);
			$subject = dispatches::parseTemplateForMail($templateSubject, $mailData);
			$umiRegistry = regedit::getInstance();

			$confirmMail = new umiMail();
			$confirmMail->addRecipient($subscriberEmail);
			$nameFrom = $umiRegistry->getVal("//settings/fio_from");
			$emailFrom = $umiRegistry->getVal("//settings/email_from");
			$confirmMail->setFrom($emailFrom, $nameFrom);
			$confirmMail->setSubject($subject);
			$confirmMail->setContent($content);
			$confirmMail->commit();
			$confirmMail->send();
		}

		/**
		 * Подписывает подписчика на рассылки
		 * @param iUmiObject $subscriber объект подписчика
		 * @param array $dispatches список ID рассылок
		 */
		private function subscribeDispatches(iUmiObject $subscriber, array $dispatches) {
			/**
			 * @var iUmiObject|iUmiEntinty $subscriber
			 */
			$existingDispatches = $subscriber->getValue('subscriber_dispatches');
			$existingDispatches = array_map('intval', $existingDispatches);
			$newDispatches = array_unique(array_merge($existingDispatches, $dispatches));

			$subscriber->setValue('subscriber_dispatches', $newDispatches);
			$subscriber->commit();
		}
	}
?>