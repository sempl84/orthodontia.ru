<?php
	/**
	 * Базовый класс модуля "Рассылки".
	 *
	 * Модуль управляет следующими сущностями:
	 *
	 * 1) Рассылки;
	 * 2) Выпуски;
	 * 3) Сообщения;
	 * 4) Подписчики;
	 *
	 * Модуль умеет отправлять рассылки, как в вручную (через админинстративную панель),
	 * так и автоматически по срабатыванию системного крона.
	 * @link http://help.docs.umi-cms.ru/rabota_s_modulyami/modul_rassylki/
	 */
	class dispatches extends def_module {

		/**
		 * Конструктор
		 */
		public function __construct() {
			parent::__construct();

			$umiRegistry = regedit::getInstance();
			$this->per_page = (int) $umiRegistry->getVal("//modules/dispatches/per_page");

			if (!$this->per_page) {
				$this->per_page = 15;
			}

			if (cmsController::getInstance()->getCurrentMode() == "admin") {
				$commonTabs = $this->getCommonTabs();

				if ($commonTabs) {
					$commonTabs->add('lists');
					$commonTabs->add('subscribers');
					$commonTabs->add('messages', array('releases'));
				}

				$this->__loadLib("admin.php");
				$this->__implement("DispatchesAdmin");

				$this->loadAdminExtension();

				$this->__loadLib("customAdmin.php");
				$this->__implement("DispatchesCustomAdmin", true);
			} else {
				$this->__loadLib("macros.php");
				$this->__implement("DispatchesMacros");

				$this->loadSiteExtension();

				$this->__loadLib("customMacros.php");
				$this->__implement("DispatchesCustomMacros", true);
			}

			$this->__loadLib("handlers.php");
			$this->__implement("DispatchesHandlers");

			$this->loadCommonExtension();
			$this->loadTemplateCustoms();

			$this->__loadLib("customCommon.php");
			$this->__implement("DispatchesCustomCommon", true);
		}

		/**
		 * Возвращает ссылку на страницу редактирования рассылки
		 * @param int $objectId идентификатор рассылки
		 * @param bool $type контрольный параметр
		 * @return string
		 */
		public function getObjectEditLink($objectId, $type = false) {
			return $this->pre_lang . "/admin/dispatches/edit/"  . $objectId . "/";
		}

		/**
		 * Возвращает является ли объект - подписчиком
		 * @param mixed $object проверяемый объект
		 * @return bool
		 */
		public function isSubscriber($object) {
			return ($object instanceof iUmiObject && $object->getTypeGUID() == 'dispatches-subscriber');
		}

		/**
		 * Возвращает является ли объект - рассылкой
		 * @param mixed $object проверяемый объект
		 * @return bool
		 */
		public function isDispatch($object) {
			return ($object instanceof iUmiObject && $object->getTypeGUID() == 'dispatches-dispatch');
		}

		/**
		 * Возвращает подписчика по идентификатору пользователя
		 * @param int $iUserId идентификатор пользователя
		 * @return bool|null|umiObject
		 * @throws coreException
		 */
		public function getSubscriberByUserId($iUserId) {
			$oSubscriber = null;
			$iSbsHierarchyTypeId = umiHierarchyTypesCollection::getInstance()->getTypeByName("dispatches", "subscriber")->getId();
			$umiObjectTypesCollection = umiObjectTypesCollection::getInstance();
			$iSbsTypeId =  $umiObjectTypesCollection->getTypeIdByHierarchyTypeId($iSbsHierarchyTypeId);
			$oSbsType = $umiObjectTypesCollection->getType($iSbsTypeId);

			$oSbsSelection = new umiSelection;
			$oSbsSelection->addObjectType($iSbsTypeId);
			$oSbsSelection->addPropertyFilterEqual($oSbsType->getFieldId('uid'), $iUserId);
			$arrSbsSelResults = umiSelectionsParser::runSelection($oSbsSelection);

			if (is_array($arrSbsSelResults) && count($arrSbsSelResults)) {
				$iSbsId = $arrSbsSelResults[0];
				$oSubscriber = umiObjectsCollection::getInstance()->getObject($iSbsId);
			}

			return $oSubscriber;
		}

		/**
		 * Возвращает подписчика по почтовому ящику
		 * @param string $sEmail почтовый ящик
		 * @return bool|null|umiObject
		 * @throws coreException
		 */
		public function getSubscriberByMail($sEmail) {
			$oSubscriber = null;
			$iSbsHierarchyTypeId = umiHierarchyTypesCollection::getInstance()->getTypeByName("dispatches", "subscriber")->getId();
			$iSbsTypeId =  umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeId($iSbsHierarchyTypeId);

			$oSbsSelection = new umiSelection;
			$oSbsSelection->addObjectType($iSbsTypeId);
			$oSbsSelection->addNameFilterEquals($sEmail);
			$arrSbsSelResults = umiSelectionsParser::runSelection($oSbsSelection);

			if (is_array($arrSbsSelResults) && count($arrSbsSelResults)) {
				$iSbsId = $arrSbsSelResults[0];
				$oSubscriber = umiObjectsCollection::getInstance()->getObject($iSbsId);
			}

			return $oSubscriber;
		}


		/**
		 * Возвращает список рассылок
		 * @return umiObject[]
		 * @throws selectorException
		 */
		public function getAllDispatches() {
			static $cache = null;

			if (!is_null($cache)) {
				return $cache;
			}

			$dispatches = new selector('objects');
			$dispatches->types('object-type')->name('dispatches', 'dispatch');
			$dispatches->where('is_active')->equals(true);
			$dispatches->option('no-length')->value(true);
			$dispatches->option('load-all-props')->value(true);
			return $cache = $dispatches->result();
		}

		/**
		 * Изменяет рассылку, в которую будут выгружаться новые темы форума,
		 * @param iUmiObject $dispatch новая рассылка
		 * @throws selectorException
		 */
		public function changeLoadFromForumDispatch($dispatch) {
			/** @var iUmiObject|iUmiEntinty $dispatch */

			$dispatches = new selector('objects');
			$dispatches->types('object-type')->name('dispatches', 'dispatch');
			$dispatches->where('load_from_forum')->equals(true);

			/** @var iUmiObject|iUmiEntinty $object */
			foreach ($dispatches as $object) {
				$object->setValue('load_from_forum', false);
				$object->commit();
			}

			$regEdit = regedit::getInstance();
			$regEdit->setVal('//modules/forum/dispatch_id', $dispatch->getId());
			$dispatch->setValue('load_from_forum', true);
			$dispatch->commit();
		}

		/**
		 * Возвращает сообщения рассылки
		 * @param bool|int $iReleaseId идентификатор выпуска рассылки
		 * @return umiObject[]
		 * @throws selectorException
		 */
		public function getReleaseMessages($iReleaseId = false) {
			$sel = new selector('objects');
			$sel->types('object-type')->name('dispatches', 'message');

			if ($iReleaseId) {
				$sel->where('release_reference')->equals($iReleaseId);
			}

			selectorHelper::detectFilters($sel);
			return $sel->result();
		}

		/**
		 * Возвращает идентификатор выпуска указанной рассылки
		 * @param bool|int $iDispId идентификатор рассылки
		 * @return bool|int
		 * @throws coreException
		 */
		public function getNewReleaseInstanceId($iDispId = false) {
			static $arrReleasees = array();

			if (!$iDispId) {
				$iDispId = getRequest("param0");
			}

			if (isset($arrReleasees[$iDispId])) {
				return $arrReleasees[$iDispId];
			}

			$umiObjectTypesCollection = umiObjectTypesCollection::getInstance();
			$iHierarchyTypeId = umiHierarchyTypesCollection::getInstance()->getTypeByName("dispatches", "release")->getId();
			$iReleaseTypeId =  $umiObjectTypesCollection->getTypeIdByHierarchyTypeId($iHierarchyTypeId);
			$oReleaseType = $umiObjectTypesCollection->getType($iReleaseTypeId);

			$oReleaseesSelection = new umiSelection;
			$oReleaseesSelection->addObjectType($iReleaseTypeId);
			$oReleaseesSelection->addPropertyFilterIsNull($oReleaseType->getFieldId('status'), 0, true);
			$oReleaseesSelection->addPropertyFilterEqual($oReleaseType->getFieldId('disp_reference'), $iDispId);
			$arrSelResults = umiSelectionsParser::runSelection($oReleaseesSelection);

			$bIsNewRelease = false;
			$umiObjectsCollection = umiObjectsCollection::getInstance();

			if (is_array($arrSelResults) && isset($arrSelResults[0])) {
				$iReleaseId = $arrSelResults[0];
			} else {
				$iReleaseId = $umiObjectsCollection->addObject("", $iReleaseTypeId);
				$bIsNewRelease = true;
			}
			$oRelease = $umiObjectsCollection->getObject($iReleaseId);

			if ($oRelease instanceof umiObject) {
				if ($bIsNewRelease) {
					$oRelease->setName('-');
					$oRelease->setValue('status', false);
					$oRelease->setValue('disp_reference', $iDispId);
					$oRelease->commit();
				}
				$arrReleasees[$iDispId] = $oRelease->getId();
			}

			return $iReleaseId;
		}

		/**
		 * Заполняет выпуск рассылки сообщениями
		 * @param bool|int $dispatch_id идентификатор рассылки
		 * @param bool $ignore_redirect производить редирект
		 * на страницу с формой редактирования после завершения работы
		 * @throws coreException
		 */
		public function fill_release($dispatch_id = false, $ignore_redirect = false) {
			$iDispId = $dispatch_id ? $dispatch_id : getRequest('param0');
			$umiObjectsCollection = umiObjectsCollection::getInstance();
			$oDispatch = $umiObjectsCollection->getObject($iDispId);

			if (!$oDispatch instanceof umiObject) {
				$this->getFillingResult($ignore_redirect, $iDispId);
				return;
			}

			$iReleaseId = $this->getNewReleaseInstanceId($iDispId);
			$iNewsRelation = $oDispatch->getValue("news_relation");
			$umiHierarchy = umiHierarchy::getInstance();
			$arrNewsLents = $umiHierarchy->getObjectInstances($iNewsRelation, false, true);

			$umiHierarchyTypesCollection = umiHierarchyTypesCollection::getInstance();
			$umiObjectTypesCollection = umiObjectTypesCollection::getInstance();

			$iHierarchyTypeId = $umiHierarchyTypesCollection->getTypeByName("dispatches", "release")->getId();
			$iReleaseTypeId =  $umiObjectTypesCollection->getTypeIdByHierarchyTypeId($iHierarchyTypeId);
			$oReleaseType = $umiObjectTypesCollection->getType($iReleaseTypeId);

			$sel = new umiSelection;
			$sel->addObjectType($iReleaseTypeId);
			$sel->addPropertyFilterEqual($oReleaseType->getFieldId('disp_reference'), $iDispId);
			$arrReleases =  umiSelectionsParser::runSelection($sel);

			if (count($arrNewsLents) == 0) {
				$this->getFillingResult($ignore_redirect, $iDispId);
				return;
			}

			$iElementId = (int) $arrNewsLents[0];
			$hierarchy_type_id = $umiHierarchyTypesCollection->getTypeByName("news", "item")->getId();
			$object_type_id = $umiObjectTypesCollection->getTypeIdByHierarchyTypeName("news", "item");
			$object_type = $umiObjectTypesCollection->getType($object_type_id);
			$publish_time_field_id = $object_type->getFieldId('publish_time');

			$sel = new umiSelection;
			$sel->addElementType($hierarchy_type_id);
			$sel->setOrderByProperty($publish_time_field_id, false);
			$sel->addHierarchyFilter($iElementId, 0, true);
			$sel->addLimit(50);
			$sel->setIsLangIgnored(true);
			$result = umiSelectionsParser::runSelection($sel);

			for ($iI = 0; $iI < count($result); $iI++) {
				$iNextNewId = $result[$iI];
				$oNextNew = $umiHierarchy->getElement($iNextNewId);

				if (!$oNextNew instanceof umiHierarchyElement) {
					continue;
				}

				$sName = $oNextNew->getName();
				$sHeader = $oNextNew->getValue('h1');
				$sShortBody = $oNextNew->getValue('anons');
				$sBody = $oNextNew->getValue('content');
				$oPubTime = $oNextNew->getValue('publish_time');

				if (!strlen($sBody)) {
					$sBody = $sShortBody;
				}

				$iHierarchyTypeId = $umiHierarchyTypesCollection->getTypeByName("dispatches", "message")->getId();
				$iMsgTypeId = $umiObjectTypesCollection->getTypeIdByHierarchyTypeId($iHierarchyTypeId);
				$oMsgType = $umiObjectTypesCollection->getType($iMsgTypeId);

				$oSel = new umiSelection;
				$oSel->addObjectType($iMsgTypeId);
				$oSel->addPropertyFilterEqual($oMsgType->getFieldId('new_relation'), $iNextNewId);
				$oSel->addPropertyFilterEqual($oMsgType->getFieldId('release_reference'), $arrReleases);
				$oSel->addLimit(1);

				$iCount = umiSelectionsParser::runSelectionCounts($oSel);

				if ($iCount > 0) {
					continue;
				}

				$iMsgObjId = $umiObjectsCollection->addObject($sName, $iMsgTypeId);
				$oMsgObj = $umiObjectsCollection->getObject($iMsgObjId);

				if (!$oMsgObj instanceof umiObject) {
					continue;
				}

				$oMsgObj->setValue('release_reference', $iReleaseId);
				$oMsgObj->setValue('header', $sHeader);
				$oMsgObj->setValue('body', $sBody);
				$oMsgObj->setValue('short_body', $sShortBody);
				$oMsgObj->setValue('new_relation', array($iNextNewId));

				if ($oPubTime instanceof umiDate) {
					$oMsgObj->setValue('msg_date', $oPubTime);
				}

				$oMsgObj->commit();
				$umiObjectsCollection->unloadObject($oMsgObj->getId());
			}

			$this->getFillingResult($ignore_redirect, $iDispId);
		}

		/**
		 * Отправляет выпуск рассылки всем подписчикам
		 * @param int $iDispId идентификатор рассылки
		 * @return bool
		 * @throws selectorException
		 */
		public function release_send_full($iDispId) {
			$objectsColl = umiObjectsCollection::getinstance();
			$iReleaseId = $this->getNewReleaseInstanceId($iDispId);

			$oDispatch = $objectsColl->getObject($iDispId);
			$oRelease = $objectsColl->getObject($iReleaseId);

			if (!$oDispatch instanceof umiObject || !$oRelease instanceof umiObject) {
				return false;
			}

			if ($oRelease->getValue('status')) {
				return false;
			}

			$oMailer = new umiMail();
			$arrMailBlocks = array();
			$arrMailBlocks['header'] = $oDispatch->getName();
			$arrMailBlocks['messages'] = "";

			list($sReleaseFrm, $sMessageFrm) = dispatches::loadTemplatesForMail(
				"dispatches/release",
				"release_body",
				"release_message"
			);

			$sel = new selector('objects');
			$sel->types("hierarchy-type")->name("dispatches", "message");
			$sel->where("release_reference")->equals($iReleaseId);
			$arrSelResults = $sel->result();

			if (!$sel->length()) {
				return false;
			}

			foreach ($arrSelResults as $oNextMsg) {
				if (!$oNextMsg instanceof umiObject) {
					continue;
				}

				$arrMsgBlocks = array();
				$arrMsgBlocks['body'] = $oNextMsg->getValue('body');
				$arrMsgBlocks['header'] = $oNextMsg->getValue('header');
				$arrMsgBlocks['id'] = $oNextMsg->getId();

				$arrMailBlocks['messages'] .= dispatches::parseTemplateForMail(
					$sMessageFrm,
					$arrMsgBlocks,
					false,
					$oNextMsg->getId()
				);

				$oNextAttach = $oNextMsg->getValue('attach_file');

				if ($oNextAttach instanceof umiFile && !$oNextAttach->getIsBroken()) {
					$oMailer->attachFile($oNextAttach);
				}

				$objectsColl->unloadObject($arrMsgBlocks['id']);
			}

			$umiRegistry = regedit::getInstance();
			$oMailer->setFrom($umiRegistry->getVal("//settings/email_from"), $umiRegistry->getVal("//settings/fio_from"));
			$oMailer->setSubject($arrMailBlocks['header']);

			$sel = new selector('objects');
			$sel->types("hierarchy-type")->name("dispatches", "subscriber");
			$sel->where("subscriber_dispatches")->equals($iDispId);
			$sel->group("name");

			$delay = 0;
			$max_messages = (int) mainConfiguration::getinstance()->get('modules', 'dispatches.max_messages_in_hour');

			if ($max_messages && $sel->length() >= $max_messages) {
				$delay = floor(3600 / $max_messages);
			}

			/**
			 * @var iUmiObject|iUmiEntinty $recipient
			 */
			foreach ($sel->result() as $recipient) {
				$oNextMailer = clone $oMailer;
				$oNextSbs = new umiSubscriber($recipient->getId());
				$sRecipientName = $oNextSbs->getValue('lname') . " " . $oNextSbs->getValue('fname') . " " . $oNextSbs->getValue('father_name');
				$mail = $oNextSbs->getValue('email');

				if (!strlen($mail)) {
					$mail = $oNextSbs->getName();
				}

				$arrMailBlocks['unsubscribe_link'] = $this->getUnSubscribeLink($recipient, $mail);

				$oNextMailer->setContent(dispatches::parseTemplateForMail(
					$sReleaseFrm,
					$arrMailBlocks,
					false,
					$oNextSbs->getId()
				));

				$oNextMailer->addRecipient($mail, $sRecipientName);
				$oNextMailer->commit();
				$oNextMailer->send();

				if ($delay) {
					sleep($delay);
				}

				$objectsColl->unloadObject($recipient->getId());
			}

			$oDate = new umiDate(time());
			$oDispatch->setValue('disp_last_release', $oDate);
			$oDispatch->commit();

			$oRelease->setValue('status', true);
			$oRelease->setValue('date', $oDate);
			$oRelease->setName($oDate->getFormattedDate('d-m-Y H:i'));
			$oRelease->commit();

			return true;
		}

		/**
		 * Возвращает ссылку отписки от рассылки
		 * @param iUmiObject $subscriber объект подписчика
		 * @param string $email подписчика
		 * @return string
		 */
		public function getUnSubscribeLink(iUmiObject $subscriber, $email) {
			$protocol = getSelectedServerProtocol();
			$domain = cmsController::getInstance()->getCurrentDomain()->getHost();
			/**
			 * @var iUmiObject|iUmiEntinty $subscriber
			 */
			$subscriberId = $subscriber->getId();

			return $protocol . '://' . $domain . $this->pre_lang . '/dispatches/unsubscribe/?id=' . $subscriberId . '&email=' . $email;
		}

		/**
		 * Возвращает результат наполнения выпуска рассылки сообщениями
		 * @param bool $ignore_redirect производить редирект
		 * @param int $iDispId идентификатор рассылки
		 * @return void
		 */
		private function getFillingResult($ignore_redirect, $iDispId) {
			if (!$ignore_redirect) {
				$this->redirect('/admin/dispatches/edit/' . $iDispId . '/');
			}

			return;
		}
	};
?>