<?php
// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('Ormco');
$pdf->SetAuthor('Ormco');
$pdf->SetTitle('Ormco');
$pdf->SetSubject('Ormco');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
//$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

$pdf->SetY(500);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 9, PDF_MARGIN_RIGHT, 20);
$pdf->SetHeaderMargin(false);
$pdf->SetFooterMargin(false);

// set auto page breaks
//$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


// ---------------------------------------------------------

// set default font subsetting mode
//$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.

//$this->SetFont('helvetica', '', 10);


$pdf->SetFont('arial', '', 6, '', true);
//$pdf->SetFont('stsongstdlight', '', 6, '', true);
//$pdf->SetFont('courier', '', 10, '', true);
//$pdf->SetFont('times', '', 18, '', true);
//$pdf->SetFont('helvetica', '', 15, '', true);
//$pdf->SetFont('simsun', '', 9, '', true);
//$pdf->SetFont('timcyr', '', 9, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>false, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));


$orderId = $_REQUEST['oi'];
//var_dump($orderId);
//exit('fff');
$html = file_get_contents('https://orthodontia.ru/emarket/receiptPrint/'.$orderId.'/');
//$html = implode("\n", file('./emarket/receiptt/23942/'));
$pdf->writeHTML($html, true, false, false, false, '');
// Print text using writeHTMLCell()
//$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$filename = $orderId.'.pdf';
$pdf->Output($filename, 'I');

//============================================================+
// END OF FILE
//============================================================+
