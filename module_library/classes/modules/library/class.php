<?php

class library extends def_module {
	public $per_page;

	public function __construct() {
		parent::__construct();

		if (cmsController::getInstance() -> getCurrentMode() == "admin") {

			$configTabs = $this -> getConfigTabs();
			if ($configTabs) {
				$configTabs -> add("config");
			}

			$this -> __loadLib("__admin.php");
			$this -> __implement("__library");

		} else {

			$this -> per_page = regedit::getInstance() -> getVal("//modules/library/per_page");
		}

	}

	public function groupelements($path = "", $template = "default") {
		if ($this -> breakMe())
			return;
		$element_id = cmsController::getInstance() -> getCurrentElementId();

		templater::pushEditable("library", "groupelements", $element_id);
		return $this -> group($element_id, $template) . $this -> listElements($element_id, $template);
	}

	public function item_element() {
		if ($this -> breakMe())
			return;
		$element_id = (int)cmsController::getInstance() -> getCurrentElementId();
		return $this -> view($element_id);
	}

	public function group($elementPath = "", $template = "default", $per_page = false) {
		if ($this -> breakMe())
			return;
		$hierarchy = umiHierarchy::getInstance();
		list($template_block) = def_module::loadTemplates("tpls/library/{$template}.tpl", "group");

		$elementId = $this -> analyzeRequiredPath($elementPath);

		$element = $hierarchy -> getElement($elementId);

		templater::pushEditable("library", "groupelements", $element -> id);
		return self::parseTemplate($template_block, array('id' => $element -> id), $element -> id);

	}

	public function view($elementPath = "", $template = "default") {
		if ($this -> breakMe())
			return;
		$hierarchy = umiHierarchy::getInstance();
		list($template_block) = def_module::loadTemplates("tpls/library/{$template}.tpl", "view");

		$elementId = $this -> analyzeRequiredPath($elementPath);
		$element = $hierarchy -> getElement($elementId);

		templater::pushEditable("library", "item_element", $element -> id);
		return self::parseTemplate($template_block, array('id' => $element -> id), $element -> id);
	}

	public function listGroup($elementPath, $template = "default", $per_page = false, $ignore_paging = false) {
		// Код метода
	}

	public function listElements($path = "", $template = "default", $per_page = false, $ignore_paging = false) {
		if(!$per_page) $per_page = $this->per_page;
		$per_page = intval($per_page);

		//
		list($template_block, $template_block_empty, $template_line, $template_archive) = def_module::loadTemplates("library/".$template, "lastlist_block", "lastlist_block_empty", "lastlist_item", "lastlist_archive");
		$curr_page = (int) getRequest('p');
		if($ignore_paging) $curr_page = 0;


		$parent_id = $this->analyzeRequiredPath($path);

		if ($parent_id === false && $path != KEYWORD_GRAB_ALL) {
			throw new publicException(getLabel('error-page-does-not-exist', null, $path));
		}

		$this->loadElements($parent_id);
		
		$library = new selector('pages');
		$library->types('hierarchy-type')->name('library', 'item_element');
		if ($path != KEYWORD_GRAB_ALL) {
			if (is_array($parent_id)) {
				foreach ($parent_id as $parent) {
					$library->where('hierarchy')->page($parent)->childs(1);
				}
			} else {
				$library->where('hierarchy')->page($parent_id)->childs(1);
			}
		}
		
		selectorHelper::detectFilters($library);
		$library->option('load-all-props')->value(true);
		$library->limit($curr_page * $per_page, $per_page);

		$result = $library->result();
		$total = $library->length();

		$umiLinksHelper = $this->umiLinksHelper;
		$umiHierarchy = umiHierarchy::getInstance();

		if(($sz = sizeof($result)) > 0) {
			$block_arr = Array();
			$lines = Array();
			foreach ($result as $element) {
				if (!$element instanceof umiHierarchyElement) {
					continue;
				}
				$line_arr = Array();
				$element_id = $element->getId();

				$line_arr['attribute:id'] = $element_id;
				$line_arr['node:name'] = $element->getName();
				$line_arr['attribute:link'] = $umiLinksHelper->getLinkByParts($element);
				$line_arr['xlink:href'] = "upage://" . $element_id;
				$line_arr['void:header'] = $lines_arr['name'] = $element->getName();

				$lent_name = "";
				$lent_link = "";
				$lent_id = $element->getParentId();

				if ($lent_element = $umiHierarchy->getElement($lent_id)) {
					$lent_name = $lent_element->getName();
					$lent_link = $umiLinksHelper->getLinkByParts($lent_element);
				}

				$line_arr['attribute:lent_id'] = $lent_id;
				$line_arr['attribute:lent_name'] = $lent_name;
				$line_arr['attribute:lent_link'] = $lent_link;

				$lines[] = self::parseTemplate($template_line, $line_arr, $element_id);
				$this->pushEditable("news", "item", $element_id);
				umiHierarchy::getInstance()->unloadElement($element_id);
			}

			if (is_array($parent_id)) {
				list($parent_id) = $parent_id;
			}

			$block_arr['subnodes:items'] = $block_arr['void:lines'] = $lines;
			$block_arr['archive'] = ($total > 0) ? $template_archive : "";
			$parent = $umiHierarchy->getElement($parent_id);
			if ($parent instanceof umiHierarchyElement) {
				$block_arr['archive_link'] = $umiLinksHelper->getLinkByParts($parent);
			}
			$block_arr['total'] = $total;
			$block_arr['per_page'] = $per_page;
			$block_arr['category_id'] = $parent_id;

			return $this->parseTemplate($template_block, $block_arr, $parent_id);
		} else {
			return $template_block_empty;
		}
	}
	

	public function config() {
		return __library::config();
	}

	public function getEditLink($element_id, $element_type) {
		switch($element_type) {
			case "groupelements": {
				$link_add = $this->pre_lang . "/admin/library/add/{$element_id}/item_element/";
				$link_edit = $this->pre_lang . "/admin/library/edit/{$element_id}/";

				return Array($link_add, $link_edit);
				break;
			}

			case "item_element": {
				$link_edit = $this->pre_lang . "/admin/library/edit/{$element_id}/";

				return Array(false, $link_edit);
				break;
			}

			default: {
				return false;
			}
		}	
			
		
	}

};
?>