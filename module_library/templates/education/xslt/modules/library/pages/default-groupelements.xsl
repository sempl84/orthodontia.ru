<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<xsl:stylesheet	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'library']">
		<!--Breadcrumbs -->
		<xsl:apply-templates select="document('udata://core/navibar/')/udata" />
		
		<!--Content -->
	    <div class="container-fluid library-page">
	        <div class="row">
	            <div class="col-xs-12">
	                <h2><i class="fa fa-users" aria-hidden="true"></i><xsl:value-of select="@header" disable-output-escaping="yes" /></h2>
	            </div>
	            <div class="content-col col-lg-9 col-md-8 col-sm-12 col-xs-12">
	                <xsl:apply-templates select="document('udata://library/listElements/?extProps=photo,short_desrc,full_desrc')/udata" />
	            </div>
	            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs right-pnl">
	                <!-- TODO -->
	                <div class="right-nav" role="navigation">
	                    <ul>
	                        <li><a href="/events/">Все мероприятия</a></li>
	                        <li class="active"><a href="/library/">Спикеры</a></li>
	                        <li><a href="/klinicheskie-ofis-kursy/">Клинические офис-курсы</a></li>
	                        <li><a href="#">Фотоотчеты</a></li>
	                        <li ><a href="/otzyvy/">Отзывы</a></li>
	                    </ul>
	                </div>
	                
	                <xsl:apply-templates select="." mode="right_col" />
	                
	            </div>
	        </div>
	    </div>
	</xsl:template>
	
	<xsl:template match="udata[@module='library' and @method='listElements']">
		Ничего не найдено
	</xsl:template>
	<xsl:template match="udata[@module='library' and @method='listElements' and items/item]">
		<ul class="items">
            <xsl:apply-templates select="items/item" />
        </ul>
        <xsl:apply-templates select="total" />
	</xsl:template>
	
	<xsl:template match="udata[@module='library' and @method='listElements']/items/item" >	
		<li class="item">
            <div class="white-pnl">
                <div class="left">
                    <a href="{@link}">
                    	<xsl:call-template name="makeThumbnailFull_ByPath">
							<xsl:with-param name="source" select=".//property[@name='photo']/value" />
							<xsl:with-param name="width" select="80" />
							<xsl:with-param name="height" select="80" />
							
							<xsl:with-param name="empty">&empty-photo-speaker;</xsl:with-param>
							<xsl:with-param name="element-id" select="$document-page-id" />
							<xsl:with-param name="field-name" select="'photo'" />
							
							<xsl:with-param name="alt" select="text()" />
							<xsl:with-param name="class" select="'img-responsive'" />
						</xsl:call-template>
                    </a>
                </div>
                <div class="right">
                    <h4><a href="{@link}"><xsl:value-of select="text()" disable-output-escaping="yes" /></a></h4>
                    <p umi:element-id="{@id}" umi:field-name="short_desrc" umi:empty="&empty-page-content;">
						<xsl:value-of select=".//property[@name = 'short_desrc']/value" disable-output-escaping="yes" />
					</p>
                </div>
                <div class="about">
                	<div umi:element-id="{@id}" umi:field-name="full_desrc" umi:empty="&empty-page-content;">
						<xsl:value-of select=".//property[@name = 'full_desrc']/value" disable-output-escaping="yes" />
                	</div>
                	<!-- TODO add dynamic link -->
                    <a class="link-right-arrow" href="{@link}">Мероприятия спикера <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </li>
	</xsl:template>

</xsl:stylesheet>