<?php
header("Content-type: text/html; charset=utf-8");
include "standalone.php";

//$orderId = isset($_REQUEST['id']) ? $_REQUEST['id'] : false;
$eventPid = isset($_REQUEST['id']) ? $_REQUEST['id'] : false;

//if(!$orderId){
if(!$eventPid){
    echo 'Недостаточно данных.';
    die();
}
$permissions = permissionsCollection::getInstance();
$objects = umiObjectsCollection::getInstance();
$hierarchy = umiHierarchy::getInstance();

//$eventPid = false;
//$order = $objects->getObject($orderId);
//if($order instanceof umiObject) {
    //$eventPid = $order->page_id;

    $element = $hierarchy->getElement($eventPid);
    if(!$element instanceof umiHierarchyElement) {
        echo "Не удалось получить мероприятие.";
        die();
    }

    $bg_file = $element->getValue('cert_bg');
    if ($bg_file) {
        $bg_path = $bg_file->getFilePath(true);
    }

//} else {
//    echo "Не удалось получить объект.";
//    die();
//}

//auto_send_cert_or_file
//cert_bg
//cert_template
//file_for_email
?>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Шаблон сертификата</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
          integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- <link rel="stylesheet" href="https://code.jquery.com/resources/demos/style.css"> -->
    <style>
        html,body{
            width: 100%;
            height: 100%;
            font-family: "DejaVu Sans";
        }

        .drop {
            position: absolute;
            top: 0;
            left: 0;
            width: 1121px;
            height: 794px;
            background:url('<?= $bg_path ?>') no-repeat center center;
            background-size: contain;
        }



        .create_template {
            position: absolute;
            top: 10px;
            left: 1130px;
            width: auto;
            height: 100px;
        }
        .drag {
            position: absolute;
            top: 60px;
            left: 1130px;
            width: 150px;
            height: 100px;
        }
        .drag_placeholder {
            font-family:Arial, Helvetica, sans-serif;
            border: 1px solid black;
            background-color0: rgba(0,0,0,0.2);
            color#333;
            z-index: 1;
        }

        .drag_param{
            font-size:12px;
            font-weight:400;
            position: relative;
            padding:0 3px;
            border: 1px solid black;
            background-color: rgba(255,255,255,0.9);

        }
        .drag_param > div{
            margin:3px 0
        }
        .drag_param_title{
            display:inline-block;
            width:35px;
        }

        .font_size div{
            display:inline-block;
            font-size:16px;
            cursor: pointer;
            font-weight:bold;
            line-height: 0;
            background-color: red;
            padding: 3px 4px 2px;
            color: #fff;
        }

        .font_align div{
            display:inline-block;
            font-size:16px;
            cursor: pointer;
            font-weight:bold;
            line-height: 0;
            background-color: red;
            padding: 3px 4px 2px;
            color: #fff;
        }

        .font select,
        .color select{
            width: 95px;
        }


    </style>




</head>
<body>
<div class="drop">
</div>
<a href="#" class="create_template">Сохранить текущий шаблон</a>

<div id="fio" class="drag">

    <div class="drag_placeholder" data-content='{order_fname} {order_father_name} {order_lname}'>Иван Иванович Иванов</div>
    <div class="drag_param" >
        <div class="font_size">
            <span class="drag_param_title">Size:</span>
            <div class="more"><i class="fas fa-plus"></i></div>
            <div class="less"><i class="fas fa-minus"></i></div>
        </div>
        <div class="font_align">
            <span class="drag_param_title">Align:</span>
            <div class="left"><i class="fas fa-align-left"></i></div>
            <div class="center"><i class="fas fa-align-center"></i></div>
            <div class="right"><i class="fas fa-align-right"></i></div>
        </div>
        <div class="font">
            <span class="drag_param_title">Font:</span>
            <select name="fontfamily" >
                <option value="Arial">
                    Arial
                </option>
                <option value="Arial Black">
                    Arial Black
                </option>
                <option value="Verdana">
                    Verdana
                </option>
                <option value="Calibri">
                    Calibri
                </option>
                <option value="Calibri Light">
                    Calibri Light
                </option>
                <option value="Open Sans">
                    Open Sans
                </option>
            </select>
        </div>
        <div class="color">
            <span class="drag_param_title">Color:</span>
            <select name="color_hex">
                <option value="333333">
                    Темно-серый
                </option>
                <option value="666666">
                    Cерый
                </option>
                <option value="999999">
                    Светло-серый
                </option>
                <option value="CCCCCC">
                    Очень светло-серый
                </option>
                <option value="003c79">
                    Синий
                </option>
                <option value="FF5F30">
                    Оранжевый
                </option>
                <option value="FFFFFF">
                    Белый
                </option>
                <option value="000000">
                    Черный
                </option>
            </select>
        </div>
    </div>

</div>

<textarea id="template_result"></textarea>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    $(function () {
        var coordinates = function(element) {
            //element = $(element);
            console.log(element);
            var top = element.position().top;
            var left = element.position().left;
            console.log(top);
            $('#result').text('X: ' + left + ' ' + 'Y: ' + top);
        }

        $('.drag_placeholder').resizable({
            stop: function(event, ui) {
                var width = ui.size.width;
                var height = ui.size.height;
                $('#size').text('width: ' + width + ' ' + 'height: ' + height);
            }
        });
        $('.drag').draggable({
            stop: function(event, ui) {
                var element = $(ui.helper);
                var top = element.position().top;
                var left = element.position().left;
                //console.log(top);
                $('#position').text('X: ' + left + ' ' + 'Y: ' + top);

                var width = element.find('.drag_placeholder').width();
                var height = element.find('.drag_placeholder').height();
                $('#size').text('width: ' + width + ' ' + 'height: ' + height);
            }

        });
        $('.drop').droppable({
            tolerance: 'pointer',
            hoverClass: 'drop-hover'
        });

        // изменение размера шрифта
        $('.font_size div').click(function(){
            var drag_placeholder = $(this).parents('.drag').find('.drag_placeholder'),
                curr_font_size = parseInt(drag_placeholder.css('font-size'));
            console.log(curr_font_size);
            if($(this).hasClass('more')){

                drag_placeholder.css('font-size',curr_font_size + 2 + "px")
            }else{
                drag_placeholder.css('font-size',curr_font_size - 2 + "px")
            }
        });

        // изменение выравнивания шрифта
        $('.font_align div').click(function(){
            var drag_placeholder = $(this).parents('.drag').find('.drag_placeholder'),
                align_class = $(this).attr('class');
            drag_placeholder.css('text-align',align_class);
        });

        // изменение семейства шрифта
        $('.font select').on('change', function() {
            var drag_placeholder = $(this).parents('.drag').find('.drag_placeholder'),
                font_family = this.value;
            drag_placeholder.css('font-family',font_family);
        });

        // изменение цвета текста
        $('.color select').on('change', function() {
            var drag_placeholder = $(this).parents('.drag').find('.drag_placeholder'),
                hex = '#' + this.value;
            drag_placeholder.css('color',hex);
        });

        // создание шаблона
        $('.create_template').click(function(){
            // create blank html TODO in PHP

            // copy element into body
            var bg_css = $('.drop').css('background');
            console.log(bg_css);
            var template = '',
                template_drop = $('<div>')
                                .addClass('drop')
                                .css('background-image',$('.drop').css('background-image')),
                template = $('<div>').append(template_drop).html();

            // перебор всех  перетаскиваемых блоков
            $('.drag').each(function(){
               var _this = $(this),
                   _this_placeholder = _this.find('.drag_placeholder'),
                    css_arr = {
                       'left': _this.css('left'),
                       'top': _this.css('top'),
                       'width': _this_placeholder.css('width'),
                       'height': _this_placeholder.css('height'),
                       'text-align': _this_placeholder.css('text-align'),
                       'font-size': _this_placeholder.css('font-size'),
                        'color': _this_placeholder.css('color'),
                        'font-family': _this_placeholder.css('font-family')
                    },
                    template_drag = $('<div>')
                        .addClass('drag')
                        .css(css_arr)
                        .html(_this_placeholder.data('content'));
               template = template + $('<div>').append(template_drag).html();
            });

            //console.log(template_drop);
            console.log(template);
            $('#template_result').val(template);
            console.log($('#template_result').val());

            // save cert_template
            $.ajax({
                url: "/udata/emarket/cert_template_save/.json",
                dataType: "json",
                type: "POST",
                data: {'id': <?= $eventPid ?>, 'template': $('#template_result').val()},
                success: function (data) {
                    alert(data.status)
                }
            });

            return false;
        });

    });
</script>
</body>
</html>
