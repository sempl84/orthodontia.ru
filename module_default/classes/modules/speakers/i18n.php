<?php
 
$i18n = Array(
"label-add-list" => "Добавить группу",
"label-add-item" => "Добавить страницу",
 
"header-speakers-lists" => "Группы и страницы",
"header-speakers-config" => "Настройки модуля",
"header-speakers-add" => "Добавление",
"header-speakers-edit" => "Редактирование",
 
"header-speakers-add-groupelements"   => "Добавление группы",
"header-speakers-add-item_element"     => "Добавление страницы",
 
"header-speakers-edit-groupelements"   => "Редактирование группы",
"header-speakers-edit-item_element"     => "Редактирование страницы",
 
"header-speakers-activity" => "Изменение активности",
 
'perms-speakers-view' => 'Просмотр страниц',
'perms-speakers-lists' => 'Управление страницами',

'module-speakers' => 'Спикеры',
 
);
 
?>