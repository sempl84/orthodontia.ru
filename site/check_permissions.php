<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2018, Evgenii Ioffe
 */

session_start();

chdir(dirname(dirname(__FILE__)));
require_once 'standalone.php';

if(!defined('SKIP_CHECK_AUTH')) {
    $umiAuth = new umiAuth();
    /* @var $umiAuth umiAuth */
    $umiAuth->tryPreAuth();
    
    outputBuffer::current('HTTPOutputBuffer');
    
    if(!permissionsCollection::getInstance()->isSv()) {
        echo 'Не хватает прав доступа';
        exit;
    }
}