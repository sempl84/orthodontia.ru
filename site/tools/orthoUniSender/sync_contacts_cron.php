<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

define('SKIP_CHECK_AUTH', true);

require_once dirname(dirname(__DIR__)) . '/check_permissions.php';

$class = new OrthoUniSendersSyncContacts();
$message = '';
if (isset($_REQUEST['init'])) {
    try {
        if(!class_exists('orthoUniSender')) {
            throw new publicException('Не найден модуль orthoUniSender');
        }
        
        $registry = regedit::getInstance();
        
        $emailListId = $registry->getVal(orthoUniSender::registry_param_contacts_list_id);
        if(!$emailListId) {
            throw new publicException('Не выбран список контактов для импорта');
        }
        
        $testEmail = trim($registry->getVal(orthoUniSender::registry_param_test_email));
        if($testEmail) {
            $testEmail = explode(',', $testEmail);
        }
        
        $class->init($emailListId, $testEmail);
    
        $message .= implode('<br />', $class->getLog());
    } catch (Exception $e) {
        $message = 'Ошибка: ' . $e->getMessage();
    }
} elseif($class->isActive()) {
    $class->process();
    
    $message .= implode('<br />', $class->getLog());
} else {
    $bRefresh = false;
}
?>
<html>
<head>
    <title>Синхронизация контактов с UniSender</title>
</head>
<body>
<?php if($message):?>
    <?php echo $message; ?>
<?php endif;?>
</body>
</html>