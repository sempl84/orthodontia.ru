<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2020, Evgenii Ioffe
 */

define('SKIP_CHECK_AUTH', true);

require_once dirname(dirname(__DIR__)) . '/check_permissions.php';

$class = new CatalogIndexFilters();
if(!$class->isActive()) {
    $class->init();
}
$class->process();
$message = implode('<br />', $class->getLog());
?>
<html>
    <head>
    </head>
    <body>
        <?php echo $message;?>
    </body>
</html>