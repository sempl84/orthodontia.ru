<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2020, Evgenii Ioffe
 */

require_once dirname(dirname(__DIR__)) . '/check_permissions.php';

$class = new CatalogIndexFilters();
$class->setCheckLock(false);
if(isset($_REQUEST['force'])) {
    $class->setCheckLock(false);
}
$bRefresh = false;
if(isset($_REQUEST['init'])) {
    $bRefresh = true;
    $class->init();
    $message = 'Индексация запущена';
} elseif($class->isActive()) {
    $class->process();
    if(!$class->isDone()) {
        $bRefresh = true;
        $message = 'Индексация данных <br />' . implode('<br />', $class->getLog());
    } else {
        $message = 'Индексация завершена';
    }
}

$file = str_replace($_SERVER['DOCUMENT_ROOT'], '', __FILE__);
?>
<html>
    <head>
        <?php if($bRefresh):?>
            <script type="text/javascript">setTimeout(function() {window.location.href = '<?php echo $file?>?t=<?php echo time();?>'}, 1000);</script>
        <?php endif;?>
    </head>
    <body>
        <?php if($message):?>
            <p><?php echo $message;?></p>
        <?php else:?>
            <a href="<?php echo $file;?>?init=true">Запустить индексацию</a>
        <?php endif;?>
    </body>
</html>