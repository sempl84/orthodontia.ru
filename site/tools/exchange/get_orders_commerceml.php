<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2023, Evgenii Ioffe
 */

require_once dirname(dirname(__DIR__)) . '/check_permissions.php';

$orderId = intval(getRequest('order_id'));
$error = '';
$xml = '';

if($orderId > 0) {
    try {
        $order = umiObjectsCollection::getInstance()->getObject($orderId);
        if(!$order instanceof umiObject) {
            throw new publicException('Не найден заказ ' . $orderId);
        }

        $exporter = new xmlExporter('CommerceML2');
        $exporter->addObjects([$order]);
        $exporter->setIgnoreRelations();
        $result = $exporter->execute();
        $umiDump = $result->saveXML();

        $xslFile = './xsl/export/ordersCommerceML.xsl';
        if (!is_file($xslFile)) {
            throw new publicException("Не найден файл " . $xslFile);
        }

        $document = new DOMDocument("1.0", "utf-8");
        $document->formatOutput = XML_FORMAT_OUTPUT;
        $document->loadXML($umiDump);
        $templater = umiTemplater::create('XSLT', $xslFile);
        $xml = $templater->parse($document);
    } catch (Exception $e) {
        $error = $e->getMessage();
    }
}
?>
<html>
<head>
    <meta charset="UTF-8" />
    <title>Получить заказ</title>
</head>
<body>
    <form method="GET" action="<?php echo str_replace(CURRENT_WORKING_DIR, '', __FILE__);?>">
        <p>
            <label for="order_id">Id заказа</label>
            <input type="text" id="order_id" name="order_id" value="<?php echo $orderId;?>" />
        </p>
        <p>
            <input type="submit" value="Получить" />
        </p>
    </form>
    <?php if($error):?>
        <p style="color: red;"><?php echo $error;?></p>
    <?php elseif($xml):?>
        <textarea rows="30" cols="200" disabled><?php echo $xml;?></textarea>
    <?php endif;?>
</body>
</html>
