<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class Migration396OrthoUniSender
{
    private $installer;
    
    public function __construct()
    {
        require_once CURRENT_WORKING_DIR . '/vendor/UmiSpec/Installer/Installer.php';
        
        $this->installer = new UmiSpecInstaller();
    }
    
    public function execute()
    {
        $this->createUserObjectTypeFields();
    }
    
    private function createUserObjectTypeFields()
    {
        $objectType = $this->installer->getObjectTypeByHierarchyTypeName(SiteUsersUserModel::module, SiteUsersUserModel::method);
        if(!$objectType instanceof umiObjectType) {
            throw new publicException('Не найден тип данных ' . SiteUsersUserModel::method);
        }
        
        $group = new UmiSpecInstallerGroup(SiteUsersUserModel::group_uni_sender, 'Интеграция с UniSender');
        $field = new UmiSpecInstallerField(SiteUsersUserModel::field_uni_sender_exported, 'Пользователь экспортирован в UniSender', $this->installer->getFieldTypeId('boolean'));
        $field->setVisible(false);
        $group->addField($field);
        $this->installer->createObjectTypeGroups(array($group), $objectType);
    }
}

require_once dirname(__DIR__) . '/check_permissions.php';

$migration = new Migration396OrthoUniSender();
$migration->execute();

echo 'Готово';
exit;