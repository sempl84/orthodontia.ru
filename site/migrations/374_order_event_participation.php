<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class Migration374OrderEventParticipation
{
    private $installer;
    
    public function __construct()
    {
        require_once CURRENT_WORKING_DIR . '/vendor/UmiSpec/Installer/Installer.php';
        
        $this->installer = new UmiSpecInstaller();
    }
    
    public function execute()
    {
        $this->createOrderObjectTypeFields();
    }
    
    private function createOrderObjectTypeFields()
    {
        $objectType = $this->installer->getObjectTypeByHierarchyTypeName(SiteEmarketOrderModel::module, SiteEmarketOrderModel::method);
        if(!$objectType instanceof umiObjectType) {
            throw new publicException('Не найден тип данных ' . SiteEmarketOrderModel::method);
        }
        
        $group = new UmiSpecInstallerGroup(SiteEmarketOrderModel::group_event);
        $field = new UmiSpecInstallerField(SiteEmarketOrderModel::field_event_participation, 'Присутствие на семинаре', $this->installer->getFieldTypeId('boolean'));
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
}

require_once dirname(__DIR__) . '/check_permissions.php';

$migration = new Migration374OrderEventParticipation();
$migration->execute();

echo 'Готово';
exit;