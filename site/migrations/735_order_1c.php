<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2023, Evgenii Ioffe
 */

class SiteMigration735Order1c
{
    /**
     * @var UmiSpecInstaller
     */
    private $installer;
    
    public function __construct()
    {
        require_once CURRENT_WORKING_DIR . '/vendor/UmiSpec/Installer/Installer.php';
        $this->installer = new UmiSpecInstaller();
    }
    
    /**
     * @return void
     * @throws publicException
     */
    public function execute()
    {
        $this->createOrderObjectTypeFields();
    }
    
    private function createOrderObjectTypeFields()
    {
        $objectType = $this->installer->getObjectTypeByHierarchyTypeName(SiteEmarketOrderModel::module, SiteEmarketOrderModel::method);
        if(!$objectType instanceof umiObjectType) {
            throw new publicException('Не найден тип данных ' . SiteUsersUserModel::method);
        }

        $group = new UmiSpecInstallerGroup(SiteEmarketOrderModel::group_1c, 'Обмен с 1с');
        $field = new UmiSpecInstallerField(SiteEmarketOrderModel::field_1c_log, 'Лог обмена с 1с', $this->installer->getFieldTypeId('text'));
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
}

require_once dirname(__DIR__) . '/check_permissions.php';

$migration = new SiteMigration735Order1c();
$migration->execute();

echo 'Готово';
exit;