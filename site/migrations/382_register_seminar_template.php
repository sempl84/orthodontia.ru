<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class Migration382RegisterSeminarTemplate
{
    private $installer;
    
    public function __construct()
    {
        require_once CURRENT_WORKING_DIR . '/vendor/UmiSpec/Installer/Installer.php';
        
        $this->installer = new UmiSpecInstaller();
    }
    
    public function execute()
    {
        $this->createSettingsObjectTypeFields();
        
        $this->setSettingsValues();
    }
    
    private function createSettingsObjectTypeFields()
    {
        $objectType = umiObjectTypesCollection::getInstance()->getType(SiteContentPageSettingsModel::object_type_id);
        if (!$objectType instanceof umiObjectType) {
            throw new publicException('Не найден тип данных ' . SiteContentPageSettingsModel::object_type_id);
        }
        
        $group = new UmiSpecInstallerGroup(SiteContentPageSettingsModel::group_mail_templates);
        $field = new UmiSpecInstallerField(SiteContentPageSettingsModel::field_mail_template_register_seminar_client, 'Письмо клиенту о регистрации на партнерский семинар', $this->installer->getFieldTypeId('wysiwyg'));
        $field->setInFilter(false);
        $field->setInSearch(false);
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
    
    private function setSettingsValues()
    {
        $template = <<<TEMPLATE
<p>Регистрация на семинар прошла успешно, с вами свяжутся менеджеры Ormco.</p><p>Семинар: %event_name% <br /> Город: %event_city% <br /> Дата: %event_date%</p>
TEMPLATE;
        
        $object = SiteContentPageSettingsModel::getSettingsObject();
        
        umiObjectProperty::$IGNORE_FILTER_INPUT_STRING = true;
        $object->setValue(SiteContentPageSettingsModel::field_mail_template_register_seminar_client, $template);
        $object->commit();
        umiObjectProperty::$IGNORE_FILTER_INPUT_STRING = false;
    }
}

require_once dirname(__DIR__) . '/check_permissions.php';

$migration = new Migration382RegisterSeminarTemplate();
$migration->execute();

echo 'Готово';
exit;