<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class Migration393ObjectArchive
{
    private $installer;
    
    public function __construct()
    {
        require_once CURRENT_WORKING_DIR . '/vendor/UmiSpec/Installer/Installer.php';
        
        $this->installer = new UmiSpecInstaller();
    }
    
    public function execute()
    {
        $this->createProductObjectTypeFields();
    }
    
    private function createProductObjectTypeFields()
    {
        $objectType = $this->installer->getObjectTypeByHierarchyTypeName(SiteCatalogObjectModel::module, SiteCatalogObjectModel::method);
        if(!$objectType instanceof umiObjectType) {
            throw new publicException('Не найден тип данных ' . SiteCatalogObjectModel::method);
        }
        
        $group = new UmiSpecInstallerGroup(SiteCatalogObjectModel::group_system, 'Системные свойства');
        $field = new UmiSpecInstallerField(SiteCatalogObjectModel::field_system_log, 'Лог', $this->installer->getFieldTypeId('text'));
        $field->setInFilter(false);
        $field->setInSearch(false);
        $field->setVisible(false);
        $group->addField($field);
        $this->installer->createObjectTypeGroups(array($group), $objectType);
    }
}

require_once dirname(__DIR__) . '/check_permissions.php';

$migration = new Migration393ObjectArchive();
$migration->execute();

echo 'Готово';
exit;