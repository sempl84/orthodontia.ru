<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2024, Evgenii Ioffe
 */

class SiteMigration800OrmcoStarsNotify
{
    /**
     * @var UmiSpecInstaller
     */
    private $installer;
    
    public function __construct()
    {
        require_once CURRENT_WORKING_DIR . '/vendor/UmiSpec/Installer/Installer.php';
        $this->installer = new UmiSpecInstaller();
    }
    
    /**
     * @return void
     * @throws publicException
     */
    public function execute()
    {
        $this->updateUserObjectTypeFields();
    }
    
    private function updateUserObjectTypeFields()
    {
        $objectType = $this->installer->getObjectTypeByHierarchyTypeName(SiteUsersUserModel::module, SiteUsersUserModel::method);
        if(!$objectType instanceof umiObjectType) {
            throw new publicException('Не найден тип данных ' . SiteUsersUserModel::method);
        }

        $group = new UmiSpecInstallerGroup(SiteUsersUserModel::group_ormco_stars);
        $field = new UmiSpecInstallerField(SiteUsersUserModel::field_ormco_stars_notify_hash, 'Хеш данных, отправленных при уведомлении', $this->installer->getFieldTypeId('string'));
        $field->setVisible(false);
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
}

require_once dirname(__DIR__) . '/check_permissions.php';

$migration = new SiteMigration800OrmcoStarsNotify();
$migration->execute();

echo 'Готово';
exit;