<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2022, Evgenii Ioffe
 */

class SiteMigration501Dadata
{
    /**
     * @var UmiSpecInstaller
     */
    private $installer;
    
    public function __construct()
    {
        require_once CURRENT_WORKING_DIR . '/vendor/UmiSpec/Installer/Installer.php';
        $this->installer = new UmiSpecInstaller();
    }
    
    /**
     * @return void
     * @throws publicException
     */
    public function execute()
    {
        $this->updateUserObjectTypeFields();
        
        $this->installOrthoSiteDataModule();
    }
    
    private function updateUserObjectTypeFields()
    {
        $objectType = $this->installer->getObjectTypeByHierarchyTypeName(SiteUsersUserModel::module, SiteUsersUserModel::method);
        if(!$objectType instanceof umiObjectType) {
            throw new publicException('Не найден тип данных ' . SiteUsersUserModel::method);
        }
        
        $arNotRequired = array(
            SiteUsersUserModel::field_region,
            SiteUsersUserModel::field_city
        );
        
        foreach($arNotRequired as $fieldName) {
            $fieldId = $objectType->getFieldId($fieldName);
            if(!$fieldId) {
                continue;
            }
            
            $field = umiFieldsCollection::getInstance()->getField($fieldId);
            if(!$field instanceof umiField) {
                continue;
            }
            
            $field->setIsRequired(false);
            $field->commit();
        }
    }
    
    /**
     * @return void
     * @throws publicException
     */
    private function installOrthoSiteDataModule()
    {
        cmsController::getInstance()->installModule('classes/modules/orthoSiteData/install.php');
        
        permissionsCollection::getInstance()->setModulesPermissions(permissionsCollection::getGuestId(), 'orthoSiteData', 'guest');
        permissionsCollection::getInstance()->setModulesPermissions(334, 'orthoSiteData', 'auth');
    }
}

require_once dirname(__DIR__) . '/check_permissions.php';

$migration = new SiteMigration501Dadata();
$migration->execute();

echo 'Готово';
exit;