<?php

/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2021, Evgenii Ioffe
 */

class SiteMigrationOrmco423Dadata
{
    /**
     * @var UmiSpecInstaller
     */
    private $installer;
    
    public function __construct()
    {
        require_once CURRENT_WORKING_DIR . '/vendor/UmiSpec/Installer/Installer.php';
        $this->installer = new UmiSpecInstaller();
    }
    
    /**
     * @return void
     * @throws publicException
     */
    public function execute()
    {
        $this->createUserObjectTypeFields();
    }
    
    /**
     * @return void
     * @throws publicException
     */
    private function createUserObjectTypeFields()
    {
        $objectType = $this->installer->getObjectTypeByHierarchyTypeName(SiteUsersUserModel::module, SiteUsersUserModel::method);
        if (!$objectType instanceof umiOBjectType) {
            throw new publicException('Не найден тип данных ' . SiteUsersUserModel::method);
        }
        
        $group = new UmiSpecInstallerGroup(SiteUsersUserModel::group_address, 'Адрес');
        $field = new UmiSpecInstallerField(SiteUsersUserModel::field_address_postal_code, 'Индекс', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteUsersUserModel::field_address_country, 'Страна', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteUsersUserModel::field_address_country_iso, 'Код страны ISO', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteUsersUserModel::field_address_region, 'Регион', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteUsersUserModel::field_address_area, 'Район', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteUsersUserModel::field_address_city, 'Город', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteUsersUserModel::field_address_settlement, 'Населённый пункт', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteUsersUserModel::field_address_street, 'Улица', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteUsersUserModel::field_address_house, 'Дом', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteUsersUserModel::field_address_corpus, 'Корпус', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteUsersUserModel::field_address_building, 'Строение', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteUsersUserModel::field_address_liter, 'Литер', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteUsersUserModel::field_address_room, 'Помещение', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteUsersUserModel::field_address_flat, 'Квартира', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteUsersUserModel::field_address_office, 'Офис', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $field = new UmiSpecInstallerField(SiteUsersUserModel::field_address_raw, 'Исходный адрес', $this->installer->getFieldTypeId('string'));
        $group->addField($field);
        $this->installer->createObjectTypeGroup($group, $objectType);
    }
}

require_once dirname(__DIR__) . '/check_permissions.php';

$migration = new SiteMigrationOrmco423Dadata();
$migration->execute();

echo 'Готово';
exit;