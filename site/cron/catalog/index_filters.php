<?php
/**
 * Created by Evgenii Ioffe
 * @author Evgenii Ioffe <ioffe@umispec.ru>
 * @copyright Copyright (c) 2020, Evgenii Ioffe
 */

if(isset($_SERVER['HTTP_HOST'])) {
    echo 'Script should be executed via console' . PHP_EOL;
    exit;
}

define("CRON", "CLI");

require_once dirname(dirname(dirname(__DIR__))) . '/standalone.php';

$buffer = outputBuffer::current('CLIOutputBuffer');

$class = new CatalogIndexFilters();
if(!$class->isActive()) {
    $class->init();
}
$class->process();
$buffer->push(implode(PHP_EOL, $class->getLog()));
$buffer->end();