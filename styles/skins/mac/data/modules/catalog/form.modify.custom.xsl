<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi"
	xmlns:php="http://php.net/xsl"
	>

	<xsl:template match="group[@name='short_descr_group']" mode="form-modify-group-fields" />
	<!-- <xsl:template match="group[//result/data/@type-id=191 and @name='short_descr_group']" mode="form-modify-group-fields" /> -->
</xsl:stylesheet>