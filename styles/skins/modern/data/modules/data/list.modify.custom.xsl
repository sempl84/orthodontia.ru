<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">




    <xsl:template match="/result[@method='guide_items']/data[@type = 'list' and @action = 'modify']">
        <xsl:variable name="utype_info" select="document(concat('utype://', $param0 ))/udata" />
        <div class="location">
            <div class="imgButtonWrapper imgButtonWrapper loc-left">
                <a class="smc-fast-add btn color-blue" href="{$lang-prefix}/admin/data/guide_item_add/{$param0}/"
                   ref="tree-data-guide_items">
                    <xsl:text>&label-guide-item-add;</xsl:text>
                </a>

                <!-- выводить снопки отправки сертификата посетителям, если это справочники дочерние к типу данных "Посетители мероприятия, для получения сертификата" -->
                <xsl:if test="$utype_info/type/@parent-id = 210">
                    <a href="javascript:void(0);" id="updateVisitorsList" class="btn color-blue" onclick="javascript:updateVisitorsList();">Загрузить посетителей</a>
                    <a href="/emarket/send_sert_for_visitors/{$param0}" id="sendCertForVisitorsList" class="btn color-blue">Отправка сертификата</a>
                </xsl:if>
            </div>

            <!-- выводить снопки отправки сертификата посетителям, если это справочники дочерние к типу данных "Посетители мероприятия, для получения сертификата" -->
            <xsl:if test="$utype_info/type/@parent-id = 210">
                <script type="text/javascript">
                    var guide_tid = <xsl:value-of select="$param0" />;
                    <![CDATA[
               var h = '<form method="post" id="import-users-file-form" target="_blank" enctype="multipart/form-data" action="/udata/emarket/updateEventVisitor/'+guide_tid+'" ><label id="label-csv-file">Выберите CSV-файл для импорта:</label><br/><input type="file" name="users-file" id="users-file" size="28"/></form>';

                console.log(guide_tid);
               function updateVisitorsList() {
                   openDialog('', 'Обновление списка посетителей мероприятия', {
                        html: h,
                        confirmText: 'Отправить',
                        cancelButton: true,
                        cancelText: 'Отмена',
                        confirmCallback: function (popupName) {
                          jQuery('#import-users-file-form').submit();

                          closeDialog(popupName);
                          location.reload();
                        }
                   });
               }
              ]]></script>
            </xsl:if>
        </div>


        <xsl:call-template name="ui-smc-table">
            <xsl:with-param name="control-params" select="$param0" />
            <xsl:with-param name="content-type" select="'objects'" />
        </xsl:call-template>
    </xsl:template>
</xsl:stylesheet>
