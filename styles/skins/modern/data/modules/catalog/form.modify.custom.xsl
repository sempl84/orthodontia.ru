<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common/">
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xlink="http://www.w3.org/TR/xlink"
	xmlns:umi="http://www.umi-cms.ru/TR/umi"
	xmlns:php="http://php.net/xsl">

    <xsl:template match="field[(@name = 'discount_ordinator' or @name = 'discount_ordinator_30' or @name = 'discount_doctor' or @name = 'discount_teacher' or @name = 'discount_poo' or @name = 'discount_shoo') and (@type = 'boolean')]" mode="form-modify">
        <div class="col-md-12" for="{generate-id()}" style="min-height:30px;">
            <input type="hidden" name="{@input_name}" value="0" />
            <div class="checkbox">
                <input type="checkbox" name="{@input_name}" value="1" id="{generate-id()}">
                    <xsl:apply-templates select="." mode="required_attr">
                        <xsl:with-param name="old_class" select="'checkbox'"/>
                    </xsl:apply-templates>
                    <xsl:if test=". = '1'">
                        <xsl:attribute name="checked">checked</xsl:attribute>
                    </xsl:if>
                </input>
            </div>
            <span class="label">
                <acronym>
                    <xsl:apply-templates select="." mode="sys-tips" />
                    <xsl:value-of select="@title" />
                    <xsl:text>.&#160;&#160;&#160;&#160;Стоимость мероприятия с учетом скидки: </xsl:text>
                    <xsl:choose>
                        <!-- 30% скидка -->
                        <xsl:when test="@name = 'discount_ordinator_30'">
                            <xsl:value-of select="ceiling(../..//field[@name='price'] * 0.7)" />
                        </xsl:when>
                        <!-- 40% скидка -->
                        <xsl:when test="@name = 'discount_ordinator'">
                            <xsl:value-of select="ceiling(../..//field[@name='price'] * 0.6)" />
                        </xsl:when>
                        <!-- 30% скидка -->
                        <xsl:when test="@name = 'discount_doctor'">
                            <xsl:value-of select="ceiling(../..//field[@name='price'] * 0.7)" />
                        </xsl:when>
                        <!-- 50% скидка -->
                        <xsl:when test="@name = 'discount_teacher'">
                            <xsl:value-of select="ceiling(../..//field[@name='price'] * 0.5)" />
                        </xsl:when>
                        <!-- 50% скидка -->
                        <xsl:when test="@name = 'discount_shoo'">
                            <xsl:value-of select="ceiling(../..//field[@name='price'] * 0.5)" />
                        </xsl:when>
                        <!-- 15% скидка -->
                        <xsl:when test="@name = 'discount_poo'">
                            <xsl:value-of select="ceiling(../..//field[@name='price'] * 0.85)" />
                        </xsl:when>
                    </xsl:choose>
                    <xsl:text> руб.</xsl:text>
                </acronym>
                <xsl:apply-templates select="." mode="required_text" />
            </span>
        </div>
    </xsl:template>

    <xsl:template match="field[(contains(@name,'nazvanie_skidki') or contains(@name,'stoimost_meropriyatiya_s_uchetom_skidki')) and (@type = 'string' or @type = 'int' or @type = 'price' or @type = 'float' or @type = 'counter' or @type = 'link_to_object_type')]" mode="form-modify">
        <xsl:if test="@name = 'nazvanie_skidki_1'">
            <div style="clear: left;" />
            <br/>
        </xsl:if>

        <div class="col-md-3">
            <div class="title-edit">
                <acronym title="{@tip}">
                    <xsl:apply-templates select="." mode="sys-tips" />
                    <xsl:value-of select="@title" />
                </acronym>
                <xsl:apply-templates select="." mode="required_text" />
            </div>
            <span>
                <input class="default" type="text" name="{@input_name}" value="{.}" id="{generate-id()}">
                    <xsl:apply-templates select="@type" mode="number" />
                </input>
            </span>
        </div>
    </xsl:template>

    <xsl:template match="field[contains(@name,'skidka_ogranichena_datoj') and @type = 'boolean']" mode="form-modify">
        <div class="col-md-3" for="{generate-id()}" style="min-height:30px;">
            <div class="title-edit">&#160;</div>
            <input type="hidden" name="{@input_name}" value="0" />
            <div class="checkbox">
                <input type="checkbox" name="{@input_name}" value="1" id="{generate-id()}">
                    <xsl:apply-templates select="." mode="required_attr">
                        <xsl:with-param name="old_class" select="'checkbox'"/>
                    </xsl:apply-templates>
                    <xsl:if test=". = '1'">
                        <xsl:attribute name="checked">checked</xsl:attribute>
                    </xsl:if>
                </input>
            </div>
            <span class="label">
                <acronym>
                    <xsl:apply-templates select="." mode="sys-tips" />
                    <xsl:value-of select="@title" />
                </acronym>
                <xsl:apply-templates select="." mode="required_text" />
            </span>
        </div>
    </xsl:template>

    <xsl:template match="field[contains(@name,'data_okonchaniya_skidki') and @type = 'date']" mode="form-modify">
        <div  class="col-md-3 datePicker">
            <div class="title-edit">
                <acronym title="{@tip}">
                    <xsl:apply-templates select="." mode="sys-tips" />
                    <xsl:value-of select="@title" />
                </acronym>
                <xsl:apply-templates select="." mode="required_text" />
            </div>

            <input class="default" type="text" name="{@input_name}" id="{generate-id()}">
                <xsl:apply-templates select="." mode="required_attr" />
                <xsl:attribute name="value">
                    <xsl:choose>
                        <xsl:when test="@name='publish_time' and string-length(text())=0">
                            <xsl:value-of select="document('udata://system/convertDate/now/Y-m-d%20H:i')/udata" />
                        </xsl:when>
                        <xsl:when test="@name='show_start_date' and @timestamp = 0">
                            <xsl:value-of select="document('udata://system/convertDate/now/Y-m-d%20H:i')/udata" />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="." />
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>
            </input>
        </div>
    </xsl:template>

    <xsl:template match="field[@name = 'promokod_arr' and  @type = 'text']" mode="form-modify">
        <div class="col-md-12 wysiwyg-field promokod_arr_wrap">
            <div class="title-edit">
                <acronym title="{@tip}">
                    <xsl:apply-templates select="." mode="sys-tips" />
                    <xsl:value-of select="@title" />
                </acronym>
                <xsl:apply-templates select="." mode="required_text" />
            </div>
            <span>
                <textarea style="display:none;"  name="{@input_name}" id="promokod_arr">
                    <xsl:apply-templates select="." mode="required_attr">
                        <xsl:with-param name="old_class" select="@type" />
                    </xsl:apply-templates>
                    <xsl:value-of select="." />
                </textarea>
            </span>
            <table class="table">
                <thead>
                    <tr>
                        <td style="text-align:left" colspan="7">
                            <a href="#" class="btn color-blue btn-small promokod_select_all">Выделить все</a>
                            &#160;&#160;
                            <a href="#" class="btn color-blue btn-small promokod_active_selected">Включить выделенные</a>
                            &#160;&#160;
                            <a href="#" class="btn color-blue btn-small promokod_deactive_selected">Выключить выделенные</a>
                            &#160;&#160;
                            <a href="#" class="btn color-orange btn-small promokod_del_selected">Удалить выделенные промокоды</a>
                        </td>
                    </tr>
                    <tr>
                        <th>#</th>
                        <th>Выделить</th>
                        <th>Промокод</th>
                        <th>Скидка (%)</th>
                        <th>Скидка (итоговая стоимость)</th>
                        <th>Текст успешного применения</th>
                        <th>Сколько раз можно использовать</th>
                        <th>Активность</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <!--
                    <tr>
                        <td class="promokod_wrap"><input class="default" type="text" name="promokod1" value="promotest" /></td>
                        <td><input class="default" type="number" name="promokod1_discount" value="10"  min="0" max="100" /></td>
                        <td><input class="default" type="checkbox" name="promokod1_active" value="1" checked="checked" /></td>
                        <td><a href="#" class="promokod_del">X</a></td>
                    </tr>
                    -->
                </tbody>
                <tfoot>
                    <tr>
                        <td style="text-align:left" colspan="7">
                            <a href="#" class="btn color-blue btn-small promokod_add">Добавить промокод</a>
                            &#160;&#160;
                            <a href="#" class="btn color-blue btn-small promokod_show_textarea">Добавить промокод пакетно</a>
                        </td>
                    </tr>
                    <tr id="promokod_text_tr" style="display:none;">
                        <td style="text-align:left" colspan="7" >
                            <textarea id="promokod_text"></textarea>
                            <a href="#" class="btn color-blue btn-small " id="promokod_text_add">Добавить промокоды</a>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>

        <link href="/styles/skins/modern/data/modules/catalog/promokod.css?v=4" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="/styles/skins/modern/data/modules/catalog/new_promokod2.js?v=7" />
        <!-- <script type="text/javascript" src="/styles/skins/modern/data/modules/catalog/promokod.js?v=4" /> -->

        <!-- <script type="text/javascript">
                var tmp_lang_id = <xsl:value-of select="$lang-id"/>;
        </script>
        <script type="text/javascript" src="/styles/skins/modern/data/modules/menu/tree.js" /> -->
    </xsl:template>

    <!-- скрыть лишние поля в спец разделе у спикеров и у програм -->
    <xsl:template match="group[(/result/data/page/@type-id=190 or /result/data/page/@type-id=191) and (@name='short_descr_group' or @name='discount_params')]" mode="form-modify-group-fields" />

    <xsl:template match="field[@name = 'cert_bg' and  @type = 'img_file']" mode="form-modify">
        <xsl:variable name="filemanager-id" select="document(concat('uobject://',/result/@user-id))/udata//property[@name = 'filemanager']/value/item/@id" />
        <xsl:variable name="filemanager">
            <xsl:choose>
                <xsl:when test="not($filemanager-id)">
                    <xsl:text>elfinder</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="document(concat('uobject://',$filemanager-id))/udata//property[@name = 'fm_prefix']/value" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <div class="col-md-6 img_file" id="{generate-id()}" umi:input-name="{@input_name}"
             umi:field-type="{@type}"
             umi:name="{@name}"
             umi:folder="{@destination-folder}"
             umi:file="{@relative-path}"
             umi:folder-hash="{php:function('elfinder_get_hash', string(@destination-folder))}"
             umi:file-hash="{php:function('elfinder_get_hash', string(@relative-path))}"
             umi:lang="{/result/@interface-lang}"
             umi:filemanager="{$filemanager}"

        >
            <label for="imageField_{generate-id()}">
                <div class="title-edit">
                    <acronym title="{@tip}">
                        <xsl:apply-templates select="." mode="sys-tips" />
                        <xsl:value-of select="@title" />
                    </acronym>
                    <xsl:apply-templates select="." mode="required_text" />
                    <xsl:if test="@relative-path">
                        (<a href="/certificates.php?id={//result/data/page/@id}"  target="_blank">создать шаблон сертификата</a>)
                    </xsl:if>

                </div>
                <div class="layout-row-icon" id="imageField_{generate-id()}">

                </div>
            </label>
        </div>
    </xsl:template>

    <!-- назначение справочника для списка участникам, которым можно выслать сертификат -->
    <xsl:template match="field[@name = 'cert_person']" mode="form-modify">
        <xsl:variable name="utype_info" select="document(concat('utype://', text() ))/udata" />
        <div class="col-md-6">

            <div class="title-edit">&#160;</div>
            <span>
                <xsl:choose>
                    <xsl:when test="$utype_info/type/@parent-id = 210">
                        <a class="btn color-blue" href="/admin/data/guide_items/{text()}/" target="_blank">перейти к списку посетителей</a>
                    </xsl:when>
                    <xsl:otherwise>
                        <a class="btn color-blue" href="/udata/emarket/addVisitorsGuide/{/result/data/page/@id}/" target="_blank">создать список посетителей</a>
                    </xsl:otherwise>
                </xsl:choose>
            </span>
        </div>
    </xsl:template>
</xsl:stylesheet>
