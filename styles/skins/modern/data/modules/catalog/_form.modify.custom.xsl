<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common/">
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xlink="http://www.w3.org/TR/xlink"
	xmlns:umi="http://www.umi-cms.ru/TR/umi"
	xmlns:php="http://php.net/xsl">

	<xsl:template match="field[(@name = 'discount_ordinator' or @name = 'discount_poo' or @name = 'discount_shoo') and (@type = 'boolean')]" mode="form-modify">

		<div class="col-md-12" for="{generate-id()}" style="min-height:30px;">
			<input type="hidden" name="{@input_name}" value="0" />
			<div class="checkbox">
				<input type="checkbox" name="{@input_name}" value="1" id="{generate-id()}">
					<xsl:apply-templates select="." mode="required_attr">
						<xsl:with-param name="old_class" select="'checkbox'"/>
					</xsl:apply-templates>
					<xsl:if test=". = '1'">
						<xsl:attribute name="checked">checked</xsl:attribute>
					</xsl:if>
				</input>
			</div>
			<span class="label">
				<acronym>
					<xsl:apply-templates select="." mode="sys-tips" />
					<xsl:value-of select="@title" />
					<xsl:text>.&#160;&#160;&#160;&#160;Стоимость мероприятия с учетом скидки: </xsl:text>
					<xsl:choose>
						<!-- 40% скидка -->
						<xsl:when test="@name = 'discount_ordinator'">
							<xsl:value-of select="ceiling(../..//field[@name='price'] * 0.6)" />
						</xsl:when>
						<!-- 50% скидка -->
						<xsl:when test="@name = 'discount_shoo'">
							<xsl:value-of select="ceiling(../..//field[@name='price'] * 0.5)" />
						</xsl:when>
						<!-- 15% скидка -->
						<xsl:when test="@name = 'discount_poo'">
							<xsl:value-of select="ceiling(../..//field[@name='price'] * 0.85)" />
						</xsl:when>
					</xsl:choose>
					<xsl:text> руб.</xsl:text>
				</acronym>
				<xsl:apply-templates select="." mode="required_text" />
			</span>
		</div>
	</xsl:template>

	<xsl:template match="field[(contains(@name,'nazvanie_skidki') or contains(@name,'stoimost_meropriyatiya_s_uchetom_skidki')) and (@type = 'string' or @type = 'int' or @type = 'price' or @type = 'float' or @type = 'counter' or @type = 'link_to_object_type')]" mode="form-modify">
			<xsl:if test="@name = 'nazvanie_skidki_1'">
				<div style="clear: left;" />
				<br/>
			</xsl:if>

			<div class="col-md-3">
				<div class="title-edit">
					<acronym title="{@tip}">
						<xsl:apply-templates select="." mode="sys-tips" />
						<xsl:value-of select="@title" />
					</acronym>
					<xsl:apply-templates select="." mode="required_text" />
				</div>
				<span>
					<input class="default" type="text" name="{@input_name}" value="{.}" id="{generate-id()}">
						<xsl:apply-templates select="@type" mode="number" />
					</input>
				</span>
			</div>
	</xsl:template>

	<xsl:template match="field[contains(@name,'skidka_ogranichena_datoj') and @type = 'boolean']" mode="form-modify">

			<div class="col-md-3" for="{generate-id()}" style="min-height:30px;">
				<div class="title-edit">&#160;</div>
				<input type="hidden" name="{@input_name}" value="0" />
				<div class="checkbox">
					<input type="checkbox" name="{@input_name}" value="1" id="{generate-id()}">
						<xsl:apply-templates select="." mode="required_attr">
							<xsl:with-param name="old_class" select="'checkbox'"/>
						</xsl:apply-templates>
						<xsl:if test=". = '1'">
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:if>
					</input>
				</div>
				<span class="label">
					<acronym>
						<xsl:apply-templates select="." mode="sys-tips" />
						<xsl:value-of select="@title" />
					</acronym>
					<xsl:apply-templates select="." mode="required_text" />
				</span>
			</div>
	</xsl:template>

	<xsl:template match="field[contains(@name,'data_okonchaniya_skidki') and @type = 'date']" mode="form-modify">

			<div  class="col-md-3 datePicker">
				<div class="title-edit">
					<acronym title="{@tip}">
						<xsl:apply-templates select="." mode="sys-tips" />
						<xsl:value-of select="@title" />
					</acronym>
					<xsl:apply-templates select="." mode="required_text" />
				</div>

                <input class="default" type="text" name="{@input_name}" id="{generate-id()}">
                    <xsl:apply-templates select="." mode="required_attr" />
                    <xsl:attribute name="value">
                        <xsl:choose>
                            <xsl:when test="@name='publish_time' and string-length(text())=0">
                                <xsl:value-of select="document('udata://system/convertDate/now/Y-m-d%20H:i')/udata" />
                            </xsl:when>
                            <xsl:when test="@name='show_start_date' and @timestamp = 0">
                                <xsl:value-of select="document('udata://system/convertDate/now/Y-m-d%20H:i')/udata" />
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="." />
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                </input>
			</div>

	</xsl:template>




</xsl:stylesheet>