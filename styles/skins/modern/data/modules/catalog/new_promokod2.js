var $promokods_wrap = $('.promokod_arr_wrap .table tbody');
// отрисовка промокодов из json строчки
function promokodsDecode(listObj){
	jQuery.each( listObj, function( key, value ) {
		drawItem(key,value.name,value.discount,value.discount_abs, value.text, value.count,value.active);
	});
};

// создание пункта с промокодом
function drawItem(key, name, discount, discount_abs, text, count, active) {
	var is_checked = (active == 1) ? "checked='checked'" : "";
	var text = (typeof text === "undefined") ? "" : text;
	var count = (count  >= 0) ? count : 1;
	var item_tr = jQuery("<tr/>",{
		html: " <td>"+ (key+1)+"</td>\
				<td><input class='default selected_flag' type='checkbox' name='234' value='' /></td>\
				<td class='promokod_wrap'><input class='default promokod_name' type='text' name='promokod"+key+"' value='"+name+"' /></td>\
				<td><input class='default promokod_discount' type='number' name='promokod"+key+"_discount' value='"+discount+"'  min='0' max='100' /></td>\
				<td><input class='default promokod_discount_abs' type='number' name='promokod"+key+"_discount_abs' value='"+discount_abs+"'  min='0'  /></td>\
				<td class='promokod_wrap'><input class='default promokod_text' type='text' name='promokod"+key+"_text' value='"+text+"' /></td>\
				<td><input class='default promokod_count' type='number' name='promokod"+key+"_count' value='"+count+"'  min='0'  /></td>\
				<td><input class='default promokod_active active_flag' type='checkbox' name='promokod"+key+"_active' value='"+active+"' "+is_checked+" /></td>\
				<td><a href='#' class='promokod_del'>X</a></td>"
	}).appendTo($promokods_wrap);
	console.log(item_tr);
	return;
}

// обновление json записи
function updateOutput() {
	var arrData = [];
		
	jQuery('tr',$promokods_wrap).each(function(){
		var strMatchedValue;
		
		strMatchedValue = {
			"name": jQuery('.promokod_name',jQuery(this)).val(),
		    "discount": jQuery('.promokod_discount',jQuery(this)).val(),
		    "discount_abs": jQuery('.promokod_discount_abs',jQuery(this)).val(),
		    "text": jQuery('.promokod_text',jQuery(this)).val(),
		    "count" : jQuery('.promokod_count',jQuery(this)).val(),
		    "active": jQuery('.promokod_active',jQuery(this)).prop('checked') ? 1 : 0,
		};
		arrData.push( strMatchedValue );
		
	});
	jQuery('#promokod_arr').val(JSON.stringify(arrData));
	
	return;
}

function CSVToArray( strData, strDelimiter ){
    strDelimiter = (strDelimiter || ",");
    // Create a regular expression to parse the CSV values.
    var objPattern = new RegExp(
        (
            "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" + // Delimiters.
            "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" + // Quoted fields.
            "([^\"\\" + strDelimiter + "\\r\\n]*))" // Standard fields.
        ),"gi");
    var arrData = [[]];
    var arrMatches = null;

    while (arrMatches = objPattern.exec( strData )){
        // Get the delimiter that was found.
        var strMatchedDelimiter = arrMatches[ 1 ];

        if (strMatchedDelimiter.length && strMatchedDelimiter !== strDelimiter){
            arrData.push( [] );
        }

        var strMatchedValue;

        if (arrMatches[ 2 ]){
            strMatchedValue = arrMatches[ 2 ].replace(new RegExp( "\"\"", "g" ),"\"");
        } else {
            // We found a non-quoted value.
            strMatchedValue = arrMatches[ 3 ];
        }
        // Now that we have our value string, let's add
        // it to the data array.
        arrData[ arrData.length - 1 ].push( strMatchedValue );
    }

    // Return the parsed data.
    return( arrData );
}

jQuery(document).ready(function(){
	// построение структуры меню при загрузке страницы
	var listObj = jQuery.parseJSON(jQuery('#promokod_arr').val());

	if(typeof listObj =='object' && listObj !== null) promokodsDecode(listObj);
	
	// placeholder for textarea
	var placeholder = '[Название промокода];[Размер скидки в процентах];[Стоимость мероприятия с учетом скидки в рублях];[Текст при успешном применении промокода];[Сколько раз можно использовать промокод];[Активность];\npromocode_test;15;45000;"Промокод применен";8;1;';
	$('#promokod_text').attr('value', placeholder);
	$('#promokod_text').focus(function(){
	    if($(this).val() === placeholder){
	        $(this).attr('value', '');
	    }
	});
	$('#promokod_text').blur(function(){
	    if($(this).val() ===''){
	        $(this).attr('value', placeholder);
	    }    
	});
	// \placeholder for textarea
	
	// Обновлять json при редактировании полей	 
	jQuery('.promokod_count,.promokod_name,.promokod_discount,.promokod_discount_abs,.promokod_text,.promokod_active').unbind().live('change',function(){
		updateOutput();
		return false;
	});
	// Обновлять json при редактировании полей	
	jQuery('.promokod_count,.promokod_name,.promokod_discount,.promokod_discount_abs,.promokod_text').unbind().live('keyup',function(){
		updateOutput();
		//return false;
	});
	
	// Удаление промокода
	jQuery('.promokod_del').unbind().live('click',function(){
		jQuery(this).parents('tr:first').remove();
		updateOutput();
		return false;
	});
	
	// Добавление промокода
	jQuery('.promokod_add').unbind().live('click',function(){
		var key = jQuery('tr',$promokods_wrap).length;
		drawItem(key,'',0,1,0);
		updateOutput();
		return false;
	});
	
	// Выделить все промокоды
	jQuery('.promokod_select_all').unbind().live('click',function(){
		var flag = true;
		if(jQuery('.selected_flag:checked').length == jQuery('.selected_flag').length){
			flag = false;
		}
		jQuery('.selected_flag').prop('checked',flag);
		return false;
	});
	
	// Поменять активность у выделенных промокодов
	jQuery('.promokod_active_selected').unbind().live('click',function(){
		jQuery('.selected_flag:checked').each(function(){
			jQuery(this).parents('tr:first').find('.active_flag').prop('checked',true);
		});
		updateOutput();
		return false;
	});
	
	// Поменять активность у выделенных промокодов
	jQuery('.promokod_deactive_selected').unbind().live('click',function(){
		jQuery('.selected_flag:checked').each(function(){
			jQuery(this).parents('tr:first').find('.active_flag').prop('checked',false);
		});
		updateOutput();
		return false;
	});
	
	// Удалить выделенные промокоды
	jQuery('.promokod_del_selected').unbind().live('click',function(){
		jQuery('.selected_flag:checked').each(function(){
			jQuery(this).parents('tr:first').remove();
		});
		updateOutput();
		return false;
	});
	
	// Отображение поля для ввода пачки промокодов
	jQuery('.promokod_show_textarea').unbind().live('click',function(){
		jQuery('#promokod_text_tr').toggle();
		return false;
	});
	
	// Добавление пачки промокодов
	jQuery('#promokod_text_add').unbind().live('click',function(){
		var csv = jQuery('#promokod_text').val(),
			csv_array = CSVToArray(csv,';'),
			key = jQuery('tr',$promokods_wrap).length;

		console.log(csv_array);
		for (i in csv_array) {
			promocode = csv_array[i];
			key = ++key;
			drawItem(key,promocode[0],promocode[1],promocode[2],promocode[3],promocode[4],promocode[5]);
		} 
		
		updateOutput();
		return false;
	});

	
});

/*

111;20;1
2222;50;0
 */

