$(document).ready(function(){
    if($('.users_change_log').length > 0){
        var object_id = $('.users_change_log').data('object-id');
        $.ajax({
            url: '/udata/data/get_users_change_log/?obj_id=' + object_id,
            dataType: 'xml',
            success: function (data) {
                var res = new Array();
                $(data).find('item').each(function(){
                    var row = new Array();
                    row.push($(this).find('created').text());
                    row.push('<b>' + $(this).find('author').text() + '</b>');
                    row.push('' + $(this).find('message').text() + '');
                    res.push('<td>' + row.join('</td><td>') + '</td>');
                });
                if(res.length > 0){
                    $('.users_change_log').html('<div style="border:solid 1px #ccc;background-color:#eee;padding:10px;margin:20px 0;"><table style="width:100%"><tr>' + res.join('</tr><tr>') + '</tr></table></div>');
                }
            }
        });
    }
});