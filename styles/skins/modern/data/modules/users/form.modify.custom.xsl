<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xlink="http://www.w3.org/TR/xlink"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:php="http://php.net/xsl">

    <xsl:template match="field[@name = 'logi_izmenenij_sistemnoe_pole']" mode="form-modify">
        <div class="col-md-12">
            <div class="users_change_log" data-object-id="{//object/@id}" />
            <script src="/styles/skins/modern/data/modules/users/users_change_log.js"></script>
            <style>
                .users_change_log table tr{border-bottom:solid 1px #aaa;}
                .users_change_log table tr:nth-last-child(1){border-bottom:none;}
                .users_change_log table td{text-align:center;border-right:solid 1px #aaa;padding:1px 10px;}
                .users_change_log table td:nth-last-child(1){border-right:none;text-align:left;}
            </style>
        </div>
    </xsl:template>

    <xsl:template match="field[@type = 'file' and @name='order_upload']" mode="form-modify">
        <xsl:variable name="filemanager-id" select="document(concat('uobject://',/result/@user-id))/udata//property[@name = 'filemanager']/value/item/@id" />
        <xsl:variable name="filemanager">
            <xsl:choose>
                <xsl:when test="not($filemanager-id)">
                    <xsl:text>elfinder</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="document(concat('uobject://',$filemanager-id))/udata//property[@name = 'fm_prefix']/value" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <div class="col-md-6 file" id="{generate-id()}" umi:input-name="{@input_name}"
            umi:field-type="{@type}"
            umi:name="{@name}"
            umi:folder="{@destination-folder}"
            umi:file="{@relative-path}"
            umi:folder-hash="{php:function('elfinder_get_hash', string(@destination-folder))}"
            umi:file-hash="{php:function('elfinder_get_hash', string(@relative-path))}"
            umi:lang="{/result/@interface-lang}"
            umi:filemanager="{$filemanager}">
            <label for="fileControlContainer_{generate-id()}">
                <sdiv class="title-edit">
                    <acronym>
                        <xsl:apply-templates select="." mode="sys-tips" />
                        <xsl:value-of select="@title" /> (<a href="{@relative-path}" target="_blank">открыть в новом окне</a>)
                    </acronym>
                    <xsl:apply-templates select="." mode="required_text" />
                </sdiv>
                <span class="layout-row-icon" id="fileControlContainer_{generate-id()}"> </span>
            </label>
        </div>
    </xsl:template>
</xsl:stylesheet>