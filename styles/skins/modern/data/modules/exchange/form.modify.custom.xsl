<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi"
	xmlns:php="http://php.net/xsl">


	<xsl:template match="field[@name= 'landingevent']" mode="form-modify">
			<div class="col-md-6">
				<div class="title-edit">
					<acronym title="{@tip}">
						<xsl:apply-templates select="." mode="sys-tips" />
						<xsl:value-of select="@title" />
					</acronym>
					<xsl:apply-templates select="." mode="required_text" />
				</div>
				<span>
					<select name="{@input_name}" value="{.}" style="
						width: 100%;
    height: 30px;
    display: block;
    padding: 0 0 0 10px;
    border: 1px solid #d9d9d9;
    border-radius: 3px;
    line-height: 30px;
    font-family: 'Segoe UI', Arial, Tahoma;
						">
						<xsl:value-of select="document(concat('udata://emarket/allLandingEvents/',text(),'/'))/udata" disable-output-escaping="yes" />
					</select>
					<!-- <input class="default" type="text" name="{@input_name}" value="{.}" id="{generate-id()}">
						<xsl:apply-templates select="@type" mode="number" />
					</input> -->
				</span>
			</div>
	</xsl:template>

</xsl:stylesheet>