<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common" [
]>


<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xlink="http://www.w3.org/TR/xlink">

	<xsl:template match="/result[@method = 'orders']/data[@type = 'list' and @action = 'view']">
		<div class="location">
			<div class="imgButtonWrapper loc-left">
				<a href="javascript:void(0);" id="delAllCatalog" class="btn color-blue" onclick="javascript:showClearTrashConfirm();">Выгрузка данных по лендингу</a>
				<a href="javascript:void(0);" id="exportFeedbacks" class="btn color-blue" onclick="javascript:exportFeedbacksConfirm();">Выгрузка опросов</a>
			</div>
           <script type="text/javascript"><![CDATA[
				var h = '';

				function showClearTrashConfirm() {
					$('.waiting').show();
					$.ajax({
						type: 'GET',
						url: '/udata/emarket/allLandingEvents/',
						dataType: 'xml',
						success: function (data) {
							$('.waiting').hide();
							h = 'Выберите мероприятие лендинга: <select id="eventid" style="width:250px;">';
							h += $(data).find('udata').text();
							h += '</select>';

							openDialog('', 'Экспорт данных по лендингу', {
								html: h,
								confirmText: 'Экспорт',
								cancelButton: true,
								cancelText: 'Отмена',
								confirmCallback: function (popupName) {
									console.log(jQuery('#eventid').val());
									location.href = '/udata/emarket/exportLandingOrders/' + jQuery('#eventid').val() + '/';
									closeDialog(popupName);
								}
							});
						}
					});
				}

				//выгрузка опросов
				var f = 'Выгрузить результаты опросов?';


				function exportFeedbacksConfirm() {
					openDialog('', 'Экспорт результаты опросов', {
						html: f,
						confirmText: 'Экспорт',
						cancelButton: true,
						cancelText: 'Отмена',
						confirmCallback: function (popupName) {
							location.href = '/udata/emarket/exportFeedbacks/';
							closeDialog(popupName);
						}
					});
				}
			]]></script>

			<a class="btn-action loc-right infoblock-show"><i class="small-ico i-info"></i><xsl:text>&help;</xsl:text></a>
		</div>
		<div class="layout">
		<div class="column">
		<xsl:call-template name="ui-smc-table">
			<xsl:with-param name="content-type">objects</xsl:with-param>
			<xsl:with-param name="control-params">orders</xsl:with-param>
			<xsl:with-param name="domains-show">1</xsl:with-param>
			<xsl:with-param name="hide-csv-import-button">1</xsl:with-param>
			<xsl:with-param name="js-ignore-props-edit">['order_items', 'number', 'customer_id']</xsl:with-param>
		</xsl:call-template>
		</div>
			<div class="column">
				<div  class="infoblock">
					<h3><xsl:text>&label-quick-help;</xsl:text></h3>
					<div class="content" title="{$context-manul-url}">
					</div>
					<div class="infoblock-hide"></div>
				</div>
			</div>
		</div>

		<div class="waiting" style="display: none; position: fixed; left: 0; top: 0; right: 0; bottom: 0; background: rgba(0,0,0, 0.7) url('/templates/education/img/loading.gif') center center no-repeat; z-index: 9999;"></div>
	</xsl:template>

</xsl:stylesheet>