<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common/">
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xlink="http://www.w3.org/TR/xlink">

	<xsl:include href="order-edit.xsl" />

	<!-- Edit discount settings -->
	<xsl:template match="/result[@method = 'discount_add' or @method = 'discount_modify']/data" priority="1">
		<form method="post" action="do/" enctype="multipart/form-data">
			<input type="hidden" name="referer" value="{/result/@referer-uri}"/>
			<input type="hidden" name="domain" value="{$domain-floated}"/>

			<xsl:apply-templates mode="form-modify" />

			<xsl:apply-templates select=".//field[@name = 'discount_type_id']/values/item" />
			<div class="row">
				<div id="buttons_wr" class="col-md-12">
					<xsl:choose>
						<xsl:when test="@method = 'discount_add'">
							<xsl:call-template name="std-form-buttons-add"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="std-form-buttons"/>
						</xsl:otherwise>
					</xsl:choose>
				</div>
			</div>
		</form>
		<script>
			$(document).ready(function (){
				jQuery('input.discount-type-id').bind('click', function () {
				var discountTypeId = jQuery(this).attr('value');

				jQuery('div.discount-params input').attr('disabled', true);
				jQuery('div.discount-params').css('display', 'none');

				jQuery('div.discount-params#' + discountTypeId + ' input').attr('disabled', false);
				jQuery('div.discount-params#' + discountTypeId + '').css('display', '');
				});
			})
		</script>
	</xsl:template>

	<xsl:template match="/result[not(@method = 'discount_edit')]//field[@name = 'is_active']" mode="form-modify" />
	<xsl:template match="field[@name = 'discount_modificator_id']" mode="form-modify" />
	<xsl:template match="field[@name = 'discount_rules_id']" mode="form-modify" />

	<xsl:template match="field[@name = 'discount_type_id']" mode="form-modify">
		<div class="col-md-12">
				<span class="title-edit">
					<acronym title="{@tip}">
						<xsl:apply-templates select="." mode="sys-tips" />
						<xsl:value-of select="@title" />
					</acronym>
				</span>

				<xsl:apply-templates select="values/item" mode="discount-type" />
		</div>
	</xsl:template>

	<xsl:template match="values/item" mode="discount-type">
		<xsl:variable name="description" select="document(concat('uobject://', @id, '.description'))/udata/property" />
		<p>
			<div class="inline" style="padding:5px 0;">
				<input type="radio" class="checkbox discount-type-id" name="{../../@input_name}" value="{@id}">
					<xsl:if test="@selected = 'selected'">
						<xsl:attribute name="checked" select="'checked'" />
					</xsl:if>
				</input>
				<acronym>
					<xsl:value-of select="." />
					<xsl:apply-templates select="$description" mode="desc" />
				</acronym>
			</div>
		</p>
	</xsl:template>

	<xsl:template match="property[@name = 'description']" mode="desc">
		<em>
			<xsl:value-of select="concat(' (', value, ')')" />
		</em>
	</xsl:template>

	<xsl:template match="field[@name = 'discount_type_id']/values/item">
		<div class="panel-settings discount-params" id="{@id}">
			<xsl:if test="not(@selected = 'selected')">
				<xsl:attribute name="style"><xsl:text>display: none;</xsl:text></xsl:attribute>
			</xsl:if>
			<a data-name="{@name}" data-label="{@title}"></a>
			<div class="title">
				<xsl:call-template name="group-tip">
					<xsl:with-param name="param" select="@name" />
				</xsl:call-template>
				<div class="round-toggle"></div>
				<h3>
					<xsl:value-of select="." />
				</h3>
			</div>

			<div class="content">
				<div class="layout">
					<div class="column">
						<div class="row">
							<xsl:apply-templates select="../../../field[@name = 'discount_modificator_id']" mode="modify-modificators">
								<xsl:with-param name="discount-type-id" select="@id" />
							</xsl:apply-templates>

							<xsl:apply-templates select="../../../field[@name = 'discount_rules_id']" mode="modify-rules">
								<xsl:with-param name="discount-type-id" select="@id" />
							</xsl:apply-templates>

						</div>
				</div>
					<div class="column">
						<div  class="infoblock">
							<h3>
								<xsl:text>&type-edit-tip;</xsl:text>
							</h3>
							<div class="content" >
							</div>
							<div class="group-tip-hide"></div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="field[@name = 'discount_modificator_id']" mode="modify-modificators">
		<xsl:param name="discount-type-id" />

		<xsl:variable name="modificators"
			select="document(concat('udata://emarket/getModificators/', $discount-type-id, '/', $param0))/udata" />
		<div class="col-md-6">
			<div class="title-edit">
				<acronym>
					<xsl:apply-templates select="." mode="sys-tips" />
					<xsl:value-of select="@title" />
				</acronym>
			</div>

			<xsl:apply-templates select="$modificators/items/item">
				<xsl:with-param name="input-name" select="@input_name" />
			</xsl:apply-templates>
		</div>
	</xsl:template>

	<xsl:template match="udata[@method = 'getModificators']//item">
		<xsl:param name="input-name" />
		<xsl:variable name="description" select="document(concat('uobject://', @id, '.description'))/udata/property" />
		<xsl:variable name="item-id" select="@id" />

		<p>
			<label class="inline">
				<input type="radio" class="checkbox" name="{$input-name}" value="{@id}">
					<xsl:if test="@selected = 'selected'">
						<xsl:attribute name="checked"><xsl:text>checked</xsl:text></xsl:attribute>
					</xsl:if>
				</input>
				<acronym>
					<xsl:value-of select="@name" />
					<xsl:apply-templates select="$description" mode="desc" />
				</acronym>
			</label>
		</p>
	</xsl:template>


	<xsl:template match="field[@name = 'discount_rules_id']" mode="modify-rules">
		<xsl:param name="discount-type-id" />
		<xsl:variable name="rules"
			select="document(concat('udata://emarket/getRules/', $discount-type-id, '/', $param0))/udata" />

		<div class="col-md-6">
			<div class="title-edit">
				<acronym title="{@tip}">
					<xsl:apply-templates select="." mode="sys-tips" />
					<xsl:value-of select="@title" />
				</acronym>
			</div>

			<xsl:apply-templates select="$rules/items/item">
				<xsl:with-param name="input-name" select="@input_name" />
			</xsl:apply-templates>
		</div>
	</xsl:template>

	<xsl:template match="udata[@method = 'getRules']//item">
		<xsl:param name="input-name" />
		<xsl:variable name="description" select="document(concat('uobject://', @id, '.description'))/udata/property" />

		<p>
			<label class="inline">
				<input type="checkbox" class="checkbox" name="{$input-name}" value="{@id}">
					<xsl:if test="@selected = 'selected'">
						<xsl:attribute name="checked"><xsl:text>checked</xsl:text></xsl:attribute>
					</xsl:if>
				</input>

				<acronym>
					<xsl:value-of select="@name" />
					<xsl:apply-templates select="$description" mode="desc" />
				</acronym>
			</label>
		</p>
	</xsl:template>



	<!-- Edit discount properties -->
	<xsl:template match="/result[@method = 'discount_edit']/data" priority="1">
		<form method="post" action="do/" enctype="multipart/form-data">
			<input type="hidden" name="referer" value="{/result/@referer-uri}"/>
			<input type="hidden" name="domain" value="{$domain-floated}"/>

			<xsl:apply-templates mode="form-modify" />

			<!-- Select modificator and apply data::getEditForm -->
			<xsl:apply-templates select=".//field[@name = 'discount_modificator_id']/values/item" mode="discount-edit" />

			<!-- Select rules and apply data::getEditForm -->
			<xsl:apply-templates select=".//field[@name = 'discount_rules_id']/values/item" mode="discount-edit" />

			<div class="row">
				<xsl:call-template name="std-form-buttons"/>
			</div>

		</form>
	</xsl:template>

	<xsl:template match="field/values/item" mode="discount-edit">
		<xsl:apply-templates select="document(concat('udata://data/getEditForm/', @id))/udata">
			<xsl:with-param name="item-id" select="@id" />
		</xsl:apply-templates>
	</xsl:template>

	<xsl:template match="udata[@method = 'getEditForm']">
		<xsl:param name="item-id" />

		<xsl:apply-templates select="group" mode="form-modify">
			<xsl:with-param name="item-id" select="$item-id" />
		</xsl:apply-templates>
	</xsl:template>

	<xsl:template match="udata[@method = 'getEditForm']/group" mode="form-modify">
		<xsl:param name="item-id" />
		<xsl:variable name="label" select="document(concat('uobject://', $item-id))/udata//property[@name = 'rule_type_id' or @name = 'modificator_type_id']//item/@name" />

		<div class="panel-settings" name="g_{@name}">
			<summary class="group-tip">
				<xsl:text>Тип модификатора скидки</xsl:text>
			</summary>
			<a data-name="{@name}" data-label="{$label}"></a>
			<div class="title">
				<xsl:call-template name="group-tip">
					<xsl:with-param name="param" select="@name" />
				</xsl:call-template>
				<div class="round-toggle"></div>
				<h3>
					<xsl:value-of select="$label" />
				</h3>
			</div>

			<div class="content">
				<div class="layout">
					<div class="column">
						<div class="row">
							<xsl:apply-templates select="field" mode="form-modify" />

						</div>
					</div>
					<div class="column">
						<div  class="infoblock">
							<h3>
								<xsl:text>&type-edit-tip;</xsl:text>
							</h3>
							<div class="content" >
							</div>
							<div class="group-tip-hide"></div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="udata//field[@name = 'modificator_type_id' or @name = 'rule_type_id']" mode="form-modify" priority="1" />
	<xsl:template match="/result[@method = 'discount_edit']//field[@name = 'discount_type_id']" mode="form-modify" />

	<!-- Payment and delivery systems -->
	<xsl:template mode="form-modify"
		match="/result[@method = 'payment_add' or @method = 'payment_edit' or @method = 'delivery_add' or @method = 'delivery_edit']/data/object">
		<xsl:apply-templates mode="form-modify">
			<xsl:with-param name="show-type"><xsl:text>0</xsl:text></xsl:with-param>
		</xsl:apply-templates>
	</xsl:template>


	<!-- Stores -->
	<xsl:template match="/result[@method = 'store_add' or @method = 'store_edit']/data">
		<form method="post" action="do/" enctype="multipart/form-data">
			<input type="hidden" name="referer" value="{/result/@referer-uri}" id="form-referer" />
			<input type="hidden" name="domain" value="{$domain-floated}"/>

			<xsl:apply-templates mode="form-modify">
				<xsl:with-param name="group-title"><xsl:text>&label-store-common;</xsl:text></xsl:with-param>
			</xsl:apply-templates>
			<div class="row">
				<xsl:choose>
					<xsl:when test="$data-action = 'create'">
						<xsl:call-template name="std-form-buttons-add"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="std-form-buttons"/>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</form>
	</xsl:template>


	<!-- Delivery -->
	<xsl:template match="/result[@method = 'delivery_address_edit']/data/object" mode="form-modify">
		<xsl:apply-templates select="properties/group" mode="form-modify">
			<xsl:with-param name="show-name"><xsl:text>0</xsl:text></xsl:with-param>
		</xsl:apply-templates>
	</xsl:template>

	<!-- Payment -->
	<xsl:template match="field[@name = 'payment_type_id' or @name='delivery_type_id']" mode="form-modify">
		<div class="col-md-6" id="{generate-id()}" xmlns:umi="http://www.umi-cms.ru/TR/umi" umi:type="{@type-id}">
			<div class="title-edit">
				<acronym title="{@tip}">
					<xsl:apply-templates select="." mode="sys-tips" />
					<xsl:value-of select="@title" />
				</acronym>
				<xsl:apply-templates select="." mode="required_text" />
			</div>
			<div>
				<select name="{@input_name}" id="relationSelect{generate-id()}" class="default newselect type_select" autocomplete="off">
					<xsl:apply-templates select="." mode="required_attr" >
						<xsl:with-param name="old_class" >default newselect type_select</xsl:with-param>
					</xsl:apply-templates>
					<xsl:apply-templates select="values/item" mode="type-select">
						<xsl:with-param name="object-type-guid" select="document(concat('utype://', /result/data/object/@type-id))/udata/type/@guid" />
					</xsl:apply-templates>
				</select>
				<!--<script>
					$('select.type_select').on('change',function (){
						console.log($(this).val());
					});
				</script>-->
			</div>
		</div>
	</xsl:template>

	<xsl:template match="field/values/item" mode="type-select">
		<xsl:param name="object-type-guid" select="0" />
		<xsl:variable name="type-guid" select="document(concat('uobject://', @id))/udata/object/properties/group/property[@name='payment_type_guid' or @name='delivery_type_guid']/value" />
		<option value="{@id}">
			<xsl:if test="$type-guid=$object-type-guid">
				<xsl:attribute name="selected"><xsl:text>selected</xsl:text></xsl:attribute>
			</xsl:if>
			<xsl:value-of select="." />
		</option>
	</xsl:template>

	<xsl:template match="properties/group[@name = 'statistic_info']" mode="form-modify">
		<div class="panel-settings" name="g_{@name}">
			<a data-name="{@name}" data-label="{@title}"></a>
			<div class="title">
				<xsl:call-template name="group-tip">
					<xsl:with-param name="param" select="@name" />
				</xsl:call-template>
				<div class="round-toggle"></div>
				<h3>
					<xsl:value-of select="@title" />
				</h3>
			</div>
			<div class="content">
				<div class="layout">
					<div class="column">
						<div class="row">
							<xsl:apply-templates select="field" mode="form-modify" />
						</div>
					</div>
					<div class="column">
						<div  class="infoblock">
							<h3>
								<xsl:text>&type-edit-tip;</xsl:text>
							</h3>
							<div class="content" >
							</div>
							<div class="group-tip-hide"></div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="properties/group[@name = 'statistic_info']/field" mode="form-modify">
		<div class="col-md-6">

				<div class="title-edit">
					<acronym title="{@tip}"><xsl:value-of select="./@title" /></acronym>
				</div>
				<xsl:apply-templates select="." />

		</div>
	</xsl:template>

	<xsl:template match="properties/group[@name = 'statistic_info']/field[@name = 'http_referer' or @name = 'http_target']" mode="form-modify">
		<div class="col-md-6">

				<div class="title-edit">
					<acronym title="{@tip}"><xsl:value-of select="./@title" /></acronym>
				</div>
				<a href="{.}" id="{generate-id()}" class="text" name="{@input_name}"><xsl:apply-templates select="." mode="value" /></a>
		</div>
	</xsl:template>

	<xsl:template match="properties/group[@name = 'statistic_info']/field" mode="value">
		<xsl:text>/</xsl:text>
	</xsl:template>

	<xsl:template match="properties/group[@name = 'statistic_info']/field[. != '']" mode="value">
		<xsl:value-of select="." disable-output-escaping="yes" />
	</xsl:template>

</xsl:stylesheet>