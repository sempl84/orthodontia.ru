<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://common">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<!-- Current language info -->
	<xsl:variable name="lang-prefix" select="/result/@pre-lang"/>
	
	<!-- Header and title of current page -->
	<xsl:variable name="header"	select="document('udata://core/header')/udata"/>
	<xsl:variable name="title" select="concat('&cms-name; - ', $header)" />
	
	<!-- Skins and langs list from system settings -->
	<xsl:variable name="skins" select="document('udata://system/getSkinsList/')/udata" />
	<xsl:variable name="interface-langs" select="document('udata://system/getInterfaceLangsList/')/udata" />
	<xsl:variable name="from_page" select="document('udata://system/getCurrentURI/1/')/udata"/>
	
	<xsl:template match="/">
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>
					<xsl:value-of select="$title" />
				</title>
				<link type="text/css" rel="stylesheet" href="/styles/skins/modern/design/css/grid.css"/>
				<link type="text/css" rel="stylesheet" href="/styles/skins/modern/design/css/main.css"/>
				<link type="text/css" rel="stylesheet" href="/styles/skins/modern/design/css/selectize.css"/>
				<style>
					body {
						background-image: linear-gradient(transparent 70%, #082d42 100%), radial-gradient(circle farthest-side, #0077c5 0px, #045684 70%, #082d42 100%);
						height: 100%;
						overflow: hidden;
						width: 100%;
					}

					.bubbles {
						background: rgba(0, 0, 0, 0) url("/styles/skins/modern/design/img/auth-bubbles-back.png") no-repeat scroll center center;
						height: 100%;
						opacity: 1;
						position: absolute;
						top: 0;
						transition: opacity 0.2s linear 0s;
						width: 100%;
					}

					.bubbles-front {
						background: rgba(0, 0, 0, 0) url("/styles/skins/modern/design/img/auth-bubbles-front.png") no-repeat scroll center center;
						height: 100%;
						opacity: 1;
						position: absolute;
						top: 0;
						transition: opacity 0.2s linear 0s;
						width: 100%;
					}

					#auth {
						position: absolute;
						width: 410px;
						min-width:410px;
						top: 50%;
						left: 50%;
						margin: -250px 0 0 -207px;
						<!--border-radius: 10px 10px 0 0;
						-webkit-border-radius: 10px 10px 0 0;
						-moz-border-radius: 10px 10px 0 0;-->

						box-sizing: border-box;
						opacity: 0.85;

					}
					#auth img{
						position: relative;
						margin: 0 0 20px 50px;
					}

					.title {
						padding: 5px 10px 8px 15px;
						background: #00a0dc none repeat scroll 0 0;
						color: #ffffff;
						border-radius: 10px 10px 0 0;
						font-weight: bold;
					}

					.cont{
						padding: 1px 10px 10px 10px;
						background: #ffffff;
						border: 1px solid #d0dce6;
						box-shadow: 0 0 10px 1px #097aaa;
						border-radius: 5px;
					}
				</style>
			</head>
			<body onload="javascript:document.getElementById('login_field').focus();">
					<div class="bubbles"></div>
					<div class="bubbles-front"></div>
					<div id="auth">
						<!--<div class="title">
							<div>&main-authorization;</div>

							&lt;!&ndash;<div>
								<xsl:value-of select="$header"/>
							</div>&ndash;&gt;
						</div>-->
						<img src="/styles/skins/modern/design/img/auth-logo.png"/>
						<div class="cont">
							<xsl:apply-templates select="result/data/error" />

							<form action="{$lang-prefix}/admin/users/login_do/" method="post">
								<input type="hidden" name="from_page" value="{$from_page}"/>
								
								<div class="row">
									<div class="col-md-12">
										<div class="title-edit">
											<xsl:text>&label-login;</xsl:text>
										</div>

										<xsl:choose>
											<xsl:when test="/result[@demo='1']">
												<input class="default" type="text" id="login_field" name="login"
													   value="demo"/>
											</xsl:when>
											<xsl:otherwise>
												<input class="default" type="text" id="login_field" name="login"/>
											</xsl:otherwise>
										</xsl:choose>
									</div>

								</div>
								
								<div class="row">
									<div class="col-md-12">
										<div class="title-edit">
											<xsl:text>&label-password;</xsl:text>
										</div>

										<xsl:choose>
											<xsl:when test="/result[@demo='1']">
												<input class="default" type="password" id="password_field" name="password"
													   value="demo"/>
											</xsl:when>
											<xsl:otherwise>
												<input class="default" type="password" id="password_field" name="password"/>
											</xsl:otherwise>
										</xsl:choose>
									</div>

								</div>
								
								<xsl:if test="count($skins//item) &gt; 1">
									<div class="row">
										<div class="col-md-12">
											<div class="title-edit"><xsl:text>&label-skin;</xsl:text>
											</div>
											<select id="skin_field" name="skin_sel">
												<xsl:apply-templates select="$skins"/>
											</select>
										</div>
									</div>
								</xsl:if>
								<xsl:if test="count($interface-langs//item) &gt; 1">
									<div class="row">
										<div class="col-md-12">
											<div class="title-edit"><xsl:text>&label-interface-lang;</xsl:text>
											</div>
											<select id="ilang" name="ilang">
												<xsl:apply-templates select="$interface-langs"/>
											</select>
										</div>
									</div>
								</xsl:if>
								<div class="row">
									<div class="col-md-6">
										<input type="submit" id="submit_field" value="&label-login-do;" class="btn color-blue btn-small" />
									</div>
									<div class="col-md-6">
										<div class="checkbox">
											<input type="checkbox" value="1" name="u-login-store"/>
										</div>
										<span>
											<xsl:text>&label-remember-login;</xsl:text>
										</span>
									</div>
								</div>
							</form>
							<div style="clear:both" />
						</div>
						<div class="foot" />
					</div>
					<script type="text/javascript" src="/js/jquery/jquery.js"></script>
					<script type="text/javascript" src="/styles/skins/modern/design/js/selectize.min.js"></script>
					<script>
						<![CDATA[
						(function (){
							$('select').selectize({
								allowEmptyOption: true,
								create: false,
								hideSelected:true
							});

							$('.checkbox input:checked').parent().addClass('checked');
    						$('.checkbox').click(function () {
        						$(this).toggleClass('checked');
    						});

							var d = document.querySelector('.bubbles'),
								e = document.querySelector('.bubbles-front'),
								f = function (a) {
									var b = document.body.offsetWidth,
									c = document.body.offsetHeight,
									f = 0.04,
									g = 0.04,
									h = (b / 2 - a.clientX) * f,
									i = (c / 2 - a.clientY) * g;
									d.style.marginLeft = h + 'px',
									d.style.marginTop = i + 'px',
									e.style.marginLeft = 0.2 * h + 'px',
									e.style.marginTop = 0.2 * i + 'px'
								},
								g = !0;
							document.onmousemove = function (a) {
								g && (d.className = 'bubbles visible', e.className = 'bubbles-front visible', g = !1),
                        		f(a)
							}
						})();
						]]>
					</script>
			</body>
		</html>
	</xsl:template>
	
	<xsl:template match="udata[@module = 'system' and @method = 'getSkinsList']//item">
		<option value="{@id}">
			<xsl:value-of select="." />
		</option>
	</xsl:template>
	
	<xsl:template match="udata[@module = 'system' and @method = 'getSkinsList']//item[@id = ../@current]">
		<option value="{@id}" selected="selected">
			<xsl:value-of select="." />
		</option>
	</xsl:template>
	
	
	<xsl:template match="udata[@module = 'system' and @method = 'getInterfaceLangsList']//item">
		<option value="{@prefix}">
			<xsl:value-of select="." />
		</option>
	</xsl:template>
	
	<xsl:template match="udata[@module = 'system' and @method = 'getInterfaceLangsList']//item[@prefix = ../@current]">
		<option value="{@prefix}" selected="selected">
			<xsl:value-of select="." />
		</option>
	</xsl:template>
	
	
	<xsl:template match="error">
		<div class="row">
			<div class="col-md-12">
				<p class="error">
					<strong>
						<xsl:text>&label-error;: </xsl:text>
					</strong>
					<xsl:value-of select="." />
				</p>
			</div>
		</div>
	</xsl:template>

</xsl:stylesheet>