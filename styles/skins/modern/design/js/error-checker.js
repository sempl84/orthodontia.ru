/**
 * Функция валидации значений полей и вывода соответветствующих ошибок в админке
 * @param options опции для валидации
 * @param options.form форма, поля которой будут провалидированы
 * @param options.check список имен валидаторов и селектор полей, которые будут провалидированы
 * @example Пример вызова функции с указанием формы и валидаторов на пустые поля и числовые значения.
 * checkErrors({
 *  form: $('form').eq(0),
 *  check: {
 *      empty: 'input.required',
 *      number: 'input[type=number]'
 *  }});
 */
var checkErrors = (function($, _) {
    var funcPrefix = 'check';
    var templateElement = null;
    var template = null;
    var errors = [];
    var elementsWithErrors = [];

    var allowedCheckingFunctions = {

        /**
         * Валидирует поля на пустые значения
         * @param {String|HTMLCollection|HTMLElement|jQuery} form форма, в которой находятся целевые поля
         * @param {String|HTMLCollection|HTMLElement|jQuery} fields поля, которые будут провалидированы
         */
        checkEmpty: function(form, fields) {
            var isTextArea;
            var editor = null;
            var editorValue = null;
            var isEmptyValue = false;
            var fieldName = '';
            fields = fields || $('sup:contains(*)', form).parent().next().children();

            $(fields, form).each(function() {
                isTextArea = this.tagName && this.tagName.toLowerCase() === 'textarea';
                if (isTextArea && typeof tinyMCE == 'object') {
                    editor = tinyMCE.get(this.id);

                    if (editor && typeof(editor.getContent) === 'function') {
                        editorValue = editor.getContent({format: 'text'});
                        isEmptyValue = (typeof(editorValue) === 'string' && editorValue.length === 0) ||
                            (editorValue.length === 1 && editorValue.charCodeAt(0) === "\n".charCodeAt(0));
                    }
                } else {
                    isEmptyValue = (this.value === '');
                }

                if (isEmptyValue) {
                    if (isTextArea && editor) {
                        elementsWithErrors.push($(editor.contentAreaContainer).closest('table').get(0));
                    } else {
                        elementsWithErrors.push(this);
                    }

                    fieldName =  $(this).parent().parent().find('acronym').eq(0).text();
                    errors.push({
                        title: getLabel('js-error-required-field'),
                        text: fieldName
                    });
                }
            });
        },

        /**
         * Валидирует поля на числовые значения
         * @param {String|HTMLCollection|HTMLElement|jQuery} form форма, в которой находятся целевые поля
         * @param {String|HTMLCollection|HTMLElement|jQuery} fields поля, которые будут провалидированы
         */
        checkNumber: function(form, fields) {
            fields = fields || $('input.number-field', form);
            var value;
            var fieldName = '';
            var isNumber;

            $(fields, form).each(function() {

                var isEmpty = false;

                value = $(this).val();

                if (value === '') {
                    return true;
                }

                isNumber = !isEmpty && !isNaN(parseFloat(value)) && value.match(/^[0-9eE.,]+$/);

                if (!isNumber) {
                    elementsWithErrors.push(this);
                    fieldName =  $(this).parent().parent().find('acronym').eq(0).text();
                    errors.push({
                        title: getLabel('js-error-number-field'),
                        text: fieldName
                    })
                }
            });
        },

		/**
		 * Валидирует поля на значение времени в формате hh:mm
		 * @param {String|HTMLCollection|HTMLElement|jQuery} form форма, в которой находятся целевые поля
		 * @param {String|HTMLCollection|HTMLElement|jQuery} fields поля, которые будут провалидированы
		 */
		checkTime: function(form, fields) {
			fields = fields || $('input.time-field', form);
			var value;
			var fieldName = '';
			var isTime;

			$(fields, form).each(function() {

				var isEmpty = false;

				value = $(this).val();

				if (value === '') {
					return true;
				}

				isTime = !isEmpty && value.match(/^[0-9]{2}:[0-9]{2}$/);

				if (!isTime) {
					elementsWithErrors.push(this);
					fieldName =  $(this).parent().parent().find('acronym').eq(0).text();
					errors.push({
						title: getLabel('js-error-time-field'),
						text: fieldName
					})
				}
			});
		},

		/**
		 * Валидирует поля на значение даты в формате d:m:Y
		 * @param {String|HTMLCollection|HTMLElement|jQuery} form форма, в которой находятся целевые поля
		 * @param {String|HTMLCollection|HTMLElement|jQuery} fields поля, которые будут провалидированы
		 */
		checkDate: function(form, fields) {
			fields = fields || $('input.date-field', form);
			var value;
			var fieldName = '';
			var isTime;

			$(fields, form).each(function() {

				var isEmpty = false;

				value = $(this).val();

				if (value === '') {
					return true;
				}

				isTime = !isEmpty && value.match(/^[0-9]{2}\.[0-9]{2}\.[0-9]{4}$/);

				if (!isTime) {
					elementsWithErrors.push(this);
					fieldName =  $(this).parent().parent().find('acronym').eq(0).text();
					errors.push({
						title: getLabel('js-error-date-field'),
						text: fieldName
					})
				}
			});
		}
    };

    return function (options) {
        options = options || {};
        var form = options.form || $('form.form_modify');
        var checkingFunctionsSuffixes = options.check;
        var checkingFunctions = [];
        var errorsHTML = '';
        templateElement = $('#error-checker-template');

        if (templateElement.length === 0) {
            return;
        }

        template = _.template(templateElement.html());

        _.each(checkingFunctionsSuffixes, function(fields, funcName) {
            var camelFuncName = funcName.charAt(0).toUpperCase() + funcName.slice(1);
            var fullFuncName = funcPrefix + camelFuncName;

            if (typeof allowedCheckingFunctions[fullFuncName] === 'function') {
                checkingFunctions[fullFuncName] = fields;
                allowedCheckingFunctions[fullFuncName](form, fields);
            }
        });

        if (errors.length > 0) {
            errorsHTML = template({errors: errors});

            openDialog('', getLabel('js-label-errors-occurred'), {
                timer: 5000,
                width: 400,
                html: errorsHTML,
                closeCallback: function() {
                    _.each(elementsWithErrors, function(element) {
						style = window.getComputedStyle(element);

						if (style.display != 'none') {
							$(element).effect("highlight", {color:"#00a0dc"}, 5000);
						}
                    });

                    elementsWithErrors = [];
                    errors = [];
                }
            });

            return false;
        }

        return true;
    }

}(jQuery, _));