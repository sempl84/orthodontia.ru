/**
 * Переделка контрола Relation для форм
 * Created by Mu57Di3 on 03.12.15.
 */


var ControlRelation = function (options) {
    var $ = jQuery || {},
        container = options.container || null,
        selectizerInput = null,
        enteredVal = '',
        needLoad = true,
        selectizeObject = null,
        typeId = options.type || null,
        id = options.id || "",
        empty = options.empty || false,
        sourceUri = options.sourceUri || '/admin/data/guide_items_all/',
        searchMode=false;


    function addObjectToGuide(_name, _guideId, callback) {
        var objectName = _name;
        var guideId = _guideId;
        var request = {
            url: '',
            data: null
        };
        var newObject = {
            id: 0,
            name: ''
        };


        request.url = '/admin/udata://data/addObjectToGuide/.json';
        request.data = {
            param0: objectName,
            param1: guideId
        };

        jQuery.ajax({
            url: request.url,
            dataType: 'json',
            data: request.data,
            type: 'post',
            success: function (response) {
                if (typeof(response.data) !== 'undefined' &&
                    typeof(response.data.object) !== 'undefined') {
                    newObject.id = parseInt(response.data.object.id);

                    if (isNaN(newObject.id) || newObject.id <= 0) {
                        return;
                    }

                    newObject.name = response.data.object.name;

                    if (typeof(callback) === 'function') {
                        callback(newObject.name, newObject.id);
                    }

                }
            },
            error: function () {
                addButton.removeAttribute('disabled');
            }
        });
    }

    function addElement() {
        openDialog('', 'Создание нового элемента', {
            html: '<div class="title-edit">Введите название:</div>' +
                  '<input id="newRelationVal" type="text" class="default" value="' + String(enteredVal).trim() + '"/>',
            cancelButton: true,
            confirmText: 'Добавить',
            cancelText: 'Отменить',
            confirmOnEnterElement: '#newRelationVal',
            confirmCallback: function(popupName) {
                var val = $('#newRelationVal').val();
                if (val != null && val.length > 0) {
                    addObjectToGuide(val, typeId, function (name, data) {
                        selectizeObject.addOption({text: name, value: data});
                        selectizeObject.addItem(data, true);
                        closeDialog(popupName);
                    })
                }
            }
        });
    }

    function loadItemsAll(callback, override) {
        jQuery.ajax({
            url: sourceUri + typeId + ".xml?allow-empty",
            type: "get",
            complete: function (r) {
                if (override) {
                    callback(r);
                    return;
                }

                updateItemsAll(r, callback);
            }
        });
    }

    function updateItemsAll(r, callback) {
        callback = typeof callback == 'function' ? callback : function() {};
        updateElements(r, callback);
    }

    /**
     * Обновляет элементы DOM в соответствии с загруженными данными
     * @param {Object} r ответ от сервера
     * @param {Function} callback вызывается после обновления DOM
     */
    function updateElements(r, callback) {
        callback = typeof callback == 'function' ? callback : function() {};

        if (selectizeObject) {
            var items = r.responseXML.getElementsByTagName('object');
            selectizeObject.lock();
            var oldval = selectizeObject.getValue();

            if (typeof oldval == 'string') {
                oldval = [oldval];
            }

            selectizeObject.clearOptions(true);
            for (var i = 0, cnt = items.length; i < cnt; i++) {
                selectizeObject.addOption({text: items[i].getAttribute('name'), value: items[i].getAttribute('id')})
            }
            for (var j= 0, cntj=oldval.length; j<cntj; j++) {
                selectizeObject.addItem(oldval[j], true);
            }
            selectizeObject.unlock();
            needLoad = false;
            callback(r);
        }
    }

    function makeSearch() {
        $('.selectize-control',container).remove();
        var select = $('select',container),
            fieldName = select.attr('name'),
            oldval = select.val(),
            option = '';
        if (oldval !== null){
            option = '<option value="'+oldval+'" selected>'+$('option',select).text()+'</option>';
        }
        select.remove();
        $('.layout-col-control',container).append($('<select placeholder="Поиск" name="'+fieldName+'">'+option+'</select>'));
        createSearchedSelectize();
    }

    function createSearchedSelectize() {
        selectizeObject = $('select', container).selectize({
            plugins: ['remove_button'],
            allowEmptyOption: true,
            create: false,
            onType: function () {
                var input = selectizerInput || $('.selectize-input input', container);
                enteredVal = input.val();
            },
            load:function (query,callback) {
                jQuery.ajax({
                    url: sourceUri + typeId + ".xml",
                    data: {
                        search: [query]
                    },
                    type: "get",
                    complete: function (r) {
                        if (callback) {
                            var items = r.responseXML.getElementsByTagName('object'),
                                result=[];
                            for (var i = 0, cnt = items.length; i < cnt; i++) {
                                result.push({text: items[i].getAttribute('name'), value: items[i].getAttribute('id')});
                            }
                            callback(result);
                            return;
                        }
                    }
                });

            }

        });

        selectizeObject = selectizeObject[0].selectize;
    }

    function init(options) {
        $('.relation-add', options.container).bind('click', function () {
            addElement();
        });
        var preload = options.preload == undefined ? true : options.preload;
        selectizeObject = $('select', options.container).selectize({
            plugins: ['remove_button'],
            allowEmptyOption: true,
            create: false,
            /* hideSelected:true,*/
            onType: function () {
                var input = selectizerInput || $('.selectize-input input', container);
                enteredVal = input.val();
            },
            onFocus: function () {
                loadItems();
            },
            onInitialize: function () {
                if (preload) {
                    container.one('mouseover', function () {
                        loadItems();
                    });
                }
            }
        });

        /**
         * Загружает элементы из сервера, если они присутствуют
         */
        function loadItems() {

            if (needLoad) {
                loadItemsAll(function(response) {
                    var items = response.responseXML.getElementsByTagName('object');

                    if (items.length) {
                        updateElements(response);
                    } else {
                        items = response.responseXML.getElementsByTagName('empty');
                        var result = String(items[0].getAttribute('result')).toLowerCase().replace(/[\s]/g,'');
                        if (result === 'toomuchitems'){
                            makeSearch();
                        }
                    }

                    needLoad = false;

                }, true);
            }
        }

        selectizeObject = selectizeObject[0].selectize;
    }

    init(options);

    return {
        selectize: selectizeObject,
        loadItemsAll: loadItemsAll
    };
};