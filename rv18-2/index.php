<?php
session_start();
header("HTTP/1.1 200 OK");
header('Content-Type: text/html; charset=UTF-8');

$event_id = 1822;
$event_name = "Рождественские встречи 2018";
$uploaddir = "https://orthodontia.ru/rv18/";
function places($num) {
	$rest = $num % 10;
	if ($num > 4 && $num < 21)
		return "мест";
	else {
		if ($num > 1 && $num < 5)
			return "места";
		else {
			if ($num == 1 || $rest == 1)
				return "место";
			else {
				if ($num > 20 && $rest > 1 && $rest < 5)
					return "места";
				else
					return "мест";
			}
		}
	}
}

$speakers = array(
	1 => array(	"name" => "Андре Назаров",
				"about" => "Д-р Назаров родился в Архангельске.
В 1983 году окончил стоматологический факультет Архангельского государственного медицинского института (с 2000 г. – Северный государственный медицинский университет).
Работал стоматологом-терапевтом и ортопедом.
В 1993 году переехал в Денвер (штат Колорадо, США), где начал заниматься ортодонтией.
В 2000 году окончил Школу стоматологии Бостонского университета. Получил сертификат по ортодонтии и степень магистра стоматологических наук в Центре продвинутого обучения стоматологов Университета Сент-Луиса.
С 2003 года ведет частную практику в клинике Allegro Orthodontics
Член Американской стоматологической ассоциации и Американской ассоциации ортодонтов.
Более 15-ти лет работает системой Damon.",
				"image" => "nazarov.jpg",
	),
	2 => array(	"name" => "Леонид Эмдин",
				"about" => "Врач-стоматолог-ортопед, ортодонт, высшая квалификационная категория. В 2000 году с отличием окончил стоматологический факультет СПбГМУ им. акад. И.П.Павлова. В 2001 году окончил интернатуру СЗГМУ им. И.И. Мечникова на кафедре «Терапевтической стоматологии №2» по специальности «Стоматология общей практики». В 2003 году окончил ординатуру СЗГМУ им. И.И. Мечникова на кафедре «Терапевтической стоматологии №2» под руководством профессора Б.Т. Мороза по специальности «Стоматология терапевтическая». В 2003 году получил дополнительную специализацию на курсе профессиональной переподготовки «Современные аспекты ортопедической стоматологии с курсом ортодонтии» в СЗГМУ им. И.И. Мечникова на кафедре «Ортопедической стоматологии» под руководством профессора А.В. Цимбалистова.

В 2005 году прошёл специализацию по ортодонтии в СПбГМУ им.акад. И.П. Павлова. Работает врачом-стоматологом-ортопедом, ортодонтом в ООО «Райден». С февраля 2017 года занимает должность заместителя главного врача по оказанию платной медицинской помощи в СПб ГБУЗ ГСП №33. Участник большого количества обучающих мероприятий в России и за рубежом.",
				"image" => "emdin.jpg",
	),
	3 => array(	"name" => "Михаил Морозов",
				"about" => "Врач-ортодонт, руководитель ортодонтического отделения клиники «Конфиденция» (Санкт-Петербург). Имеет значительный опыт работы с Damon System и Insignia. Курирует клиническую практику ординаторов на базе ортодонтического отделения клиники",
				"image" => "morozov_new.jpg",
	),
	4 => array(	"name" => "Анна Бадмаева",
				"about" => "Врач-ортодонт клиники эстетической ортодонтии «Конфиденция» (Санкт-Петербург), консультант по применению ортодонтической продукции Ormco, участник большого числа обучающих семинаров и международных конгрессов. Стажировалась в клиниках доктора Дидье Фийона (Париж), доктора Алана Багдена (Вашингтон), отвечает в клинике за  лечение пациентов с дисфункцией ВНЧС, а также за раннее ортодонтическое лечение детей", "theme" => "Исправление прикуса в детском возрасте. Раннее лечение на брекет-системе", "themedesc" => "В рамках мастер-класса мы затронем основные аспекты раннего ортодонтического лечения на частичной брекет-системе, разберем несколько клинических случаев, решим ряд задач на их основе, выбрав альтернативные алгоритмы лечения. Коснемся вопроса лечения детей на съемной ортодонтической технике",
				"image" => "badmaeva.jpg",
	)
);
$mkcounts = explode("/", file_get_contents("mkcounts.txt"));
$mktimes = array(1 => "10:00 - 11:30", 2 => "12:00 - 13:30", 3 => "14:30 - 16:00", 4 => "16:30 - 18:00");
$mks = array(
	1 => array(
		1 => array(
			"speaker" => 2, 
			"similar" => "4,7,10",
			"theme" => "Планирование комплексного лечения пациента с патологией прикуса",
			"themedesc" => "В стоматологии не может быть обособленной диагностики и клинической картины.
На мастер-классе будет рассмотрен пример комплексной диагностики клинической картины. В результат будет составлен единый полный план лечения пациента, который должен быть реализован в определённой последовательности всеми участниками команды специалистов. Формулировать и отвечать за такой сценарий будет лидер команды, которым чаще всего является врач-стоматолог-ортопед или врач-стоматолог-ортодонт.
Программа:
•	Фотопротокол 
•	Анализ лицевых признаков
•	Фонетический анализ
•	Зубо-губной анализ
•	Выбор ориентировочных плоскостей 
•	Анализ статической и динамической окклюзии 
•	Методы регистрации центрального соотношения челюстей для гипсования моделей в артикуляторе 
•	Ортопедические аспекты расчета телерентгенограммы в боковой проекции
•	Определение прогноза зубов
•	Использование приложений мобильных устройств для эстетико-функционального анализа 
•	Тренинг по формулировке плана лечения
"
		),
		2 => array(
			"speaker" => 3, 
			"similar" => "5,8,11",
			"theme" => "Концепция активного нивелирования",
			"themedesc" => "Даже если вы не торопитесь и сообщили пациенту, что будете лечить его 2.5 года, позитивные изменения на начальном этапе могут растопить сердце даже самого сурового пациента-скептика. А завоеванное в начале ношения брекетов доверие поможет комфортно чувствовать себя врачу на завершении, занимаясь такими любимыми деталями. 
Пассивное самолигирование позволяет быстро получить первые результаты, но если в работу системы добавить немного «специй», то результаты могут быть ошеломительными. 
Концепция активного нивелирования – это микс техник из лингвальной и вестибулярной механики, помогающий зубам перемещаться в нужном направлении на начальных этапах и ускоряющий процесс перехода от круглых дуг к прямоугольным без потери уникальных эффектов пассивного самолигирования. 
Цель мастер-класса – не только показать легкие в исполнении техники, ускоряющие нивелирование, но и научить сочетать их, используя принципы биомеханики, а также видеть места, в которых системе нужна помощь. После разбора теории взаимодействия сил и практики по установке основных элементов, каждый сможет потренироваться в решении логических задач по ускорению нивелирования.
"
		),
		3 => array(
			"speaker" => 4, 
			"similar" => "6,9,12",
			"theme" => "Изготовление суставной шины (сплинта) в артикуляторе для пациента с дисфункцией височно-нижнечелюстного сустава (ВНЧС)",
			"themedesc" => "•	Роль артикулятора в планировании ортодонтического лечения;
•	Задачи применения артикулятора в ортодонтической диагностике;
•	Наложение лицевой дуги, определение центрального соотношения, гипсовка в артикулятор;
•	Предортодонтическая коррекция состояния сустава;
•	Применение капп, суставных шин/сплинтов (практическая часть): правила изготовления, типы шин по целям использования, особенности клинической коррекции шины; 
•	Как перейти от шины к брекетам. Сроки, этапы, клинические примеры.

Чего не будет: диагностики ВНЧС, разбора жалоб, видов щелчков и болей, постановки диагноза.
Что желательно знать участнику мастер-класса: анатомию ВНЧС, жалобы пациентов, диагностика дисфункции, причины возникновения дисфункции, варианты определения центрального соотношения.
"
		)
	),
	2 => array(
		4 => array(
			"speaker" => 2, 
			"similar" => "1,7,10",
			"theme" => "Планирование комплексного лечения пациента с патологией прикуса",
			"themedesc" => "В стоматологии не может быть обособленной диагностики и клинической картины.
На мастер-классе будет рассмотрен пример комплексной диагностики клинической картины. В результат будет составлен единый полный план лечения пациента, который должен быть реализован в определённой последовательности всеми участниками команды специалистов. Формулировать и отвечать за такой сценарий будет лидер команды, которым чаще всего является врач-стоматолог-ортопед или врач-стоматолог-ортодонт.
Программа:
•	Фотопротокол 
•	Анализ лицевых признаков
•	Фонетический анализ
•	Зубо-губной анализ
•	Выбор ориентировочных плоскостей 
•	Анализ статической и динамической окклюзии 
•	Методы регистрации центрального соотношения челюстей для гипсования моделей в артикуляторе 
•	Ортопедические аспекты расчета телерентгенограммы в боковой проекции
•	Определение прогноза зубов
•	Использование приложений мобильных устройств для эстетико-функционального анализа 
•	Тренинг по формулировке плана лечения
"
		),
		5 => array(
			"speaker" => 3, 
			"similar" => "2,8,11",
			"theme" => "Концепция активного нивелирования",
			"themedesc" => "Даже если вы не торопитесь и сообщили пациенту, что будете лечить его 2.5 года, позитивные изменения на начальном этапе могут растопить сердце даже самого сурового пациента-скептика. А завоеванное в начале ношения брекетов доверие поможет комфортно чувствовать себя врачу на завершении, занимаясь такими любимыми деталями. 
Пассивное самолигирование позволяет быстро получить первые результаты, но если в работу системы добавить немного «специй», то результаты могут быть ошеломительными. 
Концепция активного нивелирования – это микс техник из лингвальной и вестибулярной механики, помогающий зубам перемещаться в нужном направлении на начальных этапах и ускоряющий процесс перехода от круглых дуг к прямоугольным без потери уникальных эффектов пассивного самолигирования. 
Цель мастер-класса – не только показать легкие в исполнении техники, ускоряющие нивелирование, но и научить сочетать их, используя принципы биомеханики, а также видеть места, в которых системе нужна помощь. После разбора теории взаимодействия сил и практики по установке основных элементов, каждый сможет потренироваться в решении логических задач по ускорению нивелирования.
"
		),
		6 => array(
			"speaker" => 4, 
			"similar" => "3,9,12",
			"theme" => "Изготовление суставной шины (сплинта) в артикуляторе для пациента с дисфункцией височно-нижнечелюстного сустава (ВНЧС)",
			"themedesc" => "•	Роль артикулятора в планировании ортодонтического лечения;
•	Задачи применения артикулятора в ортодонтической диагностике;
•	Наложение лицевой дуги, определение центрального соотношения, гипсовка в артикулятор;
•	Предортодонтическая коррекция состояния сустава;
•	Применение капп, суставных шин/сплинтов (практическая часть): правила изготовления, типы шин по целям использования, особенности клинической коррекции шины; 
•	Как перейти от шины к брекетам. Сроки, этапы, клинические примеры.

Чего не будет: диагностики ВНЧС, разбора жалоб, видов щелчков и болей, постановки диагноза.
Что желательно знать участнику мастер-класса: анатомию ВНЧС, жалобы пациентов, диагностика дисфункции, причины возникновения дисфункции, варианты определения центрального соотношения.
"
		)
	),
	3 => array(
		7 => array(
			"speaker" => 2, 
			"similar" => "10,1,4",
			"theme" => "Планирование комплексного лечения пациента с патологией прикуса",
			"themedesc" => "В стоматологии не может быть обособленной диагностики и клинической картины.
На мастер-классе будет рассмотрен пример комплексной диагностики клинической картины. В результат будет составлен единый полный план лечения пациента, который должен быть реализован в определённой последовательности всеми участниками команды специалистов. Формулировать и отвечать за такой сценарий будет лидер команды, которым чаще всего является врач-стоматолог-ортопед или врач-стоматолог-ортодонт.
Программа:
•	Фотопротокол 
•	Анализ лицевых признаков
•	Фонетический анализ
•	Зубо-губной анализ
•	Выбор ориентировочных плоскостей 
•	Анализ статической и динамической окклюзии 
•	Методы регистрации центрального соотношения челюстей для гипсования моделей в артикуляторе 
•	Ортопедические аспекты расчета телерентгенограммы в боковой проекции
•	Определение прогноза зубов
•	Использование приложений мобильных устройств для эстетико-функционального анализа 
•	Тренинг по формулировке плана лечения
"
		),
		8 => array(
			"speaker" => 3, 
			"similar" => "11,2,5",
			"theme" => "Концепция активного нивелирования",
			"themedesc" => "Даже если вы не торопитесь и сообщили пациенту, что будете лечить его 2.5 года, позитивные изменения на начальном этапе могут растопить сердце даже самого сурового пациента-скептика. А завоеванное в начале ношения брекетов доверие поможет комфортно чувствовать себя врачу на завершении, занимаясь такими любимыми деталями. 
Пассивное самолигирование позволяет быстро получить первые результаты, но если в работу системы добавить немного «специй», то результаты могут быть ошеломительными. 
Концепция активного нивелирования – это микс техник из лингвальной и вестибулярной механики, помогающий зубам перемещаться в нужном направлении на начальных этапах и ускоряющий процесс перехода от круглых дуг к прямоугольным без потери уникальных эффектов пассивного самолигирования. 
Цель мастер-класса – не только показать легкие в исполнении техники, ускоряющие нивелирование, но и научить сочетать их, используя принципы биомеханики, а также видеть места, в которых системе нужна помощь. После разбора теории взаимодействия сил и практики по установке основных элементов, каждый сможет потренироваться в решении логических задач по ускорению нивелирования.
"
		),
		9 => array(
			"speaker" => 4, 
			"similar" => "12,3,6",
			"theme" => "Изготовление суставной шины (сплинта) в артикуляторе для пациента с дисфункцией височно-нижнечелюстного сустава (ВНЧС)",
			"themedesc" => "•	Роль артикулятора в планировании ортодонтического лечения;
•	Задачи применения артикулятора в ортодонтической диагностике;
•	Наложение лицевой дуги, определение центрального соотношения, гипсовка в артикулятор;
•	Предортодонтическая коррекция состояния сустава;
•	Применение капп, суставных шин/сплинтов (практическая часть): правила изготовления, типы шин по целям использования, особенности клинической коррекции шины; 
•	Как перейти от шины к брекетам. Сроки, этапы, клинические примеры.

Чего не будет: диагностики ВНЧС, разбора жалоб, видов щелчков и болей, постановки диагноза.
Что желательно знать участнику мастер-класса: анатомию ВНЧС, жалобы пациентов, диагностика дисфункции, причины возникновения дисфункции, варианты определения центрального соотношения.
"
		)
	),
	4 => array(
		10 => array(
			"speaker" => 2, 
			"similar" => "7,1,4",
			"theme" => "Планирование комплексного лечения пациента с патологией прикуса",
			"themedesc" => "В стоматологии не может быть обособленной диагностики и клинической картины.
На мастер-классе будет рассмотрен пример комплексной диагностики клинической картины. В результат будет составлен единый полный план лечения пациента, который должен быть реализован в определённой последовательности всеми участниками команды специалистов. Формулировать и отвечать за такой сценарий будет лидер команды, которым чаще всего является врач-стоматолог-ортопед или врач-стоматолог-ортодонт.
Программа:
•	Фотопротокол 
•	Анализ лицевых признаков
•	Фонетический анализ
•	Зубо-губной анализ
•	Выбор ориентировочных плоскостей 
•	Анализ статической и динамической окклюзии 
•	Методы регистрации центрального соотношения челюстей для гипсования моделей в артикуляторе 
•	Ортопедические аспекты расчета телерентгенограммы в боковой проекции
•	Определение прогноза зубов
•	Использование приложений мобильных устройств для эстетико-функционального анализа 
•	Тренинг по формулировке плана лечения
"
		),
		11 => array(
			"speaker" => 3, 
			"similar" => "8,2,5",
			"theme" => "Концепция активного нивелирования",
			"themedesc" => "Даже если вы не торопитесь и сообщили пациенту, что будете лечить его 2.5 года, позитивные изменения на начальном этапе могут растопить сердце даже самого сурового пациента-скептика. А завоеванное в начале ношения брекетов доверие поможет комфортно чувствовать себя врачу на завершении, занимаясь такими любимыми деталями. 
Пассивное самолигирование позволяет быстро получить первые результаты, но если в работу системы добавить немного «специй», то результаты могут быть ошеломительными. 
Концепция активного нивелирования – это микс техник из лингвальной и вестибулярной механики, помогающий зубам перемещаться в нужном направлении на начальных этапах и ускоряющий процесс перехода от круглых дуг к прямоугольным без потери уникальных эффектов пассивного самолигирования. 
Цель мастер-класса – не только показать легкие в исполнении техники, ускоряющие нивелирование, но и научить сочетать их, используя принципы биомеханики, а также видеть места, в которых системе нужна помощь. После разбора теории взаимодействия сил и практики по установке основных элементов, каждый сможет потренироваться в решении логических задач по ускорению нивелирования.
"
		),
		12 => array(
			"speaker" => 4, 
			"similar" => "9,3,6",
			"theme" => "Изготовление суставной шины (сплинта) в артикуляторе для пациента с дисфункцией височно-нижнечелюстного сустава (ВНЧС)",
			"themedesc" => "•	Роль артикулятора в планировании ортодонтического лечения;
•	Задачи применения артикулятора в ортодонтической диагностике;
•	Наложение лицевой дуги, определение центрального соотношения, гипсовка в артикулятор;
•	Предортодонтическая коррекция состояния сустава;
•	Применение капп, суставных шин/сплинтов (практическая часть): правила изготовления, типы шин по целям использования, особенности клинической коррекции шины; 
•	Как перейти от шины к брекетам. Сроки, этапы, клинические примеры.

Чего не будет: диагностики ВНЧС, разбора жалоб, видов щелчков и болей, постановки диагноза.
Что желательно знать участнику мастер-класса: анатомию ВНЧС, жалобы пациентов, диагностика дисфункции, причины возникновения дисфункции, варианты определения центрального соотношения.
"
		)
	),
);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Рождественские встречи с ORMCO 2018</title>
		<meta http–equiv="Content–Type" content="text/html; charset=UTF–8">
		<link rel="stylesheet" media="screen" type="text/css" href="main.css">
		<meta name="keywords" content="">
		<meta name="description" content="">
		<meta name="HandheldFriendly" content="true">
		<meta name="MobileOptimized" content="width">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="/favicon.gif" type="image/x-icon">
		<link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="js/jquery.touchSwipe.min.js" type="text/javascript"></script>
		<script src="js/ajaxupload.3.5.js" type="text/javascript"></script>
		<script src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyBoDhMbGeceqxv4_7SROd3YL1-0C-SyBGo" type="text/javascript"></script>
		<script>
			var pop1 = 0;
			var userid = "";
			function parallaxScroll(){
				var scrolled = $(window).scrollTop();
				$('#parallax-bg1').css('top',(0-(scrolled*.25))+'px');
				$('#parallax-bg2').css('top',(0-(scrolled*.5))+'px');
				$('#parallax-bg3').css('top',(0-(scrolled*.75))+'px');
				// $('body').css('background-position', '50% ' + (0-(scrolled*.75))+'px');
			}
			function remember_pass() {
				var e_email = $("#e_email").val();
				if (isEmail(e_email)) {
					$.ajax({
						url:"https://orthodontia.ru/udata/emarket/forget_pas/" + e_email + "/.json",
						xhrFields: {
						  withCredentials: true
						},
						dataType: "json",
						success:function(data) {
							if (data.status == "success") {
								$("#rp_ok").fadeIn(300);
								setTimeout(function() {$("#rp_ok").fadeOut(300)}, 15000);
							}
							else {
								$("#rp_fail").text("Пользователь не найден в базе, попробуйте еще раз").fadeIn(300);
								setTimeout(function() {$("#rp_fail").fadeOut(300)}, 15000);
							}
						}
					});
				}
				else {
					$("#rp_fail").text("Укажите свой email!").fadeIn(300);
					setTimeout(function() {$("#rp_fail").fadeOut(300)}, 15000);
				}
			}
			function countdown() {
				var mcl1 = $("#mcl1").val();
				var mcl2 = $("#mcl2").val();
				$.ajax({
					type: "POST",
					url: "xp_countdown.php",
					timeout: 5000,
					data: "mcl1=" + mcl1 + "&mcl2=" + mcl2,
					success: function(data) {
					},
					error: function(data) {
					}
				});
			}
			function writedata() {
				var alldata = "";
				$('form input').each(function() {
					if ($(this).attr("id") != undefined)
						alldata += "&" + $(this).attr("id") + "=" + $(this).val();
				});
				$('form select').each(function() {
					if ($(this).attr("id") != undefined)
						alldata += "&" + $(this).attr("id") + "=" + $(this).val();
				});
				$.ajax({
					type: "POST",
					url: "xp_write.php",
					timeout: 5000,
					data: "alldata=" + alldata,
					success: function(data) {
					},
					error: function(data) {
					}
				});
			}
			function setdiscount(discount, element, chk) {
				$(".dscnt").not("#" + element).removeAttr("checked");
				var pr = $("#regularprice").val();
				if (chk) {
					$("#discount").text(discount);
					$(".discount").fadeIn(300);
					pr = pr - parseInt(pr*(discount/100));
				}
				else {
					$(".discount").fadeOut(300);
					$(".newform_upload").fadeOut(300);
					$("#uploaded").fadeOut(300);
				}
				$("#finalprice").text(pr + " р.");
			}
			function addnewyur() {
				$(".yurs").fadeOut(300);
				$(".newform_reg").fadeOut(300);
				$(".addyur").fadeOut(300, function() {
					$(".addyurform1").fadeIn(300);
				});
			}
			function addyurajax() {
				var tosend = "id=" + userid;
				$('.addyurform input').each(function(index) {
					var did = $(this).attr("id");
					if (did == "yurname")
						did = "name";
					if (did == "yuremail")
						did = "email";
					var dval = $(this).val();
					tosend += "&data[new][" + did + "]=" + dval;
				});
				$('.addyurform textarea').each(function(index) {
					var did = $(this).attr("id");
					var dval = $(this).val();
					tosend += "&data[new][" + did + "]=" + dval;
				});
				$.ajax({
					url:"https://orthodontia.ru/udata/emarket/add_legal_item/.json?" + tosend,
					xhrFields: {
					  withCredentials: true
					},
					dataType: "json",
					success:function(data) {
						if (data.result != 0) {
							$(".addyurform").fadeOut(300).promise().done(function() {
								// добавляем новое юр. лицо в select
								$('#yur').append($('<option>', { 
										value: data.result,
										text : $("#yurname").val()
								}));
								$(".newform_reg").fadeIn(300);
								$(".addyur").fadeIn(300);
								$(".yurs").fadeIn(300);
							});
						}
						else {
							$(".addyurform").fadeOut(300, function() {
								$(".newform_reg").fadeIn(300);
								$(".addyur").fadeIn(300);
								$(".yurs").fadeIn(300);
							});
							alert("Новое юр. лицо не добавлено! Проверьте данные!");
						}
					}
				});
			}
			function showyur(n) {
				var foo = 0;
				if (n == 1) {
					$(".addyurform1 .ness").each(function() {
						if ($(this).val() == '') {
							if (foo == 0) {
								$('html,body').animate({scrollTop: $(this).offset().top - 100}, 500);
							}
							$(this).addClass('redfld');
							foo = 1;
						}
						else {
							$(this).removeClass('redfld');
						}
					});
				}
				if (foo == 0) {
					$(".addyurform" + (3 - n)).fadeOut(300, function() {
						$(".addyurform" + n).fadeIn(300);
					});
				}
			}
			function showtab(tab) {
				if (tab == 1)
					$("#yur").attr("disabled", "disabled");
				else
					$("#yur").removeAttr("disabled");
				$(".tab" + (3 - tab)).fadeOut(300);
				$(".tab" + tab).fadeIn(300);
				$(".tabbut" + (3 - tab)).removeClass("active");
				$(".tabbut" + tab).addClass("active");
			}
			function showpart(part) {
				if (chkform(2)) {
					if (userid == "") {
						$.ajax({
							url:"https://orthodontia.ru/udata/emarket/logout_user/.json",
							xhrFields: {
							  withCredentials: true
							},
							dataType: "json",
							success:function(data) {
								// авторизация на сайте
								var tosend = "id=" + userid;
								$('.part1 input').each(function(index) {
									var did = $(this).attr("id");
									var dval = $(this).val();
									tosend += "&data[new][" + did + "]=" + dval;
								});
								$('.part1 select').each(function(index) {
									var did = $(this).attr("id");
									var dval = $(this).val();
									tosend += "&data[new][" + did + "]=" + dval;
								});
								$.ajax({
									url:"https://orthodontia.ru/udata/users/lpreg/.json?" + tosend,
									dataType: "json",
									success:function(data) {
										$(".f_anim").css("opacity", 0);
										if (data.status == "successful") {
											// пользователь зарегистрирован
											user_id = data.user_id;
											// отправляем данные в GA
											var utype = $("#who").val();
											var usertype = 'not a doctor';
											if (utype == "Ортодонт")
												usertype = "orthodontist";
											if (utype == "Хирург")
												usertype = "surgeon";
											if (utype == "Другая специализация")
												usertype = "other";
											console.log("usertype: " + usertype);
											ga('send', 'event', 'form', 'registration', usertype);
											yaCounter23063434.reachGoal('UserRegistration');
											$.ajax({
												url:"https://orthodontia.ru/udata/emarket/logout_user/.json",
												xhrFields: {
												  withCredentials: true
												},
												dataType: "json",
												success:function(data) {
													var newemail = $("#email").val();
													var newpassword = $("#password").val();
													$.ajax({
														xhrFields: {
														  withCredentials: true
														},
														dataType: "json",
														url:"https://orthodontia.ru/udata/emarket/get_user_info/" + newemail + "/" + newpassword + "/.json",
														success:function(data) {
															$(".part" + (3 - part)).fadeOut(300);
															$(".part" + part).fadeIn(300);
															$(".partbut" + (3 - part)).addClass("hidden");
															$(".partbut" + part).removeClass("hidden");
															$(".notfound").hide(0);
														}
													});
												}
											});
										}
										else {
											// неудачно, рисуем ошибку
											$("#form_error").html(data.result);
											$(".mask1").fadeIn(300);
											$(".form_error").fadeIn(300);
										}
									}
								});
							}
						});
					}
					else {
						$(".part" + (3 - part)).fadeOut(300);
						$(".part" + part).fadeIn(300);
						$(".partbut" + (3 - part)).addClass("hidden");
						$(".partbut" + part).removeClass("hidden");
					}
				}
			}
			function chk_entry() {
				$("#entry_but").text("Проверяем...").attr("disabled", "disabled");
				$(".wrong").fadeOut(300);
				var e_email = $("#e_email").val();
				var e_pass = $("#e_pass").val();
				var eventid = $("#events").val();
				$.ajax({
					url:"https://orthodontia.ru/udata/emarket/logout_user/.json",
					xhrFields: {
					  withCredentials: true
					},
					dataType: "json",
					success:function(data) {
						$.ajax({
							xhrFields: {
							  withCredentials: true
							},
							dataType: "json",
							url:"https://orthodontia.ru/udata/emarket/get_user_info/" + e_email + "/" + e_pass + "/.json",
							success:function(data) {
								$(".f_anim").css("opacity", 0);
								if (typeof data.extended === 'object') {
									// пользователь ввел правильные данные
									console.log("user auth");
									// отправляем данные в GA
									ga('send', 'event', 'form', 'authorization');
									yaCounter23063434.reachGoal('UserAuthorization');
									var user = data;
									$(".wrong").fadeOut(300);
									$.ajax({
										url:"https://orthodontia.ru/udata/emarket/is_first_purchase/" + e_email + "/" + eventid + "/.json",
										xhrFields: {
										  withCredentials: true
										},
										dataType: "json",
										success:function(data) {
											$(".f_anim").css("opacity", 0);
											if (data.result == 1) {
												// пользователь уже зарегистрирован на мероприятие, выводим надпись
												$(".exists").css("opacity", 1);
											}
											else {
												userid = user.id;
												// alert(userid);
												// пользователь еще не зарегистрирован, все ОК, рисуем форму регистрации на мероприятие
												$(".openform").fadeOut(300, function() {
													$(".formbox2").hide(0);
													// console.table(user);
													// вставляем поля
													arruser = new Array();
													var allprops = user.extended.groups.group[0].property;
													$.each(allprops, function(index, value) {
														// console.log(this.name);
														arruser[this.name] = this.value;
													});
													if (typeof user.extended.groups.group[1] == "object") {
														var allprops = user.extended.groups.group[1].property;
														$.each(allprops, function(index, value) {
															// console.log(this.name);
															arruser[this.name] = this.value;
														});
													}
													// console.table(arruser);
													$("#fname").val(arruser["fname"].value);
													$("#lname").val(arruser["lname"].value);
													$("#father_name").val(arruser["father_name"].value);
													$("#email").val(arruser["e-mail"].value);
													$("#phone").val(arruser["phone"].value);
													$("#city").val(arruser["city"].value);
													$("#company").val(arruser["company"].value);
													$("#bd").val(arruser["bd"].value);
													$("#country").val(arruser["country"].item.name);
													$("#region").val(arruser["region"].item.id);
													if (typeof arruser["prof_status"] == "object")
														$("#who").val(arruser["prof_status"].item.name);
													// убираем пароли из формы
													$(".phide").hide();
													// рисуем форму
													$(".formbox3").show(0);
													$(".openform").fadeIn(300);
												});
												// подгружаем юр. лиц
												$.ajax({
													url:"https://orthodontia.ru/udata/emarket/legalList/.json",
													xhrFields: {
													  withCredentials: true
													},
													dataType: "json",
													success:function(data1) {
														// alert(data1.items.length);
														// добавляем юр. лица в select
														$.each(data1.items.item, function(index, value) {
															$('#yur').append($('<option>', { 
																	value: value.id,
																	text : value.name 
															}));
														});
														// console.table(data1.items);
													}
												});
											}
										},
										error:function(data) {
										}
									});
								}
								else {
									// пользователь ввел неверные данные
									$("#e_email").addClass("invalid");
									$("#e_pass").addClass("invalid");
									$(".wrong").fadeIn(300, function() {
										$("#entry_but").text("Войти").removeAttr("disabled");
									});
								}
							}
						});
					}
				});
			}
			function fixbody() {
				$("body").addClass("hold");
			}
			function unfixbody() {
				$("body").removeClass("hold");
			}
			function chg_passstate() {
				if ($("#e_pass").attr("type") == "password") {
					$("#e_pass").attr("type", "text");
				}
				else {
					$("#e_pass").attr("type", "password");
				}
			}
			function chk_f_email() {
				var f_email = $("#f_email").val();
				var eventid = $("#events").val();
				if (f_email != "" && isEmail(f_email)) {
					$(".exists").fadeOut(300);
					$(".f_anim").css("opacity", 1);
					$("#f_email").removeClass("invalid");
					// проверяем email
					$.ajax({
						url:"https://orthodontia.ru/udata/emarket/is_user_exist/" + f_email + "/.json",
						dataType: "json",
						success:function(data) {
							$(".f_anim").css("opacity", 0);
							if (data.result == 1) {
								// пользователь найден, проверяем, есть ли регистрация на мероприятие
								$.ajax({
									url:"https://orthodontia.ru/udata/emarket/is_first_purchase/" + f_email + "/" + eventid + "/.json",
									dataType: "json",
									success:function(data) {
										$(".f_anim").css("opacity", 0);
										if (data.result == 1) {
											// пользователь уже зарегистрирован на мероприятие, выводим надпись
											$(".exists").fadeIn(300);
										}
										else {
											// еще не зарегистрирован, рисуем форму входа
											$(".openform").fadeOut(300, function() {
												$("#email").val($("#f_email").val());
												$(".formbox1").hide(0);
												$("#e_email").val(f_email);
												$(".formbox2").show(0);
												$(".openform").fadeIn(300);
											});
										}
									}
								});
							}
							else {
								// пользователь не найден, отображаем форму регистрации пользователя
								$(".openform").fadeOut(300, function() {
									$("#email").val($("#f_email").val());
									$(".notfound").show(0);
									$(".formbox1").hide(0);
									$(".formbox3").show(0);
									$(".openform").fadeIn(300);
								});
							}
						},
						error:function(data) {
						}
					});
				}
				else {
					$("#f_email").addClass("invalid");
				}
			}
			jQuery(function($){
				$.datepicker.regional['ru'] = {
					closeText: 'Закрыть',
					prevText: '&#x3C;Пред',
					nextText: 'След&#x3E;',
					currentText: 'Сегодня',
					monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
					'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
					monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
					'Июл','Авг','Сен','Окт','Ноя','Дек'],
					dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
					dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
					dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
					weekHeader: 'Нед',
					dateFormat: 'yy-mm-dd',
					firstDay: 1,
					isRTL: false,
					showMonthAfterYear: false,
					yearSuffix: ''};
				$.datepicker.setDefaults($.datepicker.regional['ru']);
			});
			$(function() {
				$(".date1").datepicker({
					changeYear: true,
					yearRange: '1920:2020',
					defaultDate: '1980-01-01'
				});
			});
			lnks = new Array("form", "place", "price", "speakers", "programm", "about");
			var gmargin = 797;
			var curgal = 1;
			var maxx = 6;
			var gmargin1 = 797;
			var curgal1 = 1;
			var maxx1 = 12;
			var igos = 0;
			var totigos = 2;
			var prods = 0;
			var filedone = 0;
			function pset(ord, val, state) {
				if (state) {
					prods++;
					$("#p" + ord).val(val);
					$(".prodsel span").text("Выбрано " + prods);
				}
				else {
					prods--;
					$("#p" + ord).val("");
					if (prods == 0)
						$(".prodsel span").text("-------");
					else
						$(".prodsel span").text("Выбрано " + prods);
				}
			}
			function igo(group, ord, theme, state, similar) {
				if (state) {
					igos++;
					var arr_s = similar.split(",");
					$(".prog[data-group=" + group + "]").addClass("faded").find(".igo").fadeOut(200);
					$(".prog[data-mk=" + ord + "]").removeClass("faded").find(".igo").fadeIn(200);
					$(".igoset[data-group=" + group + "]").addClass("faded").find("input").attr("disabled", "disabled");
					$(".igoset[data-mk=" + ord + "]").removeClass("faded").find("input").removeAttr("disabled").attr("checked", "checked");;
					$.each(arr_s, function(key, val) {
						$(".prog[data-mk=" + val + "]").addClass("faded").find(".igo").fadeOut(200);
						$(".igoset[data-mk=" + val + "]").removeClass("faded").find("input").removeAttr("disabled").attr("checked", "checked");;
					})
					$("#mk" + ord).parent().addClass("sel");
					$("#lf" + ord).text("Пойду!");
					$("#mk_" + ord).parent().addClass("sel");
					$("#lf_" + ord).text("Пойду!");
					var html = "<div id=\"chosen" + ord + "\" class=\"chitem group\"><div class=\"igonum\">" + igos + "</div><div class=\"igodesc\" id=\"igod" + ord + "\">" + theme + "</div><!--<div class=\"igodel\" onClick=\"igo(" + group + ", " + ord + ", '', false);\"><span>X</span> Удалить</div>--></div>";
					$(".chosen").append(html);
					$("#mclasses" + group).val(theme);
					$("#mcl" + group).val(ord);
					// для прокрутки вниз при выборе первого мастер-класса
					if (igos == 1)
						$('html,body').animate({scrollTop: $(".prog[data-group=" + (3 - group) + "]").offset().top - 200}, 500);
					if (igos == totigos) {
						fixbody();
						$('.mask').fadeIn(300);
						$('.openform').fadeIn(300);
					}
				}
				else {
					igos--;
					var arr_s = similar.split(",");
					$(".prog[data-group=" + group + "]").removeClass("faded").find(".igo").fadeIn(200);
					$(".igoset[data-group=" + group + "]").not(".cancelled").removeClass("faded").find("input").removeAttr("disabled");
					$.each(arr_s, function(key, val) {
						$(".prog[data-mk=" + val + "]").removeClass("faded").find(".igo").fadeIn(200);
						$(".igoset[data-mk=" + val + "]").not(".cancelled").removeClass("faded").find("input").removeAttr("disabled");
					})
					$(".igoset[data-mk=" + ord + "]").find("input").removeAttr("checked");
					$("#mk" + ord).parent().removeClass("sel");
					$("#lf" + ord).text("Хочу пойти");
					$("#mk_" + ord).parent().removeClass("sel");
					$("#lf_" + ord).text("Хочу пойти");
					$("#chosen" + ord).remove();
					$('.chitem').each(function(index) {
						$(this).find(".igonum").text(index + 1);
					});
					$("#mclasses" + group).val("");
					$("#mcl" + group).val("");
				}
			}
			function openmob(n) {
				var wdth = $(window).width();
				if (wdth < 960) {
					$("#event" + n).slideToggle(300);
					$("#prog" + n).toggleClass("open");
				}
			}
			function isEmail(email) {
				var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				return regex.test(email);
			}
			function noplaces() {
				$('.mask').fadeIn(300);
				$('#noplaces').fadeIn(300);
			}
			function chkigos() {
				if (igos < totigos) {						
					$('.prog1').fadeOut(200);
					$('.progtab.sel').removeClass('sel');
					$('.progtab2').addClass('sel');
					$('div[data-date=15]').fadeOut(300);
					$('h2[data-date=15]').fadeOut(300);
					$('.progspace').fadeIn(300);
					$('.mktime').css('display', 'table');
					$('div[data-date=16]').fadeIn(300);
					$('h2[data-date=16]').fadeIn(300);
					$('.prog2').fadeIn(200).promise().done(function() {
						$('html,body').animate({scrollTop: $(".prog2").offset().top - 100}, 500);
					});
					if (pop1 == 0) {
						$('.mask').fadeIn(300);
						$('#popup').fadeIn(300);
					}
					return false;
				}
				else
					return true;
			}
			function chkform(chkfile) {
				var foo = 0;
				if (chkfile == 1) {
					$("#register").attr("disabled", "disabled");
					var pr = $("#finalprice").val();
					pr = pr.replace(' p.', '');
					// отправляем данные в GA
					ga('send', 'event', 'ET', 'event_click', 'LP');
					yaCounter23063434.reachGoal('EventClick');
					dataLayer.push({
						"ecommerce": {
							"add": {
								"products": [
									{
									"name" : "<?=$event_name;?>",
									"price": pr,
									"category": "Платное",
									"quantity": 1
									}
								]
							}
						}
					});
				}
				if (chkfile != 0 && filedone == 0 && $('.upl:checked').length > 0)
					{
					if (foo == 0) {
						$('html,body').animate({scrollTop: $('.upl').offset().top - 100}, 500);
					}
					$('#upload').addClass('redfld');
					foo = 1;
				}
				$('.ness:visible').each(function() {
					if ($(this).val() == '') {
						if (foo == 0) {
							$('html,body').animate({scrollTop: $(this).offset().top - 100}, 500);
						}
						$(this).addClass('redfld');
						foo = 1;
					}
					else {
						$(this).removeClass('redfld');
					}
				}); 
				$('#email').each(function() {
					if (!isEmail($(this).val())) {
						if (foo == 0) {
							// alert($(this).offset().top);
							$('html,body').animate({scrollTop: $(this).offset().top - 100}, 500);
						}
						$(this).addClass('redfld');
						foo = 1;
					}
					else {
						$(this).removeClass('redfld');
					}
				});
				/*
				if (foo == 0) {
					if (($("#events").val() == "Лекции и мастер-классы" || $("#events").val() == "Только лекция 15 декабря") && igos < 3) {
						$('html,body').animate({scrollTop: $(".igosel").offset().top - 100}, 500);
						$(".igosel").addClass('redfld');
						foo = 1;
					}
					else {
						$(".igosel").removeClass('redfld');
					}
				}
				*/
				if (foo == 0) {
					if (userid != "" && chkfile == 1) {
						countdown();
						writedata();
						ga('send', 'event', 'ET', 'event_registration', 'LP');
						ga('ec:addProduct', {
						'name': '<?=$event_name;?>',
						'category': 'Платное',
						'price': pr,
						'quantity': 1});
						ga('ec:setAction', 'purchase', {
						'id': '<?=$event_id;?>',
						'revenue': pr,
						});
						yaCounter23063434.reachGoal('EventRegistration');
						dataLayer.push({
							"ecommerce": {
								"purchase": {
									"actionField": {
										"id" : "<?=$event_id;?>"
									},
									"products": [{
										"name" : "<?=$event_name;?>",
										"price": pr
									}]
								}
							}
						});
					}
					return true;
				}
				else {
					$("#register").removeAttr("disabled");
					return false;
				}
			}
			$(window).resize(function() {
				var wdth = $(window).width();
				if (wdth < 960) {
					gmargin = wdth - 20;
					$(".gal_slide").css("width", gmargin);
					var ghght = gmargin/780*330;
					$(".gal_slide").css("height", ghght);
					$(".gallery").css("height", ghght);
					$(".gal_navs").css("top", ghght - 80);
				}
				/*
				var gwdth = $(window).width();
				if (gwdth > 1250)
					gwdth = 1250;
				gmargin = gwdth;
				var ghght = gwdth/780*330;
				if (gwdth < 480)
					$(".prog").css("width", gwdth - 40);
				$(".gal_slide").css("width", gwdth);
				$(".gal_slide").css("height", ghght);
				$(".gallery").css("height", ghght);
				$(".gal_navs").css("top", ghght - 80);
				*/
			})
			// $(window).load(function() {
				// $("#left").css("margin-left", "-1046px");
				// $("#right").css("margin-left", "250px");
			// });
			var spmargin = 0;
			var swidth = 320;
			var curspeaker = 1;
 			function speaker_left() {
				if (spmargin < 0) {
					spmargin = spmargin + swidth;
					$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 500);
					curspeaker--;
					$(".spbullets").find(".active").removeClass("active");
					$("#spbul_" + curspeaker).addClass("active");
				}
				else {
					$(".speakers").animate({margin: '0 0 0 20'}, 200, function() {
						$(".speakers").animate({margin: '0 0 0 0'}, 200);
					});
				}
			}
 			function speaker_right() {
				var slide_count = $(".speakers").find(".speaker:visible").length;
				var limit = (slide_count - 1) * swidth;
				if (spmargin > -limit) {
					spmargin = spmargin - swidth;
					$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 500);
					$(".spbullets").find(".active").removeClass("active");
					curspeaker++;
					$(".spbullets").find(".active").removeClass("active");
					$("#spbul_" + curspeaker).addClass("active");
				}
				else {
					$(".speakers").animate({margin: '0 0 0 ' + (spmargin - 20)}, 200, function() {
						$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 200);
					});
				}
			}
			function speaker_go(sp) {
				spmargin = - swidth * (sp - 1);
				$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 500);
				$(".spbullets").find(".active").removeClass("active");
				$("#spbul_" + sp).addClass("active");
				curspeaker = sp;
			}
			$(document).ready(function() {
				$(window).bind('scroll',function(e){
					parallaxScroll();
				});
				$("body").bind("click", function(e) {
					if ($(e.target).closest(".igosel").length > 0 || $(e.target).closest(".prodsel").length > 0 || $(e.target).closest(".igoset").length > 0 || $(e.target).closest(".prodset").length > 0) {
						return;
					}
					$(".igodd").fadeOut(500);
					$(".igosel").removeClass('sel');
					$(".proddd").fadeOut(500);
					$(".prodsel").removeClass('sel');
				});
				var wdth = $(window).width();
				if (wdth < 960) {
					gmargin = wdth - 20;
					$(".gal_slide").css("width", gmargin);
					var ghght = gmargin/780*330;
					$(".gal_slide").css("height", ghght);
					$(".gallery").css("height", ghght);
					$(".gal_navs").css("top", ghght - 80);
					$(".speakers").swipe( {
						swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
							speaker_right();
						},
						swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
							speaker_left();
						},
					   threshold: 75
					});
					$(".gallery").swipe( {
						swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
							gallery_right();
						},
						swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
							gallery_left();
						},
					   threshold: 75
					});
					swidth = wdth;
					$(".speaker").css("width", wdth);
				}
				/*
				maxx = $(".gal_slide").length;
				var gwdth = $(window).width();
				if (gwdth > 1250)
					gwdth = 1250;
				gmargin = gwdth;
				var ghght = gwdth/780*330;
				if (gwdth < 480)
					$(".prog").css("width", gwdth - 40);
				$(".gal_slide").css("width", gwdth);
				$(".gal_slide").css("height", ghght);
				$(".gallery").css("height", ghght);
				$(".gal_navs").css("top", ghght - 80);
				*/
				hghts = new Array();
				for (var key in lnks) {
					if ($("[name=" + lnks[key] +"]").length) {
						hghts[key] = $("[name=" + lnks[key] +"]").offset().top;
					}
				}
				var i = document.location.hash.replace("#", "");
				$("a[href*='#']:not([href='#'])").click(function() {
					if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
						|| location.hostname == this.hostname) {
						var target = $(this.hash);
						target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
						   if (target.length) {
							var i = target.attr("name");
							scrolling = 1;
							$("#mobmenu").slideUp(300);
							$('html,body').animate({
								 scrollTop: target.offset().top - 60
							}, 1000, function() {
								scrolling = 0;
								// alert(target.offset().top);
							});
							$(".ch").removeClass("ch");
							$(this).parent().addClass("ch");
							// history.pushState(null, i, i);
							return false;
						}
					}
				});
			});
			function gallery_go(n) {
				curgmargin = -gmargin * (n - 1);
				$(".gal_slider").animate({marginLeft: curgmargin});
				curgal = n;
			}
			function gallery_left() {
				if (curgal > 1) {
					gallery_go(curgal - 1);
				}
				else {
					gallery_go(maxx);
				}
			}
			function gallery_right() {
				if (curgal < maxx) {
					gallery_go(curgal + 1);
				}
				else {
					gallery_go(1);
				}
			}
			function gallery_go1(n) {
				curgmargin1 = -gmargin1 * (n - 1);
				$(".gal_slider1").animate({marginLeft: curgmargin1});
				curgal1 = n;
			}
			function gallery_left1() {
				if (curgal1 > 1) {
					gallery_go1(curgal1 - 1);
				}
				else {
					gallery_go1(maxx1);
				}
			}
			function gallery_right1() {
				if (curgal1 < maxx1) {
					gallery_go1(curgal1 + 1);
				}
				else {
					gallery_go1(1);
				}
			}
			var scrolling = 0;
			window.onscroll = function() {
				var hght = $('body').height();
				// alert(hght);
				var prevscrolled = scrolled;
				var scrolled = window.pageYOffset || document.documentElement.scrollTop;
				if (scrolling == 0)
					{
					for (var key in hghts)
						{
						if (scrolled >= hghts[key] - 300) {
							$(".ch").removeClass("ch");
							$("#mi" + lnks[key]).addClass("ch");
							$("#mi" + lnks[key] + "1").addClass("ch");
							break;
						}
					}
				}
				var wdth = $(window).width();
				/*
				if (scrolled < 320 && wdth > 960) {
					var leftmarg = scrolled - 1146;
					var rightmarg = 450 - scrolled;
					$("#left").css("margin-left", leftmarg);
					$("#right").css("margin-left", rightmarg);
				}
				*/
				if (wdth > 960) {
					if (prevscrolled < scrolled)
						$(".header").addClass("out");
					else
						$(".header").removeClass("out");
				}
			}
			function reload() {
				var src = document.captcha.src;
				document.captcha.src = '/images/loading.gif';
				document.captcha.src = src + '?rand='+Math.random();
			}
			var x1 = 59.9317953;
			var y1 = 30.3507702;
			var x2 = 25.217603;
			var y2 = 55.2828107;
			var myLatlng1;
			var myLatlng2;
			var map;
			var marker1;
			var marker2;
			function initialize_map() {
				myLatlng1 = new google.maps.LatLng(x1, y1);
				myLatlng2 = new google.maps.LatLng(x2, y2);
				var mapOptions = {
					center: myLatlng1,
					mapTypeControl:!1,
					streetViewControl:!1,
					scrollwheel:!1,
					panControl:!1,
					draggable:!1,
					zoomControlOptions:{position:google.maps.ControlPosition.LEFT_TOP},
					zoom: 17,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				map = new google.maps.Map(document.getElementById("map"),
					mapOptions);
				marker1 = new google.maps.Marker({
					position: myLatlng1,
					map: map,
					title: "отель \"Отель «Коринтия» Санкт-Петербург\""
				});
				var infowindow1 = new google.maps.InfoWindow({
					content: "<div class=\"onmap\"><h3>Отель «Коринтия», Санкт-Петербург</h3>, Невский пр., дом 57</div>"
				});
				marker1.addListener('click', function() {
					infowindow1.open(map, marker1);
				});
				/*
				marker2 = new google.maps.Marker({
					position: myLatlng2,
					map: map,
					title: "Санкт-Петербург"
				});
				*/
			}
			function map1() {
				map.setCenter(myLatlng1);
			}
			function map2() {
				map.setCenter(myLatlng2);
			}
			// загрузка изображения
			$(function(){
				var btnUploads = $("#upload");
				$(btnUploads).each(function() {
					new AjaxUpload(this, {
						action: "xp_upload.php",
						name: "uploadfile",
						onComplete: function(file, response){
							if (response.replace("Error", "") == response) {
								$("#upload").addClass("done").text("Файл загружен");
								$("#uploaded").html("<img width=\"100%\" src=\"<?=$uploaddir;?>uploads/" + response + "\" alt=\"\" />");
								$("#file").val('<?=$uploaddir;?>uploads/' + response);
								filedone = 1;
							}
							else {
								alert("Файл " + file + " не загружен! Ошибка: " + response.replace("Error", ""));
							}
						}
					});
				})
			});
		</script>
	</head>
	<body>

<!-- Parallax foreground snowflakes -->
<div id="parallax-bg3">
	<img id="bg3-1" src="images/left.png" />
	<img id="bg3-2" src="images/right.png" />
</div>

<!-- Parallax midground snowflakes -->
<div id="parallax-bg2">
	<img id="bg2-1" src="images/sf1.png" />
	<img id="bg2-2" src="images/sf1.png" />
	<img id="bg2-3" src="images/sf1.png" />
	<img id="bg2-4" src="images/sf1.png" />
	<img id="bg2-5" src="images/sf1.png" />
	<img id="bg2-6" src="images/sf1.png" />
</div>

<!-- Parallax background snowflakes -->
<div id="parallax-bg1">
	<img id="bg1-1" src="images/sf2.png" />
	<img id="bg1-2" src="images/sf2.png" />
	<img id="bg1-3" src="images/sf2.png" />
	<img id="bg1-4" src="images/sf2.png" />
	<img id="bg1-5" src="images/sf2.png" />
	<img id="bg1-6" src="images/sf2.png" />
</div>
		<div class="container">
			<a name="about"></a>
			<div class="section top">
				<div class="section header">
					<div class="logo"><a href="#about"><img src="images/logo_transp.png" /></a></div>
					<div class="menu">
						<ul>
							<button type="button" onClick="<?if ($mkcounts[0] > 0) {?>if (chkigos()) {fixbody(); $('.mask').fadeIn(300); $('.openform').fadeIn(300);}<?}else{?>noplaces();<?}?>">Регистрация</button>
							<li id="miprogramm"><a href="#programm">Программа</a></li>
							<li id="mispeakers"><a href="#speakers">Спикеры</a></li>
							<li id="miprice"><a href="#price">Стоимость</a></li>
							<li id="miplace"><a href="#place">Место проведения</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="section baseinfo">
				<div class="seminar">
					<!--<div class="blur"><img src="images/post_rv.jpg" /></div>-->
					<h1>РОЖДЕСТВЕНСКИЕ ВСТРЕЧИ</h1>
					<h1 class="h1">12 МЕСЯЦЕВ<br />С ОРМКО</h1>
					<h1 class="h2">15 - 16 декабря 2018 года</h2>
					<h3 class="h3">Санкт-Петербург, отель «Коринтия», Невский пр., д. 57</h3>
				</div>
				<div class="main">
					<!-- Программа -->
					<a name="programm"></a>
					<h1>Программа</h1>
					<br />
					<div class="progtabbs group">
						<div class="progline"></div>
						<div class="progtabs group">
							<div class="progtab progtab1 sel" onClick="$('.prog2').fadeOut(200); $('.prog1').fadeIn(200); $('.progtab.sel').removeClass('sel'); $('.progtab1').addClass('sel'); $('div[data-date=16]').fadeOut(300); $('h2[data-date=16]').fadeOut(300); $('.progspace').fadeOut(300); $('div[data-date=15]').fadeIn(300); $('h2[data-date=15]').fadeIn(300); $('.mktime').css('display', 'none'); $('html,body').animate({scrollTop: $('.main').offset().top - 20}, 500);">15 декабря</div>
							<div class="progtab progtab2" onClick="$('.prog1').fadeOut(200); $('.prog2').fadeIn(200); $('.progtab.sel').removeClass('sel'); $('.progtab2').addClass('sel'); $('div[data-date=15]').fadeOut(300); $('h2[data-date=15]').fadeOut(300); $('.progspace').fadeIn(300); $('div[data-date=16]').fadeIn(300); $('h2[data-date=16]').fadeIn(300); $('.mktime').css('display', 'table'); $('html,body').animate({scrollTop: $('.main').offset().top - 20}, 500); if (pop1 == 0) {$('.mask').fadeIn(300); $('#popup').fadeIn(300);}">16 декабря</div>
						</div>
						<div class="progline"></div>
					</div>
					<div class="prog group" data-date="15" id="prog0" onClick="openmob(0);">
						<div class="event1">
							<div class="psimg"><img align="left" src="images/nazarov.jpg" /></div>
							<div class="eshort">Лекция Андре Назарова, Уникальность механики пассивной самолигирующей системы с вариантами выбора торка при раннем ортодонтическом лечении III класса. Возможности аппарата D-gainer</div>
						</div>
						<div class="pspeaker group">
							<div class="psimg"><img align="left" src="images/nazarov.jpg" /></div>
							<div class="psd"><h4>Андре<br />Назаров</h4>
							</div>
						</div>
						<div class="event">
							<img id="imgopen" src="images/arr_open.png" onClick="$(this).fadeOut(200); $('#imgclose').fadeIn(200); $(this).parent().find('.elong').slideDown(300);" />
							<img id="imgclose" src="images/arr_close.png" style="display: none;" onClick="$(this).fadeOut(200); $('#imgopen').fadeIn(200); $(this).parent().find('.elong').slideUp(300);" />
							<div class="eshort">Лекция Андре Назарова "Уникальность механики пассивной самолигирующей системы с вариантами выбора торка при раннем ортодонтическом лечении III класса. Возможности аппарата D-gainer"</div>
							<div class="elong">Наиболее интересная, но одновременно с тем сложная группа пациентов в ортодонтии – это дети. Пациенты на раннем ортодонтическом лечении требуют особых знаний от врача и подхода к процессу лечения.
В рамках лекции я поделюсь своей международной практикой использования аппарата D-gainer совместно с механикой Damon system (преимущества слабых сил). Мы поговорим с вами о D-gainer как о первом этапе комплексного лечения пациента. Это нежная и весьма эффективная альтернатива небному расширению при скученности.

Рассмотрим подробные примеры легкого и эффективного лечения ранних случаев III класса без удаления и без использования лицевой маски. Я покажу простые, а главное – предсказуемые варианты раннего ортодонтического лечения из своей практики, которые позволят предотвратить удаление зубов у пациента в будущем.</div>
						</div>
						<div class="event2" id="event0">
							<div class="psd">Наиболее интересная, но одновременно с тем сложная группа пациентов в ортодонтии – это дети. Пациенты на раннем ортодонтическом лечении требуют особых знаний от врача и подхода к процессу лечения.
В рамках лекции я поделюсь своей международной практикой использования аппарата D-gainer совместно с механикой Damon system (преимущества слабых сил). Мы поговорим с вами о D-gainer как о первом этапе комплексного лечения пациента. Это нежная и весьма эффективная альтернатива небному расширению при скученности.

Рассмотрим подробные примеры легкого и эффективного лечения ранних случаев III класса без удаления и без использования лицевой маски. Я покажу простые, а главное – предсказуемые варианты раннего ортодонтического лечения из своей практики, которые позволят предотвратить удаление зубов у пациента в будущем.</div>
						</div>
						<div class="dt">
							<span>15</span><br />декабря
						</div>
					</div>
					<div class="progspace"></div>
					<div class="prog1">Образовательное мероприятие соответствует требованиям для НМО.<br /><br />
Слушатели получат 6 кредитов по специальности (по выбору): Ортодонтия, Стоматология терапевтическая, Стоматология общей практики.</div>
					<div class="prog1">Осталось мест на лекцию <?=$mkcounts[0];?></div>
					<div class="prog2"><h3>Обратите внимание!</h3>
<br />
- 3 мастер-класса проходят одновременно в разных аудиториях
<br />
- каждый мастер-класс длится 3 часа;
<br />
- в середине мастер-класса – кофе-брейк;
<br />
- можно выбрать только по 1 мастер-классу в каждом блоке (до обеда и после обеда);
<br />
- количество мест ограничено.
<br /><br /><h3>Пожалуйста, выберите 1 мастер-класс до обеда и 1 - после обеда</h3></div>
					<div class="progs_cont">
						<div class="progs">
							<!-- 8 декабря -->
							<!-- Событие 1 -->
							<div class="prog group" data-date="15" id="prog1" onClick="openmob(1);">
								<div class="event1">
									<time>09:00 – 09:50</time>
									<div class="eshort">Регистрация, welcome coffee</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									<div class="psd"><h4>09:00 – 10:00</h4></div>
								</div>
								<div class="event">
									<div class="eshort">Регистрация</div>
								</div>
								<div class="event2" id="event1">
									<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									<div class="psd"><span>15</span> декабря<br /></div>
								</div>
								<div class="dt">
									<span>15</span><br />декабря
								</div>
							</div>
							<!-- Событие 2 -->
							<!--<div class="prog group" data-date="15" id="prog2" onClick="openmob(2);">
								<div class="event1">
									<time>09:50 – 10:00</time>
									<div class="eshort">Приветственное слово</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/opening.png" /></div>
									<div class="psd"><h4>09:50 – 10:00</h4></div>
								</div>
								<div class="event">
									<div class="eshort">Поздравление с наступающим праздником, приветственное слово от компании ORMCO</div>
								</div>
								<div class="event2" id="event2">
									<div class="psimg"><img align="left" src="images/opening.png" /></div>
									<div class="psd"><span>15</span> декабря<br /></div>
								</div>
								<div class="dt">
									<span>15</span><br />декабря
								</div>
							</div>-->
							<!-- Событие 3 -->
							<div class="prog group" data-date="15" id="prog3" onClick="openmob(3);">
								<div class="event1">
									<time>10:00 - 11:30</time>
									<div class="eshort">Лекция</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/master.jpg" /></div>
									<div class="psd"><h4><time>10:00 - 11:30</time></h4></div>
								</div>
								<div class="event">
									<button type="button">Лекция</button>
								</div>
								<div class="event2" id="event3">
									<div class="psimg"><img align="left" src="images/nazarov.jpg" /></div>
									<div class="psd"><span>15</span> декабря<br /><button type="button">Лекция</button></div>
								</div>
								<div class="dt">
									<span>15</span><br />декабря
								</div>
							</div>
							<!-- Событие 4 -->
							<div class="prog group" data-date="15" id="prog4" onClick="openmob(4);">
								<div class="event1">
									<time>11:30 – 12:00</time>
									<div class="eshort">Кофе-брейк</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									<div class="psd"><h4><time>11:30 – 12:00</time></h4></div>
								</div>
								<div class="event">
									<div class="eshort">Кофе-брейк</div>
								</div>
								<div class="event2" id="event4">
									<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									<div class="psd"><span>15</span> декабря<br /></div>
								</div>
								<div class="dt">
									<span>15</span><br />декабря
								</div>
							</div>
							<!-- Событие 5 -->
							<div class="prog group" data-date="15" id="prog5" onClick="openmob(5);">
								<div class="event1">
									<time>12:00 - 13:30</time>
									<div class="eshort">Лекция</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/master.jpg" /></div>
									<div class="psd"><h4><time>12:00 - 13:30</time></h4></div>
								</div>
								<div class="event">
									<button type="button">Лекция</button>
								</div>
								<div class="event2" id="event5">
									<div class="psimg"><img align="left" src="images/nazarov.jpg" /></div>
									<div class="psd"><span>15</span> декабря<br /><button type="button">Лекция</button></div>
								</div>
								<div class="dt">
									<span>15</span><br />декабря
								</div>
							</div>
							<!-- Событие 6 -->
							<div class="prog group" data-date="15" id="prog6" onClick="openmob(6);">
								<div class="event1">
									<time>13:30 - 14:30</time>
									<div class="eshort">Обед</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/lunch.png" /></div>
									<div class="psd"><h4><time>13:30 - 14:30</time></h4></div>
								</div>
								<div class="event">
									<div class="eshort">Обед</div>
								</div>
								<div class="event2" id="event6">
									<div class="psimg"><img align="left" src="images/lunch.png" /></div>
									<div class="psd"><span>15</span> декабря<br /></div>
								</div>
								<div class="dt">
									<span>15</span><br />декабря
								</div>
							</div>
							<!-- Событие 7 -->
							<div class="prog group" data-date="15" id="prog7" onClick="openmob(7);">
								<div class="event1">
									<time>14:30 - 16:00</time>
									<div class="eshort">Лекция</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/master.jpg" /></div>
									<div class="psd"><h4><time>14:30 - 16:00</time></h4></div>
								</div>
								<div class="event">
									<button type="button">Лекция</button>
								</div>
								<div class="event2" id="event7">
									<div class="psimg"><img align="left" src="images/nazarov.jpg" /></div>
									<div class="psd"><span>15</span> декабря<br /><button type="button">Лекция</button></div>
								</div>
								<div class="dt">
									<span>15</span><br />декабря
								</div>
							</div>
							<!-- Событие 8 -->
							<div class="prog group" data-date="15" id="prog8" onClick="openmob(8);">
								<div class="event1">
									<time>16:00 – 16:30</time>
									<div class="eshort">Кофе-брейк</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									<div class="psd"><h4><time>16:00 – 16:30</time></h4></div>
								</div>
								<div class="event">
									<div class="eshort">Кофе-брейк</div>
								</div>
								<div class="event2" id="event8">
									<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									<div class="psd"><span>15</span> декабря<br /></div>
								</div>
								<div class="dt">
									<span>15</span><br />декабря
								</div>
							</div>
							<!-- Событие 9 -->
							<div class="prog group" data-date="15" id="prog9" onClick="openmob(9);">
								<div class="event1">
									<time>16:30 - 18:00</time>
									<div class="eshort">Лекция</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/master.jpg" /></div>
									<div class="psd"><h4><time>16:30 - 18:00</time></h4></div>
								</div>
								<div class="event">
									<button type="button">Лекция</button>
								</div>
								<div class="event2" id="event9">
									<div class="psimg"><img align="left" src="images/nazarov.jpg" /></div>
									<div class="psd"><span>15</span> декабря<br /><button type="button">Лекция</button></div>
								</div>
								<div class="dt">
									<span>15</span><br />декабря
								</div>
							</div>
							<!-- Событие 10 -->
							<div class="prog group" data-date="15" id="prog10" onClick="openmob(10);">
								<div class="event1">
									<time>18:15 - 20:00</time>
									<div class="eshort">Christmas party</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/champ.jpg" /></div>
									<div class="psd"><h4><time>18:15 - 20:00</time></h4></div>
								</div>
								<div class="event">
									<div class="eshort">Christmas party</div>
								</div>
								<div class="event2" id="event10">
									<div class="psimg"><img align="left" src="images/champ.jpg" /></div>
									<div class="psd"><span>15</span> декабря<br /></div>
								</div>
								<div class="dt">
									<span>15</span><br />декабря
								</div>
							</div>
							<h2 data-date="16" style="display: none;">Мастер-классы</h2>
							<!-- Событие 11 -->
							<div class="prog group" data-date="16" style="display: none;" id="prog11" onClick="openmob(11);">
								<div class="event1">
									<time>09:00 – 10:00</time>
									<div class="eshort">Регистрация</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									<div class="psd"><h4><time>09:00 – 10:00</time></h4></div>
								</div>
								<div class="event">
									<div class="eshort">Регистрация</div>
								</div>
								<div class="event2" id="event11">
									<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									<div class="psd"><span>16</span> декабря<br /></div>
								</div>
								<div class="dt">
									<span>16</span><br />декабря
								</div>
							</div>
							<!-- Событие 13 -->
							<!--<div class="prog group" data-date="16" style="display: none;" id="prog13" onClick="openmob(13);">
								<div class="event1">
									<time>09:50 – 10:00</time>
									<div class="eshort">Приветственное слово</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/present.png" /></div>
									<div class="psd"><h4><time>09:50 – 10:00</time></h4></div>
								</div>
								<div class="event">
									<div class="eshort">Приветственное слово</div>
								</div>
								<div class="event2" id="event13">
									<div class="psimg"><img align="left" src="images/present.png" /></div>
									<div class="psd"><span>16</span> декабря<br /></div>
								</div>
								<div class="dt">
									<span>16</span><br />декабря
								</div>
							</div>-->
							<div class="progspace"></div>
							<div class="mktime"><?=$mktimes[1];?></div>
							<h2 data-date="16" style="display: none;">Первый блок мастер-классов</h2>
							<!-- Мастер-классы 1 -->
							<?
							foreach ($mks[1] as $m => $mk)
								{
								?>
								<div class="prog group" data-date="16" style="display: none;" data-group="1" data-similar="<?=$mk["similar"];?>" data-sp="<?=$mk["speaker"];?>" data-mk="<?=$m;?>" id="prog<?=$m;?>" onClick="openmob(<?=$m + 20;?>);">
									<div class="event1">
										<!--<time><?=$mktimes[1];?></time>-->
										<div class="psimg"><img align="left" src="images/<?=$speakers[$mk["speaker"]]["image"];?>" /></div>
										<div class="eshort2"><h4><?=$speakers[$mk["speaker"]]["name"];?></h4></div>
										<div class="eshort"><?=$mk["theme"];?><div><h5>Мастер-класс до обеда, часть 1</h5></div></div>
										<div class="psd">
											<button type="button">Мастер-класс</button>
											<div class="mkcount"><?if ($mkcounts[$m] > 0) {?>Осталось <?=$mkcounts[$m];?> <?=places($mkcounts[$m]);?><?}else{?>Мест больше нет<?}?></div>
										</div>
										<div class="igo"><?if (($mkcounts[$m]) > 0) {?><input type="checkbox" name="mk_<?=$m;?>" id="mk_<?=$m;?>" onClick="igo(1, <?=$m;?>, '<strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?>', this.checked, '<?=$mk["similar"];?>');" /><label id="lf_<?=$m;?>" for="mk_<?=$m;?>">Хочу пойти</label><?}else{?>Мест больше нет<?}?></div>
									</div>
									<div class="pspeaker group">
										<div class="psimg"><img align="left" src="images/<?=$speakers[$mk["speaker"]]["image"];?>" /></div>
										<div class="psd"><h4><?=str_replace(" ", "<br />", $speakers[$mk["speaker"]]["name"]);?></h4>
											<!--<h5><?=$halls[$mk["hall"]];?></h5>-->
										</div>
									</div>
									<div class="event">
										<!--<time><?=$mktimes[1];?></time>--><button type="button">Мастер-класс</button><div class="mkcount"><?if ($mkcounts[$m] > 0) {?>Осталось <?=$mkcounts[$m];?> <?=places($mkcounts[$m]);?><?}else{?>Мест больше нет<?}?></div>
										<img id="imgopen<?=$m;?>" src="images/arr_open.png" onClick="$(this).fadeOut(200); $('#imgclose<?=$m;?>').fadeIn(200); $(this).parent().find('.elong').slideDown(300);" />
										<img id="imgclose<?=$m;?>" src="images/arr_close.png" style="display: none;" onClick="$(this).fadeOut(200); $('#imgopen<?=$m;?>').fadeIn(200); $(this).parent().find('.elong').slideUp(300);" />
										<div class="eshort"><?=$mk["theme"];?><div><h5>Мастер-класс до обеда, часть 1</h5></div></div>
										<div class="elong"><?=nl2br($mk["themedesc"]);?></div>
										<div class="igo"><?if (($mkcounts[$m]) > 0) {?><input type="checkbox" name="mk<?=$m;?>" id="mk<?=$m;?>" onClick="igo(1, <?=$m;?>, '<strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?>', this.checked, '<?=$mk["similar"];?>');" /><label id="lf<?=$m;?>" for="mk<?=$m;?>">Хочу пойти</label><?}else{?>Мест больше нет<?}?></div>
									</div>
									<div class="event2" id="event<?=$m + 20;?>">
										<hr class="blue" />
										<div class="elong2"><?=nl2br($mk["themedesc"]);?></div>
									</div>
									<div class="dt">
										<span>16</span><br />декабря
									</div>
								</div>
								<?
							}
							?>
							<div class="progspace"></div>
							<!-- Событие 14 -->
							<div class="prog group" data-date="16" style="display: none;" id="prog14" onClick="openmob(14);">
								<div class="event1">
									<time>11:30 - 12:00</time>
									<div class="eshort">Кофе-брейк</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									<div class="psd"><h4><time>11:30 - 12:00</time></h4></div>
								</div>
								<div class="event">
									<div class="eshort">Кофе-брейк</div>
								</div>
								<div class="event2" id="event14">
									<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									<div class="psd"><span>16</span> декабря<br /></div>
								</div>
								<div class="dt">
									<span>16</span><br />декабря
								</div>
							</div>
							<div class="progspace"></div>
							<div class="mktime"><?=$mktimes[2];?></div>
							<?
							foreach ($mks[2] as $m => $mk)
								{
								?>
								<div class="prog group" data-date="16" style="display: none;" data-group="1" data-similar="<?=$mk["similar"];?>" data-sp="<?=$mk["speaker"];?>" data-mk="<?=$m;?>" id="prog<?=$m;?>" onClick="openmob(<?=$m + 20;?>);">
									<div class="event1">
										<!--<time><?=$mktimes[2];?></time>-->
										<div class="psimg"><img align="left" src="images/<?=$speakers[$mk["speaker"]]["image"];?>" /></div>
										<div class="eshort2"><h4><?=$speakers[$mk["speaker"]]["name"];?></h4></div>
										<div class="eshort"><?=$mk["theme"];?><div><h5>Мастер-класс до обеда, часть 2</h5></div></div>
										<div class="psd">
											<button type="button">Мастер-класс</button>
											<div class="mkcount"><?if ($mkcounts[$m] > 0) {?>Осталось <?=$mkcounts[$m];?> <?=places($mkcounts[$m]);?><?}else{?>Мест больше нет<?}?></div>
										</div>
										<div class="igo"><?if (($mkcounts[$m]) > 0) {?><input type="checkbox" name="mk_<?=$m;?>" id="mk_<?=$m;?>" onClick="igo(1, <?=$m;?>, '<strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?>', this.checked, '<?=$mk["similar"];?>');" /><label id="lf_<?=$m;?>" for="mk_<?=$m;?>">Хочу пойти</label><?}else{?>Мест больше нет<?}?></div>
									</div>
									<div class="pspeaker group">
										<div class="psimg"><img align="left" src="images/<?=$speakers[$mk["speaker"]]["image"];?>" /></div>
										<div class="psd"><h4><?=str_replace(" ", "<br />", $speakers[$mk["speaker"]]["name"]);?></h4>
											<!--<h5><?=$halls[$mk["hall"]];?></h5>-->
										</div>
									</div>
									<div class="event">
										<!--<time><?=$mktimes[2];?></time>--><button type="button">Мастер-класс</button><div class="mkcount"><?if ($mkcounts[$m] > 0) {?>Осталось <?=$mkcounts[$m];?> <?=places($mkcounts[$m]);?><?}else{?>Мест больше нет<?}?></div>
										<img id="imgopen<?=$m;?>" src="images/arr_open.png" onClick="$(this).fadeOut(200); $('#imgclose<?=$m;?>').fadeIn(200); $(this).parent().find('.elong').slideDown(300);" />
										<img id="imgclose<?=$m;?>" src="images/arr_close.png" style="display: none;" onClick="$(this).fadeOut(200); $('#imgopen<?=$m;?>').fadeIn(200); $(this).parent().find('.elong').slideUp(300);" />
										<div class="eshort"><?=$mk["theme"];?><div><h5>Мастер-класс до обеда, часть 2</h5></div></div>
										<div class="elong"><?=nl2br($mk["themedesc"]);?></div>
										<div class="igo"><?if (($mkcounts[$m]) > 0) {?><input type="checkbox" name="mk<?=$m;?>" id="mk<?=$m;?>" onClick="igo(1, <?=$m;?>, '<strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?>', this.checked, '<?=$mk["similar"];?>');" /><label id="lf<?=$m;?>" for="mk<?=$m;?>">Хочу пойти</label><?}else{?>Мест больше нет<?}?></div>
									</div>
									<div class="event2" id="event<?=$m + 20;?>">
										<hr class="blue" />
										<div class="elong2"><?=nl2br($mk["themedesc"]);?></div>
									</div>
									<div class="dt">
										<span>16</span><br />декабря
									</div>
								</div>
								<?
							}
							?>
							<div class="progspace"></div>
							<!-- Событие 16 -->
							<div class="prog group" data-date="16" style="display: none;" id="prog16" onClick="openmob(16);">
								<div class="event1">
									<time>13:30 - 14:30</time>
									<div class="eshort">Обед</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/lunch.png" /></div>
									<div class="psd"><h4><time>13:30 - 14:30</time></h4></div>
								</div>
								<div class="event">
									<div class="eshort">Обед</div>
								</div>
								<div class="event2" id="event16">
									<div class="psimg"><img align="left" src="images/lunch.png" /></div>
									<div class="psd"><span>16</span> декабря<br /></div>
								</div>
								<div class="dt">
									<span>16</span><br />декабря
								</div>
							</div>
							<div class="progspace"></div>
							<div class="mktime"><?=$mktimes[3];?></div>
							<h2 data-date="16" style="display: none;">Второй блок мастер-классов</h2>
							<?
							foreach ($mks[3] as $m => $mk)
								{
								?>
								<div class="prog group" data-date="16" style="display: none;" data-group="2" data-similar="<?=$mk["similar"];?>" data-sp="<?=$mk["speaker"];?>" data-mk="<?=$m;?>" id="prog<?=$m;?>" onClick="openmob(<?=$m + 20;?>);">
									<div class="event1">
										<!--<time><?=$mktimes[3];?></time>-->
										<div class="psimg"><img align="left" src="images/<?=$speakers[$mk["speaker"]]["image"];?>" /></div>
										<div class="eshort2"><h4><?=$speakers[$mk["speaker"]]["name"];?></h4></div>
										<div class="eshort"><?=$mk["theme"];?><div><h5>Мастер-класс после обеда, часть 1</h5></div></div>
										<div class="psd">
											<button type="button">Мастер-класс</button>
											<div class="mkcount"><?if ($mkcounts[$m] > 0) {?>Осталось <?=$mkcounts[$m];?> <?=places($mkcounts[$m]);?><?}else{?>Мест больше нет<?}?></div>
										</div>
										<div class="igo"><?if (($mkcounts[$m]) > 0) {?><input type="checkbox" name="mk_<?=$m;?>" id="mk_<?=$m;?>" onClick="igo(2, <?=$m;?>, '<strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?>', this.checked, '<?=$mk["similar"];?>');" /><label id="lf_<?=$m;?>" for="mk_<?=$m;?>">Хочу пойти</label><?}else{?>Мест больше нет<?}?></div>
									</div>
									<div class="pspeaker group">
										<div class="psimg"><img align="left" src="images/<?=$speakers[$mk["speaker"]]["image"];?>" /></div>
										<div class="psd"><h4><?=str_replace(" ", "<br />", $speakers[$mk["speaker"]]["name"]);?></h4>
											<!--<h5><?=$halls[$mk["hall"]];?></h5>-->
										</div>
									</div>
									<div class="event">
										<!--<time><?=$mktimes[3];?></time>--><button type="button">Мастер-класс</button><div class="mkcount"><?if ($mkcounts[$m] > 0) {?>Осталось <?=$mkcounts[$m];?> <?=places($mkcounts[$m]);?><?}else{?>Мест больше нет<?}?></div>
										<img id="imgopen<?=$m;?>" src="images/arr_open.png" onClick="$(this).fadeOut(200); $('#imgclose<?=$m;?>').fadeIn(200); $(this).parent().find('.elong').slideDown(300);" />
										<img id="imgclose<?=$m;?>" src="images/arr_close.png" style="display: none;" onClick="$(this).fadeOut(200); $('#imgopen<?=$m;?>').fadeIn(200); $(this).parent().find('.elong').slideUp(300);" />
										<div class="eshort"><?=$mk["theme"];?><div><h5>Мастер-класс после обеда, часть 1</h5></div></div>
										<div class="elong"><?=nl2br($mk["themedesc"]);?></div>
										<div class="igo"><?if (($mkcounts[$m]) > 0) {?><input type="checkbox" name="mk<?=$m;?>" id="mk<?=$m;?>" onClick="igo(2, <?=$m;?>, '<strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?>', this.checked, '<?=$mk["similar"];?>');" /><label id="lf<?=$m;?>" for="mk<?=$m;?>">Хочу пойти</label><?}else{?>Мест больше нет<?}?></div>
									</div>
									<div class="event2" id="event<?=$m + 20;?>">
										<hr class="blue" />
										<div class="elong2"><?=nl2br($mk["themedesc"]);?></div>
									</div>
									<div class="dt">
										<span>16</span><br />декабря
									</div>
								</div>
								<?
							}
							?>
							<div class="progspace"></div>
							<!-- Событие 17 -->
							<div class="prog group" data-date="16" style="display: none;" id="prog17" onClick="openmob(17);">
								<div class="event1">
									<time>16:00 - 16:30</time>
									<div class="eshort">Кофе-брейк</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									<div class="psd"><h4><time>16:00 - 16:30</time></h4></div>
								</div>
								<div class="event">
									<div class="eshort">Кофе-брейк</div>
								</div>
								<div class="event2" id="event17">
									<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									<div class="psd"><span>16</span> декабря<br /></div>
								</div>
								<div class="dt">
									<span>16</span><br />декабря
								</div>
							</div>
							<div class="progspace"></div>
							<div class="mktime"><?=$mktimes[4];?></div>
							<?
							foreach ($mks[4] as $m => $mk)
								{
								?>
								<div class="prog group" data-date="16" style="display: none;" data-group="2" data-similar="<?=$mk["similar"];?>" data-sp="<?=$mk["speaker"];?>" data-mk="<?=$m;?>" id="prog<?=$m;?>" onClick="openmob(<?=$m + 20;?>);">
									<div class="event1">
										<!--<time><?=$mktimes[4];?></time>-->
										<div class="psimg"><img align="left" src="images/<?=$speakers[$mk["speaker"]]["image"];?>" /></div>
										<div class="eshort2"><h4><?=$speakers[$mk["speaker"]]["name"];?></h4></div>
										<div class="eshort"><?=$mk["theme"];?><div><h5>Мастер-класс после обеда, часть 2</h5></div></div>
										<div class="psd">
											<button type="button">Мастер-класс</button>
											<div class="mkcount"><?if ($mkcounts[$m] > 0) {?>Осталось <?=$mkcounts[$m];?> <?=places($mkcounts[$m]);?><?}else{?>Мест больше нет<?}?></div>
										</div>
										<div class="igo"><?if (($mkcounts[$m]) > 0) {?><input type="checkbox" name="mk_<?=$m;?>" id="mk_<?=$m;?>" onClick="igo(2, <?=$m;?>, '<strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?>', this.checked, '<?=$mk["similar"];?>');" /><label id="lf_<?=$m;?>" for="mk_<?=$m;?>">Хочу пойти</label><?}else{?>Мест больше нет<?}?></div>
									</div>
									<div class="pspeaker group">
										<div class="psimg"><img align="left" src="images/<?=$speakers[$mk["speaker"]]["image"];?>" /></div>
										<div class="psd"><h4><?=str_replace(" ", "<br />", $speakers[$mk["speaker"]]["name"]);?></h4>
											<!--<h5><?=$halls[$mk["hall"]];?></h5>-->
										</div>
									</div>
									<div class="event">
										<!--<time><?=$mktimes[4];?></time>--><button type="button">Мастер-класс</button><div class="mkcount"><?if ($mkcounts[$m] > 0) {?>Осталось <?=$mkcounts[$m];?> <?=places($mkcounts[$m]);?><?}else{?>Мест больше нет<?}?></div>
										<img id="imgopen<?=$m;?>" src="images/arr_open.png" onClick="$(this).fadeOut(200); $('#imgclose<?=$m;?>').fadeIn(200); $(this).parent().find('.elong').slideDown(300);" />
										<img id="imgclose<?=$m;?>" src="images/arr_close.png" style="display: none;" onClick="$(this).fadeOut(200); $('#imgopen<?=$m;?>').fadeIn(200); $(this).parent().find('.elong').slideUp(300);" />
										<div class="eshort"><?=$mk["theme"];?><div><h5>Мастер-класс после обеда, часть 2</h5></div></div>
										<div class="elong"><?=nl2br($mk["themedesc"]);?></div>
										<div class="igo"><?if (($mkcounts[$m]) > 0) {?><input type="checkbox" name="mk<?=$m;?>" id="mk<?=$m;?>" onClick="igo(2, <?=$m;?>, '<strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?>', this.checked, '<?=$mk["similar"];?>');" /><label id="lf<?=$m;?>" for="mk<?=$m;?>">Хочу пойти</label><?}else{?>Мест больше нет<?}?></div>
									</div>
									<div class="event2" id="event<?=$m + 20;?>">
										<hr class="blue" />
										<div class="elong2"><?=nl2br($mk["themedesc"]);?></div>
									</div>
									<div class="dt">
										<span>16</span><br />декабря
									</div>
								</div>
								<?
							}
							?>
							<!--<div class="progspace"></div>-->
							<!-- Событие 18 -->
							<!--<div class="prog group" data-date="16" style="display: none;" id="prog18" onClick="openmob(18);">
								<div class="event1">
									<time>18:00 - 18:10</time>
									<div class="eshort">Торжественное завершение Рождественских встреч</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/ny.png" /></div>
									<div class="psd"><h4><time>18:00 - 18:10</time></h4></div>
								</div>
								<div class="event">
									<div class="eshort">Торжественное завершение Рождественских встреч</div>
								</div>
								<div class="event2" id="event18">
									<div class="psimg"><img align="left" src="images/ny.png" /></div>
									<div class="psd"><span>16</span> декабря<br /></div>
								</div>
								<div class="dt">
									<span>16</span><br />декабря
								</div>
							</div>-->
						</div>
						<div class="prog1">Осталось мест на лекцию <?=$mkcounts[0];?></div>
					</div>
					<br />
					<div class="progtabbs group">
						<div class="progline"></div>
						<div class="progtabs group">
							<div class="progtab progtab1 sel" onClick="$('.prog2').fadeOut(200); $('.prog1').fadeIn(200); $('.progtab.sel').removeClass('sel'); $('.progtab1').addClass('sel'); $('div[data-date=16]').fadeOut(300); $('h2[data-date=16]').fadeOut(300); $('.progspace').fadeOut(300); $('div[data-date=15]').fadeIn(300); $('h2[data-date=15]').fadeIn(300); $('.mktime').css('display', 'none'); $('html,body').animate({scrollTop: $('.main').offset().top - 20}, 500);">15 декабря</div>
							<div class="progtab progtab2" onClick="$('.prog1').fadeOut(200); $('.prog2').fadeIn(200); $('.progtab.sel').removeClass('sel'); $('.progtab2').addClass('sel'); $('div[data-date=15]').fadeOut(300); $('h2[data-date=15]').fadeOut(300); $('.progspace').fadeIn(300); $('div[data-date=16]').fadeIn(300); $('h2[data-date=16]').fadeIn(300); $('.mktime').css('display', 'table'); $('html,body').animate({scrollTop: $('.main').offset().top - 20}, 500); if (pop1 == 0) {$('.mask').fadeIn(300); $('#popup').fadeIn(300);}">16 декабря</div>
						</div>
						<div class="progline"></div>
					</div>
					<div class="spec" onClick="$('.mask').fadeIn(300); $('#special').fadeIn(300);">Специальные условия проживания</div>
					<div class="spec_desc" id="special">
						<div class="close" onClick="$('.mask').fadeOut(300); $('.spec_desc').fadeOut(300);"><img src="images/close.png" /></div>
						<h2>Специальные условия проживания</h2>
						<?=nl2br("Участники мероприятия могут забронировать номера в отеле Corinthia Hotel St. Petersburg по специальным ценам::

<strong>Одноместное размещение (1 ночь) – 6 500 р.</strong>
<strong>Двухместное размещение (1 ночь) – 8 500 р.</strong>

Для размещения брони, пожалуйста, <a href='Анкета_Коринтия_РВ.docx'><span>заполните эту анкету</span></a> и отправьте на e-mail адрес <a href='mailto:Natalia.Postnikova@ormco.com'>Natalia.Postnikova@ormco.com</a> (Наталья)</p>
");
						?>
					</div>
					<a name="new"></a>
					<!--<div class="ny1"><img src="images/ny1.png" /></div>-->
					<br />
					<!--<div class="kavo"><img src="images/kavo.png" /></div>
					<br />
					<br />-->
					<div><button type="button" onClick="<?if ($mkcounts[0] > 0) {?>if (chkigos()) {fixbody(); $('.mask').fadeIn(300); $('.openform').fadeIn(300);}<?}else{?>noplaces();<?}?>">Регистрация</button></div>
					<br />
					<!--<img src="images/kavo.png" />
					<br />
					<div class="gallery">
						<div class="gal_slider1 group">
							<div class="gal_slide"><img src="images/ph2016.jpg" /></div>
							<div class="gal_slide"><img src="images/4SWEeqzEmj4.jpg" /></div>
							<div class="gal_slide"><img src="images/7o47zHo3uKA.jpg" /></div>
							<div class="gal_slide"><img src="images/c2PjPgWLSdM.jpg" /></div>
							<div class="gal_slide"><img src="images/cwJLRs3fRJE.jpg" /></div>
							<div class="gal_slide"><img src="images/IMG_8198.JPG" /></div>
							<div class="gal_slide"><img src="images/IMG_8867.JPG" /></div>
							<div class="gal_slide"><img src="images/IMG_8871.JPG" /></div>
							<div class="gal_slide"><img src="images/MnWQ5XrkK7U.jpg" /></div>
							<div class="gal_slide"><img src="images/nX78ZKohlJw.jpg" /></div>
							<div class="gal_slide"><img src="images/wOACH2SmskI.jpg" /></div>
							<div class="gal_slide"><img src="images/Yk6JEzH2qRs.jpg" /></div>
						</div>
						<div class="gal_navs">
							<div class="gal_nav" onClick="gallery_left1();"><img src="images/arr_left.png" /></div>
							<div class="gal_nav" onClick="gallery_right1();"><img src="images/arr_right.png" /></div>
						</div>
					</div>
					<br />-->
					<!-- Спикеры -->
					<a name="speakers"></a>
					<h1>Спикеры</h1>
					<hr class="mobile" />
					<div class="speakersdiv">
						<div class="speakers group">
							<?
							$scnt = 0;
							foreach ($speakers as $speaker)
								{
								$scnt++;
								$arr_name = explode(" ", $speaker["name"]);
								?>
								<div class="speaker" onClick="$('.mask').fadeIn(300); $('#spdesc<?=$scnt;?>').fadeIn(300);">
									<img src="images/<?=$speaker["image"];?>" />
									<div class="speakerdata">
										<h4><?=$arr_name[0];?><br /><?=$arr_name[1];?> <?=$arr_name[2];?></h4>
									</div>
								</div>
								<div class="speaker_desc" id="spdesc<?=$scnt;?>">
									<div class="close" onClick="$('.mask').fadeOut(300); $('.speaker_desc').fadeOut(300);"><img src="images/close.png" /></div>
									<h4><?=$speaker["name"];?></h4>
									<?=$speaker["about"];?>
								</div>
								<?
								if ($scnt == 1)
									{
									?><hr class="between" /><?
								}
							}
							?>
						</div>
						<div class="arrleft" onClick="speaker_left();"><img src="images/arr_left.png" /></div>
						<div class="arrright" onClick="speaker_right();"><img src="images/arr_right.png" /></div>
					</div>
					<div class="spbullets group">
						<?
						$scnt = 0;
						foreach ($speakers as $speaker)
							{
							$scnt++;
							?>
							<div id="spbul_<?=$scnt;?>" class="spbullet<?if ($scnt == 1) {?> active<?}?>" onClick="speaker_go(<?=$scnt;?>);"></div>
							<?
						}
						?>
					</div>
					<!-- Стоимость -->
					<!--<div class="lefter"><img src="images/left.png" /></div>
					<div class="righter"><img src="images/right.png" /></div>-->
					<a name="price"></a>
					<h1>Стоимость участия <sup>*</sup></h1>
					<div class="txt">Стоимость участия может измениться в зависимости от даты оплаты. Просим вас не затягивать и позаботиться об оплате мероприятия заблаговременно</div>
					<h2>Скидки</h2>
					<h5><sup>*</sup> Ординаторам предоставляется скидка <span class="red">40%</span> при предъявлении подтверждающего документа</h5>
					<h5><sup>*</sup> Участникам Школы ортодонтии предоставляется скидка <span class="red">50%</span></h5>
					<h5><sup>*</sup> Членам Профессионального общества ортодонтов предоставляется скидка <span class="red">15%</span> при предъявлении подтверждающего документа</h5>
					<br /><br />
					<div class="prices group<?if (date("Y-m-d") >= "2118-11-15") {?> opa<?}?>">
						<div class="pr_title">При оплате до 15 ноября</div>
						<div class="price">12 000 р.</div>
						<div class="button"><button type="button"<?if (date("Y-m-d") < "2118-11-15") {?> onClick="$('#regularprice').val(12000); $('#finalprice').text('12 000 р.'); <?if ($mkcounts[0] > 0) {?>if (chkigos()) {fixbody(); $('.mask').fadeIn(300); $('.openform').fadeIn(300);}<?}else{?>noplaces();<?}?>"<?}?>>Регистрация</button></div>
					</div>
					<div class="prices group<?if (date("Y-m-d") < "2118-11-15") {?> opa<?}?>">
						<div class="pr_title">При оплате после 15 ноября</div>
						<div class="price">14 000 р.</div>
						<div class="button"><button type="button"<?if (date("Y-m-d") >= "2118-11-15") {?> onClick="$('#regularprice').val(14000); $('#finalprice').text('14 000 р.'); <?if ($mkcounts[0] > 0) {?>if (chkigos()) {fixbody(); $('.mask').fadeIn(300); $('.openform').fadeIn(300);}<?}else{?>noplaces();<?}?>"<?}?>>Регистрация</button></div>
					</div>
					<br />
					<!-- Место -->
					<a name="place"></a>
					<h1>Место проведения</h1>
					<h2>Отель «Коринтия»</h2>
					<h3>Адрес: г. Санкт-Петербург, Невский пр., д. 57</h3>
					<div class="places group">
						<div class="placeline"></div>
						<div class="placetabs group">
							<div class="placetab sel" onClick="$('.placetab.sel').removeClass('sel'); $(this).addClass('sel'); $('.map').fadeOut(300); $('.gallery').fadeIn(300);">Галерея</div>
							<div class="placetab" onClick="$('.placetab.sel').removeClass('sel'); $(this).addClass('sel'); $('.gallery').fadeOut(300); $('.map').fadeIn(300); initialize_map();">На карте</div>
						</div>
						<div class="placeline"></div>
					</div>
					<div class="gallery">
						<div class="gal_slider group">
							<div class="gal_slide"><img src="images/building.jpg" /></div>
							<div class="gal_slide"><img src="images/building1.jpg" /></div>
							<div class="gal_slide"><img src="images/building2.jpg" /></div>
							<div class="gal_slide"><img src="images/building3.jpg" /></div>
							<div class="gal_slide"><img src="images/building4.jpg" /></div>
							<div class="gal_slide"><img src="images/building5.jpg" /></div>
							<div class="gal_slide"><img src="images/building6.jpg" /></div>
						</div>
						<div class="gal_navs">
							<div class="gal_nav" onClick="gallery_left();"><img src="images/arr_left.png" /></div>
							<div class="gal_nav" onClick="gallery_right();"><img src="images/arr_right.png" /></div>
						</div>
					</div>
					<div class="map"><div id="map"></div></div>
					<a name="form"></a>
					<h1>Регистрация</h1>
					<div class="formmiddle">
						<div class="formtable group">
							<img src="images/face.jpg" />
							<div class="formtxt">
								По всем вопросам вы можете связаться с менеджером мероприятия Натальей Постниковой: +7 (812) 324-42-60, <a href="mailto:Natalia.Postnikova@ormco.com">Natalia.Postnikova@ormco.com</a>
							</div>
						</div>
						<button type="button" onClick="<?if ($mkcounts[0] > 0) {?>if (chkigos()) {fixbody(); $('.mask').fadeIn(300); $('.openform').fadeIn(300);}<?}else{?>noplaces();<?}?>">Зарегистрироваться</button>
					</div>
				</div>
			</div>
		</div>
		<div class="openform">
			<div class="openform_">
				<div class="newform formbox1">
					<div class="formbox_close" onClick="unfixbody(); $('.mask').fadeOut(300); $('.openform').fadeOut(300);"><span class="shape1"></span><span class="shape2"></span></div>
					<h2>Регистрация</h2>
					<h4>Введите Ваш email. Если он уже есть в нашей базе, регистрация пройдет быстрее</h4>
					<input id="f_email" name="f_email" value="<?=$_SESSION["f_email"];?>" />
					<button type="button" onClick="chk_f_email();">Далее</button>
					<div class="f_anim">
						<div class="at">@</div>
						<div class="arr arr1">←</div>
						<div class="dash arr2"></div>
						<div class="dash arr3"></div>
						<div class="dash arr4"></div>
						<div class="arr arr5">→</div>
						<div class="db">
							<div class="db1"></div>
							<div class="db2"></div>
							<div class="db3"></div>
							<div class="db4"></div>
							<div class="db5"></div>
							<div class="db6"></div>
							<div class="db7"></div>
							<div class="db8"></div>
							<div class="db9"></div>
						</div>
						<div class="f_desc">Проверяем email на наличие в базе данных</div>
					</div>
					<div class="exists">Человек с данным Email уже зарегистрировался на данное мероприятие</div>
				</div>
				<div class="newform formbox2">
					<div class="formbox_close" onClick="unfixbody(); $('.mask').fadeOut(300); $('.openform').fadeOut(300);"><span class="shape1"></span><span class="shape2"></span></div>
					<h2>Вход в систему</h2>
					<h4>Email найден! Пожалуйста, введите пароль от вашей учетной записи Ormco (сайты ormco.ru или orthodontia.ru), чтобы зарегистрироваться на семинар</h4>
					<div class="newform_field group">
						<div class="newform_ttl">Логин (Email):</div>
						<div class="newform_fld"><input id="e_email" name="e_email" value="<?=$_SESSION["f_email"];?>" /></div>
					</div>
					<div class="newform_field group">
						<div class="newform_ttl">Пароль:</div>
						<div class="newform_fld"><input type="password" id="e_pass" name="e_pass" value="" /><img onClick="chg_passstate();" src="images/eye.svg" /></div>
					</div>
					<div class="newform_link"><span onClick="remember_pass();">Вспомнить пароль</span></div>
					<div id="rp_ok" class="form_msg">Письмо с дальнейшими инструкциями по восстановлению пароля отправлены на указанный email</div>
					<div id="rp_fail" class="form_msg"></div>
					<div class="newform_field group">
						<div class="newform_ttl"></div>
						<div class="newform_fld"><button type="button" id="entry_but" onClick="chk_entry();">Войти</button></div>
					</div>
					<div class="exists">Человек с данным Email уже зарегистрировался на данное мероприятие</div>
					<div class="wrong">Пароль или логин указаны неверно</div>
				</div>
				<div class="newform formbox3">
					<div class="formbox_close" onClick="unfixbody(); $('.mask').fadeOut(300); $('.openform').fadeOut(300);"><span class="shape1"></span><span class="shape2"></span></div>
					<h2>Регистрация на мероприятие</h2>
					<div class="notfound red hidden">Пользователь не найден в базе данных. Вы можете зарегистрироваться на мероприятие, заполнив форму ниже:</div>
					<form action="https://orthodontia.ru/emarket/lporderSmart/" method="post" onSubmit="return chkform(1);" enctype="multipart/form-data">
					<input type="hidden" name="regularprice" id="regularprice" value="<?if (date("Y-m-d") < "2118-11-15") {?>12000<?}else{?>14000<?}?>" />
					<input type="hidden" name="mclasses1" id="mclasses1" value="" />
					<input type="hidden" name="mclasses2" id="mclasses2" value="" />
					<input type="hidden" name="mcl1" id="mcl1" value="" />
					<input type="hidden" name="mcl2" id="mcl2" value="" />
					<input type="hidden" name="p1" id="p1" value="" />
					<input type="hidden" name="p2" id="p2" value="" />
					<input type="hidden" name="file" id="file" value="" />
					<input type="hidden" name="events" id="events" value="<?=$event_id;?>" />
					<div class="partbuts">
						<!--<div class="partbut active partbut1" onClick="showpart(2);">Личные данные</div> → <div class="partbut partbut2" onClick="showpart(2);">Способ оплаты</div>-->
						<h4 class="partbut1">Личные данные</h4>
						<h4 class="partbut2 hidden">Способ оплаты</h4>
					</div>
					<hr class="hr">
					<div class="part1">
						<div class="newform_field group">
							<div class="newform_ttl">Email <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="email" id="email" /></div>
						</div>
						<div class="newform_field group phide">
							<div class="newform_ttl"></div>
							<div class="newform_fld small">На этот email будет отправлено подтверждение регистрации и ссылка для активации вашей учетной записи Ormco</div>
						</div>
						<div class="newform_field group phide">
							<div class="newform_ttl">Пароль <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="password" size="30" name="password" id="password" /></div>
						</div>
						<div class="newform_field group phide">
							<div class="newform_ttl">Пароль еще раз <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="password" size="30" name="password2" id="password2" /></div>
						</div>
						<br />
						<div class="newform_field group">
							<div class="newform_ttl">Фамилия <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="lname" id="lname" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Имя <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="fname" id="fname" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Отчество <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="father_name" id="father_name" /></div>
						</div>
						<div class="newform_field group" id="birthday">
							<div class="newform_ttl">Дата рождения<sup>*</sup></div>
							<div class="newform_fld"><input placeholder="" class="date1 ness" type="text" size="20" name="bd" id="bd" readonly /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Телефон <sup>*</sup></div>
							<div class="newform_fld"><input placeholder="+7 (987) 1234567" class="short ness" type="text" size="25" name="phone" id="phone" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl big">Специальность для получения баллов по НМО<sup>*</sup></div>
							<div class="newform_fld"><select name="spec" id="spec" class="ness">
									<option value="">-= выберите Вашу специальность =-</option>
									<option value="Ортодонтия">Ортодонтия</option>
									<option value="Стоматология терапевтическая">Стоматология терапевтическая</option>
									<option value="Стоматология общей практики">Стоматология общей практики</option>
								</select>
							</div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Страна<sup>*</sup></div>
							<div class="newform_fld"><select name="country" id="country" class="ness" onChange="if (this.value == 'Россия') {$('.region_field').fadeIn(300);}else{$('.region_field').fadeOut(300);}">
									<option value="Россия">Россия</option>
									<option value="Азербайджан">Азербайджан</option>
									<option value="Армения">Армения</option>
									<option value="Белоруссия">Белоруссия</option>
									<option value="Грузия">Грузия</option>
									<option value="Украина">Украина</option>
									<option value="Другая">Другая</option>
								</select>
							</div>
						</div>
							<div class="newform_field group region_field">
								<div class="newform_ttl">Регион <sup>*</sup></div>
								<div class="newform_fld"><select name="region" id="region" class="ness">
									<option value="">-= выберите регион =-</option>
									<option value="9870">Москва и МО</option>
									<option value="9871">Санкт-Петербург и ЛО</option>
									<option value="9872">Северо-Западный</option>
									<option value="9873">Центральный</option>
									<option value="9874">Сибирский</option>
									<option value="9875">Приволжский</option>
									<option value="9876">Южный</option>
									<option value="9877">Северо-Кавказский</option>
									<option value="9878">Уральский</option>
									<option value="9879">Дальневосточный</option>
									<option value="16761">Не Россия</option>
								</select></div>
							</div>
						<div class="newform_field group">
							<div class="newform_ttl">Город<sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="city" id="city" /></div>
						</div>
						<div class="newform_field group" id="speciality">
							<div class="newform_ttl">Кто Вы? <sup>*</sup></div>
							<div class="newform_fld"><select name="who" id="who" class="ness">
									<option value="">Кто Вы?</option>
									<option value="Ортодонт">Ортодонт</option>
									<option value="Хирург">Хирург</option>
									<option value="Другая специализация">Другая специализация</option>
									<option value="Не врач">Не врач</option>
								</select>
							</div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Клиника/ВУЗ<sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="company" id="company" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl"></div>
							<div class="newform_fld"><button type="button" id="entry_but" onClick="showpart(2);">Далее</button></div>
						</div>
					</div>
					<div class="part2 hidden">
						<h3>Выберите скидку. Обратите внимание: скидка будет направлена менеджеру Ormco для подтверждения</h3>
						<div class="newform_field group">
							<div class="newform_flda"><input type="checkbox" class="dscnt upl" name="poo" id="poo" value="Член ПОО" onClick="$('.newform_upload').fadeIn(300); $('#uploaded').fadeIn(300); setdiscount(15, 'poo', this.checked);" /> <label for="poo">Являюсь членом Профессионального общества ортодонтов</label></div>
						</div>
						<div class="newform_field group">
							<div class="newform_flda"><input type="checkbox" class="dscnt" name="school" id="school" value="Действующий участник Школы ортодонтии" onClick="$('.newform_upload').fadeOut(300); $('#uploaded').fadeOut(300); setdiscount(50, 'school', this.checked);" /> <label for="school">Действующий участник Школы ортодонтии</label></div>
						</div>
						<div class="newform_field group">
							<div class="newform_flda"><input type="checkbox" class="dscnt upl" name="ordinator" id="ordinator" value="Ординатор" onClick="$('.newform_upload').fadeIn(300); $('#uploaded').fadeIn(300); setdiscount(40, 'ordinator', this.checked);" /> <label for="ordinator">Я ординатор</label></div>
						</div>
						<div class="newform_field newform_upload hidden group">
							<div class="newform_fldu">Загрузить удостоверение <button type="button" class="upload" name="upload" id="upload">Выберите файл</button></div>
						</div>
						<div class="uploaded hidden" id="uploaded">(форматы jpg, gif, png или pdf)</div>
						<div class="discount hidden">
							Ваша скидка <span id="discount">0</span> процентов
						</div>
						<div class="finalprice">
							Стоимость участия <span id="finalprice"><?if (date("Y-m-d") < "2118-11-15") {?>12 000<?}else{?>14 000<?}?> р.</span>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Оплата:</div>
							<div class="newform_fld"><div class="tabbut active tabbut1" onClick="showtab(1);">Как физ. лицо</div><div class="tabbut tabbut2" onClick="showtab(2);">Как юр. лицо</div></div>
						</div>
						<div class="tab1">
							<div class="newform_field group">
								<div class="newform_ttl">Способ оплаты:</div>
								<div class="newform_fld"><select name="payment_id" id="payment_id" class="ness" onChange="if (this.value == 4666) {showtab(2);}">
										<option value="">выберите способ оплаты</option>
										<option value="4663">Квитанция в банк</option>
										<option value="4664" comment="Не позднее, чем за 10 дней до мероприятия!">Наличными в офисе Ormco</option>
										<option value="17688" comment="Приготовьтесь сразу произвести оплату">Онлайн оплата</option>
										<option value="4666">Счет для юр. лиц</option>
									</select>
								</div>
							</div>
						</div>
						<div class="tab2 hidden">
							<div class="newform_field group yurs">
								<div class="newform_ttl">Юр. лицо:</div>
								<div class="newform_fld"><select name="yur" id="yur" class="ness" disabled>
									</select>
								</div>
							</div>
							<div class="addyur" onClick="addnewyur();">
								<span>+</span> Добавить новое юр. лицо
								<small>Приготовьтесь ввести реквизиты юр. лица</small>
							</div>
							<div class="addyurform addyurform1 hidden">
								<div class="addyur_close" onClick="$('.addyurform').fadeOut(300, function(){$('.newform_reg').fadeIn(300); $('.addyur').fadeIn(300); $('.yurs').fadeIn(300);})"><span class="shape1"></span><span class="shape2"></span></div>
								<div class="newform_field group">
									<div class="newform_ttl">Новое юр. лицо:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="yurname" id="yurname" /><small>Введите юридическое название организации</small></div>
								</div>
								<h4>Контактная информация</h4>
								<div class="newform_field group">
									<div class="newform_ttl">ФИО:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="fio_rukovoditelya" id="fio_rukovoditelya" /><small>ФИО руководителя организации</small></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Должность:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="dolzhnost" id="dolzhnost" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Контактное лицо:</div>
									<div class="newform_fld"><input type="text" size="30" name="contact_person" id="contact_person" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Email<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="yuremail" id="yuremail" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Телефон</div>
									<div class="newform_fld"><input class="short" type="text" size="30" name="phone_number" id="phone_number" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Факс</div>
									<div class="newform_fld"><input class="short" type="text" size="30" name="fax" id="fax" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl"></div>
									<div class="newform_fld"><button type="button" onClick="showyur(2);">Далее</button></div>
								</div>
							</div>
							<div class="addyurform addyurform2 hidden">
								<div class="addyur_back" onClick="showyur(1);"></div>
								<div class="addyur_close" onClick="$('.addyurform').fadeOut(300, function(){$('.newform_reg').fadeIn(300); $('.addyur').fadeIn(300); $('.yurs').fadeIn(300);})"><span class="shape1"></span><span class="shape2"></span></div>
								<div class="newform_field group">
									<div class="newform_ttl">ИНН:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="inn" id="inn" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">КПП:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="kpp" id="kpp" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Рассчетный счет:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="account" id="account" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">БИК:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness short" type="text" size="30" name="bik" id="bik" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Корр. счет:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="bank_account" id="bank_account" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">ОГРН:</div>
									<div class="newform_fld"><input type="text" size="30" name="ogrn" id="ogrn" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Юридический адрес:<sup>*</sup></div>
									<div class="newform_fld"><textarea class="ness" name="legal_address" id="legal_address"></textarea></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Фактический адрес:<sup>*</sup></div>
									<div class="newform_fld"><textarea class="ness" name="defacto_address" id="defacto_address"></textarea></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Почтовый индекс:</div>
									<div class="newform_fld"><input type="text" size="30" name="postal_address" id="postal_address" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl"></div>
									<div class="newform_fld"><button type="button" onClick="addyurajax();">Добавить юр. лицо</button></div>
								</div>
							</div>
						</div>
						<div id="igosel">
							<h4>Лекция 15 декабря (осталось <?=$mkcounts[0];?> мест)</h4>
							<h3>Выбранные мастер-классы</h3>
							<div class="chosen"></div>
						</div>
						<div class="newform_field newform_reg group">
							<div class="newform_ttl"></div>
							<div class="newform_fld"><button id="register">Зарегистрироваться</button></div>
						</div>
						<!--<div class="newform_field group">
							<div class="newform_ttla double">Планирую покупку продукции Ormco в ближайшее время (1-2 месяца)</div>
							<div class="newform_flda"><input type="checkbox" size="30" name="want_to_buy" id="want_to_buy" value="1" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttla double">Принимал(а) участие в образовательных мероприятиях Ormco в течение последнего года</div>
							<div class="flda"><input type="checkbox" size="30" name="how_you_been_edu" id="how_you_been_edu" value="1" /></div>
						</div>
						<div id="igosel">
							<h3>Выбранные мастер-классы</h3>
							<div class="igosel" onClick="$('.igodd').fadeToggle(200); $(this).toggleClass('sel');"><span>Выберите три мастер-класса</span></div>
							<div class="igodd">
								<?
								foreach ($mks as $n => $nks)
									{
									?>
									<div class="igotime igo<?=$n;?>"><?=$mktimes[$n];?></div>
									<?
									foreach ($nks as $m => $mk)
										{
										?>
										<div class="igoset igo<?=$n;?><?if ($mkcounts[$m] <= 0) {?> faded cancelled<?}?>" data-group="<?=$n;?>" data-mk="<?=$m;?>" id="igo_<?=$m;?>"><input type="checkbox" <?if ($mkcounts[$m] <= 0) {?>disabled <?}?>name="iset<?=$m;?>" id="iset<?=$m;?>" onClick="igo(<?=$n;?>, <?=$m;?>, '<strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?>', this.checked);" /><label for="iset<?=$m;?>"><strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?></label></div>
										<?
									}
								}
								?>
							</div>
							<div class="chosen"></div>
						</div>-->
						<div class="formagree">Нажимая «Отправить», вы соглашаетесь с <span class="lnk" onClick="$('#pol').fadeIn(300);">политикой конфиденциальности</span> и <a target="_blank" href="http://ormco.ru/files/sys/oferta_Ormco_Seminary.docx">условиями договора-оферты</a></div>
					</div>
					</form>
				</div>
			</div>
		</div>
		<div class="sugrob" style="background-image: url('images/sugrob.png'); background-size: cover;"></div>
		<div class="footer">
			Ormco Corporation &copy; 2018<br />
			<a href="http://ormco.ru">ormco.ru</a>
		</div>
		<div class="mask"></div>
		<div class="mask1"></div>
		<div class="burger" onClick="$('#mobmenu').slideDown(300);"><img src="images/burger.png" /></div>
		<div id="mobmenu" class="mobmenu">
			<div class="close2" onClick="$('#mobmenu').slideUp(300);"><img src="images/close2.png" /></div>
			<ul>
				<li id="miprogramm1"><a href="#programm">Программа</a></li>
				<li id="mispeakers1"><a href="#speakers">Спикеры</a></li>
				<li id="miprice1"><a href="#price">Стоимость</a></li>
				<li id="miplace1"><a href="#place">Место проведения</a></li>
				<li id="miform1"><a href="#form">Регистрация</a></li>
			</ul>
		</div>
	<div class="form_error">
		<div class="close" onClick="$('.form_error').fadeOut(300); $('.mask1').fadeOut(300);"><img src="images/close.png" /></div>
		<h3 id="form_error"></h3>
	</div>
	<div class="popup" id="noplaces">
		<div class="close" onClick="$('.popup').fadeOut(300); $('.mask').fadeOut(300);"><img src="images/close.png" /></div>
		<h3>Мест больше нет, регистрация окончена!</h3>
		<br />
		Вы можете записаться в лист ожидания. Для этого свяжитесь с нашим менеджером Натальей Постниковой<br />по телефону <a href="tel:+78123244260">+7 (812) 324-42-60</a> или e-mail <a href="mailto:Natalia.Postnikova@ormco.com">Natalia.Postnikova@ormco.com</a>
		<br />
		<br />
		<h4>Обратите внимание!</h4>
		Участники из листа ожидания смогут принять участие в тех мастер-классах, на которых появятся свободные места. Мы не можем гарантировать наличие мест именно на тех мастер-классах, на которые вы больше всего хотите попасть.
	</div>
	<div class="popup" id="popup">
		<div class="close" onClick="$('.popup').fadeOut(300); $('.mask').fadeOut(300);"><img src="images/close.png" /></div>
		<h3>Обратите внимание!</h3>
<br />
- 3 мастер-класса проходят одновременно в разных аудиториях
<br />
- каждый мастер-класс длится 3 часа;
<br />
- в середине мастер-класса – кофе-брейк;
<br />
- можно выбрать только по 1 мастер-классу в каждом блоке (до обеда и после обеда);
<br />
- количество мест ограничено.
<br /><br /><h3>Пожалуйста, выберите 1 мастер-класс до обеда и 1 - после обеда</h3>
	<button type="button" onClick="pop1 = 1; $('.popup').fadeOut(300); $('.mask').fadeOut(300);">Понятно!</button>
	</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter23063434 = new Ya.Metrika({
                    id:23063434,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    ecommerce:"dataLayer"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/23063434" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67881160-2', 'auto');
  ga('require', 'displayfeatures');
  ga('require', 'linkid');
  ga('require', 'ec');

  ga('ec:addProduct', {'name': '<?=$event_name;?>', 'category': 'Платное', 'price': '<?if (date("Y-m-d") < "2118-11-15") {?>12 000<?}else{?>14 000<?}?>', 'quantity': 1});
  ga('ec:setAction','checkout');

  ga('send', 'pageview');

</script>
<div id="pol">
<div class="close" onClick="$('#pol').fadeOut(300);"><img src="images/close.png" /></div>
<div id="inpol">
<h2>Политика конфиденциальности</h2>
<h4>1. Общие положения</h4>
Корпорация Ormco в лице ООО «Ормко» (далее «Ормко») и её аффилированные лица с уважением относятся к конфиденциальной информации любого лица, ставшего посетителем данного сайта. Мы хотели бы проинформировать Вас о том, какие именно данные мы собираем и каким образом используем собранные данные. Вы также узнаете о том, как Вы можете проверить точность собранной информации и дать нам указание об удалении подобной информации. Данные собираются, обрабатываются и используются строго в соответствии с требованиями действующего законодательства того государства, в котором расположено соответствующее аффилированное лицо компании "Ормко", отвечающее за защиту персональных данных. Мы делаем все возможное для обеспечения соответствия требованиям действующего законодательства.
Данное заявление не распространяется на сайты, на которые сайт компании "Ормко" содержит гиперссылки.
<h4>2. Сбор, использование и переработка персональных данных</h4>
Мы осуществляем сбор информации, относящейся к определенным лицам, лишь в целях обработки и использования информации и только в том случае, если Вы добровольно предоставили информацию или явно выразили свое согласие на ее использование.
Когда Вы посещаете наш сайт, определенные данные автоматически записываются на серверы компании "Ормко" и/или её аффилированных лиц для целей системного администрирования или для статистических или резервных целей. Записываемая информация содержит название Вашего интернет-провайдера, в некоторых случаях Ваш IP-адрес, данные о версии программного обеспечения Вашего браузера, об операционной системе компьютера, с которого Вы посетили наш сайт, адреса сайтов, после посещения которых Вы по ссылке зашли на наш сайт, заданные Вами параметры поиска, приведшие Вас на наш сайт.
В зависимости от обстоятельств, подобная информация позволяет сделать выводы о том, какая аудитория посещает наш сайт. В данном контексте, однако, не используются никакие персональные данные. Использоваться может лишь анонимная информация. Если информация передается компанией "Ормко" и/или её аффилированными лицами внешнему провайдеру, принимаются все возможные технические и организационные меры, гарантирующие передачу данных в соответствии с требованиями действующего законодательства.
Если Вы добровольно предоставляете нам персональную информацию, мы обязуемся не использовать, не обрабатывать и не передавать такую информацию способом, выходящим за рамки требований действующего законодательства. Использование и распространение Ваших персональных данных без Вашего согласия возможно только на основании судебного решения или в ином порядке, предусмотренном законодательством РФ.
Любые изменения, которые будут внесены в правила по соблюдению конфиденциальности, будут размещены на данной странице. Это позволяет Вам в любое время получить информацию о том, какие данные у нас хранятся и о том, каким образом мы собираем и храним такие данные.
<h4>3. Безопасность данных</h4>
Компания "Ормко" и её аффилированные лица обязуется бережно хранить Ваши персональные данные и принимать все меры предосторожности для защиты Ваших персональных данных от утраты, неправильного использования или внесения в персональные данные изменений. Партнеры компании "Ормко" и её аффилированных лиц, которые имеют доступ к Вашим данным, необходимым им для предоставления Вам услуг от имени компании "Ормко" и её аффилированных лиц, несут перед компанией "Ормко" и её аффилированными лицами закрепленные в контрактах обязательства соблюдать конфиденциальность данной информации и не имеют права использовать предоставляемые данные для каких-либо иных целей.
<h4>4. Персональные данные несовершеннолетних потребителей</h4>
Компания "Ормко" и её аффилированные лица не ведет сбор информации в отношении потребителей, не достигших 14 лет. При необходимости, мы можем специально попросить ребенка не присылать в наш адрес никакой личной информации. Если родители или иные законные представители ребенка обнаружат, что дети сделали какую-либо информацию доступной для компании "Ормко" и её аффилированных лиц, и сочтут, что предоставленные ребенком данные должны быть уничтожены, таким родителям или иным законным представителям необходимо связаться с нами по нижеуказанному (см. п. 6) адресу. В этом случае мы немедленно удалим личную информацию о ребенке.
<h4>5. Файлы Cookie</h4>
Мы используем файлы cookie для того, чтобы предоставить Вам больше возможностей при использовании сайта. Файлы cookie представляют собой небольшие блоки данных, помещаемые Вашим браузером на временное хранение на жестком диске Вашего компьютера, необходимые для навигации по нашему сайту. Файлы cookie не содержат никакой личной информации о Вас и не могут использоваться для идентификации отдельного пользователя. Файл cookie часто содержит уникальный идентификатор, представляющий собой анонимный номер (генерируемый случайным образом), сохраняемый на Вашем устройстве. Некоторые файлы удаляются по окончании Вашего сеанса работы на сайте; другие остаются на Вашем компьютере дольше.
Приступая к использованию данного веб-сайта, вы соглашаетесь на использование файлов cookie, Вы также признаете, что в подобном контенте могут использоваться свои файлы cookie.
Компания Ормко не контролирует и не несет ответственность за файлы cookie сторонних разработчиков. Дополнительную информацию Вы можете найти на сайте разработчика.
<h4>6. Отслеживание через интернет</h4>
На данном сайте осуществляется сбор и хранение данных для маркетинга и оптимизации с использованием технологии Яндекс.Метрика и Google Analitycs. Эти данные могут использоваться для создания профилей пользователей под псевдонимами. Сайт может устанавливать файлы cookie.
Без ясно выраженного согласия наших пользователей данные, собираемые с помощью технологий Яндекс.Метрика и Google Analitycs, не используются для идентификации личности посетителя и не связываются с какими-либо другими личными данными носителя псевдонима.
Дополнительную информацию об отслеживании через интернет Вы можете найти на сайтах провайдеров сервисов Яндекс.Метрика и Google Analitycs.
<h4>7. Ваши пожелания и вопросы</h4>
Хранящиеся данные будут стерты компанией "Ормко" и/или её аффилированными лицами по истечении периода хранения, установленного законодательством или договором либо в случае если сама компания "Ормко" и/или её аффилированные лица отменит хранение тех или иных данных. Вы вправе в любое время потребовать удаления из базы данных компании "Ормко" и/или её аффилированных лиц информации о Вас. Вы также вправе в любое время отозвать Ваше согласие на использование или переработку Ваших персональных данных. В таких случаях, а также, если у Вас есть какие-либо иные пожелания, связанные с Вашими персональными данными, просим Вас выслать письмо по почте в адрес «Ормко» в России по адресу: 195112, Санкт-Петербург, Малоохтинский пр-т, д. 64, корп. 3. или по электронной почте dc-sales@ormco.com. Просим Вас также связаться с нами в случае, если Вам хотелось бы узнать, собираем ли мы данные о Вас и если да, то какие именно данные. Мы постараемся выполнить Ваши пожелания в возможно короткие сроки.
<h4>8. Законодательство по обработке персональных данных</h4>
Все действия с персональными данными, собираемыми на данном сайте, производятся в соответствии с Федеральным законом Российской Федерации №152-ФЗ от 27 июля 2006 года «О персональных данных».
<h4>(1) Заявленная цель сбора, обработки или использования данных:</h4>
•	Предметом деятельности «Ормко» и её аффилированных лиц является производство и распространение стоматологических продуктов всех типов, главным образом брекет-систем, ортодонтических инструментов, микроимплантов, адгезивов;
<h4>(2) Описание групп вовлеченных лиц и соответствующих данных или категорий данных:</h4>
Данные, касающиеся заказчиков, сотрудников, пенсионеров, сотрудников сторонних компаний (субподрядчиков), персонала, работающего по лизингу, претендентов на рабочие места, авторов изобретений, не являющихся сотрудниками компаний, или наследников, соответственно, поставщиков товаров и услуг, сторонних заказчиков, потребителей, добровольцев, участвующих в потребительских испытаниях, посетителей производственных объектов корпорации, инвесторов – насколько это необходимо для выполнения целей, определенных в пункте 4.
<h4>(3) Получатели или категории получателей, которым могут быть разглашены данные:</h4>
Органы власти, фонды страхования здоровья, ассоциация по страхованию ответственности работодателей при наличии соответствующего правового регулирования, сторонние подрядчики в соответствии сторонние поставщики услуг, ассоциация пенсионеров «Ормко», аффилированные лица и внутренние подразделения для выполнения целей, указанных в пункте 4.
<h4>(4) Периодичность регулярного удаления данных:</h4>
Юристами подготовлено множество инструкций, касающихся обязанностей по хранению данных и периодов хранения. Данные удаляются в установленном порядке по истечении указанных периодов. Данные, не подпадающие под действие данных условий, удаляются, если цели, указанные в пункте 4, перестают существовать.
<h4>(5) Запланированная передача данных другим странам:</h4>
В рамках всемирной системы информации о трудовых ресурсах, данные по персоналу должны быть доступны определенным руководящим работникам в других странах. Соответствующие соглашения о защите данных должны быть заключены с соответствующими компаниями в соответствии со стандартами ЕС.
<h4>9. Использование встраиваемых модулей для социальных сетей</h4>
На наших интернет-страницах предусмотрена возможность встраивания модулей для социальных сетей ВКонтакте, Twitter, Instagram, Youtube (далее – «провайдеры»). 
Только если Вы активируете модуль, тем самым разрешая передачу данных, браузер создаст прямое соединение с сервером провайдера. Содержимое различных модулей впоследствии передается соответствующим провайдером непосредственно в Ваш браузер и выводится на экран Вашего компьютера.
Модуль сообщает провайдеру, на какую из страниц нашего сайта Вы вошли. Если во время просмотра нашего сайта Вы вошли на ВКонтакте, Instagram, Youtube или Twitter под своей учетной записью, провайдер может подобрать информацию, в соответствии с Вашими интересами, т.е. информацию, которую Вы просматриваете с помощью Вашей учетной записи. При использовании какой-либо функции встроенного модуля (например, кнопки “Мне нравится”, размещения комментария), эта информация также будет передана браузером непосредственно провайдеру для сохранения.
Дополнительную информацию по сбору и использованию данных социальными сетями, а также по правам и возможностям защиты Вашей конфиденциальности в данных обстоятельствах, можно найти в рекомендациях провайдеров по защите данных /конфиденциальности:
Для того, чтобы не подключаться к учетным записям провайдеров при посещении нашего сайта, Вам необходимо отключиться от соответствующей учетной записи перед посещением наших интернет-страниц.
</div>
</div>
	</body>
</html>
