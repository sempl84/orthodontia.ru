<?xml version="1.0" encoding="utf-8"?>
<!--<!DOCTYPE xsl:stylesheet SYSTEM	"ulang://i18n/smthng.dtd:file">-->
<xsl:stylesheet	version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:xlink="http://www.w3.org/TR/xlink"
		xmlns:umi="http://www.umi-cms.ru/TR/umi"
		exclude-result-prefixes="xlink">

	<xsl:output encoding="utf-8" method="html" indent="yes"/>

	<xsl:template match="/">
		<xsl:apply-templates select="КоммерческаяИнформация/Каталог/Товары/Товар" />
	</xsl:template>
	
	<xsl:template match="Товар" >
		<xsl:value-of select="Ид" />;<xsl:value-of select="Наименование" />;<xsl:value-of select="ДатаС" />;<xsl:value-of select="ДатаПо" /> \n
		
	</xsl:template>

</xsl:stylesheet>