<?php
require_once 'standalone.php';

//echo "<h1>Все заказы с подписью о скидках</h1>";
//$orders = new selector('objects');
//$orders->types('object-type')->name('emarket', 'order');
////$orders->where('freez_ormco_star')->isnull(true);
//$orders->where('event_discount_name')->isnull(false);
//$orders->group('event_discount_name');
//$orders->order('event_discount_name');
//$orders->limit(0,200);
//echo "<h2>Всего: {$orders->length()}</h2>";
////echo "<div class='fixed_height'>";
//foreach($orders as $order){
//    echo "{$order->getValue('event_discount_name')}<br />\n";
////    echo "{$order->id} - {$order->getValue('event_discount_name')} - {$order->name}<br />\n";
//}
////echo "</div>";
//
//die();

echo "<h1>Мероприятия с начислением баллов - выставленной галочкой 'Получение баллов лояльности при покупке мероприятия'</h1>";
$events_id = array();
$events = new selector('pages');
$events->types('object-type')->name('catalog', 'object');
$events->where('add_loyalty')->equals(1);
$events->where('is_active')->equals([0,1]);
$events->where('is_deleted')->equals([0,1]);
$events->limit(0,100);
echo "<h2>Всего: {$events->length()}</h2>";
echo "<div class='fixed_height small'>";
foreach($events as $event){
    echo "<a target='_blank' href='/admin/catalog/edit/{$event->id}/'>{$event->id}</a> - {$event->name}<br />\n";
    $events_id[] = $event->id;
}
echo "</div>";

echo "<h1>Наименования в заказе, где указаны данные мероприятия</h1>";
$order_items_id = array();
$order_items = new selector('objects');
//$order_items->types('object-type')->name('emarket', 'orderitem');
$order_items->types('object-type')->id(46);
$order_items->where('item_link')->equals($events_id);
$order_items->limit(0,1000);
//echo $order_items->query();

echo "<h2>Всего: {$order_items->length()}</h2>";
echo "<div class='fixed_height small'>";
foreach($order_items as $order_item){
    echo "<a target='_blank' href='/admin/data/guide_item_edit/{$order_item->id}/'>{$order_item->id}</a> - {$order_item->name} - " . /*print_r($order_item->getValue('item_link'), true) .*/ "<br />\n";
    $order_items_id[] = $order_item->id;
}
echo "</div>";

//$cmsController = cmsController::getInstance();
//$custom_module = $cmsController->getModule('custom');
$custom_module = system_buildin_load('custom');
$collection = umiObjectsCollection::getInstance();

echo "<h1>Заказы с данными мероприятиями и отсутствующими начислениями в поле 'Потенциальные баллы'</h1>";
$orders = new selector('objects');
$orders->types('object-type')->name('emarket', 'order');
$orders->where('potential_ormco_star')->isnull(false);
$orders->where('freez_ormco_star')->isnull(true);
$orders->where('order_items')->equals($order_items_id);
$orders->order('order_date')->asc();
$orders->limit(0,10000);
echo "<h2>Всего: {$orders->length()}</h2>";
echo "<div class='fixed_height'>";
foreach($orders as $order){
    if($order->name){
        $price = $order->getValue('event_discount_value');
        if(empty($price)){
            foreach($order->getvalue('order_items') as $one_order_item_id){
                $one_order_item_obj = $collection->getObject($one_order_item_id);
                if($one_order_item_obj instanceof umiObject){
                    $price += $one_order_item_obj->getValue('item_total_price');
                }
            }
        }

        $date_obj = $order->getValue('order_date');
        $date = '';
        if($date_obj instanceof umiDate){
            $date = date('d:m:Y', $date_obj->getDateTimeStamp());
        }

        $new_potential_ormco_star = $custom_module->getLoyaltyPotentialBonus($price);
        if($order->getValue('potential_ormco_star') - $new_potential_ormco_star != 0){
            echo "<b>";
        }
        echo "$date - <a target='_blank' href='/admin/emarket/order_edit/{$order->id}/'>{$order->id}</a> - {$order->name} - Начислено: {$order->getValue('potential_ormco_star')} - Предполагается: {$new_potential_ormco_star} - Цена: {$price}<br />\n";
        if($order->getValue('potential_ormco_star') - $new_potential_ormco_star != 0){
            echo "</b>";
        }
//        $order->setValue('potential_ormco_star', $new_potential_ormco_star);
//        echo $order->getValue("need_export");
//        $order->setValue("need_export", 1);
//        $order->commit();
//        echo $order->getValue("need_export");
    }
}
echo "</div>";
?>
<style>
    .fixed_height{height:400px;overflow:scroll;border:solid 1px #ccc;}
    .fixed_height.small{height:250px;}
</style>