<?php
session_start();
header("HTTP/1.1 200 OK");
header('Content-Type: text/html; charset=UTF-8');

$pageurl = "https://orthodontia.ru/forum2018e/";
$states = array(0 => "coffee.png", 1 => "speaker.jpg", 2 => "lunch.png", 3 => "ortho.jpg", 4 => "champ.jpg");
$programm = array(
	1 => array(	"date" => "2018-06-01",
				"time_start" => "9:00",
				"time_end" => "10:00",
				"name" => "Registration, welcome-coffee",
				"state" => 0,
				"speaker" => 0
	),
	2 => array(	"date" => "2018-06-01",
				"time_start" => "10:00",
				"time_end" => "11:30",
				"name" => "<strong>Finishing off cases with the Damon System</strong><br />
<ul><li>My evolution from conventional mechanics to the Damon System. The reason for changing.</li>
<li>The use of differential torques. When and why</li></ul>",
				"state" => 1,
				"speaker" => 2
	),
	3 => array(	"date" => "2018-06-01",
				"time_start" => "11:30",
				"time_end" => "12:00",
				"name" => "Coffee-break",
				"state" => 0,
				"speaker" => 0
	),
	4 => array(	"date" => "2018-06-01",
				"time_start" => "12:00",
				"time_end" => "13:30",
				"name" => "<strong>Finishing off cases with the Damon System</strong><br />
<ul><li>Crowding. D-Gainer.</li>
<li>When to extract. Change of paradigm.</li>
<li>Transversal problems. Crossbite.</li></ul>",
				"state" => 1,
				"speaker" => 2
	),
	5 => array(	"date" => "2018-06-01",
				"time_start" => "13:30",
				"time_end" => "14:30",
				"name" => "Lunch",
				"state" => 2,
				"speaker" => 0
	),
	6 => array(	"date" => "2018-06-01",
				"time_start" => "14:30",
				"time_end" => "16:00",
				"name" => "<strong>Finishing off cases with the Damon System</strong><br />
Sagittal  problems. Class II and Class III:
<ul><li>The potential of elastics</li>
<li>Camouflaging surgical cases</li></ul>",
				"state" => 1,
				"speaker" => 2
	),
	7 => array(	"date" => "2018-06-01",
				"time_start" => "16:00",
				"time_end" => "16:30",
				"name" => "Coffee-break",
				"state" => 0,
				"speaker" => 0
	),
	8 => array(	"date" => "2018-06-01",
				"time_start" => "16:30",
				"time_end" => "18:00",
				"name" => "<strong>Finishing off cases with the Damon System</strong><br />
Vertical problems:
<ul><li>Overbite</li>
<li>Openbite</li></ul>",
				"state" => 1,
				"speaker" => 2
	),
	9 => array(	"date" => "2018-06-02",
				"time_start" => "8:30",
				"time_end" => "10:00",
				"name" => "Ortho-breakfast with Ormco: \"Primary consultation: interdisciplinary approach\". The activity will be moderated by leading Russian speakers of different specializations: Andrey Tikhonov, Yana Dyachkova, Leonid Emdin <div class=red>Please note that lectures during ortho-breakfast are in Russian, no translation will be provided</div>",
				"state" => 3,
				"speaker" => 0
	),
	10 => array("date" => "2018-06-02",
				"time_start" => "10:00",
				"time_end" => "11:30",
				"name" => "How to handle the Pitfalls of Orthodontics – from the early treatment to the TMD Patient<br /><small>Development of the malocclusion – early recognition and prevention</small>",
				"state" => 1,
				"speaker" => 1
	),
	11 => array("date" => "2018-06-02",
				"time_start" => "11:30",
				"time_end" => "12:00",
				"name" => "Coffee-break",
				"state" => 0,
				"speaker" => 0
	),
	12 => array("date" => "2018-06-02",
				"time_start" => "12:00",
				"time_end" => "13:30",
				"name" => "How to handle the Pitfalls of Orthodontics – from the early treatment to the TMD Patient<br /><small>Three dimensional development of the arches with respect to mandibular posture: A to Z D-Gainer</small>",
				"state" => 1,
				"speaker" => 1
	),
	13 => array("date" => "2018-06-02",
				"time_start" => "13:30",
				"time_end" => "14:30",
				"name" => "Lunch",
				"state" => 2,
				"speaker" => 0
	),
	14 => array("date" => "2018-06-02",
				"time_start" => "14:30",
				"time_end" => "16:00",
				"name" => "How to handle the Pitfalls of Orthodontics – from the early treatment to the TMD Patient<br /><small>Practical diagnosis and treatment approach in TMD prevention and treatment</small>",
				"state" => 1,
				"speaker" => 1
	),
	15 => array("date" => "2018-06-02",
				"time_start" => "16:00",
				"time_end" => "16:30",
				"name" => "Coffee-break",
				"state" => 0,
				"speaker" => 0
	),
	16 => array("date" => "2018-06-02",
				"time_start" => "16:30",
				"time_end" => "18:00",
				"name" => "How to handle the Pitfalls of Orthodontics – from the early treatment to the TMD Patient<br /><small>Asymmetries and effect on mandibular position and TMD</small>",
				"state" => 1,
				"speaker" => 1
	),
	17 => array("date" => "2018-06-02",
				"time_start" => "19:00",
				"time_end" => "23:00",
				"name" => "Gala-dinner",
				"state" => 4,
				"speaker" => 0
	),
	18 => array("date" => "2018-06-03",
				"time_start" => "9:00",
				"time_end" => "10:00",
				"name" => "Welcome-coffee",
				"state" => 0,
				"speaker" => 0
	),
	19 => array("date" => "2018-06-03",
				"time_start" => "10:00",
				"time_end" => "12:00",
				"name" => "<strong>Unique Features of Insignia</strong>",
				"descr" => "
Utilizing Insignia custom smile design helps the orthodontist visualize the final outcome before treatment has even started. Through case studies showing every appointment of treatment, Dr. Kozlowski will teach how Insignia can help you take your clinical efficiency and quality of treatment results to the next level. 
<br />
COURSE DESCRIPTION:
<ol><li>Choosing the best cases to use Insignia (hint, it's not always the most difficult ones)</li>
<li>Understanding efficient and effective Damon mechanics to enhance your results</li>
<li>Learn how appointment intervals and wire sequencing is like driving a Ferrari</li>
<li>Utilization of disarticulation and elastics for the pinnacle in treatment efficiency</li>
<li>Managing difficult cases with Insignia and Damon mechanics</li></ol>",
				"state" => 1,
				"speaker" => 3
	),
	20 => array("date" => "2018-06-03",
				"time_start" => "12:00",
				"time_end" => "13:00",
				"name" => "Lunch",
				"state" => 2,
				"speaker" => 0
	),
	21 => array("date" => "2018-06-03",
				"time_start" => "13:00",
				"time_end" => "15:00",
				"name" => "<strong>Alias – future of lingual orthodontics</strong><br />
ALIAS - LSW bracket system represent the new generation of lingual Orthodontics. The systematic is characterized by the use of digital customized passive self-ligating brackets and lingual archwire without bends (Straight Wire)",
				"state" => 1,
				"speaker" => 5
	),
	22 => array("date" => "2018-06-03",
				"time_start" => "15:00",
				"time_end" => "15:30",
				"name" => "Coffee-break",
				"state" => 0,
				"speaker" => 0
	),
	23 => array("date" => "2018-06-03",
				"time_start" => "15:30",
				"time_end" => "17:30",
				"name" => "<strong>Checkmate to Difficult cases</strong>",
				"descr" => "
Diagnostic, treatment and resolution of clinical cases with different pathologies:
<ul><li>Class 3</li>
<li>Class 2</li>
<li>Open Bite</li>
<li>Severe Crowding</li>
<li>Narrow arches</li></ul>",
				"state" => 1,
				"speaker" => 4
	),
);
$allprices = array(
	1 => array(	"name" => "Forum",
				"price1" => "45000",
				"price2" => "55000",
	),
	2 => array(	"name" => "Ortho-Breakfast",
				"price1" => "2000",
				"price2" => "2000",
	),
	3 => array(	"name" => "Gala-dinner",
				"price1" => "7500",
				"price2" => "7500",
	),
);
$ids = array(
	"001" => 1651,
	"010" => 1652,
	"011" => 1653,
	"100" => 1649,
	"101" => 1650,
	"110" => 1647,
	"111" => 1648,
);
$discounts = array(
	1 => array(	"name" => "Скидка для членов ПОО",
				"discount" => 15
	)
);
$speakers = array(
	1 => array(	"name" => "Dr. Elizabeth Menzel",
				"short" => "Herrsching am Ammersee, Germany",
				"about" => "B.CH.D.University Stellenbosch. Orthodontic training at the University of Frankfurt, training in the practice ofDr König-Toll Kronberg, Dr. Toll in Bad Soden, Germany and with Prof. Axel Bumann in Kiel with an emphasis on jaw diseases and how to treat them. Works in a practice in Herrsching am Ammersee, Germany since 15 years.  She has also worked in Great Britain and Vilnius, Lithuania for the orthodontic treatment of patients with severe jaw diseases. Internationally recognized lecturer since 2006. Damon user since 2003.",
				"image" => "menzel.jpg",
	),
	2 => array(	"name" => "Dr. Ramón Perera",
				"short" => "Lleida, Spain",
				"about" => "Dr. Perera obtained his degree in Medicine and Surgery from the University of Barcelona in 1981 and then went on to complete his studies in Stomatology in 1983. In 1986, he completed postgraduate studies in Orthodoncy at the Centro Europeo de Ortodoncia in Madrid. From 1995 to 1997 he then completed a further series of studies with the Roth-Williams Centre. R. Perera has been a member of SEDO since 1991 and is also a member of AESOR, AIO and WFO. He is the author of various scientific articles and has given talks and presentations at numerous courses and congresses. Dr. Perera was a pioneer in the introduction of the Damon System in Spain. He first began using the Damon System in 2001 and since his visit to Dr. Damon’s surgery in Spokane, in 2003, he has exclusively used this system at his surgeries in Lleida and Tarragona.",
				"image" => "perera.jpg",
	),
	3 => array(	"name" => "Dr. Jeff Kozlowski",
				"short" => "Connecticut, USA",
				"about" => "Dr. Jeff Kozlowski is world renowned for his unique perspective on clinical efficiency. An internationally sought-after lecturer, he has presented on topics including Clinical Efficiency, Digital Orthodontics, Facial Esthetics, Efficient Early Treatment, and Orthodontic Treatment Mechanics. Dr. Kozlowski graduated with a BS in Economics from Syracuse University prior to receiving his DDS and Orthodontic Specialty Certificate from the State University of New York at Buffalo. He has been published in several orthodontic journals, including Seminars in Orthodontics, Journal of Clinical Orthodontics, Clinical Impressions and The Progressive Orthodontist. As an avid cyclist and endurance athlete, he has completed the Ironman Triathlon 5 times and the grueling Mt. Washington Bicycle Hill climb 10 times. He has practices in New London and East Lyme, CT. He and his wife, Amy, a pediatric dentist, have two children: Amelia and Jake.",
				"image" => "kozlowski.jpg",
	),
	5 => array(	"name" => "Dr. Giuseppe Scuzzo",
				"short" => "Rome, Italy",
				"about" => "Giuseppe Scuzzo graduated in medicine in 1983 from Rome University. He specialized in dentistry in 1987 (also Rome) and in orthodontics at Ferrara University. He is the author of numerous publications and has spoken extensively on lingual orthodontics. He is a founder member of the Italian Society of Lingual Orthodontics, and a past President of the European Society of Lingual Orthodontics. He is a professor at Ferrara University and associate professor at New York University. He is also the director of the First International Master in Lingual Orthodontics at Ferrara University",
				"image" => "scuzzo.jpg",
	),
	4 => array(	"name" => "Dr. Matias Anghileri",
				"short" => "Buenos Aires, Argentina",
				"about" => "Dentist graduated at the Universidad de Buenos Aires, Argentina – Specialist in Orthodontics and Functional Orthopedics – Member of the World Federation of Orthodontists – Damon System Certified Educator – Former Professor of Orthodontics at the Universidad de Morón, Argentina – Member of the Asociación Iberoamericana de Ortodoncistas – President of AIOI Bahía Blanca (Academia Internacional de Odontología Integral) – Speaker at the Sociedad Argentina de Ortodoncia – Speaker at long and short Damon System courses all over the country – Guest Speaker at the Universidad Católica Argentina. International Speaker at Damon courses in Colombia, Ecuador, Perú, Uruguay, Bolivia, Paraguay, and other Latin American countries. First Argentinian speaker at the Forum 2016 (California – USA)",
				"image" => "anghillieri.jpg",
	),
);
$speakers2 = array(
	1 => array(	"name" => "Dr. Andrey Tikhonov",
				"about" => "St. Petersburg, Russia

Orthodontist (has own practice in St. Petersburg) and consultant on the use of Ormco products. Assistant of the Chair of Orthodontics of the North-West State Medical University. Graduated from the First Pavlov State Medical University of St. Petersburg in 2000 and successfully completed his orthodontic training in 2003. He published ten articles and three workbooks on basic orthodontic problems. Dr. Tikhonov gives lectures in Russia and around the world. Together with his brother, he founded the Ormco Orthodontics School project. Certified European lecturer on Damon System. Experience with Damon System - since 2004.",
				"image" => "tikhonov.jpg",
	),
	2 => array(	"name" => "Dr. Yana Dyachkova",
				"about" => "Moscow, Russia

Orthodontist, Ph. D. In 1999 she graduated from the Moscow State University of Medicine and Dentistry. From 1999 to 2001 she was trained in clinical residency at the Department of Orthodontics and Children's Prosthetics of Moscow State Medical University, later - in graduate school at the same department. In 2009 she defended her thesis on \"Improving diagnostics of dental anomalies through computer technology\". Participant of many seminars and lectures on bracket systems of different manufacturers, features of treatment of orthodontic abnormalities with lingual braces, orthodontic implants, aligners, correction of TMJ dysfunction. Prefers Damon System in her clinical practice.",
				"image" => "dyachkova.jpg",
	),
	3 => array(	"name" => "Dr. Leonid Emdin",
				"about" => "St. Petersburg, Russia

Orthopedic dentist, orthodontist. In 2000 he graduated from the First Pavlov State Medical University of St. Petersburg. In 2001 he graduated from the internship of the North-Western State Medical University named after I.I. Mechnikov at the Department of therapeutic dentistry, majoring in General Practice Dentistry and in 2003 – graduated from residency on the specialty \"Dentistry therapeutical\". In 2003 he received an additional specialization in the course of professional retraining \"Modern aspects of orthopedic dentistry with the course of orthodontics\". In 2005 he completed a specialization in orthodontics at the First Pavlov State Medical University of St. Petersburg. He works as an orthopedic dentist and orthodontist in St. Petersburg. Participant of a large number of training events in Russia and abroad.",
				"image" => "emdin.jpg",
	)
);
$mkcounts = explode("/", file_get_contents("../forum2018/mkcounts.txt"));
?>
<!DOCTYPE html>
<html>
	<head>
		<title>ORMCO FORUM Russia 2018 Jun 1 - 3 2018</title>
		<meta http–equiv="Content–Type" content="text/html; charset=UTF–8">
		<link rel="stylesheet" media="screen" type="text/css" href="main.css">
		<meta name="keywords" content="">
		<meta name="description" content="">
		<meta name="HandheldFriendly" content="true">
		<meta name="MobileOptimized" content="width">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="/favicon.gif" type="image/x-icon">
		<link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="js/jquery.touchSwipe.min.js" type="text/javascript"></script>
		<script src="js/ajaxupload.3.5.js" type="text/javascript"></script>
		<script src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyBoDhMbGeceqxv4_7SROd3YL1-0C-SyBGo" type="text/javascript"></script>
		<script>
			var userid = "";
			var price = 0;
			ids = new Array();
			eids = new Array();
			eids[1] = "1";
			eids[2] = "0";
			eids[3] = "0";
			<?
			foreach ($ids as $num => $id)
				{
				?>
				ids["<?=$num;?>"] = <?=$id;?>;
				<?
			}
			?>
			function number_format( number, decimals, dec_point, thousands_sep ) {	// Format a number with grouped thousands
				// 
				// +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
				// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
				// +	 bugfix by: Michael White (http://crestidg.com)

				var i, j, kw, kd, km;

				// input sanitation & defaults
				if( isNaN(decimals = Math.abs(decimals)) ){
					decimals = 2;
				}
				if( dec_point == undefined ){
					dec_point = ",";
				}
				if( thousands_sep == undefined ){
					thousands_sep = ".";
				}

				i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

				if( (j = i.length) > 3 ){
					j = j % 3;
				} else{
					j = 0;
				}

				km = (j ? i.substr(0, j) + thousands_sep : "");
				kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
				//kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
				kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");


				return km + kw + kd;
			}
			function getprice(num, pr, state) {
				if (state) {
					price = price + pr;
					eids[num] = "1";
				}
				else {
					price = price - pr;
					eids[num] = "0";
				}
				$("#eprice").text(number_format(price, 0, ".", " ") + " RUR");
				var eid = "" + eids[1] + "" + eids[2] + "" + eids[3];
				// alert(eid);
				$("#events").val(ids[eid]);
				$("#regularprice").val(price);
				$("#finalprice").text(price + " RUR");
				if (price == 0)
					$("#startreg").attr("disabled", "disabled");
				else
					$("#startreg").removeAttr("disabled");
			}
			function showmes(mes) {
				$("#form_error").html(mes);
				$(".mask1").fadeIn(300);
				$(".form_error").fadeIn(300);
			}
			function remember_pass() {
				var e_email = $("#e_email").val();
				if (isEmail(e_email)) {
					$.ajax({
						url:"https://orthodontia.ru/udata/emarket/forget_pas/" + e_email + "/.json",
						xhrFields: {
						  withCredentials: true
						},
						dataType: "json",
						success:function(data) {
							if (data.status == "success") {
								$("#rp_ok").fadeIn(300);
								setTimeout(function() {$("#rp_ok").fadeOut(300)}, 15000);
							}
							else {
								$("#rp_fail").text("Пользователь не найден в базе, попробуйте еще раз").fadeIn(300);
								setTimeout(function() {$("#rp_fail").fadeOut(300)}, 15000);
							}
						}
					});
				}
				else {
					$("#rp_fail").text("Укажите свой email!").fadeIn(300);
					setTimeout(function() {$("#rp_fail").fadeOut(300)}, 15000);
				}
			}
			function countdown() {
				var events = $("#events").val();
				$.ajax({
					type: "POST",
					url: "xp_countdown.php",
					timeout: 5000,
					data: "events" + events,
					success: function(data) {
					},
					error: function(data) {
					}
				});
			}
			function writedata() {
				var alldata = "";
				$('form input').each(function() {
					if ($(this).attr("id") != undefined)
						alldata += "&" + $(this).attr("id") + "=" + $(this).val();
				});
				$('form select').each(function() {
					if ($(this).attr("id") != undefined)
						alldata += "&" + $(this).attr("id") + "=" + $(this).val();
				});
				$.ajax({
					type: "POST",
					url: "xp_write.php",
					timeout: 5000,
					data: "alldata=" + alldata,
					success: function(data) {
					},
					error: function(data) {
					}
				});
			}
			function setdiscount(discount, element, chk) {
				$(".upl").not("#" + element).removeAttr("checked");
				var pr = $("#regularprice").val();
				if (chk) {
					$("#discount").text(discount);
					$(".discount").fadeIn(300);
					pr = pr - parseInt(pr*(discount/100));
				}
				else {
					$(".discount").fadeOut(300);
					$(".newform_upload").fadeOut(300);
					$("#uploaded").fadeOut(300);
				}
				$("#finalprice").text(pr + " RUR");
			}
			function addnewyur() {
				$(".yurs").fadeOut(300);
				$(".newform_reg").fadeOut(300);
				$(".addyur").fadeOut(300, function() {
					$(".addyurform1").fadeIn(300);
				});
			}
			function addyurajax() {
				var tosend = "id=" + userid;
				$('.addyurform input').each(function(index) {
					var did = $(this).attr("id");
					if (did == "yurname")
						did = "name";
					if (did == "yuremail")
						did = "email";
					var dval = $(this).val();
					tosend += "&data[new][" + did + "]=" + dval;
				});
				$('.addyurform textarea').each(function(index) {
					var did = $(this).attr("id");
					var dval = $(this).val();
					tosend += "&data[new][" + did + "]=" + dval;
				});
				$.ajax({
					url:"https://orthodontia.ru/udata/emarket/add_legal_item/.json?" + tosend,
					xhrFields: {
					  withCredentials: true
					},
					dataType: "json",
					success:function(data) {
						if (data.result != 0) {
							$(".addyurform").fadeOut(300).promise().done(function() {
								// добавляем новое юр. лицо в select
								$('#yur').append($('<option>', {
										value: data.result,
										text : $("#yurname").val()
								}));
								$(".newform_reg").fadeIn(300);
								$(".addyur").fadeIn(300);
								$(".yurs").fadeIn(300);
							});
						}
						else {
							$(".addyurform").fadeOut(300, function() {
								$(".newform_reg").fadeIn(300);
								$(".addyur").fadeIn(300);
								$(".yurs").fadeIn(300);
							});
							alert("Новое юр. лицо не добавлено! Проверьте данные!");
						}
					}
				});
			}
			function showyur(n) {
				var foo = 0;
				if (n == 1) {
					$(".addyurform1 .ness").each(function() {
						if ($(this).val() == '') {
							if (foo == 0) {
								$('html,body').animate({scrollTop: $(this).offset().top - 100}, 500);
							}
							$(this).addClass('redfld');
							foo = 1;
						}
						else {
							$(this).removeClass('redfld');
						}
					});
				}
				if (foo == 0) {
					$(".addyurform" + (3 - n)).fadeOut(300, function() {
						$(".addyurform" + n).fadeIn(300);
					});
				}
			}
			function showtab(tab) {
				if (tab == 1)
					$("#yur").attr("disabled", "disabled");
				else
					$("#yur").removeAttr("disabled");
				$(".tab" + (3 - tab)).fadeOut(300);
				$(".tab" + tab).fadeIn(300);
				$(".tabbut" + (3 - tab)).removeClass("active");
				$(".tabbut" + tab).addClass("active");
			}
			function showpart(part) {
				if (chkform(2)) {
					if (userid == "") {
						$.ajax({
							url:"https://orthodontia.ru/udata/emarket/logout_user/.json",
							xhrFields: {
							  withCredentials: true
							},
							dataType: "json",
							success:function(data) {
								// авторизация на сайте
								var tosend = "id=" + userid;
								$('.part1 input').each(function(index) {
									var did = $(this).attr("id");
									var dval = $(this).val();
									tosend += "&data[new][" + did + "]=" + dval;
								});
								$('.part1 select').each(function(index) {
									var did = $(this).attr("id");
									var dval = $(this).val();
									tosend += "&data[new][" + did + "]=" + dval;
								});
								$.ajax({
									url:"https://orthodontia.ru/udata/users/lpreg/.json?" + tosend,
									dataType: "json",
									success:function(data) {
										$(".f_anim").css("opacity", 0);
										if (data.status == "successful") {
											// пользователь зарегистрирован
											user_id = data.user_id;
											$.ajax({
												url:"https://orthodontia.ru/udata/emarket/logout_user/.json",
												xhrFields: {
												  withCredentials: true
												},
												dataType: "json",
												success:function(data) {
													var newemail = $("#email").val();
													var newpassword = $("#password").val();
													$.ajax({
														xhrFields: {
														  withCredentials: true
														},
														dataType: "json",
														url:"https://orthodontia.ru/udata/emarket/get_user_info/" + newemail + "/" + newpassword + "/.json",
														success:function(data) {
															$(".part" + (3 - part)).fadeOut(300);
															$(".part" + part).fadeIn(300);
															$(".partbut" + (3 - part)).addClass("hidden");
															$(".partbut" + part).removeClass("hidden");
															$(".notfound").hide(0);
														}
													});
												}
											});
										}
										else {
											// неудачно, рисуем ошибку
											showmes(data.result);
										}
									}
								});
							}
						});
					}
					else {
						$(".part" + (3 - part)).fadeOut(300);
						$(".part" + part).fadeIn(300);
						$(".partbut" + (3 - part)).addClass("hidden");
						$(".partbut" + part).removeClass("hidden");
					}
				}
			}
			function chk_entry() {
				$("#entry_but").text("Проверяем...").attr("disabled", "disabled");
				$(".wrong").fadeOut(300);
				var e_email = $("#e_email").val();
				var e_pass = $("#e_pass").val();
				var eventid = $("#events").val();
				$.ajax({
					url:"https://orthodontia.ru/udata/emarket/logout_user/.json",
					xhrFields: {
					  withCredentials: true
					},
					dataType: "json",
					success:function(data) {
						$.ajax({
							xhrFields: {
							  withCredentials: true
							},
							dataType: "json",
							url:"https://orthodontia.ru/udata/emarket/get_user_info/" + e_email + "/" + e_pass + "/.json",
							success:function(data) {
								$(".f_anim").css("opacity", 0);
								if (typeof data.extended === 'object') {
									// пользователь ввел правильные данные
									var user = data;
									$(".wrong").fadeOut(300);
									$.ajax({
										url:"https://orthodontia.ru/udata/emarket/is_first_purchase/" + e_email + "/" + eventid + "/.json",
										xhrFields: {
										  withCredentials: true
										},
										dataType: "json",
										success:function(data) {
											$(".f_anim").css("opacity", 0);
											if (data.result == 1) {
												// пользователь уже зарегистрирован на мероприятие, выводим надпись
												$(".exists").css("opacity", 1);
											}
											else {
												userid = user.id;
												// alert(userid);
												// пользователь еще не зарегистрирован, все ОК, рисуем форму регистрации на мероприятие
												$(".openform").fadeOut(300, function() {
													$(".formbox2").hide(0);
													// console.table(user);
													// вставляем поля
													arruser = new Array();
													var allprops = user.extended.groups.group[0].property;
													$.each(allprops, function(index, value) {
														// console.log(this.name);
														arruser[this.name] = this.value;
													});
													if (typeof user.extended.groups.group[1] == "object") {
														var allprops = user.extended.groups.group[1].property;
														$.each(allprops, function(index, value) {
															// console.log(this.name);
															arruser[this.name] = this.value;
														});
													}
													// console.table(arruser);
													$("#fname").val(arruser["fname"].value);
													$("#lname").val(arruser["lname"].value);
													$("#father_name").val(arruser["father_name"].value);
													$("#email").val(arruser["e-mail"].value);
													$("#phone").val(arruser["phone"].value);
													$("#city").val(arruser["city"].value);
													$("#company").val(arruser["company"].value);
													$("#bd").val(arruser["bd"].value);
													$("#country").val(arruser["country"].item.name);
													$("#region").val(arruser["region"].item.id);
													if (typeof arruser["prof_status"] == "object")
														$("#who").val(arruser["prof_status"].item.name);
													// убираем пароли из формы
													$(".phide").hide();
													// рисуем форму
													$(".formbox3").show(0);
													$(".openform").fadeIn(300);
												});
												// подгружаем юр. лиц
												$.ajax({
													url:"https://orthodontia.ru/udata/emarket/legalList/.json",
													xhrFields: {
													  withCredentials: true
													},
													dataType: "json",
													success:function(data1) {
														// alert(data1.items.length);
														// добавляем юр. лица в select
														$.each(data1.items.item, function(index, value) {
															$('#yur').append($('<option>', {
																	value: value.id,
																	text : value.name
															}));
														});
														// console.table(data1.items);
													}
												});
											}
										},
										error:function(data) {
										}
									});
								}
								else {
									// пользователь ввел неверные данные
									$("#e_email").addClass("invalid");
									$("#e_pass").addClass("invalid");
									$(".wrong").fadeIn(300, function() {
										$("#entry_but").text("Войти").removeAttr("disabled");
									});
								}
							}
						});
					}
				});
			}
			function fixbody() {
				$("body").addClass("hold");
			}
			function unfixbody() {
				$("body").removeClass("hold");
			}
			function chg_passstate() {
				if ($("#e_pass").attr("type") == "password") {
					$("#e_pass").attr("type", "text");
				}
				else {
					$("#e_pass").attr("type", "password");
				}
			}
			function chk_f_email() {
				var f_email = $("#f_email").val();
				var eventid = $("#events").val();
				if (f_email != "" && isEmail(f_email)) {
					$(".exists").fadeOut(300);
					$(".f_anim").css("opacity", 1);
					$("#f_email").removeClass("invalid");
					// проверяем email
					$.ajax({
						url:"https://orthodontia.ru/udata/emarket/is_user_exist/" + f_email + "/.json",
						dataType: "json",
						success:function(data) {
							$(".f_anim").css("opacity", 0);
							if (data.result == 1) {
								// пользователь найден, проверяем, есть ли регистрация на мероприятие
								$.ajax({
									url:"https://orthodontia.ru/udata/emarket/is_first_purchase/" + f_email + "/" + eventid + "/.json",
									dataType: "json",
									success:function(data) {
										$(".f_anim").css("opacity", 0);
										if (data.result == 1) {
											// пользователь уже зарегистрирован на мероприятие, выводим надпись
											$(".exists").fadeIn(300);
										}
										else {
											// еще не зарегистрирован, рисуем форму входа
											$(".openform").fadeOut(300, function() {
												$("#email").val($("#f_email").val());
												$(".formbox1").hide(0);
												$("#e_email").val(f_email);
												$(".formbox2").show(0);
												$(".openform").fadeIn(300);
											});
										}
									}
								});
							}
							else {
								// пользователь не найден, отображаем форму регистрации пользователя
								$(".openform").fadeOut(300, function() {
									$("#email").val($("#f_email").val());
									$(".notfound").show(0);
									$(".formbox1").hide(0);
									$(".formbox3").show(0);
									$(".openform").fadeIn(300);
								});
							}
						},
						error:function(data) {
						}
					});
				}
				else {
					$("#f_email").addClass("invalid");
				}
			}
			jQuery(function($){
				$.datepicker.regional['ru'] = {
					closeText: 'Закрыть',
					prevText: '&#x3C;Пред',
					nextText: 'След&#x3E;',
					currentText: 'Сегодня',
					monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
					'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
					monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
					'Июл','Авг','Сен','Окт','Ноя','Дек'],
					dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
					dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
					dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
					weekHeader: 'Нед',
					dateFormat: 'yy-mm-dd',
					firstDay: 1,
					isRTL: false,
					showMonthAfterYear: false,
					yearSuffix: ''};
				$.datepicker.setDefaults($.datepicker.regional['ru']);
			});
			$(function() {
				$(".date1").datepicker({
					changeYear: true,
					yearRange: '1920:2020',
					defaultDate: '1980-01-01'
				});
			});
			lnks = new Array("form", "place", "price", "speakers", "programm", "about");
			var gmargin = 797;
			var curgal = 1;
			var maxx = 7;
			var gmargin1 = 797;
			var curgal1 = 1;
			var maxx1 = 5;
			var igos = 0;
			var totigos = 2;
			var prods = 0;
			var filedone = 0;
			function pset(ord, val, state) {
				if (state) {
					prods++;
					$("#p" + ord).val(val);
					$(".prodsel span").text("Выбрано " + prods);
				}
				else {
					prods--;
					$("#p" + ord).val("");
					if (prods == 0)
						$(".prodsel span").text("-------");
					else
						$(".prodsel span").text("Выбрано " + prods);
				}
			}
			function igo(group, ord, theme, state, similar) {
				if (state) {
					igos++;
					var arr_s = similar.split(",");
					$(".prog[data-group=" + group + "]").addClass("faded").find(".igo").fadeOut(200);
					$(".prog[data-mk=" + ord + "]").removeClass("faded").find(".igo").fadeIn(200);
					$(".igoset[data-group=" + group + "]").addClass("faded").find("input").attr("disabled", "disabled");
					$(".igoset[data-mk=" + ord + "]").removeClass("faded").find("input").removeAttr("disabled").attr("checked", "checked");;
					$.each(arr_s, function(key, val) {
						$(".prog[data-mk=" + val + "]").addClass("faded").find(".igo").fadeOut(200);
						$(".igoset[data-mk=" + val + "]").addClass("faded").find("input").attr("disabled", "disabled");
					})
					$("#mk" + ord).parent().addClass("sel");
					$("#lf" + ord).text("Пойду!");
					$("#mk_" + ord).parent().addClass("sel");
					$("#lf_" + ord).text("Пойду!");
					var html = "<div id=\"chosen" + ord + "\" class=\"chitem group\"><div class=\"igonum\">" + igos + "</div><div class=\"igodesc\" id=\"igod" + ord + "\">" + theme + "</div><!--<div class=\"igodel\" onClick=\"igo(" + group + ", " + ord + ", '', false);\"><span>X</span> Удалить</div>--></div>";
					$(".chosen").append(html);
					$("#mclasses" + group).val(theme);
					$("#mcl" + group).val(ord);
					// для прокрутки вниз при выборе первого мастер-класса
					if (igos == 1)
						$('html,body').animate({scrollTop: $(".prog[data-group=" + (3 - group) + "]").offset().top - 200}, 500);
					
				}
				else {
					igos--;
					var arr_s = similar.split(",");
					$(".prog[data-group=" + group + "]").removeClass("faded").find(".igo").fadeIn(200);
					$(".igoset[data-group=" + group + "]").not(".cancelled").removeClass("faded").find("input").removeAttr("disabled");
					$.each(arr_s, function(key, val) {
						$(".prog[data-mk=" + val + "]").removeClass("faded").find(".igo").fadeIn(200);
						$(".igoset[data-mk=" + val + "]").not(".cancelled").removeClass("faded").find("input").removeAttr("disabled");
					})
					$(".igoset[data-mk=" + ord + "]").find("input").removeAttr("checked");
					$("#mk" + ord).parent().removeClass("sel");
					$("#lf" + ord).text("Хочу пойти");
					$("#mk_" + ord).parent().removeClass("sel");
					$("#lf_" + ord).text("Хочу пойти");
					$("#chosen" + ord).remove();
					$('.chitem').each(function(index) {
						$(this).find(".igonum").text(index + 1);
					});
					$("#mclasses" + group).val("");
					$("#mcl" + group).val("");
				}
			}
			function openmob(n) {
				var wdth = $(window).width();
				if (wdth < 960) {
					$("#event" + n).slideToggle(300);
					$("#prog" + n).toggleClass("open");
				}
			}
			function isEmail(email) {
				var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				return regex.test(email);
			}
			function chkigos() {
				if (igos < totigos) {						
					$('.prog1').fadeOut(200);
					$('.progtab.sel').removeClass('sel');
					$('.progtab2').addClass('sel');
					$('div[data-date=8]').fadeOut(300);
					$('h2[data-date=8]').fadeOut(300);
					$('.progspace').fadeIn(300);
					$('.mktime').css('display', 'table');
					$('div[data-date=9]').fadeIn(300);
					$('h2[data-date=9]').fadeIn(300);
					$('.prog2').fadeIn(200).promise().done(function() {
						$('html,body').animate({scrollTop: $(".prog2").offset().top - 100}, 500);
					});
					return false;
				}
				else
					return true;
			}
			function chkform(chkfile) {
				var foo = 0;
				if (chkfile == 1)
					$("#register").attr("disabled", "disabled");
				if (chkfile != 0 && filedone == 0 && $('.upl:checked').length > 0)
					{
					if (foo == 0) {
						$('html,body').animate({scrollTop: $('.upl').offset().top - 100}, 500);
					}
					$('#upload').addClass('redfld');
					foo = 1;
				}
				$('.ness:visible').each(function() {
					if ($(this).val() == '') {
						if (foo == 0) {
							$('html,body').animate({scrollTop: $(this).offset().top - 100}, 500);
						}
						$(this).addClass('redfld');
						foo = 1;
					}
					else {
						$(this).removeClass('redfld');
					}
				});
				$('#email').each(function() {
					if (!isEmail($(this).val())) {
						if (foo == 0) {
							// alert($(this).offset().top);
							$('html,body').animate({scrollTop: $(this).offset().top - 100}, 500);
						}
						$(this).addClass('redfld');
						foo = 1;
					}
					else {
						$(this).removeClass('redfld');
					}
				});
				if (foo == 0) {
					if (userid != "" && chkfile == 1) {
						countdown();
						writedata();
					}
					return true;
				}
				else {
					$("#register").removeAttr("disabled");
					return false;
				}
			}
			$(window).resize(function() {
				var wdth = $(window).width();
				if (wdth < 960) {
					gmargin = wdth - 20;
					$(".gal_slide").css("width", gmargin);
					var ghght = gmargin/1250*669;
					$(".gal_slide").css("height", ghght);
					$(".gallery").css("height", ghght);
					$(".gal_navs").css("top", ghght - 80);
				}
				/*
				var gwdth = $(window).width();
				if (gwdth > 1250)
					gwdth = 1250;
				gmargin = gwdth;
				var ghght = gwdth/1250*669;
				if (gwdth < 480)
					$(".prog").css("width", gwdth - 40);
				$(".gal_slide").css("width", gwdth);
				$(".gal_slide").css("height", ghght);
				$(".gallery").css("height", ghght);
				$(".gal_navs").css("top", ghght - 80);
				*/
			})
			// $(window).load(function() {
				// $("#left").css("margin-left", "-1046px");
				// $("#right").css("margin-left", "250px");
			// });
			var spmargin = 0;
			var spmargin1 = 0;
			var swidth = 320;
			var curspeaker = 1;
			var curspeaker1 = 1;
 			function speaker1_left() {
				if (spmargin1 < 0)
					speaker1_go(curspeaker1 - 1);
			}
 			function speaker1_right() {
				var slide_count = $(".speakers1").find(".speaker:visible").length;
				var limit = (slide_count - 1) * swidth;
				if (spmargin1 > -limit)
					speaker1_go(curspeaker1 + 1);
			}
 			function speaker_left() {
				if (spmargin < 0) {
					spmargin = spmargin + swidth;
					$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 500);
					curspeaker--;
					$(".spbullets").find(".active").removeClass("active");
					$("#spbul_" + curspeaker).addClass("active");
				}
				else {
					$(".speakers").animate({margin: '0 0 0 20'}, 200, function() {
						$(".speakers").animate({margin: '0 0 0 0'}, 200);
					});
				}
			}
 			function speaker_right() {
				var slide_count = $(".speakers").find(".speaker:visible").length;
				var limit = (slide_count - 1) * swidth;
				if (spmargin > -limit) {
					spmargin = spmargin - swidth;
					$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 500);
					$(".spbullets").find(".active").removeClass("active");
					curspeaker++;
					$(".spbullets").find(".active").removeClass("active");
					$("#spbul_" + curspeaker).addClass("active");
				}
				else {
					$(".speakers").animate({margin: '0 0 0 ' + (spmargin - 20)}, 200, function() {
						$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 200);
					});
				}
			}
			function speaker1_go(sp) {
				spmargin1 = - swidth * (sp - 1);
				$(".speakers1").animate({margin: '0 0 0 ' + spmargin1}, 500);
				$(".spbullets1").find(".active").removeClass("active");
				$("#spbul_" + sp).addClass("active");
				curspeaker1 = sp;
			}
			function speaker_go(sp) {
				spmargin = - swidth * (sp - 1);
				$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 500);
				$(".spbullets").find(".active").removeClass("active");
				$("#spbul_" + sp).addClass("active");
				curspeaker = sp;
			}
			$(document).ready(function() {
				$("body").bind("click", function(e) {
					if ($(e.target).closest(".igosel").length > 0 || $(e.target).closest(".prodsel").length > 0 || $(e.target).closest(".igoset").length > 0 || $(e.target).closest(".prodset").length > 0) {
						return;
					}
					$(".igodd").fadeOut(500);
					$(".igosel").removeClass('sel');
					$(".proddd").fadeOut(500);
					$(".prodsel").removeClass('sel');
				});
				var wdth = $(window).width();
				if (wdth < 960) {
					gmargin = wdth - 20;
					$(".gal_slide").css("width", gmargin);
					var ghght = gmargin/1250*669;
					$(".gal_slide").css("height", ghght);
					$(".gallery").css("height", ghght);
					$(".gal_navs").css("top", ghght - 80);
					$(".speakers").swipe( {
						swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
							speaker_right();
						},
						swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
							speaker_left();
						},
					   threshold: 75
					});
					$(".speakers1").swipe( {
						swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
							speaker1_right();
						},
						swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
							speaker1_left();
						},
					   threshold: 75
					});
					$(".gallery").swipe( {
						swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
							gallery_right();
						},
						swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
							gallery_left();
						},
					   threshold: 75
					});
					swidth = wdth;
					$(".speaker").css("width", wdth);
				}
				/*
				maxx = $(".gal_slide").length;
				var gwdth = $(window).width();
				if (gwdth > 1250)
					gwdth = 1250;
				gmargin = gwdth;
				var ghght = gwdth/1250*669;
				if (gwdth < 480)
					$(".prog").css("width", gwdth - 40);
				$(".gal_slide").css("width", gwdth);
				$(".gal_slide").css("height", ghght);
				$(".gallery").css("height", ghght);
				$(".gal_navs").css("top", ghght - 80);
				*/
				hghts = new Array();
				for (var key in lnks) {
					if ($("[name=" + lnks[key] +"]").length) {
						hghts[key] = $("[name=" + lnks[key] +"]").offset().top;
					}
				}
				var i = document.location.hash.replace("#", "");
				$("a[href*='#']:not([href='#'])").click(function() {
					if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
						|| location.hostname == this.hostname) {
						var target = $(this.hash);
						target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
						   if (target.length) {
							var i = target.attr("name");
							scrolling = 1;
							$("#mobmenu").slideUp(300);
							$('html,body').animate({
								 scrollTop: target.offset().top - 60
							}, 1000, function() {
								scrolling = 0;
								// alert(target.offset().top);
							});
							$(".ch").removeClass("ch");
							$(this).parent().addClass("ch");
							// history.pushState(null, i, i);
							return false;
						}
					}
				});
			});
			function gallery_go(n) {
				curgmargin = -gmargin * (n - 1);
				$(".gal_slider").animate({marginLeft: curgmargin});
				curgal = n;
			}
			function gallery_left() {
				if (curgal > 1) {
					gallery_go(curgal - 1);
				}
				else {
					gallery_go(maxx);
				}
			}
			function gallery_right() {
				if (curgal < maxx) {
					gallery_go(curgal + 1);
				}
				else {
					gallery_go(1);
				}
			}
			function gallery_go1(n) {
				curgmargin1 = -gmargin1 * (n - 1);
				$(".gal_slider1").animate({marginLeft: curgmargin1});
				curgal1 = n;
			}
			function gallery_left1() {
				if (curgal1 > 1) {
					gallery_go1(curgal1 - 1);
				}
				else {
					gallery_go1(maxx1);
				}
			}
			function gallery_right1() {
				if (curgal1 < maxx1) {
					gallery_go1(curgal1 + 1);
				}
				else {
					gallery_go1(1);
				}
			}
			var scrolling = 0;
			/*
			window.onscroll = function() {
				var hght = $('body').height();
				// alert(hght);
				var scrolled = window.pageYOffset || document.documentElement.scrollTop;
				if (scrolling == 0)
					{
					for (var key in hghts)
						{
						if (scrolled >= hghts[key] - 300) {
							$(".ch").removeClass("ch");
							$("#mi" + lnks[key]).addClass("ch");
							$("#mi" + lnks[key] + "1").addClass("ch");
							break;
						}
					}
				}
				var wdth = $(window).width();
				if (scrolled < 320 && wdth > 960) {
					var leftmarg = scrolled - 1146;
					var rightmarg = 450 - scrolled;
					$("#left").css("margin-left", leftmarg);
					$("#right").css("margin-left", rightmarg);
				}
			}
			*/
			function reload() {
				var src = document.captcha.src;
				document.captcha.src = '/images/loading.gif';
				document.captcha.src = src + '?rand='+Math.random();
			}
			var x1 = 59.9317953;
			var y1 = 30.3507702;
			var x2 = 25.217603;
			var y2 = 55.2828107;
			var myLatlng1;
			var myLatlng2;
			var map;
			var marker1;
			var marker2;
			function initialize_map() {
				myLatlng1 = new google.maps.LatLng(x1, y1);
				myLatlng2 = new google.maps.LatLng(x2, y2);
				var mapOptions = {
					center: myLatlng1,
					mapTypeControl:!1,
					streetViewControl:!1,
					scrollwheel:!1,
					panControl:!1,
					draggable:!1,
					zoomControlOptions:{position:google.maps.ControlPosition.LEFT_TOP},
					zoom: 17,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				map = new google.maps.Map(document.getElementById("map"),
					mapOptions);
				marker1 = new google.maps.Marker({
					position: myLatlng1,
					map: map,
					title: "г. Москва, улица Тверская, 26/1, отель \"Отель «Коринтия» Санкт-Петербург\""
				});
				var infowindow1 = new google.maps.InfoWindow({
					content: "<div class=\"onmap\"><h3>Отель «Коринтия», Санкт-Петербург</h3>, Невский пр., дом 57</div>"
				});
				marker1.addListener('click', function() {
					infowindow1.open(map, marker1);
				});
				/*
				marker2 = new google.maps.Marker({
					position: myLatlng2,
					map: map,
					title: "Санкт-Петербург"
				});
				*/
			}
			function map1() {
				map.setCenter(myLatlng1);
			}
			function map2() {
				map.setCenter(myLatlng2);
			}
			// загрузка изображения
			$(function(){
				var btnUploads = $("#upload");
				$(btnUploads).each(function() {
					new AjaxUpload(this, {
						action: "xp_upload.php",
						name: "uploadfile",
						onComplete: function(file, response){
							if (response.replace("Error", "") == response) {
								$("#upload").addClass("done").text("Файл загружен");
								if (response.replace(".pdf", "") == response && response.replace(".PDF", "") == response) {
									$("#uploaded").html("<img width=\"100%\" src=\"uploads/" + response + "\" alt=\"\" />");
								}
								// $("#file").val('<a href="<?=$_SERVER["REQUEST_URI"];?>uploads/' + response + '">скачать</a>');
								$("#file").val('<?=$pageurl;?>uploads/' + response);
								filedone = 1;
							}
							else {
								alert("Файл " + file + " не загружен! Ошибка: " + response.replace("Error", ""));
							}
						}
					});
				})
			});
		</script>
	</head>
	<body>
		<div class="container">
			<a name="about"></a>
			<div class="section top">
				<div class="section header">
					<img src="images/logo_.png" />
					<div class="menu">
						<ul>
							<a href="#choose"><button>Registration</button></a>
							<li id="miprogramm"><a href="#programm">Agenda</a></li>
							<li id="migala"><a href="#gala">Gala-dinner</a></li>
							<li id="mispeakers"><a href="#speakers">Speakers</a></li>
							<li id="miprice"><a href="#price">Participation Fee</a></li>
							<li id="miplace"><a href="#place">Venue</a></li>
						</ul>
					</div>
					<a href="/forum2018"><div class="eng">Ру</div></a>
				</div>
			</div>
			<div class="section baseinfo">
				<!--<div class="superback">
					<div class="superback_">
						<div class="superback_left"><h1>ОРМКО ФОРУМ 2018</h1></div>
						<div class="superback_right">
							<h2 class="h2"><span>1 - 3</span> июня 2018 г.</h2>
							<h3 class="h3">Санкт-Петербург, Отель «Коринтия»</h3>
						</div>
					</div>
				</div>-->
				<div class="superback1">
					<div class="superback1_">
						<div class="superback_left"><h1>ORMCO<br />FORUM<br />RUSSIA<br />2018</h1></div>
						<div class="superback_right">
							<h2 class="h2"><span>June 1-3</span>, 2018</h2>
							<h3 class="h3">Saint-Petersburg “Corinthia” Hotel</h3>
						</div>
					</div>
				</div>
				<div class="main">
					<!-- Программа -->
					<a name="programm"></a>
					<h1>Agenda</h1>
					<br />
					<?
					foreach ($programm as $prog)
						$progdates[] = $prog["date"];
					$progdates = array_unique($progdates);
					?>
					<div class="progtabbs">
						<div class="progline"></div>
						<div class="progtabs">
							<?
							$pdcnt = 0;
							foreach ($progdates as $pd => $progdate)
								{
								?>
								<div class="progtab progtab<?=$pd;?><?if ($pdcnt++ == 0) {?> sel<?}?>" onClick="$('.progtab.sel').removeClass('sel'); $(this).addClass('sel'); $('.prog[data-date!=<?=$progdate;?>]').fadeOut(300); $('.prog[data-date=<?=$progdate;?>]').fadeIn(300); $('.txt[data-date!=<?=$progdate;?>]').fadeOut(300); $('.txt[data-date=<?=$progdate;?>]').fadeIn(300);"><?=ourdate($progdate, 0);?></div>
								<?
							}
							?>
						</div>
						<div class="progline"></div>
					</div>
					<div class="txt" data-date="2018-06-01">In this presentation we show step-by-step details of numerous clinical cases treated using the Damon system. These examples demonstrate that, regardless of the nature and complexity of the initial malocclusion, it is possible to finish off each treatment while at the same time maintaining total three-dimensional control over the teeth, obtaining excellent dental results and achieving spectacular improvements in final smiles and facial aesthetics.
<br /><br />
During the course we will examine many clinical cases step by step, focusing on the most important aspects and providing numerous details about how to correctly finish cases.</div>
					<div class="progs">
						<?
						$pdcnt = 0;
						foreach ($programm as $p => $prog)
							{
							$dt = explode(" ", ourdate($prog["date"], 0));
							?>
							<!-- <?=ourdate($prog["date"], 1);?> -->
							<!-- Событие <?=$p;?> -->
							<div class="prog group" data-date="<?=$prog["date"];?>" id="prog<?=$p;?>" onClick="openmob(<?=$p;?>);"<?if ($prog["date"] != $programm[1]["date"]) {?> style="display: none"<?}?>>
								<div class="event1">
									<time><?=$prog["time_start"];?> – <?=$prog["time_end"];?></time>
									<div class="eshort"><?=$prog["name"];?></div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/<?if ($prog["state"] == 1) {?><?=$speakers[$prog["speaker"]]["image"];?><?}else{?><?=$states[$prog["state"]];?><?}?>" /></div>
									<div class="psd"><h4><?=$prog["time_start"];?> – <?=$prog["time_end"];?></h4></div>
								</div>
								<div class="event">
									<?if ($prog["state"] == 1) {?><h4><?=$speakers[$prog["speaker"]]["name"];?></h4><button type="button">Lecture</button><?}?>
									<?
									if ($prog["descr"] != "")
										{
										?>
										<img id="imgopen<?=$p;?>" src="images/arr_open.png" onClick="$(this).fadeOut(200); $('#imgclose<?=$p;?>').fadeIn(200); $(this).parent().find('.elong').slideDown(300);" />
										<img id="imgclose<?=$p;?>" src="images/arr_close.png" style="display: none;" onClick="$(this).fadeOut(200); $('#imgopen<?=$p;?>').fadeIn(200); $(this).parent().find('.elong').slideUp(300);" />
										<?
									}
									?>
									<div class="eshort"><?=$prog["name"];?></div>
									<div class="elong"><?=$prog["descr"];?></div>
								</div>
								<div class="event2" id="event<?=$p;?>">
									<div class="psimg"><img align="left" src="images/<?if ($prog["state"] == 1) {?><?=$speakers[$prog["speaker"]]["image"];?><?}else{?><?=$states[$prog["state"]];?><?}?>" /></div>
									<div class="psd"><span><?=$dt[0];?></span> <?=$dt[1];?><br /></div>
								</div>
								<div class="dt">
									<span><?=$dt[0];?></span><br /><?=$dt[1];?>
								</div>
							</div>
							<?
						}
						?>
					</div>
					<div class="prog1">Main forum agenda tickets left: <?=$mkcounts[0];?></div>
					<div class="txtdiv">The language of translation is only English</div>
					<div class="spec" onClick="$('.mask').fadeIn(300); $('#spec').fadeIn(300);">Special Terms of Hotel Accommodation</div>
					<div class="txtdiv">We kindly ask you to take care of your hotel accommodation in advance and book your hotel stay at earliest convenience! In June Saint-Petersburg becomes to be one of the most beautiful and attractive cities, so the city is extremely popular with the tourists from all over the world. Meanwhile in spring and summer of 2018 several global events will be held in Saint Petersburg, among them: World  Economic Forum and FIFA World Cup and it is expected that many more guests will come to Saint-Petersburg during White Nights time</div>
					<div class="txtdiv"><span class="red">Please note that you may need visa for visiting Russia!</span><br />For visa assistance please contact our partner agency Starliner at e-mail <a href="mailto:e.danilova@starliner.ru">e.danilova@starliner.ru</a> (Elizabeth)</div>
					<div class="spec_desc" id="spec">
						<div class="close" onClick="$('.mask').fadeOut(300); $('.spec_desc').fadeOut(300);"><img src="images/close.png" /></div>
						<h2>Special Terms of Hotel Accommodation</h2>
						<?=nl2br("Participants can book deluxe rooms at Corinthia Hotel St. Petersburg. Petersburg at special prices:

<strong>Single accommodation (1 night) - 12 500 RUR</strong>
<strong>Double accommodation (1 night) - 14 500 RUR</strong>

To place the reservation, please <a href='uploads/Booking-form-OrmcoForumRussia.docx'><span>fill in this form</span></a> and send it to the specialists of the travel agency Starliner at e-mail <a href='mailto:e.danilova@starliner.ru'>e.danilova@starliner.ru</a> (Elizabeth) или <a href='mailto:j.brednikova@starliner.ru'>j.brednikova@starliner.ru</a> (Julia). Starliner is a partner of Ormco and will arrange for you the reservation of the selected room.

<span class='red'>Note!</span> The number of rooms available for booking at special prices is limited! Only 80 rooms are available!");
						?>
					</div>
					<div class="spec" onClick="$('.mask').fadeIn(300); $('#football').fadeIn(300);">NOTE! Rules of registration in St.Petersburg due to FIFA 2016 World Cup</div>
					<div class="spec_desc" id="football">
						<div class="close" onClick="$('.mask').fadeOut(300); $('.spec_desc').fadeOut(300);"><img src="images/close.png" /></div>
						<h2>Dear Ormco Forum Russia 2081 Guests,</h2>
						<?=nl2br("Please be informed that all the guests visiting Saint-Petersburg from May 25 to July 25 during FIFA 2018 World Cup are expected to get registered in local Immigration & Citizenship Department

If you stay at the hotel you will get registered by receptionists upon check-in. Guest registration fee is approx. 250 RUR.

If you prefer to stay at another place (friends, relatives or rental apartment) you need to visit one of the affiliated branches of Immigration Department within first 24 hours and get registered there. Please  be accompanied by your host person who should provide related documents for the premises you stay at.

We hope these minor requirements will not disturb you much and you will have great time obtaining the most up-to-date knowledge and enjoying unique white night period in St Petersburg.");
						?>
					</div>
					<a name="new"></a>
					<br />
					<div><a href="#choose"><button type="button">Registration</button></a></div>
					<br />
					<a name="gala"></a>
					<h1>Gala-dinner</h1>
					<div class="txtdiv">
					<div class="gallery1">
						<div class="gal_slider1 group">
							<div class="gal_slide"><img src="images/building7.jpg" /></div>
							<div class="gal_slide"><img src="images/building8.jpg" /></div>
							<div class="gal_slide"><img src="images/building9.jpg" /></div>
							<div class="gal_slide"><img src="images/building10.jpg" /></div>
							<div class="gal_slide"><img src="images/building11.jpg" /></div>
						</div>
						<div class="gal_navs">
							<div class="gal_nav" onClick="gallery_left1();"><img src="images/arr_left.png" /></div>
							<div class="gal_nav" onClick="gallery_right1();"><img src="images/arr_right.png" /></div>
						</div>
					</div>
					<?=nl2br("
The gala-dinner on June 2 will take place in Marble Palace – a unique sample of architecture of the XVIII century and one of the most beautiful buildings of St. Petersburg. It was constructed in 1768 — 1785 according to the design by an Italian architect Antonio Rinaldi. That was Empress Catherine II who ordered to put it up for Grigory Orlov –

General-Feldzeugmeister and her favorite.

Grigory Orlov died when the palace was still under construction, and Catherine II bought it out from Grigory’s heirs — Orlov brothers – and presented it to her second grandson on his wedding which was held in 1796 г. Then Emperor Nicolas I appointed the palace to be the property of his second son Grand Duke Konstantin (1827-1892).

In 1844 — 1849 Marble palace and its service pavilion were reconstructed according to the project by an architect Alexander Bryullov for next palace owner’s wedding. Major changes occurred on the second floor as it was newly planned and state and private interiors were redesigned. The XVIII century hanging garden was replaced with a winter one.

Grand Duke Konstantin (1858-1915), the son of Konstantin-senior, inherited the palace in 1892. His sons had to sell the palace complex to the state after his death as they didn’t have enough money to maintain it. This happened in the autumn of 1917, when Provisional Government had already assumed power and the palace housed The Ministry of Labor.

Now Marble palace belongs to the State Russian Museum and permanently displays the collection of Russian and foreign pieces of art coming from the second half of the XX century.

Read more http://en.rusmuseum.ru/marble-palace/history/

<big>Find yourself in the atmospehe of XIX century Russian ball and discover Russian culture and hospitality!</big>

<em>Please be advised to book tickets in advance as the number of places is limited.</em>");?>
					</div>
					<br />					<!-- Спикеры -->
					<a name="speakers"></a>
					<h1>Speakers, Main Agenda</h1>
					<hr class="mobile" />
					<div class="speakersdiv">
						<div class="speakers group">
							<?
							$scnt = 0;
							foreach ($speakers as $speaker)
								{
								$scnt++;
								$arr_name = explode(" ", $speaker["name"]);
								?>
								<div class="speaker" onClick="$('.mask').fadeIn(300); $('#spdesc<?=$scnt;?>').fadeIn(300);">
									<img src="images/<?=$speaker["image"];?>" />
									<div class="speakerdata">
										<h4><?=$arr_name[0];?><br /><?=$arr_name[1];?> <?=$arr_name[2];?></h4>
									</div>
								</div>
								<div class="speaker_desc" id="spdesc<?=$scnt;?>">
									<div class="close" onClick="$('.mask').fadeOut(300); $('.speaker_desc').fadeOut(300);"><img src="images/close.png" /></div>
									<h4><?=$speaker["name"];?></h4>
									<h5><?=$speaker["short"];?></h5>
									<?=$speaker["about"];?>
								</div>
								<?
								if ($scnt > 0 && $scnt % 4 == 0)
									{
									?><!--<hr class="between" />--><?
								}
							}
							?>
						</div>
						<div class="arrleft" onClick="speaker_left();"><img src="images/arr_left.png" /></div>
						<div class="arrright" onClick="speaker_right();"><img src="images/arr_right.png" /></div>
					</div>
					<div class="spbullets group">
						<?
						$scnt = 0;
						foreach ($speakers as $speaker)
							{
							$scnt++;
							?>
							<div id="spbul_<?=$scnt;?>" class="spbullet<?if ($scnt == 1) {?> active<?}?>" onClick="speaker_go(<?=$scnt;?>);"></div>
							<?
						}
						?>
					</div>
					<h1>Speakers, Ortho-Breakfast</h1>
					<hr class="mobile" />
					<div class="speakersdiv">
						<div class="speakers1 group">
							<?
							foreach ($speakers2 as $speaker)
								{
								$scnt++;
								$arr_name = explode(" ", $speaker["name"]);
								?>
								<div class="speaker" onClick="$('.mask').fadeIn(300); $('#spdesc<?=$scnt;?>').fadeIn(300);">
									<img src="images/<?=$speaker["image"];?>" />
									<div class="speakerdata">
										<h4><?=$arr_name[0];?><br /><?=$arr_name[1];?> <?=$arr_name[2];?></h4>
									</div>
								</div>
								<div class="speaker_desc" id="spdesc<?=$scnt;?>">
									<div class="close" onClick="$('.mask').fadeOut(300); $('.speaker_desc').fadeOut(300);"><img src="images/close.png" /></div>
									<h4><?=$speaker["name"];?></h4>
									<h5><?=$speaker["short"];?></h5>
									<?=$speaker["about"];?>
								</div>
								<?
								if ($scnt > 0 && $scnt % 4 == 0)
									{
									?><!--<hr class="between" />--><?
								}
							}
							?>
						</div>
						<div class="arrleft" onClick="speaker1_left();"><img src="images/arr_left.png" /></div>
						<div class="arrright" onClick="speaker1_right();"><img src="images/arr_right.png" /></div>
					</div>
					<div class="spbullets1 group">
						<?
						$scnt = 0;
						foreach ($speakers2 as $speaker)
							{
							$scnt++;
							?>
							<div id="spbul_<?=$scnt;?>" class="spbullet<?if ($scnt == 1) {?> active<?}?>" onClick="speaker_go(<?=$scnt;?>);"></div>
							<?
						}
						?>
					</div>
					<a name="price"></a>
					<!-- Стоимость -->
					<h1>Participation Fee <sup>*</sup></h1>
					<div class="txtdiv">Please note participation fee may change in relation to the day of payment. We kindly advise you to make payment in advance</div>
					<div class="txtdiv">As a part of our event you may choose a format option that is more interesting for you. Optionally you may register only visiting Ormco Forum from June 01-03 and take part in ortho-breakfast and gala-dinner on June 02, 2018.  Please indicate (put a tick) in front of the activities you would like to take part in and click “Register” afterwards</div>
					<br />
					<a name="choose"></a>
					<h2>Choose your option:</h2>
					<div class="prices">
						<div class="pr_title">
						</div>
						<div class="price hdr">
							Prior to<br />April 15, 2018 
						</div>
						<div class="price hdr">
							After<br /> April 15, 2018 
						</div>
						<div class="pr_title">
							<div class="cb"><input type="checkbox" name="forum" id="e_forum" onClick="getprice(1, <?if (date("Y-m-d") < "2018-04-15") {?><?=$allprices[1]["price1"];?><?}else{?><?=$allprices[1]["price2"];?><?}?>, this.checked);" /><label for="e_forum">Main Forum Agenda (June, 1 - 3), tickets left: <?=$mkcounts[0];?></label></div>
						</div>
						<div class="price<?if (date("Y-m-d") >= "2018-04-15") {?> desktop<?}?>">
							<div class="cb"><?=number_format($allprices[1]["price1"], 0, ".", " ");?> RUR</div>
						</div>
						<div class="price<?if (date("Y-m-d") < "2018-04-15") {?> desktop<?}?>">
							<div class="cb"><?=number_format($allprices[1]["price2"], 0, ".", " ");?> RUR</div>
						</div>
						<?if (date("Y-m-d") < "2018-04-15") {?><div class="mobile small">(prior to April 15 2018, after April 15 2018 the fee is <?=number_format($allprices[1]["price2"], 0, ".", " ");?> RUR)</div><?}?>
						<div class="pr_title">
							<div class="cb"><input type="checkbox" name="bf" id="e_bf" onClick="getprice(2, <?if (date("Y-m-d") < "2018-04-15") {?><?=$allprices[2]["price1"];?><?}else{?><?=$allprices[2]["price2"];?><?}?>, this.checked);" /><label for="e_bf">Ortho-breakfast (June 2, morning), tickets left: <?=$mkcounts[1];?></label></div>
						</div>
						<div class="price<?if (date("Y-m-d") >= "2018-04-15") {?> desktop<?}?>">
							<div class="cb"><?=number_format($allprices[2]["price1"], 0, ".", " ");?> RUR</div>
						</div>
						<div class="price<?if (date("Y-m-d") < "2018-04-15") {?> desktop<?}?>">
							<div class="cb"><?=number_format($allprices[2]["price2"], 0, ".", " ");?> RUR</div>
						</div>
						<div class="pr_title">
							<div class="cb"><input type="checkbox" name="dinner" id="e_dinner" onClick="getprice(3, <?if (date("Y-m-d") < "2018-04-15") {?><?=$allprices[3]["price1"];?><?}else{?><?=$allprices[3]["price2"];?><?}?>, this.checked);" /><label for="e_dinner">Gala-dinner (June 2, evening), tickets left: <?=$mkcounts[2];?></label></div>
						</div>
						<div class="price<?if (date("Y-m-d") >= "2018-04-15") {?> desktop<?}?>">
							<div class="cb"><?=number_format($allprices[3]["price1"], 0, ".", " ");?> RUR</div>
						</div>
						<div class="price<?if (date("Y-m-d") < "2018-04-15") {?> desktop<?}?>">
							<div class="cb"><?=number_format($allprices[3]["price2"], 0, ".", " ");?> RUR</div>
						</div>
					</div>
					<div class="afterprices">
						Total cost based on options you chose: <div class="afterprice" id="eprice">0 RUR</div>
						<div class="button"><button id="startreg" type="button" onClick="if ($('#events').val() != '') {fixbody(); $('.mask').fadeIn(300); $('.openform').fadeIn(300);}else{showmes('Выберите способ участия!');}" disabled>Registration</button></div>
					</div>
					<!--<h5><sup>*</sup> The discount of 15% for members of Russian Community of Professional Orthodontists</h5>-->
					<br />
					Please note that the cost will be written off in local currency in accordance with the rate set by the bank that issued your card
					<br />
					<div class="spec" onClick="$('.mask').fadeIn(300); $('#spec').fadeIn(300);">Special Terms of Hotel Accommodation</div>
					<div class="txtdiv">We kindly ask you to take care of your hotel accommodation in advance and book your hotel stay at earliest convenience! In June Saint-Petersburg becomes to be one of the most beautiful and attractive cities, so the city is extremely popular with the tourists from all over the world. Meanwhile in spring and summer of 2018 several global events will be held in Saint Petersburg, among them: World  Economic Forum and FIFA World Cup and it is expected that many more guests will come to Saint-Petersburg during White Nights time</div>
					<br />
					<!-- Место -->
					<a name="place"></a>
					<h1>Venue</h1>
					<div class="txtdiv">
						“Corinthia” Hotel </h3><br />
						Saint-Petersburg, 57 Nevskiy pr.
					</div>
					<div class="places group">
						<div class="placeline"></div>
						<div class="placetabs group">
							<div class="placetab sel" onClick="$('.placetab.sel').removeClass('sel'); $(this).addClass('sel'); $('.map').fadeOut(300); $('.gallery').fadeIn(300);">Gallery</div>
							<div class="placetab" onClick="$('.placetab.sel').removeClass('sel'); $(this).addClass('sel'); $('.gallery').fadeOut(300); $('.map').fadeIn(300); initialize_map();">Map</div>
						</div>
						<div class="placeline"></div>
					</div>
					<div class="gallery">
						<div class="gal_slider group">
							<div class="gal_slide"><img src="images/building.jpg" /></div>
							<div class="gal_slide"><img src="images/building1.jpg" /></div>
							<div class="gal_slide"><img src="images/building2.jpg" /></div>
							<div class="gal_slide"><img src="images/building3.jpg" /></div>
							<div class="gal_slide"><img src="images/building4.jpg" /></div>
							<div class="gal_slide"><img src="images/building5.jpg" /></div>
							<div class="gal_slide"><img src="images/building6.jpg" /></div>
						</div>
						<div class="gal_navs">
							<div class="gal_nav" onClick="gallery_left();"><img src="images/arr_left.png" /></div>
							<div class="gal_nav" onClick="gallery_right();"><img src="images/arr_right.png" /></div>
						</div>
					</div>
					<div class="map"><div id="map"></div></div>
					<a name="form"></a>
					<h1>Registration</h1>
					<div class="formmiddle">
						<div class="formtable group">
							<img src="images/face.jpg" />						
							<div class="formtxt">
								Should you have any questions please contact us: +7&nbsp;(812)&nbsp;324-42-60
							</div>
						</div>
						<a href="#choose"><button type="button">Register</button></a>
					</div>
					<div class="section footer">
						Ormco Corporation &copy; 2017<br />
						<a href="http://ormco.ru">ormco.ru</a>
					</div>
				</div>
			</div>
		</div>
		<div class="openform">
			<div class="openform_">
				<div class="newform formbox1">
					<div class="formbox_close" onClick="unfixbody(); $('.mask').fadeOut(300); $('.openform').fadeOut(300);"><span class="shape1"></span><span class="shape2"></span></div>
					<h2>Registration</h2>
					<h4>Please enter your e-mail</h4>
					<input id="f_email" name="f_email" value="<?=$_SESSION["f_email"];?>" />
					<button type="button" onClick="chk_f_email();">More</button>
					<div class="f_anim">
						<div class="at">@</div>
						<div class="arr arr1">←</div>
						<div class="dash arr2"></div>
						<div class="dash arr3"></div>
						<div class="dash arr4"></div>
						<div class="arr arr5">→</div>
						<div class="db">
							<div class="db1"></div>
							<div class="db2"></div>
							<div class="db3"></div>
							<div class="db4"></div>
							<div class="db5"></div>
							<div class="db6"></div>
							<div class="db7"></div>
							<div class="db8"></div>
							<div class="db9"></div>
						</div>
						<div class="f_desc">Проверяем email на наличие в базе данных</div>
					</div>
					<div class="exists">Человек с данным Email уже зарегистрировался на данное мероприятие</div>
				</div>
				<div class="newform formbox2">
					<div class="formbox_close" onClick="unfixbody(); $('.mask').fadeOut(300); $('.openform').fadeOut(300);"><span class="shape1"></span><span class="shape2"></span></div>
					<h2>Вход в систему</h2>
					<h4>Email найден! Пожалуйста, введите пароль от вашей учетной записи Ormco (сайты ormco.ru или orthodontia.ru), чтобы зарегистрироваться на семинар</h4>
					<div class="newform_field group">
						<div class="newform_ttl">Логин (Email):</div>
						<div class="newform_fld"><input id="e_email" name="e_email" value="<?=$_SESSION["f_email"];?>" /></div>
					</div>
					<div class="newform_field group">
						<div class="newform_ttl">Пароль:</div>
						<div class="newform_fld"><input type="password" id="e_pass" name="e_pass" value="" /><img onClick="chg_passstate();" src="images/eye.svg" /></div>
					</div>
					<div class="newform_link"><span onClick="remember_pass();">Вспомнить пароль</span></div>
					<div id="rp_ok" class="form_msg">Письмо с дальнейшими инструкциями по восстановлению пароля отправлены на указанный email</div>
					<div id="rp_fail" class="form_msg"></div>
					<div class="newform_field group">
						<div class="newform_ttl"></div>
						<div class="newform_fld"><button type="button" id="entry_but" onClick="chk_entry();">Войти</button></div>
					</div>
					<div class="exists">Человек с данным Email уже зарегистрировался на данное мероприятие</div>
					<div class="wrong">Пароль или логин указаны неверно</div>
				</div>
				<div class="newform formbox3">
					<div class="formbox_close" onClick="unfixbody(); $('.mask').fadeOut(300); $('.openform').fadeOut(300);"><span class="shape1"></span><span class="shape2"></span></div>
					<h2>Registration on the event</h2>
					<div class="notfound red hidden">Please fill in the form below to register your participation and to create Ormco Russia Account:</div>
					<form action="https://orthodontia.ru/emarket/lporderSmart/" method="post" onSubmit="return chkform(1);" enctype="multipart/form-data">
					<input type="hidden" name="regularprice" id="regularprice" value="17000" />
					<input type="hidden" name="mclasses1" id="mclasses1" value="" />
					<input type="hidden" name="mclasses2" id="mclasses2" value="" />
					<input type="hidden" name="mcl1" id="mcl1" value="" />
					<input type="hidden" name="mcl2" id="mcl2" value="" />
					<input type="hidden" name="p1" id="p1" value="" />
					<input type="hidden" name="p2" id="p2" value="" />
					<input type="hidden" name="file" id="file" value="" />
					<input type="hidden" name="events" id="events" value="<?=$ids[100];?>" />
					<div class="partbuts">
						<!--<div class="partbut active partbut1" onClick="showpart(2);">Личные данные</div> → <div class="partbut partbut2" onClick="showpart(2);">Способ оплаты</div>-->
						<h4 class="partbut1">Personal information</h4>
						<h4 class="partbut2 hidden">You can pay online with you banking or credit card</h4>
					</div>
					<hr class="hr">
					<div class="part1">
						<div class="newform_field group">
							<div class="newform_ttl">Email <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="email" id="email" /></div>
						</div>
						<!--<div class="newform_field group phide">
							<div class="newform_ttl"></div>
							<div class="newform_fld small">На этот email будет отправлено подтверждение регистрации и ссылка для активации вашей учетной записи Ormco</div>
						</div>-->
						<div class="newform_field group phide">
							<div class="newform_ttl">Password <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="password" size="30" name="password" id="password" /></div>
						</div>
						<div class="newform_field group phide">
							<div class="newform_ttl">Repeat the password <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="password" size="30" name="password2" id="password2" /></div>
						</div>
						<br />
						<div class="newform_field group">
							<div class="newform_ttl">Surname <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="lname" id="lname" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Name <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="fname" id="fname" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Father’s name <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="father_name" id="father_name" /></div>
						</div>
						<div class="newform_field group" id="birthday">
							<div class="newform_ttl">Date of birth <sup>*</sup></div>
							<div class="newform_fld"><input placeholder="" class="date1 ness" type="text" size="20" name="bd" id="bd" readonly /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Cell phone number <sup>*</sup></div>
							<div class="newform_fld"><input placeholder="+7 (987) 1234567" class="short ness" type="text" size="25" name="phone" id="phone" /></div>
						</div>
						<input type="hidden" name="country" id="country" value="Другая" />
						<input type="hidden" name="region" id="region" value="16761" />
						<!--<div class="newform_field group">
							<div class="newform_ttl">Страна<sup>*</sup></div>
							<div class="newform_fld"><select name="country" id="country" class="ness" onChange="if (this.value == 'Россия') {$('.region_field').fadeIn(300);}else{$('.region_field').fadeOut(300);}">
									<option value="Россия">Россия</option>
									<option value="Азербайджан">Азербайджан</option>
									<option value="Армения">Армения</option>
									<option value="Белоруссия">Белоруссия</option>
									<option value="Грузия">Грузия</option>
									<option value="Украина">Украина</option>
									<option value="Другая">Другая</option>
								</select>
							</div>
						</div>
						<div class="newform_field group region_field">
							<div class="newform_ttl">Регион <sup>*</sup></div>
							<div class="newform_fld"><select name="region" id="region" class="ness">
								<option value="">-= выберите регион =-</option>
								<option value="9870">Москва и МО</option>
								<option value="9871">Санкт-Петербург и ЛО</option>
								<option value="9872">Северо-Западный</option>
								<option value="9873">Центральный</option>
								<option value="9874">Сибирский</option>
								<option value="9875">Приволжский</option>
								<option value="9876">Южный</option>
								<option value="9877">Северо-Кавказский</option>
								<option value="9878">Уральский</option>
								<option value="9879">Дальневосточный</option>
								<option value="16761">Не Россия</option>
							</select></div>
						</div>-->
						<div class="newform_field group">
							<div class="newform_ttl">City <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="city" id="city" /></div>
						</div>
						<div class="newform_field group" id="speciality">
							<div class="newform_ttl">Choose you specialty <sup>*</sup></div>
							<div class="newform_fld"><select name="who" id="who" class="ness">
									<option value="">Choose you specialty</option>
									<option value="Ортодонт">Orthodontist</option>
									<option value="Хирург">Surgeon</option>
									<option value="Другая специализация">Other</option>
									<option value="Не врач">Not a doctor</option>
								</select>
							</div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Name of your practice/Where do you study <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="company" id="company" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl"></div>
							<div class="newform_fld"><button type="button" id="entry_but" onClick="showpart(2);">More</button></div>
						</div>
					</div>
					<div class="part2 hidden">
						<!--<h3>Выберите скидку. Обратите внимание: скидка будет направлена менеджеру Ormco для подтверждения</h3>
						<div class="newform_field group">
							<div class="newform_flda"><input type="checkbox" class="upl" name="poo" id="poo" value="Член ПОО" onClick="$('.newform_upload').fadeIn(300); $('#uploaded').fadeIn(300); setdiscount(15, 'poo', this.checked);" /> <label for="poo">Являюсь членом Профессионального общества ортодонтов</label></div>
						</div>
						<div class="newform_field group">
							<div class="newform_flda"><input type="checkbox" name="school" id="school" value="Действующий участник Школы ортодонтии" onClick="$('.newform_upload').fadeOut(300); $('#uploaded').fadeOut(300); setdiscount(50, 'school', this.checked);" /> <label for="school">Действующий участник Школы ортодонтии</label></div>
						</div>
						<div class="newform_field group">
							<div class="newform_flda"><input type="checkbox" class="upl" name="ordinator" id="ordinator" value="Ординатор" onClick="$('.newform_upload').fadeIn(300); $('#uploaded').fadeIn(300); setdiscount(40, 'ordinator', this.checked);" /> <label for="ordinator">Я ординатор</label></div>
						</div>-->
						<div class="newform_field newform_upload hidden group">
							<div class="newform_fldu">Загрузить удостоверение <button type="button" class="upload" name="upload" id="upload">Выберите файл</button></div>
						</div>
						<div class="uploaded hidden" id="uploaded">(форматы jpg, gif, png или pdf)</div>
						<!--<div class="discount hidden">
							Ваша скидка <span id="discount">0</span> процентов
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Оплата:</div>
							<div class="newform_fld"><div class="tabbut active tabbut1" onClick="showtab(1);">Как физ. лицо</div><div class="tabbut tabbut2" onClick="showtab(2);">Как юр. лицо</div></div>
						</div>-->
						<div class="note">Please note that the participation fee will be debited from your credit card in your currency at the rate of the bank that issued the card</div>
						<div class="finalprice">
							Participation Fee <span id="finalprice">0 RUR</span>
						</div>
						<div class="tab1">
							<div class="newform_field group">
								<div class="newform_ttl">Payment:</div>
								<div class="newform_fld"><select name="payment_id" id="payment_id" class="ness" onChange="if (this.value == 4666) {showtab(2);}">
										<option value="17688" comment="">Pay online right now</option>
									</select>
								</div>
							</div>
						</div>
						<div class="tab2 hidden">
							<div class="newform_field group yurs">
								<div class="newform_ttl">Юр. лицо:</div>
								<div class="newform_fld"><select name="yur" id="yur" class="ness" disabled>
									</select>
								</div>
							</div>
							<div class="addyur" onClick="addnewyur();">
								<span>+</span> Добавить новое юр. лицо
								<small>Приготовьтесь ввести реквизиты юр. лица</small>
							</div>
							<div class="addyurform addyurform1 hidden">
								<div class="addyur_close" onClick="$('.addyurform').fadeOut(300, function(){$('.newform_reg').fadeIn(300); $('.addyur').fadeIn(300); $('.yurs').fadeIn(300);})"><span class="shape1"></span><span class="shape2"></span></div>
								<div class="newform_field group">
									<div class="newform_ttl">Новое юр. лицо:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="yurname" id="yurname" /><small>Введите юридическое название организации</small></div>
								</div>
								<h4>Контактная информация</h4>
								<div class="newform_field group">
									<div class="newform_ttl">ФИО:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="fio_rukovoditelya" id="fio_rukovoditelya" /><small>ФИО руководителя организации</small></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Должность:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="dolzhnost" id="dolzhnost" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Контактное лицо:</div>
									<div class="newform_fld"><input type="text" size="30" name="contact_person" id="contact_person" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Email<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="yuremail" id="yuremail" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Телефон</div>
									<div class="newform_fld"><input class="short" type="text" size="30" name="phone_number" id="phone_number" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Факс</div>
									<div class="newform_fld"><input class="short" type="text" size="30" name="fax" id="fax" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl"></div>
									<div class="newform_fld"><button type="button" onClick="showyur(2);">More</button></div>
								</div>
							</div>
							<div class="addyurform addyurform2 hidden">
								<div class="addyur_back" onClick="showyur(1);"></div>
								<div class="addyur_close" onClick="$('.addyurform').fadeOut(300, function(){$('.newform_reg').fadeIn(300); $('.addyur').fadeIn(300); $('.yurs').fadeIn(300);})"><span class="shape1"></span><span class="shape2"></span></div>
								<div class="newform_field group">
									<div class="newform_ttl">ИНН:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="inn" id="inn" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">КПП:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="kpp" id="kpp" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Рассчетный счет:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="account" id="account" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">БИК:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness short" type="text" size="30" name="bik" id="bik" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Корр. счет:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="bank_account" id="bank_account" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">ОГРН:</div>
									<div class="newform_fld"><input type="text" size="30" name="ogrn" id="ogrn" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Юридический адрес:<sup>*</sup></div>
									<div class="newform_fld"><textarea class="ness" name="legal_address" id="legal_address"></textarea></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Фактический адрес:<sup>*</sup></div>
									<div class="newform_fld"><textarea class="ness" name="defacto_address" id="defacto_address"></textarea></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Почтовый индекс:</div>
									<div class="newform_fld"><input type="text" size="30" name="postal_address" id="postal_address" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl"></div>
									<div class="newform_fld"><button type="button" onClick="addyurajax();">Добавить юр. лицо</button></div>
								</div>
							</div>
						</div>
						<!--<div id="igosel">
							<h4>Лекция 8 февраля (осталось <?=$mkcounts[0];?> мест)</h4>
						</div>-->
						<div class="newform_field newform_reg group">
							<div class="newform_ttl"></div>
							<div class="newform_fld"><button id="register">Register</button></div>
						</div>
						<!--<div class="newform_field group">
							<div class="newform_ttla double">Планирую покупку продукции Ormco в ближайшее время (1-2 месяца)</div>
							<div class="newform_flda"><input type="checkbox" size="30" name="want_to_buy" id="want_to_buy" value="1" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttla double">Принимал(а) участие в образовательных мероприятиях Ormco в течение последнего года</div>
							<div class="flda"><input type="checkbox" size="30" name="how_you_been_edu" id="how_you_been_edu" value="1" /></div>
						</div>
						<div id="igosel">
							<h3>Выбранные мастер-классы</h3>
							<div class="igosel" onClick="$('.igodd').fadeToggle(200); $(this).toggleClass('sel');"><span>Выберите три мастер-класса</span></div>
							<div class="igodd">
								<?
								foreach ($mks as $n => $nks)
									{
									?>
									<div class="igotime igo<?=$n;?>"><?=$mktimes[$n];?></div>
									<?
									foreach ($nks as $m => $mk)
										{
										?>
										<div class="igoset igo<?=$n;?><?if ($mkcounts[$m - 1] <= 0) {?> faded cancelled<?}?>" data-group="<?=$n;?>" data-mk="<?=$m;?>" id="igo_<?=$m;?>"><input type="checkbox" <?if ($mkcounts[$m - 1] <= 0) {?>disabled <?}?>name="iset<?=$m;?>" id="iset<?=$m;?>" onClick="igo(<?=$n;?>, <?=$m;?>, '<strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?>', this.checked);" /><label for="iset<?=$m;?>"><strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?></label></div>
										<?
									}
								}
								?>
							</div>
							<div class="chosen"></div>
						</div>-->
						<div class="formagree">By pressing Register button you approve <span class="lnk" onClick="$('#pol').fadeIn(300);">Privacy policy</span> and <a target="_blank" href="https://orthodontia.ru/files/sys/Dogovor-prisoedineniya-Ormco.docx">terms of the Agreement</a></div>
					</div>
					</form>
				</div>
			</div>
		</div>
		<div class="mask"></div>
		<div class="mask1"></div>
		<div class="burger" onClick="$('#mobmenu').slideDown(300);"><img src="images/burger.png" /></div>
		<div id="mobmenu" class="mobmenu">
			<div class="close2" onClick="$('#mobmenu').slideUp(300);"><img src="images/close2.png" /></div>
			<ul>
				<li id="miprogramm1"><a href="#programm">Agenda</a></li>
				<li id="migala"><a href="#speakers">Gala-dinner</a></li>
				<li id="mispeakers"><a href="#gala">Speakers</a></li>
				<li id="miprice1"><a href="#price">Participation Fee</a></li>
				<li id="miplace1"><a href="#place">Venue</a></li>
				<li id="miform1"><a href="#choose">Registration</a></li>
			</ul>
		</div>
	<div class="form_error">
		<div class="close" onClick="$('.form_error').fadeOut(300); $('.mask1').fadeOut(300);"><img src="images/close.png" /></div>
		<h3 id="form_error"></h3>
	</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter23063434 = new Ya.Metrika({
                    id:23063434,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    ecommerce:"dataLayer"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/23063434" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-67881160-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-67881160-2');
</script>
<div id="pol">
<div class="close" onClick="$('#pol').fadeOut(300);"><img src="images/close.png" /></div>
<div id="inpol">
<h2>Политика конфиденциальности</h2>
<h4>1. Общие положения</h4>
Корпорация Ormco в лице ООО «Ормко» (More «Ормко») и её аффилированные лица с уважением относятся к конфиденциальной информации любого лица, ставшего посетителем данного сайта. Мы хотели бы проинформировать Вас о том, какие именно данные мы собираем и каким образом используем собранные данные. Вы также узнаете о том, как Вы можете проверить точность собранной информации и дать нам указание об удалении подобной информации. Данные собираются, обрабатываются и используются строго в соответствии с требованиями действующего законодательства того государства, в котором расположено соответствующее аффилированное лицо компании "Ормко", отвечающее за защиту персональных данных. Мы делаем все возможное для обеспечения соответствия требованиям действующего законодательства.
Данное заявление не распространяется на сайты, на которые сайт компании "Ормко" содержит гиперссылки.
<h4>2. Сбор, использование и переработка персональных данных</h4>
Мы осуществляем сбор информации, относящейся к определенным лицам, лишь в целях обработки и использования информации и только в том случае, если Вы добровольно предоставили информацию или явно выразили свое согласие на ее использование.
Когда Вы посещаете наш сайт, определенные данные автоматически записываются на серверы компании "Ормко" и/или её аффилированных лиц для целей системного администрирования или для статистических или резервных целей. Записываемая информация содержит название Вашего интернет-провайдера, в некоторых случаях Ваш IP-адрес, данные о версии программного обеспечения Вашего браузера, об операционной системе компьютера, с которого Вы посетили наш сайт, адреса сайтов, после посещения которых Вы по ссылке зашли на наш сайт, заданные Вами параметры поиска, приведшие Вас на наш сайт.
В зависимости от обстоятельств, подобная информация позволяет сделать выводы о том, какая аудитория посещает наш сайт. В данном контексте, однако, не используются никакие персональные данные. Использоваться может лишь анонимная информация. Если информация передается компанией "Ормко" и/или её аффилированными лицами внешнему провайдеру, принимаются все возможные технические и организационные меры, гарантирующие передачу данных в соответствии с требованиями действующего законодательства.
Если Вы добровольно предоставляете нам персональную информацию, мы обязуемся не использовать, не обрабатывать и не передавать такую информацию способом, выходящим за рамки требований действующего законодательства. Использование и распространение Ваших персональных данных без Вашего согласия возможно только на основании судебного решения или в ином порядке, предусмотренном законодательством РФ.
Любые изменения, которые будут внесены в правила по соблюдению конфиденциальности, будут размещены на данной странице. Это позволяет Вам в любое время получить информацию о том, какие данные у нас хранятся и о том, каким образом мы собираем и храним такие данные.
<h4>3. Безопасность данных</h4>
Компания "Ормко" и её аффилированные лица обязуется бережно хранить Ваши персональные данные и принимать все меры предосторожности для защиты Ваших персональных данных от утраты, неправильного использования или внесения в персональные данные изменений. Партнеры компании "Ормко" и её аффилированных лиц, которые имеют доступ к Вашим данным, необходимым им для предоставления Вам услуг от имени компании "Ормко" и её аффилированных лиц, несут перед компанией "Ормко" и её аффилированными лицами закрепленные в контрактах обязательства соблюдать конфиденциальность данной информации и не имеют права использовать предоставляемые данные для каких-либо иных целей.
<h4>4. Персональные данные несовершеннолетних потребителей</h4>
Компания "Ормко" и её аффилированные лица не ведет сбор информации в отношении потребителей, не достигших 14 лет. При необходимости, мы можем специально попросить ребенка не присылать в наш адрес никакой личной информации. Если родители или иные законные представители ребенка обнаружат, что дети сделали какую-либо информацию доступной для компании "Ормко" и её аффилированных лиц, и сочтут, что предоставленные ребенком данные должны быть уничтожены, таким родителям или иным законным представителям необходимо связаться с нами по нижеуказанному (см. п. 6) адресу. В этом случае мы немедленно удалим личную информацию о ребенке.
<h4>5. Файлы Cookie</h4>
Мы используем файлы cookie для того, чтобы предоставить Вам больше возможностей при использовании сайта. Файлы cookie представляют собой небольшие блоки данных, помещаемые Вашим браузером на временное хранение на жестком диске Вашего компьютера, необходимые для навигации по нашему сайту. Файлы cookie не содержат никакой личной информации о Вас и не могут использоваться для идентификации отдельного пользователя. Файл cookie часто содержит уникальный идентификатор, представляющий собой анонимный номер (генерируемый случайным образом), сохраняемый на Вашем устройстве. Некоторые файлы удаляются по окончании Вашего сеанса работы на сайте; другие остаются на Вашем компьютере дольше.
Приступая к использованию данного веб-сайта, вы соглашаетесь на использование файлов cookie, Вы также признаете, что в подобном контенте могут использоваться свои файлы cookie.
Компания Ормко не контролирует и не несет ответственность за файлы cookie сторонних разработчиков. Дополнительную информацию Вы можете найти на сайте разработчика.
<h4>6. Отслеживание через интернет</h4>
На данном сайте осуществляется сбор и хранение данных для маркетинга и оптимизации с использованием технологии Яндекс.Метрика и Google Analitycs. Эти данные могут использоваться для создания профилей пользователей под псевдонимами. Сайт может устанавливать файлы cookie.
Без ясно выраженного согласия наших пользователей данные, собираемые с помощью технологий Яндекс.Метрика и Google Analitycs, не используются для идентификации личности посетителя и не связываются с какими-либо другими личными данными носителя псевдонима.
Дополнительную информацию об отслеживании через интернет Вы можете найти на сайтах провайдеров сервисов Яндекс.Метрика и Google Analitycs.
<h4>7. Ваши пожелания и вопросы</h4>
Хранящиеся данные будут стерты компанией "Ормко" и/или её аффилированными лицами по истечении периода хранения, установленного законодательством или договором либо в случае если сама компания "Ормко" и/или её аффилированные лица отменит хранение тех или иных данных. Вы вправе в любое время потребовать удаления из базы данных компании "Ормко" и/или её аффилированных лиц информации о Вас. Вы также вправе в любое время отозвать Ваше согласие на использование или переработку Ваших персональных данных. В таких случаях, а также, если у Вас есть какие-либо иные пожелания, связанные с Вашими персональными данными, просим Вас выслать письмо по почте в адрес «Ормко» в России по адресу: 195112, Санкт-Петербург, Малоохтинский пр-т, д. 64, корп. 3. или по электронной почте dc-sales@ormco.com. Просим Вас также связаться с нами в случае, если Вам хотелось бы узнать, собираем ли мы данные о Вас и если да, то какие именно данные. Мы постараемся выполнить Ваши пожелания в возможно короткие сроки.
<h4>8. Законодательство по обработке персональных данных</h4>
Все действия с персональными данными, собираемыми на данном сайте, производятся в соответствии с Федеральным законом Российской Федерации №152-ФЗ от 27 июля 2006 года «О персональных данных».
<h4>(1) Заявленная цель сбора, обработки или использования данных:</h4>
•	Предметом деятельности «Ормко» и её аффилированных лиц является производство и распространение стоматологических продуктов всех типов, главным образом брекет-систем, ортодонтических инструментов, микроимплантов, адгезивов;
<h4>(2) Описание групп вовлеченных лиц и соответствующих данных или категорий данных:</h4>
Данные, касающиеся заказчиков, сотрудников, пенсионеров, сотрудников сторонних компаний (субподрядчиков), персонала, работающего по лизингу, претендентов на рабочие места, авторов изобретений, не являющихся сотрудниками компаний, или наследников, соответственно, поставщиков товаров и услуг, сторонних заказчиков, потребителей, добровольцев, участвующих в потребительских испытаниях, посетителей производственных объектов корпорации, инвесторов – насколько это необходимо для выполнения целей, определенных в пункте 4.
<h4>(3) Получатели или категории получателей, которым могут быть разглашены данные:</h4>
Органы власти, фонды страхования здоровья, ассоциация по страхованию ответственности работодателей при наличии соответствующего правового регулирования, сторонние подрядчики в соответствии сторонние поставщики услуг, ассоциация пенсионеров «Ормко», аффилированные лица и внутренние подразделения для выполнения целей, указанных в пункте 4.
<h4>(4) Периодичность регулярного удаления данных:</h4>
Юристами подготовлено множество инструкций, касающихся обязанностей по хранению данных и периодов хранения. Данные удаляются в установленном порядке по истечении указанных периодов. Данные, не подпадающие под действие данных условий, удаляются, если цели, указанные в пункте 4, перестают существовать.
<h4>(5) Запланированная передача данных другим странам:</h4>
В рамках всемирной системы информации о трудовых ресурсах, данные по персоналу должны быть доступны определенным руководящим работникам в других странах. Соответствующие соглашения о защите данных должны быть заключены с соответствующими компаниями в соответствии со стандартами ЕС.
<h4>9. Использование встраиваемых модулей для социальных сетей</h4>
На наших интернет-страницах предусмотрена возможность встраивания модулей для социальных сетей ВКонтакте, Twitter, Instagram, Youtube (More – «провайдеры»).
Только если Вы активируете модуль, тем самым разрешая передачу данных, браузер создаст прямое соединение с сервером провайдера. Содержимое различных модулей впоследствии передается соответствующим провайдером непосредственно в Ваш браузер и выводится на экран Вашего компьютера.
Модуль сообщает провайдеру, на какую из страниц нашего сайта Вы вошли. Если во время просмотра нашего сайта Вы вошли на ВКонтакте, Instagram, Youtube или Twitter под своей учетной записью, провайдер может подобрать информацию, в соответствии с Вашими интересами, т.е. информацию, которую Вы просматриваете с помощью Вашей учетной записи. При использовании какой-либо функции встроенного модуля (например, кнопки “Мне нравится”, размещения комментария), эта информация также будет передана браузером непосредственно провайдеру для сохранения.
Дополнительную информацию по сбору и использованию данных социальными сетями, а также по правам и возможностям защиты Вашей конфиденциальности в данных обстоятельствах, можно найти в рекомендациях провайдеров по защите данных /конфиденциальности:
Для того, чтобы не подключаться к учетным записям провайдеров при посещении нашего сайта, Вам необходимо отключиться от соответствующей учетной записи перед посещением наших интернет-страниц.
</div>
</div>
	</body>
</html>
<?
function ourdate($dt, $incly) {
	$newdt = date("M" ,strtotime($dt)).", ".date("d" ,strtotime($dt))." ".date("Y",strtotime($dt));
	return $newdt;
}
function places($num) {
	$rest = $num % 10;
	if ($num > 4 && $num < 21)
		return "мест";
	else {
		if ($num > 1 && $num < 5)
			return "места";
		else {
			if ($num == 1 || $rest == 1)
				return "место";
			else {
				if ($num > 20 && $rest > 1 && $rest < 5)
					return "места";
				else
					return "мест";
			}
		}
	}
}
?>