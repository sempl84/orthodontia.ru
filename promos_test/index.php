<?php
session_start();
header("HTTP/1.1 200 OK");
header('Content-Type: text/html; charset=UTF-8');

function places($num) {
	$rest = $num % 10;
	if ($num > 4 && $num < 21)
		return "мест";
	else {
		if ($num > 1 && $num < 5)
			return "места";
		else {
			if ($num == 1 || $rest == 1)
				return "место";
			else {
				if ($num > 20 && $rest > 1 && $rest < 5)
					return "места";
				else
					return "мест";
			}
		}
	}
}

$speakers = array(
	1 => array(	"name" => "С.А. Попов",
				"short" => "Доктор медицинских наук",
				"about" => "<p>Доктор медицинских наук, доцент, заведующий кафедрой ортодонтии СЗГМУ им. И.И. Мечникова, один из ведущих врачей-ортодонтов России</p>",
				"image" => "popov.jpg",
	)
);
$mkcounts = explode("/", file_get_contents("mkcounts.txt"));
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Семинар С.А. Попова 3 - 4 февраля 2018 года</title>
		<meta http–equiv="Content–Type" content="text/html; charset=UTF–8">
		<link rel="stylesheet" media="screen" type="text/css" href="main.css">
		<meta name="keywords" content="">
		<meta name="description" content="">
		<meta name="HandheldFriendly" content="true">
		<meta name="MobileOptimized" content="width">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="/favicon.gif" type="image/x-icon">
		<link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="js/jquery.touchSwipe.min.js" type="text/javascript"></script>
		<script src="js/ajaxupload.3.5.js" type="text/javascript"></script>
		<script src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyBoDhMbGeceqxv4_7SROd3YL1-0C-SyBGo" type="text/javascript"></script>
		<script>
			var userid = "";
			var promo_dsc = 0;
			function remember_pass() {
				var e_email = $("#e_email").val();
				if (isEmail(e_email)) {
					$.ajax({
						url:"https://orthodontia.ru/udata/emarket/forget_pas/" + e_email + "/.json",
						xhrFields: {
						  withCredentials: true
						},
						dataType: "json",
						success:function(data) {
							if (data.status == "success") {
								$("#rp_ok").fadeIn(300);
								setTimeout(function() {$("#rp_ok").fadeOut(300)}, 15000);
							}
							else {
								$("#rp_fail").text("Пользователь не найден в базе, попробуйте еще раз").fadeIn(300);
								setTimeout(function() {$("#rp_fail").fadeOut(300)}, 15000);
							}
						}
					});
				}
				else {
					$("#rp_fail").text("Укажите свой email!").fadeIn(300);
					setTimeout(function() {$("#rp_fail").fadeOut(300)}, 15000);
				}
			}
			function countdown() {
				var mcl1 = $("#mcl1").val();
				var mcl2 = $("#mcl2").val();
				$.ajax({
					type: "POST",
					url: "xp_countdown.php",
					timeout: 5000,
					data: "mcl1=" + mcl1 + "&mcl2=" + mcl2,
					success: function(data) {
					},
					error: function(data) {
					}
				});
			}
			function writedata() {
				var alldata = "";
				$('form input').each(function() {
					if ($(this).attr("id") != undefined)
						alldata += "&" + $(this).attr("id") + "=" + $(this).val();
				});
				$('form select').each(function() {
					if ($(this).attr("id") != undefined)
						alldata += "&" + $(this).attr("id") + "=" + $(this).val();
				});
				$.ajax({
					type: "POST",
					url: "xp_write.php",
					timeout: 5000,
					data: "alldata=" + alldata,
					success: function(data) {
					},
					error: function(data) {
					}
				});
			}
			function chk_promo() {
				var promo = $("#promo").val();
				if (promo == "")
					$("#promo_fail").fadeOut(300);
				var pr = $("#regularprice").val();
				var ev = $("#events").val();
				$.ajax({
					url:"https://orthodontia.ru/udata/emarket/check_promokod/" + ev + "/" + promo + "/.json",
					xhrFields: {
					  withCredentials: true
					},
					dataType: "json",
					success:function(data) {
						if (data.result == 1) {
							promo_dsc = parseInt(data.discount);
							$("#promo_fail").fadeOut(300);
							$(".dscnt").not("#promo_dscnt").prop("checked", false);
							$("#promo_dscnt").removeAttr("disabled").prop("checked", true).val("Скидка по промокоду " + promo_dsc + "%");
							$("#promo_d").text(promo_dsc);
							$("#promo_radio").fadeIn(300);
							$("#discount").text(promo_dsc);
							$(".discount").fadeIn(300);
							pr = pr - parseInt(pr*(promo_dsc/100));
							$("#finalprice").text(pr + " р.");
							$("#afterprice").text(pr + " р.");
							$("#promo_discount").val("Промокод \"" + promo + "\" (" + promo_dsc + "%) - " + pr + " руб.");
						}
						else {
							promo_dsc = 0;
							$("#promo_fail").fadeIn(300);
							$("#promo_dscnt").prop("checked", false).attr("disabled", "disabled").val("Скидка по промокоду 0%");
							$("#promo_d").text(0);
							$("#promo_radio").fadeOut(300);
							$("#discount").text(0);
							$(".discount").fadeOut(300);
							$("#finalprice").text(pr + " р.");
							$("#afterprice").text(pr + " р.");
							$("#promo_discount").val("Промокод \"" + promo + "\" (" + promo_dsc + "%) - " + pr + " руб.");
						}
					}
				});
			}
			function setdiscount(discount, element, chk) {
				$(".dscnt").not("#" + element).removeAttr("checked");
				var pr = $("#regularprice").val();
				if (chk) {
					$("#discount").text(discount);
					$(".discount").fadeIn(300);
					pr = pr - parseInt(pr*(discount/100));
				}
				else {
					$(".discount").fadeOut(300);
					$(".newform_upload").fadeOut(300);
					$("#uploaded").fadeOut(300);
				}
				$("#finalprice").text(pr + " р.");
				$("#afterprice").text(pr + " р.");
			}
			function addnewyur() {
				$(".yurs").fadeOut(300);
				$(".newform_reg").fadeOut(300);
				$(".addyur").fadeOut(300, function() {
					$(".addyurform1").fadeIn(300);
				});
			}
			function cancelyur() {
				$(".addyurform1").fadeOut(300, function() {
					$(".yurs").fadeIn(300);
					$(".newform_reg").fadeIn(300);
					$(".addyur").fadeIn(300);
				});
				$(".addyurform2").fadeOut(300, function() {
					$(".yurs").fadeIn(300);
					$(".newform_reg").fadeIn(300);
					$(".addyur").fadeIn(300);
				});
			}
			function addyurajax() {
				var tosend = "id=" + userid;
				$('.addyurform input').each(function(index) {
					var did = $(this).attr("id");
					if (did == "yurname")
						did = "name";
					if (did == "yuremail")
						did = "email";
					var dval = $(this).val();
					tosend += "&data[new][" + did + "]=" + dval;
				});
				$('.addyurform textarea').each(function(index) {
					var did = $(this).attr("id");
					var dval = $(this).val();
					tosend += "&data[new][" + did + "]=" + dval;
				});
				$.ajax({
					url:"https://orthodontia.ru/udata/emarket/add_legal_item/.json?" + tosend,
					xhrFields: {
					  withCredentials: true
					},
					dataType: "json",
					success:function(data) {
						if (data.result != 0) {
							$(".addyurform").fadeOut(300).promise().done(function() {
								// добавляем новое юр. лицо в select
								$('#yur').append($('<option>', {
										value: data.result,
										text : $("#yurname").val()
								}));
								$(".newform_reg").fadeIn(300);
								$(".addyur").fadeIn(300);
								$(".yurs").fadeIn(300);
							});
						}
						else {
							$(".addyurform").fadeOut(300, function() {
								$(".newform_reg").fadeIn(300);
								$(".addyur").fadeIn(300);
								$(".yurs").fadeIn(300);
							});
							alert("Новое юр. лицо не добавлено! Проверьте данные!");
						}
					}
				});
			}
			function showyur(n) {
				var foo = 0;
				if (n == 2) {
					$(".addyurform1 .ness").each(function() {
						if ($(this).val() == '') {
							if (foo == 0) {
								$('html,body').animate({scrollTop: $(this).offset().top - 100}, 500);
							}
							$(this).addClass('redfld');
							foo = 1;
						}
						else {
							$(this).removeClass('redfld');
						}
					});
				}
				if (foo == 0) {
					$(".addyurform" + (3 - n)).fadeOut(300, function() {
						$(".addyurform" + n).fadeIn(300);
					});
				}
			}
			function showtab(tab) {
				if (tab == 1)
					$("#yur").attr("disabled", "disabled");
				else
					$("#yur").removeAttr("disabled");
				$(".tab" + (3 - tab)).fadeOut(300);
				$(".tab" + tab).fadeIn(300);
				$(".tabbut" + (3 - tab)).removeClass("active");
				$(".tabbut" + tab).addClass("active");
			}
			function showpart(part) {
				if (chkform(2)) {
					if (userid == "") {
						$.ajax({
							url:"https://orthodontia.ru/udata/emarket/logout_user/.json",
							xhrFields: {
							  withCredentials: true
							},
							dataType: "json",
							success:function(data) {
								// авторизация на сайте
								var tosend = "id=" + userid;
								$('.part1 input').each(function(index) {
									var did = $(this).attr("id");
									var dval = $(this).val();
									tosend += "&data[new][" + did + "]=" + dval;
								});
								$('.part1 select').each(function(index) {
									var did = $(this).attr("id");
									var dval = $(this).val();
									tosend += "&data[new][" + did + "]=" + dval;
								});
								$.ajax({
									url:"https://orthodontia.ru/udata/users/lpreg/.json?" + tosend,
									dataType: "json",
									success:function(data) {
										$(".f_anim").css("opacity", 0);
										if (data.status == "successful") {
											// пользователь зарегистрирован
											user_id = data.user_id;
											$.ajax({
												url:"https://orthodontia.ru/udata/emarket/logout_user/.json",
												xhrFields: {
												  withCredentials: true
												},
												dataType: "json",
												success:function(data) {
													var newemail = $("#email").val();
													var newpassword = $("#password").val();
													$.ajax({
														xhrFields: {
														  withCredentials: true
														},
														dataType: "json",
														url:"https://orthodontia.ru/udata/emarket/get_user_info/" + newemail + "/" + newpassword + "/.json",
														success:function(data) {
															$(".part" + (3 - part)).fadeOut(300);
															$(".part" + part).fadeIn(300);
															$(".partbut" + (3 - part)).addClass("hidden");
															$(".partbut" + part).removeClass("hidden");
															$(".notfound").hide(0);
														}
													});
												}
											});
										}
										else {
											// неудачно, рисуем ошибку
											$("#form_error").html(data.result);
											$(".mask1").fadeIn(300);
											$(".form_error").fadeIn(300);
										}
									}
								});
							}
						});
					}
					else {
						$(".part" + (3 - part)).fadeOut(300);
						$(".part" + part).fadeIn(300);
						$(".partbut" + (3 - part)).addClass("hidden");
						$(".partbut" + part).removeClass("hidden");
					}
				}
			}
			function chk_entry() {
				$("#entry_but").text("Проверяем...").attr("disabled", "disabled");
				$(".wrong").fadeOut(300);
				var e_email = $("#e_email").val();
				var e_pass = $("#e_pass").val();
				var eventid = $("#events").val();
				$.ajax({
					url:"https://orthodontia.ru/udata/emarket/logout_user/.json",
					xhrFields: {
					  withCredentials: true
					},
					dataType: "json",
					success:function(data) {
						$.ajax({
							xhrFields: {
							  withCredentials: true
							},
							dataType: "json",
							url:"https://orthodontia.ru/udata/emarket/get_user_info/" + e_email + "/" + e_pass + "/.json",
							success:function(data) {
								$(".f_anim").css("opacity", 0);
								if (typeof data.extended === 'object') {
									// пользователь ввел правильные данные
									var user = data;
									$(".wrong").fadeOut(300);
									$.ajax({
										url:"https://orthodontia.ru/udata/emarket/is_first_purchase/" + e_email + "/" + eventid + "/.json",
										xhrFields: {
										  withCredentials: true
										},
										dataType: "json",
										success:function(data) {
											$(".f_anim").css("opacity", 0);
											if (data.result == 1) {
												// пользователь уже зарегистрирован на мероприятие, выводим надпись
												$(".exists").css("opacity", 1);
											}
											else {
												userid = user.id;
												// alert(userid);
												// пользователь еще не зарегистрирован, все ОК, рисуем форму регистрации на мероприятие
												$(".openform").fadeOut(300, function() {
													$(".formbox2").hide(0);
													// console.table(user);
													// вставляем поля
													arruser = new Array();
													var allprops = user.extended.groups.group[0].property;
													$.each(allprops, function(index, value) {
														// console.log(this.name);
														arruser[this.name] = this.value;
													});
													if (typeof user.extended.groups.group[1] == "object") {
														var allprops = user.extended.groups.group[1].property;
														$.each(allprops, function(index, value) {
															// console.log(this.name);
															arruser[this.name] = this.value;
														});
													}
													// console.table(arruser);
													$("#fname").val(arruser["fname"].value);
													$("#lname").val(arruser["lname"].value);
													$("#father_name").val(arruser["father_name"].value);
													$("#email").val(arruser["e-mail"].value);
													$("#phone").val(arruser["phone"].value);
													$("#city").val(arruser["city"].value);
													$("#company").val(arruser["company"].value);
													$("#bd").val(arruser["bd"].value);
													$("#country").val(arruser["country"].item.name);
													$("#region").val(arruser["region"].item.id);
													if (typeof arruser["prof_status"] == "object")
														$("#who").val(arruser["prof_status"].item.name);
													// убираем пароли из формы
													$(".phide").hide();
													// рисуем форму
													$(".formbox3").show(0);
													$(".openform").fadeIn(300);
												});
												// подгружаем юр. лиц
												$.ajax({
													url:"https://orthodontia.ru/udata/emarket/legalList/.json",
													xhrFields: {
													  withCredentials: true
													},
													dataType: "json",
													success:function(data1) {
														// alert(data1.items.length);
														// добавляем юр. лица в select
														$.each(data1.items.item, function(index, value) {
															$('#yur').append($('<option>', {
																	value: value.id,
																	text : value.name
															}));
														});
														// console.table(data1.items);
													}
												});
											}
										},
										error:function(data) {
										}
									});
								}
								else {
									// пользователь ввел неверные данные
									$("#e_email").addClass("invalid");
									$("#e_pass").addClass("invalid");
									$(".wrong").fadeIn(300, function() {
										$("#entry_but").text("Войти").removeAttr("disabled");
									});
								}
							}
						});
					}
				});
			}
			function fixbody() {
				$("body").addClass("hold");
			}
			function unfixbody() {
				$("body").removeClass("hold");
			}
			function chg_passstate() {
				if ($("#e_pass").attr("type") == "password") {
					$("#e_pass").attr("type", "text");
				}
				else {
					$("#e_pass").attr("type", "password");
				}
			}
			function chk_f_email() {
				var f_email = $("#f_email").val();
				var eventid = $("#events").val();
				if (f_email != "" && isEmail(f_email)) {
					$(".exists").fadeOut(300);
					$(".f_anim").css("opacity", 1);
					$("#f_email").removeClass("invalid");
					// проверяем email
					$.ajax({
						url:"https://orthodontia.ru/udata/emarket/is_user_exist/" + f_email + "/.json",
						dataType: "json",
						success:function(data) {
							$(".f_anim").css("opacity", 0);
							if (data.result == 1) {
								// пользователь найден, проверяем, есть ли регистрация на мероприятие
								$.ajax({
									url:"https://orthodontia.ru/udata/emarket/is_first_purchase/" + f_email + "/" + eventid + "/.json",
									dataType: "json",
									success:function(data) {
										$(".f_anim").css("opacity", 0);
										if (data.result == 1) {
											// пользователь уже зарегистрирован на мероприятие, выводим надпись
											$(".exists").fadeIn(300);
										}
										else {
											// еще не зарегистрирован, рисуем форму входа
											$(".openform").fadeOut(300, function() {
												$("#email").val($("#f_email").val());
												$(".formbox1").hide(0);
												$("#e_email").val(f_email);
												$(".formbox2").show(0);
												$(".openform").fadeIn(300);
											});
										}
									}
								});
							}
							else {
								// пользователь не найден, отображаем форму регистрации пользователя
								$(".openform").fadeOut(300, function() {
									$("#email").val($("#f_email").val());
									$(".notfound").show(0);
									$(".formbox1").hide(0);
									$(".formbox3").show(0);
									$(".openform").fadeIn(300);
								});
							}
						},
						error:function(data) {
						}
					});
				}
				else {
					$("#f_email").addClass("invalid");
				}
			}
			jQuery(function($){
				$.datepicker.regional['ru'] = {
					closeText: 'Закрыть',
					prevText: '&#x3C;Пред',
					nextText: 'След&#x3E;',
					currentText: 'Сегодня',
					monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
					'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
					monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
					'Июл','Авг','Сен','Окт','Ноя','Дек'],
					dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
					dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
					dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
					weekHeader: 'Нед',
					dateFormat: 'yy-mm-dd',
					firstDay: 1,
					isRTL: false,
					showMonthAfterYear: false,
					yearSuffix: ''};
				$.datepicker.setDefaults($.datepicker.regional['ru']);
			});
			$(function() {
				$(".date1").datepicker({
					changeYear: true,
					yearRange: '1920:2020',
					defaultDate: '1980-01-01'
				});
			});
			lnks = new Array("form", "place", "price", "speakers", "programm", "about");
			var gmargin = 797;
			var curgal = 1;
			var maxx = 5;
			var gmargin1 = 797;
			var curgal1 = 1;
			var maxx1 = 12;
			var igos = 0;
			var totigos = 2;
			var prods = 0;
			var filedone = 0;
			function pset(ord, val, state) {
				if (state) {
					prods++;
					$("#p" + ord).val(val);
					$(".prodsel span").text("Выбрано " + prods);
				}
				else {
					prods--;
					$("#p" + ord).val("");
					if (prods == 0)
						$(".prodsel span").text("-------");
					else
						$(".prodsel span").text("Выбрано " + prods);
				}
			}
			function igo(group, ord, theme, state, similar) {
				if (state) {
					igos++;
					var arr_s = similar.split(",");
					$(".prog[data-group=" + group + "]").addClass("faded").find(".igo").fadeOut(200);
					$(".prog[data-mk=" + ord + "]").removeClass("faded").find(".igo").fadeIn(200);
					$(".igoset[data-group=" + group + "]").addClass("faded").find("input").attr("disabled", "disabled");
					$(".igoset[data-mk=" + ord + "]").removeClass("faded").find("input").removeAttr("disabled").attr("checked", "checked");;
					$.each(arr_s, function(key, val) {
						$(".prog[data-mk=" + val + "]").addClass("faded").find(".igo").fadeOut(200);
						$(".igoset[data-mk=" + val + "]").addClass("faded").find("input").attr("disabled", "disabled");
					})
					$("#mk" + ord).parent().addClass("sel");
					$("#lf" + ord).text("Пойду!");
					$("#mk_" + ord).parent().addClass("sel");
					$("#lf_" + ord).text("Пойду!");
					var html = "<div id=\"chosen" + ord + "\" class=\"chitem group\"><div class=\"igonum\">" + igos + "</div><div class=\"igodesc\" id=\"igod" + ord + "\">" + theme + "</div><!--<div class=\"igodel\" onClick=\"igo(" + group + ", " + ord + ", '', false);\"><span>X</span> Удалить</div>--></div>";
					$(".chosen").append(html);
					$("#mclasses" + group).val(theme);
					$("#mcl" + group).val(ord);
					// для прокрутки вниз при выборе первого мастер-класса
					if (igos == 1)
						$('html,body').animate({scrollTop: $(".prog[data-group=" + (3 - group) + "]").offset().top - 200}, 500);
					
				}
				else {
					igos--;
					var arr_s = similar.split(",");
					$(".prog[data-group=" + group + "]").removeClass("faded").find(".igo").fadeIn(200);
					$(".igoset[data-group=" + group + "]").not(".cancelled").removeClass("faded").find("input").removeAttr("disabled");
					$.each(arr_s, function(key, val) {
						$(".prog[data-mk=" + val + "]").removeClass("faded").find(".igo").fadeIn(200);
						$(".igoset[data-mk=" + val + "]").not(".cancelled").removeClass("faded").find("input").removeAttr("disabled");
					})
					$(".igoset[data-mk=" + ord + "]").find("input").removeAttr("checked");
					$("#mk" + ord).parent().removeClass("sel");
					$("#lf" + ord).text("Хочу пойти");
					$("#mk_" + ord).parent().removeClass("sel");
					$("#lf_" + ord).text("Хочу пойти");
					$("#chosen" + ord).remove();
					$('.chitem').each(function(index) {
						$(this).find(".igonum").text(index + 1);
					});
					$("#mclasses" + group).val("");
					$("#mcl" + group).val("");
				}
			}
			function openmob(n) {
				var wdth = $(window).width();
				if (wdth < 960) {
					$("#event" + n).slideToggle(300);
					$("#prog" + n).toggleClass("open");
				}
			}
			function isEmail(email) {
				var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				return regex.test(email);
			}
			function chkigos() {
				if (igos < totigos) {						
					$('.prog1').fadeOut(200);
					$('.progtab.sel').removeClass('sel');
					$('.progtab2').addClass('sel');
					$('div[data-date=8]').fadeOut(300);
					$('h2[data-date=8]').fadeOut(300);
					$('.progspace').fadeIn(300);
					$('.mktime').css('display', 'table');
					$('div[data-date=9]').fadeIn(300);
					$('h2[data-date=9]').fadeIn(300);
					$('.prog2').fadeIn(200).promise().done(function() {
						$('html,body').animate({scrollTop: $(".prog2").offset().top - 100}, 500);
					});
					return false;
				}
				else
					return true;
			}
			function chkform(chkfile) {
				var foo = 0;
				if (chkfile == 1)
					$("#register").attr("disabled", "disabled");
				if (chkfile != 0 && filedone == 0 && $('.upl:checked').length > 0)
					{
					if (foo == 0) {
						$('html,body').animate({scrollTop: $('.upl').offset().top - 100}, 500);
					}
					$('#upload').addClass('redfld');
					foo = 1;
				}
				$('.ness:visible').each(function() {
					if ($(this).val() == '') {
						if (foo == 0) {
							$('html,body').animate({scrollTop: $(this).offset().top - 100}, 500);
						}
						$(this).addClass('redfld');
						foo = 1;
					}
					else {
						$(this).removeClass('redfld');
					}
				});
				$('#email').each(function() {
					if (!isEmail($(this).val())) {
						if (foo == 0) {
							// alert($(this).offset().top);
							$('html,body').animate({scrollTop: $(this).offset().top - 100}, 500);
						}
						$(this).addClass('redfld');
						foo = 1;
					}
					else {
						$(this).removeClass('redfld');
					}
				});
				if (foo == 0) {
					if (userid != "" && chkfile == 1) {
						countdown();
						writedata();
					}
					return true;
				}
				else {
					$("#register").removeAttr("disabled");
					return false;
				}
			}
			$(window).resize(function() {
				var wdth = $(window).width();
				if (wdth < 960) {
					gmargin = wdth - 20;
					$(".gal_slide").css("width", gmargin);
					var ghght = gmargin/1250*669;
					$(".gal_slide").css("height", ghght);
					$(".gallery").css("height", ghght);
					$(".gal_navs").css("top", ghght - 80);
				}
				/*
				var gwdth = $(window).width();
				if (gwdth > 1250)
					gwdth = 1250;
				gmargin = gwdth;
				var ghght = gwdth/1250*669;
				if (gwdth < 480)
					$(".prog").css("width", gwdth - 40);
				$(".gal_slide").css("width", gwdth);
				$(".gal_slide").css("height", ghght);
				$(".gallery").css("height", ghght);
				$(".gal_navs").css("top", ghght - 80);
				*/
			})
			// $(window).load(function() {
				// $("#left").css("margin-left", "-1046px");
				// $("#right").css("margin-left", "250px");
			// });
			var spmargin = 0;
			var swidth = 320;
			var curspeaker = 1;
 			function speaker_left() {
				if (spmargin < 0) {
					spmargin = spmargin + swidth;
					$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 500);
					curspeaker--;
					$(".spbullets").find(".active").removeClass("active");
					$("#spbul_" + curspeaker).addClass("active");
				}
				else {
					$(".speakers").animate({margin: '0 0 0 20'}, 200, function() {
						$(".speakers").animate({margin: '0 0 0 0'}, 200);
					});
				}
			}
 			function speaker_right() {
				var slide_count = $(".speakers").find(".speaker:visible").length;
				var limit = (slide_count - 1) * swidth;
				if (spmargin > -limit) {
					spmargin = spmargin - swidth;
					$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 500);
					$(".spbullets").find(".active").removeClass("active");
					curspeaker++;
					$(".spbullets").find(".active").removeClass("active");
					$("#spbul_" + curspeaker).addClass("active");
				}
				else {
					$(".speakers").animate({margin: '0 0 0 ' + (spmargin - 20)}, 200, function() {
						$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 200);
					});
				}
			}
			function speaker_go(sp) {
				spmargin = - swidth * (sp - 1);
				$(".speakers").animate({margin: '0 0 0 ' + spmargin}, 500);
				$(".spbullets").find(".active").removeClass("active");
				$("#spbul_" + sp).addClass("active");
				curspeaker = sp;
			}
			$(document).ready(function() {
				var ev = $("#events").val();
				$.ajax({
					url:"https://orthodontia.ru/udata/emarket/show_promokod/" + ev + "/.json",
					xhrFields: {
					  withCredentials: true
					},
					dataType: "json",
					success:function(data) {
						if (data.result == 1) {
							// есть промокоды, показываем поле
							$("#promo_field").show();
						}
					}
				});
			
				$("body").bind("click", function(e) {
					if ($(e.target).closest(".igosel").length > 0 || $(e.target).closest(".prodsel").length > 0 || $(e.target).closest(".igoset").length > 0 || $(e.target).closest(".prodset").length > 0) {
						return;
					}
					$(".igodd").fadeOut(500);
					$(".igosel").removeClass('sel');
					$(".proddd").fadeOut(500);
					$(".prodsel").removeClass('sel');
				});
				var wdth = $(window).width();
				if (wdth < 960) {
					gmargin = wdth - 20;
					$(".gal_slide").css("width", gmargin);
					var ghght = gmargin/1250*669;
					$(".gal_slide").css("height", ghght);
					$(".gallery").css("height", ghght);
					$(".gal_navs").css("top", ghght - 80);
					$(".speakers").swipe( {
						swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
							speaker_right();
						},
						swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
							speaker_left();
						},
					   threshold: 75
					});
					$(".gallery").swipe( {
						swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
							gallery_right();
						},
						swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
							gallery_left();
						},
					   threshold: 75
					});
					swidth = wdth;
					$(".speaker").css("width", wdth);
				}
				/*
				maxx = $(".gal_slide").length;
				var gwdth = $(window).width();
				if (gwdth > 1250)
					gwdth = 1250;
				gmargin = gwdth;
				var ghght = gwdth/1250*669;
				if (gwdth < 480)
					$(".prog").css("width", gwdth - 40);
				$(".gal_slide").css("width", gwdth);
				$(".gal_slide").css("height", ghght);
				$(".gallery").css("height", ghght);
				$(".gal_navs").css("top", ghght - 80);
				*/
				hghts = new Array();
				for (var key in lnks) {
					if ($("[name=" + lnks[key] +"]").length) {
						hghts[key] = $("[name=" + lnks[key] +"]").offset().top;
					}
				}
				var i = document.location.hash.replace("#", "");
				$("a[href*='#']:not([href='#'])").click(function() {
					if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
						|| location.hostname == this.hostname) {
						var target = $(this.hash);
						target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
						   if (target.length) {
							var i = target.attr("name");
							scrolling = 1;
							$("#mobmenu").slideUp(300);
							$('html,body').animate({
								 scrollTop: target.offset().top - 60
							}, 1000, function() {
								scrolling = 0;
								// alert(target.offset().top);
							});
							$(".ch").removeClass("ch");
							$(this).parent().addClass("ch");
							// history.pushState(null, i, i);
							return false;
						}
					}
				});
			});
			function gallery_go(n) {
				curgmargin = -gmargin * (n - 1);
				$(".gal_slider").animate({marginLeft: curgmargin});
				curgal = n;
			}
			function gallery_left() {
				if (curgal > 1) {
					gallery_go(curgal - 1);
				}
				else {
					gallery_go(maxx);
				}
			}
			function gallery_right() {
				if (curgal < maxx) {
					gallery_go(curgal + 1);
				}
				else {
					gallery_go(1);
				}
			}
			function gallery_go1(n) {
				curgmargin1 = -gmargin1 * (n - 1);
				$(".gal_slider1").animate({marginLeft: curgmargin1});
				curgal1 = n;
			}
			function gallery_left1() {
				if (curgal1 > 1) {
					gallery_go1(curgal1 - 1);
				}
				else {
					gallery_go1(maxx1);
				}
			}
			function gallery_right1() {
				if (curgal1 < maxx1) {
					gallery_go1(curgal1 + 1);
				}
				else {
					gallery_go1(1);
				}
			}
			var scrolling = 0;
			/*
			window.onscroll = function() {
				var hght = $('body').height();
				// alert(hght);
				var scrolled = window.pageYOffset || document.documentElement.scrollTop;
				if (scrolling == 0)
					{
					for (var key in hghts)
						{
						if (scrolled >= hghts[key] - 300) {
							$(".ch").removeClass("ch");
							$("#mi" + lnks[key]).addClass("ch");
							$("#mi" + lnks[key] + "1").addClass("ch");
							break;
						}
					}
				}
				var wdth = $(window).width();
				if (scrolled < 320 && wdth > 960) {
					var leftmarg = scrolled - 1146;
					var rightmarg = 450 - scrolled;
					$("#left").css("margin-left", leftmarg);
					$("#right").css("margin-left", rightmarg);
				}
			}
			*/
			function reload() {
				var src = document.captcha.src;
				document.captcha.src = '/images/loading.gif';
				document.captcha.src = src + '?rand='+Math.random();
			}
			var x1 = 55.787932;
			var y1 = 37.680472;
			var x2 = 25.217603;
			var y2 = 55.2828107;
			var myLatlng1;
			var myLatlng2;
			var map;
			var marker1;
			var marker2;
			function initialize_map() {
				myLatlng1 = new google.maps.LatLng(x1, y1);
				myLatlng2 = new google.maps.LatLng(x2, y2);
				var mapOptions = {
					center: myLatlng1,
					mapTypeControl:!1,
					streetViewControl:!1,
					scrollwheel:!1,
					panControl:!1,
					draggable:!1,
					zoomControlOptions:{position:google.maps.ControlPosition.LEFT_TOP},
					zoom: 17,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				map = new google.maps.Map(document.getElementById("map"),
					mapOptions);
				marker1 = new google.maps.Marker({
					position: myLatlng1,
					map: map,
					title: "г. Москва, улица Тверская, 26/1, отель \"Holiday Inn Moscow Sokolniki\""
				});
				var infowindow1 = new google.maps.InfoWindow({
					content: "<div class=\"onmap\"><h3>Holiday Inn Moscow Sokolniki, Москва</h3>, ул. Русаковская, дом 24</div>"
				});
				marker1.addListener('click', function() {
					infowindow1.open(map, marker1);
				});
				/*
				marker2 = new google.maps.Marker({
					position: myLatlng2,
					map: map,
					title: "Санкт-Петербург"
				});
				*/
			}
			function map1() {
				map.setCenter(myLatlng1);
			}
			function map2() {
				map.setCenter(myLatlng2);
			}
			// загрузка изображения
			$(function(){
				var btnUploads = $("#upload");
				$(btnUploads).each(function() {
					new AjaxUpload(this, {
						action: "xp_upload.php",
						name: "uploadfile",
						onComplete: function(file, response){
							if (response.replace("Error", "") == response) {
								$("#upload").addClass("done").text("Файл загружен");
								if (response.replace(".pdf", "") == response && response.replace(".PDF", "") == response) {
									$("#uploaded").html("<img width=\"100%\" src=\"uploads/" + response + "\" alt=\"\" />");
								}
								// $("#file").val('<a href="<?=$_SERVER["SERVER_NAME"];?><?=$_SERVER["REQUEST_URI"];?>uploads/' + response + '">скачать</a>');
								$("#file").val('<?=$_SERVER["REQUEST_URI"];?>uploads/' + response);
								filedone = 1;
							}
							else {
								alert("Файл " + file + " не загружен! Ошибка: " + response.replace("Error", ""));
							}
						}
					});
				})
			});
		</script>
	</head>
	<body>
		<div class="container">
			<a name="about"></a>
			<div class="section top">
				<div class="section header">
					<img src="images/logo.png" />
					<div class="menu">
						<ul>
							<button type="button" onClick="fixbody(); $('.mask').fadeIn(300); $('.openform').fadeIn(300);">Регистрация</button>
							<li id="miprogramm"><a href="#programm">Программа</a></li>
							<li id="mispeakers"><a href="#speakers">Спикер</a></li>
							<li id="miprice"><a href="#price">Стоимость</a></li>
							<li id="miplace"><a href="#place">Место проведения</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="section baseinfo">
				<div class="seminar">
					<h1 class="h1">Семинар С.А. Попова<br /><span>«Новые возможности в ортодонтии с абсолютной опорой Vector TAS»</span></h1>
					<h2 class="h2"><span>3 - 4</span> февраля 2018 г.</h2>
					<h3 class="h3">Москва, отель «Holiday Inn Moscow Sokolniki»</h3>
				</div>
				<div class="main">
					<!-- Программа -->
					<a name="programm"></a>
					<h1>Программа</h1>
					<br />
					<div class="progtabbs group">
						<div class="progline"></div>
						<div class="progtabs group">
							<div class="progtab progtab1 sel" onClick="$('.progtab.sel').removeClass('sel'); $(this).addClass('sel'); $('div[data-date=4]').fadeOut(300); $('h2[data-date=4]').fadeOut(300); $('.progspace').fadeOut(300); $('div[data-date=3]').fadeIn(300); $('h2[data-date=3]').fadeIn(300); $('.mktime').css('display', 'none');">3 февраля</div>
							<div class="progtab progtab2" onClick="$('.progtab.sel').removeClass('sel'); $(this).addClass('sel'); $('div[data-date=3]').fadeOut(300); $('h2[data-date=3]').fadeOut(300); $('.progspace').fadeIn(300); $('div[data-date=4]').fadeIn(300); $('h2[data-date=4]').fadeIn(300); $('.mktime').css('display', 'table');">4 февраля</div>
						</div>
						<div class="progline"></div>
					</div>
					<div class="prog group" data-date="3">
						<div class="event1">
							<div class="psimg"><img align="left" src="images/popov.jpg" /></div>
							<div class="eshort">Лекция &quot;Новые возможности в ортодонтии с абсолютной опорой Vector TAS&quot;</div>
						</div>
						<div class="pspeaker group">
							<div class="psimg"><img align="left" src="images/popov.jpg" /></div>
							<div class="psd"><h4>С.А. Попов</h4>
							</div>
						</div>
						<div class="event">
							<div class="eshort">Лекция &quot;Новые возможности в ортодонтии с абсолютной опорой Vector TAS&quot;</div>
						</div>
						<div class="dt">
							<span>3</span><br />февраля
						</div>
					</div>
					<div class="prog group" data-date="4" style="display: none;">
						<div class="event1">
							<div class="psimg"><img align="left" src="images/popov.jpg" /></div>
							<div class="eshort">Лекция &quot;Новые возможности в ортодонтии с абсолютной опорой Vector TAS&quot;</div>
						</div>
						<div class="pspeaker group">
							<div class="psimg"><img align="left" src="images/popov.jpg" /></div>
							<div class="psd"><h4>С.А. Попов</h4>
							</div>
						</div>
						<div class="event">
							<div class="eshort">Лекция &quot;Новые возможности в ортодонтии с абсолютной опорой Vector TAS&quot;</div>
						</div>
						<div class="dt">
							<span>4</span><br />февраля
						</div>
					</div>
					<div class="progspace"></div>
					<div class="prog1">Заявка по учебному мероприятию представлена в Комиссию по оценке учебных мероприятий и материалов для НМО на соответствие установленным требованиям</div>
					<!--<div class="prog1">Осталось мест на лекцию <?=$mkcounts[0];?></div>-->
					<div class="progs_cont">
						<div class="progs">
							<!-- Событие 1 -->
							<div class="prog group" data-date="3" id="prog1" onClick="openmob(1);">
								<div class="event1">
									<time>08:00 – 08:45</time>
									<div class="eshort">Регистрация</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/opening.png" /></div>
									<div class="psd"><h4>08:00 – 08:45</h4></div>
								</div>
								<div class="event">
									<div class="eshort">Регистрация</div>
								</div>
								<div class="event2" id="event1">
									<div class="psimg"><img align="left" src="images/opening.png" /></div>
									<div class="psd"><span>3</span> февраля<br /></div>
								</div>
								<div class="dt">
									<span>3</span><br />февраля
								</div>
							</div>
							<!-- Событие 3 -->
							<div class="prog group" data-date="3" id="prog3" onClick="openmob(3);">
								<div class="event1">
									<time>09:00 - 11:00</time>
									<div class="eshort">Лекция «Слагаемые успешности ортодонтической практики»</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/master.jpg" /></div>
									<div class="psd"><h4><time>09:00 - 11:00</time></h4></div>
								</div>
								<div class="event">
									<button type="button">Лекция</button>
									<div class="eshort">Лекция «Слагаемые успешности ортодонтической практики»</div>
								</div>
								<div class="event2" id="event3">
									<div class="psimg"><img align="left" src="images/master.jpg" /></div>
									<div class="psd"><span>3</span> февраля<br /><button type="button">Лекция</button></div>
								</div>
								<div class="dt">
									<span>3</span><br />февраля
								</div>
							</div>
							<!-- Событие 4 -->
							<div class="prog group" data-date="3" id="prog4" onClick="openmob(4);">
								<div class="event1">
									<time>11:00 – 11:30</time>
									<div class="eshort">Кофе-брейк</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									<div class="psd"><h4><time>11:00 – 11:30</time></h4></div>
								</div>
								<div class="event">
									<div class="eshort">Кофе-брейк</div>
								</div>
								<div class="event2" id="event4">
									<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									<div class="psd"><span>3</span> февраля<br /></div>
								</div>
								<div class="dt">
									<span>3</span><br />февраля
								</div>
							</div>
							<!-- Событие 5 -->
							<div class="prog group" data-date="3" id="prog5" onClick="openmob(5);">
								<div class="event1">
									<time>11:30 - 13:00</time>
									<div class="eshort">Лекция «Конструктивные особенности и преимущества минивинтов <em>Vector TAS</em>»</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/master.jpg" /></div>
									<div class="psd"><h4><time>11:30 - 13:00</time></h4></div>
								</div>
								<div class="event">
									<button type="button">Лекция</button>
									<div class="eshort">Лекция «Конструктивные особенности и преимущества минивинтов <em>Vector TAS</em>»</div>
								</div>
								<div class="event2" id="event5">
									<div class="psimg"><img align="left" src="images/master.jpg" /></div>
									<div class="psd"><span>3</span> февраля<br /><button type="button">Лекция</button></div>
								</div>
								<div class="dt">
									<span>3</span><br />февраля
								</div>
							</div>
							<!-- Событие 6 -->
							<div class="prog group" data-date="3" id="prog6" onClick="openmob(6);">
								<div class="event1">
									<time>13:00 - 14:00</time>
									<div class="eshort">Обед</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/lunch.png" /></div>
									<div class="psd"><h4><time>13:00 - 14:00</time></h4></div>
								</div>
								<div class="event">
									<div class="eshort">Обед</div>
								</div>
								<div class="event2" id="event6">
									<div class="psimg"><img align="left" src="images/lunch.png" /></div>
									<div class="psd"><span>3</span> февраля<br /></div>
								</div>
								<div class="dt">
									<span>3</span><br />февраля
								</div>
							</div>
							<!-- Событие 7 -->
							<div class="prog group" data-date="3" id="prog7" onClick="openmob(7);">
								<div class="event1">
									<time>14:00 - 16:00</time>
									<div class="eshort">Лекция «Рентгенологическая диагностика»</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/master.jpg" /></div>
									<div class="psd"><h4><time>14:00 - 16:00</time></h4></div>
								</div>
								<div class="event">
									<button type="button">Лекция</button>
									<div class="eshort">Лекция «Рентгенологическая диагностика»</div>
								</div>
								<div class="event2" id="event7">
									<div class="psimg"><img align="left" src="images/master.jpg" /></div>
									<div class="psd"><span>3</span> февраля<br /><button type="button">Лекция</button></div>
								</div>
								<div class="dt">
									<span>3</span><br />февраля
								</div>
							</div>
							<!-- Событие 8 -->
							<div class="prog group" data-date="3" id="prog8" onClick="openmob(8);">
								<div class="event1">
									<time>16:00 – 16:30</time>
									<div class="eshort">Кофе-брейк</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									<div class="psd"><h4><time>16:00 – 16:30</time></h4></div>
								</div>
								<div class="event">
									<div class="eshort">Кофе-брейк</div>
								</div>
								<div class="event2" id="event8">
									<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									<div class="psd"><span>3</span> февраля<br /></div>
								</div>
								<div class="dt">
									<span>3</span><br />февраля
								</div>
							</div>
							<!-- Событие 9 -->
							<div class="prog group" data-date="3" id="prog9" onClick="openmob(9);">
								<div class="event1">
									<time>16:30 - 17:30</time>
									<div class="eshort">Лекция «Пошаговый клинический разбор постановки минивинта»</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/master.jpg" /></div>
									<div class="psd"><h4><time>16:30 - 17:30</time></h4></div>
								</div>
								<div class="event">
									<button type="button">Лекция</button>
									<div class="eshort">Лекция «Пошаговый клинический разбор постановки минивинта»</div>
								</div>
								<div class="event2" id="event9">
									<div class="psimg"><img align="left" src="images/master.jpg" /></div>
									<div class="psd"><span>3</span> февраля<br /><button type="button">Лекция</button></div>
								</div>
								<div class="dt">
									<span>3</span><br />февраля
								</div>
							</div>
							<!-- Событие 10 -->
							<div class="prog group" data-date="3" id="prog10" onClick="openmob(10);">
								<div class="event1">
									<time>17:30 - 18:00</time>
									<div class="eshort">Дискуссия, сессия вопросов и ответов</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/master.jpg" /></div>
									<div class="psd"><h4><time>17:30 - 18:00</time></h4></div>
								</div>
								<div class="event">
									<div class="eshort">Дискуссия, сессия вопросов и ответов</div>
								</div>
								<div class="event2" id="event10">
									<div class="psimg"><img align="left" src="images/master.jpg" /></div>
									<div class="psd"><span>3</span> февраля<br /></div>
								</div>
								<div class="dt">
									<span>3</span><br />февраля
								</div>
							</div>
							<!-- Событие 11 -->
							<div class="prog group" data-date="4" style="display: none;" id="prog11" onClick="openmob(11);">
								<div class="event1">
									<time>08:30 – 09:00</time>
									<div class="eshort">Кофе-брейк</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									<div class="psd"><h4><time>08:30 – 09:00</time></h4></div>
								</div>
								<div class="event">
									<div class="eshort">Кофе-брейк</div>
								</div>
								<div class="event2" id="event11">
									<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									<div class="psd"><span>4</span> февраля<br /></div>
								</div>
								<div class="dt">
									<span>4</span><br />февраля
								</div>
							</div>
							<!-- Событие 13 -->
							<div class="prog group" data-date="4" style="display: none;" id="prog13" onClick="openmob(13);">
								<div class="event1">
									<time>09:00 – 11:00</time>
									<div class="eshort">Лекция «Демонстрация клинических случаев с детальным разбором принципов биомеханики»</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/master.jpg" /></div>
									<div class="psd"><h4><time>09:00 – 11:00</time></h4></div>
								</div>
								<div class="event">
									<button type="button">Лекция</button>
									<div class="eshort">Лекция «Демонстрация клинических случаев с детальным разбором принципов биомеханики»</div>
								</div>
								<div class="event2" id="event13">
									<div class="psimg"><img align="left" src="images/master.jpg" /></div>
									<div class="psd"><span>4</span> февраля<br /></div>
								</div>
								<div class="dt">
									<span>4</span><br />февраля
								</div>
							</div>
							<!-- Событие 14 -->
							<div class="prog group" data-date="4" style="display: none;" id="prog14" onClick="openmob(14);">
								<div class="event1">
									<time>11:00 - 11:30</time>
									<div class="eshort">Кофе-брейк</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									<div class="psd"><h4><time>11:00 - 11:30</time></h4></div>
								</div>
								<div class="event">
									<div class="eshort">Кофе-брейк</div>
								</div>
								<div class="event2" id="event14">
									<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									<div class="psd"><span>4</span> февраля<br /></div>
								</div>
								<div class="dt">
									<span>4</span><br />февраля
								</div>
							</div>
							<!-- Событие 15 -->
							<div class="prog group" data-date="4" style="display: none;" id="prog15" onClick="openmob(15);">
								<div class="event1">
									<time>11:30 – 13:00</time>
									<div class="eshort">Лекция «Демонстрация клинических случаев с детальным разбором принципов биомеханики»</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/master.jpg" /></div>
									<div class="psd"><h4><time>11:30 – 13:00</time></h4></div>
								</div>
								<div class="event">
									<button type="button">Лекция</button>
									<div class="eshort">Лекция «Демонстрация клинических случаев с детальным разбором принципов биомеханики»</div>
								</div>
								<div class="event2" id="event15">
									<div class="psimg"><img align="left" src="images/master.jpg" /></div>
									<div class="psd"><span>4</span> февраля<br /></div>
								</div>
								<div class="dt">
									<span>4</span><br />февраля
								</div>
							</div>
							<!-- Событие 16 -->
							<div class="prog group" data-date="4" style="display: none;" id="prog16" onClick="openmob(16);">
								<div class="event1">
									<time>13:00 - 14:00</time>
									<div class="eshort">Обед</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/lunch.png" /></div>
									<div class="psd"><h4><time>13:00 - 14:00</time></h4></div>
								</div>
								<div class="event">
									<div class="eshort">Обед</div>
								</div>
								<div class="event2" id="event16">
									<div class="psimg"><img align="left" src="images/lunch.png" /></div>
									<div class="psd"><span>4</span> февраля<br /></div>
								</div>
								<div class="dt">
									<span>4</span><br />февраля
								</div>
							</div>
							<!-- Событие 17 -->
							<div class="prog group" data-date="4" style="display: none;" id="prog17" onClick="openmob(17);">
								<div class="event1">
									<time>14:00 – 15:30</time>
									<div class="eshort">Лекция «Анатомо-топографические особенности при постановке минивинтов Vector TAS на нижней челюсти»</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/master.jpg" /></div>
									<div class="psd"><h4><time>14:00 – 15:30</time></h4></div>
								</div>
								<div class="event">
									<button type="button">Лекция</button>
									<div class="eshort">Лекция «Анатомо-топографические особенности при постановке минивинтов Vector TAS на нижней челюсти»</div>
								</div>
								<div class="event2" id="event17">
									<div class="psimg"><img align="left" src="images/master.jpg" /></div>
									<div class="psd"><span>4</span> февраля<br /></div>
								</div>
								<div class="dt">
									<span>4</span><br />февраля
								</div>
							</div>
							<!-- Событие 18 -->
							<div class="prog group" data-date="4" style="display: none;" id="prog18" onClick="openmob(18);">
								<div class="event1">
									<time>15:30 - 16:00</time>
									<div class="eshort">Кофе-брейк</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									<div class="psd"><h4><time>15:30 - 16:00</time></h4></div>
								</div>
								<div class="event">
									<div class="eshort">Кофе-брейк</div>
								</div>
								<div class="event2" id="event18">
									<div class="psimg"><img align="left" src="images/coffee.png" /></div>
									<div class="psd"><span>4</span> февраля<br /></div>
								</div>
								<div class="dt">
									<span>4</span><br />февраля
								</div>
							</div>
							<!-- Событие 17 -->
							<div class="prog group" data-date="4" style="display: none;" id="prog19" onClick="openmob(19);">
								<div class="event1">
									<time>16:00 – 17:30</time>
									<div class="eshort">Лекция «Риски и осложнения»</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/master.jpg" /></div>
									<div class="psd"><h4><time>16:00 – 17:30</time></h4></div>
								</div>
								<div class="event">
									<button type="button">Лекция</button>
									<div class="eshort">Лекция «Риски и осложнения»</div>
								</div>
								<div class="event2" id="event19">
									<div class="psimg"><img align="left" src="images/master.jpg" /></div>
									<div class="psd"><span>4</span> февраля<br /></div>
								</div>
								<div class="dt">
									<span>4</span><br />февраля
								</div>
							</div>
							<!-- Событие 20 -->
							<div class="prog group" data-date="4" style="display: none;" id="prog20" onClick="openmob(20);">
								<div class="event1">
									<time>17:30 - 18:00</time>
									<div class="eshort">Дискуссия</div>
								</div>
								<div class="pspeaker group">
									<div class="psimg"><img align="left" src="images/closing.png" /></div>
									<div class="psd"><h4><time>17:30 - 18:00</time></h4></div>
								</div>
								<div class="event">
									<div class="eshort">Дискуссия</div>
								</div>
								<div class="event2" id="event20">
									<div class="psimg"><img align="left" src="images/closing.png" /></div>
									<div class="psd"><span>4</span> февраля<br /></div>
								</div>
								<div class="dt">
									<span>4</span><br />февраля
								</div>
							</div>
						</div>
						<!--<div class="prog1">Осталось мест на лекцию <?=$mkcounts[0];?></div>-->
					</div>
					<div><button type="button" class="blue" onClick="fixbody(); $('.mask').fadeIn(300); $('.moreprog').fadeIn(300);">Смотреть полную программу</button></div>
					<div class="moreprog">
						<div class="formbox_close" onClick="unfixbody(); $('.mask').fadeOut(300); $('.moreprog').fadeOut(300);"><span class="shape1"></span><span class="shape2"></span></div>

						<?=nl2br("<h2>Новые возможности в ортодонтии с абсолютной опорой Vector TAS.</h2>
<h3>Первый день</h3>

1.Слагаемые успешности ортодонтической практики.

2. Новые концепции в ортодонтии и возможности их реализаций на практике.

3. Опыт прошлых ошибок при сопротивлении 3 закону Ньютона.

4. Моя объективность в обсуждении абсолютного опоры (более 9 лет использования минивинтов, все клинические случаи имеют более 2 лет в ретенционном периоде).

5. Конструктивные особенности и преимущества минивинтов Vector TAS.

6. Возможные топографические ориентиры для постановки различных минивинтов Vector TAS.

7. Способы расположения минивинтов в зависимости от толщины мягких тканей альвеолярной части и высоты прикрепления подвижной и неподвижной десны.

8. Клинические противопоказания для установки минивинта.

9. Рентгенологическая диагностика.

10. Антисептическая обработка операционной зоны и способы анестезии.

11. Пошаговый клинический разбор постановки минивинта, набор инструментов.

12. Сила приложенной нагрузки в зависимости от топографии и ортодонтической биомеханики.

13. Условия достижения стабильности минивинта под нагрузкой.

14. Конструктивные характеристики отверток, используемых для установки минивинтов Vector TAS.

15. После установки минивинта:

- контроль оптимального приложения векторных сил; - вероятность раздражения окружающих мягких тканей; - дополнительные элементы для придания оптимального вектора сил; - выбор эластических модулей и объем их активации.

16. Возможность реактивации при потере стационарной опоры или миграции минивинта.

17. Преимущества применения минивинтов в ортодонтической практике.

18. Научные исследования топографии плотности костной ткани.

19. Анатомо-топографические особенности при постановке минивинтов Vector TAS на верхней челюсти.

20. Область ретромолярного пространства на верхней челюсти ( демонстрация более 15 клинических случаев с детальным разбором принципов биомеханики ): - для мезио-дистального отклонения боковых зубов на этапе нивелирования; - для стабилизации положения боковой группы зубов верхней челюсти при ретракции передней группы зубов; - для односторонней дистализации боковой группы зубов верхней челюсти, стабилизации их при ретракции передней группы; - для двусторонней дистализации боковых групп зубов верхней челюсти, стабилизации их при ретракции передней группы с трансверзальной коррекцией.

21. Вопросы. Дискуссия.

<h3>Второй день</h3>

1. Область межкорневого пространства между 1 и 2 молярами верхней челюсти вестибулярно и небно ( демонстрация более 8 клинических случаев с детальным разбором принципов биомеханики ): - для интрузии моляров, стабилизации их положения; - для интрузии боковых сегментов в\ч, стабилизации их положения; - для стабилизации опоры первых моляров при ретракции передней группы зубов; - для интрузии боковых зубов с небным отклонением.

2. Область межкорневого пространства между 1 моляром и 2 премоляром верхней челюсти вестибулярно и небно ( демонстрация клинических случаев с детальным разбором принципов биомеханики ): - для интрузии боковых сегментов в\ч, стабилизации их положения; - для усиления экструзионного эффекта в боковых сегментах нижней челюсти; - для мезиализации 2 моляров в/ч; - для ретракции переднего сегмента верхней челюсти с интрузионным эффектом.

3. Область межкорневого пространства между клыком и 1 премоляром верхней челюсти вестибулярно ( демонстрация клинических случаев с детальным разбором принципов биомеханики ): - для мезиального перемещения боковых зубов.

4. Область межкорневого пространства между резцами верхней челюсти вестибулярно ( демонстрация клинических случаев с детальным разбором принципов биомеханики ): - для интрузии и контроля торка передних зубов на этапе ретракции переднего сегмента.

5. Анатомо-топографические особенности при постановке минивинтов Vector TAS на нижней челюсти.

6. Область ретромолярного пространства на верхней челюсти ( демонстрация более 15 клинических случаев с детальным разбором принципов биомеханики ): - для мезио-дистального отклонения боковых зубов на этапе нивелирования; - для стабилизации положения боковой группы зубов верхней челюсти при ретракции передней группы зубов; - для односторонней дистализации боковой группы зубов верхней челюсти, стабилизации их при ретракции передней группы.

7. Область межкорневого пространства между 1 моляром и 2 премоляром нижней челюсти вестибулярно ( демонстрация клинических случаев с детальным разбором принципов биомеханики ): - для предотвращения экструзии боковых зубов на этапе нивелирования; - для стабилизации опорной функции моляров при парафункции языка.

8. Область межкорневого пространства между 1премоляром и 2 премоляром нижней челюсти вестибулярно ( демонстрация клинических случаев с детальным разбором принципов биомеханики ): - мезиальное перемещение боковых зубов.

9. Вершина альвеолярного гребня нижней челюсти в области отсутствующих зубов ( демонстрация клинических случаев с детальным разбором принципов биомеханики ): - мезио-дистальное отклонение боковых зубов на этапе нивелирования; - использование в качестве опоры для временного протезирования.

10. Риски и осложнения: - осложнения возникающие в ходе установки минивинта; - осложнения возникающие при приложении негативного вектора силы.

11. Вопросы. Дискуссия.");?>
					</div>
					<a name="new"></a>
					<br />
					<div><button type="button" onClick="fixbody(); $('.mask').fadeIn(300); $('.openform').fadeIn(300);">Регистрация</button></div>
					<br />
					<!-- Спикеры -->
					<a name="speakers"></a>
					<h1>Спикер</h1>
					<hr class="mobile" />
					<div class="speakersdiv">
						<div class="speakers group">
							<?
							$scnt = 0;
							foreach ($speakers as $speaker)
								{
								$scnt++;
								$arr_name = explode(" ", $speaker["name"]);
								?>
								<div class="speaker" onClick="$('.mask').fadeIn(300); $('#spdesc<?=$scnt;?>').fadeIn(300);">
									<img src="images/<?=$speaker["image"];?>" />
									<div class="speakerdata">
										<h4><?=$arr_name[0];?><br /><?=$arr_name[1];?> <?=$arr_name[2];?></h4>
									</div>
								</div>
								<span class="link" onClick="$('.mask').fadeIn(300); $('#spdesc<?=$scnt;?>').fadeIn(300);">Подробнее о спикере</span>
								<div class="speaker_desc" id="spdesc<?=$scnt;?>">
									<div class="close" onClick="$('.mask').fadeOut(300); $('.speaker_desc').fadeOut(300);"><img src="images/close.png" /></div>
									<h4><?=$speaker["name"];?></h4>
									<?=$speaker["about"];?>
								</div>
								<?
								if ($scnt % 3 == 0)
									{
									?><hr class="between" /><?
								}
							}
							?>
						</div>
						<div class="arrleft" onClick="speaker_left();"><img src="images/arr_left.png" /></div>
						<div class="arrright" onClick="speaker_right();"><img src="images/arr_right.png" /></div>
					</div>
					<!--<div class="spbullets group">
						<?
						$scnt = 0;
						foreach ($speakers as $speaker)
							{
							$scnt++;
							?>
							<div id="spbul_<?=$scnt;?>" class="spbullet<?if ($scnt == 1) {?> active<?}?>" onClick="speaker_go(<?=$scnt;?>);"></div>
							<?
						}
						?>
					</div>-->
					<!-- Стоимость -->
					<a name="price"></a>
					<h1>Стоимость участия <sup>*</sup></h1>
					<div class="txt">Стоимость участия может измениться в зависимости от даты оплаты. Просим вас не затягивать и позаботиться об оплате мероприятия заблаговременно</div>
					<h5><sup>*</sup> Ординаторам предоставляется скидка 40% при предъявлении подтверждающего документа</h5>
					<h5><sup>*</sup> Участникам Школы ортодонтии предоставляется скидка 50% при предъявлении подтверждающего документа</h5>
					<h5><sup>*</sup> Членам Профессионального общества ортодонтов предоставляется скидка 15%</h5>
					<br /><br />
					<div class="prices group<?if (date("Y-m-d") >= "2018-02-04") {?> opa<?}?>">
						<div class="pr_title">Стоимость участия</div>
						<div class="price">7000 р.</div>
						<div class="button"><button type="button"<?if (date("Y-m-d") < "2018-02-04") {?> onClick="fixbody(); $('.mask').fadeIn(300); $('.openform').fadeIn(300);"<?}?>>Регистрация</button></div>
					</div>
					<!--<div class="prices group<?if (date("Y-m-d") < "2017-11-15") {?> opa<?}?>">
						<div class="pr_title">При оплате после 15 ноября</div>
						<div class="price">13000 р.</div>
						<div class="button"><button type="button"<?if (date("Y-m-d") >= "2017-11-15") {?> onClick="$('#finalprice').text('13000 р.'); fixbody(); $('.mask').fadeIn(300); $('.openform').fadeIn(300);"<?}?>>Регистрация</button></div>
					</div>-->
					<br />
					<!-- Место -->
					<a name="place"></a>
					<h1>Место проведения</h1>
					<div class="txt">
						Holiday Inn Moscow Sokolniki<br />
						Москва, ул. Русаковская, дом 24
					</div>
					<div class="places group">
						<div class="placeline"></div>
						<div class="placetabs group">
							<div class="placetab sel" onClick="$('.placetab.sel').removeClass('sel'); $(this).addClass('sel'); $('.map').fadeOut(300); $('.gallery').fadeIn(300);">Галерея</div>
							<div class="placetab" onClick="$('.placetab.sel').removeClass('sel'); $(this).addClass('sel'); $('.gallery').fadeOut(300); $('.map').fadeIn(300); initialize_map();">На карте</div>
						</div>
						<div class="placeline"></div>
					</div>
					<div class="gallery">
						<div class="gal_slider group">
							<div class="gal_slide"><img src="images/building1.jpg" /></div>
							<div class="gal_slide"><img src="images/building2.jpg" /></div>
							<div class="gal_slide"><img src="images/building3.jpg" /></div>
							<div class="gal_slide"><img src="images/building4.jpg" /></div>
							<div class="gal_slide"><img src="images/building5.jpg" /></div>
						</div>
						<div class="gal_navs">
							<div class="gal_nav" onClick="gallery_left();"><img src="images/arr_left.png" /></div>
							<div class="gal_nav" onClick="gallery_right();"><img src="images/arr_right.png" /></div>
						</div>
					</div>
					<div class="map"><div id="map"></div></div>
					<a name="form"></a>
					<h1>Регистрация</h1>
					<div class="formmiddle">
						<div class="formtable group">
							<img src="images/face.jpg" />						
							<div class="formtxt">
								В случае появления вопросов или сложностей при регистрации, вы можете связаться с нами по телефону +7&nbsp;(495)&nbsp;664-75-55
							</div>
						</div>
						<button type="button" onClick="fixbody(); $('.mask').fadeIn(300); $('.openform').fadeIn(300);">Зарегистрироваться</button>
					</div>
					<div class="section footer">
						Ormco Corporation &copy; 2017<br />
						<a href="http://ormco.ru">ormco.ru</a>
					</div>
				</div>
			</div>
		</div>
		<div class="openform">
			<div class="openform_">
				<div class="newform formbox1">
					<div class="formbox_close" onClick="unfixbody(); $('.mask').fadeOut(300); $('.openform').fadeOut(300);"><span class="shape1"></span><span class="shape2"></span></div>
					<h2>Регистрация</h2>
					<h4>Введите Ваш email. Если он уже есть в нашей базе, регистрация пройдет быстрее</h4>
					<input id="f_email" name="f_email" value="<?=$_SESSION["f_email"];?>" />
					<button type="button" onClick="chk_f_email();">Далее</button>
					<div class="f_anim">
						<div class="at">@</div>
						<div class="arr arr1">←</div>
						<div class="dash arr2"></div>
						<div class="dash arr3"></div>
						<div class="dash arr4"></div>
						<div class="arr arr5">→</div>
						<div class="db">
							<div class="db1"></div>
							<div class="db2"></div>
							<div class="db3"></div>
							<div class="db4"></div>
							<div class="db5"></div>
							<div class="db6"></div>
							<div class="db7"></div>
							<div class="db8"></div>
							<div class="db9"></div>
						</div>
						<div class="f_desc">Проверяем email на наличие в базе данных</div>
					</div>
					<div class="exists">Человек с данным Email уже зарегистрировался на данное мероприятие</div>
				</div>
				<div class="newform formbox2">
					<div class="formbox_close" onClick="unfixbody(); $('.mask').fadeOut(300); $('.openform').fadeOut(300);"><span class="shape1"></span><span class="shape2"></span></div>
					<h2>Вход в систему</h2>
					<h4>Email найден! Пожалуйста, введите пароль от вашей учетной записи Ormco (сайты ormco.ru или orthodontia.ru), чтобы зарегистрироваться на семинар</h4>
					<div class="newform_field group">
						<div class="newform_ttl">Логин (Email):</div>
						<div class="newform_fld"><input id="e_email" name="e_email" value="<?=$_SESSION["f_email"];?>" /></div>
					</div>
					<div class="newform_field group">
						<div class="newform_ttl">Пароль:</div>
						<div class="newform_fld"><input type="password" id="e_pass" name="e_pass" value="" /><img onClick="chg_passstate();" src="images/eye.svg" /></div>
					</div>
					<div class="newform_link"><span onClick="remember_pass();">Вспомнить пароль</span></div>
					<div id="rp_ok" class="form_msg">Письмо с дальнейшими инструкциями по восстановлению пароля отправлены на указанный email</div>
					<div id="rp_fail" class="form_msg"></div>
					<div class="newform_field group">
						<div class="newform_ttl"></div>
						<div class="newform_fld"><button type="button" id="entry_but" onClick="chk_entry();">Войти</button></div>
					</div>
					<div class="exists">Человек с данным Email уже зарегистрировался на данное мероприятие</div>
					<div class="wrong">Пароль или логин указаны неверно</div>
				</div>
				<div class="newform formbox3">
					<div class="formbox_close" onClick="unfixbody(); $('.mask').fadeOut(300); $('.openform').fadeOut(300);"><span class="shape1"></span><span class="shape2"></span></div>
					<h2>Регистрация на мероприятие</h2>
					<div class="notfound red hidden">Пользователь не найден в базе данных. Вы можете зарегистрироваться на мероприятие, заполнив форму ниже:</div>
					<form action="https://orthodontia.ru/emarket/lporderSmart/" method="post" onSubmit="return chkform(1);" enctype="multipart/form-data">
					<input type="hidden" name="regularprice" id="regularprice" value="7000" />
					<input type="hidden" name="afterprice" id="promo_discount" value="7000" />
					<input type="hidden" name="mclasses1" id="mclasses1" value="" />
					<input type="hidden" name="mclasses2" id="mclasses2" value="" />
					<input type="hidden" name="mcl1" id="mcl1" value="" />
					<input type="hidden" name="mcl2" id="mcl2" value="" />
					<input type="hidden" name="p1" id="p1" value="" />
					<input type="hidden" name="p2" id="p2" value="" />
					<input type="hidden" name="file" id="file" value="" />
					<input type="hidden" name="events" id="events" value="1686" />
					<input type="hidden" name="promo_discount" id="promo_discount" value="" />
					<div class="partbuts">
						<!--<div class="partbut active partbut1" onClick="showpart(2);">Личные данные</div> → <div class="partbut partbut2" onClick="showpart(2);">Способ оплаты</div>-->
						<h4 class="partbut1">Личные данные</h4>
						<h4 class="partbut2 hidden">Способ оплаты</h4>
					</div>
					<hr class="hr">
					<div class="part1">
						<div class="newform_field group">
							<div class="newform_ttl">Email <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="email" id="email" /></div>
						</div>
						<div class="newform_field group phide">
							<div class="newform_ttl"></div>
							<div class="newform_fld small">На этот email будет отправлено подтверждение регистрации и ссылка для активации вашей учетной записи Ormco</div>
						</div>
						<div class="newform_field group phide">
							<div class="newform_ttl">Пароль <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="password" size="30" name="password" id="password" /></div>
						</div>
						<div class="newform_field group phide">
							<div class="newform_ttl">Пароль еще раз <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="password" size="30" name="password2" id="password2" /></div>
						</div>
						<br />
						<div class="newform_field group">
							<div class="newform_ttl">Фамилия <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="lname" id="lname" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Имя <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="fname" id="fname" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Отчество <sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="father_name" id="father_name" /></div>
						</div>
						<div class="newform_field group" id="speciality">
							<div class="newform_ttl big">Специальность для получения баллов по НМО<sup>*</sup></div>
							<div class="newform_fld"><select name="spec" id="spec" class="ness">
									<option value="">-= выберите Вашу специальность =-</option>
									<option value="Ортодонтия">Ортодонтия</option>
									<option value="Ортопедическая стоматология">Ортопедическая стоматология</option>
									<option value="Хирургическая стоматология">Хирургическая стоматология</option>
								</select>
							</div>
						</div>
						<div class="newform_field group" id="birthday">
							<div class="newform_ttl">Дата рождения<sup>*</sup></div>
							<div class="newform_fld"><input placeholder="" class="date1 ness" type="text" size="20" name="bd" id="bd" readonly /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Телефон <sup>*</sup></div>
							<div class="newform_fld"><input placeholder="+7 (987) 1234567" class="short ness" type="text" size="25" name="phone" id="phone" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Страна<sup>*</sup></div>
							<div class="newform_fld"><select name="country" id="country" class="ness" onChange="if (this.value == 'Россия') {$('.region_field').fadeIn(300);}else{$('.region_field').fadeOut(300);}">
									<option value="Россия">Россия</option>
									<option value="Азербайджан">Азербайджан</option>
									<option value="Армения">Армения</option>
									<option value="Белоруссия">Белоруссия</option>
									<option value="Грузия">Грузия</option>
									<option value="Украина">Украина</option>
									<option value="Другая">Другая</option>
								</select>
							</div>
						</div>
							<div class="newform_field group region_field">
								<div class="newform_ttl">Регион <sup>*</sup></div>
								<div class="newform_fld"><select name="region" id="region" class="ness">
									<option value="">-= выберите регион =-</option>
									<option value="9870">Москва и МО</option>
									<option value="9871">Санкт-Петербург и ЛО</option>
									<option value="9872">Северо-Западный</option>
									<option value="9873">Центральный</option>
									<option value="9874">Сибирский</option>
									<option value="9875">Приволжский</option>
									<option value="9876">Южный</option>
									<option value="9877">Северо-Кавказский</option>
									<option value="9878">Уральский</option>
									<option value="9879">Дальневосточный</option>
									<option value="16761">Не Россия</option>
								</select></div>
							</div>
						<div class="newform_field group">
							<div class="newform_ttl">Город<sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="city" id="city" /></div>
						</div>
						<div class="newform_field group" id="speciality">
							<div class="newform_ttl">Кто Вы? <sup>*</sup></div>
							<div class="newform_fld"><select name="who" id="who" class="ness">
									<option value="">Кто Вы?</option>
									<option value="Ортодонт">Ортодонт</option>
									<option value="Хирург">Хирург</option>
									<option value="Другая специализация">Другая специализация</option>
									<option value="Не врач">Не врач</option>
								</select>
							</div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Клиника/ВУЗ<sup>*</sup></div>
							<div class="newform_fld"><input class="ness" type="text" size="30" name="company" id="company" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl"></div>
							<div class="newform_fld"><button type="button" id="entry_but" onClick="showpart(2);">Далее</button></div>
						</div>
					</div>
					<div class="part2 hidden">
						<h3>Выберите скидку. Обратите внимание: скидка будет направлена менеджеру Ormco для подтверждения</h3>
						<div class="newform_field group">
							<div class="newform_flda"><input type="checkbox" class="dscnt upl" name="poo" id="poo" value="Член ПОО" onClick="$('.newform_upload').fadeIn(300); $('#uploaded').fadeIn(300); setdiscount(15, 'poo', this.checked);" /> <label for="poo">Являюсь членом Профессионального общества ортодонтов</label></div>
						</div>
						<div class="newform_field group">
							<div class="newform_flda"><input type="checkbox" class="dscnt" name="school" id="school" value="Действующий участник Школы ортодонтии" onClick="$('.newform_upload').fadeOut(300); $('#uploaded').fadeOut(300); setdiscount(50, 'school', this.checked);" /> <label for="school">Действующий участник Школы ортодонтии</label></div>
						</div>
						<div class="newform_field group">
							<div class="newform_flda"><input type="checkbox" class="dscnt upl" name="ordinator" id="ordinator" value="Ординатор" onClick="$('.newform_upload').fadeIn(300); $('#uploaded').fadeIn(300); setdiscount(40, 'ordinator', this.checked);" /> <label for="ordinator">Я ординатор</label></div>
						</div>
						<div class="newform_field group hidden" id="promo_radio">
							<div class="newform_flda"><input type="checkbox" disabled checked class="dscnt" name="promo_dscnt" id="promo_dscnt" value="Скидка по промокоду 0%" onClick="$('.newform_upload').fadeOut(300); $('#uploaded').fadeOut(300); setdiscount(promo_dsc, 'promo_dscnt', this.checked);" /> <label for="promo_dscnt">Скидка по промо-коду <span id="promo_d">0</span>%</label></div>
						</div>
						<div class="newform_field newform_upload hidden group">
							<div class="newform_fldu">Загрузить удостоверение <button type="button" class="upload" name="upload" id="upload">Выберите файл</button></div>
						</div>
						<div class="uploaded hidden" id="uploaded">(форматы jpg, gif, png или pdf)</div>
						<div id="promo_field" class="hidden">
							<h4>Если у вас есть промокод, введите его в поле ниже и нажмите кнопку «Применить промокод»</h4>
							<div class="newform_field group">
								<div class="newform_ttl">Промо-код</div>
								<div class="newform_fld"><input type="text" size="30" name="promo" id="promo" /></div>
							</div>
							<div class="newform_field group">
								<div class="newform_ttl"></div>
								<div class="newform_fld"><button type="button" onClick="chk_promo();">Применить промокод</button></div>
							</div>
							<div id="promo_fail" class="form_msg hidden">Вы указали неверный промокод</div>
						</div>
						<div class="discount hidden">
							Ваша скидка <span id="discount">0</span> процентов
						</div>
						<div class="finalprice">
							Стоимость участия <span id="finalprice">7000 р.</span>
						</div>
						<div class="newform_field group">
							<div class="newform_ttl">Оплата:</div>
							<div class="newform_fld"><div class="tabbut active tabbut1" onClick="showtab(1);">Как физ. лицо</div><div class="tabbut tabbut2" onClick="showtab(2);">Как юр. лицо</div></div>
						</div>
						<div class="tab1">
							<div class="newform_field group">
								<div class="newform_ttl">Способ оплаты:</div>
								<div class="newform_fld"><select name="payment_id" id="payment_id" class="ness" onChange="if (this.value == 4666) {showtab(2);}">
										<option value="">выберите способ оплаты</option>
										<option value="4663">Квитанция в банк</option>
										<option value="4664" comment="Не позднее, чем за 10 дней до мероприятия!">Наличными в офисе Ormco</option>
										<option value="17688" comment="Приготовьтесь сразу произвести оплату">Онлайн оплата</option>
										<option value="4666">Счет для юр. лиц</option>
									</select>
								</div>
							</div>
						</div>
						<div class="tab2 hidden">
							<div class="newform_field group yurs">
								<div class="newform_ttl">Юр. лицо:</div>
								<div class="newform_fld"><select name="yur" id="yur" class="ness" disabled>
									</select>
								</div>
							</div>
							<div class="addyur" onClick="addnewyur();">
								<span>+</span> Добавить новое юр. лицо
								<small>Приготовьтесь ввести реквизиты юр. лица</small>
							</div>
							<div class="addyurform addyurform1 hidden">
								<div class="addyur_close" onClick="$('.addyurform').fadeOut(300, function(){$('.newform_reg').fadeIn(300); $('.addyur').fadeIn(300); $('.yurs').fadeIn(300);})"><span class="shape1"></span><span class="shape2"></span></div>
								<div class="newform_field group">
									<div class="newform_ttl">Новое юр. лицо:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="yurname" id="yurname" /><small>Введите юридическое название организации</small></div>
								</div>
								<h4>Контактная информация</h4>
								<div class="newform_field group">
									<div class="newform_ttl">ФИО:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="fio_rukovoditelya" id="fio_rukovoditelya" /><small>ФИО руководителя организации</small></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Должность:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="dolzhnost" id="dolzhnost" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Контактное лицо:</div>
									<div class="newform_fld"><input type="text" size="30" name="contact_person" id="contact_person" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Email<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="yuremail" id="yuremail" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Телефон</div>
									<div class="newform_fld"><input class="short" type="text" size="30" name="phone_number" id="phone_number" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Факс</div>
									<div class="newform_fld"><input class="short" type="text" size="30" name="fax" id="fax" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl"></div>
									<div class="newform_fld"><button type="button" onClick="showyur(2);">Далее</button></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl"></div>
									<div class="newform_fld"><button type="button" onClick="cancelyur();">Отменить добавление</button></div>
								</div>
							</div>
							<div class="addyurform addyurform2 hidden">
								<div class="addyur_back" onClick="showyur(1);"></div>
								<div class="addyur_close" onClick="$('.addyurform').fadeOut(300, function(){$('.newform_reg').fadeIn(300); $('.addyur').fadeIn(300); $('.yurs').fadeIn(300);})"><span class="shape1"></span><span class="shape2"></span></div>
								<div class="newform_field group">
									<div class="newform_ttl">ИНН:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="inn" id="inn" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">КПП:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="kpp" id="kpp" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Рассчетный счет:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="account" id="account" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">БИК:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness short" type="text" size="30" name="bik" id="bik" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Корр. счет:<sup>*</sup></div>
									<div class="newform_fld"><input class="ness" type="text" size="30" name="bank_account" id="bank_account" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">ОГРН:</div>
									<div class="newform_fld"><input type="text" size="30" name="ogrn" id="ogrn" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Юридический адрес:<sup>*</sup></div>
									<div class="newform_fld"><textarea class="ness" name="legal_address" id="legal_address"></textarea></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Фактический адрес:<sup>*</sup></div>
									<div class="newform_fld"><textarea class="ness" name="defacto_address" id="defacto_address"></textarea></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl">Почтовый индекс:</div>
									<div class="newform_fld"><input type="text" size="30" name="postal_address" id="postal_address" /></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl"></div>
									<div class="newform_fld"><button type="button" onClick="addyurajax();">Добавить юр. лицо</button></div>
								</div>
								<div class="newform_field group">
									<div class="newform_ttl"></div>
									<div class="newform_fld"><button type="button" onClick="cancelyur();">Отменить добавление</button></div>
								</div>
							</div>
						</div>
						<div class="newform_field newform_reg group">
							<div class="newform_ttl"></div>
							<div class="newform_fld"><button id="register">Зарегистрироваться</button></div>
						</div>
						<!--<div class="newform_field group">
							<div class="newform_ttla double">Планирую покупку продукции Ormco в ближайшее время (1-2 месяца)</div>
							<div class="newform_flda"><input type="checkbox" size="30" name="want_to_buy" id="want_to_buy" value="1" /></div>
						</div>
						<div class="newform_field group">
							<div class="newform_ttla double">Принимал(а) участие в образовательных мероприятиях Ormco в течение последнего года</div>
							<div class="flda"><input type="checkbox" size="30" name="how_you_been_edu" id="how_you_been_edu" value="1" /></div>
						</div>
						<div id="igosel">
							<h3>Выбранные мастер-классы</h3>
							<div class="igosel" onClick="$('.igodd').fadeToggle(200); $(this).toggleClass('sel');"><span>Выберите три мастер-класса</span></div>
							<div class="igodd">
								<?
								foreach ($mks as $n => $nks)
									{
									?>
									<div class="igotime igo<?=$n;?>"><?=$mktimes[$n];?></div>
									<?
									foreach ($nks as $m => $mk)
										{
										?>
										<div class="igoset igo<?=$n;?><?if ($mkcounts[$m - 1] <= 0) {?> faded cancelled<?}?>" data-group="<?=$n;?>" data-mk="<?=$m;?>" id="igo_<?=$m;?>"><input type="checkbox" <?if ($mkcounts[$m - 1] <= 0) {?>disabled <?}?>name="iset<?=$m;?>" id="iset<?=$m;?>" onClick="igo(<?=$n;?>, <?=$m;?>, '<strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?>', this.checked);" /><label for="iset<?=$m;?>"><strong><?=$speakers[$mk["speaker"]]["name"];?></strong> <?=$mk["theme"];?></label></div>
										<?
									}
								}
								?>
							</div>
							<div class="chosen"></div>
						</div>-->
						<div class="formagree">Нажимая «Отправить», вы соглашаетесь с <span class="lnk" onClick="$('#pol').fadeIn(300);">политикой конфиденциальности</span> и <a target="_blank" href="http://ormco.ru/files/sys/oferta_Ormco_Seminary.docx">условиями договора-оферты</a></div>
					</div>
					</form>
				</div>
			</div>
		</div>
		<div class="mask"></div>
		<div class="mask1"></div>
		<div class="burger" onClick="$('#mobmenu').slideDown(300);"><img src="images/burger.png" /></div>
		<div id="mobmenu" class="mobmenu">
			<div class="close2" onClick="$('#mobmenu').slideUp(300);"><img src="images/close2.png" /></div>
			<ul>
				<li id="miprogramm1"><a href="#programm">Программа</a></li>
				<li id="mispeakers1"><a href="#speakers">Спикеры</a></li>
				<li id="miprice1"><a href="#price">Стоимость</a></li>
				<li id="miplace1"><a href="#place">Место проведения</a></li>
				<li id="miform1"><a href="#form">Регистрация</a></li>
			</ul>
		</div>
	<div class="form_error">
		<div class="close" onClick="$('.form_error').fadeOut(300); $('.mask1').fadeOut(300);"><img src="images/close.png" /></div>
		<h3 id="form_error"></h3>
	</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter23063434 = new Ya.Metrika({
                    id:23063434,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    ecommerce:"dataLayer"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/23063434" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-67881160-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-67881160-2');
</script>
<div id="pol">
<div class="close" onClick="$('#pol').fadeOut(300);"><img src="images/close.png" /></div>
<div id="inpol">
<h2>Политика конфиденциальности</h2>
<h4>1. Общие положения</h4>
Корпорация Ormco в лице ООО «Ормко» (далее «Ормко») и её аффилированные лица с уважением относятся к конфиденциальной информации любого лица, ставшего посетителем данного сайта. Мы хотели бы проинформировать Вас о том, какие именно данные мы собираем и каким образом используем собранные данные. Вы также узнаете о том, как Вы можете проверить точность собранной информации и дать нам указание об удалении подобной информации. Данные собираются, обрабатываются и используются строго в соответствии с требованиями действующего законодательства того государства, в котором расположено соответствующее аффилированное лицо компании "Ормко", отвечающее за защиту персональных данных. Мы делаем все возможное для обеспечения соответствия требованиям действующего законодательства.
Данное заявление не распространяется на сайты, на которые сайт компании "Ормко" содержит гиперссылки.
<h4>2. Сбор, использование и переработка персональных данных</h4>
Мы осуществляем сбор информации, относящейся к определенным лицам, лишь в целях обработки и использования информации и только в том случае, если Вы добровольно предоставили информацию или явно выразили свое согласие на ее использование.
Когда Вы посещаете наш сайт, определенные данные автоматически записываются на серверы компании "Ормко" и/или её аффилированных лиц для целей системного администрирования или для статистических или резервных целей. Записываемая информация содержит название Вашего интернет-провайдера, в некоторых случаях Ваш IP-адрес, данные о версии программного обеспечения Вашего браузера, об операционной системе компьютера, с которого Вы посетили наш сайт, адреса сайтов, после посещения которых Вы по ссылке зашли на наш сайт, заданные Вами параметры поиска, приведшие Вас на наш сайт.
В зависимости от обстоятельств, подобная информация позволяет сделать выводы о том, какая аудитория посещает наш сайт. В данном контексте, однако, не используются никакие персональные данные. Использоваться может лишь анонимная информация. Если информация передается компанией "Ормко" и/или её аффилированными лицами внешнему провайдеру, принимаются все возможные технические и организационные меры, гарантирующие передачу данных в соответствии с требованиями действующего законодательства.
Если Вы добровольно предоставляете нам персональную информацию, мы обязуемся не использовать, не обрабатывать и не передавать такую информацию способом, выходящим за рамки требований действующего законодательства. Использование и распространение Ваших персональных данных без Вашего согласия возможно только на основании судебного решения или в ином порядке, предусмотренном законодательством РФ.
Любые изменения, которые будут внесены в правила по соблюдению конфиденциальности, будут размещены на данной странице. Это позволяет Вам в любое время получить информацию о том, какие данные у нас хранятся и о том, каким образом мы собираем и храним такие данные.
<h4>3. Безопасность данных</h4>
Компания "Ормко" и её аффилированные лица обязуется бережно хранить Ваши персональные данные и принимать все меры предосторожности для защиты Ваших персональных данных от утраты, неправильного использования или внесения в персональные данные изменений. Партнеры компании "Ормко" и её аффилированных лиц, которые имеют доступ к Вашим данным, необходимым им для предоставления Вам услуг от имени компании "Ормко" и её аффилированных лиц, несут перед компанией "Ормко" и её аффилированными лицами закрепленные в контрактах обязательства соблюдать конфиденциальность данной информации и не имеют права использовать предоставляемые данные для каких-либо иных целей.
<h4>4. Персональные данные несовершеннолетних потребителей</h4>
Компания "Ормко" и её аффилированные лица не ведет сбор информации в отношении потребителей, не достигших 14 лет. При необходимости, мы можем специально попросить ребенка не присылать в наш адрес никакой личной информации. Если родители или иные законные представители ребенка обнаружат, что дети сделали какую-либо информацию доступной для компании "Ормко" и её аффилированных лиц, и сочтут, что предоставленные ребенком данные должны быть уничтожены, таким родителям или иным законным представителям необходимо связаться с нами по нижеуказанному (см. п. 6) адресу. В этом случае мы немедленно удалим личную информацию о ребенке.
<h4>5. Файлы Cookie</h4>
Мы используем файлы cookie для того, чтобы предоставить Вам больше возможностей при использовании сайта. Файлы cookie представляют собой небольшие блоки данных, помещаемые Вашим браузером на временное хранение на жестком диске Вашего компьютера, необходимые для навигации по нашему сайту. Файлы cookie не содержат никакой личной информации о Вас и не могут использоваться для идентификации отдельного пользователя. Файл cookie часто содержит уникальный идентификатор, представляющий собой анонимный номер (генерируемый случайным образом), сохраняемый на Вашем устройстве. Некоторые файлы удаляются по окончании Вашего сеанса работы на сайте; другие остаются на Вашем компьютере дольше.
Приступая к использованию данного веб-сайта, вы соглашаетесь на использование файлов cookie, Вы также признаете, что в подобном контенте могут использоваться свои файлы cookie.
Компания Ормко не контролирует и не несет ответственность за файлы cookie сторонних разработчиков. Дополнительную информацию Вы можете найти на сайте разработчика.
<h4>6. Отслеживание через интернет</h4>
На данном сайте осуществляется сбор и хранение данных для маркетинга и оптимизации с использованием технологии Яндекс.Метрика и Google Analitycs. Эти данные могут использоваться для создания профилей пользователей под псевдонимами. Сайт может устанавливать файлы cookie.
Без ясно выраженного согласия наших пользователей данные, собираемые с помощью технологий Яндекс.Метрика и Google Analitycs, не используются для идентификации личности посетителя и не связываются с какими-либо другими личными данными носителя псевдонима.
Дополнительную информацию об отслеживании через интернет Вы можете найти на сайтах провайдеров сервисов Яндекс.Метрика и Google Analitycs.
<h4>7. Ваши пожелания и вопросы</h4>
Хранящиеся данные будут стерты компанией "Ормко" и/или её аффилированными лицами по истечении периода хранения, установленного законодательством или договором либо в случае если сама компания "Ормко" и/или её аффилированные лица отменит хранение тех или иных данных. Вы вправе в любое время потребовать удаления из базы данных компании "Ормко" и/или её аффилированных лиц информации о Вас. Вы также вправе в любое время отозвать Ваше согласие на использование или переработку Ваших персональных данных. В таких случаях, а также, если у Вас есть какие-либо иные пожелания, связанные с Вашими персональными данными, просим Вас выслать письмо по почте в адрес «Ормко» в России по адресу: 195112, Санкт-Петербург, Малоохтинский пр-т, д. 64, корп. 3. или по электронной почте dc-sales@ormco.com. Просим Вас также связаться с нами в случае, если Вам хотелось бы узнать, собираем ли мы данные о Вас и если да, то какие именно данные. Мы постараемся выполнить Ваши пожелания в возможно короткие сроки.
<h4>8. Законодательство по обработке персональных данных</h4>
Все действия с персональными данными, собираемыми на данном сайте, производятся в соответствии с Федеральным законом Российской Федерации №152-ФЗ от 27 июля 2006 года «О персональных данных».
<h4>(1) Заявленная цель сбора, обработки или использования данных:</h4>
•	Предметом деятельности «Ормко» и её аффилированных лиц является производство и распространение стоматологических продуктов всех типов, главным образом брекет-систем, ортодонтических инструментов, микроимплантов, адгезивов;
<h4>(2) Описание групп вовлеченных лиц и соответствующих данных или категорий данных:</h4>
Данные, касающиеся заказчиков, сотрудников, пенсионеров, сотрудников сторонних компаний (субподрядчиков), персонала, работающего по лизингу, претендентов на рабочие места, авторов изобретений, не являющихся сотрудниками компаний, или наследников, соответственно, поставщиков товаров и услуг, сторонних заказчиков, потребителей, добровольцев, участвующих в потребительских испытаниях, посетителей производственных объектов корпорации, инвесторов – насколько это необходимо для выполнения целей, определенных в пункте 4.
<h4>(3) Получатели или категории получателей, которым могут быть разглашены данные:</h4>
Органы власти, фонды страхования здоровья, ассоциация по страхованию ответственности работодателей при наличии соответствующего правового регулирования, сторонние подрядчики в соответствии сторонние поставщики услуг, ассоциация пенсионеров «Ормко», аффилированные лица и внутренние подразделения для выполнения целей, указанных в пункте 4.
<h4>(4) Периодичность регулярного удаления данных:</h4>
Юристами подготовлено множество инструкций, касающихся обязанностей по хранению данных и периодов хранения. Данные удаляются в установленном порядке по истечении указанных периодов. Данные, не подпадающие под действие данных условий, удаляются, если цели, указанные в пункте 4, перестают существовать.
<h4>(5) Запланированная передача данных другим странам:</h4>
В рамках всемирной системы информации о трудовых ресурсах, данные по персоналу должны быть доступны определенным руководящим работникам в других странах. Соответствующие соглашения о защите данных должны быть заключены с соответствующими компаниями в соответствии со стандартами ЕС.
<h4>9. Использование встраиваемых модулей для социальных сетей</h4>
На наших интернет-страницах предусмотрена возможность встраивания модулей для социальных сетей ВКонтакте, Twitter, Instagram, Youtube (далее – «провайдеры»).
Только если Вы активируете модуль, тем самым разрешая передачу данных, браузер создаст прямое соединение с сервером провайдера. Содержимое различных модулей впоследствии передается соответствующим провайдером непосредственно в Ваш браузер и выводится на экран Вашего компьютера.
Модуль сообщает провайдеру, на какую из страниц нашего сайта Вы вошли. Если во время просмотра нашего сайта Вы вошли на ВКонтакте, Instagram, Youtube или Twitter под своей учетной записью, провайдер может подобрать информацию, в соответствии с Вашими интересами, т.е. информацию, которую Вы просматриваете с помощью Вашей учетной записи. При использовании какой-либо функции встроенного модуля (например, кнопки “Мне нравится”, размещения комментария), эта информация также будет передана браузером непосредственно провайдеру для сохранения.
Дополнительную информацию по сбору и использованию данных социальными сетями, а также по правам и возможностям защиты Вашей конфиденциальности в данных обстоятельствах, можно найти в рекомендациях провайдеров по защите данных /конфиденциальности:
Для того, чтобы не подключаться к учетным записям провайдеров при посещении нашего сайта, Вам необходимо отключиться от соответствующей учетной записи перед посещением наших интернет-страниц.
</div>
</div>
	</body>
</html>
