<?php
session_start();

require_once 'standalone.php';

// только админ
$permissions = permissionsCollection::getInstance();
if(!$permissions->isAdmin()){
    die();
}

$del_id = getRequest('del_id');
if(!empty($del_id)){
    umiObjectsCollection::getInstance()->delObject($del_id);
    header('Location:/clear_double_speakers.php');
    exit();
}

?>
<table width="100%" border="1">
    <tr>
        <th width="25%">Имя спикера, H1, краткое описание</th>
        <th>Мероприятия спикера</th>
    </tr>
<?php
$speakers = new selector('objects');
$speakers->types('object-type')->id(139);
$speakers->order('name');
foreach($speakers as $speaker){
    echo "<tr><td valign='top'>";
    echo "<h4><a href='https://orthodontia.ru/admin/data/guide_item_edit/{$speaker->id}/' target='_blank'>{$speaker->name}</a></h4>";
    echo "<b>{$speaker->getValue('h1')}</b>";
    echo "<div>{$speaker->getValue('full_desrc')}</div>";
    echo "<div style='text-align:right'><a href='?del_id={$speaker->id}' onclick='return confirm(\"Удаление объекта будет невозможно отменить. Удалить объект с потерей всех данных?\");'>удалить</a></div>";
    echo "</td><td class='events'>";
    $events = new selector('objects');
    $events->types('object-type')->name('catalog', 'object');
    $events->where('speaker')->equals($speaker->id);
    foreach($events as $event){
        echo "<a href='https://orthodontia.ru/admin/catalog/edit/{$event->id}/' target='_blank'>{$event->id}</a> - {$event->name}<br>\n";
    }
    echo "</td></tr>";
}
?>
</table>

<style>
    th,td{padding:2px;}
    .events{font-size:13px;vertical-align:top;}
</style>